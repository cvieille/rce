﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImpresoraZebra.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ImpresoraZebra" %>

<div id="divEstadoImpresoras" class="row d-flex justify-content-center"></div>

<script src="<%= ResolveClientUrl("~/Script/Zebra/BrowserPrint-3.1.250.min.js") %>"></script>
<script src="<%= ResolveClientUrl("~/Script/ModuloGEN/ImpresorasZebra.js") + "?v=" + GetVersion() %>" ></script>
