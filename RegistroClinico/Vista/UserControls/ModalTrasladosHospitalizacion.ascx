﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalTrasladosHospitalizacion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalTrasladosHospitalizacion" %>
<div class="modal fade" id="mdlTrasladosHospitalizacion" tabindex="-1" style="z-index:1060">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-9">
                        <h4>Movimientos <span id="tituloTraslados"></span></h4>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <button id="lnkExportarTrasladosExcel" type="button" class="btn btn-info form-control btn-exportar">
                            <i class="fa fa-file-excel-o" style="font-size: 18px;"></i>Exportar a excel 
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <h6 id="nombrePacienteTraslado"></h6>
                <hr class="mb-4" />
                
                <table id="tblTrasladosHospitalizacion" class="table table-striped table-bordered table-hover nowrap dataTable no-footer collapsed w-100">
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalTrasladosHospitalizacion.js") + "?v=" + GetVersion()  %>"></script>