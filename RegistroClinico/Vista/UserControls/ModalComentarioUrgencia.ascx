﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalComentarioUrgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalCambioCama" %>
<!--modal ingreso comentarios-->
<div class="modal fade" id="modalComentariosAtencion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h5 class="modal-title" id="">Ingrese los comentarios a la atención</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="txtIdAtecionUrgenciaComentarios" />
                    <span id="txtNombrePacienteModalComentarios"></span>
                    <textarea id="txtComentarioAtencion" class="form-control" placeholder="Escriba sus comentarios aca.." style="resize: none;"></textarea>
                    <button type="button" onclick="guardarComentario()" class="btn btn-primary m-1">Guardar comentario</button>
                </div>
                <div id="divComentariosAtencion" class="row"></div>
            </div>
            <div class="modal-footer">
                <!-- Nombre del usuario logueado a la izquierda -->
                <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                    <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                    <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Fion modal comentarios-->

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalComentarioUrgencia.js") + "?v=" + GetVersion()  %>"></script>
