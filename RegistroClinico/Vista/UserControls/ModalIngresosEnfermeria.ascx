﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalIngresosEnfermeria.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalIngresosEnfermeria" %>
<!---Este es el modal ingreso de enfermeria--->
<div id="mdlCamaOrigen" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title"><strong><i class='fa fa-bed nav-icon mr-2'></i>Ingreso de enfermería</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Id hospitalización</label>
                        <input type="text" class="form-control" disabled id="numHospitalizacionCamaOrigen" />
                    </div>
                    <div class="col-md-5">
                        <label>Nombre paciente</label>
                        <input type="text" class="form-control" disabled id="nomPacienteCamaOrigen" />
                    </div>
                    <div class="col-md-4">
                        <label>Ubicación actual</label>
                        <input type="text" class="form-control" disabled id="ubicacionActualCamaOrigen" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-4">
                        <h3 class="text-center">Historial de ingresos</h3>
                    </div>
                    <div class="col-md-12">
                        <p class="text-center">
                            <i class="fa fa-info-circle"></i>
                            La tabla detalla el historial de ingresos a servicios que ha tenido el paciente en su hospitalización.
                        </p>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblIngresosEnfermeria" class="table table-hover table-striped table-bordered w-100"></table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="button" onclick="$('#mdlCamaOrigen').modal('hide'); return false;">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalIngresosEnfermeria.js") + "?v=" + GetVersion()  %>"></script>