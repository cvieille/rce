﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalBrazaleteHospitalizacion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalBrazaleteHospitalizacion" %>

<div id="mdlBrazalete" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">

                <h6 class="heading lead" id="txtModalBrazalete"></h6>
                <div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <label for="txtNumHospModalBrazalete">N° Hospitalizacion</label>
                        <input type="text" class="form-control" id="txtNumHospModalBrazalete" disabled />
                    </div>
                    <div class="col-md-4">
                        <label for="txtNomPacModalBrazalete">Paciente</label>
                        <input type="text" class="form-control" id="txtNomPacModalBrazalete" disabled />
                    </div>
                    <div class="col-md-3">
                        <label for="txtNumCamaModalBrazalete">Cama</label>
                        <input type="text" class="form-control" id="txtNumCamaModalBrazalete" disabled />
                    </div>
                    <hr />
                </div>
                <div class="row mt-4">
                    <div class="col-md text-left text-black">
                        <h5><span>Uso de Brazalete</span></h5>
                    </div>
                </div>
                <div class="row" id="divBrazalete">
                    <div class="col-12 col-md-6 col-lg-2">
                        <label for="sltTipoExtremidadBrazalete" id="lblIdTipoExtremidad" class="form-label mt-3">Tipo Extremidad</label>
                        <select id="sltTipoExtremidadBrazalete" class="form-control">
                        </select>
                    </div>
                    <div class="col-12 col-md-6 col-lg-2">
                        <label for="sltIdTipoPlano" id="lblIdTipoPlano" class="form-label mt-3">Plano</label>
                        <select id="sltIdTipoPlano" class="form-control">
                        </select>
                    </div>
                    <div class="col-12 col-md-5 col-lg-2">
                        <label for="inputFechaPlano" id="lblIdFechaPlano" class="form-label mt-3">Fecha instalación</label>
                        <input type="date" class="form-control" name="name" id="inputFechaPlano"/>
                    </div>
                    <div class="col-12 col-md-4 col-lg-2">
                        <label for="inputHoraPlano" id="lblIdHoraPlano" class="form-label mt-3">Hora instalación</label>
                        <input type="time" class="form-control" name="name" id="inputHoraPlano"/>
                    </div>
                    <div class="col-12 col-md-1 col-lg-2">
                        <br />
                        <br />
                        <button type="button" class="btn btn-outline-info" id="lblagregarBrazalete" onclick="agregarBrazalete(this)"><i class="fa fa-save"></i>Agregar</button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xl mt-4">
                        <div class="table-responsive-xl">
                            <table id="tblBrazalete" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Movimientos del brazalete -->
<div class="modal" tabindex="-1" role="dialog" id="mdlMovimientosBrazalete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white" id="mdlHeaderMovimientosBrazalete">
                <h5 class="modal-title">Movimientos del Brazalete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="mdlBodyMovimientosBrazalete">
                <div id="contenidoMovimientoBrazalete">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalBrazaleteHospitalizacion.js") + "?v=" + GetVersion() %>"></script>
<!-- no subir sin GetInfo() -->
