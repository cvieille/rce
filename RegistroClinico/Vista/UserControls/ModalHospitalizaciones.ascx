﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalHospitalizaciones.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalHospitalizaciones" %>

<div id="mdlVerHospitalizaciones" class="modal show fade" role="dialog" aria-labelledby="mdlVerHospitalizaciones"
    aria-hidden="true" tabindex="1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header-info">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-9">
                        <h4><i class="fas fa-history"></i> Historial de Hospitalizaciones</h4>
                    </div>
                </div>
            </div>    
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h5>Paciente: <span id="nomPacModalVerHospitalizaciones"></span></h5>
                        <input type="hidden" id="txtIdHospModalHosp" />
                    </div>
                    <div class="col-md-12">
                            <ul class="nav nav-tabs" id="tabHistorialHospitalizacion" role="tablist">
                              <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="btn-hosp-rce-tab" data-toggle="tab" data-target="#hosp-rce-tab" type="button" role="tab" aria-controls="hosp-rce-tab" aria-selected="true">Historial de RCE</button>
                              </li>
                              <li class="nav-item" role="presentation">
                                <button class="nav-link" id="btn-otras-hosp-tab" data-toggle="tab" onclick="buscarOtrasHospitalizaciones()" data-target="#otras-hosp-tab" type="button" role="tab" aria-controls="otras-hosp-tab" aria-selected="false">Otros registros</button>
                              </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="hosp-rce-tab" role="tabpanel" aria-labelledby="hosp-rce-tab">
                                    <div class="mt-2">
                                        <table id="tblHistorialHospitalizacionesModal" class="table table-striped table-bordered table-hover  nowrap dataTable no-footer w-100">
                                        </table>
                                    </div>
                              </div>
                              <div class="tab-pane fade  p-1" id="otras-hosp-tab" role="tabpanel" aria-labelledby="otras-hosp-tab">
                                  <table id="tblHistorialOtrasHospitalizacionesModal" class="table table-striped table-bordered table-hover mt-2 text-wrap dataTable no-footer w-100">
                                  </table>
                              </div>
                            </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal modal-print fade" id="mdlImprimirHisHos" tabindex="-1">
    <div class="modal-dialog modal-fluid" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <p class="heading lead">
                    <strong><i class="fa fa-print"></i>Impresión</strong>
                </p>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true" class="white-text">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body-print">
                    <iframe id="frameImpresionHis" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalHospitalizaciones.js") + "?v=" + GetVersion() %>"></script>