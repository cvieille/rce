﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.Login" %>
<div id="mdlLogin" class="modal fade left" role="dialog" aria-labelledby="mdlLogin" aria-hidden="true" tabindex="-1" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info center-horizontal">
                <div class="heading lead form-inline">Inicio de sesión</div>
            </div>
            <div class="modal-body">
                <label for="txtUsuarioLogin">Usuario</label>
                <input id="txtUsuarioLogin" class="form-control" type="text" maxlength="8" placeholder="Nombre de usuario" />
                <label for="txtClaveLogin">Contraseña</label>
                <input id="txtClaveLogin" class="form-control" type="password" placeholder="Contraseña" />
            </div>
            <div class="modal-footer justify-content-center">
                <a id="aLoginModal" class="btn btn-default btn-block z-depth-2 waves-effect waves-light">Iniciar Sesión
                </a>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/Login.js") + "?v=" + GetVersion() %>"></script>