﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalPerfiles.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalPerfiles" %>

<script>

    $(document).ready(function () {

        $("#btnSeleccionarPerfil").on('click', async function () {
            ShowModalCargando(true);
            $("#btnSeleccionarPerfil").attr("disabled", "disabled");
            await Redirigir();
            ShowModalCargando(false);
            $("#btnSeleccionarPerfil").removeAttr("disabled");
        })

    })

    function obtenerToken(idPerfil) {

        let success = false;
        let resp;
        let user, pass;

        if ($("#txtUsuario").length && $("#txtClave").length) {

            user = $('#txtUsuario').val();
            pass = CryptoJS.MD5($('#txtClave').val());
        } else {
            var usuario = {};
            usuario = GetLoginPass();
            user = usuario.login;
            pass = usuario.pass;
        }

        var webapi = GetWebApiUrl() + "recuperarToken";
        var datos = {
            "grant_type": "password",
            "username": user,
            "password": '' + pass,
            "clientid": '14',
            "idPerfil": idPerfil
        }

        delete $.ajaxSettings.headers['Authorization'];

        $.ajax({
            type: "POST",
            url: webapi,
            data: datos,
            async: false,
            success: function (response) {
                resp = response;
                success = true;
            },
            error: function (err) {
                console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
            }
        });

        if (success) {
            setSession("TOKEN", resp.access_token);
            setSession("FECHA_FINAL_SESION", moment(GetFechaActual()).add(parseInt(resp.expires_in), 'seconds').format('YYYY-MM-DD HH:mm:ss'));
            $.ajaxSetup({
                headers: { 'Authorization': GetToken() }
            });
        }
    }

    function Redirigir() {


        const _redirigir = async () => {

            ShowModalCargando(true);

            await sleep(100);

            deleteSession('MODULO');
            deleteSession('MENU');
            sessionStorage.removeItem('menuRCE');
            let bValidar = true;
            if ($("#ucMdlPerfiles_ddlPerfiles")[0])
                drop = "#ucMdlPerfiles_ddlPerfiles";
            else
                drop = "#<%=ddlPerfiles.ClientID%>";

            bValidar = ValidarDropDown($(drop), bValidar);
            if (!bValidar)
                return false;

            const iCodigoPerfil = parseInt($(drop).val().split('|')[0]);
            const iIdPerfil = parseInt($(drop).val().split('|')[1]);
            const sNombrePerfil = $(drop + " option[value='" + $(drop).val() + "']").text();

            setSession("CODIGO_PERFIL", iCodigoPerfil);
            setSession("ID_PERFIL", iIdPerfil);
            setSession("PERFIL_USUARIO", sNombrePerfil);

            obtenerToken(iIdPerfil);
            $("#mdlPeriles").modal("toggle");
            //Acá hay otro SetSessionServer OJO!
            SetSessionServer({
                LOGIN_USUARIO: getSession().LOGIN_USUARIO,
                NOMBRE_USUARIO: getSession().NOMBRE_USUARIO,
                CODIGO_PERFIL: iCodigoPerfil,
                PERFIL_USUARIO: sNombrePerfil,
                TOKEN: getSession().TOKEN
            });

            // Valida si la contraseña es igual al RUT del usuario
            if (getSession().ES_CAMBIO_CONTRASEÑA == true) {
                window.location.replace(`${ObtenerHost()}/Vista/CambioContrasena.aspx`);
            } else
                window.location.replace(`${ObtenerHost()}/Vista/Inicio.aspx`);
        }

        _redirigir().then(() => ShowModalCargando(false));

    }

    function ValidarDropDown(drop, b) {

        var bValido = true;

        if (drop[0].selectedIndex == 0) {
            bValido = false;
            if (!drop.hasClass('is-invalid'))
                drop.addClass('is-invalid');
        } else {
            if (drop.hasClass('is-invalid'))
                drop.removeClass('is-invalid');
        }

        //es válido si no hay otra opción... los valido onda los opcionales
        if (drop[0].length == 1) {
            if (drop.hasClass('is-invalid'))
                drop.removeClass('is-invalid');
            bValido = true;
        }

        if (!b)
            return false;

        return bValido;

    }

</script>
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header bg-info text-white">
            <h5 class="modal-title">PERFILES DE SISTEMA</h5>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12 ">
                    <div class="active">Seleccione un perfil:</div>
                    <asp:DropDownList ID="ddlPerfiles" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-12 mt-3">
                    <button id="btnSeleccionarPerfil" type="button" class="btn btn-primary btn-block">
                        Ingresar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
