﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalSignosVitales.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalSignosVitales" %>
<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>

<div id="mdlSignosVitales" class="modal fade ml-2 mr-2" tabindex="-1">
    <div class="modal-dialog modal-fluid modal-dialog-centered" role="document">
        <div class="modal-content ml-4 mr-4">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="staticBackdropLabel12"><i class="fas fa-heartbeat" aria-hidden="true"></i> Signos Vitales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-header">
                <div class="col-lg-12">
                    <h6 class="modal-title">Nombre de paciente:
                        <label id="lblPacienteSignosVitales"></label>
                    </h6>
                </div>
            </div>
            <div class="modal-body">
                <div id="divSignosVitales"></div>
                <div class='text-right mt-2' id="divBotonesSignosVitales">
                    <%--<a id="aGuardarSignoVital" class="btn btn-info"><i class="fa fa-save"></i>Guardar</a>--%>
                    <button id="aGuardarSignoVital" class="btn btn-info"><i class="fa fa-save mr-1"></i>Guardar</button>
                    <button id="btnCargarSignos" class="btn btn-success d-none" onclick="cargarSignosVitales(this)" type="button">Cargar registros anteriores</button>
                    <button id="aEditarSignoVital" class="btn btn-warning" onclick="editarSignosVitales(this)" style="display:none"><i class="fa fa-save mr-1"></i>Editar</button>
                </div>
                <!--<wuc:SignosVitales ID="wucSignosVitales" runat="server"/>-->

                <div class="accordion m-3">
                    <div id="historialItemsModal">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalSignosVitales.js") + "?v=" + GetVersion()  %>"></script>
