﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalCerrarAtencionUrgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalCerrarAtencionUrgencia" %>
<!-- Modal Cerrar Atencion -->
<div class="modal fade" id="mdlCerrarAtencion" role="dialog" aria-labelledby="mdlCerrarAtencion" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h2><strong>Cerrar Atención</strong></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                        <div class="card border-secondary mb-3">
                            <div class="card-header text-center bg-info">Cerrar atención</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Observaciones de cierre:</label>
                                        <textarea id="observacionesCierreAtencion" class="form-control" maxlength="100" rows="5"></textarea>
                                        <br />
                                    </div>
                                </div>
                                <div id="divHospitalizacion">
                                    <div class="card border-secondary mb-3">
                                        <div class="card-header text-center bg-info">Hospitalización</div>
                                        <div class="card-body">
                                            <h6 class="text-center">¿El paciente aceptó la hospitalización?</h6>
                                            <div class="w-100 text-center p-2">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input id="rdoSiHospitalizacion"  type="radio" class="form-check-input" name="hos" data-required="true"> SI
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input id="rdoNoHospitalizacion" type="radio" class="form-check-input" name="hos" data-required="true"> NO
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="divMotivosRechazoHospitalizacion">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Motivo de rechazo</label>
                                                        <select id="sltTipoMotivoRechazo" class="form-control" data-required="true"></select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Obsevaciones del rechazo</label>
                                                        <textarea id="txtObservacionesRechazoHospitalizacion" class="form-control" placeholder="Especifique información adicional" rows="4"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row d-flex flex-row justify-content-end w-100">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                        <button type="button" id="btnCerrarCaso" onclick="CerrarAtencionUrgencia(this)" class="btn btn-primary btn-md w-100">Aceptar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                        <!-- Datos de paciente -->
                        <div class="card border-secondary mb-3">
                            <div class="card-header text-center bg-info">Datos de paciente</div>
                            <div class="card-body">
                                <div class="row" id="divDatosPancienteCierreAtencion">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                    <!-- Nombre del usuario logueado a la izquierda -->
                    <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                        <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                        <a id="aCancelarCierre" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal Cerrar Atencion -->
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalCerrarAtencionUrgencia.js") + "?v=" + GetVersion()  %>"></script>
