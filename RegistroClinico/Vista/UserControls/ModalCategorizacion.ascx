﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalCategorizacion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalCategorizacion" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<div id="mdlCategorizacion" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h5 class="modal-title"><i class="fa fa-address-card" aria-hidden="true"></i> Categorizando paciente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>            
            <div class="modal-body">
                <div class="row d-none" id="cuadroAyudaRecienNacido" >
                    <div class="alert alert-warning alert-dismissible fade show"  role="alert">
                      <h5>Categorizando recién nacido</h5>
                      <i class="fa fa-info-circle fa-lg"></i> Se hará una busqueda automática de RN asociados al rut materno,
                        seleccione recién nacido a categorizar y responda el cuestionario para categorizarlo.<br />
                        La categorización del RN estará asociada a la última categorización de la madre, por esta razón deberia categorizar primero a la madre y luego al RN

                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                    </div>
                </div>

                <div class="row m-2">
                    <div class="col-md-12 d-none" id="divUcPaciente">
                        <wuc:Paciente ID="wucPaciente" runat="server" />
                    </div>
                </div>

                <div class="row" id="informacionPacienteCategorizar">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-warning" role="alert">
                            <i class="fa fa-info-circle fa-xl"></i> El paciente solo puede ser categorizado una vez durante el dia (24 hrs)
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <h5><b>Nombre:</b><span id="txtCategorizacionNombrePaciente"></span> </h5>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <h5><b>Id Atención</b> <span id="txtCategorizacionIdHospitalizacion"></span></h5>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <h5><b>Cama:</b><span id="txtCategorizacionNombreCama"></span></h5>
                        <input type="hidden" id="txtEstadoCamaActual" />
                        <input type="hidden" id="inputIdUbicacion" />
                        <input type="hidden" id="inputIdCategorizacion" />
                        <input type="hidden" id="inputIdCama" />
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-check mb-2">
                            <input class="form-check-input" type="checkbox" id="checkFormularioSaludMental" onchange="cambiarFormularioSaludMental(this)">
                            <label class="form-check-label" for="checkFormularioSaludMental">
                                Usar formulario de salud mental <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Desplegar preguntas de un paciente psiquiatrico." style="cursor: pointer;"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <button class="btn btn-danger" type="button" id="btnUcPacienteAusente" onclick="categorizarPacienteAusente()">Paciente ausente</button>
                    </div>
                </div>

                <div id="contenidoTablas">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="categorizacion-tab" onclick="cerrarPopOvers()" data-toggle="tab" href="#categorizacion" role="tab" aria-controls="categorizacion" aria-selected="true">Nueva categorización</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="historialcategorizacion-tab" data-toggle="tab" href="#historialcategorizacion" role="tab" aria-controls="historialcategorizacion" aria-selected="false">Historial categorizaciones</a>
                        </li>
                    </ul>
                    <div class="tab-content" >
                        <div class="tab-pane fade show active m-1" id="categorizacion" role="tabpanel" aria-labelledby="categorizacion-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive-xl">
                                        <table id="categorizacionPreguntas" class="table table-hover table-striped table-responsive"></table>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-2 offset-md-10">
                                    <button type="button" id="btnUcCategorizarPaciente" class="btn btn-primary" onclick="categorizarPaciente()">Categorizar</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade m-1" id="historialcategorizacion" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive-xl">
                                        <table id="tblHistorialCategorizacion" class="table table-hover table-striped"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSiguiente" class="btn btn-success d-none" onclick="categorizarNuevoPaciente()">Siguiente</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalCategorizacion.js") + "?v=" + GetVersion() %>"></script>
