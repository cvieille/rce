﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalConsentimientoDocente.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalConsentimientoDocente" %>


<div class="modal" id="mdlConsentimiento" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-info">
        <h5 class="modal-title"><i class="fa fa-universal-access" aria-hidden="true"></i> Formulario consentimiento docente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <label>Nombre paciente</label>
                        <input type="text" class="form-control" id="txtPacienteConsentimiento" disabled />
                        <input type="hidden" class="form-control" id="txtIdPacienteConsentimiento" disabled />
                    </div>

                    <div class="col-md-12 m-2">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                    id="nav-historialconsentimiento-tab"
                                    data-toggle="tab"
                                    href="#historialconsentimiento"
                                    role="tab"
                                    aria-controls="nav-historialconsentimiento"
                                    aria-selected="true">Historial de informes consentimiento</a>
                                <a class="nav-item nav-link"
                                    id="nav-nuevoconsentimiento-tab"
                                    data-toggle="tab"
                                    href="#nuevoconsentimiento"
                                    role="tab"
                                    aria-controls="nav-nuevoconsentimiento"
                                    aria-selected="false">Nuevo informe consentimiento</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="historialconsentimiento" role="tabpanel" aria-labelledby="nav-historialconsentimiento-tab">
                                <div class="col-md-12 p-1">
                                    <%--tabla de informes de consentimiento--%>
                                    <table id="tblHistorialConsentimientos" class="w-100 dataTable table table-hover"></table>
                                    <%--fin tabla de consentimiento--%>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nuevoconsentimiento" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                        <label>Fecha</label>
                                        <input type="text" class="form-control" id="txtFechaConsentimiento" disabled />
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <p>Paciente se auto representa</p>
                                        <input type="checkbox" class="form-control" id="checkAutoRepresenta" />
                                        <input type="hidden" class="form-control" id="idAtencionConsentimineto" />
                                        <input type="hidden" class="form-control" id="TipoAtencionConsentimiento" />
                                    </div>
                                </div>
                                <div class="row" id="divAcomConsentimiento">
                                    <div class="col-md-2 col-sm-6">
                                        <label id="Tipo identificación"></label>
                                        <select class="form-control" id="sltTipoIdentificacionConsentimiento" data-required="true"></select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label id="labelTipoIdentificacion" for="txtnumeroDocAcomCosentimiento">Numero documento</label>
                                        <div class="input-group">
                                            <input id="txtnumeroDocAcomCosentimiento" type="text" class="form-control numero-documento number" placeholder="Rut sin puntos" style="width: 100px;" data-required="true" maxlength="8">
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">
                                                    <strong>-</strong>
                                                </div>
                                            </div>
                                            <input id="txtDigAcomConsentimiento" type="text" class="form-control digito text-center" maxlength="1" style="width: 15px;" placeholder="DV">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-12"></div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="txtNombreAcomConsentimiento">Nombre</label>
                                        <input type="text" id="txtnombreacompanante" class="form-control" data-required="true" disabled maxlength="30" />
                                        <input type="hidden" id="txtidacompanante" />
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="txtApellidoPAcomConsentimiento">Primer Apellido</label>
                                        <input type="text" id="txtapellidopcompanante" class="form-control" data-required="true" disabled maxlength="30" />
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="txtApellidoMAcomConsentimiento">Segundo Apellido</label>
                                        <input type="text" id="txtapellidomacompanante" class="form-control" data-required="true" disabled maxlength="30" />
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="txtApellidoMAcomConsentimiento">Tipo representación</label>
                                        <select id="sltTipoAcomConsentimiento" class="form-control" data-required="true"></select>
                                    </div>

                                </div>
                                <div class="row m-2">
                                    <div class="col-md-12">
                                        <h5>El paciente declara haber recibido y comprendido la información sobre la actividad docente que se 
                                desarrolla en el Hospital Clínico de Magallanes, sus objetivos, beneficios, potenciales 
                                riesgos.
                                        </h5>
                                    </div>
                                    <div class="col-md-12">
                                        <label>¿El paciente manifestó estar conforme?</label>
                                        <input type="checkbox" id="sltAceptaConsentimiento" class="form-control" />
                                    </div>
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary" onclick="generarConsentimiento()">Generar consentimiento</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalConsentimientoDocente.js") + "?v=" + GetVersion() %>"></script>
<script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>

