﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;

namespace RegistroClinico.Vista.UserControls
{
    public partial class CargadoDeArchivos : Handlers.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Funciones.ValidaSesion(this.Page);
        }

        protected void descargarArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                string data = Request["__EVENTARGUMENT"];
                HttpContext contexto = HttpContext.Current;
                Dictionary<string, string> json = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string path = Funciones.GetPropiedad(ref contexto, "urlDocumentos") + "\\Modulo" + json["modulo"] + "\\" + json["id"] + "\\" + json["nombreArchivo"];

                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + json["nombreArchivo"] + ";");
                Response.WriteFile(path);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }
    }
}