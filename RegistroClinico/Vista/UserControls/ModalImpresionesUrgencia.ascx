﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalImpresionesUrgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalPerfiles" %>
<%@ Register Src="~/Vista/UserControls/ModalIngresoBrazalete.ascx" TagName="Brazalete" TagPrefix="wuc" %>

<!-- Modal Imprimir -->
<div class="modal fade" id="mdlBotoneraImprimir" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h5 class="modal-title" id="tituloModalImprimir">Seleccione documento a imprimir.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 id="textoModalImprimir"></h5>
                <div class="row" id="divBotoneraUrgenciaUc">
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btnImprimirDau" type="button" data-tipo="dau" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fa fa-print fa-1x"></i>
                            <br />
                            Imprimir DAU</button>
                    </div>
                    <div class="col-6 p-1" id="divReceteImprimirUc">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirReceta" type="button" data-tipo="receta" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-file" aria-hidden="true"></i>
                            <br />
                            Imprimir receta</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btnBrazalete" type="button" data-tipo="dau">
                            <i class="fa fa-ticket-alt fa-1x"></i>
                            <br />
                            Brazalete</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btnImprimirExamenesLab" type="button" data-tipo="exameneslab" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-microscope fa-1x"></i>
                            <br />
                            Imprimir exámenes laboratorio</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btnImprimirExamenesImagen" type="button" data-tipo="examenesimagen" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-chalkboard-teacher fa-1x"></i>
                            <br />
                            Imprimir exámenes imageneologia</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirProcedimientos" type="button" data-tipo="procedimientos" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-heartbeat fa-1x"></i>
                            <br />
                            Imprimir procedimientos</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirMedicamentos" type="button" data-tipo="medicamentos" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-pills fa-1x"></i>
                            <br />
                            Imprimir medicamentos</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirInterconsultor" type="button" data-tipo="interconsultor" onclick="imprimirFormularioUrgenciaUc(this)" disabled="disabled">
                            <i class="fas fa-user-md fa-1x"></i>
                            <br />
                            Imprimir interconsultor</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirEvolucion" type="button" data-tipo="evolucion" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-file-medical-alt" aria-hidden="true"></i>
                            <br />
                            Imprimir evolución</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirSolicitudHos" type="button" data-tipo="solicitudhos" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-file-medical-alt" aria-hidden="true"></i>
                            <br />
                            Imprimir Solicitud Hospitalización</button>
                    </div>
                    <div class="col-6 p-1">
                        <button class="btn btn-info w-100 btn-lg" id="btImprimirLpp" type="button" data-tipo="lpp" onclick="imprimirFormularioUrgenciaUc(this)">
                            <i class="fas fa-procedures" aria-hidden="true"></i>
                            <br />
                            Imprimir LPP
                        </button>
                    </div>
                </div>
                <div class="row" id="divTablaExamenesUc" style="display: none;">
                    <div class="col-md-8">
                        <h5 id="titleTblSolicitudExamen">sdad</h5>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-secondary" onclick="volverBotoneraImpresionesUc()"><i class="fa fa-arrow-left"></i>Volver atras</button>
                    </div>
                    <div class="col-md-12">
                        <table id="tblSolicitudExamenes" class="w-100 table table-hover table-striped"></table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<wuc:Brazalete ID="ModalIngresoBrazalete" runat="server" />
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalImpresionesUrgencia.js") + "?v=" + GetVersion() %>"></script>
<script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Examenes.js") + "?v=" + GetVersion() %>"></script>

<!-- FIN Modal Imprimir -->

