﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalIngresoBrazalete.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalIngresoBrazalete" %>
<%@ Register Src="~/Vista/UserControls/ImpresoraZebra.ascx" TagName="ImpresoraZebra" TagPrefix="wuc" %>

<script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
<script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>

<style>
    .brazalete {
        display: inline-block;
        background-color: white;
    }

    .brazalete-left {
        width: 110px;
        height: 80px;
        margin-right: -1px;
        border-left: 1px solid black;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        z-index: 1;
    }

    .brazalete-right.brazalete-right-pediatrico {
        width: 531px;
    }

    .brazalete-right.brazalete-right-adulto {
        width: 901px;
    }

    .brazalete-right {
        height: 96px;
        border: 1px solid black;
    }

    .brazalete-div-text {
        position: absolute;
        width: 100%;
        height: 100%;
        margin-right: 300px !important;
        z-index: 2;
    }

    .brazalete-text {
        font-family: system-ui;
        transform: scaleY(1.2);
        font-weight: bold;
    }

        .brazalete-text.brazalete-text-pediatrico {
            font-size: 13.7px;
            padding-right: 40px;
        }

        .brazalete-text.brazalete-text-pediatrico-subtitulo {
            font-size: 11.3px;
            text-align: center;
            white-space: pre;
            padding-right: 40px;
        }

        .brazalete-text.brazalete-text-adulto {
            font-size: 15.2px;
            padding-right: 300px;
        }

        .brazalete-text.brazalete-text-adulto-subtitulo {
            font-size: 13px;
            text-align: center;
            white-space: pre;
            padding-right: 300px;
        }
</style>

<div class="modal fade" id="mdlBrazalete" role="dialog" aria-labelledby="mdlViolencia" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-100 mt-0">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h4 class="m-0">
                    <strong><i class="fa fa-ticket-alt"></i>Generar Brazalete</strong>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="divPacienteBrazalete" class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>N° DAU</label>
                        <input id="txtNumeroDAUBrazalete" class="form-control" disabled />
                    </div>
                    <div class="col-md-2">
                        <label>Fecha Admisión</label>
                        <input id="txtFechaAdmision" class="form-control" disabled />
                    </div>
                    <div class="col-md-2">
                        <label>Tipo Atención</label>
                        <input id="txtTipoAtencion" class="form-control" disabled />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-2">
                        <label>Identificación</label>
                        <input id="txtIdentificacionBrazalete" class="form-control" disabled />
                    </div>
                    <div class="col-md-3">
                        <label>N° Documento</label>
                        <input id="txtNumeroDocBrazalete" class="form-control" disabled />
                    </div>
                    <div class="col-md-3">
                        <label>Nombre</label>
                        <input id="txtNombreBrazalete" class="form-control" maxlength="50" data-required="true" />
                    </div>
                    <div class="col-md-2">
                        <label>1° Apellido</label>
                        <input id="txtPrimerApellidoBrazalete" class="form-control" maxlength="50" data-required="true" />
                    </div>
                    <div class="col-md-2">
                        <label>2° Apellido</label>
                        <input id="txtSegundoApellidoBrazalete" class="form-control" maxlength="50" />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-2">
                        <label>Fecha Nacimiento</label>
                        <input id="txtFechaNacimientoBrazalete" class="form-control" type="date" data-required="true" />
                    </div>
                    <div class="col-md-2">
                        <label>Edad</label>
                        <input id="txtEdadBrazalete" class="form-control" disabled />
                    </div>
                    <div class="col-md-2">
                        <label>Sexo</label>
                        <select id="sltSexoBrazalete" class="form-control" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Nombre Social</label>
                        <input id="txtNombreSocialBrazalete" type="text" class="form-control" maxlength="50" />
                    </div>
                </div>

                <div class="mt-4">
                    <div class="d-flex justify-content-center">
                        <div class="text-center">
                            <label>Seleccione Tipo Brazalete</label><br />
                            <input id="rdoTipoBrazaletePediatrico" type="radio" name="TipoBrazalete" class="bootstrapSwitch" data-on-text="Pediátrico"
                                data-off-text="Pediátrico" data-on-color="success" data-size="large" data-tipo-brazalete="Pediatrico" />
                            <input id="rdoTipoBrazaleteAdulto" type="radio" name="TipoBrazalete" class="bootstrapSwitch" data-on-text="Adulto"
                                data-off-text="Adulto" data-on-color="success" data-size="large" data-tipo-brazalete="Adulto" />
                        </div>
                    </div>
                    <div id="divVistaPreviaBrazalete" class="pt-4 pb-3"></div>
                    
                    <div id="divImprimir">
                        <wuc:ImpresoraZebra runat="server" />
                        <div class="d-flex justify-content-center">
                            <button id="btnImprimirBrazalete" class="btn btn-info">
                                <i class="fa fa-print"></i> Imprimir Brazalete
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenerarBrazalete.js") + "?v=" + GetVersion() %>"></script>
