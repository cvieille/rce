﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Paciente.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.Paciente" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>


<div id="accordionUserControlPaciente">
    <div id="divDatosPaciente">
            <div class="card text-white">
                <div id="divCollapseUserControlPaciente" class="card-header bg-dark" data-toggle="collapse" data-target="#collapseUserControlPaciente" aria-expanded="true" aria-controls="collapseUserControlPaciente">
                    <div class="row">
                        <div class="col-md-3">
                            <span id="test"></span>
                            <h5 class="mb-0"><strong><i class="fas fa-wheelchair fa-lg pr-1"></i></strong>Datos del paciente</h5>
                        </div>
                        <div class="col-md-6">
                            <span id="txtNombrePacienteCollapsive"></span>
                        </div>
                        <div class="col-md-3 text-right">
                            <a id="lnbEvento" class="btn btn-primary" onclick="LnbEvento_Click()">
                                <i class="fa fa-eye"></i>Cambiar Evento
                            </a>
                            <span id="collapsiveInfoPaciente"><i class="fa-lg fa"></i></span>
                        </div>
                    </div>
                </div>
                <div id="collapseUserControlPaciente" class="collapse show" aria-labelledby="headingOne" >
                    <!--Inicio card body informacion paciente -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <label>Identificación</label>
                                <select id="sltIdentificacion" class="form-control">
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 resize-pac">
                                <label id="lblIdentificacion">RUT</label>
                                <div class="input-group">
                                    <input id="txtnumerotPac" type="text" class="form-control" style="width: 80px;" maxlength="8">
                                    <div class="input-group-prepend digito">
                                        <div class="input-group-text">
                                            <strong>-</strong>
                                        </div>
                                    </div>
                                    <input type="text" id="txtDigitoPac" class="form-control digito" style="width: 5px"; maxlength="1" placeholder="DV">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                <label for="txtNuiPac">N° Ubicación Interna</label>
                                <input
                                    id="txtNuiPac"
                                    type="text"
                                    class="form-control"
                                    style="width: 80px;"
                                    maxlength="6"
                                    onkeypress="if(this.value.length==6) return false;"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                            </div>
                        </div>
                        <div id="divInfoPaciente">
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Nombre/es</label>
                                    <input id="txtnombrePac" type="text" class="form-control" maxlength="50" data-required="true" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Primer Apellido</label>
                                    <input id="txtApePat" class="form-control" type="text" maxlength="50" data-required="true" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Segundo Apellido</label>
                                    <input id="txtApeMat" type="text" class="form-control" maxlength="50" />
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label for="sltNacionalidad">Nacionalidad</label>
                                    <select id="sltNacionalidad" class="form-control datos-pac" data-required="true">
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label>Sexo Biológico</label>
                                    <select id="sltsexoPac" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label>Identidad de Género</label>
                                    <select id="sltgeneroPac" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <label>Nombre social (Paciente responde a)</label>
                                    <input id="txtNombreSocial" class="form-control" type="text" maxlength="50" />
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label>Fecha de nacimiento</label>
                                    <b class="color-error"></b>
                                    <input id="txtFecNacPac" class="form-control" type="date" data-required="true" onblur="TxtFecNacPac_CalculaEdad()"
                                        onkeydown="event.keyCode == 13 && TxtFecNacPac_CalculaEdad()" max="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" min="<%= ("1900-01-01") %>" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                    <label>Edad</label>
                                    <input id="txtEdadPac" type="text" class="form-control" disabled="disabled" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                                    <label class="form">Dirección</label>
                                    <input id="txtDireccion" class="form-control" type="text" maxlength="100" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                    <label class="form">Número</label>
                                    <input id="txtNumDireccionPaciente" class="form-control" type="text" maxlength="50" />
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Teléfono</label>
                                    <input id="txtTelefono" type="text" class="form-control number" onkeydown="ValidarNumeros()" placeholder="Ej:9 11335869" maxlength="50" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Otro teléfono</label>
                                    <input id="txtOtroTelefono" placeholder="Ej:9 11335869" onkeydown="ValidarNumeros()" type="text" class="form-control number" maxlength="50" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                    <label class="form">Correo electrónico</label>
                                    <input id="txtCorreoElectronico" placeholder="Ej: correo@dominio.com" type="email" class="form-control" maxlength="50" />
                                    <input type="text" id="txtIdPaciente" class="d-none" name="name" value="" />
                                </div>
                            </div>
                            <div id="divPrevisionPaciente" class="mt-2">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                        <label>Previsión</label>
                                        <%--<input type="text" class="form-control" name="name" id="NombrePrevision" disabled />--%>
                                        <select id="sltPrevision" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                        <label>Tramo</label>
                                        <select id="sltPrevisionTramo" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                        <input type="button" id="btnCargarPrevisionFonasaPaciente" 
                                            class="btn btn-info btnguardar" 
                                            value="Actualizar desde FONASA" 
                                            onclick="cargarPrevisionApiFonasa(this)" 
                                            style="margin-top: 30px;"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div id="divPacienteAcompañante" class="row mt-2">
                                <div class="col-md-2">
                                    <div class="form-check pl-0">
                                        <label class="mr-2">Acompañante</label>
                                        <input id="chkAcompañante" type="checkbox" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                            data-on-color="primary" data-off-color="primary" data-size="normal" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--fin card body informacion paciente-->
                </div>
            </div>
            
            <!--Inicio modal rut dupluicacdo-->
            <div class="modal fade" id="mdlRutMaterno" role="dialog" data-backdrop="static" data-keyboard="false"
                aria-labelledby="mdlRutMaterno" aria-hidden="true" tabindex="-1">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-info">
                            <p class="heading lead">Rut materno</p>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info text-center" role="alert" style="margin-top: 10px;">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;Se han
                            encontrado pacientes con el rut indicado <b>por favor verifique que el paciente no esté
                                duplicado</b>.
                            </div>
                            <table id="tblIngresoExistente" class="table table-bordered table-hover w-100">
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button id="btnNuevoPacienteRN" class="btn btn-secondary">Es un paciente nuevo</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--fin modal rut duplicado-->

        <!--Inicio datos acompanante-->
 <%--       <div class="card" id="divAcompañante">
            <div class="card-header bg-dark text-white" data-toggle="collapse" data-target="#collapseUserControlAcompanante" aria-expanded="true" aria-controls="collapseUserControlAcompanante">
                <div class="row">
                    <div class="col-md-10">
                        <h5 class="mb-0"><strong><i class="fas fa-user-friends fa-lg pr-1"></i></strong>Acompañante</h5>
                    </div>
                    <div class="col-md-2 text-right">
                        <span id="collapsiveInfoAcompanante"> <i class=" fa-lg fa"></i></span>
                    </div>
                </div>
            </div>
            <div id="collapseUserControlAcompanante" class="collapse show" aria-labelledby="headingOne" >
                <div class="card-body">
                    <div class="mt-3">
                        <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />
                    </div>
                </div>
            </div>
        </div>--%>
        <!--fin datos acompanante-->
    </div>
    <div class="card" id="cardPacienteDuplicado" style="display:none;">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info text-center" role="alert" style="margin-top: 10px;">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;Se han
                            encontrado pacientes con el rut indicado <b>por favor verifique que el paciente no esté
                                duplicado</b>.
                            </div>
                   </div>
            </div>
        </div>
        <div class="card-body">
            <table id="tblListadoPacientes" class="table table-bordered table-hover w-100"></table>
        </div>
    </div>
    
</div>


<script src="<%= ResolveClientUrl("~/Script/UserControls/Paciente.js") + "?v=" + GetVersion() %>"></script>
<script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
<script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/ComboPacientes.js") + "?v=" + GetVersion() %>"></script>
<script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>