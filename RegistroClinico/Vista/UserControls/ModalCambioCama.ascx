﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalCambioCama.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalCambioCama" %>
<div id="mdlMovimientoHospitalizacion" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title">
                    <i class="fas fa-bed"></i>
                    Cambiar de cama</h5>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true" class="white-text">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="txtMdlTrasladoCama">Nombre del paciente:</label>
                        <input type="text" class="form-control" id="txtMdlTrasladoCama" disabled="disabled" />
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <label for="txtUbicacionActual">Ubicación actual:</label>
                        <input type="text" class="form-control" id="txtUbicacionActual" disabled="disabled" />
                    </div>
                    <div class="col-md-6">
                        <label for="txtCamaActualMov">Cama actual:</label>
                        <input type="text" class="form-control" id="txtCamaActualMov" disabled="disabled" />
                    </div>
                </div>
                <div id="divFormMovimiento" class="row mt-4">
                    <div class="col-md-6">
                        <label for="sltCamaServicio">Ubicacion destino:</label>
                        <select id="sltServicio" class="form-control" disabled="disabled" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Cama Destino</label>
                        <div class="input-group mb-3">
                            <select class="custom-select" id="sltCamaServicio" aria-label="Example select with button addon" data-required="true">
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-outline-success" type="button" onclick="ActualizarCamasDisponibles()">Actualizar Camas Disponibles</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="inputIdCamaActual" value="" />
                    <input type="hidden" id="inputIdHospActual" value="" />
                </div>
                <div class="text-right mt-4">
                    <a id="btnRealizarMovimiento" class="btn btn-success" onclick="realizarMovimiento()">
                        <i class="fas fa-share"></i> Realizar movimiento
                    </a>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <table id="tblTrasladosUrgencia" class="table table-striped table-bordered table-hover w-100">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalCambioCama.js") + "?v=" + GetVersion()  %>"></script>
