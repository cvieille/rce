﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalAislamiento.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalAislamiento" %>


<div id="mdlAislamiento" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                
                <p class="heading lead" id="textInfoModal"></p>
                <div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div>
                    <h5><b>Hospitalización</b> <span id="lblIdHospitalizacion"></span></h5>
                    <h5><b>Nombre paciente</b> <span id="lblNombrePaciente"></span></h5>
                    <h5><b>Ubicación</b> <span id="lblUbicacion"></span></h5>
                    <h5><b>Cama actual</b> <span id="lblCama"></span></h5>
                </div>
                <div class="card" style="padding: 20px;">
                    <%--Inicio de los tabs--%>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a
                                class="nav-link active"
                                id="aislamientos-tab"
                                data-toggle="tab"
                                data-target="#aislamientos-tab-pane"
                                aria-controls="aislamientos-tab-pane"
                                aria-selected="false"
                                style="cursor: pointer;">Aislamientos
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a
                                class="nav-link"
                                id="vigilancias-tab"
                                data-toggle="tab"
                                data-target="#vigilancias-tab-pane"
                                role="tab"
                                aria-controls="vigilancias-tab-pane"
                                aria-selected="false"
                                style="cursor: pointer;">Vigilancias IAAS
                            </a>
                        </li>
                    </ul>
                    <%--Fin de los tabs--%>
                    <%-- Inicio Contenido de los tabs--%>
                    <div class="tab-content" id="myTabContent">
                        <%--Tab 1 aislamientos--%>
                        <div class="tab-pane fade show active" id="aislamientos-tab-pane" role="tabpanel" aria-labelledby="aislamientos-tab">
                            <div id="divNuevoAislamiento" style="display: none;">
                                <h5 class="mt-3 text-center" id="infoAislamiento">Nuevo aislamiento</h5>
                                <div class="row mt-3" id="divAislamientos">
                                    <div class="col-md-3">
                                        <label class="active">Motivo aislamiento</label>
                                        <select id="ddlMotivoAislamiento" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="active">Tipo aislamiento</label>
                                        <select id="ddlTipoAislamiento" class="selectpicker" data-width="100%" data-actions-box="true" data-required="true" multiple title="Seleccione">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="hidden" name="name" id="txtIdAislamiento" value="" />

                                        <label class="active">Inicio aislamiento</label>
                                        <input id="txtFechaInicioAisl" type="date" class="form-control" data-required="true" />
                                    </div>

                                    <div class="col-md-2">
                                        <label class="active">Fin aislamiento</label>
                                        <input id="txtFechaFinAisl" type="date" class="form-control" data-required="true" />
                                    </div>
                                    <div class="col-md-2">
                                        <div class="divButtonLabel" id="divAgregarAislamiento">
                                            <button id="btnNuevoAislamiento" type="button" data-method="CREAR" onclick="AgregarEditarAislamiento(this)" class="btn btn-success"><i class="fa fa-save"></i></button>
                                        </div>
                                        <div class="divButtonLabel d-none" id="divEdicionAislamiento">
                                            <button id="btnEditarAislamiento" type="button" data-method="EDITAR" onclick="AgregarEditarAislamiento(this)" class="btn btn-warning"><i class="fa fa-edit" aria-hidden="true"></i></button>

                                            <button id="btnCancelarAislamiento" onclick="cancelarEdicion()" type="button" class="btn btn-danger"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12">
                                        Aislamientos:
                                        <div class="table-responsive-xl">
                                            <table id="tblAislamientoPaciente" class="table table-striped table-bordered table-hover col-sm col-xs col-md">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--Tab2 vigilancia IAAS--%>
                        <div class="tab-pane fade" id="vigilancias-tab-pane" role="tabpanel" aria-labelledby="vigilancias-tab">
                            <table id="tblInvasivosPaciente" class="table table-bordered table-hover w-100">
                            </table>
                        </div>
                        <%--Fin vigilancia IAAS--%>
                    </div>
                    <%--Fin contenido de los tabs--%>
                    <div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalAislamiento.js") + "?v=" + GetVersion() %>"></script>
