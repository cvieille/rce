﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Medicamentos.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.Medicamentos" %>
<div class="row" id="divMedicamentosUc">
    <div class="col-md-12 col-sm-12">
        <label>Medicamentos <b class="color-error">(*)</b></label>
        <div class="typeahead__container">
            <div class="typeahead__field">
                <div class="typeahead__query">
                    <input id="thMedicamentosUc" name="hockey_v1[query]" type="search" placeholder="Agregar/Buscar Medicamentos"
                                                autocomplete="off" onclick="this.select();" data-required="true">
                </div>
           </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <label>Dosís</label>
        <input id="txtDosisMedicamento" class="form-control" placeholder="Especifique dosís"
                                    type="text" data-required="true" />
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <label>Frecuencia</label>
        <input id="txtFrecuenciaMedicamento" class="form-control" placeholder="Especifique dosís"
                                    type="text" data-required="true" />
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <label>Via administracion</label>
        <select id="sltViaAdministracion" class="form-control" data-required="true">
        </select>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <button type="button" id="btnAgregarMedicamentoUc" class="btn btn-primary divButtonLabel"><i class="fa fa-plus"></i>Agregar</button>
    </div>

    <div class="col-md-12 col-xs-12 mt-2">
        <table id="tblMedicamentosUserControl" class="table table-bordered w-100"></table>
    </div>

     <script src="<%= ResolveClientUrl("~/Script/UserControls/Medicamentos.js") + "?v=" + GetVersion() %>"></script>
</div>