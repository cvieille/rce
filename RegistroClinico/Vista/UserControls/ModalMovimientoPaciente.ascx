﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalMovimientoPaciente.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalIngresosEnfermeria" %>

<div id="mdlPasaraBox" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nombre de paciente:
                        <label id="lblPasaraBox"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">

                    <h5 class="mb-4">
                        <strong>
                            <i class="fa fa-location-arrow"></i> Enviar a box
                        </strong>
                    </h5>

                    <div id="divBox" class="row">
                        <div class="col-md-4">
                            <label>Tipo atención</label>
                            <select id="sltServicioBox" class="form-control" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Tipo de box</label>
                            <select id="sltTipoBox" class="form-control" data-required="true">
                            </select>
                        </div>
                        <input type="hidden" id="inpIdUbicacion" value="" />
                        <div class="col-md-4">
                            <label>Nombre box</label>
                            <select id="sltNombreBox" class="form-control" data-required="true">
                            </select>
                        </div>
                    </div>
                    <div class="text-right mt-2">
                        <a id="aEnviarABox" class="btn btn-success">
                            <i class="fas fa-share"></i>Enviar a box
                        </a>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <table id="tblTrasladosUrgencia" class="table table-striped table-bordered table-hover">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalMovimientoPaciente.js") + "?v=" + GetVersion() %>"></script>