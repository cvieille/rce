﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicamentosHospitalizacion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.Medicamentos" %>
<div class="row" id="divMedicamentosUc">

    <div class="col-md-2 col-sm-2">
        <label>Tipo de medicamento</label>
        <select class="form-control" id="sltTipoMedicamento" onchange="cambioTipoMedicamento(this)">
            <option value="arsenal">Dentro del arsenal</option>
            <option value="no-arsenal">Fuera del arsenal</option>
        </select>
    </div>
    <div class="col-md-10 col-sm-10" id="divMedicamentoArsenal">
        <label>Medicamentos dentro del arsenal <b class="color-error">(*)</b></label>
        <div class="typeahead__container">
            <div class="typeahead__field">
                <div class="typeahead__query">
                    <input id="thMedicamentosUc" name="hockey_v1[query]" type="search" placeholder="Agregar/Buscar Medicamentos"
                                                autocomplete="off" onclick="this.select();" data-required="true">
                </div>
           </div>
        </div>
    </div>
    <div class="col-md-10 col-sm-10 d-none" id="divMedicamentoFueraArsenal">
        <label>Medicamentos fuera del arsenal <b class="color-error">(*)</b></label>
        <input type="text" class="form-control" placeholder="Ingrese el nombre y presentación del medicamento" id="txtMedicamentoFueraArsenal" />
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <label>Dosís</label>
        <input id="txtDosisMedicamento" class="form-control" placeholder="Especifique dosís"
                                    type="text" data-required="true" />
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <label>Frecuencia</label>
        <input id="txtFrecuenciaMedicamento" class="form-control" placeholder="Especifique frecuencia"
                                    type="text" data-required="true" />
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <label>Duración del tratamiento</label>
        <input id="txtDuracionMedicamento" class="form-control" placeholder="Especifique duración"
                                    type="text" data-required="true" />
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <label>Via administracion</label>
        <select id="sltViaAdministracion" class="form-control" data-required="true">
        </select>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <button type="button" id="btnAgregarMedicamentoUc" class="btn btn-primary divButtonLabel"><i class="fa fa-plus"></i>Agregar</button>
    </div>

    <div class="col-md-12 col-xs-12 mt-2">
        <table id="tblMedicamentosUserControl" class="table table-bordered w-100"></table>
    </div>

     <script src="<%= ResolveClientUrl("~/Script/UserControls/MedicamentosHospitalizacion.js") + "?v=" + GetVersion() %>"></script>
</div>