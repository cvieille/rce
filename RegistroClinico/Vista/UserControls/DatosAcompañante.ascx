﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatosAcompañante.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DatosAcompañante" %>

<!--Inicio datos acompanante-->
<div class="card" id="divAcompañante">
    <div class="card-header bg-dark text-white" data-toggle="collapse" data-target="#collapseUserControlAcompanante" aria-expanded="true" aria-controls="collapseUserControlAcompanante">
        <div class="row">
            <div class="col-md-10">
                <h5 class="mb-0"><strong><i class="fas fa-user-friends fa-lg pr-1"></i></strong>Acompañante</h5>
            </div>
            <div class="col-md-2 text-right">
                <span id="collapsiveInfoAcompanante"><i class=" fa-lg fa"></i></span>
            </div>
        </div>
    </div>
    <div id="collapseUserControlAcompanante" class="collapse show" aria-labelledby="headingOne">
        <div class="card-body">
            <div class="mt-3">
                <div class="row">
                    <div class="col-md-2">
                        <label>Identificación</label>
                        <select id="sltIdentificacionAcompañante" class="form-control identificacion" data-tipo="acompanante" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label id="lblIdentificacionAcompañante" for="sltIdentificacionAcompañante"></label>
                        <div class="input-group">
                            <input id="txtnumeroDocAcompañante" type="text" class="form-control numero-documento" style="width: 40px;"
                                data-required="false" maxlength="8" />
                            <div class="input-group-prepend digitoAcompanante">
                                <div class="input-group-text">
                                    <strong>-</strong>
                                </div>
                            </div>
                            <input id="txtDigAcompañante" type="text" class="form-control digitoAcompanante text-center col-md-3" maxlength="1" placeholder="DV" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <input type="text" class="d-none" id="txtIdAcom" name="name" value="" />
                    <div class="col-md-3">
                        <label for="txtNombreAcompañante">Nombre</label>
                        <input id="txtNombreAcompañante" type="text" class="form-control datos-persona" maxlength="50" data-required="false"/>
                    </div>
                    <div class="col-md-3">
                        <label for="txtApePatAcompañante">Primer Apellido</label>
                        <input id="txtApePatAcompañante" type="text" class="form-control datos-persona" maxlength="50" data-required="true"/>
                    </div>
                    <div class="col-md-3">
                        <label for="txtApeMatAcompañante">Segundo Apellido</label>
                        <input id="txtApeMatAcompañante" type="text" class="form-control datos-persona" maxlength="50"/>
                    </div>
                    <div class="col-md-3">
                        <label for="txtTelAcompañante">Teléfono (Opcional)</label>
                        <input id="txtTelAcompañante" type="text" class="form-control datos-persona" maxlength="12"/>
                    </div>
                    <div class="col-md-3">
                        <label for="lblTipoGeneroAcompaniante">Genero</label>
                        <select id="sltTipoGeneroAcompaniante" class="form-control datos-persona" data-required="false"></select>
                    </div>
                    <div class="col-md-3">
                        <label for="lblSexoAcompañante">Sexo</label>
                        <select id="sltSexoAcompañante" class="form-control datos-persona" data-required="false"></select>
                    </div>
                    <div class="col-md-3">
                        <label for="lblTipoRepresentante">Tipo Representante</label>
                        <select id="sltTipoRepresentante" class="form-control" data-required="false"></select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin datos acompanante-->

<script src="<%= ResolveClientUrl("~/Script/UserControls/DatosAcompañante.js") + "?" + GetVersion() %>"></script>
