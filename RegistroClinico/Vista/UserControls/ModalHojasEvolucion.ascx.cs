﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.UserControls
{
    public partial class ModalHojasEvolucion : Handlers.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnbExportarBandejaH_Click(object sender, EventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("Aqui");

            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                //Data viene de arreglo "data" de datatable de Bandeja
                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);
                System.Diagnostics.Debug.WriteLine(des);
                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["Fecha H.E"] = Convert.ToString(des[i][0]),
                        ["Diagnostico"] = Convert.ToString(des[i][1]),
                        ["Profesional"] = Convert.ToString(des[i][2]),
                    };
                    dic.Add(d);
                }
                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Bandeja Hoja de Evolución " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Bandeja Hoja de Evolución " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), workbook);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
}