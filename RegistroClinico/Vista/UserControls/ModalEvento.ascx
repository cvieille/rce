﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalEvento.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalEvento" %>

<div class="modal fade left" id="mdlEventoNuevo" role="dialog" aria-labelledby="mdlEventoNuevo" 
        aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-full-height modal-left modal-lg" role="document" style="max-width:700px;">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <p class="heading lead">Nuevo Evento</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group list-group-flush" id="divEvento">
                    <li class="list-group-item">
                        <div class="text-center">
                            <i class="fa fa-clipboard-list fa-3x mb-3 animated rotateIn"></i>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="md-form">
                                    <i class="fa fa-user prefix grey-text"></i>
                                    <input type="text" id="txtEventoPacNom" placeholder="&nbsp;" class="form-control disabled" readonly />
                                    <label for="txtEventoPacNom" class="active">Paciente</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form">
                                    <input type="text" id="txtEventoPacRut" placeholder="&nbsp;" class="form-control disabled" readonly />
                                    <label for="txtEventoPacRut" class="active">Rut</label>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <select id="sltEventoAmbito" class="selectpicker" data-required="true" 
                                        data-style="btn-primary-dark waves-effect">
                                    </select>
                                    <label for="sltEventoAmbito" class="active">Tipo Evento</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form">
                                    <textarea id="txtEventoCausa" data-required="true" maxlength="200" class="md-textarea form-control" rows="2"></textarea>
                                    <label for="txtEventoCausa">Causa</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <textarea id="txtEventoAnamnesis" data-required="true" maxlength="200" class="md-textarea form-control" rows="2"></textarea>
                                    <label for="txtEventoAnamnesis">Anamnesis</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form">
                                    <textarea id="txtEventoDiagnostico" data-required="true" maxlength="200" class="md-textarea form-control" rows="2"></textarea>
                                    <label for="txtEventoDiagnostico">Diagnóstico</label>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="md-form">
                                    <input type="date" id="txtEventoFechaAlta" required class="form-control"/>
                                    <label class="active" for="txtEventoFechaAlta">Fecha Alta Paciente</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form">
                                    <input type="date" id="txtEventoInicioFecha" required class="form-control"/>
                                    <label class="active" for="txtEventoInicioFecha">
                                        Fecha Inicio Evento
                                    </label>
                                    <span style="color:crimson; display:none;" id="spanFechaInicio">
                                        <i class="fa fa-times"></i> La fecha de Inicio no puede ser mayor a la fecha de Fin
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form">
                                    <input type="date" id="txtEventoFinFecha" required class="form-control"/>
                                    <label class="active" for="txtEventoFinFecha">Fecha Fin Evento</label>
                                    <span style="color:crimson; display:none;" id="spanFechaFin">
                                        <i class="fa fa-times"></i>La fecha de Fin no puede ser menor a la fecha de Inicio
                                    </span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="modal-footer justify-content-center">
                <button id="btnNuevoEvento" class="btn btn-secondary" onclick="return false;">Nuevo Evento</button>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalEvento.js") + "?v=" + GetVersion() %>"></script>