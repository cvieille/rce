﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalPacienteEnTransito.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalPacienteEnTransito" %>

<div id="mdlPacienteEnTransito" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title"><i class="fa fa-child" aria-hidden="true"></i> Traslado de paciente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="divDatosPacienteEntransito">
                <div class="row">
                    <div class="col-md-7">
                        <label>Nombre:</label><input class="form-control" id="txtNombrePacienteEntransito" disabled />
                    </div>
                    <div class="col-md-3">
                        <label>Id hospitalizacion</label>
                        <input class="form-control" id="txtHospitalizacionPacienteEntransito" disabled />
                    </div>
                    <div class="col-md-2">
                        <label>Cama:</label><input class="form-control" id="txtNombreCamaPacienteEntransito" disabled />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-4">
                        <label for="sltTipoTransito">Tipo Tránsito:</label>
                        <select id="sltTipoTransito" class="form-control" data-required="true">
                            <option value="0">-Seleccione-</option>
                            <option value="1">Intrahospitalario</option>
                            <option value="2">Pabellón</option>
                        </select>
                    </div>
                    <div class="col-md-4" id="divUbicacionTransito">
                        <label for="sltUbicacionPacienteTransito">Ubicacion:</label>
                        <select id="sltUbicacionPacienteTransito" class="form-control" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-4" id="divEstablecimientoTransito">
                        <label for="sltEstablecimientoPacienteTransito">Establecimiento:</label>
                        <select id="sltEstablecimientoPacienteTransito" class="form-control" data-required="true">
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md">
                        <label for="txtObservacionPacienteEnTransito">Observaciones</label>
                        <textarea class="form-control" id="txtObservacionPacienteEnTransito" maxlength="500" style="resize: none;"></textarea>
                    </div>
                </div>
                <div class="row mt-2 text-right">
                    <div class="col-md">
                        <a id="btnGuardarTransitoPaciente" class="btn btn-success" data-cama="" data-id="" onclick="guardarPacienteEnTransito(this)">
                            <i class="fas fa-share"></i>
                            Aceptar
                        </a>
                        <a id="btnCancelarTransitoPaciente" class="btn btn-warning" data-dismiss="modal">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalPacienteEnTransito.js") + "?v=" + GetVersion()  %>"></script>
