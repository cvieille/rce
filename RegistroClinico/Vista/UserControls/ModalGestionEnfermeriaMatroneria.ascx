﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalGestionEnfermeriaMatroneria.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalGenericoMovimientosSistema" %>
<div class="row" id="divGestionPacienteHospitalizado">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1">
        <button class="btn btn-warning btn-lg w-100" id="btnAsignarCama" type="button" data-tipomodal="ingresoenfermeria" data-idtipoestado="88" onclick="linkIrUserControlModal(this)">
            <i class="fas fa-bed"></i>
            <br>
            &nbsp;Ingresar traslado</button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalTraslados" data-tipomodal="cambiocama" type="button" onclick="linkIrUserControlModal(this)">
            <i class="fas fa-bed"></i>
            <br>
            Cambiar de cama</button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalPacienteEnTransito" data-tipomodal="pacientetransito" type="button" onclick="linkIrUserControlModal(this)">
            <i class="fas fa-user-nurse"></i>
            <br>
            Traslado de paciente
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalCategorizar" type="button" data-tipomodal="categorizacion" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-address-card" aria-hidden="true"></i>
            <br>
            Categorizar</button>
    </div>

    <%-- BOTON DE ATENCION CLINICA HOS  --%>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-success btn-lg w-100" id="btnIrAtencionClinicaHos" type="button" onclick="irAtencionClinicaHos(this)">
            <i class="fa fa-file" aria-hidden="true"></i>
            <br />
            Atención Clinica</button>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado " id="divVerModalCategorizarRN" style="display: none;">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalCategorizarRN" type="button" data-rn="true" data-tipomodal="categorizacion" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-baby"></i>
            <br />
            Categorizar RN</button>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalEscalasClinicas" type="button" data-tipomodal="escalasclinicas" onclick="linkIrUserControlModal(this)">
            <i class="fas fa-thermometer-quarter"></i>
            <br>
            Escalas clínicas</button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalAislamientos" type="button" data-tipomodal="aislamiento" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-eye-slash" aria-hidden="true"></i>
            <br>
            Aislamiento</button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalConsentimiento" type="button" data-tipomodal="consentimiento" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-universal-access" aria-hidden="true"></i>
            &nbsp;Consentimiento docente
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalBrazalete" type="button" data-tipomodal="brazalete" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-barcode" aria-hidden="true"></i>
            &nbsp;Brazalete de hospitalización
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalAlergias" type="button" data-tipomodal="alergias" onclick="linkIrUserControlModal(this)">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            &nbsp;Alergias del paciente
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerFichaNeoNato" data-tipomodal="neonato" type="button" onclick="linkIrUserControlModal(this)" disabled="disabled">
            <i class="fa fa-child" aria-hidden="true"></i>
            &nbsp;Ficha clínica neonatal
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-info btn-lg w-100" id="btnVerModalHojaEnfermeria" type="button" onclick="linkMostrarModalHojaEnfermeria(this)" disabled="disabled">
            <i class="fas fa-user-nurse"></i>
            &nbsp;Hoja enfermería/matronería
        </button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 mb-1 ingresado">
        <button class="btn btn-warning btn-lg w-100" id="btnVerModalEgreso" type="button" onclick="linkMostrarModalEgreso(this)">
            <i class="fa fa-arrow-right" aria-hidden="true"></i>
            <br>
            Dar alta enfermería
        </button>
    </div>

</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGestionEnfermeriaMatroneria.js") + "?v=" + GetVersion()  %>"></script>
