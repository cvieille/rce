﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalAltaUrgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalAltaUrgencia" %>
<%@ Register Src="~/Vista/UserControls/DiagnosticoCIE10Urgencia.ascx" TagName="DiagnosticoUrgencia" TagPrefix="wuc" %>

<div id="mdlEgresarAtencion" class="modal fade">
    <div class="modal-dialog modal-100 mt-0">
        <div class="card modal-content">
            <div class="modal-header modal-header-info center-horizontal">
                <h4 class="heading form-inline">
                    <b>Alta / Egreso de Paciente</b>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body m-2">
                        <div id="divAltaPadre" class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                <div class="card-header text-center bg-info">Alta del paciente</div>
                                <div id="divAltaEgreso" class="card p-3">
                                    <div class="card-body">
                                        <div class="row" id="divTipoEgreso">
                                            <div class="col-md-12 text-center">
                                                <p class="h5">Tipo de egreso</p>
                                                <input id="rdoTipoEgresoNormal" type="radio" name="TE" class="bootstrapSwitch" data-on-text="Clínico"
                                                    data-off-text="Clínico" data-on-color="success" data-size="large" />
                                                <input id="rdoTipoEgresoIrregular" type="radio" name="TE" class="bootstrapSwitch" data-on-text="Administrativo"
                                                    data-off-text="Administrativo" data-on-color="warning" data-size="large" />
                                            </div>
                                        </div>
                                        <div id="divCondicionPaciente" class="row mt-3">
                                            <div class="col-md-12 text-center">
                                                <p class="h5">Condición del paciente al final de la atención</p>
                                                <input id="rdoVivo" type="radio" name="CP" class="bootstrapSwitch" data-on-text="Vivo"
                                                    data-off-text="Vivo" data-on-color="success" data-size="large" />
                                                <input id="rdoFallecido" type="radio" name="CP" class="bootstrapSwitch" data-on-text="Fallecido"
                                                    data-off-text="Fallecido" data-on-color="warning" data-size="large" />
                                            </div>
                                        </div>
                                        <div id="divEgresoIrregular" class="mt-2">

                                            <p class="h4 text-center">Detalles del cierre de caso</p>

                                            <div class="row">
                                                <div class="col-md-6 col-lg-4">
                                                    <label>Motivo de cierre</label>
                                                    <select id="sltMotivoCierre" class="form-control" data-required="true">
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row mt-2">
                                                <div class="col-md-12">
                                                    <label>Observaciones</label>
                                                    <textarea id="txtMotivoCierrePaciente" class="form-control" rows="4"
                                                        placeholder="Especifique motivo de cierre del caso" maxlength="100">
                                                </textarea>
                                                </div>
                                            </div>
                                            <div class="row d-flex flex-row justify-content-end mt-2">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <%--<a id="btnEgresarAltaAdministrativa" class="btn btn-success w-100">
                                                        <i class="fa fa-plus mr-1"></i>Dar alta Administrativa
                                                    </a>--%>
                                                    <button id="btnEgresarAltaAdministrativa" class="btn btn-success w-100">
                                                        <i class="fa fa-plus mr-1"></i> Dar alta Administrativa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="alertEditandoAlta"></div>
                                        <div id="divEgresoNormal" class="mt-3">
                                            <div id="divEgresoMedico">
                                                <ul class="nav nav-tabs" id="tabAlta" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button id="datos-administartivos-tab"
                                                            class="nav-link active"
                                                            data-toggle="tab"
                                                            data-target="#divDatosAdministrativos"
                                                            type="button"
                                                            role="tab"
                                                            aria-controls="divDatosAdministrativos"
                                                            aria-selected="true">
                                                            Datos Generales</button>

                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button id="datos-diagnostico-tab"
                                                            class="nav-link"
                                                            data-toggle="tab"
                                                            data-target="#divDiagnosticosAlta"
                                                            type="button"
                                                            role="tab"
                                                            aria-controls="divDiagnosticosAlta"
                                                            aria-selected="false">
                                                            Diagnóstico</button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button id="datos-indicaciones-tab"
                                                            class="nav-link"
                                                            data-toggle="tab"
                                                            data-target="#divIndicacionesAlta"
                                                            type="button"
                                                            role="tab"
                                                            aria-controls="divIndicacionesAlta"
                                                            aria-selected="false">
                                                            Indicaciones</button>
                                                    </li>
                                                </ul>
                                                <div class="tab-content mt-4" id="tabEvolucionUrgContent">
                                                    <div id="divDatosAdministrativos" class="tab-pane fade show active" role="tabpanel" aria-labelledby="divDatosAdministrativos">

                                                        <div id="divMedicoEgreso" class="row">
                                                            <div class="col-md-12">
                                                                <label>Médico que egresa <b class="color-error">(*)</b></label>
                                                                <div class="typeahead__container">
                                                                    <div class="typeahead__field">
                                                                        <div class="typeahead__query">
                                                                            <input
                                                                                id="thMedicoEgresa"
                                                                                name="hockey_v1[query]"
                                                                                type="search"
                                                                                placeholder="Escribe el nombre del médico que egresa"
                                                                                autocomplete="off"
                                                                                onclick="this.select();"
                                                                                data-required="true" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="divFechaHoraAdmisionCat" class="mt-2">
                                                            <div class="row">
                                                                <div class='col-sm-4'>
                                                                    <label>Fecha Llegada</label>
                                                                    <input id="txtFechaLlegada" class='form-control' disabled />
                                                                </div>
                                                                <div class='col-sm-4'>
                                                                    <label>Fecha Categorización</label>
                                                                    <input id="txtFechaCategorizacion" class='form-control' disabled />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="divFechaHoraAtencion" class="row mt-2">
                                                            <div class="col-md-4">
                                                                <label>Fecha Atención Clínica</label>
                                                                <input id="txtFechaHoraAtencion" type="date" class="form-control" data-required="true" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Hora Atención Clínica</label>
                                                                <input id="txtHoraAtencion" type="time" class="form-control" data-required="true" />
                                                            </div>
                                                        </div>

                                                        <div id="divFechaHoraAlta" class="row mt-2">
                                                            <div class="col-md-4">
                                                                <label>Fecha Alta</label>
                                                                <input id="txtFechaAlta" type="date" class="form-control" data-required="true" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Hora Alta</label>
                                                                <input id="txtHoraAlta" type="time" class="form-control" data-required="true" />
                                                            </div>
                                                        </div>

                                                        <div class="row mt-2">
                                                            <input type="hidden" id="txtIdAltaUrgencia" />
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                                <label>Destino Alta</label>
                                                                <select id="sltDestinoAltaPaciente" class="form-control" data-required="true">
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8" id="divFallecimiento">
                                                                <label>Fallecimiento</label>
                                                                <select id="sltProcesoFallecimiento" class="form-control" data-required="false">
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                                                <label>Ubicación</label>
                                                                <select id="sltUbicacionDerivado" class="form-control" data-required="false">
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                                                <label>Destino</label>
                                                                <select id="sltDestinoDerivacion" class="form-control" data-required="false">
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                                                <label>Establecimiento</label>
                                                                <select id="sltEstablecimientoAltaPaciente" class="form-control" data-required="false">
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row mt-2">
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                                <label>Pronóstico Médico Legal</label>
                                                                <select id="sltPronosticoMedicoLegal" class="form-control">
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="text-center mt-4 card-footer">
                                                            <a id="aSiguienteDatosGenerales"
                                                                class="btn btn-light">Siguiente <i class="fa fa-angle-right"></i></a>
                                                        </div>

                                                    </div>
                                                    <div id="divDiagnosticosAlta" class="tab-pane fade" role="tabpanel" aria-labelledby="divDiagnosticosAlta">
                                                        <wuc:DiagnosticoUrgencia ID="wuc_diagnosticoAltaUrgencia" runat="server" />
                                                        <div class="row mt-4 card-footer">
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 
                                                            d-flex 
                                                            flex-row
                                                            justify-content-center
                                                            flex-wrap">
                                                                <a onclick="$('#datos-administartivos-tab').trigger('click')"
                                                                    class="btn btn-light m-1"><i class="fa fa-angle-left"></i>Atrás</a>
                                                                <a id="aSiguienteTabDiagnostico"
                                                                    class="btn btn-light m-1">Siguiente <i class="fa fa-angle-right"></i></a>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                                                                <button id="btnEgresarAltaDigitador" class="btn btn-success w-100">Dar alta Paciente</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divIndicacionesAlta" class="tab-pane fade" role="tabpanel" aria-labelledby="divIndicacionesAlta">
                                                        <div class="row mt-2">
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                                                <label>Indicaciones al alta:</label>
                                                                <textarea id="txaIndicacionesAlta" rows="8" class="form-control" placeholder="Indicaciones al alta"
                                                                    maxlength="8000" data-required="true">
                                                            </textarea>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                                                <label>Medicamentos:</label>
                                                                <textarea id="txaMedicamentosAlta" rows="8" class="form-control" placeholder="Medicamentos a indicar"
                                                                    maxlength="8000"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-2 card-footer d-flex flex-row justify-content-end flex-wrap">
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 text-center">
                                                                <a onclick="$('#datos-diagnostico-tab').trigger('click')"
                                                                    class="btn btn-light"><i class="fa fa-angle-left"></i>Atrás</a>
                                                            </div>
                                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                                <%--<a id="btnEgresarAlta" class="btn btn-success w-100"> Dar alta Paciente
                                                                </a>--%>
                                                                <button id="btnEgresarAlta" class="btn btn-success w-100">Dar alta Paciente</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <%--<div id="divEgresoEnfermeria" class="row p-2">
                                        <div class="col-sm-12">
                                            <label>Ingrese observaciones de alta</label>
                                            <textarea class="form-control" id="txtObservacionEnfermera" rows="4"></textarea>
                                        </div>
                                        <div class="col-sm-12 p-2 text-right">
                                            <button class="btn btn-success" id="btnAltaEnfermeria" type="button">
                                                <i class="fa fa-plus"></i>Dar alta Paciente
                                            </button>
                                        </div>
                                    </div>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                <div class="card border-secondary mb-3">
                                    <div class="card-header text-center bg-info">Datos de paciente</div>
                                    <div class="card-body">
                                        <div id="divMasInformacionUrgencia">
                                            <!-- Contenido de datos del paciente -->
                                        </div>
                                    </div>
                                </div>
                                
                        </div>
                    </div>
                </div>
            </div>
            <!-- Botón para cerrar modal -->
            <div class="modal-footer">
                <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                    <!-- Nombre del usuario logueado a la izquierda -->
                    <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                        <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-1">
                        <a id="aCerrarAlta" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalAltaUrgencia.js") + "?v=" + GetVersion() %>"></script>
