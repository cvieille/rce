﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalAlergias.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalAlergias" %>

<div id="accordionUserControlAlergia">
    <div class="card">
        <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoAlergia"
            aria-expanded="true" aria-controls="collapseInfoAlergia">
            <div class="row">
                <div class="col-md-8">
                    <h5 class="mb-0"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Alergias</h5>
                </div>
                <div class="col-md-4 text-right">
                    <i class="fa-lg fa"></i>
                </div>
            </div>
        </div>
        <div class="card-body collapse show" id="collapseInfoAlergia" aria-labelledby="headingOne"
            data-parent="#accordionUserControlAlergia">
            <div id="divAlergiasPaciente">
                <div id="divHideAlergiaPac">
                    <div id="divDatosHosp" class="row">
                        <div class="col-md-2">
                            <label for="txtNumHospAlergias">N° Hospitalizacion</label>
                            <input type="text" class="form-control" id="txtNumHospModalAlergias" disabled />
                        </div>
                        <div class="col-md-4">
                            <label for="txtNomPacModalAlergias">Paciente</label>
                            <input type="text" class="form-control" id="txtNomPacModalAlergias" disabled />
                        </div>
                        <div class="col-md-3">
                            <label for="txtNumCamaModalAlergias">Cama</label>
                            <input type="text" class="form-control" id="txtNumCamaModalAlergias" disabled />
                        </div>
                        <hr />
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="sltTipoAlergia" id="lblTipoAlergia" class="form-label mt-3">Alergias</label>
                            <select id="sltTipoAlergias" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <br />
                            <br />
                            <button type="button" class="btn btn-outline-info" onclick="agregarAlergia(this)"><i class="fa fa-save pr-1"></i>Agregar</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="divContainerTablaAlergia" class="col-md-12 mt-4">
                        <div class="table-responsive-xl">
                            <table id="tblTipoAlergias" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fin card body collapsive-->
    </div>
    <!--Fin card-->
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalAlergias.js") + "?v=" + GetVersion() %>"></script>
<!-- no subir sin GetInfo() -->
