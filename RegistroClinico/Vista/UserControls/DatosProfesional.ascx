﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatosProfesional.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DatosProfesional" %>
<div class="row mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-dark">
                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos profesional Médico</strong> </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Número documento</label>
                        <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-3">
                        <label>Nombre/es</label>
                        <input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-3">
                        <label>Primer Apellido</label>
                        <input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-3">
                        <label>Segundo Apellido</label>
                        <input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/DatosProfesional.js") + "?v=" + GetVersion() %>"></script>
</div>