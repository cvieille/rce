﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignosVitales.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.SignosVitales" %>
<div class="w-100 mt-2">
    <div id="divDetalleSignosVitales"></div>
    <div id="divSignosVitalesVacio" class="alert alert-warning text-center mt-3">
        <strong>
            <i class="fa fa-exclamation-triangle"></i> Paciente sin ingresos de Signos vitales.
        </strong>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/SignosVitales.js") + "?v=" + GetVersion() %>"></script>