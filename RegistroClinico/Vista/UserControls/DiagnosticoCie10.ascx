﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DiagnosticoCie10.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DiagnosticoCie10" %>
<div class="row">
    <div class="col-md-12">
        <%--Diagnostico CIE10--%>
        <div class="card">
            <div class="card-header bg-dark ">
                <h5 class="mb-0"><i class="fas fa-stethoscope fa-lg pr-1"></i>Diagnóstico Cie10</h5>
            </div>
            <div class=" card-body">
               <div class="row">
                     <div class="col-md-8">
                                <label>Diagnóstico CIE-10:</label>
                                <div class="typeahead__container">
                                <div class="typeahead__field">
                                <div class="typeahead__query">
                                        <input id="txtDiagnosticoCIE10Uc" 
                                            name="hockey_v1[query]"
                                            type="search" 
                                            data-provide="typeahead"
                                            placeholder="Buscar Diagnóstico CIE-10" 
                                            autocomplete="off"
                                            onclick="this.select();" class="typeahead"/>
                                </div>
                            </div>
                        </div>
                     </div>
                   <div class="col-md-4" style="margin-top: 32px;">
                        <div class="input-group">
                            <input id="txtDiagnosticoCIEPorCodigoUc"
                                type="text"
                                class="form-control"
                                placeholder="Código" />
                            <div class="input-group-append">
                                <a id="btnAgregarPorCodigo" class="btn btn-success"  onclick="agregarPorCodigoDiagnosticoCIE10Uc()">Buscar</a>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <table id="tblDiagnosticos" class='table table-striped table-borderless table-hover w-100' style="display:none;">
                        <thead class="thead-light">
                            <tr class="text-center">
                                <th>Descripción Diagnóstico CIE-10</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%--fin duiagnostico CIE10--%>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/DiagnosticoCie10.js") + "?v=" + GetVersion() %>"></script>
</div>