﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DiagnosticoCIE10Urgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DiagnosticoCIE10Urgencia" %>

<div id="<%= this.ClientID %>_divDiagnosticos">
    <div id="<%= this.ClientID %>_divInfoDiagnostico">
        <div class="row mt-2">

            <div class="col-sm-12 mt-1 mb-1" >
                Acceso rápido a los diagnóstios más frecuentes, clic para agregar.
                <div class="row" id="<%= this.ClientID %>_divDiagnosticosFrecuentesUc" ></div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8" id="<%= this.ClientID %>_divBusquedaDiagnosticoUc">
                
                <label>Diagnóstico CIE-10: <b class="color-error">(*)</b></label>
                <div class="typeahead__container">
                    <div class="typeahead__field">
                        <div class="typeahead__query">
                            <input
                                id="<%= this.ClientID %>_txtDiagnosticoCIE10"
                                name="hockey_v2[query]"
                                data-provide="typeahead"
                                type="search"
                                placeholder="Buscar Diagnóstico CIE-10"
                                autocomplete="off"
                                onclick="this.select();" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8" id="<%= this.ClientID %>_divSeleccionAliasDiagnosticoUc" style="display:none;">

                <div class="row">
                    <div class="col-12">
                        <label for="sltSeleccionAliasDiagnosticoUc"> <i class="fa fa-info-circle text-info fa-2x latidos"></i> El código ingresado arrojo los siguientes resultados:</label>
                    </div>
                    <div class="col-8">
                        <select class="form-control" id="<%= this.ClientID %>_sltSeleccionAliasDiagnosticoUc"></select>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="agregarAliasSeleccionado()"><i class="fa fa-check"></i> Agregar</button>
                    </div>
                    <div class="col-2">
                         <button type="button" class="btn btn-sm btn-danger" onclick="cancelarSeleccionDiagnosticoPorAlias()"> <i class="fa fa-arrow-left"></i> Cancelar</button>
                    </div>
                </div>
        
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3" >
                <label>Buscar por código:</label>
                <div class="input-group mb-3">
                    <input id="<%= this.ClientID %>_txtDiagnosticoCIEPorCodigo"
                        type="text"
                        class="form-control"
                        placeholder="Código" />
                    <div class="input-group-append">
                        <button id="<%= this.ClientID %>_btnAgregarPorCodigoDiag" class="btn btn-success" type="button"> <i class="fa fa-search"></i> Buscar</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="<%= this.ClientID %>_divDiagnosticoSeleccionado" class="alert alert-info text-center mt-2"></div>

        <div class="row mt-2">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                <label>Detalles del Diagnóstico:</label>
                <textarea id="<%= this.ClientID %>_txtDiagnosticoDetalle" rows="3" class="form-control" maxlength="1500"
                    placeholder="Detalles del diagnóstico"></textarea>
            </div>
            <div id="<%= this.ClientID %>_divClasificacionDiagnostico" class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                <label>Estado:</label>
                <select id="<%= this.ClientID %>_sltClasificacionDiagnostico" class="form-control" data-required="true"></select>
            </div>
        </div>

        <div class="text-left mt-4">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <button id="<%= this.ClientID %>_btnAgregarDiagnosticoAlta" class="btn btn-success w-100">
                        <i class="fa fa-plus mr-1"></i>Agregar Diagnostico
                    </button>
                </div>
            </div>
        </div>

    </div>
    <div class="row mt-2">
        <div class="col-sm-12">
            <table id="<%= this.ClientID %>_tblDiagnosticosAlta" class='table table-striped table-borderless table-hover w-100 text-black'>
                <thead class="thead-light">
                    <tr class="text-center">
                        <th>Diagnóstico CIE-10</th>
                        <th>Estado</th>
                        <th>Detalle</th>
                        <th>Diagnóstico Principal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script src="<%= (Session["cargarScript"] == null) ? ResolveClientUrl("~/Script/UserControls/DiagnosticoCIE10Urgencia.js") + "?v=" + GetVersion() : "" %>"></script>

<%
    Session["cargarScript"] = false;
%>