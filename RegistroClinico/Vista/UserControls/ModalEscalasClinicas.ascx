﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalEscalasClinicas.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalEscalasClinicas" %>
<!-- MODAL ESCALAS CLINICAS -->
    <div class="modal fade" id="mdlVerEscalas" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4><i class="fas fa-thermometer-quarter"></i>  Escalas Clínicas</h4>
                        </div>
                        <div class="col-md-6 col-lg-3">
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body" style="min-height: 450px;">
                    <h6>Paciente: <b><span id="nomPacModalEscalas"></span></b></h6>
                    <h6>Cama: <b><span id="numCamaModalEscalas" class="mr-1"></span></b>N° Hospitalizacion: <b><span id="numHospitalizacionModalEscalas"></span></b></h6>
                    <hr />
                    <br />
                    <div class="row">
                        <!--Vertical pills-->
                        <div class="col-sm-12 col-md-12">
                            <ul class="nav nav-tabs" role="tablist" id="ulPillsEscalas">
                                <%--Esto se llena dinamicamente--%>
                            </ul>
                        </div>
                        <!--Contenido de los pills-->
                        <div class="tab-content col-sm-12 col-md-12 mt-2" id="v-pills-tabContent">
                           <%--Esto tambien se llena dinamicamente--%> 
                        </div>
                        <!-- otra seccion aca-->
                    </div>
                </div>
            </div>
        </div>
    </div>


<!--Modal valoracion pacientes de edad avanzada--->
<div class="modal fade" id="mdlValoraciones" tabindex="-1" style="box-sizing: border-box">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info p-3" id="modalHeaderValoraciones">
                <h4>Agregando encuestas clínicias</h4>
            </div>
            <div class="m-3">
                <h6>Paciente: <b><span id="nomPacModalIndicesEscala"></span></b></h6>
                <h6>Cama: <b><span id="numCamaModalIndicesEscala" class="mr-1"></span></b>N° Hospitalizacion: <b><span id="numHospitalizacionModalIndicesEscala"></span></b></h6>
            </div>
            <div class="modal-body  p-2" id="modalBodyValoraciones">
            </div>
        </div>
    </div>
</div>
<!--fin modal pacientes de edad avanzada-->
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalEscalasClinicas.js") + "?v=" + GetVersion() %>"></script>
