﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatosHospitalizacion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.DatosHospitalizacion" %>
<div id="accordionUserControlHospitalizacion">
    <div class="card">
         <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoHospitalizacion" aria-expanded="true" aria-controls="collapseInfoHospitalizacion">
            <div class="row">
              <div class="col-md-8">
                      <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Hospitalización</h5>
              </div>
              <div class="col-md-4 text-right">
                  <i class="fa-lg fa"></i>
              </div>
            </div>
        </div>
        <div class="card-body collapse show"  id="collapseInfoHospitalizacion" aria-labelledby="headingOne" data-parent="#accordionUserControlHospitalizacion">
            <div id="datosHospitalizacion">
                <div class="row">
                    <div class="col-md-2">
                        <label>Id hospitalización</label>
                        <input type="text" class="form-control"  id="txtUbicacionHospitalizacion" disabled/>
                    </div>
                    <div class="col-md-2">
                        <label>Fecha ingreso</label>
                        <input type="text" class="form-control" id="fechaHosp" disabled/>
                    </div>
                    <div class="col-md-2">
                        <label>Dias estadia</label>
                        <input class="form-control" type="text" id="diasEstadia" disabled/>
                    </div>
                    <div class="col-md-2">
                        <label>Cama</label>
                        <input type="text" class="form-control" id="camaHosp" disabled />
                    </div>
                    <div class="col-md-4">
                        <label>Procedencia</label>
                        <input class="form-control" id="txtProcedencia" type="text" disabled />
                    </div>
                    <div class="col-md-4">
                        <label>Ubicacion</label>
                        <input class="form-control" id="txtUbicacion" type="text" disabled />
                    </div>
                
                </div>
            </div>

            <div id="antecedentesClinicosUserControlHospitalizacion" style="display:none;" class="mt-2">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="intervencionProcedimiento" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="recetasIndicacionesMedicas" data-toggle="tab" href="#recetaseindicaionesmedicas-tab" role="tab" aria-controls="recetasIndicacionesMedicas"
                                aria-selected="false">
                                <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Recetas e Indicaciones Médicas<span id="cantidadRecetasIndicacionesMedicas" class="badge badge-pill badge-success ml-1"></span></h5>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="intervenciones" data-toggle="tab" href="#intervenciones-tab" role="tab"
                                aria-controls="intervenciones" aria-selected="true">
                                <h5 class="mt-2"><strong><i class="fas fa-user-md fa-lg mr-2"></i></strong>Intervenciones Realizadas <span id="cantidadIntervenciones" class="badge badge-pill badge-success ml-1"></span></h5>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="procedimientos" data-toggle="tab" href="#procedimientos-tab" role="tab" aria-controls="procedimientos"
                                aria-selected="false">
                                <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Cirugias Programadas <span id="cantidadProcedimientosProgramados" class="badge badge-pill badge-success ml-1"></span></h5>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="interconsultas" data-toggle="tab" href="#interconsultas-tab" role="tab" aria-controls="interconsultas"
                                aria-selected="false">
                                <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Interconsultas <span id="cantidadInterconsultas" class="badge badge-pill badge-success ml-1"></span></h5>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="otros-procedimientos" data-toggle="tab" href="#otros-procedimientos-tab" role="tab" aria-controls="otros-procedimientos"
                                aria-selected="false">
                                <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Otros Procedimientos <span id="cantidadOtrosProcedimientos" class="badge badge-pill badge-success ml-1"></span></h5>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="nav-invervencionProcedimiento">
                        <div class="tab-pane fade show active" id="intervenciones-tab" role="tabpanel" aria-labelledby="nav-invervenciones-tab">
                            <div class="row">
                                <div class="col-md mt-4">
                                    <div id="alertIntervenciones">
                                    </div>
                                    <div class="table-responsive-xl">
                                        <table class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100" id="tblIntervenciones">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="recetaseindicaionesmedicas-tab" role="tabpanel" aria-labelledby="nav-recetaseindicaionesmedicas-tab">
                            <div class="row">
                                <div class="col-md mt-4">
                                    <div id="alertRecetasIndicacionesMedicas">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade active" id="procedimientos-tab" role="tabpanel" aria-labelledby="nav-procedimientos-tab">
                            <div class="row">
                                <div class="col-md mt-4">
                                    <div id="alertProcedimientos">
                                    </div>
                                    <div class="table-responsive-xl">
                                        <table class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100" id="tblProcedimientosProgramados">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="interconsultas-tab" role="tabpanel" aria-labelledby="nav-interconsultas-tab">
                            <div class="row">
                                <div class="col-md mt-4">
                                    <div id="alertInterconsulta">
                                    </div>
                                    <div class="table-responsive-xl">
                                        <table class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100" id="tblInterconsultas">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="otros-procedimientos-tab" role="tabpanel" aria-labelledby="nav-otros-procedimientos-tab">
                            <div class="row">
                                <div class="col-12 col-md-6 mt-4">
                                    <div id="alertOtrosProcedimientos">
                                    </div>
                                    <label for="txtOtrosProcedimientos">Otros Procedimientos:</label>
                                    <textarea class="form-control" id="txtOtrosProcedimientos" rows="4" maxlength="300"></textarea>
                                </div>
                            </div>
                        </div>
                    </div><!--Fin tab content-->
                </div>
            </div><!--Fin div antecedentes clinicos--->
        </div><!--Fin card body collapsive-->
    </div><!--Fin card-->
</div><!--Fin collapsive div padre-->

<script src="<%= ResolveClientUrl("~/Script/UserControls/DatosHospitalizacion.js") + "?v=" + GetVersion()  %>"></script>
