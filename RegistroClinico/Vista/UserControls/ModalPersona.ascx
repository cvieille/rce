﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalPersona.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalPersona" %>

<div id="mdlPersona" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <p class="heading lead" id=""></p>
                <div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="card" id="divMdlPersona">
                    <div class="card-header bg-dark text-white" data-toggle="collapse" data-target="#collapseUserControlPersona" aria-expanded="true" aria-controls="collapseUserControlPersona">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="mb-0"><strong><i class="fas fa-user-friends fa-lg pr-1"></i></strong>Persona</h5>
                            </div>
                            <div class="col-md-2 text-right">
                                <span id="collapsiveInfoPersona"><i class=" fa-lg fa"></i></span>
                            </div>
                        </div>
                    </div>
                    <div id="collapseUserControlPersona" class="collapse show" aria-labelledby="headingOne">
                        <div class="card-body">
                            <div class="mt-3">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Identificación</label>
                                        <select id="sltIdentificacionPersona" class="form-control identificacion">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label id="lblRutPersona" for="sltRut">RUT</label>
                                        <div class="input-group">
                                            <input id="txtnumeroDocPersona" type="text" class="form-control numero-documento" style="width: 40px;"
                                                data-required="false" maxlength="8" />
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">
                                                    <strong>-</strong>
                                                </div>
                                            </div>
                                            <input id="txtDigPersona" type="text" class="form-control digito text-center col-md-3" maxlength="1" placeholder="DV" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <input type="text" class="d-none" id="txtIdPersona" name="name" value="" />
                                    <div class="col-md-3">
                                        <label for="txtNombrePersona">Nombre</label>
                                        <input id="txtNombrePersona" type="text" class="form-control datos-persona" maxlength="50" data-required="false"
                                            placeholder="Escriba nombre del cuidador" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtApePatPersona">Primer Apellido</label>
                                        <input id="txtApePatPersona" type="text" class="form-control datos-persona" maxlength="50" data-required="true"
                                            placeholder="Escriba primer apellido del cuidador" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtApeMatPersona">Segundo Apellido</label>
                                        <input id="txtApeMatPersona" type="text" class="form-control datos-persona" maxlength="50"
                                            placeholder="Escriba segundo apellido del cuidador" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtTelPersona">Teléfono (Opcional)</label>
                                        <input id="txtTelPersona" type="text" class="form-control datos-persona" maxlength="12"
                                            placeholder="Escriba teléfono del cuidador" />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="lblTipoGeneroPersona">Genero</label>
                                        <select id="sltTipoGeneroPersona" class="form-control datos-persona" data-required="false"></select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="lblSexoPersona">Sexo</label>
                                        <select id="sltSexoPersona" class="form-control datos-persona" data-required="false"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md text-right mt-3">
                                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                            <button id="btnCancelarModalCuidadoEnfermeria" class="btn btn-outline-secondary mr-2" type="button">Cancelar</button>
                                            <button id="btnAgregarPersona" class="btn btn-primary" type="button">Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalPersona.js") + "?v=" + GetVersion() %>"></script>
<!-- no subir sin GetInfo() -->
