﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalReubicarUrgencia.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalReubicarUrgencia" %>
<!--Modal Reubicar / Sacar paciente -->
<div class="modal fade" id="mdlReubicarSacarPaciente" role="dialog" aria-labelledby="mdlReubicarSacarPaciente" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h2><strong>Reubicar / Sacar Paciente</strong></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <button
                            type="button"
                            id="btnCambiarCardReubicarSacarPac"
                            class="btn btn-success"
                            onclick="toggleCardReubicarSacarPac(this);">
                            Sacar Paciente
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <%-- Card Trasladar Paciente --%>
                        <div class="card" id="cardTrasladarPaciente" data-visible="true">
                            <div class="card-header bg-info">
                                <h5 class="m-0"><strong>Trasladar Paciente</strong></h5>
                            </div>
                            <div class="card-body">
                                <div id="divReubicarPaciente" class="row mt-2">
                                    <div class="col-sm-4">
                                        <label><strong>Tipo Atención</strong></label>
                                        <select id="sltTipoAtencionReubicarPaciente" class="form-control" data-required="true"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label><strong>Ubicación Box</strong></label>
                                        <select id="sltTipoBoxReubicarPaciente" class="form-control" data-required="true"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label><strong>Box</strong></label>
                                        <select id="sltBoxReubicarPaciente" class="form-control" data-required="true"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button id="btnReubicarPaciente" class="btn btn-success">Trasladar Paciente</button>
                            </div>
                        </div>
                        <%-- Card Sacar Paciente --%>
                        <div class="card" id="cardSacarPaciente">
                            <div class="card-header bg-info">
                                <h5 class="m-0"><strong>Volver a trasladar al paciente en bandeja</strong></h5>
                            </div>
                            <div class="card-body">
                                <div class="card border-dark mb-3" style="max-width: 50rem;">
                                    <div class="card-header">Retirar paciente del box</div>
                                    <div class="card-body text-dark">
                                        <h5 class="card-title"></h5>
                                        <p class="card-text">Si el paciente ha sido ubicado incorrectamente en un box, puede trasladarlo de inmediato a la bandeja con esta opción</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="button" id="btnSacarPaciente" class="btn btn-success" onclick="sacarPacienteDeBox();">Retirar Paciente</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card border-secondary mb-3" style="max-width: 30rem;">
                            <div class="card-header text-center bg-info">Datos de paciente</div>
                            <div class="card-body">
                                <div class="row" id="datosPacienteReubicacion">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-right">
                <%--<a class="btn btn-success btn-md" onclick="guardarMedicamentosUrg()">Aceptar</a>--%>
                <a class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalReubicarUrgencia.js") + "?v=" + GetVersion() %>"></script>