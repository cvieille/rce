﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalHojasEvolucion.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalHojasEvolucion" %>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalHojasEvolucion.js") + "?" + GetVersion() %>"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('.tabular.menu .item').tab();
        $('#lnbExportarBandeja').click(function () {
            __doPostBack("<%= lnbExportarBandejaH.UniqueID %>", JSON.stringify(datosExportarHE));
        });
    });
</script>


<div id="mdlVerHojasEvolucion" class="modal show fade" role="dialog" aria-labelledby="mdlImprimir"
    aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div id="divCarouselHos" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" style="overflow: visible;">
                    <div class="modal-header modal-header-info">
                        <p class="heading lead" id="txtTitle">
                            Hojas de Evolución
                        </p>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true" class="white-text">x</span>
                        </button>
                    </div>
                    <div class="carousel-item active">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nombre de paciente:</label>
                                    <input id="txtPacienteHE" class="form-control" disabled="disabled">
                                </div>
                                <div class="col-md-2">
                                    <label>Cama actual:</label>
                                    <input id="txtCamaActualHE" class="form-control" disabled="disabled">
                                </div>
                            </div>
                            <div class="row mt-3 mb-3">
                                <div class="col-md-4">
                                    <a id='linkNuevaHojaEvolucion' class='btn btn-info load-click' onclick='linkCrearNuevaHojaEvolucion(this);'>
                                        <i class='fa fa-plus'></i>Nueva hoja evolucion
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <asp:Button runat="server" ID="lnbExportarBandejaH" OnClick="lnbExportarBandejaH_Click" Style="display: none;" />
                                    <a id="lnbExportarBandeja" class="btn btn-success" href="#\">Exportar a Excel <i class="fa fa-file-excel-o"></i>
                                    </a>
                                </div>
                            </div>

                            <table id="tblHojasEvolucion" class="table table-bordered table-hover">
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimirHE" role="dialog" aria-labelledby="mdlImprimir"
        aria-hidden="true" tabindex="2"
        style="z-index: 1051 !important;">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameHojaEvolucion" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlMovimientoHE" class="modal fade" role="dialog" aria-labelledby="mdlMovimiento"
        aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead" id="txtTitleMovHE">
                        Movimientos hoja de evolucion
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-3">
                    <table id="tblMovHE" class="table table-bordered table-hover"></table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
