﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalAlerta.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalAlerta" %>
<div id="modalAlerta" class="modal fade" tabindex="2" style="z-index: 1063 !important;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="hTitulo" class="modal-title"></h4>
                <button type="button" class="close white-text" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center">
                <i id="iMensaje" class="fa fa-4x mb-3 animated rotateIn"></i>
                <p id="pMensaje" style="font-size: x-large"></p>
            </div>
            <div class="modal-footer">
                <button id="btnAlertaAceptar" type="button" class="btn btn-primary">Aceptar</button>
                <button id="btnAlertaCerrar" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalAlerta.js") + "?v=" + GetVersion() %>"></script>
