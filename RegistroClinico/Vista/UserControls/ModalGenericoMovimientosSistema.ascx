﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalGenericoMovimientosSistema.ascx.cs" Inherits="RegistroClinico.Vista.UserControls.ModalGenericoMovimientosSistema" %>
<div class="modal fade" id="mdlGenericoMovimientosSistema" tabindex="-1" style="z-index:1060">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-9">
                        <h4><i class="fa fa-list nav-icon"></i> Movimientos <span id="tituloMovimientos"></span></h4>
                    </div>
                </div>
            </div>
            <div class="modal-body">

                <ul class="nav nav-tabs" id="tabMovimiento" role="tablist" style="display:none;">
                    <li class="nav-item" role="presentation">
                        <button id="datos-Movimiento-tab"
                            class="nav-link active"
                            data-toggle="tab"
                            data-target="#divMovimientos"
                            type="button"
                            role="tab"
                            aria-controls="divMovimientos"
                            aria-selected="true">
                            Movimientos</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button id="datos-detalle-movimientos-tab"
                            class="nav-link"
                            data-toggle="tab"
                            data-target="#divDetalleMovimientos"
                            type="button"
                            role="tab"
                            aria-controls="divDetalleMovimientos">Detalle
                        </button>
                    </li>
                </ul>

                <div class="tab-content" id="tabMovimientosContent">
                    <div id="divMovimientos" class="tab-pane fade show active" role="tabpanel">
                        <h6 id="nombrePaciente"></h6>
                        <hr class="mb-4" />
                        <div id="detallesTablaGenerico" class="modal-body" style="margin-top: -30px; display: none;">
                            <table id="tblGenericoMovimientosSistema" class="table table-striped table-bordered table-hover dataTable no-footer collapsed w-100">
                            </table>
                        </div>
                        <!-- div para la vista de detalles del usuario -->
                        <div id="detallesUsuarioVista" class="modal-body" style="margin-top: -30px; display: none;">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="detalleNombre"><strong>Movimiento realizado por:</strong></label>
                                    <input type="text" class="form-control" id="detalleNombre" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="detalleNumeroDocumento"><strong>Número de Documento:</strong></label>
                                    <input type="text" class="form-control" id="detalleNumeroDocumento" readonly>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="detalleEmail"><strong>Email:</strong></label>
                                    <input type="text" class="form-control" id="detalleEmail" readonly>
                                </div>
                            </div>
                            <hr />
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-warning" onclick="volverDetallesUsuarioVista()">Volver a movimientos</button>
                            </div>
                        </div>
                    </div>
                    <div id="divDetalleMovimientos" class="tab-pane fade" role="tabpanel">
                        <h5 class="text-center mt-3 mb-4">Detalles del Movimiento</h5>
                        <strong class="mt-3 mb-3">Detalle:</strong>
                        <div id="divDetalles" class="mb-3 p-2" style="border: 1px solid lightgray;width: 100%;"></div>
                        <div class="text-center">
                            <a class="btn btn-primary" onclick='$("#datos-Movimiento-tab").trigger("click");'>Volver a movimientos</a>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion()  %>"></script>