﻿using RegistroClinico.Negocio;
using System;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Vista.Handlers
{
    public abstract class PageBase : Page
    {
        protected override void OnLoad(EventArgs e)
        {
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
        }

        public static string GetVersion()
        {
            return Funciones.sVersion;
        }

    }
}