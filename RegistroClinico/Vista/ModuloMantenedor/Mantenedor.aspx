﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Mantenedor.aspx.cs" Inherits="RegistroClinico.Vista.ModuloMantenedor.Mantenedor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Mantenedor"></titulo-pagina>

    <div class="container mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Mantenedor de Aplicación Web</h5>
            </div>
            <div class="card-body">
                <p class="card-text">Este es un mantenedor de la aplicación web que te permite realizar tareas de gestión de datos.</p>
                <a href="#" class="btn btn-primary">Ir al Mantenedor</a>
            </div>
            <div class="card-footer">
                <i class="fas fa-cogs"></i>Mantenimiento de Datos
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloMantenedor/Mantenedor.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
