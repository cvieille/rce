﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Ubicacion.aspx.cs" Inherits="RegistroClinico.Vista.ModuloMantenedor.Ubicacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Mantenedor de Ubicaciones"></titulo-pagina>

    <div class="container mt-2">
        <div class="row" id="divTblUbicaciones">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="sltMantenedorAmbito">Ámbito</label>
                                <select id="sltMantenedorAmbito" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info" style="margin-top: 30px;" onclick="filtrarUbicaciones()">
                                    Filtrar por ámbito
                                </button>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <table id="tblMantenedorUbicacion" class="table table-hover table-striped table-bordered w-100"></table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row d-none" id="divConfigurarUbicacion">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10"> <h4>Configurando una ubicación, sus alas, camas y habitaciones</h4></div>
                        <div class="col-md-2"><button class="btn btn-outline-secondary" onclick="volverAtras()" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver atrás</button></div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 accordion"  id="divUbicacion">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL CREACIONDE CAMA O HABITACION -->
        <div class="modal fade" id="mdlCrearElemento" tabindex="-1" role="dialog" aria-labelledby="mdlCrearElemento" aria-hidden="true" data-id="0">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="tituloModalCrearElemento"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="divCrearElemento">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    <!--Mdl gestion cama-->

    <div class="modal fade" id="mdlGestionCama" tabindex="-1" role="dialog" aria-labelledby="mdlCrearElemento" aria-hidden="true" data-id="0">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="tituloModalGestionCama"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="divGestionCama">
                        <div class="col-md-12 col-sm-12 col-xs-12"  id="divGestionInfo">
    
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label for="txtNombreCama">Nombre cama</label>
                            <input type="hidden" disabled id="txtIdCama" />
                            <input type="hidden" disabled id="txtIdHabitacion" />
                            <input type="hidden" disabled id="txtIdAla" />
                            <input type="hidden" disabled id="txtIdUbicacion" />
                            <input type="text" class="form-control"  data-required=true disabled id="txtNombreCama" />
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label for="sltTipoCamaGestion">Tipo Cama</label>
                            <select id="sltTipoCamaGestion" disabled  data-required=true class="form-control"></select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label for="sltTipoClasificacionCamaGestion">Clasificación Cama</label>
                            <select id="sltTipoClasificacionCamaGestion" data-required=true disabled class="form-control"></select>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label>Estado</label><br/>
                            <input type="checkbox" id="estado" />
                        </div>

                        <div class="col-md-12 d-none" id="divEdicionAvanzada">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <button class="btn btn-warning btn-sm mt-3" onclick="cargarEdicionEvanzada(true)" type="button">Edicion avanzada</button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 inputGestionCama d-none">
                                    <label>Ubicación</label>
                                    <select class="form-control" id="sltUbicacion" data-required="true" onchange="cargarAlas(this, '#sltAla')"></select>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 inputGestionCama d-none">
                                    <label>Ala</label>
                                    <select class="form-control" id="sltAla" data-required="true" onchange="cargarHabitaciones(this)"></select>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 inputGestionCama d-none">
                                    <label>Habitación</label>
                                    <select class="form-control" data-required="true" id="sltHabitacion"></select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" id="divGestionEstado">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12"  id="divGestionBotones">
                        
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="mdlReubicacionHabitacion" tabindex="-1" role="dialog" aria-labelledby="mdlCrearElemento" aria-hidden="true" data-id="0">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title" id="tituloReubicacionHabitacion">Movimiento de una habitación a nueva ubicación o ala</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h5 id="txtInfoReubicacionHabitacion"></h5>
                    </div>
                    <div class="col-3">
                        <label>Nombre Habitacion</label>
                        <input type="text" class="form-control" id="txtNombreHabitacion"  />
                    </div>
                    <div class="col-3">
                        <label>Ubicación</label>
                        <select class="form-control" data-required="true" onchange="cargarAlas(this, '#sltAlaHabitacion')" id="sltUbicacionHabitacion"></select>
                    </div><div class="col-3">
                        <label>Ubicación</label>
                        <select class="form-control" data-required="true" id="sltAlaHabitacion"></select>
                    
                    </div><div class="col-3">
                        <button id="btnReubicarHabitacion" type="button" onclick="reubicarHabitacion()" class="btn btn-primary btn-sm mt-4">Reubicar habitación</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>       
    </div>
</div>




<div class="modal fade" id="mdlMovimientosCama" tabindex="-1" role="dialog" aria-labelledby="mdlCrearElemento" aria-hidden="true" data-id="0">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title" id="tituloMdlMovimientosCama"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="tblMovimientosCama" class="table table-bordered table-hover w-100"></table>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="mdlAsociarAla" tabindex="-1" role="dialog" aria-labelledby="mdlAsociarAla" aria-hidden="true" data-id="0">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title" id="tituloAsociarAla"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="divAsociarAla">
                <label>Seleccione el ala</label>
                <select id="sltAlas" data-required="true" class="form-control">

                </select>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnAsociarAla" onclick="cambiarEstadoAla(0,0,1,this)" class="btn btn-primary">Asociar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloMantenedor/Ubicaciones/Ubicacion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloMantenedor/Ubicaciones/Tabla.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
