﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="AdminIps.aspx.cs" Inherits="RegistroClinico.Vista.ModuloMantenedor.AdminIps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Mantenedor de IP"></titulo-pagina>

    <!-- Tabla para mostrar las IPs autorizadas -->
    <div class="container mt-4">
        <h3>Listado de IPs Autorizadas</h3>
        <table id="tablaIps" class="table table-bordered">
            <thead>
                <tr>
                    <th>IP</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <!-- Aquí se cargarán las IPs dinámicamente -->
            </tbody>
        </table>
        <br>
        <!-- Botones de acción -->
        <%--<button type="button" class="btn btn-primary" onclick="agregarFila()">Agregar IP</button>
        <button type="button" class="btn btn-success" onclick="guardarCambios()">Guardar Cambios</button>--%>
        <br><br>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloMantenedor/AdminIP/AdminIps.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
