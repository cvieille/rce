﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace RegistroClinico.Vista.ModuloHOS
{


    public partial class NuevaHojaEnfermeria
    {

        /// <summary>
        /// Control wucPaciente.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.Paciente wucPaciente;

        /// <summary>
        /// Control wucDatosAcompañante.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.DatosAcompañante wucDatosAcompañante;

        /// <summary>
        /// Control mdlEscalas.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalEscalasClinicas mdlEscalas;

        /// <summary>
        /// Control mdlAislamiento.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAislamiento mdlAislamiento;

        /// <summary>
        /// Control wucMdlAlerta.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAlerta wucMdlAlerta;

        /// <summary>
        /// Control ModalSigonsVitales.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalSignosVitales ModalSigonsVitales;

        /// <summary>
        /// Control DatosHospitalizacion.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.DatosHospitalizacion DatosHospitalizacion;

        /// <summary>
        /// Control ModalAlergias.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAlergias ModalAlergias;
    }
}
