﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaSolicitudHospitalizacion.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevaSolicitudHospitalizacion" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DiagnosticoCIE10Urgencia.ascx" TagName="DiagnosticoUrgencia" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="card mb-0 mt-3">
        <titulo-pagina data-title="Solicitud de Hospitalización"></titulo-pagina>
        <div class="card-body p-0">
            <ul id="ulTablist" class="nav nav-pills mt-2 mb-2">
                <li id="liDatosPaciente" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                    <a class="nav-link active" data-toggle="pill" href="#divDatosPaciente">Datos del paciente</a>
                </li>
                <li id="liAntecedentes" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                    <a class="nav-link" data-toggle="pill" href="#divAntecedentes">Antecedentes del paciente</a>
                </li>
                <li id="liDiagnosticos" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                    <a class="nav-link" data-toggle="pill" href="#divDiagnosticos">Diagnóstico</a>
                </li>
                <li id="liDatosSolicitud" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                    <a class="nav-link" data-toggle="pill" href="#divDatosSolicitud">Datos de la solicitud</a>
                </li>
            </ul>
            <div class="tab-content card">

                <div id="divDatosPaciente" class="tab-pane fade show active pt-3" role="tabpanel" aria-labelledby="home-tab-just">
                    <wuc:Paciente ID="wucPaciente" runat="server" />
                    <div class="text-right">
                        <a class="btn btn-primary" onclick="$('#liAntecedentes a').trigger('click');">Siguiente <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>

                <div id="divAntecedentes" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="contact-tab-just">

                    <div class="card text-white">
                        <div id="divCollapseAntecedentes" class="card-header bg-dark" data-toggle="collapse" data-target="#CollapseAntecedentes" aria-expanded="true" aria-controls="CollapseAntecedentes">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="mb-0"><strong><i class="fas fa-notes-medical fa-lg pr-1"></i></strong>Antecedentes del Paciente</h5>
                                </div>
                            </div>
                        </div>
                        <div id="CollapseAntecedentes" class="collapse show p-3" aria-labelledby="headingOne">
                            <div id="divClasificacionAntecedentes"></div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a class="btn btn-primary" onclick="$('#liDatosPaciente a').trigger('click');">
                            <i class="fa fa-arrow-left"></i>Atrás
                        </a>
                        <a class="btn btn-primary" onclick="$('#liDiagnosticos a').trigger('click');">Siguiente <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>

                <div id="divDiagnosticos" class="tab-pane fade" role="tabpanel" aria-labelledby="contact-tab-just">

                    <div class="card text-white">
                        <div id="divCollapseDiagnosticos" class="card-header bg-dark" data-toggle="collapse" data-target="#CollapseDiagnosticos" aria-expanded="true" aria-controls="CollapseAntecedentes">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="mb-0"><strong><i class="fas fa-notes-medical fa-lg pr-1"></i></strong>Diagnósticos</h5>
                                </div>
                            </div>
                        </div>
                        <div id="CollapseDiagnosticos" class="collapse show p-3" aria-labelledby="headingOne">
                            <wuc:DiagnosticoUrgencia ID="wuc_diagnosticoAtencionUrgencia" runat="server" />
                        </div>
                    </div>

                    <div class="text-right">
                        <a class="btn btn-primary" onclick="$('#liAntecedentes a').trigger('click');">
                            <i class="fa fa-arrow-left"></i>Atrás
                        </a>
                        <a class="btn btn-primary" onclick="$('#liDatosSolicitud a').trigger('click');">Siguiente <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>

                </div>

                <div id="divDatosSolicitud" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="contact-tab-just">

                    <div class="card">
                        <div id="divCollapseSolicitudHospitalizacion" class="card-header bg-dark" data-toggle="collapse" data-target="#CollapseSolicitudHospitalizacion" aria-expanded="true" aria-controls="CollapseSolicitudHospitalizacion">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="mb-0"><strong><i class="fas fa-bars fa-lg pr-1"></i></strong>Datos de la Solicitud</h5>
                                </div>
                            </div>
                        </div>
                        <div id="CollapseSolicitudHospitalizacion" class="collapse show p-3" aria-labelledby="headingOne">
                            <div id="divSolicitudHospitalizacionDatosClinicos">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Ubicación Destino</label>
                                        <select id="sltUbicacion" class="form-control" data-required="true"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 w-100 mt-3">
                                        <label>Aislamiento</label><br />
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input id="rdoSiAislamiento" type="radio" class="form-check-input" name="hos" data-required="true">
                                                SI
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input id="rdoNoAislamiento" type="radio" class="form-check-input" name="hos" data-required="true">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="divAislamientoSolicitudHos">
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label>Motivo Aislamiento</label>
                                            <select id="sltMotivoAislamiento" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Tipo Aislamiento</label>
                                            <select id="sltipoAislamiento" class="form-control">
                                            </select>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <button
                                                    type="button"
                                                    class="btn btn-success btn-sm btn-md btn-lg"
                                                    id="btnAgregarAislamiento" onclick="AgregarAislamientoSolicitudHos()"
                                                    style="margin-top: 30px;">
                                                    Agregar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md mt-3">
                                            <div class="table-responsive w-100" style="overflow: auto;">
                                                <table id="tblAislamientosSolicitudHos" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 mt-3">
                                            <label>Observaciones Aislamiento</label>
                                            <textarea id="txtObservaciones" class="form-control" rows="5" placeholder="Escriba observaciones del aislamiento"></textarea>
                                        </div>
                                    </div>
                                </div>
                            <div class="row mt-3">
                                <div class="col-sm-6">
                                    <label>Anamnesis</label>
                                    <textarea id="txtAnamnesis" class="form-control" rows="5" placeholder="Escriba el Anamnesis" data-required="true"></textarea>
                                </div>
                                <div class="col-sm-6">
                                    <label>Examen Físico</label>
                                    <textarea id="txtExamenFisico" class="form-control" rows="5" placeholder="Escriba el Examen Físico" data-required="true"></textarea>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-6">
                                    <label>Motivo de consulta</label>
                                    <textarea id="txtMotivoConsulta" class="form-control" rows="4" placeholder="Escriba Motivo consulta" data-required="true"></textarea>
                                </div>
                                <div class="col-sm-6">
                                    <label>Plan de manejo</label>
                                    <textarea id="txtPlanManejo" class="form-control" rows="4" placeholder="Escriba Planes de Manejo" data-required="true"></textarea>
                                </div>
                            </div>

                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a id="aAtrasIngresoUrgencia" class="btn btn-primary" onclick="$('#liDiagnosticos a').trigger('click');">
                            <i class="fa fa-arrow-left"></i>Atrás
                        </a>
                        <button id="btnGuardarSolicitudHospitalizacion" class="btn btn-success">
                            <i class="fa fa-save"></i>Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevaSolicitudHospitalizacion.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
