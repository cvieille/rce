﻿<%@ Page Language="C#"  MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="FichaNeoNatal.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.FichaNeoNatal" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPerfiles.ascx" TagName="mdlPerfiles" TagPrefix="mdlPerfiles" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Script/Funciones/FuncionesApi.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
    <script type="text/javascript">


    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="alertObligatorios" class="alert text-center alert-obligatorios">
        <b>Los campos marcados son obligatorios (*)</b>.
    </div>
    <div class="row">
        <div class="col-md-12">
            <wuc:Paciente ID="wucPaciente" runat="server" />
        </div>
        <div class="col-md-12">
            <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoHospitalizacion" aria-expanded="true" aria-controls="collapseInfoHospitalizacion">
                    <div class="row">
                      <div class="col-md-12">
                              <h5 class="mb-0"><i class="fas fa-female fa-lg pr-1"></i>Informacion materna</h5>
                      </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Fecha ultima regla</label>
                            <input type="date" id="dataFurOperacional" class="form-control" name="name" value=""  />
                        </div>
                        <div class="col-md-3">
                            <label>COOMBS</label>
                            <select id="sltCoombs" class="form-control" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Grupo sanguineo y RH madre</label>
                            <select id="sltGrupoSanguineoMadre" class="form-control" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Grupo sanguineo y RH hijo</label>
                            <select id="sltGrupoSanguineoHijo" class="form-control" >
                            </select>
                        </div>
                        <div class="col-md-12 p-2">
                             <div class="row">
                                 <div class="col-md-3">
                                     <div class="form-check">
                                        <input class="form-check-input" id="chkControlPrenatal" type="checkbox" name="name" value="" />
                                        <label class="form-check-label"  for="chkControlPrenatal">Control prenatal</label>
                                     </div>
                                 </div>
                                    <div class="col-md-9 d-none" id="divControlPrenatal">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Edad gestacional</label>
                                                <input class="form-control number" id="txtEdadGestacional" placeholder="Edad al 1er control" maxlength="2" type="text" name="name" value="" />
                                            </div>
                                            <div class="col-md-2">
                                                <label>N° controles</label>
                                                <input class="form-control number" type="text" id="txtNumControles" placeholder="Numero de controles" maxlength="2" name="name" value="" />
                                            </div>
                                            <div class="col-md-2">
                                                <label>DARO</label>
                                                <input class="form-control number" type="text" id="txtDaro" placeholder="cantidad de riesgos obstetricos" maxlength="2" name="name" value="" />
                                            </div>
                                        </div>
                                 </div>
                             </div>
                       </div>
                        
                    </div>
                    <div class="row">
                        <div class="card">
                            <div class="card-header">
                                <h3>Antecedentes obstétricos</h3>
                            </div>
                            <div class="card-body">
                                <div class="row  p-2" id="divAntecedentesObstetricos">

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-2">
                            <label>Estado nutricional</label>
                            <select class="form-control" id="sltEstadoNutricional" data-required="true">
                                
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Vacunas</label>
                            <select id="sltVacunas"   data-actions-box="true"  name="sltVacunas" data-width="100%" multiple title="Seleccione"></select>
                        </div>
                        <div class="col-md-3">
                            <label>Corticoide 1°</label>
                            <input type="text" class="form-control" id="txtPrimerCorticoide" name="name" value="" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha Corticoide 1°</label>
                            <input type="date" class="form-control" id="dpPrimerCorticoide"  />
                        </div>
                        <div class="col-md-3">
                            <label>Corticoide 2°</label>
                            <input type="text" class="form-control" id="txtSegundoCorticoide" name="name" value="" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha Corticoide 2°</label>
                            <input type="date" class="form-control" id="dpSegundoCorticoide"  />
                        </div>
                        <div class="col-md-12">
                            <div class="row" id="divHabitos"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label>Farmacos embarazo</label>
                            <input type="text" id="txtFarmacoEmbarazo" class="form-control" name="name" value="" />
                        </div>
                        <div class="col-md-3">
                            <label>Antibioticos</label>
                            <input type="text" id="txtAntibiotico" class="form-control" name="name" value="" />
                        </div>

                        <div class="col-md-2">
                            <label>Fecha 1ra ecografia</label>
                            <input type="date" id="datePrimeraEco" class="form-control" name="name" value="" />
                        </div>
                        <div class="col-md-2">
                            <label>Edad gestacional</label>
                            <input type="number" id="epfPrimeraEco" placeholder="Con respecto a la primera ecografia" class="form-control" name="name" value="" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha ultima ecografia</label>
                            <input type="date" id="dateUltimaEco" class="form-control" name="name" value="" />
                        </div>
                        <div class="col-md-2">
                            <label>Estimacion peso del feto</label>
                            <input type="number" id="epfUltimaEco" placeholder="Con respecto a la ultima ecografia" class="form-control" name="name" value="" />
                        </div>
                        <div class="col-md-4">
                            <label>Sulfato magnesio</label><br />
                            <input type="checkbox" id="sulfmag" />
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <label>Otros antecedentes</label>
                            <textarea class="form-control" id="txtOtrosAntecedentes" maxlength="300" style="resize:none;" ></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Descripcion malformacion</label>
                            <textarea class="form-control" id="txtDescripcionMalformacion" maxlength="300" style="resize:none;"></textarea>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
             <%--examenes--%>
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h5>Examenes</h5>
                        </div>
                        <div class="card-body">
                            <div class="row"  id="divExamenes">
                                <div class="col-md-4 col-sm-6">
                                    <label for="sltExamenes">Seleccione examen</label>
                                    <select id="sltExamenes" class="form-control"></select>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <label for="dpFechaExamen">Fecha</label>
                                    <input type="date"  class="form-control" id="dbFechaExamen" />
                                </div>
                                <div class="col-md-2 col-ms-6 col-xs-12">
                                    <label>Resultado</label> <br />
                                    <input type="checkbox" id="checkExamen" />
                                </div>
                                <div class="col-md-2 col-ms-6 col-xs-12">
                                    <label></label>
                                    <br />
                                    <button class="btn btn-primary" type="button" id="" onclick="agregarExamen()">Agregar Examen</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table class="table-bordered table dataTable table-striped" id="tlbExamenes"></table>
                        </div>
                    </div>
                    <%--fin examenes--%>
            </div>
            <div class="col-md-12">
                <%--Diagnostico CIE10--%>
                    <div class="card">
                        <div class="card-header bg-dark ">
                            <h5>Diagnósticos CIE10</h5>
                        </div>
                        <div class=" card-body">
                            <div class="row">
                                <div class="col-md-12">
                                 <label>Diagnóstico CIE-10: <b class="color-error">(*)</b></label>
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
                                            <div class="typeahead__query">
                                                <input id="txtDiagnosticoCIE10" name="hockey_v1[query]" type="search" placeholder="Buscar Diagnóstico CIE-10" autocomplete="off"
                                                                            onclick="this.select();" class="typeahead" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                   <table id="tblDiagnosticos" class='table table-striped table-borderless table-hover w-100' style="display:none;">
                                       <thead class="thead-light">
                                           <tr class="text-center">
                                               <th>Descripción Diagnóstico CIE-10</th>
                                               <th></th>
                                           </tr>
                                       </thead>
                                       <tbody></tbody>
                                   </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--fin duiagnostico CIE10--%>
            </div>
            <div class="col-md-3 offset-md-9">
                 <button type="button" class="btn btn-primary m-3" onclick="guardarPreNatal()"> <i class="fas fa=save"></i> Guardar ficha prenatal</button>
            </div>

    </div>



    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FichaNeoNatal.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>