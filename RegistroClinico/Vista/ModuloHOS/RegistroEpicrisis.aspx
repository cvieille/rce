﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="RegistroEpicrisis.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.RegistroEpicrisis" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DiagnosticoCIE10Urgencia.ascx" TagName="DiagnosticoUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/MedicamentosHospitalizacion.ascx" TagName="Medicamentos" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Textbox.io/textboxio.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <wuc:Paciente ID="wucPaciente" runat="server" />
    </div>
    <div class="col-md-12 col-sm-12">
        <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header bg-dark">
                <h5> Epicrisis</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="container" class="container mt-5">
                      <div class="progress px-1" style="height: 3px;">
                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <div class="step-container d-flex justify-content-between">
                        <div class="step-circle" onclick="$('#general-tab').click()"><i class="fa fa-file fa-lg"></i></div>
                        <div class="step-circle" onclick="displayStep(2)"><i class="fa fa-diagnoses fa-lg"></i></div>
                        <div class="step-circle" onclick="displayStep(3)"><i class="fa fa-bed fa-lg"></i></div>
                          <div class="step-circle" onclick="displayStep(4)"><i class="fa fa-stethoscope fa-lg"></i></div>
                          <div class="step-circle" onclick="displayStep(5)"><i class="fa fa-capsules fa-lg"></i></div>
                          <div class="step-circle" onclick="displayStep(6)"><i class="fa fa-hospital fa-lg"></i></div>
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general-div" role="tab" aria-controls="home" aria-selected="true"><h4>Datos generales</h4></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" id="diagnostico-tab" data-toggle="tab" href="#diagnostico-div" role="tab" aria-controls="profile" aria-selected="false"><h4> Diagnóstico</h4></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" id="hosp-tab" data-toggle="tab" href="#hospitalizacion-div" role="tab" aria-controls="contact" aria-selected="false"><h4>Hospitalización</h4></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" id="indicaciones-tab" data-toggle="tab" href="#indicaciones-div" role="tab" aria-controls="contact" aria-selected="false"><h4>Indicaciones</h4></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" id="medicamentos-tab" data-toggle="tab" href="#medicamentos-div" role="tab" aria-controls="contact" aria-selected="false"><h4>Medicamentos</h4></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" id="citaciones-tab" data-toggle="tab" href="#citaciones-div" role="tab" aria-controls="contact" aria-selected="false"><h4>Citaciones</h4></a>
                          </li>
                        </ul>
                        <!--div general 1-->
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="general-div" role="tabpanel" aria-labelledby="general-tab">
                            <div class="row" id="divInfoGeneral">
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label for="sltAmbito">Ámbito</label>
                                    <select class="form-control" id="sltAmbito" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label for="dbIngreso" >Fecha ingreso</label>
                                    <input type="date" id="dpIngreso" class="form-control" data-required="true"/>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label for="dpEgreso" >Fecha egreso</label>
                                    <input type="date" id="dpEgreso" class="form-control" data-required="true" />
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label>Borrador</label><br/>
                                    <input type="checkbox" id="chkBorrador" />
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label>Paciente fallecido</label><br />
                                    <input type="checkbox" id="chkFallecido" />
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label>ACV agudo</label><br />
                                    <input type="checkbox" id="chkAcv" />
                                </div>
                            </div> 
                            <div class="row" id="divPediatria" style="display:none;">
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <label for="txtPesoIngreso">Peso ingreso</label>
                                    <input type="number" class="form-control number" id="txtPesoIngreso" placeholder="Peso en gramos" data-required="true" />
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <label for="txtPesoEgreso">Peso egreso</label>
                                    <input type="number" class="form-control number" id="txtPesoEgreso" placeholder="Peso en gramos" data-required="true" />
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <label for="txtDiasUci">Dias en uci</label>
                                    <input type="number" class="form-control number" id="txtDiasUci" placeholder="cantidad de dias" data-required="true" />
                                </div>
                            </div>
                            <div class="row" id="divNeonato" style="display:none;">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="txtNombrePadre">Nombre del padre</label>
                                    <input type="text" class="form-control" id="txtNombrePadre" placeholder="Nombre y Apellido" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="sltEscolaridadPadre">Escolaridad del padre</label>
                                    <select class="form-control" id="sltEscolaridadPadre">
                                        <option value="value">text</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="sltDetalleEscPadre">Detalle escolaridad padre</label>
                                    <input type="text" class="form-control" id="sltDetalleEscPadre" placeholder="mas detalles " />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="txtNombreMadre">Nombre del padre</label>
                                    <input type="text" class="form-control" id="txtNombreMadre" placeholder="Nombre y Apellido" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="sltEscolaridadMadre">Escolaridad de la madre</label>
                                    <select class="form-control" id="sltEscolaridadMadre">
                                        <option value="value">text</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="txtDetalleEscMadre">Detalle escolaridad padre</label>
                                    <input type="text" class="form-control" id="txtDetalleEscMadre" placeholder="mas detalles" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <label for="txtDireccion">Direccion</label>
                                    <input type="text" class="form-control" id="txtDireccionEpicrisis" placeholder="Calle de ejemplo" />
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtNumero">Numero</label>
                                    <input type="text" class="form-control" id="txtNumeroEpicrisis" placeholder="12345" />
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtTelefono">Telefono</label>
                                    <input type="text" class="form-control" id="txtTelefonoEpicrisis" placeholder="9 1234 5678" />
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label for="sltTipoParto">Tipo parto</label>
                                    <select class="form-control" id="sltTipoParto" data-required="true">
                                        <option value="value">text</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtApgar">Escala Apgar</label>
                                    <input type="number" class="form-control number" id="txtApgar" data-required="true" placeholder="numero del 1-10" />
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label for="sltRhMadre">Grupo RH madre</label>
                                    <select class="form-control" id="sltRhMadre" data-required="true">
                                        <option value="value">text</option>
                                    </select>
                                </div>
                               <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label for="sltRhRn">Grupo RH Recien nacido</label>
                                    <select class="form-control" id="sltRhRn">
                                        <option value="value">text</option>
                                    </select>
                               </div>
                               <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label for="dpFechaBgc">Fecha BGC</label>
                                    <input type="date" id="dpFechaBgc"  class="form-control"/>
                               </div>
                               <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label for="dpFechaPku">Fecha PKU</label>
                                    <input type="date" id="dpFechaPku"  class="form-control"/>
                               </div>

                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtApgar">Peso nacimiento</label>
                                    <input type="number" class="form-control" data-required="true" id="txtPesoNacimiento"placeholder="peso en kg" />
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtApgar">Peso alta</label>
                                    <input type="number" class="form-control" data-required="true" id="txtPesoAlta"placeholder="peso en kg" />
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtApgar">Diferencia de peso</label>
                                    <input type="number" class="form-control" data-required="true" id="txtDiferenciaPeso"placeholder="peso en kg" />
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <label for="txtApgar">Días estadia</label>
                                    <input type="number" class="form-control disabled" id="txtDiasEstadia" disabled="disabled" placeholder="numero del 1-10" />
                                </div>

                             </div>

                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <button type="button" class="btn btn-primary divButtonLabel" data-tab="general" onclick="nextStep(this)">Siguiente</button>
                                </div>
                            </div>
                          </div>
                          <!--div diagnostico 2-->
                          <div class="tab-pane fade" id="diagnostico-div" role="tabpanel" aria-labelledby="diagnostico-tab">
                              <div class="row">
                                  <div class="col-md-12 col-xs-12">
                                    <wuc:DiagnosticoUrgencia ID="wuc_diagnosticoEpicrisis" runat="server" />
                                  </div>
                              </div>
                              <div class="row" id="divInfoDiagnostico">
                                  <div class="col-md-6 col-xs-6">
                                    <label>Diagnóstico principal</label>
                                    <textarea id="txtDiagnosticoPrincipal"  data-required="true" class="form-control w-100"></textarea>
                                  </div>
                                  <div class="col-md-6 col-xs-6">
                                      <label>Otros diagnósticos</label>
                                      <textarea id="txtOtroDiagnostico"  class="form-control w-100"></textarea>
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                    <button type="button" class="btn btn-primary divButtonLabel" data-tab="diagnostico" onclick="nextStep(this)">Siguiente</button>
                                  </div>
                              </div>
                          </div>
                            <!--div hospitalizacion 3-->
                          <div class="tab-pane fade" id="hospitalizacion-div" role="tabpanel" aria-labelledby="hospitalizacion-tab">
                              <div class="row" id="divInfoHospitalizacion">
                                  <div class="col-md-12 col-xs-12">
                                      <label>Resumen hospitalización</label>
                                      <textarea id="txtResumenHospitalizacion" style="width: 100%; height: 250px;" data-required="true" class="textboxio"></textarea>
                                  </div>
                                  <div class="col-md-12 col-xs-12">
                                      <label>Complicaciones durante la hospitalización</label>
                                      <textarea id="txtComplicaciones" style="width: 100%; height: 250px;"  class="form-control resize-none"></textarea>
                                  </div>
                                  <div class="col-md-6 col-xs-12">
                                      <label>Cirugías y/o intervenciones</label>
                                      <textarea id="txtCirugias" style="width: 100%; height: 250px;" class="form-control  resize-none"></textarea>
                                  </div>
                                  <div class="col-md-6 col-xs-12">
                                      <label>Procedimientos (ej: punción, artrocentesis, intralación cáteter.)</label>
                                      <textarea id="txtProcedimientos" style="width: 100%; height: 250px;"  class="form-control  resize-none"></textarea>
                                  </div>
                                  <div class="col-md-12 col-xs-12">
                                      <label>Infecciones intrahospitalarias</label>
                                      <textarea id="txtInfecciones" style="width: 100%; height: 250px;" class="form-control  resize-none"></textarea>
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                    <button type="button" class="btn btn-primary divButtonLabel" data-tab="hospitalizacion" onclick="nextStep(this)">Siguiente</button>
                                  </div>
                              </div>
                          </div>
                          <!--div indicaciones 3-->
                          <div class="tab-pane fade" id="indicaciones-div" role="tabpanel" aria-labelledby="indicaciones-tab">
                            <div class="row" id="divInfoIndicaciones">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                <label>Reposo</label>
                                      <textarea id="txtReposo" data-required="true" class="form-control  resize-none"></textarea>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label>Régimen</label>
                                          <textarea id="txtRegimen" data-required="true" class="form-control  resize-none"></textarea>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Otras indicaciones</label>
                                          <textarea id="txtOtrasIndicaciones" style="width: 100%; height: 250px;" class="form-control  resize-none"></textarea>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <button type="button" class="btn btn-primary divButtonLabel" data-tab="indicaciones" onclick="nextStep(this)">Siguiente</button>
                                </div>
                            </div>
                          </div>
                          <div class="tab-pane fade" id="medicamentos-div" role="tabpanel" aria-labelledby="medicamentos-tab">
                                <wuc:Medicamentos ID="Medicamentos" runat="server" />
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <button type="button" class="btn btn-primary divButtonLabel" data-tab="medicamentos" onclick="nextStep(this)">Siguiente</button>
                                    </div>
                                </div>
                          </div>
                          <div class="tab-pane fade" id="citaciones-div" role="tabpanel" aria-labelledby="citaciones-tab">
                              <div class="row">
                                  <div class="col-12" id="divAvisoCitacion">
                                      <div class="alert alert-warning" role="alert">
                                      <i class="fa fa-info-circle"></i> Usted no ha agregado una citación, puede agregar una citación a la epicrisis en el formulario <i class="fa fa-arrow-down"></i>.
                                      </div>
                                  </div>
                              </div>
                              <div class="row " id="divIngresoCitacion">
                                  <div class="col-md-4">
                                      <label>Lugar de citación</label>
                                      <input type="text" id="txtLugarCitacion" data-required="true" class="form-control" />
                                  </div>
                                  <div class="col-md-4">
                                      <label>Fecha aproximada</label>
                                      <input type="date" id="dpFechaCitacion" data-required="true" class="form-control" />
                                  </div>
                                  <div class="col-md-4" id="divBotonCitacion">
                                      <label>&nbsp; </label>
                                      <button type="button" onclick="guardarCitacion()" class="btn btn-success mt-4">Agregar citación</button>
                                  </div>

                                  <div class="col-md-12 mt-4">
                                      <h5>Si todo esta correcto puede guardar la epicrisis en borrador para poder editarla después.</h5>
                                      <button class="btn btn-primary" type="button" id="btnGuardarEpicrisis" onclick="guardarEpicrisis()" > <i class="fa fa-save fa-lg"></i> Guardar epicrisis</button>
                                  </div>
                              </div>                              
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
</div>




<script src="<%= ResolveClientUrl("~/Script/ModuloHOS/RegistroEpicrisis.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
