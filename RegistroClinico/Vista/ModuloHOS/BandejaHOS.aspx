﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaHOS.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.BandejaHOS" %>

<%@ Register Src="~/Vista/UserControls/ModalHojasEvolucion.ascx" TagName="HojaEvolucion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHospitalizaciones.ascx" TagName="VerHospitalizaciones" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEscalasClinicas.ascx" TagName="mdlEscalasClinicas" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEnTransito.ascx" TagName="ModalPacienteEnTransito" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAislamiento.ascx" TagName="ModalAislamiento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalCategorizacion.ascx" TagName="ModalCategorizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGestionEnfermeriaMatroneria.ascx" TagName="ModalGestionEnfermeriaMatroneria" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalConsentimientoDocente.ascx" TagName="ModalConsentimientoDocente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalBrazaleteHospitalizacion.ascx" TagName="ModalBrazaleteHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalCambioCama.ascx" TagName="ModalCambioCama" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlergias.ascx" TagName="ModalAlergias" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalIngresosEnfermeria.ascx" TagName="ModalIngresosEnfermeria" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <style>
        .small-box {
            margin-bottom: 0px;
        }

        .info-box {
            min-height: unset;
            padding: unset;
            margin-bottom: unset;
        }

        .badge-orange {
            color: #ffffff !important;
            background-color: orange !important;
        }

        .dropdown.bootstrap-select .dropdown-toggle {
            border: 1px solid lightgray;
        }
    </style>

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.tabular.menu .item').tab();
        });

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <!-- Control de usuario moviemientos de sistema-->
    <wuc:ModalGenericoMovimientosSistema ID="ModalGenericoMovimientosSistema" runat="server" />
    <wuc:ModalAislamiento ID="ModalAislamiento" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlEscalasClinicas ID="MdlEscalas" runat="server" />
    <wuc:ModalCategorizacion ID="mdlCategorizacion" runat="server" />
    <wuc:ModalConsentimientoDocente ID="mdlConsentimientoDocente" runat="server" />
    <wuc:ModalBrazaleteHospitalizacion ID="ModalBrazaleteHospitalizacion" runat="server" />
    <wuc:ModalCambioCama ID="ModalCambioCama" runat="server" />
    <wuc:ModalPacienteEnTransito ID="ModalPacienteEnTransito" runat="server" />
    <wuc:ModalIngresosEnfermeria ID="ModalIngresosEnfermeria" runat="server" />

    <input type="hidden" id="idHos" value="0" />
    <input type="hidden" id="idTraslado" value="0" />
    <!-- Componente de titulo-->
    <titulo-pagina data-title="Hospitalización"></titulo-pagina>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <ul id="ulTablist" class="nav nav-tabs nav-justified md-tabs ml-0 mr-0" role="tablist" style="z-index: 0; display: none;">
                        <li id="liBandejaHospitalizacion" class="nav-item">
                            <a id="aBandejaHospitalizacion" class="nav-link active" data-toggle="tab"
                                href="#divBandejaHospitalizacion" role="tab" aria-controls="home-just" onclick="getTablaHospitalizacion();"
                                aria-selected="true">
                                <h3>Bandeja Hospitalización</h3>
                            </a>
                        </li>
                        <li id="liDatosIngreso" class="nav-item" style="display: none;">
                            <a id="aBandejaSolicitudesHospitalizacion" class="nav-link" data-toggle="tab" href="#divBandejaSolicitudesHospitalizacion" role="tab" aria-controls="contact-just"
                                aria-selected="false">
                                <h3>Bandeja Solicitudes de Hospitalización</h3>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="divBandejaHospitalizacion" class="tab-pane fade show active" role="tabpanel"
                            aria-labelledby="home-tab-just">
                            <div class="card-body">
                                <button id="btn-FiltroBandejaHospitalizacion" data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                                    data-target="#divFiltroHos" onclick="return false;">
                                    FILTRAR BANDEJA
                                </button>
                                <div id="divFiltroHos" class="collapse card p-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroId">ID/NRO.</label>
                                                <input id="txtHosFiltroId" type="number" class="form-control" max="9" placeholder="De Hospitalización" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                                <select id="sltTipoIdentificacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label id="lblTipoIdentificacion" for="txtHosFiltroRut">RUT</label>
                                                <div class="input-group" id="divRutDigito">
                                                    <input id="txtHosFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del paciente" />
                                                    <div id="guionDigitoHos" class="input-group-prepend digito">
                                                        <div class="input-group-text">-</div>
                                                    </div>
                                                    <input type="text" id="txtHosFiltroDV" class="form-control digito" style="width: 10px" disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroNombre">Nombre</label>
                                                <input id="txtHosFiltroNombre" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroApePaterno">Primer Apellido</label>
                                                <input id="txtHosFiltroApePaterno" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtHosFiltroApeMaterno">Segundo Apellido</label>
                                                <input id="txtHosFiltroApeMaterno" type="text" class="form-control" placeholder="Del Paciente" maxlength="50" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlHosEstado">Estado</label>
                                                <select id="ddlHosEstado" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlUbicacion">Ubicación</label>
                                                <select id="ddlUbicacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlTipoHospitalizacion">Tipo de Hospitalización</label>
                                                <select id="ddlTipoHospitalizacion" class="form-control">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12 text-right">
                                                <a id="btnHosFiltro" class="btn btn-success" onclick="getTablaHospitalizacion()">
                                                    <i class="fa fa-search"></i>Buscar</a>
                                                <a id="btnHosLimpiarFiltro" class="btn btn-default">
                                                    <i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 20px; padding-top: 20px;">
                                    <div class="col-md-12 text-right">
                                        <a id="btnNuevoHos" style="display: none;" class="btn btn-info waves-effect waves-light" href="NuevoFormularioAdmision.aspx?id=0">
                                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Nuevo Ingreso Admisión
                                        </a>
                                        <a id="btnRefrescar" class="btn btn-primary" onclick="getTablaHospitalizacion()"><i class="fa fa-refresh"></i>Refrescar</a>
                                    </div>
                                </div>
                                <h1 id="testHolaMundo"></h1>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="tblHospitalizacion" class="table table-striped table-bordered table-hover w-100">
                                        </table>
                                    </div>
                                </div>
                                <br />
                                <div class="card" style="padding: 10px; background: #f0f0f0;">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-danger badge-count">&nbsp;A&nbsp;</span>
                                            Hospitalización con aislamiento.                                            
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-secondary badge-count">&nbsp;P&nbsp;</span>
                                            Hospitalización pre-ingresada
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-success badge-count" style="padding:3px 13px 3px 13px">I</span>
                                            Hospitalización ingresada
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-warning badge-count">&nbsp;E&nbsp;</span>
                                            Hospitalización con Epicrisis
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-info badge-count">&nbsp;U&nbsp;</span>
                                            Hospitalización de Urgencia.                                            
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-primary badge-count">&nbsp;P&nbsp;</span>
                                            Hospitalización Programada
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3 col-lg-2">
                                            <span class="badge badge-pill btn-warning badge-count">&nbsp;T&nbsp;</span>
                                            Paciente en Tránsito
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divBandejaSolicitudesHospitalizacion" class="tab-pane fade" role="tabpanel" aria-labelledby="contact-tab-just">
                            <div class="card-body">
                                <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                                    data-target="#divFiltroIngreso" onclick="return false;">
                                    FILTRAR BANDEJA
                                </button>
                                <div id="divFiltroIngreso" class="collapse card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="sltIngTipoIdentificacion">Tipo de identificación</label>
                                                <select id="sltIngTipoIdentificacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label id="lblIngTipoIdentificacion" for="txtHosFiltroRut">RUT</label>
                                                <div class="input-group">
                                                    <input id="txtIngFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del Paciente" />
                                                    <div class="input-group-prepend digito">
                                                        <div class="input-group-text">-</div>
                                                    </div>
                                                    <input type="text" id="txtIngFiltroDV" class="form-control digito" style="width: 10px" disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtIngFiltroNombre">Nombre</label>
                                                <input id="txtIngFiltroNombre" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroApe">Primer Apellido</label>
                                                <input id="txtIngFiltroApe" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txtIngFiltroSApe">Segundo Apellido</label>
                                                <input id="txtIngFiltroSApe" type="text" placeholder="Del Paciente"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12 text-right">
                                                <a id="btnIngFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</a>
                                                <a id="btnIngLimpiarFiltro" class="btn btn-default"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="tblSolicitudesHospitalizacion" class="table table-striped table-bordered table-hover w-100"></table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div id="mdlTraslados" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info pb-1">
                    <h4><b>Traslados del Paciente</b></h4>
                </div>
                <div class="modal-body">
                    <div id="divTraslado">
                        <div id="divNuevoTraslado">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nombre de paciente:</label>
                                    <input id="txtPacienteTraslado" class="form-control" disabled="disabled" />
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2">
                                    <label>Cama actual:</label>
                                    <input id="txtCamaActualTraslado" class="form-control" disabled="disabled" />
                                </div>
                                <div class="col-md-2" id="dvSwitch">
                                    <center>
                                        <label>¿S. Externo?</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" id="swTrasladoExterno" class="custom-control-input form-control">
                                            <label for="swTrasladoExterno" class="custom-control-label">No/Si</label>
                                        </div>
                                    </center>
                                </div>
                                <div class="col-md-4 external-input">
                                    <label id="lblServicioDestinoTraslado">Servicio destino</label>
                                    <select id="ddlServicioDestino" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label id="lblCamaDestinoTraslado">Cama destino</label>
                                    <select id="ddlCamaDestino" class="form-control" data-required="true">
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-lg-9 col-md-6"></div>
                                <div class="col-lg-3 col-md-6 text-center">
                                    <a id="aTraslado" class="btn btn-info mt-3">
                                        <i class="fa fa-plus"></i>Agregar Traslado
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <h5>Listado de Traslados:</h5>
                        <table id="tblTrasladosPaciente" class="table table-bordered table-hover" style="width: 100%">
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" onclick="$('#mdlTraslados').modal('hide'); return false;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mdlAlerta" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal" style="justify-content: center">
                    <div class="center-horizontal">Alerta de sistema</div>
                </div>
                <div class="modal-body">
                    <div id="mdlAlertaCuerpo" class="row center-horizontal">
                    </div>
                    <div class="row" style="padding-top: 5px">
                        <hr />
                    </div>
                    <div id="mdlAlertaBoton" class="row center-horizontal">
                        <div class='col-sm-2'><a id='linkConfirmar' class='btn btn-block' href='#/'>Confirmar </a></div>
                        <div class='col-sm-2'><a id='linkCancelar' class='btn btn-warning btn-block' href='#/' onclick="$('#mdlAlerta').modal('hide');">Cancelar </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlMovimientosIngresoMedico" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title"><i class="fa fa-stethoscope nav-icon"></i>Ingreso médico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <h6>
                        <b>Id Hospitalización:</b>&nbsp;<span id="txtIdHospMdlIngresoMedico"></span>&nbsp;&nbsp;&nbsp;
                                    <b>Paciente:</b>&nbsp;<span id="txtNombrePacienteMdlIngresoMedico"></span>
                    </h6>
                    <div class="text-right mb-2">
                        <a id="aIngresoMedico" class="btn btn-info" onclick="irACrearNuevoIngresoMedico();">
                            <i class="fa fa-plus"></i>Nuevo Ingreso Médico 
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <table id="tblIngresoMedico" class="table table-bordered text-wrap table-hover w-100"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlMovimientosHospitalizacion" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4>Movimientos de hospitalización  #<span id="idHosModalMovimientos"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <h6 id="nomPacModalMovimientos"></h6>
                    <hr />
                    <br />
                    <table id="tblMovHospitalizacion" class="table table-striped table-bordered table-hover nowrap dataTable no-footer collapsed" style="width: 100%;"></table>

                </div>
            </div>
        </div>
    </div>
    <!-- MODAL HOJA ENFERMERIA -->
    <div class="modal fade" id="mdlVerHojaEnfermeria" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="max-width: 62%">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title"><i class="fas fa-user-nurse"></i>
                        Hoja de Enfermería / Matronería</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-9">
                            <h6>Paciente: <b><span id="nomPacModalHojaEnfermeria"></span></b></h6>
                            <h6>Cama: <b><span id="numCamaModalHojaEnfermeria" class="mr-1"></span></b>N° Hospitalizacion: <b><span id="numHospitalizacion"></span></b></h6>
                        </div>
                        <div class="col-md-3">
                            <button type="button" id="btnNuevaHojaEnfermeria" class="btn btn-success form-control mt-2">
                                <i class="far fa-file mr-1"></i>Nuevo
                            </button>
                        </div>
                    </div>

                    <hr />
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="nav-tab-hoja-enfermeria" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="nav-hoja-enfermeria-tab" data-toggle="tab" href="#nav-hoja-enfermeria" role="tab" aria-controls="nav-hoja-enfermeria"
                                        aria-selected="false">
                                        <h5 class="mt-2"><strong><i class="fa fa-file-alt fa-lg mr-2"></i></strong>Hojas de enfermería<span id="cantidadHojaEnfermeria" class="badge badge-pill badge-success ml-1"></span></h5>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="nav-tab-enfermeria-content">
                                <div class="tab-pane fade show active" id="nav-hoja-enfermeria" role="tabpanel" aria-labelledby="nav-hoja-enfermeria-tab">
                                    <div id="ocultarTablaHojaEnfermeriaPorHospitalizacion">
                                        <div class="table-responsive-xl mt-3">
                                            <table id="tblHojaEnfermeriaPorHospitalizacion"
                                                class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100"
                                                style="width: 100%">
                                            </table>
                                        </div>
                                    </div>
                                    <span id="alertHojaEnfermeriaPorHospitalizacion"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL TURNOS HOJA ENFERMERIA -->
    <div class="modal" tabindex="-1" role="dialog" id="mdlNuevoTurnoHE">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white" id="mdlHeadNuevoTipoTipoTurnoHE">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="mdlBodyNuevoTipoTipoTurnoHE">
                    <h5 id="titleBodyTipoTurnoHE"></h5>
                    <div class="row mt-2">
                        <div class="col-md-6 col-sm-12 mb-1">
                            <button type="button" class="btn btn-lg w-100"
                                id="turnoDiurnoHE" data-id="0" onclick="creaNuevahojaEnfermeria(this,1);">
                                <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-brightness-high-fill" viewBox="0 0 16 16">
                                    <path d="M12 8a4 4 0 1 1-8 0 4 4 0 0 1 8 0zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z" />
                                </svg>
                                <br>
                                Largo
                            </button>
                        </div>
                        <div class="col-md-6 col-sm-12 mb-1">
                            <button type="button" class="btn btn-lg w-100"
                                id="turnoNocturnoHE" data-id="0" onclick="creaNuevahojaEnfermeria(this,3);">
                                <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-moon-fill" viewBox="0 0 16 16">
                                    <path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278z" />
                                </svg>
                                <br>
                                Noche
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="showModalHojaEnfermeria()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN -->
    <!--MODAL IMPRIMIR-->
    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameImpresion" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade right" id="mdlFormularios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog modal-lg modal-top-right modal-notify" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <p class="heading lead">Formularios</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevoIPD" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloIPD/NuevoIPD.aspx">
                                IPD <i class="nav-icon fa fa-file"></i>
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevoIC" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloIC/NuevoIC.aspx">
                                Interconsulta <i class="fa fa-user-md" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevoEXA" type="button" class="btn btn-primary btn-block btn-lg h-100" disabled>
                                Exámenes
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevoFQX" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnCargarNuevoForm(this);">
                                Formulario Ind Qx <i class="fa fa-h-square" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevoIngresoMedico" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloHOS/NuevoIngresoMedico.aspx">
                                Ingreso médico <i class="fa fa-stethoscope nav-icon"></i>
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevaSolicitudTrasfusion" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.aspx">
                                Solicitud transfusión <i class="fa fa-tint"></i>
                            </button>
                        </div>
                        <div class="col-md-4  mb-2">
                            <button id="btnNuevaHojaEvolucion" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloHOS/NuevaHojaEvolucionHOS.aspx">
                                Hoja de evolución <i class="fa fa-history" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div class="col-md-4  mb-2">
                            <button id="btnEpicrisis" type="button" class="btn btn-primary btn-block btn-lg h-100" onclick="BtnNuevoFormRCE(this);"
                                data-href="Vista/ModuloHOS/RegistroEpicrisis.aspx">
                                Epicrisis <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="mdlGestionPacienteEnfermeria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog modal-lg modal-top-right modal-notify" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title">Gestion enfermería/matronería sobre el paciente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row p-1 mb-2">
                        <div class="col-md-4">
                            <label>Nombre:</label>
                            <input class="form-control" id="txtModalGestionPacNombrePaciente" disabled />
                        </div>
                        <div class="col-md-4">
                            <label>Id hospitalizacion</label>
                            <input class="form-control" id="txtModalGestionPacIdHospitalizacion" disabled />
                        </div>
                        <div class="col-md-4">
                            <label>Cama:</label>
                            <input class="form-control" id="txtModalGestionPacNombreCama" disabled />
                        </div>
                    </div>
                    <!--cuando los paciente estan hospitalizados-->
                     <wuc:ModalGestionEnfermeriaMatroneria ID="ModalGestionEnfermeriaMatroneria" runat="server" />
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade right" id="mdlGestionHospitalizacionEnfermeria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog modal-lg modal-top-right modal-notify" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title">Gestion enfermería/matronería sobre la hospitalización</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <label>Nombre:</label><input class="form-control" id="txtModalGestionHospNombrePaciente" disabled />
                        </div>
                        <div class="col-md-3">
                            <label>Id hospitalizacion</label>
                            <input class="form-control" id="txtModalGestionHospIdHospitalizacion" disabled />
                        </div>
                        <div class="col-md-2">
                            <label>Cama:</label><input class="form-control" id="txtModalGestionHospNombreCama" disabled />
                        </div>
                        <div class="col-md-12 mb-2"></div>

                        <div class="col-md-4 col-sm-6">
                            <button class="btn btn-info btn-lg w-100" type="button" id="btnVerModalMovimiento" data-tipomodal="movimiento" onclick="linkIrUserControlModal(this, '#mdlGestionHospitalizacionEnfermeria')"><i class="fa fa-list nav-icon"></i>Movimientos Hospitalización</button>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <button class="btn btn-info btn-lg w-100" type="button" id="btnVerModalHistorial" data-tipomodal="historial" onclick="linkIrUserControlModal(this, '#mdlGestionHospitalizacionEnfermeria')">
                                <i class="fas fa-history"></i>
                                <br />
                                Historial hospitalización</button>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <button class="btn btn-info btn-lg w-100" type="button" id="btnVerModalIngresoMedico" data-tipomodal="ingresomedico" onclick="linkIrUserControlModal(this, '#mdlGestionHospitalizacionEnfermeria')">
                                <i class="fa fa-stethoscope nav-icon"></i>
                                <br />
                                Ingreso medico</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Alergias de paciente -->
    <div id="mdlAlergiasPaciente" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title">Alergias del Paciente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <wuc:ModalAlergias ID="ModalAlergias" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div id="mdlFichaNeoNato" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title"><i class="fa fa-child" aria-hidden="true"></i>Ficha clínica neonatal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <label>Nombre:</label><input class="form-control" id="txtModalNeoNatoNombrePaciente" disabled />
                        </div>
                        <div class="col-md-3">
                            <label>Id hospitalizacion</label>
                            <input class="form-control" id="txtModalNeoNatoIdHospitalizacion" disabled />
                        </div>
                        <div class="col-md-2">
                            <label>Cama:</label><input class="form-control" id="txtModalNeoNatoHospNombreCama" disabled />
                        </div>
                    </div>
                    <div class="row  mt-2">
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-info" id="btnIrNuevaFechaNeoNatal" onclick="irFichaNeoNatal(this)"><i class="fa fa-plus"></i>Nuevo ficha</button>
                        </div>
                        <div class="col-md-12 mt-2">
                            <table id="tblNeoNatal" class="table table-hover table-bordered w-100">
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <wuc:HojaEvolucion ID="mdlHojaEvolucion" runat="server" />
    <wuc:VerHospitalizaciones ID="mdlVerHospitalizaciones" runat="server" />
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/BandejaHos.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/BandejaHos.Combos.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/BandejaHos.Links.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Combo.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
