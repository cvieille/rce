﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Categorizaciones.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.Categorizaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="card m-1">
        <div class="card-header bg-info">
            <h4> Mapa de categorizaciones</h4>
        </div>
        <div class="card-body">
            <div class="row p-2" id="divFiltrosBuscarCama">
                <div class="col-md-12">
                    <i class="fa fa-info"></i> &nbsp; Seleccione una ubicación y una fecha para buscar categorizaciones.
                </div>

                <div class="col-md-2">
                    <label for="dpFechaBuscar">Fecha</label>
                    <input type="date" id="dpFechaBuscar" value="" class="form-control" />
                </div>
                <div class="col-md-3">
                    <label for="sltUbicacion" class="active">Servicio</label>
                    <select id="sltUbicacion"  data-required="true" class="form-control">
                    </select>
                </div>
                <div class="col-md-2">
                <br />
                <a id="btnBuscarAla" onclick="buscarAla()" class="btn btn-success">Buscar
                </a>
                </div>
            </div>

            <div class="row p-2">
                <div class="col-md-3 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <canvas id="miGrafico"></canvas>
                        </div>
                        <div class="col-md-12">
                            <canvas id="graficoCategorias"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="contenidoUbicacion"></div>
            </div>
        </div>
    </div>
    
    
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/Categorizaciones.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Chartjs/Chart.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
