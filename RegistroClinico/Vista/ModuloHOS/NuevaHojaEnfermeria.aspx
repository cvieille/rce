﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevaHojaEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevaHojaEnfermeria" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalSignosVitales.ascx" TagName="ModalSignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEscalasClinicas.ascx" TagName="mdlEscalasClinicas" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAislamiento.ascx" TagName="mdlAislamiento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlergias.ascx" TagName="ModalAlergias" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <style>
        .small-box {
            margin-bottom: 0px;
        }

        .info-box {
            min-height: unset;
            padding: unset;
            margin-bottom: unset;
        }

        .badge-orange {
            color: #ffffff !important;
            background-color: orange !important;
        }

        .dropdown.bootstrap-select .dropdown-toggle {
            border: 1px solid lightgray;
        }
    </style>

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Textbox.io/textboxio.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <!-- Componente de titulo-->

    <titulo-pagina data-title="Hoja Enfermería / Matronería"></titulo-pagina>
    <div class="row">
        <div class="col-md">
            <span id="titleTurno"></span>
        </div>
    </div>
    <div class="text-center" id=""></div>
    <wuc:Paciente ID="wucPaciente" runat="server" />
    <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />

    

    <wuc:mdlEscalasClinicas ID="mdlEscalas" runat="server" />
    <wuc:mdlAislamiento ID="mdlAislamiento" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalSignosVitales ID="ModalSigonsVitales" runat="server" />

    <!-- Datos Hospitalizacion-->
    <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />

    <div id="accordionUserControlBrazalete">
        <div class="card">
            <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoBrazalete"
                aria-expanded="true" aria-controls="collapseInfoBrazalete">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="mb-0">Brazalete</h5>
                    </div>
                    <div class="col-md-4 text-right">
                        <i class="fa-lg fa"></i>
                    </div>
                </div>
            </div>
            <div class="card-body collapse show" id="collapseInfoBrazalete" aria-labelledby="headingOne"
                data-parent="#accordionUserControlBrazalete">
                <div class="row">
                    <div class="col-md-8 mt-4">
                        <div class="table-responsive-xl">
                            <table id="tblBrazaleteHojaEnfermeria" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!--Fin card body collapsive-->
        </div>
        <!--Fin card-->
    </div>

    <wuc:ModalAlergias ID="ModalAlergias" runat="server" />

    <div id="accordionPesoTalla">
        <div class="card">
            <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoPesoTalla"
                aria-expanded="true" aria-controls="collapseInfoPesoTalla">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="mb-0"><i class="fa-regular fa-triangle-exclamation"></i>Peso / Talla</h5>
                    </div>
                    <div class="col-md-4 text-right">
                        <i class="fa-lg fa"></i>
                    </div>
                </div>
            </div>
            <div class="card-body collapse show" id="collapseInfoPesoTalla" aria-labelledby="headingOne"
                data-parent="#accordionPesoTalla">
                <div class="w-100">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 p-3">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Peso</h3>
                                </div>
                            </div>
                            <div class="row mb-4" id="divTipoPesoHE">
                                <div class="col-md-2">
                                    <label>Valor</label>
                                    <input type="text" class="form-control decimal" placeholder="Ej:45" id="txtPesoHE" onkeydown="validarDecimales(event)" onkeypress="validarLongitud(this,6,event);" data-required="true" />
                                </div>
                                <div class="col-md-3">
                                    <label>Medidas</label>
                                    <select class="form-control" id="sltTipoPesoHE" data-required="true"></select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-outline-success btn-sm btn-md btn-lg" id="btnGuardarPesoHE" data-tipo="peso" onclick="agregarTipoPeso()" style="margin-top: 30px;">Guardar Peso</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md">
                                    <div class="table-responsive-md">
                                        <table id="tblHojaEnfemeriaPeso" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 p-3">
                            <div class="row">
                                <div class="col-md">
                                    <h3>Talla</h3>
                                </div>
                            </div>
                            <div class="row mb-4" id="divTipoTallaHE">
                                <div class="col-md-2">
                                    <label>Valor</label>
                                    <input type="text" class="form-control decimal" placeholder="Ej:45" id="txtTallaHE" onkeydown="validarDecimales(event)" onkeypress="validarLongitud(this,6,event);" data-required="true" />
                                </div>
                                <div class="col-md-3">
                                    <label>Medidas</label>
                                    <select class="form-control" id="sltTipoTallaHE" data-required="true"></select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-outline-success btn-sm btn-md btn-lg" id="btnGuardarTallaHE" data-tipo="peso" onclick="agregarTipoTalla()" style="margin-top: 30px;">Guardar Talla</button>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md">
                                    <div class="table-responsive-md">
                                        <table id="tblHojaEnfemeriaTalla" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin card body collapsive-->
        </div>
        <!--Fin card-->
    </div>
    <div class="card">
        <div class="card-header bg-secondary">
            <div class="row">
                <div class="col-md-3">
                    <h5 class="mt-1"><i class="fas fa-procedures fa-lg pr-1"></i>
                        Hoja Enfermería/Matronería
                    </h5>
                </div>
                <div class="col-md-9">
                    <h5 class="float-left mt-1"><span id="NumeroHoja">N°</span><span id="numeroHojaEnfermeria" class="badge badge-light badge-success text-bold mt-1 ml-1"></span></h5>
                </div>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="hojaEnfermeriaNav" role="tablist">
                <li class="nav-item h5">
                    <a class="nav-link active" id="hojaGeneral-tab" data-toggle="tab" href="#hojaEnfermeriaGeneral" role="tab" aria-controls="general" aria-selected="true">Hoja General
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja1" data-toggle="tab" href="#RCEnfermeria" role="tab" aria-controls="RCEnfermeria" aria-selected="false" data-id-formulario="1">
                        <span id="textTipoHojaEnfermeria1">Registro Clínico Enfermería</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja2" data-toggle="tab" href="#RCEnfermeriaUPCPediatria" role="tab" aria-controls="RCEnfermeriaUPCPediatria" aria-selected="false" data-id-formulario="2">
                        <span id="textTipoHojaEnfermeria2">Registro Clínico Enfermería UPC Pediatría</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja3" data-toggle="tab" href="#RCEnfermeriaCuidadosBasicosPediatria" role="tab" aria-controls="RCEnfermeriaCuidadosBasicosPediatria" aria-selected="false" data-id-formulario="3">
                        <span id="textTipoHojaEnfermeria3">Registro Clínico Enfermería Cuidados Básicos Pediatría</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja4" data-toggle="tab" href="#RCMatroneria" role="tab" aria-controls="RCMatroneria" aria-selected="false" data-id-formulario="4">
                        <span id="textTipoHojaEnfermeria4">Hoja de Registros Clícnicos de Matronería</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja5" data-toggle="tab" href="#RCMatroneriaUPCNeonatologia" role="tab" aria-controls="RCMatroneriaUPCNeonatologia" aria-selected="false" data-id-formulario="5">
                        <span id="textTipoHojaEnfermeria5">Hoja de Registros Clínicos de Matronería UPC Neonatología</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja6" data-toggle="tab" href="#HojaDiariadeMatroneriaNeonatal" role="tab" aria-controls="HojaDiariadeMatroneriaNeonatal" aria-selected="false" data-id-formulario="6">
                        <span id="textTipoHojaEnfermeria6">Hoja Diaria de Matronería Neonatal</span>
                    </a>
                </li>
                <li class="nav-item h5">
                    <a class="nav-link" id="navtipoHoja7" data-toggle="tab" href="#RCEnfermeriaUPCAdulto" role="tab" aria-controls="RCEnfermeriaUPCAdulto" aria-selected="false" data-id-formulario="7">
                        <span id="textTipoHojaEnfermeria7">Registro Clínico Enfermería UPC Adulto</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-general-tab" id="hojaEnfermeriaGeneral">
                    <div id="hojaEnfermeriaValidacion">
                        <div class="row mt-4">
                            <div class="col-4 col-xs-6 col-md-3 col-xl-2 col-lg-3">
                                <label for="txtFechaHojaEnfermeria">Fecha</label>
                                <input id="txtFechaHojaEnfermeria" type="date" class="form-control" placeholder="Fecha" disabled autocomplete="off" />
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <label for="sltTipoHojaEnfermeria">Tipo Hoja Enfermería / Matronería</label>
                                <select id="sltTipoHojaEnfermeria" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-6">
                                <label for="txtDiagnostico" class="mt-3">Diagnostico</label>
                                <textarea id="txtDiagnostico" class="form-control" rows="3" maxlength="300" data-required="true" disabled placeholder="Agrega el diagnostico"></textarea>
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <label for="txtMedicamentosHabituales" class="mt-3">Medicamentos habituales</label>
                                <textarea id="txtMedicamentosHabituales" class="form-control" rows="3" maxlength="300" placeholder="Agrega medicamentos habituales"></textarea>
                            </div>

                            <div class="col-12 col-md-12 col-lg-6">
                                <label for="txtMorbidos" class="mt-3">Morbidos</label>
                                <textarea id="txtMorbidos" class="form-control" rows="3" maxlength="300" data-required="true" disabled placeholder="Agrega datos mórbidos"></textarea>
                            </div>
                            <div class="col-12 col-md-12 col-lg-6" id="divAntecendetesQx">
                                <div id="divInvalidInputAQx" class="col-4">
                                    <label for="txtAntecedentesQx" class="mt-3">Antecedentes Qx</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" id="radioAntecedentesQxSI" type="radio" name="radioAntecedentesQx" value="SI" data-required="true">
                                        <label class="form-check-label" for="radioAntecedentesQxSI">
                                            Si
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" id="radioAntecedentesQxNO" type="radio" name="radioAntecedentesQx" value="NO" data-required="true">
                                        <label class="form-check-label" for="radioAntecedentesQxNO">
                                            No
                                        </label>
                                    </div>
                                </div>

                                <textarea id="txtAntecedentesQx" class="form-control" rows="3" maxlength="200" placeholder="Antecedentes Qx"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="icon-info" class="alert text-center mt-3 alert-info-mod">
                                <h5 class="mt-2 display-5"><strong><i class="fa fa-info-circle fa-lg"></i></strong>
                                    La información solamente será guardada con el boton ingresar al final del formulario.
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div id="ocultarTabsHojaEnfermeria">
                        <!--inicio navbar-->
                        <ul class="nav nav-tabs mt-5" id="plancuidadosSignosVitales" role="tablist">
                            <li class="nav-item">
                                <!-- 1 -->
                                <a class="nav-link" id="Valoracion-tab" data-toggle="tab" href="#Valoracion" role="tab"
                                    aria-controls="Valoracion" aria-selected="true">
                                    <h5 class="mt-2"><strong><i class="fas fa-crutch mr-2"></i></strong>Valoraciones</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 2 -->
                                <a class="nav-link" id="Invasivo-tab" data-toggle="tab" href="#Invasivo" role="tab"
                                    aria-controls="Examenes" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fas fa-diagnoses fa-lg mr-2"></i></strong>Gestion  IAAS</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 3 -->
                                <a class="nav-link" id="Riesgos-tab" data-toggle="tab" href="#Riesgos" role="tab"
                                    aria-controls="Riesgos" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fa fa-procedures mr-2"></i></strong>Riesgos</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 4 -->
                                <a class="nav-link" id="Signos-tab" data-toggle="tab" href="#Signos" role="tab"
                                    aria-controls="Signos" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fa fa-heartbeat fa-lg mr-2"></i></strong>Signos vitales</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 5 -->
                                <a class="nav-link" id="planCuidadosEnfermeria" data-toggle="tab" href="#plancuidadosenfermeria" role="tab"
                                    aria-controls="planCuidadosEnfermeria" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fas fa-tasks fa-lg mr-2"></i></strong>Plan de cuidados</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 6 -->
                                <a class="nav-link" id="Evolucion-tab" data-toggle="tab" href="#Evolucion" role="tab"
                                    aria-controls="Examenes" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fa fa-file-alt fa-lg mr-2"></i></strong>Evoluciones</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 7 -->
                                <a class="nav-link" id="ManejoHeridas-tab" data-toggle="tab" href="#ManejoHeridas" role="tab"
                                    aria-controls="ManejoHeridas" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fas fa-water fa-lg mr-2"></i></strong>Manejo de Heridas</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 8 -->
                                <a class="nav-link" id="Hidrico-tab" data-toggle="tab" href="#Hidrico" role="tab"
                                    aria-controls="Hidrico" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fas fa-water fa-lg mr-2"></i></strong>Balance hídrico</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 9 -->
                                <a class="nav-link" id="Examenes-tab" data-toggle="tab" href="#Examenes" role="tab"
                                    aria-controls="Examenes" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fa fa-flask fa-lg mr-2"></i></strong>Exámenes</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- 10 -->
                                <a class="nav-link" id="medicamentos-tab" data-toggle="tab" href="#Medicamentos" role="tab"
                                    aria-controls="Examenes" aria-selected="false">
                                    <h5 class="mt-2"><strong><i class="fa fa-medkit fa-lg mr-2"></i></strong>Medicamentos</h5>
                                </a>
                            </li>
                        </ul>
                        <!--Fin navbar-->
                        <!--Inicio tab content-->
                        <div class="tab-content" id="nav-PlanCuidadosSignosVitales">

                            <div class="tab-pane fade active show" id="plancuidadosenfermeria" role="tabpanel"
                                aria-labelledby="nav-plancuidadosenfermeria-tab">
                                <!-- Plan Cuidados 3.0 tblPlancuidadosHE -->
                                <div class="col-md mt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="mb-2">Plan de Cuidados</h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" id="validaSelectorTipoCuidado">
                                            <select id="sltTipoCuidado" class="form-control" data-required="true">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-outline-info" id="btnAgregarActividadTipoCuidado" onclick="agregarActividadTipoCuidado(this)"><i class="fa fa-save pr-1"></i>Agregar</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="table-responsive-xl mt-4 mb-4" id="validaHorariosPlan">
                                                <table id="tblPlancuidadosHE" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-md">
                                                    <button class="btn btn-secondary btn-circle" type="button" data-toggle="tooltip" data-placement="bottom"
                                                        data-original-title="Pendiente" style="display: inline;">
                                                    </button>
                                                    <h6 style="display: inline;">Pendiente</h6>

                                                </div>
                                                <div class="col-md">
                                                    <button class="btn btn-success btn-circle" type="button" data-toggle="tooltip" data-placement="bottom"
                                                        data-original-title="Realizado" style="display: inline;">
                                                    </button>
                                                    <h6 style="display: inline;">Realizado</h6>

                                                </div>
                                                <div class="col-md">
                                                    <button class="btn btn-danger btn-circle" type="button" data-toggle="tooltip" data-placement="bottom"
                                                        data-original-title="No realizado" style="display: inline;">
                                                    </button>
                                                    <h6 style="display: inline;">No realizado</h6>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Signos vitales-->
                            <div class="tab-pane fade" id="Signos" role="tabpanel" aria-labelledby="pills-Signos-tab">
                                <div class="row mt-2">
                                    <div class="col text-center mt-2">
                                        <button type="button" class="btn btn-outline-success" id="aIngresosVitales">Ingreso signos vitales</button>
                                    </div>
                                </div>
                                <div id="accordion" class="m-3" style="margin: 0 auto;">
                                    <h4 id="titleIngresoActual"></h4>
                                    <div id="infoSignosVitales" class="row"></div>
                                </div>
                                <div>
                                    <h6 id="titleConSignos"></h6>
                                    <div id="historialSignosVitales" class="row">
                                        <div style="overflow-x: auto;">
                                            <table id="tblHistorialSignosVitales" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Signos Vitales</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="contenidoHistorialSignosVitales">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- fin singnos vitales-->
                            <!-- Examenes de laboratorio-->
                            <div class="tab-pane fade" id="Examenes" role="tabpanel" aria-labelledby="pills-Examenes-tab">
                                <div class="w-50" id="divExamLab" style="margin: 0 auto;">
                                    <div class="row mt-4">
                                        <div class="col-md-8">
                                            <label>Filtrar exámenes <b class="color-error">(*)</b></label>
                                            <div class="typeahead__container">
                                                <div class="typeahead__field">
                                                    <div class="typeahead__query">
                                                        <input id="thExamenesLaboratorio" name="hockey_v1[query]" type="search" placeholder="Buscar más exámenes"
                                                            autocomplete="off" onclick="this.select();">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>&nbsp;</label><br />
                                            <button type="button" id="addElementTableExam" class="btn btn-success" onclick="agregarExamenHE();">Agregar Exámen</button>
                                        </div>
                                        <!--<div class="col-md-1">
                                            <label>&nbsp;</label><br />
                                            <i id="icon-info" class="fa fa-question-circle fa-2x"></i>
                                        </div>-->
                                    </div>
                                </div>

                                <!--- Tabla con eexamenes de Imagenología-->
                                <div class="row mt-4" style="margin: 0 auto;" id="divExamenesLaboratorioImg">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-2">Exámenes de Imagenología</h4>
                                        <div class="table-responsive-xl">
                                            <table id="tblExamenesImgHE" class="w-100 table table-striped table-bordered table-hover dataTable no-footer">
                                            </table>
                                        </div>
                                        <span id="iconografiaImg"></span>
                                    </div>
                                </div>
                                <!--- Tabla con eexamenes de laboratorio-->
                                <div class="row mt-4" style="margin: 0 auto;" id="divExamenesLaboratorio">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-2">Exámenes de Laboratorio</h4>
                                        <div class="table-responsive-xl">
                                            <table id="tblExamenesLabHE" class="w-100 table table-striped table-bordered table-hover dataTable no-footer">
                                            </table>
                                        </div>
                                        <span id="iconografia"></span>
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <label for="txtObservacionesExamenes">Observación de exámenes</label>
                                        <textarea
                                            id="txtObservacionesExamenes"
                                            class="form-control"
                                            style="resize: none;"
                                            rows="5"
                                            placeholder="Observacion de los exámenes"
                                            maxlength="2999">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <!--Fin examenes laboratorio-->

                            <!-- Medicamentos -->
                            <div class="tab-pane fade" id="Medicamentos" role="tabpanel" aria-labelledby="pills-Examenes-tab">
                                <div class="row mt-4">
                                    <div class="col-md mt-3">
                                        <h2 id="tituloMedicamentos">Medicamentos del Paciente</h2>
                                        <div class="table-responsive-xl mt-4" id="validaHorariosMedicamentos">
                                            <table id="tblMedicamentos" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                                <thead id="theadMedicamentos">
                                                    <tr>
                                                        <th>Medicamento
                                                        </th>
                                                        <th>Horario
                                                        </th>
                                                        <th>Acciones
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyMedicamentos">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <button class="btn btn-secondary btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Indicado por médico" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Indicado</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-success btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Administrado" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Administrado</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-warning btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Suspendido por médico" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Suspendido</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-danger btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Rechazado por paciente" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Rechazado</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-info btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Paciente en ayunas" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">En ayunas</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-primary btn-circle" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Paciente en regimen 0" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Regimen 0</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-dark btn-circle btn-sm" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="No se cuenta con el medicaento" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">No hay medicamento</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-success btn-circle btn-sm" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Paciente en procedimiento" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">En procedimiento</h6>

                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-danger btn-circle btn-sm" type="button" data-toggle="tooltip"
                                                    data-placement="bottom" data-original-title="Paciente con alteración de conciencia" style="display: inline;">
                                                </button>
                                                <h6 style="display: inline;">Por alteración de conciencia</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--- Tabla de medicamentos -->
                            </div>
                            <!--Fin Medicamentos-->
                            <!--Hoja de evoluciones-->
                            <div class="tab-pane fade" id="Evolucion" role="tabpanel" aria-labelledby="pills-Evolucion-tab">
                                <!--otro nav bar-->
                                <nav class="mt-4">
                                    <div class="nav nav-tabs" id="Tab-HojaEvoluciones" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-ingresoEvolucion-tab" data-toggle="tab" href="#ingresoEvolucion" role="tab" aria-controls="nav-ingresoEvolucion" aria-selected="true">Nueva evolución</a>
                                        <a class="nav-item nav-link" id="nav-historialEvolucion-tab" data-toggle="tab" href="#historialEvolucion" role="tab" aria-controls="nav-historialEvolucion" aria-selected="false">Historial de evolución</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent-Evoluciones">
                                    <div class="tab-pane fade show active" id="ingresoEvolucion" role="tabpanel" aria-labelledby="nav-ingresoEvolucion-tab">
                                        <div class="row m-3">
                                            <div class="col" id="divEvolucionesHE">
                                                <label>Descripción de evolución</label>
                                                <textarea id="txtEvolucionAtencion"
                                                    style="resize: none;"
                                                    class="form-control"
                                                    rows="10"
                                                    placeholder="Describa la evolución del paciente"
                                                    maxlength="3000"
                                                    data-required="true">
                                            </textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md">
                                                <div class="col-md">
                                                    <button type="button" id="btn-evolucion" class="btn btn-outline-success ml-3" onclick="agregarEvolucionHojaEnfermeria()">Guardar Evolucion</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="historialEvolucion" role="tabpanel" aria-labelledby="nav-historialEvolucion-tab">
                                        <div class="row mb-3">
                                            <div class="col-md mt-4">
                                                <div class="table-responsive-xl">
                                                    <table id="tblHojaEnfemeriaEvolucion" class="table table-striped table-bordered table-hover col-sm col-xs col-md w-100">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- f otro nav-->
                            </div>
                            <!-- Fin hoja de evoluciones-->
                            <!--tab Invasivos-->
                            <div class="tab-pane fade" id="Invasivo" role="tabpanel" aria-labelledby="pills-Invasivo-tab">
                                <!--otro nav bar-->
                                <nav class="mt-4">
                                    <div class="nav nav-tabs" id="Tab-invasivos" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-invasivos-tab" data-toggle="tab" href="#ingresoInvasivo" role="tab" aria-controls="nav-ingresoEvolucion" aria-selected="true">Nuevo invasivo/Aislamiento</a>
                                        <a class="nav-item nav-link" id="nav-historialInvasivos-tab" data-toggle="tab" href="#historialInvasivo" role="tab" aria-controls="nav-historialEvolucion" aria-selected="false">Historial de invasivos</a>
                                    </div>
                                </nav>

                                <div class="tab-content" id="nav-tabContent-Invasivos">
                                    <div class="tab-pane fade show active" id="ingresoInvasivo" role="tabpanel" aria-labelledby="nav-invasivos-tab">


                                        <div id="cuerpoModalVigilancia" class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button"
                                                        id="btnGestionAislamientos"
                                                        onclick="linkVerAislamiento(this)"
                                                        class="btn btn-outline-success">
                                                        Gestión de Aislamientos</button>
                                                </div>
                                            </div>
                                            <div class="row mt-4">
                                                <div class="col-md-4">
                                                    <label for="sltTipoInvasivoVigilancia">Invasivo:</label>
                                                    <select id="sltTipoInvasivoVigilancia" class="form-control"></select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="sltTipoExtremidadVigilancia">Extremidad: </label>
                                                    <select id="sltTipoExtremidadVigilancia" class="form-control"></select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="sltTipoPlanoVigilancia">Plano: </label>
                                                    <select id="sltTipoPlanoVigilancia" class="form-control"></select>
                                                </div>
                                            </div>
                                            <div class="row mt-4">
                                                <div class="col-md-3">
                                                    <label for="dtFechaVigilancia">Fecha de instalación: </label>
                                                    <input id="dtFechaVigilancia" type="date" class="form-control" />
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="tmHoraVigilancia">Hora: </label>
                                                    <input id="tmHoraVigilancia" type="time" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="row mt-4">
                                                <label for="txtObservacionesVigilancia">&nbsp; Observaciones</label>
                                                <div class="col-md-12">
                                                    <textarea id="txtObservacionesVigilancia" class="form-control" maxlength="200" rows="3" placeholder="Ingrese las observaciones.."></textarea>
                                                </div>
                                            </div>
                                            <div class="row d-flex justify-content-end mt-3">
                                                <div class="">
                                                    <button type="button" id="btnAgregarVigilancia" class="btn btn-outline-success mr-2" onclick="agregarVigilancia()">
                                                        <i class="fa fa-plus fa-lg"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <div class="table-responsive-xl">
                                                <table class="table table-bordered table-hover col-sm col-xs col-md w-100" id="tblVigilanciaIAAS">
                                                </table>
                                            </div>
                                            <span id="alertVigilanciaIAASPorHospitalizacion"></span>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade " id="historialInvasivo" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                        <div class="row mt-2">
                                            <div class="col">
                                                <div class="table-responsive-xl">
                                                    <table class="table table-bordered table-hover col-sm col-xs col-md w-100">
                                                        <thead>
                                                            <tr>
                                                                <th>Invasivo</th>
                                                                <th>Extremidad</th>
                                                                <th>Plano</th>
                                                                <th>Solicita</th>
                                                                <th>Retira</th>
                                                                <th>Dias instalado</th>
                                                                <th>Observaciones</th>
                                                                <th>Motivo Retiro</th>
                                                                <th>Acciones</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="tbodyInvasivos">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div>
                                                    <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: rgb(223, 240, 216); border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                                    <span>Dispositivo en uso</span>
                                                    <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: #ed969e; border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                                    <span>Dispositivo retirado</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--fin otro navbar-->
                            </div>
                            <!--Fin tab invasivos-->

                            <!--Riesgos tab-->
                            <div class="tab-pane fade" id="Riesgos" role="tabpanel" aria-labelledby="pills-Riesgos-tab">
                                <div class="col-md-12 text-center mt-2">
                                    <button type="button" class="btn btn-outline-info mt-2 mr-2 btnMdlEscalas" data-id-escala-clinica="RIESGOS" onclick="linkVerEscalasClinicas(this)">Aplicar Escala</button>
                                    <button type="button" id="addRiesgos" class="btn btn-outline-success mt-2" onclick="getRiesgosSeguridad(1)">Medidas Preventivas y Seguridad</button>
                                    <button type="button" id="aRiesgos" class="btn btn-outline-danger mt-2 d-none" onclick="getRiesgosSeguridad(1)">Cancelar evaluación de Riesgo</button>
                                </div>
                                <div class="row m-3" id="divRiesgos">
                                    <div class="nav flex-column nav-pills col-lg-3 resize-riesgo" id="riesgos-pills-tab" role="tablist" aria-orientation="vertical">
                                    </div>
                                    <div class="tab-content col-lg-9 resize-riesgo" id="riesgos-pills-tabContent" style="max-height: 800px; overflow: auto;">
                                    </div>
                                </div>
                                <div class="row" id="divHistorialRiesgos">
                                    <div id="titleHistorialRiesgo" class="col-md-12 text-center"></div>
                                    <br />
                                    <div id="historialRiesgos" class="col-md-12">
                                    </div>
                                </div>
                            </div>
                            <!--Fin riesgos tab-->
                            <!--Valoraciones tab-->
                            <div class="tab-pane fade" id="Valoracion" role="tabpanel" aria-labelledby="pills-Valoracion-tab">
                                <div class="col-md-12 text-center mt-2">
                                    <button type="button" class="btn btn-outline-info mt-2 mr-2 btnMdlEscalas" data-id-escala-clinica="VALORACIONES" onclick="linkVerEscalasClinicas(this)">Aplicar Escala</button>
                                    <button type="button" id="addValoracion" class="btn btn-outline-success mt-2" onclick="getRiesgosSeguridad(10)">Agregar valoración</button>
                                    <button type="button" id="aValoracion" class="btn btn-outline-danger mt-2 d-none" onclick="getRiesgosSeguridad(10)">Cancelar valoración</button>
                                </div>

                                <div class="row mt-4" id="divValoracion">
                                </div>
                                <div class="row d-none mb-2" id="divInputValoracion">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtObservacionesValoracion">Observaciones de la valoración</label>
                                        <textarea
                                            id="txtObservacionesValoracion"
                                            class="form-control"
                                            style="resize: none;"
                                            rows="8"
                                            placeholder="Observacion de la valoración"
                                            onkeydown="validarLongitud(this,2999)"
                                            maxlength="2999" data-required="true"></textarea>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="txtExamenFisicoValoracion">Exámen físico</label>
                                        <textarea id="txtExamenFisicoValoracion" class="textboxio" style="resize: none; height: 206px;" placeholder="&nbsp;" maxlength="2999" data-required="true"></textarea>
                                    </div>
                                </div>

                                <div class="row" id="divHistorialValoracion">

                                    <div id="titleHistorialValoracion" class="col-md-12 text-center"></div>
                                    <br />

                                    <div id="historialValoracion" class="col-md-12">
                                    </div>
                                </div>
                            </div>
                            <!--Fin valoraciones tab-->
                            <!--tab balance hidrico-->
                            <div class="tab-pane fade" id="Hidrico" role="tabpanel" aria-labelledby="pills-Hidrico-tab">
                                <!--otro nav bar-->
                                <nav class="mt-4">
                                    <div class="nav nav-tabs" id="Tab-hidrico" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-hidrico-tab" data-toggle="tab" href="#ingresoHidrico" role="tab" aria-controls="nav-ingresoHidrico" aria-selected="true">Ingreso balances Hidrico</a>
                                        <a class="nav-item nav-link" id="nav-historialHidrico-tab" data-toggle="tab" href="#historialHidrico" role="tab" aria-controls="nav-historialHidrico" aria-selected="false">Historial de balances Hidricos</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent-Hidricos">
                                    <div class="tab-pane fade show active" id="ingresoHidrico" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                        <div class="row mt-3 mb-3" id="divInputHidricos">
                                            <div class="col-md-3 "></div>
                                            <div class="col-md-3">
                                                <select id="selectHidrico" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control decimal" id="valorHidrico" placeholder="Ingrese valor:ej 45.5" onkeydown="validarDecimales(event)" maxlength="6" />
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" id="addHidrico" class="btn btn-outline-success" onclick="agregarHidricos()"><i class="fa fa-plus"></i></button>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Tabla de ingresos</h4>
                                                <div class="table-responsive-xl">
                                                    <table class="table table-bordered table-hover col-sm col-xs col-md w-100" id="dataTableIngresoHidrico">
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4>Tabla de egresos</h4>
                                                <div class="table-responsive-xl">
                                                    <table class="table table-bordered table-hover col-sm col-xs col-md w-100" id="dataTableEgresoHidrico">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="historialHidrico" role="tabpanel" aria-labelledby="nav-invasivos-tab">
                                        <div class="row mt-3">
                                            <div class="col-md-2">
                                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a class="nav-link active" id="v-pills-Ingresos-tab" data-toggle="pill" href="#v-pills-Ingresos" role="tab" aria-controls="v-pills-Ingresos" aria-selected="true">Historial ingresos</a>
                                                    <a class="nav-link" id="v-pills-Totales-tab" data-toggle="pill" href="#v-pills-Totales" role="tab" aria-controls="v-pills-Totales" aria-selected="false">Totales</a>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade show active" id="v-pills-Ingresos" role="tabpanel" aria-labelledby="v-pills-Ingresos-tab">
                                                        <div id="divHistorialHidricos" class="row mt-2"></div>
                                                    </div>
                                                    <div class="tab-pane fade" id="v-pills-Totales" role="tabpanel" aria-labelledby="v-pills-Totales-tab">
                                                        <div id="totalHistorialHidrico" class="row mt-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Fin tab balance hidrico-->


                            <!--Manejo de Heridas tab-->
                            <div class="tab-pane fade" id="ManejoHeridas" role="tabpanel"
                                aria-labelledby="nav-plancuidadosenfermeria-tab">

                                <div id="divManHeridas">
                                    <div id="divManHeridasForm">
                                        <div class="row ">
                                            <div class="col-md-12 text-center mt-2">
                                                <button type="button" class="btn btn-outline-info mt-2 mr-2 btnMdlEscalas" data-id-escala-clinica="HERIDAS" onclick="linkVerEscalasClinicas(this)">Aplicar Escala</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md">
                                                <h2>Manejo de Heridas</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Clasificación Heridas</label>
                                                <select id="mhClasificacionHerida" class="form-control mb-4">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="form-label">Tipo Extremidad</label>
                                                <select id="mhTipoExtremidad" class="form-control mb-4">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="form-label">Plano</label>
                                                <select id="mhPlano" class="form-control mb-4">
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="form-label">Registro de curaciones</label>
                                                <input type="text" id="txtObservacionManejoHerida" class="form-control mb-4" rows="1" maxlength="100" placeholder="Descripción de la lesión y curación">
                                            </div>
                                            <div class="col-md-1">
                                                <label class="form-label" style="margin-right: 100px">
                                                    <br />
                                                </label>
                                                <button id="" class="btn btn-outline-success mb-4" type="button" onclick="AgregarManejoHeridas()">Agregar</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="margin: 0 auto;" id="divTablaManejoHeridas">
                                        <div class="table-responsive-xl">
                                            <table id="tblManejoHeridas" class="w-100 table table-striped table-bordered table-hover dataTable no-footer">
                                                <thead id="theadManejoHeridas">
                                                    <tr>
                                                        <th>Clasificación Herida
                                                        </th>
                                                        <th>Extremidad
                                                        </th>
                                                        <th>Plano
                                                        </th>
                                                        <th>Observación
                                                        </th>
                                                        <th>Acciones
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyManejoHeridas">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Fin Manejo de Heridas tab-->                           
                        </div>
                    </div>
                    <!-- fin tab content-->
                </div>
                <div class="tab-pane fade" id="RCEnfermeria" role="tabpanel" aria-labelledby="navtipoHoja1">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria1"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="RCEnfermeriaUPCPediatria" role="tabpanel" aria-labelledby="navtipoHoja2">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria2"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="RCEnfermeriaCuidadosBasicosPediatria" role="tabpanel" aria-labelledby="navtipoHoja3">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria3"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="RCMatroneria" role="tabpanel" aria-labelledby="navtipoHoja4">
                    <div class="row">
                        <div class="col-md text-center mt-4">
                            <div class="col-md-12 text-center" id="historialApoyoIntegral">
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="button" id="addApoyoIntegralMatroneria" class="btn btn-outline-success mt-2" style="font-size: 14pt" onclick="getValoracionPsicosocial(11,this)" data-click="true">
                                    Agregar Apoyo Integral
                                </button>
                                <button type="button" id="addObservacionRecienNacido" class="btn btn-outline-primary mt-2" style="font-size: 14pt" onclick="getObservacionRecienNacido(this)" data-click="true">
                                    Agregar Observación Recien Nacido
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md text-center mt-3">
                            <div class="card" id="cardApoyoIntegral">
                                <div class="card-header card bg-light">
                                    <h5 class="card-title"><strong id="titulo-vpsicosocial"></strong></h5>
                                </div>
                                <div class="card-body" id="divApoyoIntegral">

                                    <div class="row" id="cardPsicosocial">
                                    </div>
                                    <div class="row" id="divDestinoProfesional">
                                        <div class="col-md-6">
                                            <label for="txtDestinoDerivacion" class="form-label mt-3">Destino</label>
                                            <input class="form-control" id="txtDestinoDerivacion" type="text"
                                                autocomplete="off">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="txtProfesionalDerivacion" class="form-label mt-3">Profesional</label>
                                            <input class="form-control" id="txtProfesionalDerivacion" type="text"
                                                autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-left">
                                <div class="col-md" id="botonesHistorialVpsicosocial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- INICIO DIV OBS RECIEN NACIDO-->
                    <div class="row">
                        <div class="col-md text-center mt-3">
                            <div class="card" id="cardObsRecienNacido">
                                <div class="card-header card bg-light">
                                    <h5 class="card-title"><strong id="titulo-obsRecienNacido"></strong></h5>
                                </div>
                                <div class="card-body" id="divobsRecienNacido">
                                    <div class="row">
                                        <div class="col-md text-center" id="contenido-obsRecienNacido-pills-tab">
                                        </div>
                                    </div>
                                    <div class="row" id="cardobsRecienNacido">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- FIN DIV OBS RECIEN NACIDO-->
                </div>
                <div class="tab-pane fade" id="RCMatroneriaUPCNeonatologia" role="tabpanel" aria-labelledby="navtipoHoja5">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria5"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="HojaDiariadeMatroneriaNeonatal" role="tabpanel" aria-labelledby="navtipoHoja6">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria6"></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="RCEnfermeriaUPCAdulto" role="tabpanel" aria-labelledby="navtipoHoja7">
                    <div class="row">
                        <div class="col-md mt-3">
                            <span id="titleTipoHojaEnfermeria7"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin div ocultarTabsHojaEnfermeria  -->
            <div class="row">
                <div class="col-md text-right mt-3">
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button id="btnIngresoNuevaHojaEnfermeria" class="btn btn-primary mr-1" type="button">Ingresar</button>
                        <button id="btnCancelarHojaEnfermeria" class="btn btn-outline-secondary mr-2" type="button">Cancelar</button>
                    </div>
                </div>
            </div>
            <!-- MODAL HORARIOS PLAN CUIDADO MATRONERIA -->
            <div class="modal fade" id="mdlHorariosPlanCuidadoEnfermeria" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header bg-primary">
                            <h5 class="modal-title">
                                <span id="tituloHorariosTipoCuidados"></span>
                            </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md">
                                    <a class="btn btn-success btn-sm m-1"
                                        onclick='agregarHoraPlanCuidado(this)'
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        title="Agregar"
                                        style="cursor: pointer; width: 80px;">
                                        <i class="fa fa-plus fa-lg"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="formHorarios" class="row mt-2">
                            </div>
                            <div class="row">
                                <div class="col-md">
                                    <textarea id="observacionesHorarios" class="form-control invalid-input" maxlength="200" rows="5" style="resize: none;" data-required="true"></textarea>
                                </div>
                            </div>
                            <div class="col-md text-right mt-3">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button id="btnCancelarModalCuidadoEnfermeria" class="btn btn-outline-secondary mr-2" type="button">Cancelar</button>
                                    <button id="btnAgregarModalCuidadoEnfermeria" class="btn btn-primary" type="button" onclick="agregarHorariosTablaPlanCuidados()">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MODAL HORARIOS PLAN CUIDADO MATRONERIA -->
            <div class="modal fade" id="mdlEditarHorariosPlanCuidadoEnfermeria" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header bg-primary">
                            <h5 class="modal-title">
                                <span id="tituloEditarHorariosTipoCuidados"></span>
                            </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md">
                                    <a class="btn btn-success btn-sm m-1"
                                        onclick=""
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        title="Agregar"
                                        style="cursor: pointer; width: 80px;">
                                        <i class="fa fa-plus fa-lg"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="formEditarHorarios" class="row mt-2">
                            </div>
                            <div class="row">
                                <div class="col-md">
                                    <textarea id="observacionesEditarHorarios" class="form-control invalid-input" maxlength="200" rows="5" style="resize: none;" data-required="true"></textarea>
                                </div>
                            </div>
                            <div class="col-md text-right mt-3">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button id="btnEdiatAgregarModalCuidadoEnfermeria" class="btn btn-primary" type="button" onclick="editarModalCuidadoEnfermeria(this)">Editar</button>
                                    <button id="btnEditarCancelarModalCuidadoEnfermeria" class="btn btn-outline-secondary mr-2" type="button">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- MODAL ESTADO HORARIOS PLAN DE CUIDADOS -->
        <div class="modal" tabindex="-1" role="dialog" id="mdlEstadoHorariosPlanCuidadoEnfermeria">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tituloEstadoHorariosTipoCuidados">Cambiando estado del examen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5>Seleccione Estado:</h5>
                        <div id="formEstadoPlanCuidado">
                            <div class="form-check">
                                <input class="form-check-input" id="radioPendiente" type="radio" name="estadoHorario" value="119">
                                <label class="form-check-label" for="radioPendiente">
                                    Pendiente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id="radioRealizado" type="radio" name="estadoHorario" value="120">
                                <label class="form-check-label" for="radioRealizado">
                                    Realizado
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id="radioNoRealizado" type="radio" name="estadoHorario" value="121">
                                <label class="form-check-label" for="radioNoRealizado">
                                    No realizado
                                </label>
                            </div>
                            <div class="col-md mt-2">
                                <textarea class="form-control" id="txtObsHorario" rows="4" maxlength="300" placeholder="Observaciones de cierre..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="btnEstadoHorariosPlanCuidadoEnfermeria" data-id="0" data-tr="0" type="button" class="btn btn-primary" onclick="cambiarEstadoModalCuidadoEnfermeria(this)">Cambiar estado</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL HORARIOS MEDICAMENTOS -->
        <div class="modal fade" id="mdlHorariosMedicamentos" tabindex="-1" role="dialog" aria-labelledby="mdlHorariosMedicamentos" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tituloHorariosMedicamentos"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control" id="sltCantidadHorariosMedicamentos">
                                    <option value="0">-Seleccione Cantidad-</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-outline-primary" onclick="agregarHorariosMedicamentos()">Agregar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div id="formHorariosMedicamentos" class="row">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="guardarHorariosMedicamentos();">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL ESTADOS ITEM MEDICAMENTOS -->
        <div class="modal fade" id="mdlEstadosItemMedicamentos" tabindex="-1" role="dialog" aria-labelledby="mdlHorariosMedicamentos" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tituloItemMedicamentos"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input id="hiddenEstadoMedicamentoTR" type="hidden" />
                    <input id="hiddenEstadoMedicamento" type="hidden" />
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md">
                                <select class="form-control" id="sltEstadoMedicamento">
                                    <option value="0">-Seleccione estado-</option>
                                    <option value="133">Indicado por médico</option>
                                    <option value="134">Administrado</option>
                                    <option value="135">Suspendido por médico</option>
                                    <option value="136">Rechazado por paciente</option>
                                    <option value="139">Paciente en ayunas</option>
                                    <option value="140">Paciente en regimen 0</option>
                                    <option value="141">No se cuenta con el medicamento</option>
                                    <option value="150">Paciente en procedimiento</option>
                                    <option value="151">Paciente con alteración de conciencia</option>
                                </select>
                                <div id="divCerrarItemMedicamentos">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md mt-2">
                                <textarea class="form-control" id="txtObservacionesEstadoMedicamento" rows="4" maxlength="300" placeholder="Observacion estado medicamento"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="guardarEstadoMedicamento" onclick="guardartEstadoMedicamento(this)">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- EDITAR HORA MEDICAMENTO -->
        <div class="modal" tabindex="-1" role="dialog" id="mdlEditarHoraMedicamento">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Editar Hora </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input id="hiddenHoraMedicacmentoTR" type="hidden" />
                    <input id="hiddenHoraMedicacmento" type="hidden" />
                    <div class="modal-body" id="divMdlEditarHorarioMedicamento">
                        <div class="col-md text-center">
                            <label for="lblHoraMedicamento" id="lblHoraMedicamento" class="form-label m-3" style="display: inline">Hora:</label>
                            <input type="time" class="form-control" id="txtHoraMedicamento" data-required="true" style="width: 100px; display: inline;" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnHoraMedicamento" onclick="updateHoraMedicamento(this);">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL ESTADOS ITEM PLAN DE CUIDADOS -->
        <div class="modal fade" id="mdlEstadosItemPlanCuidados" tabindex="-1" role="dialog" aria-labelledby="mdlHorariosMedicamentos" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="tituloItemPlanCuidado"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input id="hiddenEstadoPlanCuidadosTR" type="hidden" />
                    <input id="hiddenEstadoPlanCuidados" type="hidden" />
                    <div class="modal-body" id="divMdlEstadosItemPlanCuidados">
                        <div class="row">
                            <div class="col-md">
                                <select class="form-control" id="sltEstadoHoraPlanCuidados" data-required="true">
                                    <option value="0">-Seleccione estado-</option>
                                    <option value="119">Pendiente</option>
                                    <option value="120">Realizado</option>
                                    <option value="121">No Realizado</option>
                                </select>
                                <div id="divCerrarItemPlanCuidados">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md mt-2" id="divObservavionesEstadosPlanCuidados">
                                <textarea class="form-control" id="txtObservacionesEstadoPlanCuidados" rows="4" maxlength="300" placeholder="Observacion"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="guardarEstadoPlanCuidados" data-estado="0" onclick="guardartEstadoPlanCuidados(this)">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- EDITAR HORA PLAN DE CUIDADOS -->
        <div class="modal" tabindex="-1" role="dialog" id="mdlEditarHoraPlanCuidados">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title">Editar Hora </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input id="hiddenIdTipoCuidadoHE" type="hidden" />
                    <input id="hiddenHoraTipoCuidadoHE" type="hidden" />
                    <div class="modal-body" id="divMdlEditarHorarioPlanCuidados">
                        <div class="col-md text-center">
                            <label for="lblHoraMedicamento" id="lblHoraPlanCuidados" class="form-label m-3" style="display: inline">Hora:</label>
                            <input type="time" class="form-control" id="txtHoraPlanCuidados" data-required="true" style="width: 100px; display: inline;" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnHoraPlanCuidados" onclick="updateHoraPlanCuidados();">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal Motivo Retiro Invasivo-->
        <div class="modal" tabindex="-1" role="dialog" id="mdlTipoRetiroInvasivo">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" id="mdlHeadTipoRetiroInvasivo">
                        <h5 class="modal-title" id="titleTipoInvasivo"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="mdlBodyTipoRetiroInvasivo">
                        <h5 id="TitleTipoRetiroInvasivo"></h5>
                        <label>Seleccione motivo</label>
                        <select id="sltTipoRetiroInvasivo" class="form-control" data-required="true">
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnTipoRetiroInvasivo" onclick="cambiarEstadoInvasivo(this)">Aceptar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Foin modal cambio de estado examen-->
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevaHojaEnfermeria.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalBrazaleteHospitalizacion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Medicamentos.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.TipoPlanCuidado.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Examenes.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Peso.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Talla.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/Enfermeria.Evolucion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>


