﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.DashboardEnfermeria" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Dashboard Hoja Enfermería / Matronería"></titulo-pagina>
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
   
    <div class="Container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-info">
                        Situacion Actual de Antenciones
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="seltUbicacionDashBoard">Servicio</label>
                                <select id="seltUbicacionDashBoard" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <table id="tblDashboardSituacionActual" class="table table-striped table-bordered table-hover nowrap dataTable no-footer collapsed w-100">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Detalle  -->
    <div class="modal fade" id="mdlDetalleSituacionActualHosp" tabindex="-1" style="z-index: 1060">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4><i class="fa fa-list nav-icon mr-2"></i><span id="tituloModalDetalleSituacionActual"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table id="tblDetalleSituacionActualHosp" class="table table-striped table-bordered table-hover w-100">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/HojaEnfermeria/DashboardEnfermeria.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
