﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="VistaMapaCamas.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.VistaMapaCamas" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalCategorizacion.ascx" TagName="mdlCategorizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalTrasladosHospitalizacion.ascx" TagName="ModalTrasladosHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGestionEnfermeriaMatroneria.ascx" TagName="ModalGestionEnfermeriaMatroneria" TagPrefix="wuc" %>

<%@ Register Src="~/Vista/UserControls/ModalEscalasClinicas.ascx" TagName="mdlEscalasClinicas" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEnTransito.ascx" TagName="ModalPacienteEnTransito" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAislamiento.ascx" TagName="ModalAislamiento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalConsentimientoDocente.ascx" TagName="ModalConsentimientoDocente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalBrazaleteHospitalizacion.ascx" TagName="ModalBrazaleteHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalCambioCama.ascx" TagName="ModalCambioCama" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlergias.ascx" TagName="ModalAlergias" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalIngresosEnfermeria.ascx" TagName="ModalIngresosEnfermeria" TagPrefix="wuc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet" />
     <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
    <style>
        .popover {
            z-index: 1062 !important;
        }

        .popover-body {
            padding: 0px !important;
        }
    </style>
    <script>
        $(document).ready(function () {

            $('#lnkExportarMovHospitalizacion').click(function () {
                var tblObj = $('#tblMovHospitalizacion').DataTable();
                __doPostBack($(this).data("btnExportarH"), JSON.stringify(tblObj.rows().data().toArray()));
            });

        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlCategorizacion ID="mdlCategorizacion" runat="server" />
    <wuc:ModalGenericoMovimientosSistema ID="mdlMovimientoGenerico" runat="server" />
    <wuc:ModalTrasladosHospitalizacion ID="mdlTraslados" runat="server" />
    <wuc:ModalCambioCama ID="ModalCambioCama" runat="server" />
    <wuc:ModalPacienteEnTransito ID="ModalPacienteEnTransito" runat="server" />
    <wuc:mdlEscalasClinicas ID="mdlEscalasClinicas" runat="server" />
    <wuc:ModalAislamiento ID="ModalAislamiento" runat="server" />
    <wuc:ModalConsentimientoDocente ID="ModalConsentimientoDocente" runat="server" />
    <wuc:ModalBrazaleteHospitalizacion ID="ModalBrazaleteHospitalizacion" runat="server" />
    <wuc:ModalIngresosEnfermeria ID="ModalIngresosEnfermeria" runat="server" />

    <div class="card">
        <div class="card-header">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Mapa de Camas"></titulo-pagina>
        </div>
        <div class="card-body">
            <div class="">
                <div id="divFiltro" class="row center-horizontal-vertical">
                    <div class="col-md-2">
                        <label for="txtFechaHospitalizacion" class="active">fecha</label>
                        <input id="txtFechaHospitalizacion" type="date" class="form-control" disabled="disabled" />
                    </div>
                    <div class="col-md-3">
                        <label for="sltUbicacion" class="active">Servicio</label>
                        <select id="sltUbicacion" class="form-control">
                        </select>
                    </div>
                    <div id="divAla" class="col-md-2">
                        <label for="sltAla" class="active">Ala</label>
                        <select id="sltAla" class="form-control" disabled>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <button class="btn btn-success" type="button" onclick="CargarMapaCamas()"><i class="fa fa-search"></i> Buscar</button>
                    </div>
                </div>
            </div>

            <hr />
            <!--Iconos-->
            <div class="row  mt-3 mb-3">
                <div class="col-md-6">
                    <div class="col text-center">
                        <h5>
                            <b>Estado de las camas</b>
                        </h5>
                    </div>
                    <br />
                    <div class="col" style="display: flex;">
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Disponible</strong><br>
                            <div class='cama disponible' style="margin: 0 auto;">
                                <img src='../../Style/img/bed.PNG' width="40" />
                                <i class='fa fa-check-circle' style='z-index: 1;'></i>
                            </div>
                        </div>
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Ocupada</strong>
                            <div class='cama ocupada' style="margin: 0 auto;">
                                <img src='../../Style/img/bed.PNG' width='40' />
                                <i class='fa fa-user-circle' style='z-index: 1;'></i>
                            </div>
                        </div>
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Bloqueada</strong>
                            <div class='cama bloqueada' style="margin: 0 auto;">
                                <img src='../../Style/img/bed.PNG' width='40' />
                                <i class='fa fa-lock' style='z-index: 1;'></i>
                            </div>

                        </div>
                        <div class='col-md-3 col-sm-6 text-center'>
                            <strong>Deshabilitada</strong>
                            <div class='cama deshabilitada' style="margin: 0 auto;">
                                <img src='../../Style/img/bed.PNG' width='40' />
                                <i class='fa fa-ban' style='z-index: 1;'></i>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 ">
                    <div class="col text-center">
                        <h5>
                            <b class="text-center   ">Categorías y niveles de riesgo.</b>
                        </h5>
                    </div>
                    <div class="row" style="display: flex;">
                        <div class="col text-center">
                            <p>
                                <strong>A</strong><br />
                                Máximo
                            </p>
                            <i class='fa fa-user-circle' style="font-size: 3em; color: #dc3545;"></i>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>B</strong><br />
                                Alto
                            </p>
                            <i class='fa fa-user-circle' style="font-size: 3em; color: #DBA607;"></i>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>C</strong><br />
                                Mediano
                            </p>
                            <i class='fa fa-user-circle' style="font-size: 3em; color: #17a2b8;"></i>
                        </div>
                        <div class="col  text-center">
                            <p>
                                <strong>D</strong><br />
                                Bajo
                            </p>
                            <i class='fa fa-user-circle' style="font-size: 3em; color: #28a745;"></i>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>S/C</strong><br />
                                Sin categoría
                            </p>
                            <i class='fa fa-user-circle' style="font-size: 3em; color: black;"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin iconos-->
            <div id="divPacienteEnTransitoSinCama"></div>
            <div id="divGeneralMapaCama"></div>

            <div class="fixed-bottom text-right">
                <a onclick="$('html, body').animate({ scrollTop: $('body').offset().top}, 500);">
                    <i class="fa fa-arrow-up fa-2x" style="cursor: pointer;"></i>
                </a>
            </div>

        </div>
    </div>
    <!---Modal información paciente-->
    <div class="modal fade " id="mdlInfoPaciente" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info text-white" id="headerModalInfoPaciente">
                    <h5 class="modal-title-mdlInfoPaciente">Cambiar de cama</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body row" id="bodyModalInfoPaciente">
                </div>
                <div class="p-2">
                    <wuc:ModalGestionEnfermeriaMatroneria ID="ModalGestionEnfermeriaMatroneria" runat="server" />
                    

                    <div class="row p-2" id="footerModalInfoPaciente">
                        <%--<div class="row" >

                        </div>--%>
                        <%--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--%>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>
    <!-- Modal Ingresos de enfermeria del paciente en transito y sin cama -->
    <div class="modal fade" id="mdlIngresosPacienteEntransito" tabindex="-1" style="z-index: 1060">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4><i class="fa fa-list nav-icon mr-2"></i>Ingresos de Enfermería del Paciente <span id="tituloIngresosPaciente"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <h6 id="nombrePaciente"></h6>
                    <hr class="mb-4" />

                    <table id="tblIngresosEnfermeriaPacEnTransito" class="table table-striped table-bordered table-hover nowrap dataTable no-footer collapsed w-100">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ingresos de enfermeria del paciente en transito y sin cama -->
    <div class="modal fade" id="mdlMovimientosHospitalicion" tabindex="-1" style="z-index: 1060">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <div class="row w-100">
                        <div class="col-md-6 col-lg-9">
                            <h4><i class="fa fa-list nav-icon mr-2"></i>Movimientos de Hospitalización <span id=""></span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <h6 id="nomPaciente"></h6>
                    <hr class="mb-4" />

                    <table id="tblMovimientosHospitalizacion" class="table table-striped table-bordered table-hover nowrap dataTable no-footer collapsed w-100">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion()  %>"></script>
    <!---->
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/VistaMapaCamas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Funciones/FuncionesApi.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
