﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoIngresoEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevoIngresoEnfermeria" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>
<%--<%@ Register Src="~/Vista/UserControls/ModalAlergias.ascx" TagName="ModalAlergias" TagPrefix="wuc" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Formulario de Ingreso Enfermería"></titulo-pagina>
    <wuc:Paciente ID="wucPaciente" runat="server" />

    <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />

    <div id="divCamaOrigen" class="card">
        <div class="card-header bg-dark" data-toggle="collapse" data-target="#collapseInfoHospitalizacion" aria-expanded="true" aria-controls="collapseInfoHospitalizacion">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Ingreso enfermeria a servicio</h5>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-4">
                    <label for="ddlServicioOrigen">Servicio</label>
                    <select id="ddlServicioOrigen" class="form-control" data-required="true">
                    </select>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <label for="ddlCamaOrigenNuevo">Cama</label>
                    <select id="ddlCamaOrigenNuevo" class="form-control" data-required="true">
                    </select>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <label for="dpFechaIngreso">Fecha ingreso</label>
                    <input type="date" class="form-control date-input" data-required="true" onblur="validarFechaLimite(this, 7, 1)" id="dpFechaIngreso" value="" />
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <label for="dpHoraIngreso">Hora ingreso</label>
                    <input type="time" class="form-control" id="dpHoraIngreso" data-required="true" value="" />
                </div>
                <div class="col-sm-12 col-md-12 col-lg-2">
                    <label for="chkIngresoCompleto">Ingreso completo</label>
                    <span><i id="toltipInfo" class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Al realizar ingreso completo debe ingresar antecedentes mórbidos y hábitos del paciente"></i></span>
                    <br />
                    <input type="checkbox" class="form-check" id="chkIngresoCompleto" onchange="requiredAntecedentes(this)" value="" />
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <label for="txtMotivoConsultaPaciente" class="mt-3">Motivo de hospitalización</label>
                    <textarea id="txtMotivoConsultaPaciente" class="form-control resize-none" rows="3" maxlength="3000" data-required="true"></textarea>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <label for="txtDiagnosticoHospitalizacion" class="mt-3">Diagnóstico</label>
                    <textarea id="txtDiagnosticoHospitalizacion" class="form-control resize-none" data-required="true" rows="3" maxlength="3000"></textarea>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <label for="txtCondicionActualPaciente" class="mt-3">Anamnesis</label>
                    <textarea id="txtCondicionActualPaciente" class="form-control resize-none" rows="3" maxlength="3000"></textarea>
                </div>
            </div>
        </div>
    </div>

    <%--<wuc:ModalAlergias ID="ModalAlergias" runat="server" />--%>

    <div class="card" id="ingresoCompletoDiv" style="display: none;">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#divAntecedentesMorb" aria-expanded="true" aria-controls="divAntecedentesMorb">
                            Antecedentes mórbidos
                               
                        </button>
                    </h5>
                </div>

                            <div id="divAntecedentesMorb" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body row" id="antecedentesMorbidos">
                                
                              </div>
                              <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 p-3">
                                <label for="txtAntecedentesMorbidos" class="mt-3">Observaciones de antecedentes mórbidos</label>
                                <textarea id="txtAntecedentesMorbidos" placeholder="Escriba la observaciones de antecedentes mórbidos si es necesario..." style="resize:none;" class="form-control w=100"  maxlength="3000"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                          <div class="card">
                            <div class="card-header" id="headingFour">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#divHabitos" aria-expanded="false" aria-controls="divHabitos">
                                  Habitos
                                </button>
                              </h5>
                            </div>
                            <div id="divHabitos" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                              <div class="card-body row" >
                                  <div class="col-md-12 col-xs-12">
                                        <div class="row"  id="habitosIngreso">

                                        </div>
                                  </div>
                                  <div class="col-md-12 col-xs-12">
                                      <textarea id="txtObservacionesHabitos" placeholder="Escriba la observaciones de habitos si es necesario..." style="resize:none;" class="form-control w=100"  maxlength="3000"></textarea>
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#divAlergias" aria-expanded="false" aria-controls="divAlergias">
                                  Alergias
                                </button>
                              </h5>
                            </div>
                            <div id="divAlergias" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body row">
                                  <div class="col-md-12 col-xs-12">
                                        <div class="row" id="alergiasIngreso">

                                        </div>
                                  </div>
                                  <div class="col-md-12">
                                      <textarea id="txtObservacionesAlergias" placeholder="Escriba la observaciones de alergias si es necesario..."  class="form-control resize-none w=100"  maxlength="3000"></textarea>
                                  </div>
                              </div>
                            </div>
                          </div>
                         </div>
                    </div>
                    <div class="row p-2">
                        <div class="col-md-4 offset-md-8">
                        <button id="btnOrigen" type="button" onclick="ingresarPacienteEnfermeria()" class="btn btn-primary m-1">
                        <i class='fa fa-file-text nav-icon mr-2'></i>Ingresar paciente
                        </button>
                        <button class="btn btn-outline-secondary m-1" type="button" onclick="window.location.href = ObtenerHost()+'/Vista/ModuloHOS/BandejaHOS.aspx';">Cancelar registro</button>
                        </div>
                    </div>


    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevoIngresoEnfermeria.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
