﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Informes.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.Informes.Informes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    

    <div class="card">
        <div class="card-header">
            <titulo-pagina data-title="Informes Hospitalización"></titulo-pagina>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <label>Informes</label>
                    <select id="sltListaInformesHos"  class="form-control" data-required="true">
                        <option value="Ingresos">Informe Ingreso a hospitalización</option>
                        <option value="Egresos">Informe Egreso Hospitalario</option>
                        <option value="Diario">Listado Diario de hospitalización</option>
                        <option value="ListadoDeTrabajo">Listado de Trabajo</option>
                        <option value="Comges">COMGES</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="fechaInicioInformeHos">Fecha Inicio</label>
                    <input id="fechaInicioInformeHos" type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label for="fechaFinInformeHos">Fecha Final</label>
                    <input id="fechaFinInformeHos" type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <button id="btnBuscarReporte" type="button" class="btn btn-info" style="margin-top: 30px;"><i class="fa fa-file-excel"></i> Descargar Excel</button>
                </div>
            </div>
        </div>
        <div id="divResultadoReporte"></div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/Informes/Informes.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
