﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoFormularioAdmision.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevoFormularioAdmision" %>

<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHospitalizaciones.ascx" TagName="VerHospitalizaciones" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
    </style>
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:VerHospitalizaciones ID="mdlVerHospitalizaciones" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlEventos ID="wucMdlEventos" runat="server" />
    <wuc:mdlPacienteEvento ID="wucMdlPacienteEvento" runat="server" />

    <div class="alert-container">
        <div id="alertHospitalizacionActiva" class="alert text-center alert-hospitalizacion-activa">
            <b>El paciente seleccionado tiene una Hospitalización ACTIVA. Se recomienda finalizar, antes de crear una nueva. Para más información, puede consultar el Historial del paciente en Bandeja</b>.
        </div>
    </div>

    <div class="card card-body">
        <div class="card-header bg-light">
            <titulo-pagina data-title="Formulario de Ingreso Paciente Hospitalizado"></titulo-pagina>
        </div>
        <div class="card-body p-0">

            <div class="card text-white">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Establecimiento</strong> </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label>Servicio de salud</label>
                            <select id="sltServicioSalud" class="form-control" data-required="true">
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label>Establecimiento</label>
                            <select id="sltEstablecimiento" data-required="true" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <wuc:Paciente ID="wucPaciente" runat="server" />

            <div id="divDatosAdicionalesPaciente" class="mb-4">

                <div class="row mt-2">
                    <div class="col-md-3">
                        <label for="sltPais" id="lblPais">País de residencia</label>
                        <select id="sltPais" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltRegion">Región</label>
                        <select id="sltRegion" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltProvincia">Provincia</label>
                        <select id="sltProvincia" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltCiudad">Comuna</label>
                        <select id="sltCiudad" class="form-control datos-pac" data-required="true">
                        </select>
                    </div>
                </div>

                <div id="puebloOriginarioNivelEducacional" class="row mt-2">
                    <div class="col-md-3">
                        <label for="sltPuebloOriginario">Pueblo Originario</label>
                        <select id="sltPuebloOriginario" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltCategoriaOcupacional">Categoría Ocupacional</label>
                        <select id="sltCategoriaOcupacional" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltOcupacion" class="datos-pac">Ocupación</label>
                        <select id="sltOcupacion" class="form-control datos-pac">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sltEscolaridad">Nivel Educacional</label>
                        <select id="sltEscolaridad" class="form-control datos-pac">
                        </select>
                    </div>
                </div>
            </div>
            <!--Control de datos de acompañante--->
            <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />

            <div id="divDatosSolicitudIngresoMedico">
                <div class="card">
                    <div class="card-header bg-dark" data-toggle="collapse" data-target="#divDatosSolicitudIngreso" aria-expanded="true" aria-controls="divDatosSolicitudIngreso">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="mb-0"><i class="fas fa-user-md fa-lg pr-1"></i>Datos de solicitud</h5>
                            </div>
                            <div class="col-md-4 text-right">
                                <i class="fa-lg fa"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body collapse show" id="divDatosSolicitudIngreso" aria-labelledby="headingOne">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="fechaDatosSolicitudIngreso">Fecha solicitud</label>
                                <input type="date" class="form-control" id="fechaDatosSolicitudIngreso" data-required="true" />
                            </div>
                            <div class="col-md-2">
                                <label for="horaDatosSolicitudIngreso">Hora solicitud</label>
                                <input type="time" class="form-control" id="horaDatosSolicitudIngreso" data-required="true" value="" />
                            </div>
                            <div class="col-md-4">
                                <label>Medico Solicitante <b class="color-error">(*)</b></label>
                                <div class="typeahead__container">
                                    <div class="typeahead__field">
                                        <div class="typeahead__query">
                                            <input id="sltMedicoSolicitudIngreso" name="hockey_v1[query]" type="search" placeholder="Buscar Médicos"
                                                autocomplete="off" onclick="this.select();" data-required="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divIngreso" class="card text-white">

                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Pre ingreso</strong> </h5>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-2">
                            <label id="lbltxtFechaIngreso" for="txtFechaIngreso">Fecha</label>
                            <input id="txtFechaIngreso" type="date" class="form-control datos-ingreso" data-required='true' />
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-2">
                            <label id="lblHoraIngreso" for="txtHoraIngreso">Hora</label>
                            <input id="txtHoraIngreso" type="time" class="form-control datos-ingreso" data-required='true' />
                        </div>
                        <div class="col-md-2">
                            <label id="lblIdHospitalizacion" for="txtHoraIngreso">Id Hospitalización</label>
                            <input id="txtIdHospitalizacion" type="text" disabled="disabled" class="form-control disabled" />
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-xs-12 col-md-6 col-lg-4 col-xl-3">
                            <label for="sltProcedenciaIngreso">Procedencia de paciente</label>
                            <select id="sltProcedenciaIngreso" class="form-control datos-ingreso" data-required="true">
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-3 col-xl-3" id="divEspecialidades">
                            <label for="sltEspecialidades">Especialidades</label>
                            <select id="sltEspecialidades" class="form-control datos-ingreso">
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-3 col-xl-3" id="divEstablecimientos">
                            <label for="sltEstablecimientoProcedenciaIngreso">Otro Establecimiento</label>
                            <select id="sltEstablecimientoProcedenciaIngreso" class="form-control datos-ingreso">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>¿Ley previsional?</label><br />
                            <input id="chbLeyPrevisionalIngreso" type="checkbox" class="editable-switch datos-ingreso" />
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4 col-xl-3">
                            <label for="sltLeyPrevisionalIngreso">Ley Previsional Ingreso</label>
                            <select id="sltLeyPrevisionalIngreso" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3">
                            <label for="sltServicioSaludIngreso">Servicio de Salud</label>
                            <select id="sltServicioSaludIngreso" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3">
                            <label for="sltUbicacionIngreso">Ubicación</label>
                            <select id="sltUbicacionIngreso" class="form-control datos-ingreso" data-required="true">
                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-3">
                            <label for="sltTipoHospitalizacion">Tipo de Hospitalización</label>
                            <select id="sltTipoHospitalizacion" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-3">
                            <label for="sltModalidadFonasaIngreso">Modalidad</label>
                            <select id="sltModalidadFonasaIngreso" class="form-control datos-ingreso" data-required='true'>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-4 col-xl-6">
                            <label for="txtMotivoHospitalizacion">Motivo de hospitalización</label>
                            <textarea id="txtMotivoHospitalizacion" class="md-textarea form-control" rows="3" data-required='true'
                                maxlength="200" placeholder="Escriba motivo(s) de hospitalización">
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button id="btnGuardar" class="btn btn-primary waves-effect waves-light">
                <i class="fa fa-save"></i>GUARDAR
            </button>
            <a id="btnCancelar" class="btn btn-default waves-effect waves-light">
                <i class="fa fa-remove"></i>CANCELAR
            </a>
        </div>
    </div>

    <!-- MODAL HOSPITALIZACION ACTIVA -->

    <div class="modal fade" id="mdlHosActiva" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width: 1000px; min-width: 1000px;">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title">Paciente con Hospitalización Previa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0" style="background-color: white !important;">
                    <div class="text-center">
                        <h4>Existen registros de hospitalización activo para este paciente, ¿que acción quiere realizar?</h4>
                        <div class="alert">
                            <h5>
                                <strong id="stgHospitalizacion"></strong>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <a id="aNuevaHospitalizacion" class='btn btn-primary' href='#/'>
                        <i class="fas fa-plus"></i>Nuevo ingreso 
                    </a>
                    <a id="aEditarHospitalizacion" class='btn btn-secondary' href='#/'>
                        <i class='fa fa-edit'></i>Editar ingreso
                    </a>
                    <a id="aCancelarIngresoHospitalizacion" class='btn btn-default' href="#/">
                        <i class="fas fa-ban"></i>Cancelar ingreso
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--Modal solicitudes hospitalizacion-->
    <div class="modal fade" id="mldSolicitudesHospitalizacion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Solicitudes de hospitalización</h5>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <h5><i class="fa fa-info-circle fa-lg"></i> Se han encontrado solicitudes de hospitalización para este paciente, puede cargar la información 
                        ingresada previamente y crear el pre ingreso desde esa solicitud de hospitalización.
                    </h5>
                </div>
                <div class="col-12">
                    <table id="tlbSolicitudesHospitalizacion" class="table dataTable table-striped w-100"></table>
                </div>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!--fin Modal solicitudes hospitalizacion-->
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/ComboPacientes.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevoFormularioAdmision.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevoFormularioAdmision.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/FuncionesHOS.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
