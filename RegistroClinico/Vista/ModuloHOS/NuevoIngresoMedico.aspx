﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoIngresoMedico.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevoIngresoMedico" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPersona.ascx" TagName="ModalPersona" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

    <style>
        textarea {
            height: auto !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="alert-container">
        <div id="alertObligatorios" class="alert text-center alert-obligatorios">
            <b>Los campos marcados son obligatorios (*)</b>.
        </div>
        <div id="alertFormatos" class="alert text-center alert-formato">
            <b>Los campos marcados deben cumplir el formato o estandar necesario</b>.
        </div>
    </div>

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <div class="card card-body">
        <div class="card-header bg-light">
            <div class="row">
                <!-- Componente de titulo-->
                <titulo-pagina data-title="Ingreso Médico"></titulo-pagina>
            </div>
        </div>
        <div class="card-body p-0 m_editable">

            <wuc:Paciente ID="wucPaciente" runat="server" />
            <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />
            <wuc:ModalPersona ID="wucModalPersona" runat="server" />

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de la atención</strong></h5>
                </div>
                <div class="card-body p-2">

                    <div class="row mb-3">
                        <div class="col-md-3">
                            <div id="divTipoIngresoMedico">
                                <label for="sltTipoIngresoMedico">Tipo Ingreso Médico</label>
                                <select id="sltTipoIngresoMedico" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                    </div>

                    <ul id="ulTablist" class="nav nav-pills">
                        <li id="liAntecedentesClinicos" class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#divAntecedentesClinicos">Antecedentes Clínicos
                            </a>
                        </li>
                        <li id="liDatosIngreso" class="nav-item">
                            <a id="contact-tab-just" class="nav-link" data-toggle="pill" href="#divDatosIngreso">Ingreso Médico
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content card">

                        <div class="tab-pane fade show active p-4" id="divAntecedentesClinicos" role="tabpanel" aria-labelledby="home-tab-just">

                            <!-- ANTECENDENTES CLINICOS -->
                            <h4 class="mb-4 mt-4">
                                <strong>Antecedentes Clínicos</strong>
                            </h4>
<%--                            <div class="card p-4" id="">--%>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#divAntecedentesMorb" aria-expanded="true" aria-controls="divAntecedentesMorb">
                                                    Antecedentes mórbidos
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="divAntecedentesMorb" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body row" id="antecedentesMorbidosIM">
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-12 p-3">
                                                    <label for="txtAntecedentesMorbidosIM" class="mt-3">Observaciones de antecedentes mórbidos</label>
                                                    <textarea id="txtAntecedentesMorbidosIM" placeholder="Escriba la observaciones de antecedentes mórbidos si es necesario..." style="resize: none;" class="form-control w=100" maxlength="3000"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<%--                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#divAlergias" aria-expanded="false" aria-controls="divAlergias">
                                                    Alergias
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="divAlergias" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body row" id="alergiasIngreso">
                                            </div>
                                        </div>
                                    </div>--%>
<%--                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#divCirugias" aria-expanded="false" aria-controls="divCirugias">
                                                    Cirugias
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="divCirugias" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                            <div class="card-body">
                                                <table id="tblCirugias" class="dataTable table table-hover table-bordered table-striped w-100"></table>

                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12 col-lg-12 p-3">
                                                        <label for="txtObservacionesQuirurgicos" class="mt-3">Observaciones de antecedentes quirúrgicos</label>
                                                        <textarea id="txtObservacionesQuirurgicos" placeholder="Escriba la observaciones de antecedentes quirúrgicos si es necesario..." style="resize: none;" class="form-control w=100" maxlength="3000"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

                                    <div class="card">
                                        <div class="card-header" id="headingFour">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#divHabitos" aria-expanded="false" aria-controls="divHabitos">
                                                    Habitos
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="divHabitos" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                            <div class="card-body row" id="habitosIngresoIM">
                                            </div>
                                        </div>
                                    </div>
                                </div>
<%--                            </div>--%>

                            <%--<h4 class="mb-3">
                                <strong>Antecedentes Clínicos</strong>
                            </h4>

                            <h5 class="mb-3">
                                <strong>1. Morbidos</strong>
                            </h5>

                            <div id="divPersonalesPediatria" class="mt-3 mb-3">

                                <h5><strong>Personales</strong></h5>

                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Convulsiones</label><br />
                                        <div class="bootstrapSwitch">
                                            <input id="checkConvulsiones" type="checkbox" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>SBOR</label><br />
                                        <div class="bootstrapSwitch">
                                            <input id="checkSbor" type="checkbox" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Neumonías</label><br />
                                        <div class="bootstrapSwitch">
                                            <input id="checkNeumonias" type="checkbox" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Gastrointest</label><br />
                                        <div class="bootstrapSwitch">
                                            <input id="checkGastrointest" type="checkbox" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Urinario</label><br />
                                        <div class="bootstrapSwitch">
                                            <input id="checkUrinario" type="checkbox" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Médicos
                                </div>
                                <div class="col-md-10">
                                    <input id="txtMedicos" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300" placeholder="Descripción Médicos" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Cirugías
                                </div>
                                <div class="col-md-10">
                                    <input id="txtCirugias" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300" placeholder="Descripción Cirugías" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Alergías
                                </div>
                                <div class="col-md-10">
                                    <input id="txtAlergias" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300" placeholder="Descripción Alergías" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Hipertensión Arterial
                                </div>
                                <div class="col-md-10">
                                    <input id="txtHipertension" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300"
                                        placeholder="Descripción Hipertensión Arterial" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Diabetes Mellitus
                                </div>
                                <div class="col-md-10">
                                    <input id="txtDiabetes" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300" placeholder="Descripción Diabetes Mellitus" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    Asma
                                </div>
                                <div class="col-md-10">
                                    <input id="txtAsma" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="300" placeholder="Descripción Asma" />
                                </div>
                            </div>

                            <h5 class="mb-3 mt-3">
                                <strong>2. Hábitos</strong>
                            </h5>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    <label>Tabaco</label>
                                </div>
                                <div class="col-md-2">
                                    <input id="chbTabaco" type="checkbox" name="tipoFiltro" data-on-text="SI" data-off-text="NO" data-on-color="success" data-off-color="warning"
                                        data-size="normal" data-descripcion="divTabaco" class="bootstrapSwitch" />
                                </div>
                                <div id="divTabaco" class="col-md-8">
                                    <input id="txtTabaco" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="200" placeholder="Descripción Tabaco" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    <label>OH</label>
                                </div>
                                <div class="col-md-2">
                                    <input id="chbOh" type="checkbox" name="tipoFiltro" data-on-text="SI" data-off-text="NO" data-on-color="success" data-off-color="warning"
                                        data-size="normal" data-descripcion="divOh" class="bootstrapSwitch" />
                                </div>
                                <div id="divOh" class="col-md-8">
                                    <input id="txtOh" class="form-control" data-required="false" type="text" autocomplete="off" maxlength="200" placeholder="Descripción OH" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 center-vertical">
                                    <label>Drogas</label>
                                </div>
                                <div class="col-md-2">
                                    <input id="chbDrogas" type="checkbox" name="tipoFiltro" data-on-text="SI" data-off-text="NO" data-on-color="success" data-off-color="warning"
                                        data-size="normal" data-descripcion="divDrogas" class="bootstrapSwitch" />
                                </div>
                                <div id="divDrogas" class="col-md-8">
                                    <input id="txtDrogas" class="form-control" data-required="false" type="text" autocomplete="off"
                                        maxlength="300" placeholder="Descripción Drogas" />
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <h5 class="mb-3">
                                        <strong>3. Medicamentos</strong>
                                    </h5>
                                    <label for="txtMedicamentos">Descripción Medicamentos</label>
                                    <textarea id="txtMedicamentos" class="md-textarea md-textarea-auto form-control" rows="5" data-required="false" maxlength="300"
                                        placeholder="Escriba descripción de medicamentos">
                                    </textarea>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="mb-3">
                                        <strong>4. Otros</strong>
                                    </h5>
                                    <label for="txtDescripcionOtros">Descripción Otros</label>
                                    <textarea id="txtDescripcionOtros" class="md-textarea md-textarea-auto form-control" rows="5" data-required="false" maxlength="300"
                                        placeholder="Escriba observaciones adicionales">
                                    </textarea>
                                </div>
                            </div>--%>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary" onclick="IrSiguiente();">Siguiente <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane fade p-4" id="divDatosIngreso" role="tabpanel" aria-labelledby="contact-tab-just">

                            <h4 class="mb-3">
                                <strong>Datos Ingreso Médico</strong>
                            </h4>

                            <div class="row">
                                <div class="col-md-2">
                                    <label for="txtFechaIngreso">Fecha de Ingreso</label>
                                    <input id="txtFechaIngreso" type="date" class="form-control text-center" data-required='true' />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtHoraIngreso">Hora de Ingreso</label>
                                    <input id="txtHoraIngreso" type="time" class="form-control text-center" data-required='true' />
                                </div>
                                <div class="col-md-3">
                                    <label for="sltUbicacion">Servicio</label>
                                    <select id="sltUbicacion" data-required="true" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Consultorio de origen</label>
                                    <select id="sltConsultorioOrigen" class="form-control">
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtMotivoConsulta">Motivo Consulta</label>
                                    <textarea id="txtMotivoConsulta" class="md-textarea md-textarea-auto form-control" rows="3" data-required="true" maxlength="200"
                                        placeholder="Escriba motivo de la consulta">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtAnamnesis">Anamnesis</label>
                                    <textarea id="txtAnamnesis" class="md-textarea md-textarea-auto form-control" rows="7" data-required="true" maxlength="1200"
                                        placeholder="Escriba anamnesis">
                                    </textarea>
                                </div>
                            </div>

                            <h6 class="mb-3 mt-3">
                                <strong>Examen físico general</strong>
                            </h6>

                            <div class="row">
                                <div class="col-md-1">
                                    <label for="txtPesoExamenFisico">Peso/x kg</label>
                                    <input id="txtPesoExamenFisico" type="text" class="form-control" maxlength="6" onkeydown="validarDecimales(event)" placeholder="Peso/x kg" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtTallaExamenFisico">Talla/x cm</label>
                                    <input id="txtTallaExamenFisico" type="text" class="form-control" maxlength="6" onkeydown="validarDecimales(event)" placeholder="Talla/x cm" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtPresionArterialExamenFisico">PA/x mmHg</label>
                                    <input id="txtPresionArterialExamenFisico" type="text" onblur="validarValorPresionArterial(this)" class="form-control" maxlength="7" placeholder="PA/x mmHg" />
                                </div>
                                <div class="col-md-1">
                                    <label for="txtFrecuenciaCardiacaExamenFisico">FC/x min.</label>
                                    <input id="txtFrecuenciaCardiacaExamenFisico" type="number" class="form-control" placeholder="FC/x min" />
                                </div>
                                <div class="col-md-1">
                                    <label for="txtFrecuenciaRespiratoriaExamenFisico">FR/x min.</label>
                                    <input id="txtFrecuenciaRespiratoriaCardiacaExamenFisico" type="number" class="form-control" placeholder="FR/x min" />
                                </div>
                                <div class="col-md-1">
                                    <label for="txtTemperaturaExamenFisico">T°</label>
                                    <input id="txtTemperaturaExamenFisico" type="text" maxlength="4" onkeydown="validarDecimales(event)" class="form-control" placeholder="T°" />
                                </div>
                            </div>

                            <div id="divExamenFisicoPediatria" class="row mt-3">
                                <div class="col-md-2">
                                    <label for="txtTannerExamenFisico">Tanner</label>
                                    <input id="txtTannerExamenFisico" type="number" class="form-control" placeholder="Tanner" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtDenticionExamenFisico">Dentición</label>
                                    <input id="txtDenticionExamenFisico" type="text" class="form-control" placeholder="Dentición" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtPArtExamenFisico">P art.</label>
                                    <input id="txtPArtExamenFisico" type="text" class="form-control" placeholder="P art." />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtSaturometriaExamenFisico">Saturometría</label>
                                    <input id="txtSaturometriaExamenFisico" type="text" class="form-control" placeholder="Saturometría" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtScoreExamenFisico">Score</label>
                                    <input id="txtScoreExamenFisico" type="number" class="form-control" placeholder="Score" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label for="txtExamenFisico">Información adicional examen físico</label>
                                    <textarea id="txtExamenFisico" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="600"
                                        placeholder="Escriba información adicional del exámen físico">
                                    </textarea>

                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>Examen segmentario</label>
                                    <textarea id="txtExamenFisicoSegmentado" style="width: 100%; height: 400px;" placeholder="&nbsp;" class=""
                                        data-required="true">
                                    </textarea>
                                </div>
                            </div>

                            <h6 class="mb-3 mt-3"><strong>Hipotesis diagnóstica o Diagnóstico de Ingreso</strong></h6>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtDiagnostico">Diagnóstico</label>
                                    <textarea id="txtDiagnostico" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="600"
                                        placeholder="Escriba el diagnósitco">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>Diagnóstico (CIE-10)</label>
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
                                            <div class="typeahead__query">
                                                <input id="txtDiagnosticoCIE10" name="hockey_v1[query]" type="search" placeholder="Buscar Diagnóstico CIE-10" autocomplete="off"
                                                    onclick="this.select();">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="tblDiagnosticos" class="table table-hover">
                                            <thead class="thead-dark">
                                                <tr class="text-center">
                                                    <th scope="col">Descripción Diagnóstico CIE-10</th>
                                                    <th scope="col" class="text-center">Quitar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txtPlanManejo">Plan de Manejo</label>
                                    <textarea id="txtPlanManejo" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="600"
                                        placeholder="Escriba el plan de manejo">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary" onclick="IrAnterior();"><i class="fa fa-arrow-left"></i>Anterior</a>
                                    <a class="btn btn-primary" onclick="IrSiguiente();" id="btn-siguiente-IM">Siguiente <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>

                        </div>

                        <!-- UCI -->

                        <div id="divUCI" data-id="6" class="tab-pane fade p-4" role="tabpanel" aria-labelledby="profile-tab-just">

                            <h5></h5>

                            <div class="row">
                                <input type="hidden" id="idUciEditar" />
                                <div class="col-md-4">
                                    <label for="sltEscalaRankin">Escala Rankin</label>
                                    <select id="sltEscalaRankin" class="form-control" data-required="true">
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="txtApache2">Apache II</label>
                                    <input id="txtApache2" type="number" class="form-control text-center" data-required="true" placeholder="N° Apache II" />
                                </div>

                                <div class="col-md-2">
                                    <label for="txtMortalidad">Mortalidad</label>
                                    <input id="txtMortalidad" type="text" class="form-control text-center" disabled="disabled" placeholder="&nbsp;" />
                                </div>

                                <div class="col-md-2 center-vertical">
                                    <a class="btn btn-info" href="https://www.mdcalc.com/apache-ii-score" target="_blank">
                                        <i class="fa fa-calculator"></i>Calculadora
                                    </a>
                                </div>

                                <div class="col-md-2">

                                    <label>Vía aerea dificil</label><br />

                                    <div class="bootstrapSwitch" data-name="ViaAeraDificil">
                                        <input id="checkViaAerea" type="checkbox" />
                                        <%--<input id="rdoViaAeraDificilNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="ViaAeraDificil" data-size="normal"
                                            class="bootstrapSwitch" />--%>
                                    </div>

                                </div>

                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary btn-anterior" onclick="IrAnterior();"><i class="fa fa-arrow-left"></i>Anterior</a>
                                </div>
                            </div>

                        </div>

                        <!-- TÉRMINO UCI -->

                        <!-- GINECOLOGÍA -->

                        <div id="divGinecología" class="tab-pane fade p-4" data-id="2" role="tabpanel" aria-labelledby="profile-tab-just">

                            <h5></h5>

                            <div class="row">
                                <div class="col-md-2">
                                    <label for="txtFechaUltimaRegla">FUR</label>
                                    <input id="txtFechaUltimaRegla" type="date" class="form-control" data-required="true" />
                                </div>
                                <div class="col-md-2">
                                    <label for="sltMetodoAnticonceptivo">Método anticonceptivo</label>
                                    <select id="sltMetodoAnticonceptivo" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="txtGestaciones">G</label>
                                            <input id="txtGestaciones" type="number" class="form-control" data-required="true" placeholder="Gestaciones" />
                                        </div>
                                        <div class="col-md-4">
                                            <label for="txtPartos">P</label>
                                            <input id="txtPartos" type="number" class="form-control" data-required="true" placeholder="Partos" />
                                        </div>
                                        <div class="col-md-4">
                                            <label for="txtAbortos">A</label>
                                            <input id="txtAbortos" type="number" class="form-control" data-required="true" placeholder="Abortos" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <label for="txtCiclos">Ciclos</label>
                                    <input id="txtCiclos" type="text" class="form-control" data-required="true" maxlength="30" placeholder="Escriba los ciclos" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="txtPapanicolaou">Papanicolaou</label>
                                    <input id="txtPapanicolaou" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Escriba papanicolaou" />
                                </div>
                                <div class="col-md-6">
                                    <label for="txtMamografia">Mamografía</label>
                                    <input id="txtMamografia" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Escriba mamografía" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <label for="divEco" class="mr-3">Ecografía TV</label>

                                    <div id="divEco" class="custom-control custom-radio custom-control-inline">
                                        <input id="rdoEcoNormal" type="radio" class="custom-control-input" name="rdoEcografia" checked>
                                        <label class="custom-control-label" for="rdoEcoNormal">Normal</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input id="rdoEcoAnormal" type="radio" class="custom-control-input" name="rdoEcografia">
                                        <label class="custom-control-label" for="rdoEcoAnormal">Anormal</label>
                                    </div>

                                    <div id="divDescripcionEcoAnormal" style="display: none;">
                                        <label for="txtDescripcionEcoAnormal">Descripción Ecografía Anormal</label>
                                        <input id="txtDescripcionEcoAnormal" type="text" class="form-control" data-required="false" maxlength="10"
                                            placeholder="Escriba la descripción ecografía anormal" />
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <label for="divEco" class="mr-3">Biopsia</label>

                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="chbPipelleBiopsia">
                                        <label class="custom-control-label" for="chbPipelleBiopsia">Pipelle</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="chbPolipoBiopsia">
                                        <label class="custom-control-label" for="chbPolipoBiopsia">Pólipo</label>
                                    </div>

                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary" onclick="IrAnterior();"><i class="fa fa-arrow-left"></i>Anterior</a>
                                </div>
                            </div>

                        </div>

                        <!-- TÉRMINO GINECOLOGÍA -->

                        <!-- OBSTETRICIA -->

                        <div id="divObstetricia" data-id="3" class="tab-pane fade p-4" role="tabpanel" aria-labelledby="profile-tab-just">

                            <h4>
                                <strong>Datos de Obstetricia</strong>
                            </h4>

                            <h5 class="mt-3">
                                <strong>1. Antecedentes Gineco-Obstétrico</strong>
                            </h5>

                            <div class="row mt-3">
                                <div class="col-md-2">
                                    <label for="txtFechaUltimaReglaOnstetricia">FUR</label>
                                    <input id="txtFechaUltimaReglaObstetricia" type="date" class="form-control" data-required="true" />
                                </div>
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="txtGestaciones">G</label>
                                            <input id="txtGestacionesObstetricia" type="number" class="form-control" data-required="true" placeholder="Gestaciones" />
                                        </div>
                                        <div class="col-md-4">
                                            <label for="txtPartos">P</label>
                                            <input id="txtPartosObstetricia" type="number" class="form-control" data-required="true" placeholder="Partos" />
                                        </div>
                                        <div class="col-md-4">
                                            <label for="txtAbortos">A</label>
                                            <input id="txtAbortosObstetricia" type="number" class="form-control" data-required="true" placeholder="Abortos" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">

                                    <label>Operacional</label><br />

                                    <div class="bootstrapSwitch" data-name="Operacional" data-required="true">

                                        <input id="rdoOperacionalSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Operacional" data-size="normal"
                                            class="bootstrapSwitch" />

                                        <input id="rdoOperacionalNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Operacional" data-size="normal"
                                            class="bootstrapSwitch" />

                                    </div>

                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2">
                                    <label for="sltTipoEmbarazo">Embarazo actual</label>
                                    <select id="sltTipoEmbarazo" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <label for="txtDescripcionEmbarazo">Descripción de Embarazo actual</label>
                                    <textarea id="txtDescripcionEmbarazo" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="30"
                                        placeholder="Escriba descripción de embarazo actual">
                                    </textarea>
                                </div>
                                <div class="col-md-5">
                                    <label for="txtOtrosAntecedentes">Otros antecedentes</label>
                                    <textarea id="txtOtrosAntecedentes" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="300"
                                        placeholder="Escriba otros antecedentes">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>Examen Físico Gineco-Obstétrico</label>
                                    <textarea id="txtExamenFisicoObstetricia" style="width: 100%; height: 400px;" placeholder="&nbsp;" class="" data-required="true">
                                    </textarea>
                                </div>
                            </div>

                            <h5 class="mt-3"><strong>2. Exámenes Complementarios</strong></h5>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>Ecografía Obstétrica</label>
                                    <textarea id="txtEcografiaObstetrica" style="width: 100%; height: 400px;" placeholder="&nbsp;" class=""
                                        data-required="true">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <label for="txtRbns">RBNS</label>
                                    <input id="txtRbns" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Registro basal no estresante" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtDu">DU</label>
                                    <input id="txtDu" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Dinámico uterino" />
                                </div>
                                <div class="col-md-2">
                                    <label for="txtPerfilBiofisico">Perfil Biofísico</label>
                                    <input id="txtPerfilBiofisico" type="text" class="form-control" data-required="true" maxlength="50" placeholder="Perfil biofísico" />
                                </div>
                                <div class="col-md-4">
                                    <label for="txtLaboratorio">Laboratorio</label>
                                    <input id="txtLaboratorio" type="text" class="form-control" data-required="true" maxlength="100" placeholder="Escriba descripción laboratorio" />
                                </div>
                            </div>

                            <h5 class="mt-3"><strong>3. Indicaciones</strong></h5>

                            <div class="row mt-3">
                                <div class="col-md-2">
                                    <label for="sltTipoReposo">Tipo reposo</label>
                                    <select id="sltTipoReposo" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtReposo">Reposo</label>
                                    <input id="txtReposo" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Escriba reposo" />
                                </div>
                                <div class="col-md-2">
                                    <label for="sltTipoRegimen">Tipo régimen</label>
                                    <select id="sltTipoRegimen" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtRegimen">Régimen</label>
                                    <input id="txtRegimen" type="text" class="form-control" data-required="true" maxlength="10" placeholder="Escriba régimen" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label for="txtPretaciones">Prescripciones</label>
                                    <textarea id="txtPretaciones" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="300" placeholder="Escriba las prescripciones">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <label for="txtCsv">CSV</label>
                                    <input id="txtCsv" type="number" class="form-control" data-required="true" placeholder="Control signos vitales" />
                                </div>
                                <div class="col-md-3">
                                    <label for="txtHgt">HGT</label>
                                    <input id="txtHgt" type="text" class="form-control" data-required="true" maxlength="50" placeholder="Hemoglucotest" />
                                </div>
                                <div class="col-md-6">
                                    <label for="txtControlObstetrico">Control Obstétrico</label>
                                    <input id="txtControlObstetrico" type="text" class="form-control" data-required="true" maxlength="50" placeholder="Escriba control obstétrico" />
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <label for="txtExamenes">Exámenes</label>
                                    <textarea id="txtExamenes" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="100" placeholder="Escriba los exámenes">
                                    </textarea>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtInterconsultas">Interconsultas</label>
                                    <textarea id="txtInterconsultas" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="100" placeholder="Escriba las interconsultas">
                                    </textarea>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtProcedimientos">Procedimientos</label>
                                    <textarea id="txtProcedimientos" class="md-textarea md-textarea-auto form-control" rows="5" data-required="true" maxlength="100" placeholder="Escriba los procedimientos">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary btn-anterior" onclick="IrAnterior();"><i class="fa fa-arrow-left"></i>Anterior</a>
                                </div>
                            </div>

                        </div>

                        <!-- TÉRMINO OBSTETRICIA -->

                        <!-- PEDIATRÍA -->

                        <div id="divPediatría" data-id="3" class="tab-pane fade p-4" role="tabpanel" aria-labelledby="profile-tab-just">

                            <h4><strong>Datos de Pediatría</strong></h4>

                            <%--<h5 class="mb-3 mt-3"><strong>1. Datos de responsable</strong></h5>--%>

                            <%--                            <div id="divResponsable">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Identificación</label>
                                        <select id="sltIdentificacionCuidador" class="form-control identificacion">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="sltIdentificacionCuidador"></label>
                                        <div class="input-group">
                                            <input id="txtnumeroDocCuidador" type="text" class="form-control numero-documento" style="width: 100px;">
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">
                                                    <strong>-</strong>
                                                </div>
                                            </div>
                                            <input id="txtDigCuidador" type="text" class="form-control digito text-center" maxlength="1" style="width: 15px;"
                                                placeholder="DV" disabled />
                                            <a id="aBuscarPacienteCuidador" class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="txtnombreCuidador">Nombre del cuidador</label>
                                        <input id="txtnombreCuidador" type="text" class="form-control datos-persona" maxlength="50" data-required="false"
                                            disabled="disabled" placeholder="Escriba nombre del cuidador" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtApePatCuidador">Primer Apellido</label>
                                        <input id="txtApePatCuidador" type="text" class="form-control datos-persona" maxlength="50" data-required="false"
                                            disabled="disabled" placeholder="Escriba primer apellido del cuidador" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtApeMatCuidador">Segundo Apellido</label>
                                        <input id="txtApeMatCuidador" type="text" class="form-control datos-persona" maxlength="50" disabled="disabled"
                                            placeholder="Escriba segundo apellido del cuidador" />
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-2">
                                        <label for="txtparentescoCuidador">Parentesco</label>
                                        <input id="txtparentescoCuidador" type="text" class="form-control  datos-persona" maxlength="50" data-required="false" disabled="disabled"
                                            placeholder="Escriba parentesco" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtTelefonoCuidador">Teléfono</label>
                                        <input id="txtTelefonoCuidador" type="text" class="form-control datos-persona" maxlength="50" data-required="false" disabled="disabled"
                                            placeholder="Escriba teléfono" />
                                    </div>
                                    <div class="col-md-8">
                                        <label for="txtDireccionCuidador">Dirección</label>
                                        <input id="txtDireccionCuidador" type="text" class="form-control datos-persona" maxlength="50" data-required="false" disabled="disabled"
                                            placeholder="Escriba dirección" />
                                    </div>
                                </div>
                            </div>--%>

                            <h5 class="mb-3 mt-3"><strong>1. Antecedentes personales</strong></h5>
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="sltRecienNacido">Tipo recién nacido</label>
                                    <select id="sltRecienNacido" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="sltParto">Parto</label>
                                    <select id="sltParto" class="form-control" data-required="true">
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <label for="txtPatologiaPerinatal">Patología perinatal</label>
                                    <textarea id="txtPatologiaPerinatal" class="form-control" rows="5" maxlength="300" placeholder="Escriba patología perinatal">
                                    </textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="txtOtrosPediatria">Otros</label>
                                    <textarea id="txtOtrosPediatria" class="form-control" rows="5" maxlength="300" placeholder="Escriba otras patologías perinatales">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-3">

                                    <label>Vacuna</label>
                                    <div class="input-group">
                                        <select id="sltVacunasPaciente" class="form-control">
                                        </select>
                                        <a id="aAgregarPaciente" class="btn btn-info ml-2"><i class="fa fa-plus mr-1"></i>Agregar</a>
                                    </div>

                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <table id="tblVacunasPaciente" class="table table-hover">
                                        <thead class="thead-dark">
                                            <tr class="text-center">
                                                <th scope="col">Edad</th>
                                                <th scope="col">Nombre vacuna</th>
                                                <th scope="col">Descripción vacuna</th>
                                                <th scope="col" class="text-center">Quitar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div id="divVacunasPaciente" class="alert alert-info text-center">
                                        <i class="fa fa-exclamation-triangle"></i>Este paciente no tiene vacunas ingresadas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>Duración L. materna (Edad)</label>
                                    <div class="input-group">
                                        <input id="txtDuracionLMaterna" type="number" class="form-control" placeholder="Edad" />
                                        <select id="sltTipoEdadDuracionLMaterna" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>Inicio L. artificial (Edad)</label>
                                    <div class="input-group">
                                        <input id="txtInicioLArtificial" type="number" class="form-control" placeholder="Edad" />
                                        <select id="sltInicioLArtificial" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="txt1Comida">Fecha 1° comida</label>
                                    <input id="txt1Comida" type="date" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label for="txt2Comida">Fecha 2° comida</label>
                                    <input id="txt2Comida" type="date" class="form-control" />
                                </div>
                            </div>

                            <h5 class="mb-3 mt-3"><strong>2. Antecedentes familiares</strong></h5>

                            <div class="row">
                                <div class="col-md-2">
                                    <label>Alergias</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoAlergiasSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Alergias" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoAlergiasNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Alergias" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Asma</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoAsmaSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Asma" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoAsmaNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Asma" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Epilepsia</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoEpilepsiaSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Epilepsia" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoEpilepsiaNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Epilepsia" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Diabetes</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoDiabetesSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Diabetes" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoDiabetesNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Diabetes" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Cardiovasc.</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoCardiovascSi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="Cardiovasc" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoCardiovascNo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="Cardiovasc" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>HTA</label><br />
                                    <div class="bootstrapSwitch">
                                        <input id="rdoHTASi" type="radio" data-on-text="SI" data-off-text="SI"
                                            data-on-color="success" name="HTA" data-size="normal" class="bootstrapSwitch" />
                                        <input id="rdoHTANo" type="radio" data-on-text="NO" data-off-text="NO"
                                            data-on-color="warning" name="HTA" data-size="normal" class="bootstrapSwitch" />
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <label for="txtMuertesFamiliares">Muertes familiares</label>
                                    <textarea id="txtMuertesFamiliares" class="form-control" placeholder="Escriba muertes familiares" rows="4">
                                    </textarea>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtAlergiaFaramco">Alergía a fármacos</label>
                                    <textarea id="txtAlergiaFaramco" class="form-control" placeholder="Escriba alergía a fármacos" rows="4">
                                    </textarea>
                                </div>
                                <div class="col-md-4">
                                    <label for="txtOtrosAntecendentesPersonales">Otros</label>
                                    <textarea id="txtOtrosAntecendentesPersonales" class="form-control" placeholder="Escriba otros antecedentes familiares" rows="4">
                                    </textarea>
                                </div>
                            </div>

                            <h5 class="mb-3 mt-3"><strong>3. Hospitalización</strong></h5>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="tblHospitalizaciones" class="table table-hover w-100">
                                            <thead class="thead-dark">
                                                <tr class="text-center">
                                                    <th>Diagnóstico</th>
                                                    <th>Edad</th>
                                                    <th>Fecha ingreso</th>
                                                    <th>Fecha egreso</th>
                                                    <th>Días hospitalización</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="divHospitalizaciones" class="alert alert-info text-center">
                                        <i class="fa fa-exclamation-triangle"></i>Este paciente no tiene hospitalizaciones
                                    </div>
                                </div>
                            </div>

                            <h5 class="mb-3 mt-3"><strong>4. Información parental</strong></h5>

                            <h6 class="mb-3 mt-3"><strong>4.1. Información parental</strong></h6>

                            <div id="divParental1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Identificación</label>
                                        <select id="sltIdentificacionParental1" class="form-control identificacion">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="sltRutParental1">RUT</label>
                                        <div class="input-group">
                                            <input id="txtnumeroDocParental1" type="text" class="form-control numero-documento" style="width: 100px;">
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">
                                                    <strong>-</strong>
                                                </div>
                                            </div>
                                            <input type="text" id="txtDigParental1" class="form-control digito" maxlength="1" style="width: 15px"
                                                placeholder="DV" />
                                            <%--<a id="aBuscarPacienteParental1" class="btn btn-info"><i class="fa fa-search" onclick="findPacienteParental(this);"></i></a>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <label for="txtnombreParental1">Nombre</label>
                                        <input id="txtnombreParental1" type="text" class="form-control datos-persona" maxlength="50"
                                            data-required="false" disabled="disabled" placeholder="Escriba nombre" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtApePatParental1">Primer Apellido</label>
                                        <input id="txtApePatParental1" type="text" class="form-control datos-persona" maxlength="50"
                                            data-required="false" disabled="disabled" placeholder="Escriba primer apellido" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtApeMatParental1">Segundo Apellido</label>
                                        <input id="txtApeMatParental1" type="text" class="form-control datos-persona" maxlength="50"
                                            disabled="disabled" placeholder="Escriba segundo apellido" />
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-2">
                                        <label>Fecha Nacimiento</label>
                                        <input id="txtFechaNacimientoParental1" type="date" class="form-control datos-persona" disabled="disabled" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Edad</label>
                                        <input id="txtEdadParental1" type="text" class="form-control edad" disabled="disabled" placeholder="Edad" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>Actividad</label>
                                        <select id="sltActividadParental1" class="form-control datos-persona" disabled="disabled">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Educación</label>
                                        <select id="sltEducaciónParental1" class="form-control datos-persona" disabled="disabled">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <h6 class="mb-3 mt-4"><strong>4.2. Información parental</strong></h6>

                            <div id="divParental2">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Identificación</label>
                                        <select id="sltIdentificacionParental2" class="form-control identificacion">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="sltRutParental2">RUT</label>
                                        <div class="input-group">
                                            <input id="txtnumeroDocParental2" type="text" class="form-control numero-documento" style="width: 100px;">
                                            <div class="input-group-prepend digito">
                                                <div class="input-group-text">
                                                    <strong>-</strong>
                                                </div>
                                            </div>
                                            <input type="text" id="txtDigParental2" class="form-control digito" maxlength="1" style="width: 15px"
                                                placeholder="DV" />
                                            <%--<a id="aBuscarPacienteParental2" class="btn btn-info"><i class="fa fa-search" onclick="findPacienteParental(this);"></i></a>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <label>Nombre</label>
                                        <input id="txtnombreParental2" type="text" class="form-control datos-persona" maxlength="50"
                                            data-required="false" disabled="disabled" placeholder="Escriba nombre" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Primer Apellido</label>
                                        <input id="txtApePatParental2" type="text" class="form-control datos-persona" maxlength="50"
                                            data-required="false" disabled="disabled" placeholder="Escriba primer apellido" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Segundo Apellido</label>
                                        <input id="txtApeMatParental2" type="text" class="form-control datos-persona" maxlength="50"
                                            disabled="disabled" placeholder="Escriba segundo apellido" />
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-2">
                                        <label>Fecha Nacimiento</label>
                                        <input id="txtFechaNacimientoParental2" type="date" class="form-control datos-persona" disabled="disabled" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Edad</label>
                                        <input id="txtEdadParental2" type="text" class="form-control edad" placeholder="Edad" disabled="disabled" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>Actividad</label>
                                        <select id="sltActividadParental2" class="form-control datos-persona" disabled="disabled">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Educación</label>
                                        <select id="sltEducaciónParental2" class="form-control datos-persona" disabled="disabled">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <label>Situación marital</label>
                                    <select id="sltSituacionMarital" class="form-control">
                                    </select>
                                </div>
                            </div>

                            <h5 class="mb-3 mt-3"><strong>5. Otros datos</strong></h5>

                            <div class="row">
                                <div class="col-md-3">
                                    <label for="sltDSM">DSM</label>
                                    <select id="sltDSM" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="sltNutricional">Nutricional</label>
                                    <select id="sltNutricional" class="form-control">
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-primary btn-anterior" onclick="IrAnterior();"><i class="fa fa-arrow-left"></i>Anterior</a>
                                </div>
                            </div>

                        </div>

                        <!-- TÉRMINO PEDIATRÍA -->

                    </div>

                </div>
            </div>

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Profesional</strong></h5>
                </div>
                <div class="card-body">
                    <div id="divDatosProfesionales">

                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtNumeroDocumentoProfesional">Número Documento</label>
                                <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtNombreProfesional">Nombre/es</label>
                                <input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApePatProfesional">Primer Apellido</label>
                                <input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApeMatProfesional">Segundo Apellido</label>
                                <input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-md-4 center-vertical">
                    <h5 id="hFechaCreacion" style="margin-bottom: 0px !important;">Fecha Creación: <strong id="strFechaCreacion"></strong>
                    </h5>
                </div>
                <div class="col-md-8 text-right">
                    <a id="aGuardarIngresoMedico" class="btn btn-primary" onclick="GuardarIngresoMedico()" data-adjunto='true'>
                        <i class="fa fa-save"></i>Guardar
                    </a>
                    <a id="aCancelarIngresoMedico" class="btn btn-default" onclick="CancelarIngresoMedico()">
                        <i class="fa fa-remove"></i>Cancelar
                    </a>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="mdlIngresoExistente" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="mdlIngresoExistente" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width: 1000px; min-width: 1000px;">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Ingreso Médico</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info text-center" role="alert" style="margin-top: 10px;">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>El paciente ya cuenta con ingreso médico, si desea crear un ingreso nuevo haga click en <b>'Nuevo ingreso'</b>.
                    </div>
                    <table id="tblHistorialIngresosMedicos" class="table table-bordered table-hover w-100">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="btnNuevoIngresoExistente" class="btn btn-info">Nuevo ingreso</button>
                    <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/IngresoMedico/NuevoIngresoMedicoPediatria.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/IngresoMedico/NuevoIngresoMedicoObstetricia.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/IngresoMedico/NuevoIngresoMedicoGinecologia.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/IngresoMedico/NuevoIngresoMedicoUCI.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/IngresoMedico/NuevoIngresoMedico.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
