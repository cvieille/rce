﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaIaas.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.BandejaIaas" %>
<%@ Register Src="~/Vista/UserControls/ModalAislamiento.ascx" TagName="ModalAislamiento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <style>
        .small-box {
            margin-bottom: 0px;
        }

        .info-box {
            min-height: unset;
            padding: unset;
            margin-bottom: unset;
        }

        .badge-orange {
            color: #ffffff !important;
            background-color: orange !important;
        }

        .dropdown.bootstrap-select .dropdown-toggle {
            border: 1px solid lightgray;
        }
    </style>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalAislamiento ID="ModalAislamiento" runat="server" />
    <div class="row w-100 p-2">
        <div class="col-md-12">
            <button
                data-toggle="collapse" 
                class="btn btn-outline-primary container-fluid"
                type="button"
                data-target="#divFiltroIaas"
                onclick="return false;"
                aria-expanded="true">
                                FILTRAR BANDEJA
            </button>
                <div id="divFiltroIaas" class="card p-3 collapse show" style="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                 <label for="txtHosFiltroId">ID/NRO.</label>
                                 <input id="txtHosFiltroId" type="number" class="form-control" placeholder="De Hospitalización">
                            </div>
                            <div class="col-md-2">
                                 <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                 <select id="sltTipoIdentificacion" class="form-control"><option value="0">Seleccione</option><option value="1">RUT</option><option value="2">DNI</option><option value="3">PASAPORTE</option><option value="4">RUT MATERNO</option><option value="5">NN</option></select>
                            </div>
                            <div class="col-md-2">
                                 <label id="lblTipoIdentificacion" for="txtHosFiltroRut">RUT</label>
                                 <div class="input-group">
                                    <input id="txtHosFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del Paciente">
                                    <div class="input-group-prepend digito">
                                        <div class="input-group-text">-</div>
                                    </div>
                                    <input type="text" id="txtHosFiltroDV" class="form-control digito" style="width: 10px" disabled="">
                                  </div>
                             </div>
                             <div class="col-md-2">
                                <label for="txtHosFiltroNombre">Nombre</label>
                                <input id="txtHosFiltroNombre" type="text" class="form-control" placeholder="Del Paciente">
                             </div>
                             <div class="col-md-2">
                                <label for="txtHosFiltroApePaterno">Primer Apellido</label>
                                <input id="txtHosFiltroApePaterno" type="text" class="form-control" placeholder="Del Paciente">
                             </div>
                             <div class="col-md-2">
                                <label for="txtHosFiltroApeMaterno">Segundo Apellido</label>
                                <input id="txtHosFiltroApeMaterno" type="text" class="form-control" placeholder="Del Paciente">
                             </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 text-right">
                            <a id="btnHosFiltro" class="btn btn-success" onclick="obtenerAislamientos()"><i class="fa fa-search"></i>Buscar</a>
                            <a id="btnHosLimpiarFiltro" onclick="limpiarFiltros()" class="btn btn-warning"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <ul class="nav nav-tabs nav-justified md-tabs ml-0 mr-0" id="myTab" role="tablist">
                  <li class="nav-item" role="presentation" style="display: list-item;">
                    <a class="nav-link active" id="aislamiento-tab" data-toggle="tab" href="#aislamiento"  role="tab" aria-controls="aislamiento" aria-selected="true"><h3>Aislamientos</h3></a>
                  </li>
                  <li class="nav-item" role="presentation" style="display: list-item;">
                    <a class="nav-link" id="invasivo-tab" data-toggle="tab" href="#invasivo"  role="tab" aria-controls="invasivo" aria-selected="false"><h3>Invasivos</h3></a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                <%--Tab aislamientos--%>
                  <div class="tab-pane fade show active p-2" id="aislamiento" role="tabpanel" aria-labelledby="aislamiento-tab">
                      <table id="tblAislamientos" class="table table-striped table-bordered table-hover"></table>
                  </div>
                  <div class="tab-pane fade p-2" id="invasivo" role="tabpanel" aria-labelledby="invasivo-tab">
                       <table id="tblInvasivos" class="table table-striped table-bordered table-hover w-100"></table>
                  </div>
                </div>
            </div>
        </div>
    </div>
    


    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/BandejaIaas.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
