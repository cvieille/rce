﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoEgresoEnfermeria.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevoEgresoEnfermeria" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <titulo-pagina data-title="Formulario de Egreso Enfermería"></titulo-pagina>
    <wuc:Paciente ID="wucPaciente" runat="server" />
    <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />
    <div class="modal-body accordion" id="acordionContenidoEgreso">
        <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />
        <div class="accordion" id="accordionInfoEgresoEnfermeria">
            <div class="card">
                <div class="card-header bg-dark" data-toggle="collapse" data-target="#divInfoDestinoRetiro" aria-expanded="true" aria-controls="divInfoDestinoRetiro">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Información de destino y retiro</h5>
                        </div>
                        <div class="col-md-4 text-right">
                            <i class="fa-lg fa"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show" id="divInfoDestinoRetiro" aria-labelledby="headingOne">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="fechaEgresoEnfermeria">Fecha egreso</label>
                            <input type="date" class="form-control" data-required="true" onblur="validarFechaLimite(this, 7, 1)" id="fechaEgresoEnfermeria" value="" />
                        </div>
                        <div class="col-md-2 mr-2">
                            <label for="horaEgresoEnfermeria">Hora egreso</label>
                            <input type="time" class="form-control" id="horaEgresoEnfermeria" data-required="true" value="" />
                        </div>
                        <div class="form-check pl-0">
                            <label class="mr-2">Condición del paciente</label>
                            <br />
                            <input id="chkCondicionPaciente" type="checkbox" class="bootstrapSwitch" data-on-text="Vivo" data-off-text="Fallecido"
                                data-on-color="success" data-off-color="warning" data-size="naormal" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mt-2">
                            <div class="alert table-warning text-sm" role="alert">
                                <strong>NOTA:</strong> La fecha y hora de Egreso de Enfermería, corresponde a cuando el paciente se va del servicio.
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3" id="divDatosRetiroAltaEnfermeria">
                        <div class="col-md-4 col-sm-12">
                            <label>Destino</label>
                            <select class="form-control" id="sltTipoDestino" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <label>Retiro</label>
                            <select class="form-control" id="sltTipoRetiro" name="sltTipoRegistro">
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-8">
                            <label>Observaciones</label>
                            <textarea id="txtObservacionesAltaEnfermeria"
                                class="form-control" maxlength="300"
                                placeholder="Escriba aquí sus observaciones"
                                rows="4"
                                style="resize: none;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion" id="accordionInfoEntregaDocumentoEgreso">
            <div class="card">
                <div class="card-header bg-dark" data-toggle="collapse" data-target="#divInfoEntregaDocumentosEgreso" aria-expanded="true" aria-controls="divInfoEntregaDocumentosEgreso">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Entrega de documentos</h5>
                        </div>
                        <div class="col-md-4 text-right">
                            <i class="fa-lg fa"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show" id="divInfoEntregaDocumentosEgreso" aria-labelledby="headingOne">
                    <div class="col-md-12">
                        <div id="divEntregaDocumentosEgreso" class="row"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion" id="accordionEntregaDocumentosEgreso">
            <div class="card">
                <div class="card-header bg-dark" data-toggle="collapse" data-target="#divEntregaExamenesEgreso" aria-expanded="true" aria-controls="divEntregaExamenesEgreso">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Entrega de exámenes</h5>
                        </div>
                        <div class="col-md-4 text-right">
                            <i class="fa-lg fa"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show" id="divEntregaExamenesEgreso" aria-labelledby="headingOne">
                    <div class="col-md-12">
                        <div id="divExamenEgreso" class="row"></div>
                    </div>

                    <div class="col-md-12" id="divExamLabEgreso">
                        <div class="row mt-4">
                            <div class="col-md-10">
                                <label>Filtrar exámenes <b class="color-error">(*)</b></label>
                                <div class="typeahead__container">
                                    <div class="typeahead__field">
                                        <div class="typeahead__query">
                                            <input id="thExamenesLaboratorio" name="hockey_v1[query]" type="search" placeholder="Buscar más exámenes"
                                                autocomplete="off" onclick="this.select();">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label><br />
                                <button type="button" id="addElementTableExam" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                            </div>
                            <div class="col-md-12 m-1">
                                <table id="tblExamenesEgreso" class="table table-hover"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion" id="accordionCuidadosEnfermeriaEgreso">
            <div class="card">
                <div class="card-header bg-dark" data-toggle="collapse" data-target="#divCuidadosEnfermeriaEgresoEnf" aria-expanded="true" aria-controls="divEntregaExamenesEgreso">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="mb-0"><i class="fas fa-bed fa-lg pr-1"></i>Cuidados de enfermería</h5>
                        </div>
                        <div class="col-md-4 text-right">
                            <i class="fa-lg fa"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show" id="divCuidadosEnfermeriaEgresoEnf" aria-labelledby="headingOne">
                    <div class="col-md-12">
                        <div id="divCuidadosEnfermeriaEgreso" class="row"></div>
                    </div>

                    <div class="col-md-12 m-1">
                        <label>Observaciones</label>
                        <textarea id="txtObservacionesInvasivosDocumentoEgreso"
                            class="form-control"
                            maxlength="300"
                            placeholder="Registre si el paciente se retira con algún tipo de cáteter por indicación médica, y los cuidados de enfermería respectivos"
                            style="resize: none;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="realizarAltaEnfermeriaPaciente()">Egresar paciente</button>
            <button type="button" class="btn btn btn-outline-secondary" onclick="salirAbandejaHos()">Cerrar</button>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevoEgresoEnfermeria.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
