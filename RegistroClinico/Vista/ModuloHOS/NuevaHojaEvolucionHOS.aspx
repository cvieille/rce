﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaHojaEvolucionHOS.aspx.cs" Inherits="RegistroClinico.Vista.ModuloHOS.NuevaHojaEvolucionHOS" %>

<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosHospitalizacion.ascx" TagName="DatosHospitalizacion" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/Textbox.io/textboxio.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <div class="card card-body">
        <div class="card-header bg-light">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Hoja de Evolución"></titulo-pagina>
        </div>
        <div class="card-body p-0">

            <wuc:Paciente ID="wucPaciente" runat="server" />
            <wuc:DatosHospitalizacion ID="DatosHospitalizacion" runat="server" />

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de la atención</strong> </h5>
                </div>
                <div class="card-body p-2">
                    <h4 class="mb-3"><strong>Datos de Evolución</strong> </h4>

                    <div id="divHojaEvolucion">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtFechaEvolucion">Fecha</label>
                                <input id="txtFechaEvolucion" type="date" class="form-control" data-required="true" />
                            </div>
                            <div class="col-md-4">
                                <label for="sltUbicacion">Servicio</label>
                                <select id="sltUbicacion" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="txtCamaActual">Cama</label>
                                <input type="text" id="txtCamaActual" class="form-control" disabled="disabled" />
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="txtDiagnosticoPrincipal">Diagnóstico Principal</label>
                                <textarea id="txtDiagnosticoPrincipal" class="form-control" rows="4" style="resize: none;" data-required="true" maxlength="200"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="txtOtrosDiagnosticos">Diagnósticos Secundarios</label>
                                <textarea id="txtOtrosDiagnosticos" class="form-control" rows="4" style="resize: none;" maxlength="200"></textarea>
                            </div>
                        </div>
                        <br />

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label for="txtEvolucion">Evolución </label>
                                <textarea id="txtEvolucion" class="md-textarea md-textarea-auto form-control" rows="4" style="resize: none;" data-required="true" maxlength="125"></textarea>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label for="txtExamenFisico">Examen Físico</label>
                                <textarea id="txtExamenFisico" class="form-control" rows="4" style="resize: none;" data-required="true" maxlength="125"></textarea>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label><strong>Plan de Manejo y/o indicaciones </strong></label>
                                <textarea id="txtPlanManejo" class="form-control" rows="4" style="resize: none;" data-required="true" maxlength="125"></textarea>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label><strong>Descripcion </strong></label>
                                <textarea id="txtDescripcionHE" class="form-control" rows="4" style="resize: none;" data-required="true" maxlength="125"></textarea>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Profesional</strong> </h5>
                </div>
                <div class="card-body">
                    <div id="divDatosProfesionales">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtNumeroDocumentoProfesional" class="active">Número Documento</label>
                                <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label class="active" for="txtNombreProfesional">Nombre/es</label>
                                <input id="txtNombreProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApePatProfesional" class="active">Primer Apellido</label>
                                <input id="txtApePatProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label for="txtApeMatProfesional" class="active">Segundo Apellido</label>
                                <input id="txtApeMatProfesional" type="text" class="form-control" placeholder="&nbsp;" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Archivos adjuntos</strong></h5>
                </div>
                <div class="card-body">
                    <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div id="divFechaCreacion" class="col-md-4 center-vertical">
                    <h5 style="margin-bottom: 0px !important;">Fecha Límite de Edición: <strong id="strFechaCreacion"></strong>
                    </h5>
                </div>
                <div class="col-md-8 text-right">

                    <a id="aGuardarHojaEvolucion" class="btn btn-primary" onclick="GuardarGeneral()"
                        data-adjunto='true'>
                        <i class="fa fa-save"></i> Guardar
                    </a>

                    <a id="aCancelarHojaEvolucion" class="btn btn-default" onclick="CancelarHojaEvolucion()">
                        <i class="fa fa-remove"></i>Cancelar
                    </a>

                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloHOS/NuevaHojaEvolucionHOS.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
