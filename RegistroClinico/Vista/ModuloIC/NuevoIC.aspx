﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevoIC.aspx.cs" Inherits="RegistroClinico.Vista.ModuloIC.NuevoIC" %>

<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <%--<script src="<%= ResolveClientUrl("~/Script/Textbox.io/textboxio.js") %>"></script>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <div id="alertObligatorios" class="alert text-center alert-obligatorios">
        <b>Los campos marcados son obligatorios (*)</b>.
    </div>

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:mdlEventos ID="wucMdlEventos" runat="server" />
    <wuc:mdlPacienteEvento ID="wucMdlPacienteEvento" runat="server" />

    <div class="card card-body">
        <div class="card">
            <div class="card-header bg-light">
                <!-- Componente de titulo-->
                <titulo-pagina data-title="Interconsulta IntraHospitalaria"></titulo-pagina>
            </div>
            <div class="card-body p-0">
                <wuc:Paciente ID="wucPaciente" runat="server" />
                <div id="divEnviado" class="card">
                    <div class="card-header bg-dark">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Origen</strong> </h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Servicio de salud</label>
                                <select id="sltServicioSaludOrigen" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Establecimiento</label>
                                <select id="sltEstablecimientoOrigen" data-required="true" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Especialidad</label>
                                <select id="sltEspecialidadOrigen" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="row d-none" id="infoHosp">
                            <div class="col-md-12 text-center m-3">
                                <span style="font-size: 1em;" class="badge badge-pill badge-warning">
                                    <i class="fa fa-info-circle fa-lg"></i>
                                    Información: Este paciente está hospitalizado actualmente
                                </span>
                            </div>
                            <div class="col-md-2">
                                <label>Id hospitalización</label>
                                <input type="text" id="txtIdHosp" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-2">
                                <label>Fecha ingreso</label>
                                <input type="text" id="txtFechaHosp" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-2">
                                <label>Cama</label>
                                <input type="text" id="txtCamaHosp" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Ubicación</label>
                                <input type="text" id="txtUbicacionHosp" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Estado</label>
                                <input type="text" id="txtEstadoHosp" class="form-control" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="divDerivado">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de destino</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <!--<div class="col-md-4">
                                <label>Servicio de salud</label>
                                <select id="sltServicioSaludDestino" data-required="true" class="form-control">
                                </select>
                            </div>-->
                            <div class="col-md-4">
                                <label>Ámbito</label>
                                <select id="sltAmbito" data-required="true" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Especialidad</label>
                                <select id="sltEspecialidadDestino" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos clínicos</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row" id="divDatosClinicos">
                            <div class="col-md-4">
                                <label>Diagnóstico clínico</label>
                                <textarea id="txtDiagnostico" class="form-control" maxlength="125" rows="4" style="resize: none;" data-required="true"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Principal sintomatología</label>
                                <textarea id="txtSintomalogia" class="form-control" maxlength="125" rows="4" style="resize: none;" data-required="true"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Se solicita</label>
                                <textarea id="txtSolicitante" class="form-control" maxlength="125" rows="4" style="resize: none;" data-required="true"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos GES</strong> </h5>
                    </div>
                    <div class="card-body">

                        <div class="row" id="IcGes">
                            <div class="col-md-3">
                                <label style="margin-right: 5px;">¿Sospecha de la salud AUGE?</label>
                                <%--<select class="form-control" id="sltAuge" data-required="true">
                                    <option value="0">Seleccione</option>
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>--%>
                                <input type="checkbox" id="sltAuge" name="sltAuge" checked>
                                <%--<input id="chbAuge" type="checkbox" name="tipoFiltro" data-on-text="SI" data-off-text="NO" data-on-color="success" data-off-color="warning"
                                    data-size="normal" data-label="AUGE" />--%>
                            </div>
                        </div>

                        <div id="divAuge">
                            <div id="divICGes">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Tipo interconsulta</label>
                                        <select id="sltTipoInterconsulta" data-required="false" class="form-control">
                                        </select>
                                    </div>
                                    <div id="divOtrosGes" class="col-md-6">
                                        <label>Especifique</label>
                                        <input id="txtOtrosGes" type="text" class="form-control" maxlength="50" data-required="false" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Grupo patología GES</label>
                                        <select id="sltGrupoGes" data-required="false" class="form-control">
                                        </select>
                                    </div>
                                    <div id="divSubGruposGes" class="col-md-3">
                                        <label>Sub-Grupo patología GES</label>
                                        <select id="sltSubGrupoGes" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Fundamentos del diagnóstico</label>
                                        <textarea id="txtFundamentosDiagnosticoGes" rows="4" maxlength="150" class="form-control divICGes"
                                            style="resize: none;" data-required="false"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="txtTratamientosGes">Tratamiento e indicaciones</label>
                                        <textarea id="txtTratamientosGes" rows="4" maxlength="150" class="form-control divICGes"
                                            style="resize: none;" data-required="false"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="divDatosProfesionales" class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos profesional Médico</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Número documento</label>
                                <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Nombre/es</label>
                                <input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Primer Apellido</label>
                                <input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Segundo Apellido</label>
                                <input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                </div>

                <div id="divRespuesta" style="display: none;" class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i> Respuestas interconsulta</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12" id="divRespuestasAnteriores">

                            </div>
                            <div class="col-md-12">
                                <textarea id="txtRespuesta" style="width: 100%; height: 250px;" data-required="true" class="textboxio">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="divCargarArchivos" class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Archivos Adjuntos</strong> </h5>
                    </div>
                    <div class="card-body">
                        <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-md-4 center-vertical">
                        <h5 style="margin-bottom: 0px !important;">Fecha creación: <strong id="strFechaCreacion"></strong>
                        </h5>
                    </div>
                    <div class="col-md-8 text-right">
                        <a id="aGuardarIC" class="btn btn-info" href='#/' onclick="CrearInterconsulta(this)" data-adjunto='true'>
                            <i class="fa fa-save"></i>&nbsp;Guardar
                        </a>
                        <a id="aCancelarInterconsulta" href='#/' class="btn btn-secondary" onclick="CancelarIC()">
                            <i class="fa fa-remove"></i>Cancelar
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/NuevoIC.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/Interconsulta.combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>

</asp:Content>
