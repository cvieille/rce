﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaIC.aspx.cs" Inherits="RegistroClinico.Vista.BandejaIC" %>

<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHojasEvolucion.ascx" TagName="HojaEvolucion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <link rel="stylesheet" href="../../Style/bootstrap-switch.css" />
    <link rel="stylesheet" href="../../Style/select2/select2.css">
    <link rel="stylesheet" href="../../Style/select2/select2-bootstrap4.css">
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/select2.full.js") %>"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#lnbExportar').click(function () {
                var tblObj = $('#tblIC').DataTable();
                __doPostBack("<%= lnbExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />

    <input type="hidden" id="idIC" value="0" />

    <!-- Componente de titulo-->
    <titulo-pagina data-title="Interconsulta"></titulo-pagina>


    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Bandeja Interconsulta</h3>
                </div>
                <div class="card-body">
                    <div class="post">
                        <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                            data-target="#divForm" onclick="return false;">
                            FILTRAR BANDEJA</button>
                        <div id="divForm" class="collapse" style="margin: 20px;">
                            <h5>Paciente</h5>
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                    <select id="sltTipoIdentificacion" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label id="lblTipoIdentificacion" for="txtFiltroRut">Rut</label>
                                    <div class="input-group">
                                        <input id="txtFiltroRut" type="text" class="form-control" style="width: 110px;" />
                                        <div class="input-group-prepend digito">
                                            <div class="input-group-text">-</div>
                                        </div>
                                        <input type="text" id="txtHosFiltroDV" class="form-control digito" style="width: 10px" disabled />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Nombre</label>
                                    <input id="txtFiltroNombre" type="text" maxlength="50" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Primer Apellido</label>
                                    <input id="txtFiltroApe" type="text" maxlength="50" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Segundo Apellido</label>
                                    <input id="txtFiltroSApe" type="text" maxlength="50" class="form-control" />
                                </div>
                            </div>
                            <h5>Datos clínicos</h5>
                            <div class="row" id="divFiltrosFechas">
                                <div class="col-md-3">
                                    <label>Especialidad origen</label>
                                    <select id="ddlEspecialidadOrigen" class="form-control select2">
                                    </select>
                                </div>
                                <div class="col-md-3" id="">
                                    <label>Especialidad Destino</label>
                                    <select id="ddlEspecialidadDestino" class="form-control select2">
                                    </select>
                                </div>
                                <div class="col-md-3" id="">
                                    <label>Estado de la interconsulta</label>
                                    <select id="sltFiltroEstadoIc" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3 d-none" id="divSltFiltroEspecialista">
                                    <label>Especialista responsable</label>
                                    <select id="sltEspecialistaFiltro" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3" id="">
                                    <label>Fecha ingreso</label>
                                    <input type="date" id="txtFechaIngreso" placeholder="dd-mm-yyy" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Fecha cierre</label>
                                    <input type="date" placeholder="dd-mm-yyy" id="txtFechaCierre" class="form-control">
                                </div>
                                <div class="col-md-3" id="">
                                    <label for="chkMisInterconsultas">
                                        Mis interconsultas
                                    </label>
                                    <br />
                                    <input class="form-check-input" type="checkbox" value="" id="chkMisInterconsultas" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="success" data-off-color="warning">
                                </div>
                                <div class="col-md-2">
                                    <label>¿Interconsulta GES?</label>
                                    <br />
                                    <input type="checkbox" id="switchGES" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                                <div class="col-md-4" id="divAuge" style="display: none;">
                                    <label>Grupo AUGE</label>
                                    <select id="sltGrupoGes" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-3" id="divSubAuge" style="display: none;">
                                    <label>Subgrupo o subproblema de salud AUGE</label>
                                    <select id="sltSubGrupoGes" class="form-control">
                                        <option value="0">-Seleccione-</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">



                                <div id="divProfesional" class="col-md-3" style="display: none;">
                                    <label>Profesional médico</label>
                                    <select id="ddlMedicoIC" class="form-control">
                                    </select>
                                </div>


                            </div>
                            <div class="text-right mt-2">
                                <button type="button" id="btnFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</button>
                                <button id="btnLimpiarFiltro" class="btn btn-secondary text-white"><i class="fa fa-eraser"></i>Limpiar filtros</button>
                            </div>

                        </div>
                        <div class="post">
                            <div class="text-right mt-2">
                                <asp:Button runat="server" ID="lnbExportarH" OnClick="lnbExportarH_Click" Style="display: none;" />
                                <a id="lnbExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                    <i class="fa fa-file-excel"></i>Exportar XLS
                                </a>
                                <a id="lnbNuevoIPD" class="btn btn-info" href="NuevoIC.aspx">
                                    <i class="fa fa-plus"></i>Nueva interconsulta
                                </a>
                                <a id="btnRefrescar" class="btn btn-primary" onclick="refrescarTabla()"><i class="fa fa-refresh"></i>Refrescar</a>
                            </div>
                        </div>
                        <div class="post" style="">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <table id="tblIC" class="table table-bordered table-hover" style="width: 100%"></table>
                                </div>
                            </div>
                        </div>
                        <div class="card-body row" style="min-height: 80px;">
                            <div class="col">
                                <div>
                                      <button class="btn 
                                        btn-outline-primary btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Interconsulta ingresada">
                                        <b>I</b>
                                        </button>
                                        Interconsulta con respuesta.
                                        <button class="btn 
                                        btn-outline-success btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Interconsulta con respuesta">
                                        <b>R</b>
                                        </button>
                                        Interconsulta con respuesta.
                                        <button class="btn 
                                        btn-outline-danger btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Interconsulta cerrada">
                                        <b>C</b>
                                        </button>
                                        Interconsulta cerrada.
                                <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: #FFF8D9; border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                                    Interconsulta sin asignar.
                                &nbsp; <i class="fa fa-info-circle fa-lg latidos" style="color: green;"></i>&nbsp;Click en la fila para más información
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Control de usuario moviemientos de sistema-->
    <wuc:ModalGenericoMovimientosSistema ID="ModalGenericoMovimientosSistema" runat="server" />

    <div class="modal fade" id="mdlEspecialidad" role="dialog" aria-labelledby="mdlEspecialidad" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Cambiar Especialidad</p>
                </div>
                <div class="modal-body">
                    <div class="row" id="divEspecialidades">
                        <div class="col-sm-12">
                            <h4><b>Id interconsulta:</b>  <span id="spanIdIc"></span></h4>
                            <h4><b>Nombre paciente:</b>  <span id="spanNombrePaciente"></span></h4>
                            <h4><b> Rut paciente:</b> <span id="spanRutPaciente"></span></h4>
                        </div>
                        <br />
                        <div class="col-sm-12 mt-3"><h3>Seleccione nuevo destino:</h3></div>
                        <div class="col-sm-6">
                            <div class="md-form">
                                <label class="active">Especialidad de origen</label>
                                <div style="height: 10px;">&nbsp;</div>
                                <select id="ddlEspecialidad1" class="form-control" data-required="true" disabled>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="md-form">
                                <label class="active">Nueva especialidad destino</label>
                                <div style="height: 10px;">&nbsp;</div>
                                <select id="ddlEspecialidad2" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnAceptarEsp" type="button" class="btn btn-primary">Aceptar</button>
                    <button class="btn btn-danger" type="button" onclick="$('#mdlEspecialidad').modal('hide'); return false;">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimir" role="dialog" aria-labelledby="mdlImprimir" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameIC" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlArchivosAdjuntos" class="modal fade" role="dialog" aria-labelledby="mdlArchivosAdjuntos"
        aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong>Archivos Adjuntos</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div id="mdlAsignarEspecialista" class="modal fade" role="dialog" aria-labelledby="mdlArchivosAdjuntos"
        aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong>Asigne esta interconsulta a algun especialista</strong>
                    </p>
                </div>
                <div class="modal-body row">
                    <div class="col-md-12">
                        <h5>
                            <b>Id interconsulta:</b>
                            <span id="idICAE"></span> 
                            &nbsp;&nbsp; 
                            <b>Solicitada por:</b>
                            <span id="profSolicitaAE"></span>
                        </h5>
                        <h5><b>Nombre paciente:</b><span id="nombrePacienteAE"></span></h5>
                        <h5><b>Diagnóstico:</b> <span id="diagnosticoAE"></span></h5>
                        <h5><i class="fa fa-info-circle fa-lg" style="color:green;"></i> El siguiente listado contiene los profesionales de: <b><span id="espDestinoAE"></span></b></h5>
                    </div>
                    <div class="col-md-12 mt-2" id="divSeleccionEspecialista">
                        <label>Seleccione especialista </label>
                        <select class="form-control" name="sltNombreEspecialista" id="sltEspecialista" data-required="true">
                        </select>
                    </div>
                    <div class="col-md-12 mt-2">
                        <button type="button" class="btn btn-primary" id="btnAsignarEspecialista" onclick="asignarEspecialistaSeleccionado()">Asignar</button>
                        <button type="button" onclick=" $('#mdlAsignarEspecialista').modal('hide');" class="btn btn-secondary">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="mdlCerrar" role="dialog" aria-labelledby="mdlRechazar" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong>Cerrar interconsulta</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row" id="divCierreIc">
                        <div class="col-md-12">
                            <p><b>Nombre paciente:</b> <span id="txtNombrePaciente"></span></p>
                            <p>
                                <b>Origen:&nbsp;</b><span id="txtOrigenSolicitud"></span>
                                <b>Destino:&nbsp;</b> <span id="txtDestinoSolicitud"></span>
                            </p>
                            <p><b>Estado:</b> <span id="txtEstado"></span></p>
                        </div>
                        <div class="alert alert-warning col-md-12 w-100" role="alert" style="color: #856404 !important; background-color: #fff3cd !important; border-color: #ffeeba !important;">
                            <p class="mb-0"><i class="fa fa-info-circle fa-lg"></i>&nbsp;Una vez cerrada esta interconsulta no podrá realizar cambios</p>
                        </div>
                        <div class="col-md-12">
                            <label>Motivo de cierre</label>
                            <select class="form-control" id="motivoCierre" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label>Observaciones de cierre</label>
                            <textarea class="form-control" id="txtMotivoCierre" style="resize: none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnAnular" class="btn btn-danger" onclick="return false;">Cerrar interconsulta</button>
                    <button type="button" class="btn btn-default" onclick=" $('#motivoCierre').val(0); $('#txtMotivoCierre').val(''); $('#mdlCerrar').modal('hide')">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/BandejaIC.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/Interconsulta.combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloIC/BandejaIC.links.js") + "?v=" + GetVersion() %>"></script>
    <wuc:HojaEvolucion ID="mdlHojaEvolucion" runat="server" />

</asp:Content>
