﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista
{
    public partial class BandejaIC : Handlers.PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lnbExportarH_Click(object sender, EventArgs e)
        {

            string sCommand = Request.Form["__EVENTARGUMENT"];



            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);
            List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();

            for (int i = 0; i < des.Count; i++)
            {
                Dictionary<string, string> d = new Dictionary<string, string>();
                d["ID IC"] = des[i][0].ToString();
                d["Fecha de ingreso"] = des[i][1].ToString();
                d["Paciente"] = des[i][2].ToString();
                d["Estado"] = des[i][3].ToString();
                d["Origen IC"] = des[i][4].ToString();
                d["Prof. origen"] = des[i][5].ToString();
                d["Destino IC"] = des[i][6].ToString();
                d["Prof destino"] = des[i][7].ToString();
                d["Diagnóstico"] = des[i][8].ToString();
                d["Síntomas"] = des[i][9].ToString();
                d["Solicitud"] = des[i][10].ToString();
                d["Espera"] = des[i][11].ToString();
                d["Ambito"] = des[i][12].ToString();
                d["Patologia GES"] = des[i][13].ToString();
                d["Respuesta"] = des[i][16].ToString() ?? "N/A";
                d["Motivo cierre"] = des[i][28].ToString();
                d["Observacion cierre"] = des[i][29].ToString();
                dic.Add(d);
            }

            XLWorkbook workbook = new XLWorkbook();
            Funciones.CrearHojaExcel(ref workbook, "Bandeja IC", dic);
            Funciones.ExportarTablaClosedXml(this.Page, "Bandeja IC", workbook);
        }

        protected void lnbExportarMovH_Click(object sender, EventArgs e)
        {
            string sCommand = Request.Form["__EVENTARGUMENT"];

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

            List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
            for (int i = 0; i < des.Count; i++)
            {
                Dictionary<string, string> d = new Dictionary<string, string>();
                d["Fecha Movimiento"] = des[i][1].ToString();
                d["Usuario"] = des[i][2].ToString();
                d["Movmiento"] = des[i][3].ToString();
                dic.Add(d);
            }

            XLWorkbook workbook = new XLWorkbook();
            Funciones.CrearHojaExcel(ref workbook, "Movimientos IC", dic);
            Funciones.ExportarTablaClosedXml(this.Page, "Movimientos IC", workbook);
        }
    }
}