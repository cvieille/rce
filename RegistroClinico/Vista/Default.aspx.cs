﻿using RegistroClinico.Negocio;
using System;

namespace RegistroClinico
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.RemoveAll();
                Application.RemoveAll();
                //Funciones.RemoveAllCookies(HttpContext.Current);
            }
        }
        public string GetVersion()
        {
            return Funciones.sVersion;
        }

        public string GetUrlWebApi()
        {
            return FuncionesAppSetting.GetUrlWebApi();
        }
    }
}