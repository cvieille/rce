﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Services;

namespace RegistroClinico.Vista.ModuloExamenes
{
    public partial class NuevaSolicitud : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static string getExamenes()
        {
            List<Dictionary<string, string>> jsonList = new List<Dictionary<string, string>>();
            ConexionSQL con = new ConexionSQL();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM GEN_Arancel WHERE GEN_grupoArancel = '03' AND GEN_vigenciaArancel = 'Vigente'");

            DataTable dt = con.GetDataTable(sb.ToString());
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dtr in dt.Rows)
                {
                    Dictionary<string, string> json = new Dictionary<string, string>
                    {
                        ["GEN_idArancel"] = dtr["GEN_idArancel"].ToString(),
                        ["GEN_glosaArancel"] = dtr["GEN_prestacionArancel"].ToString() + " " + dtr["GEN_glosaArancel"].ToString()
                    };
                    jsonList.Add(json);
                }
            }
            return JsonConvert.SerializeObject(jsonList);
        }

        [WebMethod]
        public static int insertExamen(string json)
        {
            JObject jsonObj = JObject.Parse(json);
            Dictionary<object, object> dictionary = jsonObj.ToObject<Dictionary<object, object>>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO RCE_Solicitud_Examen(RCE_fechaSolicitud_Examen, GEN_idPaciente, GEN_idServicio, GEN_idUsuarioSolicitante, RCE_diagnosticoSolicitud_Examen,");
            sb.AppendLine("GEN_idUsuarioMuestra, RCE_estadoSolicitud_Examen) OUTPUT INSERTED.RCE_idSolicitud_Examen VALUES(");
            sb.AppendLine("'" + dictionary["fecha"].ToString() + "',");
            sb.AppendLine(dictionary["idPaciente"].ToString() + ",");
            sb.AppendLine(dictionary["idServicio"].ToString() + ",");
            sb.AppendLine(dictionary["idSolicitante"].ToString() + ",");
            sb.AppendLine("'" + dictionary["diagnostico"].ToString() + "',");
            sb.AppendLine(dictionary["idMuestra"].ToString() + ",");
            sb.AppendLine("'Activo'");
            sb.AppendLine(")");

            ConexionSQL con = new ConexionSQL();
            int iID = con.InsertQuery(sb.ToString(), true);


            foreach (JToken j in (JArray)dictionary["examenes"])
            {
                sb = new StringBuilder();
                sb.AppendLine("INSERT INTO RCE_Solicitud_Arancel(RCE_idSolicitud_Examen, GEN_idArancel, RCE_estadoSolicitud_Arancel) VALUES(");
                sb.AppendLine(iID.ToString() + ",");
                sb.AppendLine(j.ToString() + ",");
                sb.AppendLine("'Activo')");
                con.InsertQuery(sb.ToString());
            }

            return iID;
        }
    }
}