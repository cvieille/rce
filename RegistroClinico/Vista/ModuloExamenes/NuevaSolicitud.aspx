﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaSolicitud.aspx.cs" Inherits="RegistroClinico.Vista.ModuloExamenes.NuevaSolicitud" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet"/>
    <script src="<%= ResolveClientUrl("~/Script/jquery.autocomplete.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloExamenes/NuevaSolicitud.js") + "?v=" + GetVersion() %>"></script>
    <style>
        label:not(.form-check-label):not(.custom-file-label) {
            font-weight:400;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <input type="hidden" id="idProSol" value="0"/>
    <input type="hidden" id="idPac" value="0"/>
    <input type="hidden" id="idProToma" value="0"/>

    <div class="card card-body">
        <div class="card">
            <div class="card-body" id="divExamen">
                <p class="h4 text-center py-4">Solicitud de Exámenes de Laboratorio</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2" style="margin-top:-2px;">
                                <div class="md-form">
                                    <label class="active">Fecha</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtFecha" class="form-control" type="date" required/>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top:8px;">
                                 <div class="md-form clockpicker" data-placement="top" data-align="bottom" data-autoclose="true">
                                    <i class="far fa-clock prefix"></i>
                                    <input type="time" id="txtHora" required class="form-control" />
                                    <label class="active" for="txtHora">Hora</label>
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title">Datos Paciente</h5>                        
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <input id="txtRutPac" data-required="true" type="text" class="form-control" maxlength="10" onclick="$(this).select();"/>
                                    <label id="lblValidarRutPac" class="active" for="txtRutPac">RUT</label>
                                    <span style="color:crimson;" id="spanRutIncorrecto">
                                        <i class="fa fa-times"></i> <strong>Rut Incorrecto</strong>
                                    </span>
                                    <span style="color:darkgreen; display:none;" id="spanRutCorrecto">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoPac" class="form-control" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtUbInterna" class="active">N° Ubicación Interna</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtUbInterna" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePac" class="active">Nombre</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombrePac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form">
                                    <label for="txtApellidoPac" class="active">Primer Apellido</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtApellidoPac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="md-form">
                                    <label for="txtSapellidoPac" class="active">Segundo Apellido</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtSapellidoPac" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top:-2px;">
                                <div class="md-form">
                                    <label for="txtFechaNacPac" class="active">Fecha Nacimiento</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtFechaNacPac" disabled class="disabled form-control" type="date" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="ddlServicio" class="active">Servicio</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <select id="ddlServicio" class="selectpicker" 
                                        data-style="btn-primary-dark waves-effect" data-required="true"
                                        title="Seleccione">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label for="txtDiagnostico" class="active">Diagnóstico</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <textarea id="txtDiagnostico" class="md-textarea md-textarea-auto form-control" rows="1"></textarea>
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title">Profesional Solicitante</h5>
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <label id="lblValidarRutPro" class="active" for="txtRutPro">RUT</label>
                                    <input id="txtRutPro" data-required="true" placeholder="&nbsp;" type="text" class="form-control disabled" maxlength="10" onclick="$(this).select();"/>
                                    <span style="color:darkgreen;" id="spanRutCorrectoPro">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoPro" class="form-control disabled" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePro" class="active">Nombre Profesional</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombrePro" placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        
                        <h5 class="card-title">Exámenes</h5>
                        <div id="divExamenes">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label class="active">Examen</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtExamen" placeholder="Escriba el nombre del examen" type="text" class="form-control form-control-sm"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="md-form">
                                    <a id="lnbAgregarEx" class="btn btn-info waves-effect waves-light" href="#\">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <h5 class="card-title">Profesional Toma de Muestra</h5>
                        <div class="row">
                            <div class="col-md-2" style="margin-top:10px;">
                                <div class="md-form">
                                    <input id="txtRutProS" data-required="true" type="text" class="form-control" maxlength="10" onclick="$(this).select();"/>
                                    <label id="lblValidarRutProS" class="active" for="txtRutProS">RUT</label>
                                    <span style="color:crimson;" id="spanRutIncorrectoProS">
                                        <i class="fa fa-times"></i> <strong>Rut Incorrecto</strong>
                                    </span>
                                    <span style="color:darkgreen; display:none;" id="spanRutCorrectoProS">
                                        <i class="fa fa-check"></i> <strong>Rut Correcto</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:10px;">
                                <div class="md-form">
                                    <input type="text" data-required="true" id="txtRutDigitoProS" class="form-control" maxlength="1" style="width: 40px; text-align: center;"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="md-form">
                                    <label for="txtNombrePro" class="active">Nombre Profesional</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <input id="txtNombreProS" disabled placeholder="&nbsp;" class="disabled form-control" type="text" />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                 <div class="text-center py-3 mt-3">
                    <button id="btnConfirmar" class="btn btn-cyan" onclick="return false;" type="submit">CONFIRMAR</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>