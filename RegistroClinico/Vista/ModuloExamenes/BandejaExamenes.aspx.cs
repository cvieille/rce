﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Services;

namespace RegistroClinico.Vista.ModuloExamenes
{
    public partial class BandejaExamenes : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lnbExportarH_Click(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string getTabla(string sIdPro)
        {
            List<Dictionary<string, string>> jsonList = new List<Dictionary<string, string>>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select RCE_Solicitud_Examen.*, ");
            sb.AppendLine("gen_paciente.GEN_nombrePaciente + ' ' + isnull(gen_paciente.GEN_ape_paternoPaciente,'') + ' ' + isnull(gen_paciente.GEN_ape_maternoPaciente,'') nombrepaciente,");
            sb.AppendLine("GEN_Servicio.GEN_nombreServicio");
            sb.AppendLine("from RCE_Solicitud_Examen ");
            sb.AppendLine("inner join GEN_Paciente on GEN_Paciente.GEN_idPaciente = rce_solicitud_examen.gen_idpaciente");
            sb.AppendLine("inner join GEN_Servicio on GEN_Servicio.GEN_idServicio = rce_solicitud_examen.gen_idservicio");
            sb.AppendLine("where rce_estadoSolicitud_Examen = 'Activo' ");
            sb.AppendLine("and rce_solicitud_examen.gen_idusuariosolicitante = " + sIdPro);

            ConexionSQL conexionSQL = new ConexionSQL();

            DataTable dt = conexionSQL.GetDataTable(sb.ToString());
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dtr in dt.Rows)
                {
                    Dictionary<string, string> json = new Dictionary<string, string>
                    {
                        ["RCE_idSolicitud_Examen"] = dtr["RCE_idSolicitud_Examen"].ToString(),
                        ["RCE_fechaSolicitud_Examen"] = DateTime.Parse(dtr["RCE_fechaSolicitud_Examen"].ToString()).ToString("yyyy-MM-dd hh:mm:ss"),
                        //json["RCE_fechaSolicitud_Examen"] = dtr["RCE_fechaSolicitud_Examen"].ToString().Replace('/','-');
                        ["GEN_idPaciente"] = dtr["GEN_idPaciente"].ToString(),
                        ["GEN_idServicio"] = dtr["GEN_idServicio"].ToString(),
                        ["GEN_idUsuarioSolicitante"] = dtr["GEN_idUsuarioSolicitante"].ToString(),
                        ["RCE_diagnosticoSolicitud_Examen"] = dtr["RCE_diagnosticoSolicitud_Examen"].ToString(),
                        ["GEN_idUsuarioMuestra"] = dtr["GEN_idUsuarioMuestra"].ToString(),
                        ["nombrepaciente"] = dtr["nombrepaciente"].ToString(),
                        ["GEN_nombreServicio"] = dtr["GEN_nombreServicio"].ToString()
                    };
                    jsonList.Add(json);
                }
            }
            return JsonConvert.SerializeObject(jsonList);
        }
    }
}