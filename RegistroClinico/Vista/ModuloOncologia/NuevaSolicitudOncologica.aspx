﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaSolicitudOncologica.aspx.cs" Inherits="RegistroClinico.Vista.ModuloOncologia.NuevaSolicitudOncologica" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DiagnosticoCie10.ascx" TagName="Cie10" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/CargadoDeArchivos.ascx" TagName="CargadorArchivos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosProfesional.ascx" TagName="DatosProfesional" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <wuc:Paciente ID="wucPaciente" runat="server" />
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark">
                    Información de ingreso
                </div>
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-3 col-xs-12">
                            <label>Fecha de ingreso</label>
                            <input type="date" class="form-control" id="dpFechaIngreso" data-required="true" />
                         </div>
                        <div class="col-md-2 col-xs-12">
                            <label>Hora de ingreso</label>
                            <input type="time" class="form-control" id="dpHoraIngreso" data-required="true"/>
                        </div>
                        <div class="col-md-4 col-xs-8">
                            <label>Establecimiento de procedencia</label>
                            <select id="sltProcedencia" class="form-control">
                                <option>Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-xs-2">
                            <label>GES</label><br />
                            <input type="checkbox" id="checkGes" />
                            
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Resumen de caso clínico</label>
                            <textarea class="form-control" maxlength="250" style="resize:none; min-height:200px;" id="txtResumenCasoclinico"></textarea>
                        </div>
                    </div>
                    <!--Control de usuario diagnostico CIE10--->
                    <wuc:Cie10 ID="Cie10" runat="server" />
                    <div class="row">
                        <div class="col-md-6">
                            <label>Derivar a comité</label>
                            <select class="form-control" id="sltDerivaComite">
                                <option>Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label> <label>&nbsp;</label><br />
                            <buton class="btn btn-info" id="btnHistorial">Ver presentaciones previas</buton>

                            <buton class="btn btn-info" id="btnRCE">Registro clínico electrónico</buton>
                        </div>
                         <div class="col-md-6">
                            <label>ECOG</label>
                             <input type="text" maxlength="250" class="form-control" id="inputEcog" />
                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label><br />
                            <input type="text" id="estActual" value="Estado Actual" class="form-control" readonly>
                            <label>Plan de tratamiento propuesto</label>
                            <textarea class="form-control" maxlength="250" style="resize:none; min-height:200px;" id="txtPlanTratamientoPropuesto"></textarea>
                        </div>
                    </div>
                     <wuc:CargadorArchivos ID="wucCargadorArchivos" runat="server" />
                    <wuc:DatosProfesional ID="wucDatosProfesional" runat="server" />
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-info" type="button" id="btnGuardarOncologico">Guardar</button>
                            <button class="btn btn-secondary" type="button" id="btnCancelarOncologico">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloOncologia/NuevaSolicitudOncologica.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
