﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace RegistroClinico.Vista.ModuloOncologia
{


    public partial class NuevaSolicitudOncologica
    {

        /// <summary>
        /// Control wucPaciente.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.Paciente wucPaciente;

        /// <summary>
        /// Control Cie10.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.DiagnosticoCie10 Cie10;

        /// <summary>
        /// Control wucCargadorArchivos.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.CargadoDeArchivos wucCargadorArchivos;

        /// <summary>
        /// Control wucDatosProfesional.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.DatosProfesional wucDatosProfesional;
    }
}
