﻿<%@ Page Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaOncologia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloOncologia.BandejaOncologia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="row p-3">
        <div class="col-md-12">
            <h1 class="text-center">Listado de solicitudes al comite Oncologico</h1>
        </div>
        <div class="col-md-12">
            <div id="divBandejaOncologia" class="tab-pane fade show active" role="tabpanel"
                            aria-labelledby="home-tab-just">
                            <div class="card-body">
                                <button id="btn-FiltroBandejaOncologia" data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                                    data-target="#divFiltroOncologia" onclick="return false;">
                                    FILTRAR BANDEJA
                                </button>
                                <div id="divFiltroOncologia" class="collapse card p-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                                <select id="sltTipoIdentificacion" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label id="lblTipoIdentificacion" for="txtOncoFiltroRut"></label>
                                                <div class="input-group" id="divRutDigito">
                                                    <input id="txtOncoFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del Paciente" />
                                                    <div class="input-group-prepend digito">
                                                        <div class="input-group-text">-</div>
                                                    </div>
                                                    <input type="text" id="txtOncoFiltroDV" class="form-control digito" style="width: 10px" disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtOncoFiltroNombre">Nombre</label>
                                                <input id="txtOncoFiltroNombre" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtOncoFiltroApePaterno">Primer Apellido</label>
                                                <input id="txtOncoFiltroApePaterno" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="txtOncoFiltroApeMaterno">Segundo Apellido</label>
                                                <input id="txtOncoFiltroApeMaterno" type="text" class="form-control" placeholder="Del Paciente" maxlength="50" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlOncoEstado">Estado</label>
                                                <select id="ddlOncoEstado" class="form-control">
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="ddlUbicacion">Ubicación</label>
                                                <select id="ddlUbicacion" class="form-control">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12 text-right">
                                                <a id="btnOncoFiltro" class="btn btn-success" onclick="getTablaHospitalizacion()">
                                                    <i class="fa fa-search"></i>Buscar</a>
                                                <a id="btnOncoLimpiarFiltro" class="btn btn-default">
                                                    <i class="fa fa-eraser"></i>Limpiar Filtros</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
        <div class="col-md-12">
            <table id="tblOncologia" class="table table-striped"></table>
        </div>
    </div>
<script src="<%= ResolveClientUrl("~/Script/ModuloOncologia/BandejaOncologia.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
