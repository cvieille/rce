﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RegistroClinico.Default" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    
    <script>
        if (!sessionStorage.getItem("IngresarComo")) {
            localStorage.removeItem("sSession");
        }

        $(document).ready(function () {
            //$('#ul_ayuda').show();
            sessionStorage.removeItem("ab");
            sessionStorage.removeItem("idMenu");
            $("#Content_btnLogin").data("modal", false);
            $(".form-simple").css("height", $(".form-simple").parent().parent().css("height"));
            ShowModalCargando(false);

            if (sessionStorage.getItem("IngresarComo")) {
                var inputClave = document.getElementById('txtClave');
                inputClave.disabled = true;
            } 

            localStorage.setItem("urlWebApi", '<%= GetUrlWebApi() %>');
        });

    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <section class="form-simple center-horizontal-vertical">
        <div id="divLogin" class="card login animated" style="width: 600px">
            <div class="text-center pt-4 grey lighten-2">
                <h4 class="deep-grey-text"><strong>Inicio de Sesión</strong></h4>
            </div>
            <div class="card-body mx-4">
                <label for="txtUsuario">Usuario</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input id="txtUsuario" type="text" class="form-control" onblur="buscarUsuario(this)" 
                           maxlength="8" autofocus />
                </div>
                <label for="txtUsuario">Contraseña</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input id="txtClave" type="password" class="form-control" onblur="buscarUsuario(this)"
                         autofocus />
                </div>
                <div class="col-md-12" id="divSltPerfiles" >
                    <label for="sltPerfiles">Seleccione perfil para iniciar</label>
                    <select id="sltPerfiles" class="form-control" data-required="true"></select>
                </div>
                <div class="text-center mt-2 mb-4">
                    <button 
                        id="btnLogin" 
                        onclick="iniciarSesion()" 
                        type="button" 
                        class="btn btn-primary btn-block">Iniciar Sesión
                    </button>
                </div>
            </div>
            <div class="alert alert-info text-center" role="alert" style="margin-top: 10px;">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Se recomienda el uso de este sistema en Navegador Google Chrome.
            </div>
        </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/ModuloGEN/Login.js") + "?v=" + GetVersion()  %>"></script>

</asp:Content>
