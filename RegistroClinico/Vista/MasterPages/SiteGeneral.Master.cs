﻿using System;
using System.Web.UI;

namespace RegistroClinico.MasterPages
{
    public partial class SiteGeneral : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Verifica si la página actual es Default.aspx
            if (Page.AppRelativeVirtualPath.Contains("Vista/Default.aspx"))
            {
                pnl_expandirPantalla.Visible = true;  // Muestra el panel solo en Default.aspx
            }
            else
            {
                pnl_expandirPantalla.Visible = false; // Oculta el panel en otras páginas
            }
        }

        public string ObtenerEstilo()
        {
            var pathForm = Page.AppRelativeVirtualPath;
            return (pathForm.Contains("Vista/Default.aspx")) ? "margin-left: 0px !important;" : "";
        }

        public string GetVersion()
        {
            return Funciones.sVersion;
        }
    }
}
