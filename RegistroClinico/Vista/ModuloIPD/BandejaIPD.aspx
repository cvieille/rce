﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BandejaIPD.aspx.cs" Inherits="RegistroClinico.Vista.ModuloIPD.BandejaIPD" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#lnbExportar').click(function () {
                var tblObj = $('#tblIPD').DataTable();
                __doPostBack("<%= lnbExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <input type="hidden" id="idIPD" value="0">
    <input type="hidden" id="idExcepcion" value="0">
    <input type="hidden" id="fechaExcepcion" value="0">
    <!-- Componente de titulo-->
    <titulo-pagina data-title="IPD"></titulo-pagina>

    <section class="content">

        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Bandeja IPD</h3>
                </div>
                <div class="card-body">
                    <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                        data-target="#divForm" onclick="return false;">
                        FILTRAR BANDEJA</button>
                    <div id="divForm" class="collapse" style="margin: 20px;">
                        <h5>Paciente</h5>
                        <div class="row">
                            <div class="col-md-2">
                                <label>ID/NRO.</label>
                                <input id="txtFiltroID" type="text" class="form-control" placeholder="De IPD" />
                            </div>
                            <div class="col-md-2">
                                <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                <select id="sltTipoIdentificacion" class="form-control"></select>
                            </div>
                            <div class="col-md-2">
                                <label id="lblTipoIdentificacion" for="txtSTFiltroRut">RUT</label>
                                <div class="input-group">
                                    <input id="txtFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del Paciente">
                                    <div class="input-group-prepend digito" id="separadorDV">
                                        <span class="input-group-text" id="basic-addon1">-</span>
                                    </div>
                                    <input type="text" id="txtSTFiltroDV" class="form-control digito" style="width: 10px;" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Nombre</label>
                                <input id="txtFiltroNombre" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                            </div>
                            <div class="col-md-2">
                                <label>Primer Apellido</label>
                                <input id="txtFiltroApe" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                            </div>
                            <div class="col-md-2">
                                <label for="txtFiltroSApe">Segundo Apellido</label>
                                <input id="txtFiltroSApe" type="text" maxlength="50" class="form-control" placeholder="Del Paciente" />
                            </div>
                        </div>
                        <h5 class="mt-2">Datos clínicos</h5>
                        <div class="row">
                            <div id="divProfesional" class="col-md-3" style="display: none;">
                                <label>Profesional médico</label>
                                <select id="ddlMedicoIPD" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Estado IPD</label>
                                <select id="ddlEstadoIPD" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Problema de salud AUGE</label>
                                <select id="ddlAuge" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3" id="divSubGrupo">
                                <label>Subgrupo o subproblema de salud AUGE</label>
                                <select id="ddlSubGrupo" class="form-control">
                                    <option value="0">-Seleccione-</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4">
                                <div class="text-right">
                                    <button id="btnFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</button>
                                    <button id="btnLimpiarFiltro" class="btn btn-secondary text-white"><i class="fa fa-eraser"></i>Limpiar filtros</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div class="post">
                        <div class="text-right">
                            <asp:Button runat="server" ID="lnbExportarH" OnClick="lnbExportarH_Click" Style="display: none;" />
                            <a id="lnbExportar" class="btn btn-info waves-effect waves-light mb-2 mb-md-0" href="#\">
                                <i class="fa fa-file-excel"></i>Exportar XLS
                            </a>
                            <a id="lnbNuevoIPD" class="btn btn-info mb-2 mb-md-0" href="NuevoIPD.aspx">
                                <i class="fa fa-plus"></i>Nuevo IPD
                            </a>
                            <a id="btnRefrescar" class="btn btn-primary mb-2 mb-md-0" onclick="recargarBandeja()"><i class="fa fa-refresh"></i>Refrescar</a>
                        </div>
                    </div>
                    <div class="post">
                        <div class="form-row">
                            <div class="col-md-12">
                                <table id="tblIPD" class="table table-bordered table-hover" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade left" id="mdlConstancia" role="dialog" aria-labelledby="mdlConstancia" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Constancia</p>
                </div>
                <div class="modal-body">
                    <div class="post">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Profesional</label>
                                <input type="text" id="txtConstanciaPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-4">
                                <label>Número documento</label>
                                <input type="text" id="txtConstanciaProRut" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-4">
                                <label>Especialidad</label>
                                <input type="text" id="txtEspecialidadPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>

                        </div>
                    </div>
                    <div class="post">
                        <div class="row">
                            <div class="col-md-2">
                                <label>¿El paciente se representa a si mismo?</label>
                                <input type="checkbox" id="switchTitular" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                            </div>
                            <div class="col-md-2" id="divRepresentante">
                                <label>Tipo representante</label>
                                <select id="ddlConstanciaRepresentante" class="disabled form-control">
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Representante</label>
                                <input id="txtConstanciaRepresentante" type="text" placeholder="&nbsp;" class="form-control disabled" />
                            </div>
                            <div class="col-md-2">
                                <label>Fecha constancia</label>
                                <input id="txtConstanciaFecha" type="date" required class="form-control disabled" />

                            </div>
                            <div class="col-md-2">
                                <label>Hora constancia</label>
                                <input id="txtConstanciaHora" type="time" required class="form-control disabled" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnAceptar" class="btn btn-success" onclick="$('#mdlConstancia').modal('hide'); return false;">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimir" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameIPD" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade left" id="mdlExcepcion" role="dialog" aria-labelledby="mdlExcepcion" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Excepciones</p>
                    <!-- Botón de cierre en la esquina superior derecha -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divExcepcionVisible">
                        <%-- Este contenido siempre es visible --%>
                        <div class="post">
                            <%--<strong>Nueva excepción</strong>--%>
                            <div class="text-right">
                                <button id="bntNuevaExcepcion" class="btn btn-info">Crear nueva excepción</button>
                            </div>
                        </div>
                        <div class="post">
                            <strong>Excepciones</strong>
                            <div class="table-responsive">
                                <table id="tblExcepcion" class="table table-bordered table-hover"></table>
                            </div>
                        </div>
                    </div>
                    <%-- Este contenido esta oculto hasta presionar el boton --%>
                    <div id="divExcepcionOculto" style="display: none;">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="active">Garantía</label>
                                <select id="ddlGarantia" data-required="true" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-12 mb-3">
                                <label>Seguimiento excepción</label>
                                <input type="checkbox" id="switchSeguimiento" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                            </div>
                            <div class="col-md-4 col-sm-12 mb-3" id="divSeguimiento">
                                <label>Fecha seguimiento</label>
                                <input id="txtSeguimientoFecha" type="date" required class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="txtObservacion">Observación</label>
                                <textarea id="txtObservacion" maxlength="150" rows="3" data-required="true" style="resize: none;" class="form-control"></textarea>
                            </div>
                            <div class="col-md-2" style="margin-top: 30px; margin-right: 1px;">
                                <button id="btnExcepcion" class="btn btn-info">Agregar Excepción</button><br>
                            </div>
                            <div class="col-md-2" style="margin-top: 30px; margin-right: 1px;">
                                <button id="btnVolver" class="btn btn-warning">Volver</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <wuc:ModalGenericoMovimientosSistema ID="ModalGenericoMovimientosSistema" runat="server" />    

    <div class="modal fade" id="mdlAlerta" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal" style="justify-content: center">
                    <div class="center-horizontal">Alerta de sistema</div>
                </div>
                <div class="modal-body">
                    <div id="mdlAlertaCuerpo" class="row center-horizontal">
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <div class='col-sm-3'><a id='linkConfirmar' class='btn btn-block' href='#/'>Confirmar </a></div>
                    <div class='col-sm-3'><a id='linkCancelar' class='btn btn-default btn-block' href='#/' onclick="$('#mdlAlerta').modal('hide');">Cancelar </a></div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloIPD/BandejaIPD.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
