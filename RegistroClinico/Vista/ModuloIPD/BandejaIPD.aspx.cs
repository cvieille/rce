﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloIPD
{
    public partial class BandejaIPD : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lnbExportarH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["ID IPD"] = des[i][0].ToString(),
                        ["FECHA IPD"] = des[i][1].ToString(),
                        ["Paciente"] = des[i][2].ToString(),
                        ["Patología GES"] = des[i][3].ToString(),
                        ["Tipo IPD"] = des[i][5].ToString(),
                        ["Estado IPD"] = des[i][7].ToString()
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Bandeja IPD", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Bandeja IPD", workbook);

            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de bandeja de IPD.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }

        protected void lnbExportarMovH_Click(object sender, EventArgs e)
        {
            try
            {
                string sCommand = Request.Form["__EVENTARGUMENT"];

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

                List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
                for (int i = 0; i < des.Count; i++)
                {
                    Dictionary<string, string> d = new Dictionary<string, string>
                    {
                        ["Fecha Movimiento"] = des[i][1].ToString(),
                        ["Usuario"] = des[i][2].ToString(),
                        ["Movmiento"] = des[i][3].ToString()
                    };
                    dic.Add(d);
                }

                XLWorkbook workbook = new XLWorkbook();
                Funciones.CrearHojaExcel(ref workbook, "Movimientos IPD", dic);
                Funciones.ExportarTablaClosedXml(this.Page, "Movimientos IPD", workbook);
            }
            catch (Exception ex)
            {
                NegLog.GuardarLog("Error al exportar Excel de movimientos de IPD.", ex.Message + " " + ex.StackTrace + " " + ex.InnerException + " " + ex.Source);
            }
        }
    }
}