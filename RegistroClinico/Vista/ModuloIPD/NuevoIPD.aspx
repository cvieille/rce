﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoIPD.aspx.cs" Inherits="RegistroClinico.Vista.ModuloIPD.NuevoIPD" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="mdlEventos" %>
<%@ Register Src="~/Vista/UserControls/ModalPerfiles.ascx" TagName="mdlPerfiles" TagPrefix="mdlPerfiles" %>

<%--CONTROL EVENTO--%>
<%@ Register Src="~/Vista/UserControls/ModalEvento.ascx" TagName="mdlEventos" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalPacienteEvento.ascx" TagName="mdlPacienteEvento" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <%--<div id="alertObligatorios" class="alert text-center alert-obligatorios">
        <b>Los campos marcados son obligatorios(*)</b>.
    </div>--%>

    <input type="hidden" id="fechaEvento" value="0" />
    <input type="hidden" id="idRepresentante" value="0" />
    <input type="hidden" id="idConstancia" value="0" />
    <input type="hidden" id="idIPD" value="0" />
    <input type="hidden" id="idProfesional" value="0" />
    <input type="hidden" id="idPaciente" value="0" />

    <div class="card card-body">
        <div class="card">
            <div class="card-header bg-light">
                <!-- Componente de titulo-->
                <titulo-pagina data-title="IPD"></titulo-pagina>
            </div>
            <div id="divIPD" class="card-body p-0">

                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Informe del proceso diagnóstico</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Servicio de salud</label>
                                <select id="ddlServicio" data-required="false" disabled class="form-control">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Establecimiento</label>
                                <select id="ddlEstablecimiento" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Especialidad</label>
                                <select id="ddlEspecialidad" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Tipo de ingreso</label>
                                <select id="ddlAmbito" class="form-control" data-required="true">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <wuc:Paciente ID="wucPaciente" runat="server" />

                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos clínicos</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>¿Pertenece al sistema AUGE?</label>
                                    <br />
                                    <input type="checkbox" id="switchAuge" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                                <div id="divAuge" class="col-md-4" style="display: none;">
                                    <label for="ddlAuge">Problema de salud AUGE</label>
                                    <select id="ddlAuge" class="form-control" data-required="true">
                                    </select>
                                </div>
                                <div id="divSubAuge" class="col-md-4" style="display: none;">
                                    <label>Subgrupo o subproblema de salud AUGE</label>
                                    <select id="ddlSubAuge" class="form-control">
                                        <option selected>-Seleccione-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-md-6 col-lg-4">
                                    <label id="lbltxtDiagnostico">Diagnóstico</label>
                                    <textarea id="txtDiagnostico" maxlength="125" data-required="true" class="form-control" rows="4" style="resize: none;"></textarea>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-4">
                                    <label id="lbltxtFundamentoDiag">Fundamentos del diagnóstico</label>
                                    <textarea id="txtFundamentoDiag" maxlength="125" data-required="true" class="form-control" rows="4" style="resize: none;"></textarea>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-4">
                                    <label>¿Confirmar diagnóstico?</label>
                                    <select id="ddlTipoIPD" class="form-control" data-required="true">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <label id="lbltxtTratamiento">Tratamiento e indicaciones</label>
                                    <textarea id="txtTratamiento" maxlength="125" data-required="true" class="form-control" rows="5"></textarea>
                                </div>
                                <div class="col-md-5">
                                    <label>El tratamiento deberá iniciarse a mas tardar el</label>
                                    <input type="date" id="txtFechaTrat" required class="form-control" />
                                    <span style="color: crimson; display: none;" id="spanFecha">
                                        <i class="fa fa-times"></i><strong>La fecha debe ser mayor a la fecha de evento</strong>
                                    </span>
                                </div>
                            </div>
                            <%-- HORA Y FECHA AQUI --%>
                            <div class="row">
                                <div id="divIPDFecha" class="col-md-2">
                                    <label>Fecha IPD</label>
                                    <input id="txtIPDFecha" type="date" data-required="true" required class="form-control" />
                                </div>
                                <div class="col-md-2">
                                    <label>Hora IPD</label>
                                    <input id="txtIPDHora" type="time" data-required="true" required class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-dark">
                        <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos del profesional</strong> </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Número documento</label>
                                <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-3">
                                <label>Nombre</label>
                                <input type="text" id="txtNombrePro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-3">
                                <label>Primer Apellido</label>
                                <input type="text" id="txtApellidoPPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                            <div class="col-md-3">
                                <label>Segundo Apellido</label>
                                <input type="text" id="txtApellidoMPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <button id="btnContinuar" class="btn btn-info float-right" type="submit">CONTINUAR</button>
            </div>

        </div>
    </div>

    <div class="modal fade bottom" id="mdlConstancia" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead mb-0">Constancia</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group list-group-flush" id="divConstancia">
                        <li class="list-group-item">
                            <div class="text-center">
                                <i class="fa fa-clipboard-list fa-3x mb-3 animated fadeInUp"></i>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Profesional</label>
                                    <input type="text" id="txtConstanciaPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>
                                <div class="col-md-4">
                                    <label>Número documento</label>
                                    <input type="text" id="txtConstanciaProRut" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>
                                <div class="col-md-4">
                                    <label>Especialidad</label>
                                    <input type="text" id="txtEspecialidadPro" placeholder="&nbsp;" class="form-control disabled" readonly />
                                </div>

                            </div>
                        </li>

                        <%-- AQUI PROBANDO FECHA Y HORA APARTE DEL PROFESIONAL --%>
                        <li class="list-group-item">
                            <div class="row d-flex justify-content-center align-items-center">
                                <div class="col-md-3">
                                    <label>Fecha constancia</label>
                                    <input id="txtConstanciaFecha" type="date" required class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Hora constancia</label>
                                    <input id="txtConstanciaHora" type="time" required class="form-control" />
                                </div>
                            </div>
                        </li>


                        <%-- FIN DE ESO --%>

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>¿El paciente se representa a si mismo?</label>
                                    <br />
                                    <input type="checkbox" id="switchTitular" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                </div>
                                <div class="col-md-2">
                                    <label>Tipo representante</label>
                                    <select id="ddlConstanciaRepresentante" data-required="true" class="form-control">
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>RUT representante</label>
                                    <div class="input-group">
                                        <input type="text" maxlength="8" id="txtRutRep" class="form-control" style="width: 95px">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">-</div>
                                        </div>
                                        <input type="text" id="txtRutDigitoRep" class="form-control" style="width: 12px">
                                    </div>
                                </div>
                                <div class="col-md-4 offset-md-1">
                                    <label>Nombre</label>
                                    <input type="text" id="txtNombreRep" class="form-control disabled" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnNuevaConstancia" class="btn btn-info">Guardar constancia</button>
                    <button id="btnOmitirConstancia" class="btn btn-secondary">Guardar sin constancia</button>
                    <button class="btn btn-danger" data-dismiss="modal">Volver</button>
                </div>
            </div>
        </div>
    </div>


    <script src="<%= ResolveClientUrl("~/Script/ModuloIPD/NuevoIPD.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
