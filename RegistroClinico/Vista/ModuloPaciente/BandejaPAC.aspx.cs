﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Services;

namespace RegistroClinico.Vista.ModuloPaciente
{
    public partial class BandejaPAC : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //este web method va a seguir existiendo mientras no exista un controlador o se decida que hacer con la bandeja
        [WebMethod]
        public static string bandeja(string s)
        {
            List<Dictionary<string, string>> jsonList = new List<Dictionary<string, string>>();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select GEN_idAtencion_Paciente_Establecimiento,");
            sb.AppendLine("GEN_Establecimiento.GEN_nombreEstablecimiento, GEN_fechaAtencion_Paciente_Establecimiento from GEN_Atencion_Paciente_Establecimiento");
            sb.AppendLine("inner join gen_paciente on gen_paciente.GEN_idPaciente = GEN_Atencion_Paciente_Establecimiento.GEN_idPaciente");
            sb.AppendLine("inner join GEN_Establecimiento on GEN_Establecimiento.GEN_idEstablecimiento = GEN_Atencion_Paciente_Establecimiento.GEN_idEstablecimiento");
            sb.AppendLine("where gen_estadoAtencion_Paciente_Establecimiento = 'Activo'");
            sb.AppendLine("and gen_paciente.gen_idpaciente = " + s);

            ConexionSQL c = new ConexionSQL();
            DataTable dt = c.GetDataTable(sb.ToString());

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dtr = dt.Rows[i];
                Dictionary<string, string> json = new Dictionary<string, string>
                {
                    ["GEN_idAtencion_Paciente_Establecimiento"] = dtr["GEN_idAtencion_Paciente_Establecimiento"].ToString(),
                    ["GEN_nombreEstablecimiento"] = dtr["GEN_nombreEstablecimiento"].ToString(),
                    ["GEN_fechaAtencion_Paciente_Establecimiento"] = dtr["GEN_fechaAtencion_Paciente_Establecimiento"].ToString()
                };
                jsonList.Add(json);
            }
            return JsonConvert.SerializeObject(jsonList);
        }
    }
}