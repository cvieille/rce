﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace RegistroClinico.Vista.ModuloPaciente
{


    public partial class BandejaPAC
    {

        /// <summary>
        /// Control Paciente1.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAlerta Paciente1;

        /// <summary>
        /// Control ModalImpresionesUrgencia.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalPerfiles ModalImpresionesUrgencia;

        /// <summary>
        /// Control ModalGenericoMovimientosSistema.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalGenericoMovimientosSistema ModalGenericoMovimientosSistema;

        /// <summary>
        /// Control ModalAlergias.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.ModalAlergias ModalAlergias;

        /// <summary>
        /// Control wucPaciente.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::RegistroClinico.Vista.UserControls.Paciente wucPaciente;
    }
}
