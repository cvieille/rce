﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="FusionPaciente.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.FusionPaciente" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
        <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />


    <section class="content-header">

        <div class="container-fluid">
            <h1 class="mb-3">Fusión de Pacientes</h1>
        </div>


    </section>

    <div class="row">

        <div class="col-sm-6"> <!-- Primera Columna -->

            <div class="card"> <!-- Paciente Origen-->
              <div class="card-header bg-info">
                <h5> Paciente Origen </h5>
              </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Identificación</label>
                            <select id="sltIdentificacion" class="form-control">
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label id="lblIdentificacion">RUT</label>
                            <div class="input-group">
                                <input id="txtnumerotPac" type="text" class="form-control" style="width: 100px;" maxlength="8">
                                <div id="digitoSeparacion" class="input-group-prepend digito">
                                    <div class="input-group-text">
                                        <strong>-</strong>
                                    </div>
                                </div>
                                <input type="text" id="txtDigitoPac" class="form-control digito" maxlength="1" style="width: 15px">
                            </div>
                        </div>
                        <div class="col-sm-3 m-4">
                            <button type="button" id="btnBuscarOrigen" class="form-control bg-success">Buscar</button>
                        </div>
                        <input hidden autocomplete="off" type="text" id="idPacienteOrigenSel" />
                    </div>
                </div>
              </div>


            <div class="card"> <!-- Datos Paciente Origen-->
              <div class="card-header bg-info">
                <h5> Datos Paciente Origen </h5>
              </div>
                <div class="card-body">

                    <table id="tblOrigen" class="table table-bordered table-hover" style="width: 100%"></table>
                 </div>      
            </div>

        </div>


        <div class="col-sm-6"> <!-- Segunda Columna -->

            <div class="card"> <!-- Paciente Destino-->
              <div class="card-header bg-info">
                <h5> Paciente Destino </h5>
              </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Identificación</label>
                            <select id="sltIdentificacionD" class="form-control">
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label id="lblIdentificacionD">RUT</label>
                            <div class="input-group">
                                <input id="txtnumerotPacD" type="text" class="form-control" style="width: 100px;" maxlength="8">
                                <div id="digitoSeparacionD" class="input-group-prepend digito">
                                    <div class="input-group-text">
                                        <strong>-</strong>
                                    </div>
                                </div>
                                <input type="text" id="txtDigitoPacD" class="form-control digito" maxlength="1" style="width: 15px">
                            </div>
                        </div>
                        <div class="col-sm-3 m-4">
                            <button type="button" id="btnBuscarOrigenD" class="form-control bg-success">Buscar</button>
                        </div>
                        <input hidden autocomplete="off" type="text" id="idPacienteDestinoSel" />
                    </div>
                </div>
              </div>


            <div class="card"> <!-- Datos Paciente Destino-->
              <div class="card-header bg-info">
                <h5> Datos Paciente Destino </h5>
              </div>
                <div class="card-body">
                     <table id="tblDestino" class="table table-bordered table-hover" style="width: 100%"></table>
                 </div>      
            </div>

        </div>

  </div>

    <div class="col-md-2 offset-md-5">
        <button type="button" id="btnFusionar" class="form-control bg-warning" onclick="FusionarPacienteConfirmacion()">Fusionar</button>
    </div>


    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/FusionPaciente.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
