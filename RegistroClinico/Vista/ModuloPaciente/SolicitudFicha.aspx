﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SolicitudFicha.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.SolicitudFicha" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Módulo paciente</h1>
                </div>
                <div class="col-sm-6">
                    <br />
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Solicitud de fichas</h3>
                    </div>
                    <div class="card-body">
                        <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                            data-target="#divForm" onclick="return false;">FILTRAR BANDEJA</button>
                        <div id="divForm" class="collapse card">
                            <div class="card-body">
                                <h6 class="card-title" style="padding-left:10px;">Datos clínicos</h6>
                                <div class="row">
                                    <div class="col-md-3 offset-1">
                                        <div class="md-form">
                                            <label class="active">Ámbito</label>
                                            <div style="height: 10px;">&nbsp;</div>
                                            <select id="ddlAmbito" class="selectpicker enterForm" data-style="btn-primary-dark waves-effect" title="Seleccione">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="display: flex; flex-direction: row; align-items: center; justify-content: flex-end;">
                                        <button id="btnFiltro" class="btn btn-default">Filtrar</button>
                                        <button id="btnLimpiarFiltro" class="btn btn-blue-grey">Limpiar Filtros</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="tblSolicitud" class="table table-bordered table-hover"></table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="mdlUbicacion" role="dialog" aria-labelledby="mdlUbicacion" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Ubicación</p>
                </div>
                <div class="modal-body">
                    <div class="card" style="padding:20px;">
                        Ubicación:
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label class="active">Ubicación actual</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <select id="ddlUbicacionOrigen" class="selectpicker" data-size="5" data-required="true" data-style="btn-primary-dark waves-effect disabled"
                                        data-live-search="true" data-live-search-placeholder="Escriba el nombre del servicio" title="Seleccione">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form">
                                    <label class="active">Ubicación destino</label>
                                    <div style="height: 10px;">&nbsp;</div>
                                    <select id="ddlUbicacionDestino" class="selectpicker" data-size="5" data-required="true" data-style="btn-primary-dark waves-effect"
                                        data-live-search="true" data-live-search-placeholder="Escriba el nombre del servicio" title="Seleccione">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnOrigen" data-required="true" onclick="return false;" class="btn btn-default">Cambiar ubicación de origen</button>
                    <button class="btn btn-warning" onclick="$('#mdlUbicacion').modal('hide'); return false;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlQuitar" role="dialog" aria-labelledby="mdlQuitar" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Quitar solicitud</p>
                </div>
                <div class="modal-body text-center">
                    <div class="card" style="padding:20px;background: #fff5d3;">
                        <b>La solicitud se quitará de la bandeja.</b>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnQuitar" data-required="true" onclick="return false;" class="btn btn-danger">Quitar solicitud</button>
                    <button class="btn btn-warning" onclick="$('#mdlQuitar').modal('hide'); return false;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/SolicitudFicha.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>