﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RegistroClinico.Vista.ModuloPaciente
{
    public partial class Reporte : Handlers.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExportarH_Click(object sender, EventArgs e)
        {
            string sCommand = Request.Form["__EVENTARGUMENT"];

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            List<List<Object>> des = JsonConvert.DeserializeObject<List<List<Object>>>(sCommand, settings);

            List<Dictionary<string, string>> dic = new List<Dictionary<string, string>>();
            string t = "Fichas creadas";
            if (des[0][0].ToString() == "2")
                t = "Fichas modificadas odontología";

            for (int i = 0; i < des.Count; i++)
            {
                Dictionary<string, string> d = new Dictionary<string, string>
                {
                    ["N° ubicación interna"] = des[i][1].ToString(),
                    ["N° documento"] = des[i][2].ToString(),
                    ["Nombre paciente"] = des[i][3].ToString(),
                    ["Fecha de creación"] = des[i][4].ToString(),
                    ["Creado por"] = des[i][5].ToString(),
                    ["Modificado por"] = des[i][6].ToString()
                };
                dic.Add(d);
            }

            XLWorkbook workbook = new XLWorkbook();
            Funciones.CrearHojaExcel(ref workbook, t, dic);
            Funciones.ExportarTablaClosedXml(this.Page, t, workbook);
        }
    }
}