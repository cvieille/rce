﻿using Newtonsoft.Json;
using RegistroClinico.Negocio;
using System;
using System.Collections.Generic;
using System.Web;

namespace RegistroClinico.Vista.ModuloPaciente
{
    public partial class ImprimirCaratulaFicha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Valida sesión según los permisos que tenga el perfil
            NegSession negSession = new NegSession(Page);
            negSession.ValidarPermisosSession();
        }

        protected void btnHidden_Click(object sender, EventArgs e)
        {
            //Arreglar llaves Etc...
            string sCommand = Request.Form["__EVENTARGUMENT"];

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            Dictionary<string, object> jsonPaciente = JsonConvert.DeserializeObject<Dictionary<string, object>>(sCommand, settings);
            //Lanza exepción cuando no hay Login

            lblUbicacionInterna.Text = jsonPaciente.ContainsKey("GEN_nuiPaciente") ? Convert.ToString(jsonPaciente["GEN_nuiPaciente"]) : "";
            lblRutPac.Text = jsonPaciente.ContainsKey("GEN_numero_documentoPaciente") ? Convert.ToString(jsonPaciente["GEN_numero_documentoPaciente"]) : "";
            lblDigitoPac.Text = jsonPaciente.ContainsKey("GEN_digitoPaciente") ? Convert.ToString(jsonPaciente["GEN_digitoPaciente"]) : "";
            lblNombrePac.Text = jsonPaciente.ContainsKey("GEN_nombrePaciente") ? Convert.ToString(jsonPaciente["GEN_nombrePaciente"]) : "";
            lblPaternoPac.Text = jsonPaciente.ContainsKey("GEN_ape_paternoPaciente") ? Convert.ToString(jsonPaciente["GEN_ape_paternoPaciente"]) : "";
            lblMaternoPac.Text = jsonPaciente.ContainsKey("GEN_ape_maternoPaciente") ? Convert.ToString(jsonPaciente["GEN_ape_maternoPaciente"]) : "";

            if (jsonPaciente.ContainsKey("GEN_fec_nacimientoPaciente"))
            {
                DateTime fecha = Convert.ToDateTime(jsonPaciente["GEN_fec_nacimientoPaciente"]);
                lblFechaNacimientoPac.Text = fecha.ToString("dd/MM/yyyy");
            }
            else
            {
                lblFechaNacimientoPac.Text = "";
            }

            lblSexoPac.Text = jsonPaciente.ContainsKey("GEN_nombreSexo") ? Convert.ToString(jsonPaciente["GEN_nombreSexo"]) : "";
            lblDireccionPac.Text = jsonPaciente.ContainsKey("GEN_dir_callePaciente") ? Convert.ToString(jsonPaciente["GEN_dir_callePaciente"]) + " " + Convert.ToString(jsonPaciente["GEN_dir_numeroPaciente"]) : "";
            lblComunaPac.Text = jsonPaciente.ContainsKey("GEN_nombreCiudad") ? Convert.ToString(jsonPaciente["GEN_nombreCiudad"]) : "";
            lblFonoPac.Text = jsonPaciente.ContainsKey("GEN_telefonoPaciente") ? Convert.ToString(jsonPaciente["GEN_telefonoPaciente"]) : "";
            lblSistemaSaludPac.Text = jsonPaciente.ContainsKey("GEN_nombrePrevision") ? Convert.ToString(jsonPaciente["GEN_nombrePrevision"]) : "";
            lblProfesionPac.Text = "";

            string sName = string.Join(" ", new string[] { "Caratula", jsonPaciente["GEN_nombrePaciente"].ToString() });
            string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            //Funciones.ExportarPDF(sUrl, this.Page, "Caratula Ficha "+sName, HttpContext.Current.Session["LOGIN_USUARIO"].ToString(),false);
        }
    }
}
