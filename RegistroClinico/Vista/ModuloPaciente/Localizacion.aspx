﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Localizacion.aspx.cs" Inherits="RegistroClinico.Vista.ModuloPaciente.Localizacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="card">
        <div class="card-header">
            <h2>Localización del paciente.</h2>
            <p>Busque la ubicación de un paciente mediante se numero de indentificación.</p>
        </div>
        <div class="card-body">
            <div class="row" id="divFiltrosBusqueda">
                <div class="col-md-2">
                    <label for="sltAmbito">Ámbito</label>
                    <select class="form-control" id="sltAmbito" data-required="true">
                        <option value="value">text</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="sltTipoIdentificacion">Tipo de identificación</label>
                    <select id="sltTipoIdentificacion" class="form-control" data-required="true">
                    </select>
                </div>
                <div class="col-md-2">
                    <label id="lblTipoIdentificacion" for="txtHosFiltroRut">RUT</label>
                    <div class="input-group" id="divRutDigito">
                        <input id="txtFiltroRut" type="text" class="form-control" style="width: 110px;" data-required="true" placeholder="Del paciente" />
                        <div id="guionDigitoHos" class="input-group-prepend digito">
                        <div class="input-group-text">-</div>
                        </div>
                        <input type="text" id="txtFiltroDV" class="form-control digito" style="width: 10px" disabled />
                    </div>
                </div> 
                <div class="col-md-2 col-sm-2">
                    <button type="button" class="btn btn-success divButtonLabel" onclick="buscarPaciente()"  ><i class="fa fa-search"></i> Buscar</button>
                </div>
                <div class="col-md-12 mt-2"  id="divTblLocalizacionHospitalizacion" style="display:none;">
                    <table id="tblLocalizacionHospitalizacion" class="table table-bordered table-striped w-100" >
                        <caption><i class="fa fa-info fa-lg"></i> Resultados de hospitalización</caption>
                    </table>
                </div>
                <div class="col-md-12 mt-2"  id="divTblLocalizacionUrgencia" style="display:none;">
                    <table id="tblLocalizacionUrgencia" class="table table-bordered table-striped w-100">
                        <caption> <i class="fa fa-info fa-lg"></i> Resultados de urgencia</caption>
                    </table>
                </div>
            </div>

        </div>
    </div>

    
    <script src="<%= ResolveClientUrl("~/Script/ModuloPaciente/Localizacion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
