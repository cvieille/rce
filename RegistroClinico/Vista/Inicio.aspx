﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="RegistroClinico.Vista.Inicio" %>
<%@ Register Src="~/Vista/UserControls/ImpresoraZebra.ascx" TagName="ImpresoraZebra" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script>

        $(document).ready(function () {

            var sSession;
            sSession = getSession();
            RevisarAcceso(false, sSession.nombre);

            $("#aConfiguracionImpresoras").on('click', async function () {
                await cargarConfiguracionImpresoras();
            });

            $("#btnGuardarInfoImpresoras").on('click', async function (evt) {

                evt.preventDefault();
                const array = [];
                ShowModalCargando(true);
                $("[data-tipo]").each(function (slt) {
                    const nombre = $(this).children().children("select").val();
                    const ip = $(this).children().children("input").val();
                    array.push({
                        Nombre: valCampo(nombre),
                        Ip: valCampo(ip),
                        Tipo: $(this).attr("data-tipo")
                    });
                });

                await $.ajax({
                    type: 'POST',
                    url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=CambiarImpresorasZebra`,
                    data: JSON.stringify(array),
                    success: function (data) {
                        $('#mdlConfiguracionImpresoras').modal('hide');
                        ShowModalCargando(false);
                    },
                    error: function () {
                        ShowModalCargando(false);
                    }
                });

            });
            cargarDatosUsuario();
            cargarManualesUsuario();
            ShowModalCargando(false);
        });
        async function cargarDatosUsuario() {
            try {
                const data = await $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_Usuarios/LOGEADO`,
                    contentType: "application/json",
                    dataType: "json"
                });

                if (!Array.isArray(data) || data.length === 0) {
                    console.error("No se encontraron datos de usuario.");
                    return;
                }

                const usuario = data[0];
                profesionalLogeado = usuario;

                const { Persona, Profesional } = usuario;
                const { Nombre, ApellidoPaterno, ApellidoMaterno } = Persona;
                const nombreCompleto = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`;
                const profesion = Profesional?.Profesion?.Valor || "N/A";

                actualizarTexto("#lblNombreUsuario", `Nombre: ${nombreCompleto}`);
                actualizarTexto("#lblNombrePerfil", `Perfil: ${session.PERFIL_USUARIO}`);
                actualizarTexto("#lblProfesionUsuario", `Profesión: ${profesion}`);

                if (Profesional?.JefeEspecialidad?.length > 0) {
                    const { DescripcionTipoJefatura, DescripcionEspecialidad } = Profesional.JefeEspecialidad[0];
                    actualizarTexto("#lblJefeEspecialidad", `Perfil: ${DescripcionTipoJefatura} ${DescripcionEspecialidad}`);
                }

                if (session.ID_PERFIL === 149 && Profesional?.Especialidad) {
                    actualizarTexto("#lblEspecialidadUsuario", `Especialidad: ${Profesional.Especialidad.Valor}`);
                }

             

            } catch (error) {
                console.error("Error al cargar datos del usuario:", error);
                ShowModalCargando(false);
            }
        }

        // Función auxiliar para actualizar el texto de un elemento
        function actualizarTexto(selector, texto) {
            $(selector).text(texto);
        }
        function crearElementoManual(index, nombre) {
            return `
                    <div class='col col-md-6'>
                        <div id='divManual_${index}' class='alert alert-info text-center' 
                             style='text-transform: uppercase; cursor: pointer;' 
                             data-nombre-documento='${nombre}'>
                            <i class='fa fa-file-pdf' style='padding-right:20px'></i> ${nombre}
                        </div>
                    </div>`;
        }

        async function cargarManualesUsuario() {
            ///Manuales de usuario            
            $.ajax({
                type: "GET",
                url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=getManuales',
                data: null,
                async: true,
                success: function (data) {
                    $("#tblManuales").empty();
                    var d = JSON.parse(data);

                    d.forEach((item, index) => {

                        var nombre = item;
                        $("#divManuales").append(crearElementoManual(index, item));
                        $(`#divManual_${index}`).click(function () {

                            $.ajax({
                                cache: false,
                                contentType: false,
                                type: "GET",
                                url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=getManual&nombre=${$(this).data("nombre-documento")}`,
                                processData: false,
                                xhrFields: {
                                    responseType: 'blob'
                                },
                                success: function (response, status, xhr) {

                                    try {

                                        var blob = new Blob([response], { type: 'application/pdf' });
                                        var URL = window.URL || window.webkitURL;
                                        var downloadUrl = URL.createObjectURL(blob);
                                        window.open(downloadUrl);

                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                },
                                error: function (err) {
                                    console.log("error al traer manual: " + JSON.stringify(err));
                                }
                            });

                        });
                    });

                    $('#mdlManuales').modal("show");
                },
                error: function (err) {
                    console.log("error" + JSON.stringify(err));
                }
            });
        }
        async function cargarConfiguracionImpresoras() {

            //"Tipo": "General_Pediatrico",
            //"NombreImpresora": "ZD510-300dpi (10.6.181.178)",
            //"Nombre": "Pedíatrico",
            //"IP": "10.6.181.178"

            ShowModalCargando(true);
            try {
                await ConectarImpresoras();
            } catch (ex) {
                console.log(ex);
                Swal.fire({ position: 'center', icon: 'error', title: 'Este PC no está habilitado para imprimir brazaletes.' });
                ShowModalCargando(false);
            }
            const impresoras = await getImpresoras();
            $("#divImpresorasConfig").empty();
            let selects = "";
            impresoras.forEach((impresora) => {
                selects += `<option value='${impresora.name}'>${impresora.name}</option>`;
            });
            impresorasZebra.forEach((impresora) => {
                $("#divImpresorasConfig").append(
                    `<label class='mt-2'>Impresora (${impresora.Nombre })</label> 
                    <div class='row' data-tipo='${impresora.Tipo}'>
                        <div class='col-md-6'>
                            <select id='sltNombreImpresara_${impresora.Nombre}' class='form-control'>
                                <option value='0'>Seleccione</option>
                                ${selects}
                            </select>
                        </div>
                        <div class='col-md-6'>
                            <input id='txtIpImpresora_${impresora.Nombre}' type='text' placeholder='IP Impresora' class='form-control' value='${impresora.IP ?? ""}' />
                        </div>
                    </div>`
                );
                $(`#sltNombreImpresara_${impresora.Nombre}`).val(impresora.NombreImpresora);
                if (!$(`#sltNombreImpresara_${impresora.Nombre}`).val())
                    $(`#sltNombreImpresara_${impresora.Nombre}`).val('0');
            });

            ShowModalCargando(false);
            $('#mdlConfiguracionImpresoras').modal('show');

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <div class="col col-md-12">
            <div class="card mt-3">
                <div class="card-header text-center" style="font-size: 22px;">
                    <h2>Bienvenido Registro Clínico Electrónico</h2>
                    <h3>Hospital Clínico Magallanes</h3>
                </div>
                <div class="card-body">
                    <div class="card ">
                        <div class="card-body">
                            <div class="row">
                                <p>
                                    Para realizar un nuevo registro o consultar información de paciente, debe navegar a través de los distintos menús habilitados. 
                                    El acceso a los módulos se entrega según rol o función. 
                                    Si necesita nuevos accesos, se deben solicitar vía correo electrónico a desarrollo.hcm@redsalud.gob.cl
                                </p>
                                <p>Recuerde que los datos de acceso son de <b>uso personal, única e intransferible según se indica en políticas de seguridad</b>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col col-md-12">
            <div class="card mt-3">

                <div class="card-body">
                    <div class="card ">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card-title alert alert-primary w-100  mb-4">USTED HA INICIADO SESIÓN COMO:</div>
                                </div>

                                <div class="col-lg-12">
                                    <label id="lblNombreUsuario" class="card-text text-uppercase" style="font-size: 35px;"></label>
                                    <br />
                                </div>

                                <div class="col-lg-6 col-sm-12">
                                    <label id="lblNombrePerfil" class="card-text text-uppercase" style="font-size: 20px;"></label>
                                    <br />
                                    <label id="lblProfesionUsuario" class="card-text text-uppercase" style="font-size: 20px;"></label>
                                    <br />
                                    <label id="lblEspecialidadUsuario" class="card-text text-uppercase" style="font-size: 20px;"></label>
                                    <br />
                                </div>

                                <div class="col-lg-6 col-sm-12">
                                    <label id="lblJefeEspecialidad" class="card-text text-uppercase" style="font-size: 20px;"></label>
                                    <br />
                                    <label id="lblEspecialidadJefe" class="card-text text-uppercase" style="font-size: 20px;"></label>
                                    <br />

                                </div>
                            </div>

                            <hr />
                            <p class="text-center">
                                Si sus datos son incorrectos o necesita generar nuevas cuentas, favor solicitar cambios al anexo(s) 
                                613669 - 613166 <i class="fa fa-phone" aria-hidden="true"></i>, o al correo 
                                desarrollo.hcm@redsalud.gob.cl
                            </p>
                            <a href="#" onclick="window.location.replace(ObtenerHost() + '/Vista/Default.aspx');" class="btn btn-secondary btn-block btn-lg">
                                <i class="fa fa-exchange-alt"></i>
                                Cambiar de Usuario</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-md-12">
            <div class="card mt-3">
                <div class="card-header ">Manuales de Usuario</div>
                <div class="card-body p-4">
                    <div class="card">
                        <div id="divManuales" class="row">
                        </div>
                        <table id="tblManuales">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header ">Ajuste del Sistema</div>
        <div class="card-body p-4">
            <div class="col col-md-6">
                <a id="aConfiguracionImpresoras" class="btn btn-info btn-block pt-2 pb-2">
                    <i class="fa fa-print" style="padding-right:20px"></i> CONFIGURAR IMPRESORAS DE BRAZALETES
                </a>
            </div>
        </div>
    </div>
    
    <div id="mdlConfiguracionImpresoras" class="modal fade" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info pb-1">
                    <h4><b><i class="fa fa-print"></i> Configuración de Impresoras</b></h4>
                </div>
                <div class="modal-body">
                    <h2 class="text-center mb-3">Impresoras</h2>
                    <div id="divImpresorasConfig" class="mb-4"></div>
                    <div class="text-center mb-4">
                        <button id="btnGuardarInfoImpresoras" class="btn btn-info">Guardar Información</button>
                    </div>
                    <wuc:ImpresoraZebra runat="server" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
