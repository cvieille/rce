﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="DashboardUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.DashboardUrgencia" %>

<%@ Register Src="~/Vista/UserControls/ModalImpresionesUrgencia.ascx" TagName="ModalImpresionesUrgencia" TagPrefix="wuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <wuc:ModalImpresionesUrgencia ID="ModalImpresionesUrgencia" runat="server" />

    <!-- Componente de titulo-->
    <titulo-pagina data-title="Dashboard Urgencia"></titulo-pagina>

    <section class="content pt-3">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4>Atenciones por Estado</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblEstadosUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalEstadosUrgencia"></span>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones por Tipo</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblTipoUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalTipoUrgencia"></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones Categorizados</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblCategorizacionUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalCategorizados"></span>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones por clasificación</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblClasificacionUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalCClasificacion"></span>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="card ">
                    <div class="card-header bg-info">
                        <h4>Atenciones por edad</h4>
                    </div>
                    <div class="card-body">
                        <table id="tblEdadUrgencia" class="table table-striped table-hover"></table>
                    </div>
                    <div class="card-footer">
                        Total: <span id="spTotalEdad"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="mdlDetalle" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detalle</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table id="tblDetalle" class="table table-bordered table-hover dataTable no-footer w-100">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Dashboard/DashboardUrgencia.js") + "?v=" + GetVersion() %>"></script>    
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalImpresionesUrgencia.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
