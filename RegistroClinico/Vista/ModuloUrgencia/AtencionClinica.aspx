﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AtencionClinica.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.AtencionClinica" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalCerrarAtencionUrgencia.ascx" TagName="ModalCerrarAtencion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAltaUrgencia.ascx" TagName="ModalAltaUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DiagnosticoCIE10Urgencia.ascx" TagName="DiagnosticoUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalSignosVitales.ascx" TagName="ModalSignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalComentarioUrgencia.ascx" TagName="ModalComentarioUrgencia" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">

    <wuc:ModalSignosVitales ID="ModalSignosVitales" runat="server" />
    <wuc:ModalComentarioUrgencia ID="ModelComentarioUrgencia" runat="server" />
    <wuc:ModalCerrarAtencion ID="ModalCerrarAtencion" runat="server" />

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

    <style>
        .modal-contentenido {
            max-width: 100%;
            overflow-y: auto;
        }

        .modal-evoluciones-urg {
            max-width: 60%; /* Porcentaje del ancho máximo del modal */
        }

        .table.dataTable td, table.dataTable th {
            padding-top: 1px;
            padding-bottom: 8px;
        }

        .salud-icon {
            margin: 5px;
            font-size: 35px; /* Tamaño del icono reducido */
        }

        .historial-signos.dataTable thead th {
            /* Estilos específicos para los th de la tabla #tblHistorialSignosVitales */
            height: 50px;
            vertical-align: middle;
        }

        .btn-outline-secondary:hover {
            color: #fff !important;
        }

        .zindexSignosVitales {
            z-index: 9999;
        }

        .usuario-fixed {
            position: fixed;
            width: auto;
            height: auto;
            z-index: 1052;
            top: 10px;
            position: fixed;
            left: 50%;
            transform: translateX(-50%);
            background-color: #2C6E78;
            color: white;
            padding: 10px 20px 10px 20px;
        }
    </style>

</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <div class="card card-body mb-0">
        <div class="card">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Atención Clinica Urgencia"></titulo-pagina>
            <div class="card-body p-0">
                <div class="tab-content card">
                    <div id="divDatosClinicos" class="pt-3">
                        <div id="divMedico" class="card">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos clínicos</strong></h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                        <div class="card">
                                            <div class="card-header bg-dark">
                                                <h4 class="text-center">Paciente</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="container">
                                                    <!-- Nombre -->
                                                    <div class="row mt-1 mb-1">
                                                        <h5><strong>Nombre:</strong> <span id="txtNombrePacienteUrg"></span></h5>
                                                    </div>
                                                    <!-- RUT Y BOX -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>N° Documento:</strong> <span id="txtRutPacienteUrg"></span>
                                                        </div>
                                                        <div class="col-auto p-0 text-right">
                                                            Ubicación: <strong><span id="txtLugarPaciente"></span></strong>
                                                        </div>
                                                    </div>
                                                    <!-- FECHA NAC -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Fecha nacimiento:</strong> <span id="txtFechaNacUrg"></span>
                                                        </div>
                                                    </div>
                                                    <!-- EDAD -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Edad:</strong> <span id="txtEdadUrg"></span>
                                                        </div>
                                                    </div>
                                                    <!-- SEXO -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Sexo:</strong> <span id="txtSexoUrg"></span>
                                                        </div>
                                                    </div>
                                                    <!-- GÉNERO -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Género:</strong> <span id="txtGeneroUrg"></span>
                                                        </div>
                                                    </div>
                                                    <!-- PREVISIÓN -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Previsión:</strong> <span id="txtPrevisionUrg"></span>
                                                        </div>
                                                    </div>
                                                    <!-- ACOMPAÑANTE -->
                                                    <div class="row mt-1 mb-1 d-flex justify-content-between align-items-center">
                                                        <div class="col-auto p-0">
                                                            <strong>Acompañante:</strong> <span id="txtAcompanante"></span>
                                                        </div>
                                                    </div>
                                                    <!-- SEPARADOR -->
                                                    <hr class="row mt-1 mb-1" style="border: 1px solid #17A2B8;">
                                                    <br>
                                                    <!-- MOTIVO -->
                                                    <div class="row mt-1 mb-1">
                                                        <strong>Motivo de consulta reportado por admisión:</strong>
                                                        <textarea id="txtMotivoConsultaUrg" class="form-control" rows="2" maxlength="200"
                                                            style="background-color: transparent; border: none; padding: 0; resize: none; width: 100%; font-size: inherit; line-height: inherit; overflow: hidden;" readonly></textarea>
                                                    </div>
                                                    <!-- INGRESADO POR -->
                                                    <br>
                                                    <div class="row mt-1 mb-1">
                                                        <h6><em>Ingresado a las <span id="txtHoraIngreso"></span>
                                                            Por <span id="txtAdmisorUrg"></span></em></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- COMIENZA CARD OTROS ANTECEDENTES --%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="otrosAntecedentes" class="card">
                                                    <div class="card-header bg-dark">
                                                        <h4 class="text-center">Otros antecedentes</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="container">
                                                            <%-- ANTECEDENTE MORBIDO --%>
                                                            <div class="row mt-1 mb-1">
                                                                <div class="col-md-12">
                                                                    <span id="divAntecedentesMorbidosUrg"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- FIN CARD OTROS ANTECEDENTES --%>
                                    </div>

                                    <%-- COMIENZA DATA DE CATEGORIZACION --%>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                        <div class="card">
                                            <div class="card-header bg-dark">
                                                <h4 class="text-center">Categorización</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-md-10">
                                                        <div id="divCategorizacion"></div>
                                                        <div id="divCategorizacionVacio" class="alert alert-warning text-center">
                                                            <strong>
                                                                <i class="fa fa-exclamation-triangle"></i>Sin categorización.
                                                            </strong>
                                                        </div>
                                                        <div id="divCategorizacionInfo">
                                                            <br />
                                                            <%-- OBSERVACIONES DE CATEGORIZACION --%>
                                                            <div class="row mt-1 mb-1" id="divObsCategorizacion">
                                                                <h6><strong>Observaciones:</strong></h6>
                                                                <textarea id="txtObsCategorizacion" class="form-control" rows="2" maxlength="200"
                                                                    style="background-color: transparent; border: none; padding: 0; resize: none; width: 100%; font-size: inherit; line-height: inherit; overflow: hidden;" readonly></textarea>
                                                            </div>
                                                            <%-- CATEGORIZADOR --%>
                                                            <div class="row mt-1 mb-1">
                                                                <h6><em>Categorización realizada <span id="txtFechaHoraCategorizacion"></span>
                                                                    <br>
                                                                    Por <span id="txtUsuarioCategorizador"></span></em></h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header bg-dark">
                                                <h4 class="text-center">Signos vitales</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="row border border-dark text-center rounded p-1 mb-1" id="ultimaTomaSignosUrg"></div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="divDetalleSignosVitalesUrg"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row d-flex flex-row justify-content-center flex-wrap w-100">
                                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 mb-2">
                                                        <a id="aSignosVitalesAgregar" class="btn btn-success btn-md w-100" onclick="linkSignosVitales(this)">Agregar</a>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                        <a id="aSignosVitalesHistorial" class="btn btn-primary btn-md w-100" onclick="modalHistorialSignosVitales()">Historial</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </div>

                        <%-- Historial de atenciones de urgencia del paciente --%>
                        <div class="card" id="divAtencionesUrgenciaPrevias">
                            <h5 class="card-header bg-dark" data-toggle="collapse" data-target="#AtencionesPreviasCollapse" aria-expanded="true" aria-controls="AtencionesPreviasCollapse">
                                <i class="fas fa-list"></i>&nbsp;Historial de atenciones 
                                 <span class="badge badge-info" id="spnCantidadAtencionesUrgencia">0</span>

                                <span id="collapsiveAtencionesPrevias" class="float-right"><i class="fa fa"></i></span>
                            </h5>
                            <div id="AtencionesPreviasCollapse" class="collapse show">
                                <div class="card-body">
                                    <div class="col-md-12 col-sm-12">
                                        <table id="tblHistorialHospitalizacionesPrevias" class="table table-bordered table-striped dataTable no-footer w-100">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- Fin Historial de atenciones de urgencia del paciente --%>


                        <%-- Anamnesis historial en vista principal --%>
                        <div class="card" id="anamnesis">
                            <h5 class="card-header bg-dark" data-toggle="collapse" data-target="#anamnesisCollapse" aria-expanded="true" aria-controls="anamnesisCollapse">
                                <i class="fas fa-stethoscope fa-lg pr-1"></i>Anamnesis y Examen físico
       
                                <span id="collapsiveInfoAnamnesis" class="float-right"><i class="fa fa"></i></span>
                            </h5>
                            <div id="anamnesisCollapse" class="collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <a class="btn btn-success btn-md w-100" id="aAnamnesis">Agregar Anamnesis y examen físico</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <div class="row pt-3" id="historialAnamnesisExamenFisico"
                                            style="border: solid 1px; border-color: #e8e8e89b; border-radius: 4px; background-color: #f6f6f6c0;">
                                            <!-- Inicio de la fila de anamnesis -->
                                            <div class="row">
                                                <!-- Contenido de la anamnesis -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- fin anamnesis historial --%>

                        <%-- Ocultando los demas divs si no existe anamnesis --%>

                        <div id="ocultarSinAnamnesis">
                            <div class="card" id="diagnosticos">
                                <h5 class="card-header bg-dark">
                                    <i class="fas fa-stethoscope fa-lg pr-1"></i>Hipótesis Diagnóstica
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div id="divAgregarDiagnostico" class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <a class="btn btn-success btn-md" onclick="modalDiagnosticoUrgencia()">Agregar</a>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblDiagnosticosUrgencia" style="width: 100%;" class="table table-bordered table-striped">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Acciones dentro de la atencion clinica-->
                            <div class="card" id="otrosDatos">
                                <h5 class="card-header bg-dark">
                                    <i class="fa fa-angle-double-right"></i>Otros datos
                                </h5>
                                <div class="card-body">
                                    <div class="row d-flex flex-row justify-content-center" style="background-color: #f6f6f6c0; border: solid 1px; border-color: #e8e8e89b; border-radius: 4px;">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 d-flex flex-column">
                                            <a id="aEvolucionesUrg" class="btn btn-outline-secondary m-2 p-3">Evoluciones</a>
                                            <a id="aAntecedentes" class="btn btn-outline-secondary m-2 p-3">Antecedentes</a>
                                            <a id="aViolencia" class="btn btn-outline-secondary btn-light m-2 p-3" onclick="modalViolencia">Violencia</a>
                                            <a id="aGinecoObstetrico" class="btn btn-outline-secondary m-2 p-3" onclick="modalGinecoObstetrico(true, true)">Gineco-Obstetrico</a>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 d-flex flex-column">
                                            <a id="aAlcoholemiaToxicologico" class="btn btn-outline-secondary m-2 p-3" onclick="modalAlcoholemiaToxicologico">Alcoholemia y Toxicológico</a>
                                            <a id="aSolicitudesExternas" class="btn btn-outline-secondary m-2 p-3">Solicitudes Externas</a>
                                            <a id="aMordedura" class="btn btn-outline-secondary m-2 p-3" onclick="modalMordedura">Mordedura</a>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 d-flex flex-column">
                                            <a id="aLpp" class="btn btn-outline-secondary m-2 p-3" onclick="modalLpp">LPP</a>
                                            <button type="button" id="aComentariosDau" onclick="mostrarModalComentarios(this)" class="btn btn-outline-secondary m-2 p-3">Comentarios <span id="spnCountComentarios"></span></button>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 d-flex flex-column">
                                            <a id="aValidarDau" onclick="validarDauPorMedico()" class="btn btn-success m-2 p-3"><i class="fa fa-check"></i>Validar DAU</a>
                                            <a id="aAltaMedica" class="btn btn-warning m-2 p-3">Dar el Alta</a>
                                            <a id="aCerrarAtencion" onclick="cargarDatosModalCierre()" class="btn btn-danger m-2 p-3">Cerrar la atención</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin div Acciones dentro de la atencion clinica-->

                            <div class="card" id="divGestionSolicitudes">
                                <div class="card-header bg-dark">
                                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Gestión de Solicitudes</strong></h5>
                                </div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Estamento/Profesión</label>
                                            <select id="sltProfesionEvolucionAtencion" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Profesional</label>
                                            <input id="txtProfesionalEvolucionAtencion" type="text" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>

                                    <div id="divIndicadoresIndicaciones" class="row mt-3 mb-3">
                                        <div class="col-md-12 text-center">
                                            <div class="btn-group flex-wrap" role="group" aria-label="Basic example">
                                                <button type="button" id="aProcedimientos" class="btn btn-atencion-clinica default text-center p-4 m-1 rounded">
                                                    <i class="fas fa-heartbeat fa-4x"></i>
                                                    <br />
                                                    <span class="mt-3">Procedimientos <span id="spProcedimientos" class="badge badge-light">0/0</span></span>
                                                </button>
                                                <button type="button" id="aMedicamentoBox" class="btn btn-atencion-clinica default text-center p-4 m-1 rounded" onclick="modalInsumosMedicamentos()">
                                                    <i class="fas fa-prescription-bottle-alt fa-4x"></i>
                                                    <br />
                                                    <span class="mt-3">Medicamentos <span id="spInsumosMedicamentos" class="badge badge-light">0/0</span></span>
                                                </button>
                                                <button type="button" id="aLaboratorio" class="btn btn-atencion-clinica default text-center p-4 m-1 rounded" onclick="mostrarModalExamenes(5)">
                                                    <i class="fas fa-microscope fa-4x"></i>
                                                    <br />
                                                    <span class="mt-3">Laboratorio <span id="spLaboratorio" class="badge badge-light">0/0</span></span>
                                                </button>
                                                <button type="button" id="aImagenologia" class="btn btn-atencion-clinica default teaSignosVitalesAgregarxt-center p-4 m-1 rounded" onclick="mostrarModalExamenes(4)">
                                                    <i class="fas fa-chalkboard-teacher fa-4x"></i>
                                                    <br />
                                                    <span class="mt-3">Imagenologia <span id="spExamenesImagenologia" class="badge badge-light">0/0</span></span>
                                                </button>
                                                <button type="button" id="aInterconsultor" class="btn btn-atencion-clinica default text-center p-4 m-1 rounded">
                                                    <i class="fas fa-user-md fa-4x"></i>
                                                    <br />
                                                    <span class="mt-3">Interconsultor <span id="spInterconsultor" class="badge badge-light">0/0</span></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divProcedimientos" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Procedimientos</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                    <button class="btn btn-info ml-auto w-100 d-flex flex-row justify-content-center flex-wrap" type="button" onclick="imprimirProcedimientosUrg(this)">
                                                        <span class="mr-2">Imprimir Procedimientos</span>
                                                        <i class="fa fa-print align-self-center"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive w-100" style="overflow: auto;">
                                                <table id="tblProcedimientos" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Servicio</th>
                                                            <th>Observaciones adicionales</th>
                                                            <th>Fecha solicitud</th>
                                                            <th>Fecha cierre</th>
                                                            <th class="text-center" style="width: 200px;">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divMedicamentos" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Medicamentos</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                    <button class="btn btn-info ml-auto w-100" type="button" onclick="imprimirMedicamentosUrg(this)"><span class="mr-2">Imprimir Medicamentos</span><i class="fa fa-print"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive w-100" style="overflow: auto;">
                                                <table id="tblMedicamentosBox" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Medicamento</th>
                                                            <th>Descripción</th>
                                                            <th>Fecha solicitud</th>
                                                            <th>Fecha cierre</th>
                                                            <th style="width: 200px;">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divExamenesLaboratorio" class="card border-dark mb-3" style="display: none;">
                                        <div class="card-header bg-light">
                                            <h3 class="mt-1 mb-1">Solicitudes de Exámenes de Laboratorio</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblExamenesLaboratorio" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divExamenesImagenologia" class="card border-dark mb-3" style="display: none;">
                                        <div class="card-header bg-light">
                                            <h3 class="mt-1 mb-1">Solicitudes de Exámenes de imageonología</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblExamenesImagenologia" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Inicio data table gineco --%>
                                    <div id="divHistorialGineco" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Gineco-Obstétrico</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblGinecoObstetrico" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Fecha</th>
                                                            <th>Motivo</th>
                                                            <th>Latidos cardio fetales</th>
                                                            <th>Ultima regla</th>
                                                            <th class='text-center'>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- Fin data table gineco --%>

                                    <%--inicio tabla integracion infinity--%>
                                    <div id="divIntegracionInfinity" class="card border-dark mb-3 d-none">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <h3 class="mt-1 mb-1">Listado de examenes por integración (INFINITY)</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblIntegracionInfinity" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%--fin inicio tabla integracion infinity--%>


                                    <%-- Inicio data table evoluciones --%>
                                    <div id="divHistorialEvoluciones" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Historial de Evoluciones</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                    <button class="btn btn-info ml-auto w-100" type="button" onclick="imprimirEvolucionUrg(this)"><span class="mr-2">Imprimir Evolución</span><i class="fas fa-print" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblEvolucionesUrg" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Fecha</th>
                                                            <th>Descripción</th>
                                                            <th>Profesional</th>
                                                            <th>IdProfesional</th>
                                                            <th class='text-center'>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- Fin data table evoluciones --%>

                                    <%-- Inicio data table altas --%>
                                    <div id="divHistorialAltas" class="card border-dark mb-3">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">Historial de Altas</h3>
                                            <button class="btn btn-info ml-auto" type="button" onclick="imprimirAltaUrg(this)" style="display: none;"><span class="mr-2">Imprimir Alta</span><i class="fas fa-file-medical-alt" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblAltasUrg" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Fin data table altas --%>

                                    <%-- Inicio data historial de formularios externos--%>

                                    <div id="divHistorialFormularios" class="card border-dark mb-3 d-none">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">Historial de Formulario Ind. Qx</h3>
                                            <button class="btn btn-info ml-auto" type="button" onclick="imprimirAltaUrg(this)" style="display: none;"><span class="mr-2">Imprimir Alta</span><i class="fas fa-file-medical-alt" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblHistorialFormulario" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Fin data table Formularios Ind Qx --%>

                                    <div id="divHistorialSolicitudesHospitalizacion" class="card border-dark mb-3">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">Historial de Solicitudes de Hospitalización</h3>
                                            <button class="btn btn-info ml-auto" type="button" onclick="imprimirAltaUrg(this)" style="display: none;"><span class="mr-2">Imprimir Alta</span><i class="fas fa-file-medical-alt" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblHistorialSolicitudesHospitalizacion" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divListaViolencias" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Violencia</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2" style="display: none;">
                                                    <button class="btn btn-info ml-auto w-100" type="button" onclick="imprimirViolenciaUrg(this)"><span class="mr-2">Imprimir Violencia</span><i class="fas fa-print" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <%--<table id="tblViolencias" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                            </table>--%>
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblViolencias" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Tipo de agresor</th>
                                                            <th>Tipo de lesión</th>
                                                            <th>Condición de la persona</th>
                                                            <th>Observaciones</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divListaAlcoholemias" class="card border-dark mb-3">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">Alcoholemia</h3>
                                            <%--<button class="btn btn-info ml-auto" type="button" onclick="imprimirAlcoholemiaUrg(this)"><span class="mr-2">Imprimir Alcoholemia</span><i class="fas fa-print" aria-hidden="true"></i></button>--%>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblAlcoholemia" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Boleta</th>
                                                            <th>Fecha</th>
                                                            <th>Apreciación</th>
                                                            <th class='text-center'>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divListaToxicologicos" class="card border-dark mb-3">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">Toxicológico</h3>
                                            <%--<button class="btn btn-info ml-auto" type="button" onclick="imprimirToxicologicoUrg(this)"><span class="mr-2">Imprimir Toxicológico</span><i class="fas fa-print" aria-hidden="true"></i></button>--%>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblToxicologico" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Boleta</th>
                                                            <th>Fecha</th>
                                                            <th class='text-center'>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divSolicitudInterconsultor" class="card border-dark mb-3">
                                        <div class="card-header bg-light ">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Solicitudes de Interconsultor</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                    <button class="btn btn-info ml-auto w-100" type="button" onclick="imprimirSolicitudInterconsultorUrg(this)"><span class="mr-2">Imprimir Solicitudes</span><i class="fa fa-print"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <div class="table-responsive" style="overflow: auto">
                                                        <table id="tblInterconsultores" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                            <thead>
                                                                <tr>
                                                                    <th>Id</th>
                                                                    <th>Especialidad</th>
                                                                    <th>Fecha solicitud</th>
                                                                    <th>Solicitud</th>
                                                                    <th>Fecha cierre</th>
                                                                    <th>Actividad</th>
                                                                    <th>Profesional</th>
                                                                    <th class='text-center'>Acciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Inicio data table mordeduras --%>
                                    <div id="divListaMordeduras" class="card border-dark mb-3">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                    <h3 class="mt-1 mb-1">Mordedura</h3>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                    <!-- impresion mordedura -->
                                                    <button class="btn btn-info ml-auto w-100" type="button" onclick="imprimirMordeduraUrg(this)"><span class="mr-2">Imprimir Mordedura</span><i class="fa fa-print"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto">
                                                <table id="tblMordeduras" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Tipo de animal</th>
                                                            <th>Tipo de mordedura</th>
                                                            <th>Indicación de vacuna</th>
                                                            <th>Código midas</th>
                                                            <th>Observaciones</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- Fin data table mordeduras --%>

                                    <%-- INICIO table LPP --%>
                                    <div id="divListaLpp" class="card border-dark mb-3">
                                        <div class="card-header bg-light d-flex">
                                            <h3 class="mt-1 mb-1">LPP</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblLpp" style="width: 100%" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Escala Braden</th>
                                                            <th>Valoración piel y mucosas</th>
                                                            <th>Medidas prevención</th>
                                                            <th>Observaciones</th>
                                                            <th>Fecha</th>
                                                            <th>Profesional</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- FIN table LPP --%>
                                    <%--inicio tabla validaciones medicas--%>
                                    <div id="divValidacionesMedicas" class="card border-dark mb-3 d-none">
                                        <div class="card-header bg-light">
                                            <div class="row d-flex flex-row justify-content-between flex-wrap">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <h3 class="mt-1 mb-1">Validaciones medicas</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="tblValidacionesMedicas" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%--fin inicio tabla validaciones medicas--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- CIERRE div que abarca si anamnesis se encuentra vacia --%>

                    <div class="card">
                        <h5 class="card-header bg-dark"><i class="fa fa-angle-double-right"></i>Datos de Profesional Médico</h5>
                        <div id="divDatosProfesionales" class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="txtNumeroDocumentoProfesional" class="active">Número Documento</label>
                                    <%--<input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled" />--%>
                                    <span id="txtNumeroDocumentoProfesional" class="form-control"></span>
                                </div>
                                <div class="col-md-3">
                                    <label class="active" for="txtNombreProfesional">Nombre/es</label>
                                    <%--<input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled" />--%>
                                    <span id="txtNombreProfesional" class="form-control"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="txtApePatProfesional" class="active">Primer Apellido</label>
                                    <%--<input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled" />--%>
                                    <span id="txtApePatProfesional" class="form-control"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="txtApeMatProfesional" class="active">Segundo Apellido</label>
                                    <%--<input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled" />--%>
                                    <span id="txtApeMatProfesional" class="form-control"></span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <div class="row d-flex flex-row justify-content-end flex-wrap">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-1">
                                    <a id="btnVolverBandejaUrgencia" class="btn btn-warning w-100">Salir <i class="fa fa-table"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <!---Inicio se la seccion de modales-->

    <!-- Modal Historial signos vitales -->
    <div class="modal" tabindex="-1" id="mdlHistorialSignosVitales">
        <div class="modal-dialog modal-100 mt-0">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2 class="modal-title"><strong>Historial Signos Vitales</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoHistorialSignosVitales">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8" id="containerHistorialSignos">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card mb-3">
                                        <div class="card-header bg-info d-flex flex-row">
                                            <div class="mr-auto bd-highlight"><span>Historial de signos vitales</span></div>
                                            <div class="bd-highlight">
                                                <button type="button" class="btn btn-primary btn-sm" onclick="toogleTablaHistorialSignos();">Ajustar</button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row mt-4">
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="table-responsive" style="overflow: auto;" id="tableResponsiveHistorialSignos">
                                                                <table id="tblHistorialSignosVitales" class="table table-bordered table-striped">
                                                                    <!-- se inserta la tabla -->
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4" id="datosPacienteHistorialSignos">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card border-secondary mb-3">
                                        <div class="card-header text-center bg-info">Datos de paciente</div>
                                        <div class="card-body">
                                            <div class="row" id="divHistorialSignosVitales">
                                                <!-- datos del paciente -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-between">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        <!-- Botón cerrar a la derecha -->
                        <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Historial signos vitales -->

    <!-- Modal ANAMNESIS -->
    <div class="modal fade" id="mdlAnamnesis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Agregue anamnesis y examen físico a la atención de urgencia</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div class="card">
                                <h5 class="card-header bg-dark">
                                    <i class="fas fa-stethoscope fa-lg pr-1"></i>Anamnesis y Examen fisico
                                </h5>
                                <div class="card-body">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row" id="divInfoAnamnesisExamenFisico">
                                            <div class="col-12">
                                                <label>Anamnesis</label>
                                                <textarea id="txtAnamnesis" class="form-control" rows="6" style="resize: none;" placeholder="Escriba anamnesis" data-required="true" maxlength="3000"></textarea>
                                            </div>
                                            <div class="col-12 mt-2">
                                                <label>Examen Físico</label>
                                                <textarea id="txtExamenFisico" class="form-control" rows="10" style="resize: none;" placeholder="Escriba Examen Físico" data-required="true" maxlength="1500"></textarea>
                                            </div>
                                            <div class="col-12 p-2">
                                                <button type="button" id="btnGuardarAnamnesis" class="btn btn-primary" onclick="guardarAnamnesis()"><i class="fa fa-save"></i>Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="card border-secondary mb-3" style="max-width: 100%;">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="datosPacienteAnamnesis">
                                        <!-- datos del paciente -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <!-- Nombre del usuario logueado a la izquierda -->
                    <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                    <!-- Botón cerrar a la derecha -->
                    <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal ANAMNESIS -->

    <!-- Modal Diagnostico urg -->
    <div class="modal fade" tabindex="-1" id="mdlDiagnosticoUrgencia">
        <div class="modal-dialog modal-100 mt-0">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h5 class="modal-title">Agregando diagnósticos a la atención de urgencia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoDiagnostico">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <wuc:DiagnosticoUrgencia ID="wuc_diagnosticoAtencionUrgencia" runat="server" />
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header text-center bg-info">Datos de paciente</div>
                                        <div class="card-body">
                                            <div class="row" id="divInfoPacienteDiagnosticos">
                                                <!-- Contenido de datos del paciente -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Botones para cerrar a la derecha -->
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <!-- Botones a la derecha -->
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <button id="btnGuardarDiagnostico" type="button" class="btn btn-success w-100">Guardar</button>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="aCerrarMdlDiagnostico" class="btn btn-warning w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Diagnostico urg -->

    <!-- Modal Violencia -->
    <div class="modal fade" id="mdlViolencia" role="dialog" aria-labelledby="mdlViolencia" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Violencia</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoViolencia">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <!-- Card AGRESOR -->
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">REM 08.G1</div>
                                <div class="card-body">
                                    <div id="divAgresor" class="row">
                                        <%-- SELECT PARA TIPO VIOLENCIA --%>
                                        <div class="col-md-12">
                                            <label for="agresor">Tipo de violencia:</label>
                                            <select id="sltTipoViolencia" class="form-control" data-required="true">
                                            </select>
                                        </div>

                                        <%-- SELECT PARA AGRESIONES DE LA VICTIMA --%>
                                        <div class="col-md-12" id="divTipoAgresor">
                                            <label for="agresor">Tipo de agresor:</label>
                                            <select id="sltTipoAgresor" class="form-control" data-required="true">
                                            </select>
                                        </div>
                                        <!-- Checkbox para controlar si hay o no lesiones -->
                                        <div class="col-md-6" id="">
                                            <div class="row" id="divtieneLesion">
                                                <div class="col-md-12">
                                                    <label for="chkTieneLesion">Lesiones constatables:</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="checkbox" id="chkTieneLesion" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>
                                        </div>
                                        <%-- SELECT PARA LESIONES DE LA VICTIMA --%>
                                        <div class="col-md-12" id="divLesiones" style="display: none;">
                                            <label id="labelTipoLesion" for="lesion">Tipo de lesión:</label>
                                            <select id="sltTipoLesion" class="form-control" title="Seleccione uno o varios tipos de lesion" data-width="100%" data-actions-box="true" multiple>
                                            </select>
                                        </div>

                                        <%-- SELECT PARA LA CONDICION --%>
                                        <div class="col-md-12">
                                            <label for="condicion">Tipo de condición persona:</label>
                                            <select id="sltTipoCondicion" class="form-control" data-required="true">
                                            </select>

                                        </div>

                                        <!-- CAMPO DE OBSERVACIONES -->
                                        <div class="col-md-12">
                                            <label for="observaciones">Observaciones:</label>
                                            <textarea id="observaciones" class="form-control" maxlength="100"></textarea>
                                            <br />
                                        </div>

                                        <!-- Campos para la violencia sexual -->
                                        <div id="camposViolenciaSexual" style="display: none;">
                                            <!-- CAMPO PARA GESTANTE -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="chkGestante">Gestante:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="chkGestante" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>
                                            <!-- CAMPO PARA ANTICONCEPCION -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="chkAnticoncepcion">Anticoncepción:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="chkAnticoncepcion" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>
                                            <!-- CAMPO PARA PROFILAXIS VIH -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="chkProfilaxisVIH">Profilaxis VIH:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="chkProfilaxisVIH" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>
                                            <!-- CAMPO PARA PROFILAXIS ITS -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="chkProfilaxisITS">Profilaxis ITS:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="chkProfilaxisITS" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>
                                            <!-- CAMPO PARA PROFILAXIS HEPATITIS -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="chkProfilaxisHepatitis">Profilaxis Hepatitis:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="chkProfilaxisHepatitis" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                        data-on-color="success" data-off-color="warning" data-size="normal" />
                                                </div>
                                            </div>

                                            <!-- CAMPO PARA TIPO TEMPORALIDAD -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="tipoTemporalidad">Tipo de Temporalidad:</label>
                                                    <select id="sltTipoTemporalidad" class="form-control" data-required="true"></select>
                                                </div>
                                                <!-- CAMPO PARA TIPO ATENCION MEDICA -->
                                                <div class="col-md-6">
                                                    <label for="tipoAtencionMedica">Tipo de Atención Médica:</label>
                                                    <select id="sltTipoAtencionMedica" class="form-control" data-required="true"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin del Card AGRESOR -->
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <!-- Datos de paciente -->
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="divPacienteViolencia">
                                        <!-- Contenido de datos de paciente -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Botón para guardar -->
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <!-- Botones a la derecha -->
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <button type="button" id="btnGuardarViolencia" class="btn btn-primary btn-md w-100" onclick="guardarViolencia()">Guardar</button>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="aCerrarViolencia" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Violencia -->

    <!-- Modal Gineco Obstetrico -->
    <div class="modal fade" id="mdlObstetrico" role="dialog" aria-labelledby="mdlObstetrico" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2 class="modal-title"><strong>Gineco Obstétrico</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoObstetrico">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div class="card border-secondary mb-3" id="">
                                <div class="card-header text-center bg-info">Datos Obstétrico</div>
                                <div class="card-body">
                                    <div class="row d-flex flex-row justify-content-between">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 mb-3">
                                            <label>Motivo emergencia obstétrica</label>
                                            <select id="sltMotivoEmergenciaObstetrica" class="form-control" data-required="true"></select>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 mb-3">
                                            <label>LCF (Latidos Cardio Fetales)</label>
                                            <input id="txtLatidosCardioFetalesObstetrico" type="number" class="form-control" placeholder="LCF" maxlength="3" />
                                        </div>
                                        <%--<div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 mb-3">
                                        <label>Fecha última regla</label>
                                        <input id="txtFechaUltimaRegla" type="date" class="form-control" />
                                    </div>--%>
                                    </div>
                                    <div class="row mt-2 d-flex flex-row justify-content-center align-items-center">
                                        <div class="col-sm-6 col-md-6 col-lg-12 col-xl-6 mb-3">
                                            <label for="txtAlturaUterina">Altura uterina</label>
                                            <input type="number" class="form-control" id="txtAlturaUterina" />
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-12 col-xl-6 mb-3">
                                            <label for="txtFlujoGenital">Flujo genital</label>
                                            <input type="checkbox" id="checkFlujoGenital" class="form-check" />
                                        </div>
                                    </div>
                                    <div class="row mt-2 d-flex flex-row justify-content-center align-items-center">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Última regla</label>
                                            <%--<input id="txtUltimaRegla" type="date" class="form-control" />--%>
                                            <textarea id="txtUltimaRegla" type="text" class="form-control" maxlength="50" rows="4" placeholder="Describa información sobre la última regla"></textarea>
                                        </div>

                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Actividad uterina</label>
                                            <%--<input id="txtUltimaRegla" type="date" class="form-control" />--%>
                                            <textarea id="txtActividadUterina" type="text" class="form-control" maxlength="300" rows="4" placeholder="Describa información sobre actividad uterina"></textarea>
                                        </div>
                                        <%--<div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                        <button class="btn btn-info w-100" disabled>Calculadora</button>
                                    </div>--%>
                                    </div>

                                    <div class="row mt-2 d-flex flex-row justify-content-center align-items-center">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Tacto vaginal</label>
                                            <textarea id="txtTactoVaginal" type="text" class="form-control" maxlength="300" rows="4" placeholder="Describa información sobre tacto vaginal"></textarea>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Especuloscopia</label>
                                            <textarea id="txtEspeculoscopia" type="text" class="form-control" maxlength="300" rows="4" placeholder="Describa información sobre especuloscopia"></textarea>
                                        </div>
                                    </div>

                                    <div class="row mt-2 d-flex flex-row justify-content-center align-items-center">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Monitoreo basal</label>
                                            <textarea id="txtMonitoreoBasal" type="text" class="form-control" maxlength="300" rows="4" placeholder="Describa información sobre monitoreo basal"></textarea>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
                                            <label>Ecografia</label>
                                            <textarea id="txtEcografia" type="text" class="form-control" maxlength="300" rows="4" placeholder="Describa información sobre ecografia"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="card border-secondary">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="datosPacienteObstetrico">
                                        <!-- Contenido de datos del paciente -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <button type="button" id="btnGuardarObstetrico" class="btn btn-primary btn-md w-100" onclick="guardarObstetrico(this)">Guardar</button>
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="aCerrarObstetrico" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Gineco Obstetrico -->

    <!--Modal Evoluciones-->
    <div class="modal fade" id="mdlEvolucionesUrg" role="dialog" aria-labelledby="mdlEvolucionesUrg" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Evolución</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card mb-3">
                                        <div class="card-header bg-info">
                                            <h3 id="tituloModalEvolucion" class="mt-1 mb-1">Ingresar Evolución</h3>
                                        </div>
                                        <div class="card-body">
                                            <%--<ul class="nav nav-tabs" id="tabEvolucionUrg" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="input-evolucion-tab" data-toggle="tab" data-target="#input-evolucion" type="button" role="tab" aria-controls="input-evolucion" aria-selected="true">Guardar</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="profile-tab-historial" data-toggle="tab" data-target="#historial-evoluciones" type="button" role="tab" aria-controls="profile" aria-selected="false">Historial</button>
                                            </li>
                                        </ul>--%>
                                            <div class="tab-content mt-2" id="tabEvolucionUrgContent">
                                                <div class="tab-pane fade show active" id="input-evolucion" role="tabpanel" aria-labelledby="input-evolucion">
                                                    <%--<div class="row mt-3">
                                                        <div class="col-sm-12 col-md-12 col-lg-8">
                                                            <label>Estamento/Profesión</label>
                                                            <span id="spnProfesionalEvolucionUrg" class="form-control" readonly=""></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md">
                                                            <label>Profesional</label>
                                                            <span id="spnNombreProfesionalUrg" class="form-control" readonly=""></span>
                                                        </div>
                                                    </div>--%>
                                                    <div class="row mt-2 mb-2" id="descEvoluciones">
                                                        <div class="col-md-12">
                                                            <label>Descripción de evolución</label>
                                                            <textarea id="txtEvolucionUrg" class="form-control" rows="10" data-required="true" placeholder="Describa la evolución del paciente"
                                                                maxlength="3000"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex justify-content-end">
                                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                            <div class="text-right mt-2 mb-4">
                                                                <button id="btnguardarEvolucionUrg" type="button" class="btn btn-success btn-md w-100" onclick="guardarEvolucionUrg(this)">Agregar Evolución</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--<div class="tab-pane fade mb-2" id="historial-evoluciones" role="tabpanel" aria-labelledby="historial-evoluciones">
                                                <table id="tblEvolucionesUrg" style="width: 100%;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                </table>
                                            </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- Datos de paciente --%>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-12">
                                    <div class="card border-secondary mb-3">
                                        <div class="card-header text-center bg-info">Datos de paciente</div>
                                        <div class="card-body">
                                            <div class="row" id="datosPacienteEvolucionUrg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                            <a id="aCerrarEvolucionesUrg" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fin Modal Evoluciones-->

    <!--Modal Antecedentes-->
    <div class="modal fade" id="mdlAntecedentesUrg" role="dialog" aria-labelledby="mdlAntecedentesUrg" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Antecedentes</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div id="divAntecedentesUrg" class="card">
                                <div class="card-header text-center bg-info">
                                    Antecedentes
                                </div>
                                <div class="card-body">

                                    <div id="divModalUrgenciaAntecedentes"></div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <strong>Alergias</strong><br />
                                            <input id="rdoAlergiaSi" type="radio" name="AL" class="bootstrapSwitch"
                                                data-on-text="SI"
                                                data-off-text="SI" data-on-color="primary" data-size="large"
                                                data-required="false" />
                                            <input id="rdoAlergiaNo" type="radio" name="AL" class="bootstrapSwitch"
                                                data-on-text="NO"
                                                data-off-text="NO" data-on-color="primary" data-size="large"
                                                data-required="false" />
                                            <input id="rdoAlergiaDesconocido" type="radio" name="AL"
                                                class="bootstrapSwitch" data-on-text="Desconocido"
                                                data-off-text="Desconocido" data-on-color="primary"
                                                data-size="large" data-required="false" />
                                        </div>
                                    </div>
                                    <div id="divModalUrgenciaCategorizacionAlergia"></div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="card">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="datosPacienteAntecedentesUrg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <%--<a id="btnGuardarAntecedentesUrg" class="btn btn-success btn-md w-100" onclick="guardarAntecedentesUrg(this)">Aceptar</a>--%>
                                <button id="btnGuardarAntecedentesUrg" class="btn btn-success btn-md w-100" onclick="guardarAntecedentesUrg(this)">Aceptar</button>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="aCerrarAntecedentes" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fin Modal Antecedentes-->

    <!--Modal Alcoholemia y Toxicologico-->
    <div class="modal fade" id="mdlAlcoholemiaToxicologico" role="dialog" aria-labelledby="mdlAlcoholemiaToxicologico" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Alcoholemia y Toxicológico</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoAlcoholemiaToxicologico">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <!-- Card Alcoholemia -->
                            <div class="card border-secondary mb-3" id="divCardAlcoholemia">
                                <div class="card-header text-center bg-info">Alcoholemia</div>
                                <div class="card-body">
                                    <div id="divAlcolemia" class="row mb-3">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <label>N° Boleta:</label>
                                            <input id="txtNumeroFrasco" type="number" class="form-control text-center" placeholder="N° Frasco" maxlength="1" data-required="true" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <label>Fecha:</label>
                                            <input id="txtFechaAlcoholemia" type="date" class="form-control" data-required="true" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                            <label>Hora:</label>
                                            <input id="txtHoraAlcoholemia" type="time" class="form-control" data-required="true" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                            <label>Apreciación Clínica:</label>
                                            <select id="sltApreciacionClinica" class="form-control" data-required="true"></select>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                            <button type="button" id="btnGuardarAlcoholemia" class="btn btn-primary btn-md w-100" onclick="guardarAlcoholemia(this)">Agregar Alcoholemía</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Toxicológico -->
                            <div class="card border-secondary mb-3" id="divCardToxicologico">
                                <div class="card-header text-center bg-info">Toxicológico</div>
                                <div class="card-body">
                                    <div id="divToxicologico" class="row mb-3">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <label>N° Boleta:</label>
                                            <input id="txtNumeroFrascoToxicologico" type="number" class="form-control text-center" placeholder="N° Frasco" maxlength="1" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <label>Fecha:</label>
                                            <input id="txtFechaToxicologico" type="date" class="form-control" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                            <label>Hora:</label>
                                            <input id="txtHoraToxicologico" type="time" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                            <button type="button" id="btnGuardarToxicologico" class="btn btn-primary btn-md w-100" onclick="guardarToxicologico(this)">Agregar Toxicológico</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <!-- Datos de paciente -->
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="divDatosPacienteAlcoholemiaToxicologico">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                            <a id="aCerrarAlcoholemiaToxicologico" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fin Modal Alcoholemia y Toxicologico-->

    <!--Modal Mordedura-->
    <div class="modal fade" id="mdlMordedura" role="dialog" aria-labelledby="mdlMordedura" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Mordedura</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="divInfoMordedura">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <!-- Card Mordedura -->
                            <div class="card border-secondary mb-3" id="divCardMordedura">
                                <div class="card-header text-center bg-info">Mordedura</div>
                                <div class="card-body">
                                    <div id="divMordedura" class="row mb-2">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 mb-2">
                                            <label>Tipo de animal</label>
                                            <select id="sltTipoAnimal" class="form-control" data-required="true"></select>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 mb-2">
                                            <label>Tipo de mordedura:</label>
                                            <select id="sltTipoMordedura" class="form-control" data-required="true"></select>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2 mb-2">
                                            <label for="chkVacunaMordedura">Vacuna:</label>
                                            <br />
                                            <input type="checkbox" id="chkVacunaMordedura" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                data-on-color="success" data-off-color="warning" data-size="normal" />
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 mb-2">
                                            <label>Codigo Midas:</label>
                                            <input id="codigoMidas" type="text" class="form-control" placeholder="Codigo midas" maxlength="11">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <label>Observaciones:</label>
                                            <textarea id="observacionesMordedura" class="form-control" placeholder="Observaciones" rows="4" maxlength="100"></textarea>
                                        </div>
                                    </div>
                                    <div class="row d-flex flex-row justify-content-end">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                            <button type="button" id="btnGuardarMordedura" class="btn btn-success btn-md w-100" onclick="guardarMordedura(this)">Agregar Mordedura</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <!-- Datos de paciente -->
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="divDatosPacienteMordedura">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                            <a id="aCerrarMordedura" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Fin modal Mordedura --%>

    <!--Modal Form Externos-->
    <div class="modal fade right" id="mdlFormulariosExternos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog modal-xl modal-top-right modal-notify" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">Formularios</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 mb-2">
                            <button id="btnNuevoFQX" type="button" class="btn btn-primary btn-block btn-lg h-100">
                                Formulario Ind Qx <i class="fa fa-h-square" aria-hidden="true"></i>
                            </button>
                        </div>
                        <!-- Nuevo botón de Solicitud transfusión -->
                        <%--<div class="col-md-4 mb-2">
                            <button id="btnNuevaSolicitudTrasfusion" type="button" class="btn btn-primary btn-block btn-lg h-100">
                                Solicitud transfusión <i class="fa fa-tint"></i>
                            </button>
                        </div>--%>
                        <div class="col-md-4 mb-2">
                            <button id="btnSolicitudDeHospitalizacion" type="button" class="btn btn-primary btn-block btn-lg h-100">
                                Solicitud de hospitalización <i class="fa fa-bookmark"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                            <a id="aCerrarFormsExternos" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Fin Modal Form Externos-->

    <%-- VER BIEN ESTOS MDL --%>
    <div id="mdlEvolucionDescripcionUrg" class="modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Descripción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="descripcionAltaUrg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="mdlAltaDescripcionUrg" class="modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Descripción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="descripcionEvolucionUrg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <%-- FIN VER BIEN ESTOS MDL --%>

    <!--Modal procedimientos-->
    <div class="modal fade" id="mdlProcedimientos" role="dialog" aria-labelledby="mdlProcedimientos" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-fluid mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Lista de Procedimientos</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!--Procedimientos favoritos-->
                        <div class="col-lg-5 col-md-5 col-xs-12">
                            <div class="container">
                                <div class="row d-flex justify-content-left">
                                    <div class="col-md-12">
                                        <label>Filtrar procedimientos <b class="color-error">(*)</b></label>
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thProcedimientos" name="hockey_v1[query]" type="search" placeholder="Buscar más procedimientos"
                                                        autocomplete="off" data-required="true" onclick="this.select();" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row d-flex justify-content-left">
                                    <div class="col-md-12">
                                        <table id="tblPod" class="table table-bordered table-striped">
                                            <thead class="text-center">
                                                <tr>
                                                    <th scope="col">Procedimiento</th>
                                                    <th scope="col">Eliminar</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyProd"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row d-flex justify-content-end w-100">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2 mb-2">
                                        <button type="button" id="aAceptarProcedimientosUrg" class="btn btn-success btn-md w-100" onclick="guardarProcedimientosUrg(this)">Aceptar</button>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2 mb-4">
                                        <button type="button" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-7 col-md-7 col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3>
                                        <strong><i class="fa fa-solid fa-star"></i>Favoritos</strong>
                                    </h3>
                                </div>
                                <div class="card-body p-1">
                                    <div id="accordion2"></div>
                                    <div id="divListaProcedimientosModal" class="row"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--fin modal procedimientos-->

    <!--Modal Medicamentos-->
    <div class="modal fade" id="mdlMedicamentos" role="dialog" aria-labelledby="mdlProcedimientos" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Lista de Medicamentos</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div id="divInsumosMedicamentos" class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card mb-3">
                                            <div class="card-header bg-info">
                                                <h3 class="mt-1 mb-1">Medicamentos</h3>
                                            </div>
                                            <div class="card-body">
                                                <label>Medicamentos <b class="color-error">(*)</b></label>
                                                <div class="typeahead__container">
                                                    <div class="typeahead__field">
                                                        <div class="typeahead__query">
                                                            <input id="thMedicamentos" name="hockey_v1[query]" type="search" placeholder="Agregar/Buscar Medicamentos"
                                                                autocomplete="off" onclick="this.select();" data-required="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- Primera parte -->
                                                    <div class="col-md-12 mt-3">
                                                        <div>
                                                            <label>Dosis única</label>
                                                        </div>
                                                        <input type="checkbox" id="chkDosisUnica" class="bootstrapSwitch" data-on-text="SI" data-off-text="NO"
                                                            data-on-color="success" data-off-color="warning" data-size="normal" />
                                                    </div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-sm">
                                                        <label>Dosis</label>
                                                        <input id="txtDosisMedicamento" class="form-control" placeholder="Especifique dosis"
                                                            type="text" maxlength="100" data-required="true" />
                                                    </div>
                                                </div>
                                                <div class="row mt-3">
                                                    <!-- Tercera parte -->
                                                    <div class="col-lg-6">
                                                        <label id="lblFrecuencia">Frecuencia</label>
                                                        <input id="txtFrecuenciaMedicamento" class="form-control" placeholder="Especifique frecuencia"
                                                            type="text" maxlength="100" data-required="true" />
                                                    </div>

                                                    <!-- Cuarta parte -->
                                                    <div class="col-lg-6">
                                                        <label>Vía de Administración</label>
                                                        <select id="sltViaAdministracion" class="form-control" data-required="true">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="divObservaciones" class="row mt-3">
                                                    <div class="col-md-12">
                                                        <label>Observaciones:</label>
                                                        <textarea id="txtObservaciones" rows="4" maxlength="500"
                                                            class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row mt-3 d-flex flex-row justify-content-end flex-wrap">
                                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-3">
                                                        <%--<a id="aGuardarEditarMedicamento" class="btn btn-success btn-lg"><i class="fa fa-save"></i>Guardar Edición</a>--%>
                                                        <a id="aAgregarMedicamento" class="btn btn-success btn-md w-100" data-accion="Agregar" onclick="agregarMedicamentoModalUrg(this)"><i class="fa fa-plus mr-1"></i>Agregar</a>
                                                    </div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-lg-12 modal-contentenido">
                                                        <table id="tblMedicamentos" class="table table-bordered table-striped">
                                                            <thead class="text-center">
                                                                <tr class="text-center">
                                                                    <th>Medicamento</th>
                                                                    <th>Dosis</th>
                                                                    <th>Frecuencia</th>
                                                                    <th>Vía</th>
                                                                    <th>Observaciones</th>
                                                                    <th>Quitar</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbodyMedicamentos"></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="datosPacienteMedicamentos">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex justify-content-between align-items-center">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end align-items-center">
                            <button id="aAceptarMedicamentosUrg" class="btn btn-success btn-md mr-2" onclick="guardarMedicamentosUrg()">Guardar</button>
                            <a id="aCerrarModalMedicamentosUrg" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Fin Modal Medicamentos-->

    <!--Modal examenes laboratorio-->
    <div class="modal fade" id="mdlExamenes" role="dialog" aria-labelledby="mdlExamenes" data-idtipoarancel="5" aria-hidden="true" tabindex="0"
        data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-fluid mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Ingresando una solicitud de Exámenes de Laboratorio</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-5 pl-5">
                    <div class="row d-flex flex-row justify-content-center flex-wrap">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5 mb-3 p-3">
                            <div id="divExamLab">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8 mb-3">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thExamenesLaboratorio" name="hockey_v1[query]" type="search" placeholder="Busque acá los examenes de laboratorio.."
                                                        autocomplete="off" data-required="true" onclick="this.select();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                        <button id="aAceptarExamenesLaboratorioModal" type="button" onclick='validarExamenIngresado(5, "#thExamenesLaboratorio")' class="btn btn-success w-100">Aceptar selección</button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive mt-2" style="overflow: auto;">
                                <table id="tblExamLab" class="table table-bordered table-striped mt-4">
                                    <caption>Listado de examenes seleccionados <i class="fa fa-arrow-up"></i></caption>
                                    <thead class="text-center">
                                        <tr>
                                            <th scope="col">Nombre examen</th>
                                            <th scope="col" class="w-25">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyExamLab"></tbody>
                                </table>
                            </div>
                            <div class="col-12">
                                <label><b>Observaciones de la solicitud</b></label>
                                <textarea id="txtObservacionLaboratorio" class="form-control w-100" style="resize: none;" rows="4" maxlength="150"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-7 p-3">
                            <div class="card p-3">
                                <div class="row d-flex flex-row justify-content-center">
                                    <div class="col-12">
                                        <p><i class="fa fa-info-circle mr-1"></i>En esta sección encuentra acceso rápido a los exámenes que se solicitan con mayor frecuencia, clic en el examen que quiera agregar a la solicitud</p>
                                    </div>
                                </div>
                                <div id="divModalExamenesLaboratorio" class="row"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 mb-2 d-flex justify-content-end">
                            <button id="btnSolicitudExamenesUrg" type="button" onclick="guardarSolicitud(5)" class="btn btn-success mr-2 w-50">Guardar solicitud</button>
                            <a id="aCerrarMdlExamenLab" class="btn btn-warning w-50" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fin modal examenes laboratorio -->

    <!--Modal imageneologia-->
    <div class="modal fade" id="mdlImagenologia" role="dialog" aria-labelledby="mdlExamenes" data-idtipoarancel="4" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-fluid mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Ingresando solicitud de Exámenes de Imagenologia</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body pr-5 pl-5">

                    <div class="row d-flex flex-row justify-content-center flex-wrap">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5 mb-3 p-3">
                            <p>Seleccione uno o más examenes que va a solicitar y posteriormente guarde la solicitud.</p>
                            <div id="divExamLabImg">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8 mb-3">
                                        <div class="typeahead__container">
                                            <div class="typeahead__field">
                                                <div class="typeahead__query">
                                                    <input id="thExamenesImagenologia" name="hockey_v1[query]" type="search" placeholder="Busque acá los examenes de imagenologia.."
                                                        autocomplete="off" data-required="true" onclick="this.select();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                        <button id="aAceptarExamenesImagenModal" type="button" onclick='validarExamenIngresado(4, "#thExamenesImagenologia")' class="btn btn-success w-100">Aceptar selección</button>
                                    </div>
                                </div>
                            </div>

                            <table id="tblExamImg" class="table table-bordered">
                                <caption>Listado de examenes seleccionados <i class="fa fa-arrow-up"></i></caption>
                                <thead class="text-center">
                                    <tr>
                                        <th scope="col">Nombre examen</th>
                                        <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyExamImg"></tbody>
                            </table>
                            <div class="row" id="infoAtencionImagen">
                                <div class="col-12" id="divSltTipoAtencionImagen">
                                    <label>Seleccione el tipo solicitud</label>
                                    <select class="form-control" id="sltTipoAtencionImagen" data-required="true">
                                    </select>
                                </div>
                                <div class="col-6" id="divMedicamentosTomografia" style="display: none;">
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="checkMetformina">
                                        <label class="form-check-label" for="checkMetformina">Metformina</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="checkLosartan">
                                        <label class="form-check-label" for="checkLosartan">Losartan</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="checkEnalapril">
                                        <label class="form-check-label" for="checkEnalapril">Enalapril</label>
                                    </div>
                                </div>

                                <div class="col-6" id="divAlergiasTomografia" style="display: none;">
                                    <label><b>Indique reacciones alergicas graves.</b></label>
                                    <textarea id="txtAlergiasTomografia" class="form-control w-100" style="resize: none;" rows="3" maxlength="100"></textarea>
                                </div>

                                <div class="col-12">
                                    <label>Observaciones de la solicitud</label>
                                    <textarea id="txtObservacionImagen" class="form-control w-100" data-required="true" rows="3" style="resize: none;" maxlength="150"></textarea>
                                </div>

                                <%--<div class="text-right mt-2">
                                    <button id="btnSolicitudExamenImagenologiaUrg" type="button" onclick="guardarSolicitud(4)" class="btn btn-success">Guardar solicitud de examen</button>
                                </div>--%>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-7 p-3">
                            <div class="card p-3">
                                <div class="row d-flex flex-row justify-content-center">
                                    <div class="col-12">
                                        <p><i class="fa fa-info-circle mr-1"></i>En esta sección encuentra acceso rápido a los exámenes que se solicitan con mayor frecuencia, clic en el examen que quiera agregar a la solicitud</p>
                                    </div>
                                </div>
                                <div id="divModalExamenesImagenologia" class="row"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 mb-2 d-flex justify-content-end">
                            <button id="btnGuardarSolicitudExamenImagenologiaUrg" type="button" onclick="guardarSolicitud(4)" class="btn btn-success mr-2 w-50">Guardar solicitud</button>
                            <a id="aCerrarMdlExamenImg" class="btn btn-warning w-50" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--fin modal imageologia-->

    <!--Modal Interconsultor-->
    <div class="modal fade" id="mdlInterconsultorUrg" role="dialog" aria-labelledby="mdlInterconsultorUrg" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h4 id="tituloInterconsultorUrg"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                            <div id="divInterconsultorUrg" class="card">
                                <div class="card-header text-center bg-info">
                                    Solicitud Interconsultor
                                </div>
                                <div class="card-body">
                                    <div id="divFechaSolicitanteIterconsultor" class="row mt-2">
                                        <div class="col-lg-4">
                                            <label>Fecha de solicitud</label>
                                            <input id="txtFechaSolicitudInterconsultor" type="text" class="form-control" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Solicitante</label>
                                            <input id="txtSolicitanteInterconsultor" type="text" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-10">
                                            <label>Especialidad</label>
                                            <select id="sltTipoConsultorUrg" class="form-control" data-required="true" disabled="disabled">
                                            </select>
                                        </div>
                                    </div>
                                    <div id="divMedicoInterconsultor" class="row mt-2">
                                        <div class="col-md-8">
                                            <label>Medico Interconsultor <b class="color-error">(*)</b></label>
                                            <div class="typeahead__container">
                                                <div class="typeahead__field">
                                                    <div class="typeahead__query">
                                                        <input id="sltMedicoInterconsultor" name="hockey_v1[query]" type="search" placeholder="Buscar Médicos"
                                                            autocomplete="off" onclick="this.select();" data-required="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divProfesionalRealizaInterconsultor" class="row mt-2">
                                        <div class="col-lg-10">
                                            <label>Profesional que cierra</label>
                                            <input id="txtProfesionalRealizaInterconsultor" type="text" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label>Solicitud</label>
                                            <textarea id="txtSolicitudInterconsultorUrg" rows="4" maxlength="3000"
                                                class="form-control" data-required="true" disabled="disabled"></textarea>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="divAtencionInterconsultorUrg">
                                        <div class="col-sm">
                                            <label>Atención</label>
                                            <textarea id="txtAtencionInterconsultorUrg" rows="4" maxlength="3000"
                                                class="form-control" data-required="true"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                            <div class="card">
                                <div class="card-header text-center bg-info">Datos de paciente</div>
                                <div class="card-body">
                                    <div class="row" id="datosPacienteInterconsultor">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 mb-2 d-flex justify-content-end">
                            <button id="btnGuardarInterconsultorUrg" class="btn btn-success mr-2 w-50" onclick="guardarInterconsultorUrg(this)">Guardar</button>
                            <a id="aCerrarInterconsultor" class="btn btn-warning w-50" data-dismiss="modal">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Interconsultor-->

    <%--<div id="mdlDatosAdicionales" class="modal fade" role="dialog" aria-labelledby="mdlDatosAdicionales" aria-hidden="true" tabindex="1">
    <div class="modal-dialog modal-lg mt-0" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <h4><strong>Seleccione</strong></h4>
                <button type="button" id="btnCerrarMdlDatosAdicionales" onclick="cerrarModalSobrePuesto(this)" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">x</span>
                </button>
            </div>
            <div class="modal-body p-4"></div>
            <div class="modal-footer p-4 text-right"></div>
        </div>
    </div>
    </div>--%>

    <%-- MODAL PARA VER ACCION REALIZADA DE LABORATORIO --%>
    <div id="mdlExamenLaboratorio" class="modal fade" aria-labelledby="mdlExamenLaboratorio" role="dialog" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-lg modal-dialog-centered mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Detalles de la Solicitud de Examen</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div id="divDetalleLaboratorio"></div>
                    <div id="divObservacionesCierreLaboratorio" class="mt-2">
                        <label>Observaciones de cierre:</label>
                        <textarea id="txtObservacionesCierreLaboratorio" class="form-control" rows="4" placeholder="Escriba observaciones acerca del caso"
                            maxlength="500">
                        </textarea>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <%--<a id="btnCerrarExamen" class="btn btn-info">
                        <i class="fas fa-check"></i>Cerrar Examen
                    </a>--%>
                    <button id="btnCerrarExamen" class="btn btn-info"><i class="fas fa-check"></i>Cerrar Examen</button>
                    <a class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <%-- FIN MODAL REALIZAR NO REALIZAR LABORATORIO --%>

    <div id="mdlCerrarCarteraServicio" class="modal fade" aria-labelledby="mdlCerrarCarteraServicio" role="dialog" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-lg modal-dialog-centered mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cierre de procedimiento</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div id="divDetalleCarteraArancel"></div>
                    <div id="divObservacionesCierreProcedimientoUrg" class="mt-2">
                        <label>Observaciones de cierre:</label>
                        <textarea id="txtObservacionesCarteraArancel" class="form-control" rows="4" placeholder="Escriba observaciones acerca del caso"
                            maxlength="500">
                            </textarea>
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <%--<a id="aCerrarCarteraArancel" class="btn btn-info">
                        <i class="fas fa-check"></i>Cerrar Procedimiento
                    </a>--%>
                    <button id="aCerrarCarteraArancel" class="btn btn-info">
                        <i class="fas fa-check"></i>Cerrar Procedimiento
                    </button>
                    <a class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlCerrarMedicamentoUrg" class="modal fade" aria-labelledby="mdlCerrarMedicamentoUrg" role="dialog" aria-hidden="true" tabindex="0">
        <div class="modal-dialog modal-lg modal-dialog-centered mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cierre de Medicamento</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div id="divDetalleMedicamento"></div>
                    <div class="row">
                        <div class="col-md">
                            <label>Observaciones de Ingreso:</label>
                            <textarea id="txtObservacionesMedicamento" class="form-control" rows="4" placeholder="Escriba observaciones acerca del caso"
                                maxlength="500" readonly></textarea>
                        </div>
                    </div>
                    <div class="row" id="divObservacionesCierreMedicamentoUrg">
                        <div class="col-md">
                            <label>Observaciones de Cierre:</label>
                            <textarea id="txtObservacionesCierreMedicamento" class="form-control" rows="4" placeholder="Escriba observaciones acerca del caso"
                                maxlength="500"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                        </div>
                    </div>

                </div>
                <div class="modal-footer text-right">
                    <%--<a id="aCerrarMedicamento" class="btn btn-info">
                        <i class="fas fa-check"></i>Cerrar Medicamento
                    </a>--%>
                    <button id="aCerrarMedicamento" class="btn btn-info"><i class="fas fa-check"></i>Cerrar Medicamento</button>
                    <a class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <%-- MODAL LPP ADULTO --%>
    <div class="modal fade" id="mdlLppAdulto" role="dialog" aria-labelledby="mdlLppAdulto" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-100 mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Prevención de Lesión por Presión (LPP)</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Tabs and Content -->
                        <div class="col-lg-9">
                            <!-- Tabs -->
                            <ul class="nav nav-tabs" id="lppTabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="escalaBradenTab" data-toggle="tab" href="#escalaBraden" role="tab" aria-controls="escalaBraden" aria-selected="true">Escala Braden</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="valoracionPielTab" data-toggle="tab" href="#valoracionPiel" role="tab" aria-controls="valoracionPiel" aria-selected="false">Valoración Piel y Mucosas</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="medidasPrevencionTab" data-toggle="tab" href="#medidasPrevencion" role="tab" aria-controls="medidasPrevencion" aria-selected="false">Medidas de Prevención en pacientes de riesgo moderado y alto</a>
                                </li>
                            </ul>
                            <!-- Tab Content -->
                            <div class="tab-content mt-3" id="lppTabsContent">
                                <!-- Escala Braden -->
                                <div class="tab-pane fade show active" id="escalaBraden" role="tabpanel" aria-labelledby="escalaBradenTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 500px;">
                                        <div class="card-header text-center bg-info">Escala Braden</div>
                                        <div class="card-body flex-grow-1">
                                            <div id="divEscalaBradenAdulto" class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>PERCEPCIÓN SENSORIAL</strong></label>
                                                    <div id="grpPercepcionSensorialAdulto" class="radio-container"></div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>HUMEDAD</strong></label>
                                                    <div id="grpHumedadAdulto" class="radio-container"></div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>ACTIVIDAD</strong></label>
                                                    <div id="grpActividadAdulto" class="radio-container"></div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>MOVILIDAD</strong></label>
                                                    <div id="grpMovilidadAdulto" class="radio-container"></div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>NUTRICIÓN</strong></label>
                                                    <div id="grpNutricionAdulto" class="radio-container"></div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label class="text-dark"><strong>FUERZA</strong></label>
                                                    <div id="grpFuerzaAdulto" class="radio-container"></div>
                                                </div>
                                            </div>

                                            <!-- Puntaje -->
                                            <div class="row" id="puntajeAdulto" style="font-size: 0.7em; margin-bottom: 0rem;">
                                                <div class="col-md-6 d-flex align-items-center p-0">
                                                    <h5 class="text-left mb-0" style="font-size: 1.5em;">Puntaje:</h5>
                                                    <div class="lblTotalPuntaje ml-2" style="font-size: 1.5em; font-weight: bold;">0</div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="text-center mt-4 card-footer">
                                            <a id="aSiguienteDatosGeneralesLpp" onclick="$('#valoracionPielTab').trigger('click')" class="btn btn-light">Siguiente <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Valoración Piel y Mucosas -->
                                <div class="tab-pane fade" id="valoracionPiel" role="tabpanel" aria-labelledby="valoracionPielTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 500px;">
                                        <div class="card-header text-center bg-info">Valoración Piel y Mucosas</div>
                                        <div class="card-body flex-grow-1">
                                            <!-- AQUI CHECKBOX -->
                                            <div class="row" id="valoracionPielRadiosAdulto">
                                                <!-- Espacio para agregar campos de valoración piel -->
                                            </div>
                                        </div>
                                        <!-- Pie de página en la parte inferior -->
                                        <div class="text-center mt-4 card-footer mt-auto">
                                            <div class="col-12 d-flex justify-content-center flex-wrap">
                                                <a onclick="$('#escalaBradenTab').trigger('click')" class="btn btn-light m-1">
                                                    <i class="fa fa-angle-left"></i>Atrás
                                                </a>
                                                <a id="aSiguienteTabPielMucosas" onclick="$('#medidasPrevencionTab').trigger('click')" class="btn btn-light m-1">Siguiente <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Medidas prevención -->
                                <div class="tab-pane fade" id="medidasPrevencion" role="tabpanel" aria-labelledby="medidasPrevencionTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 200px;">
                                        <div class="card-header text-center bg-info">Medidas de Prevención en pacientes de riesgo moderado y alto</div>
                                        <div class="card-body flex-grow-1">
                                            <!-- AQUI CHECKBOX -->
                                            <div class="row" id="medidasPrevencionRadiosAdulto">
                                                <!-- Espacio para agregar medidas prevención -->
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divObservacionesLpp" class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 100px;">
                                        <div class="card-header text-center bg-info">Observaciones</div>
                                        <div class="card-body flex-grow-1">

                                            <!-- OBSERVACIONES ADULTOS -->
                                            <div class="form-group" id="actividadesAdulto">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Cambio de</label>
                                                        <select id="sltActividadAdulto" class="form-control">
                                                            <option value="posicion">Posición</option>
                                                            <option value="panales">Pañales</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Observaciones</label>
                                                        <input id="txtObservacionesActividadesAdulto" type="text" class="form-control" data-required="true" placeholder="Escriba observaciones adicionales aquí..." />
                                                    </div>
                                                    <div class="col-sm-3 d-flex align-items-end">
                                                        <button id="btnAgregarActividadAdulto" type="button" class="btn btn-info btn-block">Agregar</button>
                                                    </div>
                                                </div>

                                                <div class="mt-3">
                                                    <table id="tblActividadesAdulto" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th>Fecha</th>
                                                                <th>Cambio de</th>
                                                                <th>Observaciones</th>
                                                                <th>Profesional</th>
                                                                <th>Eliminar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <%-- OBSERVACIONES GINE --%>
                                            <div class="form-group" id="actividadesGine">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Observaciones</label>
                                                        <textarea id="txtObservacionesActividadesGine" class="form-control" rows="4" placeholder="Escriba observaciones aquí..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center mt-4 card-footer mt-auto">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex flex-row justify-content-center flex-wrap">
                                            <a onclick="$('#valoracionPielTab').trigger('click')" class="btn btn-light m-1">
                                                <i class="fa fa-angle-left"></i>Atrás
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Datos del Paciente Section -->
                        <div class="col-lg-3">
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos del Paciente</div>
                                <div class="card-body" id="divDatosPacienteLppAdulto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Botón para guardar -->
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <!-- Botones a la derecha -->
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <button type="button" class="btn btn-primary btn-md w-100 btnGuardarLpp" onclick="guardarDatosLpp()">Guardar</button>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal LPP ADULTO -->

    <%-- MODAL LPP PEDIATRICO--%>
    <div class="modal fade" id="mdlLppPediatrico" role="dialog" aria-labelledby="mdlLppPediatrico" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-100 mt-0" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h2><strong>Prevención de Lesión por Presión (LPP)</strong></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Tabs and Content -->
                        <div class="col-lg-9">
                            <!-- Tabs -->
                            <ul class="nav nav-tabs" id="lppPediatricoTabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="escalaBradenPediatricoTab" data-toggle="tab" href="#escalaBradenPediatrico" role="tab" aria-controls="escalaBradenPediatrico" aria-selected="true">Escala Braden</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="valoracionPielPediatricoTab" data-toggle="tab" href="#valoracionPielPediatrico" role="tab" aria-controls="valoracionPielPediatrico" aria-selected="false">Valoración Piel y Mucosas</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="medidasPrevencionPediatricoTab" data-toggle="tab" href="#medidasPrevencionPediatrico" role="tab" aria-controls="medidasPrevencionPediatrico" aria-selected="false">Medidas de Prevención en pacientes de riesgo moderado y alto</a>
                                </li>
                            </ul>
                            <!-- Tab Content -->
                            <div class="tab-content mt-3" id="lppPediatricoTabsContent">
                                <!-- Escala Braden -->
                                <div class="tab-pane fade show active" id="escalaBradenPediatrico" role="tabpanel" aria-labelledby="escalaBradenPediatricoTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 685px;">
                                        <div class="card-header text-center bg-info">Escala Braden</div>
                                        <div id="divEscalaBradenPediatrico" class="card-body flex-grow-1">
                                            <!-- Intensidad y duración de la presión -->
                                            <div class="card border-secondary mb-3">
                                                <div class="card-header text-center bg-primary">Intensidad y duración de la presión</div>
                                                <div class="card-body">
                                                    <div class="row mb-3">
                                                        <div class="col-md-4">
                                                            <label class="text-dark"><strong>PERCEPCIÓN SENSORIAL</strong></label>
                                                            <div id="grpPercepcionSensorialPediatrico" class="radio-container"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="text-dark"><strong>ACTIVIDAD</strong></label>
                                                            <div id="grpActividadPediatrico" class="radio-container"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="text-dark"><strong>MOVILIDAD</strong></label>
                                                            <div id="grpMovilidadPediatrico" class="radio-container"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Tolerancia de la piel y la estructura de soporte -->
                                            <div class="card border-secondary mb-3">
                                                <div class="card-header text-center bg-primary">Tolerancia de la piel y la estructura de soporte</div>
                                                <div class="card-body">
                                                    <div class="row mb-3">
                                                        <div class="col-md-3">
                                                            <label class="text-dark"><strong>HUMEDAD</strong></label>
                                                            <div id="grpHumedadPediatrico" class="radio-container"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="text-dark"><strong>FRICCIÓN Y CIZALLAMIENTO</strong></label>
                                                            <div id="grpFriccionCizallamientoPediatrico" class="radio-container"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="text-dark"><strong>NUTRICIÓN</strong></label>
                                                            <div id="grpNutricionPediatrico" class="radio-container"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="text-dark"><strong>PERFUSIÓN TISULAR Y OXIGENACIÓN</strong></label>
                                                            <div id="grpPerfusionOxigenacionPediatrico" class="radio-container"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" id="puntajePediatrico" style="font-size: 0.7em; margin-bottom: 0rem;">
                                                <div class="col-md-6 d-flex align-items-center p-0">
                                                    <h5 class="text-left mb-0" style="font-size: 1.5em;">Puntaje:</h5>
                                                    <div class="lblTotalPuntaje ml-2" style="font-size: 1.5em; font-weight: bold;">0</div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Pie de página en la parte inferior -->
                                        <div class="text-center mt-4 card-footer mt-auto">
                                            <div class="col-12 d-flex justify-content-center flex-wrap">
                                                <a id="aSiguientePediatricoDatosGeneralesLpp" onclick="$('#valoracionPielPediatricoTab').trigger('click')" class="btn btn-light m-1">Siguiente <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Valoración Piel y Mucosas -->
                                <div class="tab-pane fade" id="valoracionPielPediatrico" role="tabpanel" aria-labelledby="valoracionPielPediatricoTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 685px;">
                                        <div class="card-header text-center bg-info">Valoración Piel y Mucosas</div>
                                        <div class="card-body flex-grow-1">
                                            <!-- AQUI CHECKBOX -->
                                            <div class="row" id="valoracionPielRadiosPediatrico">
                                                <!-- Espacio para agregar campos de valoracion piel -->
                                            </div>
                                        </div>
                                        <div class="text-center mt-4 card-footer">
                                            <div class="col-12 d-flex justify-content-center flex-wrap">
                                                <a onclick="$('#escalaBradenPediatricoTab').trigger('click')"
                                                    class="btn btn-light m-1"><i class="fa fa-angle-left"></i>Atrás</a>
                                                <a id="aSiguienteTabPediatricoPielMucosas" onclick="$('#medidasPrevencionPediatricoTab').trigger('click')" class="btn btn-light m-1">Siguiente <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Medidas prevención -->
                                <div class="tab-pane fade" id="medidasPrevencionPediatrico" role="tabpanel" aria-labelledby="medidasPrevencionPediatricoTab">
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 234px;">
                                        <div class="card-header text-center bg-info">Medidas de Prevención en pacientes de riesgo moderado y alto</div>
                                        <div class="card-body flex-grow-1">
                                            <!-- AQUI CHECKBOX -->
                                            <div class="row" id="medidasPrevencionRadiosPediatrico">
                                                <!-- Espacio para agregar medidas prevención-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card border-secondary mb-3 w-100 d-flex flex-column" style="min-height: 365px;">
                                        <div class="card-header text-center bg-info">Observaciones</div>
                                        <div class="card-body flex-grow-1">

                                            <!-- OBSERVACIONES -->
                                            <div class="form-group" id="actividadesPediatrico">

                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Cambio de</label>
                                                        <select id="sltActividadPediatrico" class="form-control">
                                                            <option value="posicion">Posición</option>
                                                            <option value="panales">Pañales</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Observaciones</label>
                                                        <input id="txtObservacionesActividadesPediatrico" type="text" class="form-control" data-required="true" placeholder="Escriba observaciones adicionales aquí..." />
                                                    </div>
                                                    <div class="col-sm-3 d-flex align-items-end">
                                                        <button id="btnAgregarActividadPediatrico" type="button" class="btn btn-info btn-block">Agregar</button>
                                                    </div>
                                                </div>

                                                <div class="mt-3">
                                                    <table id="tblActividadesPediatrico" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th>Fecha</th>
                                                                <th>Cambio de</th>
                                                                <th>Observaciones</th>
                                                                <th>Profesional</th>
                                                                <th>Eliminar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center mt-4 card-footer mt-auto">
                                        <div class="col-12 d-flex justify-content-center flex-wrap">
                                            <a onclick="$('#valoracionPielPediatricoTab').trigger('click')" class="btn btn-light m-1">
                                                <i class="fa fa-angle-left"></i>Atrás
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Datos del Paciente Section -->
                        <div class="col-lg-3">
                            <div class="card border-secondary mb-3">
                                <div class="card-header text-center bg-info">Datos del Paciente</div>
                                <div class="card-body" id="divDatosPacienteLppPediatrico">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Botón para guardar -->
                <div class="modal-footer">
                    <div class="row w-100 d-flex flex-row justify-content-between flex-wrap">
                        <!-- Nombre del usuario logueado a la izquierda -->
                        <div class="col-12 col-sm-12 col-md-6 text-left mb-2">
                            <h6><i>Sesión iniciada como: <span class="nombreUsuarioModal"></span></i></h6>
                        </div>
                        <!-- Botones a la derecha -->
                        <div class="col-12 col-sm-12 col-md-6 text-right d-flex justify-content-end">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                                <button type="button" class="btn btn-primary btn-md w-100 btnGuardarLpp" onclick="guardarDatosLpp()">Guardar</button>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
                                <a id="" class="btn btn-warning btn-md w-100" data-dismiss="modal">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal LPP PEDIATRICO -->

    <wuc:ModalAltaUrgencia ID="ModalAltaUrgencia" runat="server" />

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Urgencia.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/FuncionesURG.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/AtencionClinica.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Diagnostico.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/SolicitudPabellon.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Procedimientos.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Medicamentos.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Violencia.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/AlcoholemiaToxicologico.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/GinecoObstetrico.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Evoluciones.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Interconsultor.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Anamnesis.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Examenes.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Mordeduras.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Eno.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Alta.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/CerrarAtencion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Antecedentes.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/SolicitudHospitalizacion.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/AtencionClinica/Lpp.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/SignosVitales.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
