﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services;

namespace RegistroClinico.Vista
{
    public partial class PantallaEsperaUrgencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!EsIpAutorizada(Request.UserHostName))
                {
                    Response.Redirect("../Default.aspx");
                }
            }
        }

        private bool EsIpAutorizada(string ipCliente)
        {
            if (ipCliente == "::1")
                return true;

            List<string> ipsAutorizadas = ObtenerIpsAutorizadas();
            return ipsAutorizadas.Contains(ipCliente);
        }

        [WebMethod]
        public static List<string> ObtenerIpsAutorizadas()
        {
            string path = "C:\\ArchivoConf";
            path = Path.Combine(path, "PANTALLAURGENCIA.json");

            if (!File.Exists(path))
                path = "\\\\10.6.180.235\\ArchivoConf\\PANTALLAURGENCIA.json";

            Dictionary<string, object> json = Funciones.ObtenerJsonFile(path);

            return JsonConvert.DeserializeObject<List<string>>(json["rcePantallaEspera"].ToString());
        }
    }
}