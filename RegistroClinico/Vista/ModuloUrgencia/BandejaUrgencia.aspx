﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BandejaUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.BandejaUrgencia" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalMovimientoPaciente.ascx" TagName="mdMMP" TagPrefix="MMP" %>
<%@ Register Src="~/Vista/UserControls/ModalSignosVitales.ascx" TagName="ModalSignosVitales" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalHospitalizaciones.ascx" TagName="VerHospitalizaciones" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalCerrarAtencionUrgencia.ascx" TagName="ModalCerrarAtencion" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalImpresionesUrgencia.ascx" TagName="ModalImpresionesUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAltaUrgencia.ascx" TagName="ModalAltaUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalComentarioUrgencia.ascx" TagName="ModalComentarioUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalReubicarUrgencia.ascx" TagName="ModalReubicarUrgencia" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/bootstrap-select.css" rel="stylesheet" />
    <style>
        .small-box {
            margin-bottom: 0px;
        }

        .info-box {
            min-height: unset;
            padding: unset;
            margin-bottom: unset;
        }

        .badge-orange {
            color: #ffffff !important;
            background-color: orange !important;
        }

        .dropdown.bootstrap-select .dropdown-toggle {
            border: 1px solid lightgray;
        }

        .modal-contentenido {
            max-width: 100%;
            overflow-y: auto;
        }
    </style>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-select.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <!--Modal movimiento por paciente-->
    <MMP:mdMMP ID="MMP1" runat="server" />
    <wuc:ModalCerrarAtencion ID="ModalCerrarAtencion" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalSignosVitales ID="ModalSignosVitales" runat="server" />
    <wuc:VerHospitalizaciones ID="mdlVerHospitalizaciones" runat="server" />
    <wuc:ModalImpresionesUrgencia ID="ModalImpresionesUrgencia" runat="server" />
    <wuc:ModalComentarioUrgencia ID="ModelComentarioUrgencia" runat="server" />

    <wuc:ModalReubicarUrgencia runat="server" />

    <section class="content">
        <!-- Componente de titulo-->
        <titulo-pagina data-title="Urgencia"></titulo-pagina>
    </section>
    <section class="content">

        <div class="container-fluid">
            <div class="row mb-2">
                <div id="divTipoAtencionSeleccion" class="col-sm-4 text-right">
                    <div class="form-inline d-flex flex-row-reverse">
                        <div class="form-group">
                            <label for="sltTipoAtencionSeleccion">Tipo de atención:</label>
                            <select id="sltTipoAtencionSeleccion" class="form-control ml-3">
                                <option value="1">Atención General</option>
                                <option value="2">Atención Gineco-Obstétrico</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div id="divBandejaUrgencia" class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab-just">
            <button id="btn-FiltroBandejaUrgencia" data-toggle="collapse" class="btn btn-outline-primary container-fluid" data-target="#divFiltroUrg" onclick="return false;">
                FILTRAR BANDEJA
            </button>
            <div id="divFiltroUrg" class="collapse p-3">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-2">
                        <label for="txtIdtencionesUrgencia">ID</label>
                        <input id="txtIdtencionesUrgencia" type="text" name="id" class="form-control datos-persona" maxlength="11"
                            placeholder="ID" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-2">
                        <label>Identificación</label>
                        <select id="sltIdentificacionPaciente" name="Identificacion" class="form-control">
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-8 col-lg-4">
                        <label id="lblTipoIdentificacion" for="txtHosFiltroRut"></label>
                        <label id="lblIdentificacionPaciente" for="sltIdentificacionPaciente">N° Documento</label>
                        <div class="input-group">
                            <input id="txtnumeroDocPaciente" name="Numero-Documento" type="text" class="form-control numero-documento" style="width: 100px;"
                                data-required="false" />
                            <div id="separadorRut" class="input-group-prepend digito">
                                <div id="guionDigitoUrg" class="input-group-text">
                                    <strong>-</strong>
                                </div>
                            </div>
                            <input id="txtDigPaciente" type="text" class="form-control digito text-center" maxlength="1" style="width: 15px;"
                                placeholder="DV" disabled />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <label for="txtnombrePaciente">Nombre</label>
                        <input id="txtNombrePaciente" type="text" name="Nombre" class="form-control datos-persona" maxlength="50"
                            placeholder="Nombre del Paciente" />
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <label for="txtApePatPaciente">Primer Apellido</label>
                        <input id="txtApePatPaciente" type="text" name="Apellido-Paterno" class="form-control datos-persona" maxlength="50"
                            placeholder="Primer Apellido del Paciente" />
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <label for="txtApeMatPaciente">Segundo Apellido </label>
                        <input id="txtApeMatPaciente" type="text" name="Apellido-Materno" class="form-control datos-persona" maxlength="50"
                            placeholder="Segundo Apellido del Paciente" />
                    </div>

                </div>

                <div class="row mt-2">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label>Tipo de urgencia</label>
                        <select id="sltTipoUrg" class="selectpicker" name="Tipo-Urgencia" data-width="100%" data-actions-box="true" multiple title="Seleccione">
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label>Categorización</label>
                        <select id="sltCategorizacionFiltro" class="selectpicker" name="Prioridad" data-width="100%" multiple title="Seleccione">
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label>Estado</label>
                        <select id="sltEstado" name="Estado" class="form-control">
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label>Clasificación Atención</label>
                        <select id="sltClasificacionFiltro" name="Estado" class="form-control">
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label>Prioridad</label>
                        <select id="sltPrioridad" class="selectpicker" name="Tipo-Urgencia" data-width="100%" data-actions-box="true" multiple title="Seleccione">
                        </select>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-8" id="avisoFiltros"></div>
                    <div class="col-md-4 text-right">
                        <a id="btnUrgFiltro" class="btn btn-info"><i class="fa fa-search"></i>Buscar</a>
                        <a id="btnUrgLimpiarFiltro" class="btn btn-warning"><i class="fa fa-eraser"></i>Limpiar Filtros</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="post row">
                            <div class="col-sm-6 text-right">
                                <div class="row">
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='1' type="button" class="btn btn-danger btn-block">C1</button>
                                    </div>
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='2' type="button" class="btn btn-orange btn-block">C2</button>
                                    </div>
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='3' type="button" class="btn btn-warning  btn-block">C3</button>
                                    </div>
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='4' type="button" class="btn btn-success  btn-block">C4</button>
                                    </div>
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='5' type="button" class="btn btn-info  btn-block">C5</button>
                                    </div>
                                    <div class="col-md-2 text-center p-1">
                                        <button data-id-categorizacion='6' type="button" class="btn btn-secondary  btn-block">SC</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right pt-4">
                                <a id="btnReversarEstadoDAU" class="btn btn-info">
                                    <i class="fas fa-retweet"></i>Reversar
                                </a>
                                <a id="lblNuevoIngreso" style="display: none;" class="btn btn-info" href="NuevoIngreso.aspx">
                                    <i class="fas fa-plus"></i>Nuevo ingreso
                                </a>
                                <a id="btnRefrescar" class="btn btn-primary" onclick="CargarTablaUrgencia()">
                                    <i class="fas fa-refresh"></i>Refrescar
                                </a>
                            </div>
                        </div>
                        <div class="post">
                            <table id="tblUrgencia" class="table table-striped table-bordered table-hover dataTable w-100"></table>
                        </div>
                        <div class="card" style="padding: 10px; background: rgb(240, 240, 240);">
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-danger badge-count" style="color: white !important;">&nbsp;C1&nbsp;</span></h5>
                                    Categoría 1.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-info badge-count" style="color: white !important;">&nbsp;P&nbsp;</span></h5>
                                    Pedíatrica.
                                </div>
                                <div class="col-md-3">
                                    <span class="fa fa-user" aria-hidden="true"></span>&nbsp;Paciente Acompañado
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-orange badge-count" style="color: white !important;">&nbsp;C2&nbsp;</span></h5>
                                    Categoría 2.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-warning badge-count" style="color: white !important;">&nbsp;G&nbsp;</span></h5>
                                    Gineco-Obstétrica.
                                </div>
                                <div class="col-md-3">
                                    <i class="fa fa-medkit fa-lg text-red " aria-hidden="true"></i>
                                    Clasificación Atención
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-warning badge-count" style="color: white !important;">&nbsp;C3&nbsp;</span></h5>
                                    Categoría 3.
                                </div>
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-success badge-count" style="color: white !important;">&nbsp;A&nbsp;</span></h5>
                                    Adulto.
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-success badge-count" style="color: white !important;">&nbsp;C4&nbsp;</span></h5>
                                    Categoría 4.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-info badge-count" style="color: white !important;">&nbsp;C5&nbsp;</span></h5>
                                    Categoría 5.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 style="display: inline-block;"><span class="badge badge-pill bg-default badge-count" style="color: black !important;">&nbsp;SC&nbsp;</span></h5>
                                    Sin Categoría.
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="mdlAnularAtencion" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ID Atención:
                        <label id="lblAtencionAnular"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        El registro se anulará y será quitado de la bandeja
                    </div>
                    <div>
                        <label>Motivo:</label>
                        <textarea id="txtMotivoAnulacion" class="form-control" data-required="true" maxlength="200" rows="5" style="resize: none;">                                            </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="btnAnular">ANULAR</button>
                    <button class="btn btn-primary" data-dismiss="modal" aria-label="Close">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlReversarDAU" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reversar DAU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row mb-2">
                        <div class="hidden-sm col-lg-3">
                            <br />
                        </div>
                        <div class="col-sm-12 col-lg-6 text-center">
                            <label>N° de DAU</label>
                            <div class="input-group">
                                <input id="txtNumeroDau" type="number" class="form-control" placeholder="Especifique N° DAU" aria-label="N° DAU" aria-describedby="basic-addon2" />
                                <div class="input-group-append">
                                    <button id="btnBuscarReversarDAU" class="btn btn-info" type="button">
                                        <i class="fas fa-search"></i>Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-sm col-lg-3">
                            <br />
                        </div>
                    </div>

                    <div id="divAlertaReversarDAU" class="p-3">
                        <div id="divAlertaReversarPorDefecto" class="alert alert-info text-center">
                            <i class="fas fa-search"></i>Especifique un N° DAU a buscar. 
                        </div>
                        <div id="divAlertaReversarExitoso" class="alert alert-success text-center">
                            <i class="fas fa-check"></i><span></span>
                            <br />
                            <button id="btnAlertaReversarExitoso" class="btn btn-info mt-2">
                                <i class="fas fa-retweet"></i>Reversar
                            </button>
                        </div>
                        <div id="divAlertaReversarError" class="alert alert-warning text-center">
                            <i class="fas fa-exclamation"></i><span></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="detalleAtencion">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ID atención:
                        <label id="lblAtencion"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Motivo de consulta</label>
                            <textarea class="form-control" disabled id="txtMotivoConsulta" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" onclick="$('#detalleAtencion').modal('hide'); return false;">CERRAR</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pacienteCategorizado">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Categorización</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divCategorizacion" class="post">
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="alert alert-info">
                                    <h1 id="tCat" class="text-center"></h1>
                                    <h5 id="tEstimado" class="text-center"></h5>
                                    <div class="text-center">
                                        <button class="btn btn-warning" id="btnCancelarCategorizacion">
                                            <i class="fas fa-retweet"></i>Cambiar Categorización
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <label>Motivo Consulta</label>
                                <textarea id="txtMotivoConsultaCat" class="form-control" placeholder="Escriba Motivo de consulta" rows="3"
                                    maxlength="200" data-required="true"></textarea>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-md-6">
                                <label>Clasificación Atención</label>
                                <select id="sltTipoClasificacion" class="form-control" data-required="true">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Prioridad Paciente</label>
                                <select id="sltTipoPrioridadPaciente" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div id="divAntecedentesAdicionalesCat"></div>
                        <div id="divUrgenciaAntecedentes"></div>
                    </div>
                    <div class="post" id="divCatEnfermero" style="display: none;">
                        <div class="form-row">
                            <label class="col-form-label" for="inputWarning"><i class="far fa-bell"></i>&nbsp;Considere las siguientes categorizaciones</label>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4" id="divRecategorizarC2" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C2</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC2" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4" id="divRecategorizarC4" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C4</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC4" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="small-box bg-success">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C3</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC3" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4" id="divRecategorizarC5" style="display: none;">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                    </div>
                                    <div style="margin-left: 30px" class="icon">
                                        <h3>C5</h3>
                                    </div>
                                    <a href="#/" id="btnRecategorizarC5" class="small-box-footer">Asignar <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-info" id="btnAceptarCategorizacion">Aceptar categorización</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-print fade" id="mdlImprimir" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameDAU" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="categorizarPaciente">
        <div class="modal-dialog modal-lg">
            <div class="card modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title" id="staticBackdropLabel1"><i class="fas fa-plus-circle" aria-hidden="true"></i>Categorizar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row mb-1">
                        <div class="col-lg-12">
                            <h6 class="modal-title">Nombre de paciente:
                                <strong id="lblPacienteCat"></strong>
                            </h6>
                        </div>
                    </div>
                    <hr />

                    <ul class="nav nav-tabs" id="tabAlta" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button id="datos-antecedentes-tab"
                                class="nav-link active"
                                data-toggle="tab"
                                data-target="#divAntecedentes"
                                type="button"
                                role="tab"
                                aria-controls="divAntecedentes"
                                aria-selected="true">
                                Antecedentes</button>

                        </li>
                        <li class="nav-item" role="presentation">
                            <button id="datos-categorizacion-tab"
                                class="nav-link"
                                data-toggle="tab"
                                data-target="#divCategorizacionModal"
                                type="button"
                                role="tab"
                                aria-controls="divCategorizacionModal"
                                disabled
                                aria-selected="false">
                                Categorización</button>
                        </li>
                    </ul>

                    <div class="tab-content mt-4" id="tabEvolucionUrgContent">
                        <div id="divAntecedentes" class="tab-pane fade show active" role="tabpanel">
                            <div id="divAntecedentesAdicionales">
                                <div class="row mt-3">
                                    <div class="col-lg-12">

                                        <strong>Complemento a la consulta</strong>
                                        <textarea id="txtObservacionesCategorizacion" class="form-control" placeholder="Escriba observaciones adicionales" rows="3"
                                            maxlength="300"></textarea>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <strong>Alergias</strong><br />
                                        <input id="rdoAlergiaSi" type="radio" name="AL" class="bootstrapSwitch"
                                            data-on-text="SI"
                                            data-off-text="SI" data-on-color="primary" data-size="large"
                                            data-required="false" />
                                        <input id="rdoAlergiaNo" type="radio" name="AL" class="bootstrapSwitch"
                                            data-on-text="NO"
                                            data-off-text="NO" data-on-color="primary" data-size="large"
                                            data-required="false" />
                                        <input id="rdoAlergiaDesconocido" type="radio" name="AL"
                                            class="bootstrapSwitch" data-on-text="Desconocido"
                                            data-off-text="Desconocido" data-on-color="primary"
                                            data-size="large" data-required="false" />
                                    </div>
                                </div>
                                <div id="divUrgenciaCategorizacionAlergia"></div>
                            </div>
                            <div class="text-right mt-2">
                                <a id="aSiguienteAntecedentes" class="btn btn-primary" onclick="validaSiguiente();">Siguiente</a>
                            </div>
                        </div>

                        <div id="divCategorizacionModal" class="row tab-pane fade" role="tabpanel">

                            <div id="divCategorizacionNormal">

                                <div class="col-md-12">
                                    <div class="timeline">
                                        <div class="time-label">
                                            <button class="btn btn-danger" id="btnNuevo">Comenzar de nuevo</button>
                                        </div>
                                        <div id="catC1">
                                            <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">A</i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">

                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">¿Paciente presenta una amenaza real para su vida, requiere una intervención inmediata?</span>
                                                                </div>

                                                                <div class="btn-group btn-group-toggle" id="toggle00" data-toggle="buttons">
                                                                    <label id="btnSiAmenaza" class="btn bg-success">
                                                                        <input type="radio" name="options" checked="" />
                                                                        <i class="fas fa-check-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                    </label>
                                                                    <label id="btnNoAmenaza" class="btn bg-danger">
                                                                        <input type="radio" name="options" />
                                                                        <i class="fas fa-times-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                    </label>
                                                                </div>

                                                                <!-- /.info-box-content -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOverlayCat1" class="overlay"></div>
                                        </div>

                                        <div id="catC2">
                                            <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">B</i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">¿Se trata de un paciente que no debe esperar, es una situación de alto riesgo?</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="timeline-item" id="item01" style="display: none;">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">AVDI</span>
                                                                </div>
                                                                <div class="btn-group btn-group-toggle" id="toggle01" data-toggle="buttons">
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="1" id="opcion01" checked="">
                                                                        Alerta
                                                                    </label>
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="2" id="opcion02">
                                                                        Respuesta verbal
                                                                    </label>
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="3" id="opcion03">
                                                                        Respuesta al dolor
                                                                    </label>
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="4" id="opcion04">
                                                                        Inconsciente
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="timeline-item" id="item02" style="display: none;">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">Dolor, EVA</span>
                                                                </div>
                                                                <div class="btn-group btn-group-toggle" id="toggle02" data-toggle="buttons">
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="1" id="opcion05" checked="">
                                                                        Dolor menor a 7
                                                                    </label>
                                                                    <label class="btn bg-info clickCat2">
                                                                        <input type="radio" name="options" cat="2" id="opcion06">
                                                                        Dolor mayor/igual a 7
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="timeline-item" id="item03" style="display: none;">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">Distresado / ¿Situación de alto riesgo?</span>
                                                                    <br />
                                                                </div>
                                                                <div class="btn-group btn-group-toggle" id="toggle03" data-toggle="buttons">
                                                                    <label class="btn bg-success clickCat2">
                                                                        <input type="radio" name="options" cat="1" id="opcion07" checked="">
                                                                        <i class="fas fa-check-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                    </label>
                                                                    <label class="btn bg-danger clickCat2">
                                                                        <input type="radio" name="options" cat="2" id="opcion08">
                                                                        <i class="fas fa-times-circle" style="font-size: 30px; padding: 7px;"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOverlayCat2" class="overlay"></div>

                                        </div>

                                        <div id="catC3">
                                            <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">C</i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <div class="info-box" style="box-shadow: unset;">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">¿Cuántos recursos cree que utilizará?</span>
                                                                </div>
                                                                <div class="btn-group btn-group-toggle" id="toggle04" data-toggle="buttons">
                                                                    <label class="btn bg-info" id="btnNinguno">
                                                                        <input type="radio" name="options" checked="">
                                                                        Ninguno
                                                                    </label>
                                                                    <label class="btn bg-info" id="btnUno">
                                                                        <input type="radio" name="options">
                                                                        Uno
                                                                    </label>
                                                                    <label class="btn bg-info" id="btnMasdeuno">
                                                                        <input type="radio" name="options">
                                                                        Mas de uno
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOverlayCat3" class="overlay"></div>
                                        </div>
                                        <div id="alertSignosVitales" style="display: none;">
                                            <div class="timeline-item">
                                                <div class="alert alert-info">
                                                    La categorización no tiene datos correspondientes a 'signos vitales'
                                                </div>
                                            </div>
                                        </div>
                                        <div id="catC4">
                                            <i class="fa bg-info" style="padding: 1px 10px 1px 8px; border-radius: 20px; font-size: 20px; font-family: monospace;">D</i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    <div class="info-box" style="box-shadow: unset;">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="info-box-content">
                                                                    <span class="info-box-number">¿Signos vitales en zona de riesgo?</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row" id="divSignosVitalesCatUrg">
                                                            <div class="form-group col-md-3">
                                                                <label>F. respiratoria</label>
                                                                <input id="txtFR" type="text" class="form-control" />
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label>F. cardíaca</label>
                                                                <input id="txtFC" type="text" class="form-control" />
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label>Saturación</label>
                                                                <input id="txtSat" type="text" class="form-control" />
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label>Temperatura</label>
                                                                <input id="txtTemp" type="text" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-success float-right" id="btnAceptarDatos">Aceptar datos</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divOverlayCat6" class="overlay"></div>
                                            </div>
                                            <div class="timeline-item">
                                                <div class="timeline-body" id="divEpidemio" style="display: none;">
                                                    <div class="info-box" style="box-shadow: unset;">
                                                        <div class="form-row" style="display: contents;">
                                                            <div class="form-group col-md-6">
                                                                <label>Antecedentes epidemiológicos</label>
                                                                <br />
                                                                <input type="checkbox" id="switchEpidemiologicos" data-label-width="1" data-on-text="SI" data-off-text="NO" data-on-color="info" data-off-color="warning" />
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <div class="custom-control custom-radio">
                                                                    <input class="custom-control-input" type="radio" id="checkRequiereRec" name="radioRecurso" checked>
                                                                    <label class="custom-control-label" for="checkRequiereRec">Requiere 1 recurso</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input class="custom-control-input" type="radio" id="checkNoRequiereRec" name="radioRecurso">
                                                                    <label class="custom-control-label" for="checkNoRequiereRec">Requiere 0 recursos</label>
                                                                </div>
                                                                <div id="divOverlayEpidemio" class="overlay"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-success float-right" id="btnAceptarAnte">Aceptar antecedentes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divOverlayCat5" class="overlay"></div>
                                            </div>
                                            <div id="divOverlayCat4" class="overlay"></div>
                                        </div>
                                        <div>
                                            <i class="far fa-dot-circle bg-gray"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divCategorizacionObstetrico">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="sltCategorizacionObstetrico">Tipo Categorización Gineco-Obstetrico</label>
                                        <select id="sltCategorizacionObstetrico" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="text-center mt-4">
                                    <button id="btnCategorizacionObstetrico" class="btn btn-success">
                                        <i class="fas fa-check"></i>Confirmar Categorización
                                    </button>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <a id="aAtrasCategorizacion" class="btn btn-primary" onclick="$('#datos-antecedentes-tab').trigger('click')">Atrás</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal llamar paciente -->
    <div class="modal fade" id="mdlLlamarPaciente" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h5 class="modal-title" id="tituloModalLlamarPaciente">Llamar paciente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 id="textoModalLlamarPaciente"></h5>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <div class="row" id="divBotoneraLlamarPaciente">
                                <div class="col-12 p-1">
                                    <h5 class="text-center">Paciente Admitido</h5>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnPrimerLlamado" type="button">
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Primer llamado
                                    </button>
                                    <div id="spanPrimerLlamado" class="d-none"></div>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnSegundoLlamado" type="button" disabled>
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Segundo llamado
                                    </button>
                                    <div id="spanSegundoLlamado" class="d-none"></div>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnTercerLlamado" type="button" disabled>
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Tercer llamado
                                    </button>
                                    <div id="spanTercerLlamado" class="d-none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row" id="divBotoneraLlamarPacienteCategorizado">
                                <div class="col-12 p-1">
                                    <h5 class="text-center">Paciente Categorizado</h5>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnPrimerLlamadoCategorizado" type="button">
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Primer llamado
                                    </button>
                                    <div id="spanPrimerLlamadoCategorizado" class="d-none"></div>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnSegundoLlamadoCategorizado" type="button" disabled>
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Segundo llamado
                                    </button>
                                    <div id="spanSegundoLlamadoCategorizado" class="d-none"></div>
                                </div>
                                <div class="col-12 p-1">
                                    <button class="btn btn-info w-100 btn-lg" id="btnTercerLlamadoCategorizado" type="button" disabled>
                                        <i class="fa fa-bullhorn"></i>
                                        <br />
                                        Tercer llamado
                                    </button>
                                    <div id="spanTercerLlamadoCategorizado" class="d-none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Nuevo botón para egresar paciente -->
                        <div class="col-12 p-1" id="divBotonEgresarPaciente">
                            <button class="btn btn-warning w-100 btn-lg" id="btnEgresarPaciente" type="button" onclick="linkEgresoIngresoUrgenciaDesdeBandeja(idAtencionUrgencia)">
                                <i class="fas fa-angle-double-right"></i>
                                <br />
                                Egresar paciente
                            </button>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN Modal Llamar paciente -->

    <!-- Control de usuario moviemientos de sistema-->
    <wuc:ModalGenericoMovimientosSistema ID="ModalGenericoMovimientosSistema" runat="server" />
    <wuc:ModalAltaUrgencia ID="ModalAltaUrgencia" runat="server" />

    <div id="mdlRecaudacion" class="modal fade ml-2 mr-2" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content ml-4 mr-4">
                <div class="modal-header">
                    <h5 class="modal-title">ID Atención Urgencia #  
                        <label id="lblAtencionLiquidar"></label>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="mb-4">
                        <strong>
                            <i class="fas fa-dollar-sign"></i>Liquidar Cuenta
                        </strong>
                    </h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-12">
                                    <h6><u>Datos de Atención</u></h6>
                                    Tipo de Atención:<br />
                                    <label id="lblTipoAtencionLiq"></label>
                                    <br />
                                    Motivo de Consulta:<br />
                                    <label id="lblMotivoConsultaLiq"></label>
                                    <h6><u>Datos de Paciente</u></h6>
                                    Nombre Paciente:<br />
                                    <label id="lblPacienteLiq"></label>
                                    <br />
                                    Prevision/Tramo:<br />
                                    <label id="lblPrevisionLiq"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Observaciones: (Opcional)</h6>
                                    <textarea id="txtObservacionesLiq" class="form-control" rows="5" placeholder="Observaciones para esta liquidación"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='text-right mt-2'>
                        <a id='aRecaudacionProceder' class='btn btn-info'>Proceder y Liquidar <i class="fas fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%-- referenciando a la vista mapa de cama --%>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/VistaMapaCamasUrgencia.js") + "?v=" + GetVersion() %>"></script>
    <%--<script src="<%= ResolveClientUrl("~/Script/UserControls/ModalReubicarUrgencia.js") + "?v=" + GetVersion() %>"></script>--%>

    <script src="<%= ResolveClientUrl("~/Script/ModuloPersonas/ComboPersonas.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/FuncionesURG.js") + "?v=1234" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Bandeja/BandejaUrgencia.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Bandeja/BandejaUrgencia.Link.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Urgencia.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/SignosVitales.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Bandeja/LlamarPaciente.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Bandeja/ReversarDAU.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
