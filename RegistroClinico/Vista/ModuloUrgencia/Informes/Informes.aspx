﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="Informes.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.Informes.Informes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    
    <div class="card">
        <div class="card-header">
            <titulo-pagina data-title="Informes Urgencia"></titulo-pagina>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <label>Informes</label>
                    <select id="sltListaInformes"  class="form-control" data-required="true">
                        <option value="General">Reporte General</option>
                        <option value="General/RangoEtareo">Reporte por rango Etareo</option>
                        <option value="General/Resumen">Reporte Resumen General</option>
                        <option value="General/N4">Reporte N4</option>
                        <option value="GinecoObstetrico">Reporte Gineco-Obstétrico Mensual</option>
                        <option value="General/Procedimientos">Reporte General Indicaciones Clínicas</option>
                        <option value="Comentarios">Reporte de Comentarios</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="fechaInicioInforme">Mes</label>
                            <select id="sltMes" type="date" class="form-control">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="fechaInicioInforme">Año</label>
                            <select id="sltAnio" type="date" class="form-control">
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="fechaInicioInforme">Fecha Inicio</label>
                    <input id="fechaInicioInforme" type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label for="fechaFinInforme">Fecha Final</label>
                    <input id="fechaFinInforme" type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <button id="btnBuscarReporte" type="button" class="btn btn-info" style="margin-top: 30px;"><i class="fa fa-file-excel"></i> Descargar Excel</button>
                </div>
            </div>
        </div>
        <div id="divResultadoReporte"></div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Informes/Informes.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
