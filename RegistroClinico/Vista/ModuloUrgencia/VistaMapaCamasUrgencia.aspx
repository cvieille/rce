﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VistaMapaCamasUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.VistaMapaCamasUrgencia" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalMovimientoPaciente.ascx" TagName="mdMMP" TagPrefix="MMP" %>
<%@ Register Src="~/Vista/UserControls/ModalImpresionesUrgencia.ascx" TagName="ModalImpresionesUrgencia" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalReubicarUrgencia.ascx" TagName="ModalReubicarUrgencia" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/bootstrap-clockpicker.css" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/jquery-clockpicker.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ClockPicker/bootstrap-clockpicker.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <!--include modal movimiento paciente-->
    <MMP:mdMMP ID="MMP1" runat="server" />
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <wuc:ModalImpresionesUrgencia ID="ModalImpresionesUrgencia" runat="server" />
    <!-- fin include modal movimiento paciente = MMP-->
    <!--Iconografía de información de camas-->
    <div class="card">
        <div class="card-header">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Mapa de Camas de urgencia"></titulo-pagina>
        </div>
        <div class="card-body">
            <div class="row d-flex flex-row flex-wrap justify-content-center align-items-center">
                <div class="col-sm-12 col-md-2 d-flex justify-content-center mt-4">
                    <%-- select --%>
                    <select class="form-control" id="selectTipoAtencion">
                    </select>
                </div>
                <div class="d-flex justify-content-start mt-4">
                    <%-- refrescar --%>
                    <a id="btnRefrescar" class="btn btn-primary"><i class="far fa-file-alt mr-1"></i>Refrescar</a>
                </div>
                <div class="d-flex justify-content-center mt-4 ml-1">
                    <%-- pendientes --%>
                    <a id="btnIndicacionesPendientes" class="btn btn-warning"><i class="far fa-file-alt mr-1"></i>Pendientes</a>
                </div>
                <div class="d-flex justify-content-center mt-4 ml-1">
                    <%-- Mis atenciones (Medico) --%>
                    <a id="btnMisATencionesMedicas" class="btn btn-warning"><i class="far fa-user-med mr-1"></i>Mis atenciones</a>
                </div>
            </div>
            <div class="col-12 mt-4">
                <div class="alert alert-warning" role="alert" style="display: none;" id="divTrasladandoPaciente">
                    <h5 class="text-center">
                        <i class="fa fa-info-circle fa-lg"></i>
                        Seleccione un box disponible dónde el paciente será atendido
                            <button class="btn btn-danger btn-sm" type="button" onclick="cancelarMovimientoPaciente()"><i class="fa fa-ban"></i>Cancelar accion</button>
                    </h5>
                </div>
            </div>
            <!--mapa de camas-->
            <div id="divGeneralMapaCama">
                <div id="infoInicial" class="mt-5 mb-5 alert alert-info">
                    <h3 class='text-center w-100'>
                        <b><i class='fa fa-info-circle'></i>No se ha realizado búsqueda.</b>
                    </h3>
                </div>
            </div>

            <div class="fixed-bottom text-right">
                <a onclick="$('html, body').animate({ scrollTop: $('body').offset().top}, 500);">
                    <i class="fa fa-arrow-up fa-2x" style="cursor: pointer;"></i>
                </a>
            </div>

            <!--Mapa de camas-->
            <div id="ContCamasUrg" class="row">
            </div>
            <!--Fin mapa de camas-->

            <!--Iconos-->
            <div class="row  mt-3 mb-3">
                <div class="col-md-6 ">
                    <div class="col text-center">
                        <h5>
                            <b class="text-center   ">Categorización.</b><br>
                        </h5>
                    </div>
                    <div class="row" style="display: flex;">
                        <div class="col text-center">
                            <p>
                                <strong>Categoria 1</strong>
                            </p>
                            <span class="badge badge-pill bg-danger badge-count" style="color: white !important; font-size: 1.1em;">&nbsp;C1&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>Categoria 2</strong>
                            </p>
                            <span class="badge badge-pill bg-orange badge-count" style="color: white !important; font-size: 1.1em;">&nbsp;C2&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>Categoria 3</strong>
                            </p>
                            <span class="badge badge-pill bg-warning badge-count" style="color: white !important; font-size: 1.1em;">&nbsp;C3&nbsp;</span>
                        </div>
                        <div class="col  text-center">
                            <p>
                                <strong>Categoria 4</strong>
                            </p>
                            <span class="badge badge-pill bg-success badge-count" style="color: white !important; font-size: 1.1em;">&nbsp;C4&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>Categoria 5</strong>
                            </p>
                            <span class="badge badge-pill bg-info badge-count" style="color: white !important; font-size: 1.1em;">&nbsp;C5&nbsp;</span>
                        </div>
                        <div class="col text-center">
                            <p>
                                <strong>Sin categoría</strong>
                            </p>
                            <span class="badge badge-pill bg-default badge-count" style="font-size: 1.1em;">&nbsp;SC&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin Iconografía -->

        </div>
    </div>

    <!--modal informacion urgencia -->
    <div class="modal fade" id="mdlInformacionAtencionUrgencia" tabindex="-1" aria-labelledby="modalAtencionUrgencia" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="modalTituloAtencionUrgencia"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalContenidoAtencionUrgencia" class="row"></div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="tabsInformacionUrgencia" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="procedimientos-tab" data-toggle="tab" href="#ProcedimientosDiv" role="tab" aria-controls="ProcedimientosDiv" aria-selected="true">
                                        <h5>Procedimientos <span id="spnProcedimientos" class="badge badge-dark">0</span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="signos-tab" data-toggle="tab" href="#SignosDiv" role="tab" aria-controls="SignosDiv" aria-selected="false">
                                        <h5>Signos vitales <span id="spnSignosVitales" class="badge badge-dark">0</span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="examenesLab-tab" data-toggle="tab" href="#ExamenesLabDiv" role="tab" aria-controls="ExamenesDiv" aria-selected="false">
                                        <h5>Examenes laboratorio <span id="spnExamenesLab" class="badge badge-dark"></span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="examenesImagen-tab" data-toggle="tab" href="#ExamenesImagenDiv" role="tab" aria-controls="ExamenesDiv" aria-selected="false">
                                        <h5>Examenes imagenologia <span id="spnExamenesImagen" class="badge badge-dark">0</span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="medicamentos-tab" data-toggle="tab" href="#MedicamentosDiv" role="tab" aria-controls="MedicamentosDiv" aria-selected="false">
                                        <h5>Medicamentos <span id="spnMedicamentos" class="badge badge-dark"></span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="interconsultor-tab" data-toggle="tab" href="#InterconsultorDiv" role="tab" aria-controls="MedicamentosDiv" aria-selected="false">
                                        <h5>Interconsultor <span id="spnInterconsultor" class="badge badge-dark"></span></h5>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="comentarios-tab" data-toggle="tab" href="#comentariosDiv" role="tab" aria-controls="comentariosDiv" aria-selected="false">
                                        <h5>Comentarios <span id="spnComentarios" class="badge badge-dark"></span></h5>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="tabsInformacionUrgenciaContent">
                                <div class="tab-pane fade show active" id="ProcedimientosDiv" role="tabpanel" aria-labelledby="procedimientos-tab">
                                    <table id="tblProcedimientos" class="table table-bordered w-100"></table>
                                </div>
                                <div class="tab-pane fade" id="SignosDiv" role="tabpanel" aria-labelledby="signos-tab">
                                    <div class="row border border-dark text-center rounded p-1 mb-1" id="ultimaTomaSignosUrg"></div>
                                    <div class="row" id="divSigosVitales">
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="ExamenesLabDiv" role="tabpanel" aria-labelledby="examenes-tab">
                                    <table id="tblExamenLaboratorio" class="table table-striped table-bordered w-100 text-wrap"></table>
                                </div>
                                <div class="tab-pane fade" id="ExamenesImagenDiv" role="tabpanel" aria-labelledby="examenes-tab">
                                    <table id="tblExamenImagen" class="table table-striped table-bordered w-100 text-wrap"></table>
                                </div>
                                <div class="tab-pane fade" id="MedicamentosDiv" role="tabpanel" aria-labelledby="medicamentos-tab">
                                    <table id="tblMedicamentos" class="table table-bordered w-100 text-wrap"></table>
                                </div>
                                <div class="tab-pane fade" id="InterconsultorDiv" role="tabpanel" aria-labelledby="medicamentos-tab">
                                    <table id="tblInterconsultor" class="table table-bordered w-100 text-wrap"></table>
                                </div>
                                <div class="tab-pane fade" id="comentariosDiv" role="tabpanel" aria-labelledby="medicamentos-tab">
                                    <table id="tblComentarios" class="table table-bordered w-100 text-wrap"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN modal informacion urgencia -->

    <!-- Modal ingreso a box -->
    <div class="modal fade" id="modalPacienteUrgencia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="TituloModalPacienteUrgencia"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center align-items-center">
                        <!-- Tabs para navegar en diferentes estados -->
                        <ul class="nav nav-tabs" id="tabPacientes" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tabSinAsignar" data-toggle="tab" href="#sinAsignar" role="tab" aria-controls="sinAsignar" aria-selected="true">Sin Asignar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tabAsignados" data-toggle="tab" href="#asignados" role="tab" aria-controls="asignados" aria-selected="false">Asignados</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tabTodos" data-toggle="tab" href="#todos" role="tab" aria-controls="todos" aria-selected="false">Todos</a>
                            </li>
                        </ul>

                        <!-- Select para filtrar por tipo de atención -->
                        <div class="form-group mb-0 ml-3">
                            <select class="form-control" id="tipoAtencionSelect">
                            </select>
                        </div>
                    </div>
                    <hr />
                    <!-- Tabs separados por tipo -->
                    <div class="tab-content">
                        <div class="tab-pane fade show active table-responsive" id="sinAsignar" role="tabpanel" aria-labelledby="tabSinAsignar">
                            <table id="tblSinAsignar" class="table table-striped table-bordered w-100 text-wrap"></table>
                        </div>
                        <div class="tab-pane fade table-responsive" id="asignados" role="tabpanel" aria-labelledby="tabAsignados">
                            <table id="tblAsignados" class="table table-striped table-bordered w-100 text-wrap"></table>
                        </div>
                        <div class="tab-pane fade table-responsive" id="todos" role="tabpanel" aria-labelledby="tabTodos">
                            <table id="tblTodos" class="table table-striped table-bordered w-100 text-wrap"></table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN Modal ingreso a box -->


    <%-- MODAL INDICACIONES PENDIENTES --%>
    <div class="modal fade" id="mdlIndicacionesPendientes" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlIndicacionesPendientes" aria-hidden="true">
        <div class="modal-dialog modal-100 mt-0">
            <div class="modal-content">
                <div class="modal-header bg-info text-white">
                    <h5 class="modal-title" id="staticBackdropLabel"><i class="far fa-file-alt mr-1" aria-hidden="true"></i>Indicaciones Pendientes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="home" aria-selected="true">Procedimientos pendientes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="profile" aria-selected="false">Medicamentos pendientes</a>
                        </li>
                         <li class="nav-item">
                             <a class="nav-link" id="examenes-laboratorio-tab" data-toggle="tab" href="#tabExamenLaboratorio" role="tab" aria-controls="examenes-laboratorio" aria-selected="false">Examen laboratorio pendientes</a>
                         </li>
                        <li class="nav-item">
                             <a class="nav-link" id="examenes-imagen-tab" data-toggle="tab" href="#tabExamenImagen" role="tab" aria-controls="examenes-imagen" aria-selected="false">Examen imagen pendientes</a>
                         </li>
                        <li class="nav-item">
                             <a class="nav-link" id="interconsultorPendientes-tab" data-toggle="tab" href="#tabInterconsultorPendientes" role="tab" aria-controls="interconsultor" aria-selected="false">Interconsultor pendientes</a>
                         </li>
                        <li class="nav-item">
                             <a class="nav-link" id="signosVitalesPendientes-tab" data-toggle="tab" href="#tabSignosVitalesPendientes" role="tab" aria-controls="signosVitales" aria-selected="false">Toma de signos pendientes</a>
                         </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2>Pacientes esperando tratamiento</h2>
                                </div>
                            </div>
                            <div class="row mt-3 d-flex flex-row justify-content-center">
                                <div class="col-11">
                                    <div class="table-responsive" style="overflow: scroll">
                                        <table id="tblProcedimientosPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--ESTRUCTURA PARA MEDICAMENTOS PENDIENTES --%>
                        <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2>Pacientes esperando medicamentos</h2>
                                </div>
                            </div>
                            <div class="row mt-3 d-flex flex-row justify-content-center">
                                <div class="col-11">
                                    <div class="table-responsive" style="overflow: scroll">
                                        <table id="tblMedicamentosPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tab examenes de laboratorio-->
                         <div class="tab-pane fade" id="tabExamenLaboratorio" role="tabpanel" aria-labelledby="profile-tab">
                             <div class="row">
                                <div class="col-lg-12">
                                    <h2>Solicitudes de examen de laboratorio aun no resueltas</h2>
                                </div>
                             </div>
                             <div class="row mt-3 d-flex flex-row justify-content-center">
                                <div class="col-11">
                                    <div class="table-responsive" style="overflow: scroll">
                                        <table id="tblExamenLaboratorioPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <!--tab examenes de imagen-->
                         <div class="tab-pane fade" id="tabExamenImagen" role="tabpanel" aria-labelledby="profile-tab">
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <h2>Solicitudes de examen de imagenología aun no resueltas</h2>
                                     </div>
                                 </div>
                                 <div class="row mt-3 d-flex flex-row justify-content-center">
                                    <div class="col-11">
                                        <div class="table-responsive" style="overflow: scroll">
                                            <table id="tblExamenImagenPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                        </div>
                                    </div>
                                </div>
                         </div>
                        <!--tab interconsultor pendientes-->
                         <div class="tab-pane fade" id="tabInterconsultorPendientes" role="tabpanel" aria-labelledby="profile-tab">
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <h2>Solicitudes de interconsultor aun no resueltas</h2>
                                     </div>
                                 </div>
                                 <div class="row mt-3 d-flex flex-row justify-content-center">
                                    <div class="col-11">
                                        <div class="table-responsive" style="overflow: scroll">
                                            <table id="tblInterconsultorPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                        </div>
                                    </div>
                                </div>
                         </div>

                        <!--tab interconsultor pendientes-->
                         <div class="tab-pane fade" id="tabSignosVitalesPendientes" role="tabpanel" aria-labelledby="profile-tab">
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <h2>Listado de atenciones con toma de signos vitales pendientes</h2>
                                     </div>
                                 </div>
                                 <div class="row mt-3 d-flex flex-row justify-content-center">
                                    <div class="col-11">
                                        <div class="table-responsive" style="overflow: scroll">
                                            <table id="tblSignosVitalesPendientes" class="table table-striped table-bordered table-hover" style="width: 100%;"></table>
                                        </div>
                                    </div>
                                </div>
                         </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row mt-2 text-right">
                        <div class="col-lg-12">
                            <a id="btnCancelarIndicacionesPendientes" class="btn btn-warning" data-dismiss="modal">Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- FIN MODAL INDICACIONES PENDIENTES --%>

    <!-- Modal mis atenciones medicas -->
    <div class="modal fade" id="mdlMisAtencionesMedicas" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header bg-info text-white">
            <h5 class="modal-title" id="exampleModalLabel">Revisando mis atenciones médicas <span class="nombreUsuarioModal"></span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table id="tblMisAtencionesMedicas" class="w-100 table table-striped table-bordered dataTable"></table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <wuc:ModalReubicarUrgencia runat="server" />

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/VistaMapaCamasUrgencia.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/FuncionesURG.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
