﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoIngreso.aspx.cs" Inherits="RegistroClinico.Vista.ModuloUrgencia.NuevoIngreso" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/DatosAcompañante.ascx" TagName="DatosAcompañante" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/SignosVitales.ascx" TagName="SignosVitales" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">

    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>

    <style>
        .table.dataTable td, table.dataTable th {
            padding-top: 8px;
            padding-bottom: 8px;
        }
    </style>

</asp:Content>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <div class="card card-body mb-0">
        <div class="card">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Ingreso de atención de urgencia"></titulo-pagina>

            <div class="card-body p-0">

                <ul id="ulTablist" class="nav nav-pills mt-2 mb-2">
                    <li id="liDatosPaciente" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                        <a class="nav-link active" data-toggle="pill" href="#divAntecedentesClinicos">Datos del paciente</a>
                    </li>
                    <li id="liDatosAtencionAdm" class="nav-item col-12 col-sm-4 col-md-4 col-lg-2">
                        <a class="nav-link" data-toggle="pill" href="#divDatosAtencionAdm">Datos de la atención</a>
                    </li>
                </ul>

                <div class="tab-content card">

                    <div id="divAntecedentesClinicos" class="tab-pane fade show active pt-3" role="tabpanel" aria-labelledby="home-tab-just">

                        <div id="divDPaciente" data-loading="true">
                            <wuc:Paciente ID="wucPaciente" runat="server" />
                        </div>
                        
                        <div id="divDatosAdicionalesPaciente" class="p-2 mb-4">
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label for="sltPais" id="lblPais">País de residencia</label>
                                    <select id="sltPais" class="form-control datos-pac" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="sltRegion">Región</label>
                                    <select id="sltRegion" class="form-control datos-pac" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="sltProvincia">Provincia</label>
                                    <select id="sltProvincia" class="form-control datos-pac" data-required="true">
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="sltCiudad">Comuna</label>
                                    <select id="sltCiudad" class="form-control datos-pac" data-required="true">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <wuc:DatosAcompañante ID="wucDatosAcompañante" runat="server" />

                        <div class="card" data-loading="true">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0">
                                    <strong><i class="fa fa-angle-double-right"></i>Establecimiento</strong>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Servicio de salud</label>
                                        <select id="sltServicioSalud" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Establecimiento</label>
                                        <select id="sltEstablecimiento" class="form-control" data-required="true">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a id="aSiguienteIngresoUrgencia" class="btn btn-primary" data-li="#liDatosPaciente">
                                    Siguiente <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div id="divDatosAtencionAdm" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="contact-tab-just">
                        <div class="card">
                            <div class="card-header bg-dark">
                                <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de la atención</strong> </h5>
                            </div>
                            <div class="card-body">

                                <div id="divDatosAtencion">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Tipo de atención</label>
                                            <select id="sltTipoAtencion" data-required="true" class="form-control">
                                            </select>
                                            <br />
                                            <span id="msjAdulto" style="display: none;" class="text-primary text-center">El paciente es mayor de 15 años! Se sugiere atención de tipo Adulto.</span>
                                            <span id="msjPediatrica" style="display: none;" class="text-primary text-center">El paciente es menor de 15 años! Se sugiere atención de tipo Pediatrica.</span>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Procedencia</label>
                                            <select id="sltProcedencia" data-required="false" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-3">
                                            <label>Medio de transporte</label>
                                            <select id="sltMedioTransporte" data-required="true" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Tipo de accidente</label>
                                            <select id="sltTipoAccidente" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Leyes previsionales</label>
                                            <select id="sltLeyesPrevisionales" data-required="false" class="form-control">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <label>Lugar de accidente</label>
                                            <textarea id="txtLugarAccidente" class="form-control" disabled maxlength="200" rows="5" style="resize: none;" onkeyup="checkWords(this)"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Motivo de consulta</label>
                                            <textarea id="txtMotivoConsulta" class="form-control" data-required="true" maxlength="200" rows="5" style="resize: none;" onkeyup="checkWords(this)">
                                            </textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a id="aAtrasIngresoUrgencia" class="btn btn-primary">
                                    <i class="fa fa-arrow-left"></i>Atrás
                                </a>
                                <button id="aaGuardarIngresoUrgencia" class="btn btn-success aGuardarIngresoUrgencia">
                                    <i class="fa fa-save"></i>Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/FuncionesURG.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/Urgencia.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloUrgencia/NuevoIngreso.js") + "?v=" + GetVersion() %>"></script>

</asp:Content>
