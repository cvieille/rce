﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PantallaEsperaUrgencia.aspx.cs" Inherits="RegistroClinico.Vista.PantallaEsperaUrgencia" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pantalla Espera Urgencia</title>

    <link rel='shortcut icon' href='../../favicon.ico' type='image/x-icon' />
    <link href="../../Style/Bootstrap_3_3_7/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Style/toastr.css" rel="stylesheet" />
    <link href="../../Style/font-awesome/css/all.min.css" rel="stylesheet" />

    <style>
        html, body {
            height: 100%;
            margin: 0;
        }

        .navbar {
            border-radius: 0px;
            min-height: 100px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        #reloj {
            font-weight: bold;
            justify-content: center;
            display: flex;
            align-items: center;
            margin: auto;
            font-size: 5rem;
        }

        .main-header {
            display: flex;
            color: white;
            padding-left: 80px;
            flex-grow: 1;
        }

        .logo-header {
            max-height: 70px;
            margin-left: auto;
            margin-bottom: 0;
            padding-right: 50px;
        }

        .container {
            min-height: calc(100vh - 150px);
            display: flex;
            flex-direction: row;
            width: 100vw;
            margin: 0;
            padding: 60px;
            box-sizing: border-box;
        }

        .section {
            width: 50%;
            padding: 20px;
            position: relative;
        }

            /* Línea divisoria vertical */
            .section::after {
                content: "";
                position: absolute;
                top: 0;
                right: 0;
                width: 1px;
                height: 80%;
                background-color: #000;
            }

            /* Asegurar que la linea se posicione al medio y no al final */
            .section:last-child::after {
                display: none;
            }

        .titulo-pediatrico {
            text-align: center;
            font-size: 5rem;
            font-weight: bold;
            margin: 10px 0;
            color: #343a40;
        }

        .bg-primary {
            background-color: #007bff !important;
        }

        .bg-info {
            background-color: #17a2b8 !important;
        }

        .bg-warning {
            background-color: #ffc107 !important;
        }

        .bg-success {
            background-color: #218838 !important;
        }

        .display-1 {
            font-size: 5rem; /* Tamaño de fuente más grande */
            font-weight: bold; /* Hacer el número más destacado */
        }

        .categorias {
            display: flex;
            justify-content: space-around;
            margin-top: 20px;
            flex-wrap: wrap; /* Permite que las categorías se ajusten en dispositivos más pequeños */
        }

        .categoria-pacientes {
            text-align: center;
            padding: 20px;
            border-radius: 8px;
            color: white;
            flex: 1 1 150px; /* Permite que los elementos se expandan y contraigan */
            margin: 10px; /* Espaciado entre tarjetas */
        }

            .categoria-pacientes h3 {
                font-size: 2.7rem;
            }

        .categoria-card {
            text-align: center;
            padding: 10px;
            border-radius: 8px;
            color: white;
            flex: 1 1 150px; /* Permite que los elementos se expandan y contraigan */
            margin: 10px; /* Espaciado entre tarjetas */
        }

        .categoria-card-pediatrica {
            text-align: center;
            padding: 10px;
            border-radius: 8px;
            color: white;
            flex: 1 1 150px; /* Permite que los elementos se expandan y contraigan */
            margin: 10px; /* Espaciado entre tarjetas */
        }

            .categoria-card h2,
            .categoria-card-pediatrica h2 {
                font-size: 5rem;
            }

            .categoria-card p,
            .categoria-card-pediatrica p {
                font-size: 2.5rem;
            }


        .categoria-C1 {
            background-color: #DC3545;
        }

        .categoria-C2 {
            background-color: #FD7E14;
        }

        .categoria-C3 {
            background-color: #FFC107;
            color: #000000;
        }

        .categoria-C4 {
            background-color: #28A745;
        }

        .categoria-C5 {
            background-color: #17A2B8;
        }

        .categoria-SC {
            background-color: #5A6268;
        }

        /* EFECTO DE PULSACION */
        @keyframes latido {
            0% {
                transform: scale(1);
            }

            50% {
                transform: scale(1.17);
            }

            100% {
                transform: scale(1);
            }
        }

        .latido {
            animation: latido 1s infinite;
        }

        @media (max-width: 768px) {
            .container {
                flex-direction: column;
                padding: 20px;
            }

            .section {
                width: 100%;
                padding: 10px;
            }

            .titulo-pediatrico {
                font-size: 3rem;
            }
        }

        @media (max-width: 576px) {
            #reloj {
                font-size: 3rem;
                padding: 10px;
                margin-left: 20px;
            }

            h2.me-auto {
                font-size: 1.7rem;
                flex: 1;
                margin-left: -50px;
            }

            .logo-header {
                margin-left: 20px;
            }
        }

        @media (min-width: 1920px) {
            .container {
                padding: 10px;
            }

            .titulo-pediatrico {
                font-size: 6rem;
            }

            .display-1 {
                font-size: 7rem;
            }

            #reloj {
                font-size: 7rem;
            }
        }

        .mensaje-abajo {
            margin-top: -65px;
        }

        footer {
            background-color: #343a40;
            color: white;
            text-align: left;
            padding: 20px;
        }
    </style>
</head>
<body>

    <!-- Header -->
    <header class="main-header navbar bg-info text-white py-4 mb-5">
        <h2 id="hcm" class="me-auto">Hospital Clínico Magallanes</h2>
        <!-- Reloj -->
        <div id="reloj" class="text-white mx-4"></div>
        <img src="../../Style/img/HOSPLOGO.jpg" alt="Logo" class="logo-header ml-auto" />
    </header>

    <div class="container">
        <%-- ADULTOS --%>
        <div class="section">
            <h1 class="titulo-pediatrico">Adultos</h1>

            <!-- Pacientes Admitidos y en Atención ADULTOS -->
            <div class="categorias mb-6">
                <div class="categoria-pacientes bg-primary">
                    <h3>Pacientes en Espera</h3>
                    <span id="espera-pacientes" class="display-1"></span>
                </div>
                <div class="categoria-pacientes bg-success">
                    <h3>Pacientes en Atención</h3>
                    <span id="atendidos-pacientes" class="display-1"></span>
                </div>
            </div>

            <!-- Categorías de Pacientes adultos -->
            <div class="categorias">
                <div class="categoria-card categoria-C1">
                    <h2>C1</h2>
                    <br>
                    <p>Paciente crítico en atención</p>
                </div>
                <div class="categoria-card categoria-C2">
                    <h2>C2</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-c2" class="display-1"></span>
                </div>
                <div class="categoria-card categoria-C3">
                    <h2>C3</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-c3" class="display-1"></span>
                </div>
                <div class="categoria-card categoria-C4">
                    <h2>C4</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-c4" class="display-1"></span>
                </div>
                <div class="categoria-card categoria-C5">
                    <h2>C5</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-c5" class="display-1"></span>
                </div>
                <div class="categoria-card categoria-SC">
                    <h2>S/C</h2>
                    <p></p>
                </div>
            </div>
            <%-- FIN ADULTOS --%>
        </div>

        <div class="section">
            <%-- PEDIATRICOS --%>
            <h1 class="titulo-pediatrico">Pediátricos</h1>

            <!-- Pacientes Admitidos y en Atención PEDIATRICO -->
            <div class="categorias mb-6">
                <div class="categoria-pacientes bg-primary">
                    <h3>Pacientes en Espera</h3>
                    <span id="espera-pacientes-pediatrica" class="display-1"></span>
                </div>
                <div class="categoria-pacientes bg-success">
                    <h3>Pacientes en Atención</h3>
                    <span id="atendidos-pacientes-pediatrica" class="display-1"></span>
                </div>
            </div>

            <!-- Categorías de Pacientes Pediátricos -->
            <div class="categorias">
                <div class="categoria-card-pediatrica categoria-C1">
                    <h2>C1</h2>
                    <br>
                    <p>Paciente critico en atención</p>
                </div>
                <div class="categoria-card-pediatrica categoria-C2">
                    <h2>C2</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-ped-c2" class="display-1"></span>
                </div>
                <div class="categoria-card-pediatrica categoria-C3">
                    <h2>C3</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-ped-c3" class="display-1"></span>
                </div>
                <div class="categoria-card-pediatrica categoria-C4">
                    <h2>C4</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-ped-c4" class="display-1"></span>
                </div>
                <div class="categoria-card-pediatrica categoria-C5">
                    <h2>C5</h2>
                    <p></p>
                    <i class='fas fa-male display-1'> = </i> <span id="atendidos-pacientes-ped-c5" class="display-1"></span>
                </div>
                <div class="categoria-card-pediatrica categoria-SC">
                    <h2>S/C</h2>
                    <p></p>
                </div>
            </div>
            <%-- FIN PEDIATRICOS --%>
        </div>
    </div>

    <div class="text-center mt-5 mensaje-abajo">
        <h3><strong>Recuerde usar de manera adecuada los servicios de urgencia.<br />
            No olvide los otros dispositivos de mediana y baja complejidad como SAPU y SAR.</strong></h3>
    </div>

    <script src="../../Script/jquery3.4.1.js"></script>
    <script src="../../Script/toastr.min.js"></script>
    <script src="../../Script/moment.js"></script>
    <script src="../../Script/ModuloUrgencia/Pantallas/PantallaEsperaUrgencia.js"></script>
    <script src="../../Script/ModuloUrgencia/FuncionesURG.js"></script>
    <script src="../../Script/ModuloGEN/Funciones/Funciones.js"></script>
</body>
</html>

