﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="BandejaST.aspx.cs" Inherits="RegistroClinico.Vista.ModuloSolicitudTransfusion.BandejaST" %>

<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalGenericoMovimientosSistema.ascx" TagName="ModalGenericoMovimientosSistema" TagPrefix="wuc" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="../../Style/bootstrap-switch.css" />
    <link rel="stylesheet" href="../../Style/select2/select2.css">
    <link rel="stylesheet" href="../../Style/select2/select2-bootstrap4.css">
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/select2.full.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <input type="hidden" id="idSolicitudTransfusion" value="0" />
    <input type="hidden" id="tipoAccion" value="0" />

    <!-- Componente de titulo-->
    <titulo-pagina data-title="Solicitud de Transfusión"></titulo-pagina>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-body">
                    <button data-toggle="collapse" class="btn btn-outline-primary container-fluid"
                        data-target="#divForm" onclick="return false;">
                        FILTRAR BANDEJA</button>
                    <div id="divForm" class="card card-body collapse">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="txtSTFiltroId">ID/NRO.</label>
                                <input id="txtSTFiltroId" type="text" class="form-control" placeholder="De la Solicitud">
                            </div>
                            <div class="col-md-2">
                                <label for="sltTipoIdentificacion">Tipo de identificación</label>
                                <select id="sltTipoIdentificacion" class="form-control"></select>
                            </div>
                            <div class="col-md-2">
                                <label id="lblTipoIdentificacion" for="txtSTFiltroRut">RUT</label>
                                <div class="input-group">
                                    <input id="txtSTFiltroRut" type="text" class="form-control" style="width: 110px;" placeholder="Del Paciente">
                                    <div class="input-group-prepend digito" id="separadorDV">
                                        <div class="input-group-text">-</div>
                                    </div>
                                    <input type="text" id="txtSTFiltroDV" class="form-control digito" style="width: 10px" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="txtSTFiltroNombre">Nombre</label>
                                <input id="txtSTFiltroNombre" type="text" class="form-control" placeholder="Del Paciente">
                            </div>
                            <div class="col-md-2">
                                <label for="txtSTFiltroApeParteno">Primer Apellido</label>
                                <input id="txtSTFiltroApeParteno" type="text" class="form-control" placeholder="Del Paciente">
                            </div>
                            <div class="col-md-2">
                                <label for="txtSTFiltroSApeMaterno">Segundo Apellido</label>
                                <input id="txtSTFiltroSApeMaterno" type="text" class="form-control" placeholder="Del Paciente">
                            </div>
                            <div class="col-md-2">
                                <label for="sltSTEstado">Estado</label>
                                <select id="sltSTEstado" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="sltAmbito">Ambito</label>
                                <select id="sltAmbito" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4">
                                <div class="text-right">
                                    <button type="button" id="btnFiltro" class="btn btn-success"><i class="fa fa-search"></i>Buscar</button>
                                    <button id="btnLimpiarFiltro" class="btn btn-secondary text-white"><i class="fa fa-eraser"></i>Limpiar Filtros</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px; padding-top: 20px;">
                        <div class="col-md-12 text-right">
                            <!--OnClick="lnbExportarH_Click"-->
                            <asp:Button runat="server" ID="lnbExportarH" Style="display: none;" />
                            <a id="lnbExportar" class="btn btn-info waves-effect waves-light" href="#\">
                                <i class="fa fa-file-excel"></i>Exportar XLS
                            </a>
                            <!--
                            <a id="lnbNuevoST" class="btn btn-info" href="NuevaSolicitudTransfusion.aspx">
                                <i class="fa fa-plus"></i>Nueva Solicitud de Transfusión
                            </a>
                            -->
                            <a id="btnRefrescar" class="btn btn-primary" onclick="cargarBandejaST()"><i class="fa fa-refresh" aria-hidden="true"></i>Refrescar</a>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <table id="tblST" class="table table-striped table-bordered table-hover w-100"></table>
                        </div>
                    </div>
                    <div class="card-body row" style="min-height: 80px;">
                        <div class="col">
                                        <button class="btn 
                                        btn-outline-primary btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión solicitada">
                                        <b>S</b>
                                        </button>
                                        Transfusión solicitada.&nbsp;&nbsp;
                                        <button class="btn 
                                        btn-outline-success btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión recepcionada">
                                        <b>R</b>
                                        </button>
                                        Transfusión recepcionada.&nbsp;&nbsp;
                                        <button class="btn 
                                        btn-outline-secondary btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión procesada">
                                        <b>P</b>
                                        </button>
                                        Transfusión procesada.&nbsp;&nbsp;
                                        <button class="btn 
                                        btn-outline-dark btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión finalizada">
                                        <b>F</b>
                                        </button>
                                        Transfusión finalizada.&nbsp;&nbsp;
                                        <button class="btn 
                                        btn-outline-danger btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión anulada">
                                        <b>A</b>
                                        </button>
                                        Transfusión anulada.
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class="modal modal-print fade" id="mdlImprimirSolicitudTransfusion" tabindex="-1">
        <div class="modal-dialog modal-fluid" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead">
                        <strong><i class="fa fa-print"></i>Impresión</strong>
                    </p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body-print">
                        <iframe id="frameST" frameborder="0" onload="$('#modalCargando').hide();"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Control de usuario moviemientos de sistema-->
    <wuc:ModalGenericoMovimientosSistema ID="ModalGenericoMovimientosSistema" runat="server" />
    <div class="modal fade" id="mdlAlerta" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info center-horizontal" style="justify-content: center">
                    <div class="center-horizontal">Alerta de sistema</div>
                </div>
                <div class="modal-body">
                    <div id="mdlAlertaCuerpo" class="row center-horizontal">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class='col-sm-3'><a id='linkConfirmar' class='btn btn-block' href='#/'>Confirmar </a></div>
                    <div class='col-sm-3'><a id='linkCancelar' class='btn btn-default btn-block' href='#/' onclick="$('#mdlAlerta').modal('hide');">Cancelar </a></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Recibir ST-->
    <div class="modal fade" id="modalRecibirST" role="dialog" aria-labelledby="mdlRecibirST" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <p class="heading lead" id="tituloModalRecibirFinalizar"></p>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row" id="divCierreIc">
                        <div class="col-md-8">
                            <p>
                                <b>Nombre paciente:</b> 
                                <span id="txtNombrePaciente"></span>
                            </p>
                            <p>
                                <b>Procedimiento:&nbsp;</b>
                                <span id="txtProcedimiento"></span>
                                &nbsp;&nbsp;&nbsp;
                                <b>Motivo:</b>
                                <span id="txtMotivoTransfusion"></span>
                            </p>
                            <p>
                                <b>Solicitado por:&nbsp;</b>
                                <span id="txtProfSolicita"></span>
                            </p>
                        </div>
                        <div class="col-md-4">
                             <p>
                                <b>Rut</b> 
                                <span id="txtRutPaciente"></span> 
                            </p>
                            <p>
                                <b>Fecha</b>
                                <span id="txtFechaSolicitud"></span>
                            </p>
                            <p>
                                <b>Ámbito:&nbsp;</b>
                                <span id="txtAmbito"></span>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p>
                                <b>Prioridad:&nbsp;</b>
                                <span id="txtPrioridad"></span>
                            </p>
                        </div>
                        <div class="col-md-12"  >
                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group" id="divMotivoAnulacion">
                                        <label for="motivoAnulacion">Motivo de anulación</label>
                                        <textarea id="motivoAnulacion" class="form-control" data-required="true" rows="4" style="resize:none;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-warning col-md-12 w-100" role="alert" style="color: #856404 !important; background-color: #fff3cd !important; border-color: #ffeeba !important;"">
                            <p class="mb-0"><i class="fa fa-info-circle fa-lg"></i>&nbsp;Una vez recibida la solicitud de transfusión, no se podrá cancelar</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id="btnRecibirST" class="btn btn-success" onclick="return false;">Aceptar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Banco de Sangre-->
    <div class="modal fade" id="modalBancoSangre" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h4 id="modalBancoSangreTitulo">Información Banco de Sangre</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- BODY -->

                    <div class="row">
                        <div class="col-md-3">
                            <label for="dtFechaRecepcionBS">
                                Fecha Recepción:
                            </label>
                            <input id="dtFechaRecepcionBS" type="date" class="form-control" data-required="true" disabled />
                        </div>
                        <div class="col-md-3">
                            <label for="tmHoraRecepcionBS">
                                Hora Recepción:
                            </label>
                            <input id="tmHoraRecepcionBS" type="time" class="form-control" data-required="true" disabled/>
                        </div>
                        <div class="col-md-3">
                            <label for="sltGrupoSanguineoBS">
                                Grupo Sanguíneo:
                            </label>
                            <select id="sltGrupoSanguineoBS" class="form-control" data-required="true" disabled="disabled"></select>
                        </div>
                    </div>
                    <hr />
                    <h5 class="mb-0"><strong><i class="fa fa-angle-right"></i>Técnologo Médico Responsable</strong></h5>
                    <br />
                    <div class="row">
                        <div class="col-md-3">
                            <label for="txtNumeroDocumentoTecnologo">Número Documento</label>
                            <input id="txtNumeroDocumentoTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtNombreTecnologo">Nombre/es</label>
                            <input id="txtNombreTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApePatTecnologo">Primer Apellido</label>
                            <input id="txtApePatTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApeMatTecnologo">Segundo Apellido</label>
                            <input id="txtApeMatTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="txaObservacionesBS">
                                Observaciones:
                            </label>
                            <textarea id="txaObservacionesBS" class="form-control" data-required="true" rows="4"></textarea>
                        </div>
                    </div>




                </div>
                <div class="modal-footer justify-content-center">
                    <!-- <button id="btn" class="btn btn-success" onclick="return false;">Aceptar</button> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> -->
                </div>
            </div>
        </div>
    </div>



    <!-- Modal Banco de Sangre-->
    <div class="modal fade" id="modalProcesarST" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h4 id="modalProcesarSTTitulo">Procesar Solicitud de Transfusión Id: <span id="txtIdSolicitud"></span></h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- BODY -->
                    <input hidden type="text" id="idProcesarST" />
                    <div class="" id="divInputProcesar">
                        <div class="row p-1">
                            <div class="col-md-5 col-sm-12">
                                <span><b>Nombre:</b></span> <span id="txtNombrePacienteSolicitud"></span>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <span><b>Rut:</b></span> <span id="txtRutPacienteSolicitud"></span>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                 <span><b>Id Atencion</b></span> <span id="txtIdAtencion"></span>
                            </div>
                         </div>

                        <div class="row p-1">
                            <div class="col-md-5 col-sm-12">
                                 <span><b>Solicitado por:</b></span> <span id="txtProfesionalSolicitaTransfusion"></span>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                 <span><b>Fecha solicitud</b></span> <span id="txtFechaSolicitudTransfusion"></span>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                 <span><b>Fecha recepción</b></span> <span id="txtFechaRecepcionTransfusion"></span>
                            </div>
                        </div>

                        <div class="row p-1">
                            <div class="col-md-5 col-sm-12">
                                 <span><b>Motivo:</b></span> <span id="txtMotivoSolicitud"></span>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                 <span><b>Ámbito:</b></span> <span id="txtAmbitoTransfusion"></span>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                 <span><b>Procedimiento:</b></span> <span id="txtProcedimientoSolicitud"></span>
                            </div>
                        </div>

                        <div class="row p-1">
                            <div class="col-md-4 col-sm-12">
                                <span><b>Proridad:</b></span> <span id="txtPrioridadSolicitud"></span>
                            </div>
                        </div>
                        
                        <div class="row p-1">
                            <div class="col-md-12 col-sm-12">
                                <span><b>Diagnóstico:</b></span> <span id="txtDiagnosticoSolicitud"></span>
                            </div>
                        
                            <div class="col-md-col-12 col-sm-12">
                                <span><b>Observaciones medicas:</b></span> <span id="txtObservacionesSolicitud"></span>
                            </div>
                        </div>


                        <div class="row p-1">
                            <div class="col-md-6 col-sm-12" id="divGrupoSanguineo">
                                <label for="sltGrupoSanguineoBS">
                                    Grupo Sanguíneo:
                                </label>
                                <select id="sltGrupoSanguineoProcesar" class="form-control" data-required="true"></select>
                            </div>

                        
                            <div class="col-md-6 col-sm-12">
                                <label for="txaObservacionesBS">
                                    Observaciones:
                                </label>
                                <textarea id="procesarObservacion" 
                                    class="form-control" 
                                    placeholder="Ingrese obsevaciones" 
                                    data-required="true" 
                                    onkeydown="validarLongitud(this,298,event)"
                                    rows="2" style="resize:none;"></textarea>
                            </div>
                        </div>

                        <div class="row p-1" id="ListaExamenes">

                        </div>

                        <div class="row p-1" id="ListaHemocomponentes">

                        </div>
                        <div class="row m-2">
                            <div class="col-sm-12" id="divCheckReaccion">
                                <label for="checkReaccionAdversa">Reacción transfusional adversa?</label>
                                <input type="checkbox" id="checkReaccionAdversa" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button id="btn" type="button" class="btn btn-success" onclick="procesarSolicitudTransfusion()">Procesar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Cerrar Solicitud Tranfsucion -->
    <div class="modal fade" id="modalCerrarST" role="dialog" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <h4 id="modalCerrarSTTitulo">Cerrar Solicitud de Transfusión</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true" class="white-text">x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- BODY -->
                    <input hidden type="text" id="idCerrarST" />
                    <div class="row">
                        <div class="col-md-2">
                            <label>Solicitud</label>
                            <input type="text" class="form-control" id="idSolicitudCierre" />
                        </div>
                        <div class="col-md-6">
                            <label>Paciente</label>
                            <input type="text" class="form-control" id="cierrePaciente" />
                        </div>
                        <div class="col-md-4">
                            <label>Tipo</label>
                            <input type="text" class="form-control" id="cierreHemocomponente" />
                        </div>

                        <div class="col-md-3 col-sm-12" id="divMotivoCierre">
                            <label for="sltGrupoSanguineoBS">
                                Motivo de Cierre:
                            </label>
                            <select id="cerrarMotivoCierre" class="form-control" data-required="true"></select>
                        </div>
                        <div class="col-md-7 col-sm-12">
                            <label for="txtObservacionCierre">
                                Observacion de Cierre:
                            </label>
                            <textarea id="txtObservacionCierre" 
                                class="form-control" 
                                placeholder="Ingrese obsevaciones" 
                                onkeydown="validarLongitud(this,299,event)"
                                rows="2" style="resize:none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" id="btnCerrar" class="btn btn-success" onclick="cerrarSolicitudTransfusion()">Aceptar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="<%= ResolveClientUrl("~/Script/ModuloSolicitudTransfusion/BandejaST.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloSolicitudTransfusion/BandejaST.links.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloSolicitudTransfusion/SolicitudTransfusion.Combo.jsv=") + "?" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/UserControls/ModalGenericoMovimientosSistema.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
