﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/MasterPages/SiteGeneral.Master" AutoEventWireup="true" CodeBehind="NuevaSolicitudTransfusion.aspx.cs" Inherits="RegistroClinico.Vista.ModuloSolicitudTransfusion.NuevaSolicitudTransfusion" %>

<%@ Register Src="~/Vista/UserControls/Paciente.ascx" TagName="Paciente" TagPrefix="wuc" %>
<%@ Register Src="~/Vista/UserControls/ModalAlerta.ascx" TagName="mdlAlerta" TagPrefix="wuc" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
    <link href="../../Style/jquery.typeahead.min.css" rel="stylesheet" />
    <link href="../../Style/bootstrap-switch.css" rel="stylesheet" />
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <wuc:mdlAlerta ID="wucMdlAlerta" runat="server" />
    <div class="card card-body">
        <div class="card-header bg-light">
            <!-- Componente de titulo-->
            <titulo-pagina data-title="Nuevo/Editar Solicitud de Transfusión"></titulo-pagina>
        </div>
        <div class="card-body bg-p">
            <div class="row">
                <div class="col-md-12">
                    <wuc:Paciente ID="wucPaciente" runat="server" />
                </div>
            </div>

            <div id="divTransfusionesPrevias" class="row card">
                <div class="card-header col-md-12 bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Información de transfusiones previas</strong></h5>
                </div>
                <div class="col-md-12 p-3">
                    <table id="tblTransfusionesPrevias" class="table table-striped table-bordered table-hover w-100"></table>
                </div>
            </div>
            <div id="divDatosST" class="row card text-white cuerpoST">
                <div class="card-header bg-dark">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Datos de Solicitud</strong></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="sltTipoST">
                                Tipo Solicitud:
                            </label>
                            <select id="sltTipoST" class="form-control" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-4" id="dvPrioridadST">
                            <label>Carácter de la transfusión</label>
                            <select id="sltPrioridadST" class="form-control" data-required="true"></select>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-6 d-none" id="divObservacionesPrioridad">
                            <label>Observaciones de la reserva (procedimiento, fecha, hora, servicio) </label>
                            <textarea id="txtObservacionesReserva" onkeydown="validarLongitud(this,295,event)" class="form-control" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="dtFechaST">
                                Fecha:
                            </label>
                            <input id="dtFechaST" class="form-control" type="date" data-required="true" />
                        </div>
                        <div class="col-sm-2">
                            <label for="tmHoraST">
                                Hora:
                            </label>
                            <input id="tmHoraST" class="form-control" type="time" data-required="true" />
                        </div>
                        <div class="col-sm-2">
                            <label for="sltAmbito">
                                Ambito:
                            </label>
                            <select id="sltAmbito" class="form-control">
                            </select>
                        </div>
                        <div class="col-sm-2" id="divIDHosOrUrg">
                            <label for="txtIDHOsOrUrg">
                                ID:
                            </label>
                            <input id="txtIDHOsOrUrg" type="text" class="form-control" disabled="disabled">
                        </div>

                        <div class="col-md-4">
                            <label for="sltTipoMotivoST">
                                Observaciones
                            </label>
                            <select id="sltTipoMotivoST" class="form-control" name="sltTipoMotivoST" data-required="true">
                            </select>
                        </div>
                        <div class="col-md-12 col-sm-12 d-none" id="divOtrasObservaciones">
                            <label>Ingrese observaciones</label>
                            <textarea class="form-control" id="txtOtrasObservaciones" maxlength="50" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-3">
                            <label for="txaDiagnostico">
                                Diagnostico:
                            </label>
                            <textarea id="txaDiagnosticoST" class="form-control" maxlength="298" style="resize: none;" data-required="true" rows="4"></textarea>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-3">
                            <label for="txtObservacionSolicitud">
                                Motivo:
                            </label>
                            <textarea id="txtObservacionSolicitud" class="form-control" maxlength="298" placeholder="Ingrese motivo" style="resize: none;" rows="4"></textarea>

                        </div>
                    </div>
                </div>
            </div>

            <div id="divSangriaST" class="card cuerpoST" style="display: none;">
                <div class="card-header bg-dark text-center text-white">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Sangría Terapéutica</strong></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <label for="txtNumeroSesiones">N° de sesiones</label>
                            <input id="txtNumeroSesiones" type="text" class="form-control number" data-required="true" onkeydown="validarLongitud(this, 3,event)" placeholder="Especifíque cantidad sesiones" />
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label for="textCantidadSesiones">Cantidad por sesión</label>
                            <input id="textCantidadSesiones" type="text" class="form-control number" data-required="true" onkeydown="validarLongitud(this, 3,event)" placeholder="Especifíque cantidad por sesion" />
                        </div>
                    </div>
                    <hr />

                </div>
            </div>


            <div id="divEspecificacionesST" class="card" style="display: none;">
                <div class="card-header bg-dark text-white">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Hemocomponente Solicitado</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mx-auto">
                        <div class="col-md-12">
                            <div id="listadoHemocomponentes"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divMedicoResponsableIndicacionST" class="card cuerpoST row">
                <div class="card-header bg-dark text-white">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Identificación Médico Responsable de Indicación</strong></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="txtNumeroDocumentoProfesional">Número Documento</label>
                            <input id="txtNumeroDocumentoProfesional" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtNombreProfesional">Nombre/es</label>
                            <input id="txtNombreProfesional" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApePatProfesional">Primer Apellido</label>
                            <input id="txtApePatProfesional" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApeMatProfesional">Segundo Apellido</label>
                            <input id="txtApeMatProfesional" type="text" class="form-control" disabled="disabled">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--
            <div id="divUsoBancoDeSangreST" class="card">
                <div class="card-header bg-dark text-white">
                    <h5 class="mb-0"><strong><i class="fa fa-angle-double-right"></i>Uso Exclusivo Banco de Sangre</strong></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="dtFechaRecepcionBS">
                                Fecha Recepción:
                            </label>
                            <input id="dtFechaRecepcionBS" type="date" class="form-control" data-required="true" />
                        </div>
                        <div class="col-md-3">
                            <label for="tmHoraRecepcionBS">
                                Hora Recepción:
                            </label>
                            <input id="tmHoraRecepcionBS" type="time" class="form-control" data-required="true" />
                        </div>
                        <div class="col-md-3">
                            <label for="sltGrupoSanguineoBS">
                                Grupo Sanguíneo:
                            </label>
                            <select id="sltGrupoSanguineoBS" class="form-control" data-required="true"></select>
                        </div>
                    </div>
                    <hr />
                    <h5 class="mb-0"><strong><i class="fa fa-angle-right"></i>Técnologo Médico Responsable</strong></h5>
                    <br />
                    <div class="row">
                        <div class="col-md-3">
                            <label for="txtNumeroDocumentoTecnologo">Número Documento</label>
                            <input id="txtNumeroDocumentoTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtNombreTecnologo">Nombre/es</label>
                            <input id="txtNombreTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApePatTecnologo">Primer Apellido</label>
                            <input id="txtApePatTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                        <div class="col-md-3">
                            <label for="txtApeMatTecnologo">Segundo Apellido</label>
                            <input id="txtApeMatTecnologo" type="text" class="form-control" disabled="disabled">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="txaObservacionesBS">
                                Observaciones:
                            </label>
                            <textarea id="txaObservacionesBS" class="form-control" data-required="true" rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            -->
    </div>
    <div class="card-footer text-right">
        <a id="btnGuardarST" class="btn btn-primary waves-effect waves-light" onclick='clickBtnGuardarST(); return false;'>
            <i class="fa fa-save"></i>GUARDAR
        </a>
        <a id="btnCancelarST" href="BandejaST.aspx" class="btn btn-default waves-effect waves-light">
            <i class="fa fa-remove"></i>CANCELAR
        </a>
    </div>
    <script src="<%= ResolveClientUrl("~/Script/bootstrap-switch.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/jquery.typeahead.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloSolicitudTransfusion/SolicitudTransfusion.Combo.js") + "?v=" + GetVersion() %>"></script>
    <script src="<%= ResolveClientUrl("~/Script/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.js") + "?v=" + GetVersion() %>"></script>
</asp:Content>
