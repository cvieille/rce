﻿using Newtonsoft.Json;
using RegistroClinico.Dtos;
using System.IO;
using System.Web;

namespace RegistroClinico.Negocio
{
    public class FuncionesAppSetting
    {
        public static AppSettingDto GetAppSetting()
        {
            string root = HttpContext.Current.Server.MapPath("~/");
            string path = $"{root}AppSetting.json";

            using (var reader = new StreamReader(path))
            {
                var appSettings = JsonConvert.DeserializeObject<AppSettingDto>(reader.ReadToEnd());
                return appSettings;
            }
        }
        
        public static void SetCambiarVersion(string version)
        {
            var appSetting = GetAppSetting();
            appSetting.Version = version;
            
            string root = HttpContext.Current.Server.MapPath("~/");
            string path = $"{root}AppSetting.json";

            using (var writter = new StreamWriter(path))
            {
                var appSettings = JsonConvert.SerializeObject(appSetting);
                writter.WriteLine(appSettings);
            }
        }

        public static string GetUrlWebApi()
        {
            var appSetting = GetAppSetting();
            var urlWebApi = appSetting.WebApi.Find(a => a.Ambiente == appSetting.Ambiente);
            return urlWebApi.Url;
        }

        public static string GetUrlPabellon()
        {
            var appSetting = GetAppSetting();
            var urlPabellon = appSetting.Pabellon.Find(a => a.Ambiente == appSetting.Ambiente);
            return urlPabellon.Url;
        }

        public static string GetUrlApiFunciones()
        {
            var appSetting = GetAppSetting();
            var urlApiFunciones = appSetting.ApiFunciones.Find(a => a.Ambiente == appSetting.Ambiente);
            return urlApiFunciones.Url;
        }

    }
}