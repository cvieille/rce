﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace RegistroClinico
{
    public class ConexionSQL
    {
        private readonly string sConnectionString;
        public ConexionSQL()
        {
            //connectionString = ConfigurationManager.ConnectionStrings["local"].ConnectionString;
            //sConnectionString = ConfigurationManager.ConnectionStrings["Connection175"].ConnectionString;
            //connectionString = ConfigurationManager.ConnectionStrings["Connection176"].ConnectionString;
            sConnectionString = ConfigurationManager.ConnectionStrings["Connection235"].ConnectionString;
        }

        public DataTable GetDataTable(string sQuery, string[][] sArray = null)
        {
            SqlConnection con = new SqlConnection(sConnectionString);
            SqlCommand cmd = new SqlCommand(sQuery, con);

            if (sArray != null)
                foreach (string[] sString in sArray)
                    if (sString[1].ToLower() == "null")
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = DBNull.Value;
                    else
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = sString[1];

            SqlDataReader reader;
            con.Open();
            reader = cmd.ExecuteReader();

            DataTable dtDataTable = new DataTable();
            dtDataTable.Load(reader);
            con.Close();

            return dtDataTable;
        }

        /// <summary>
        /// Crea un Insert en la base de datos, opcionalmente retorna el valor del ID insertado
        /// Si el bID es seteado en true la consulta deberá devolver con OUTPUT INSERTED.[campo] el id insertado
        /// </summary>
        /// <param name="sQuery">Query con el Insert</param>
        /// <param name="bID">Valor opcional por defecto en false</param>
        /// <returns></returns>
        public int InsertQuery(string sQuery, bool bID = false, string[][] sArray = null)
        {
            SqlConnection con = new SqlConnection(sConnectionString);
            SqlCommand cmd = new SqlCommand(sQuery, con);
            if (sArray != null)
                foreach (string[] sString in sArray)
                    if (sString[1].ToLower() == "null")
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = DBNull.Value;
                    else
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = sString[1];

            con.Open();
            int iNewId = 0;
            if (bID)
                iNewId = (int)cmd.ExecuteScalar();
            else
                cmd.ExecuteScalar();

            con.Close();

            return iNewId;
        }

        public void UpdateQuery(string sQuery, string[][] sArray = null)
        {
            SqlConnection con = new SqlConnection(sConnectionString);

            SqlCommand cmd = new SqlCommand(sQuery, con);
            if (sArray != null)
                foreach (string[] sString in sArray)
                    if (sString[1].ToLower() == "null")
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = DBNull.Value;
                    else
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = sString[1];

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public int GetCount(string sQuery, string[][] sArray = null)
        {
            SqlConnection con = new SqlConnection(sConnectionString);
            SqlCommand cmd = new SqlCommand(sQuery, con);
            if (sArray != null)
                foreach (string[] sString in sArray)
                    if (sString[1].ToLower() == "null")
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = DBNull.Value;
                    else
                        cmd.Parameters.Add(sString[0], SqlDbType.VarChar).Value = sString[1];

            con.Open();
            int iCount = (int)cmd.ExecuteScalar();
            con.Close();

            return iCount;
        }
    }
}