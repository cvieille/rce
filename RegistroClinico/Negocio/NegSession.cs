﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;

namespace RegistroClinico.Negocio
{
    public class NegSession
    {
        public readonly Page _page;
        private readonly bool _esIframe;
        public NegSession(Page page)
        {
            _page = page;
            _esIframe = (page.Request.QueryString["iframe"] != null);
        }

        public void ValidarPermisosSession()
        {
            if (!_page.IsPostBack)
            {
                if (_page.Session["TOKEN"] == null)
                {
                    RedireccionarLogin();
                }
                else
                {
                    ValidarToken401();
                    ValidarMenu();
                }
            }
        }

        private void ValidarMenu()
        {

            string url = _page.Request.Url.AbsoluteUri.Replace("%C3%B1", "ñ");
            bool esValido = false;
            string path = HttpContext.Current.Server.MapPath("~/Config/Rutas.json");

            Dictionary<string, object> json = Funciones.ObtenerJsonFile(path);
            JsonSerializerSettings setting = new JsonSerializerSettings
            {
                Culture = new System.Globalization.CultureInfo("es-ES")
            };
            List<Dictionary<string, object>> list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json["RutasAcceso"].ToString(), setting);

            string codPerfil = Convert.ToString(_page.Session["CODIGO_PERFIL"]);/*_page.Request.Cookies["DATA-CODIGO_PERFIL"].Value*/

            if (codPerfil != "")
            {
                foreach (Dictionary<string, object> j in list)
                {
                    byte[] bytes = Encoding.GetEncoding(1252).GetBytes(j["Ruta"].ToString());
                    var ruta = Encoding.UTF8.GetString(bytes);

                    if (url.Contains(ruta))
                    {

                        List<int> listPerfilAcceso = JsonConvert.DeserializeObject<List<int>>(j["PerfilAcceso"].ToString());
                        foreach (int cod in listPerfilAcceso)
                        {

                            if (int.Parse(codPerfil) == cod)
                            {
                                esValido = true;
                                break;
                            }
                        }
                        if (esValido) break;
                    }
                }
            }

            if (!esValido)
                RedireccionarLogin();
        }

        private void ValidarToken401()
        {

            //GEN_Usuarios/LOGEADO
            if (_page.Session["TOKEN"] /*_page.Request.Cookies.Get("DATA-TOKEN")*/ != null)
            {
                HttpContext context = HttpContext.Current;
                string dominio = FuncionesAppSetting.GetUrlWebApi();
                string url = string.Format("{0}GEN_Usuarios/LOGEADO", dominio);
                string token = string.Format("Bearer {0}", _page.Session["TOKEN"] /*_page.Request.Cookies["DATA-TOKEN"].Value*/);

                try
                {
                    using (System.Net.WebClient WebClient = new System.Net.WebClient())
                    {
                        WebClient.Headers.Add("Authorization", token);
                        WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                        string result = WebClient.DownloadString(url);
                    }
                }
                catch (System.Net.WebException ex)
                {
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)ex.Response;
                    // VALIDA SI NO HA TERMINADO EL TIEMPO DEL TOKEN
                    if ((response != null && response.StatusCode == System.Net.HttpStatusCode.Unauthorized) || (response == null))
                        RedireccionarLogin();
                }
            }

        }

        private void RedireccionarLogin()
        {
            if (_esIframe)
                _page.Response.Write("<script>window.top.location.href = '../Default.aspx';</script>");
            else
                _page.Response.Redirect("~/Vista/Default.aspx", false);
            _page.Response.End();
        }

    }
}