﻿using ClosedXML.Excel;
using Newtonsoft.Json;
//using SelectPdf;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RegistroClinico
{
    public class Funciones
    {

        public static readonly string sVersion = DateTime.Now.ToString("yyyyMMddHHmmss");

        /// <summary>
        /// Muestra u oculta columna de un GridView
        /// </summary>
        /// <param name="gvGrilla">Grilla</param>
        /// <param name="iColumn">Columna a ocultar</param>
        /// <param name="bVisible">Visibilidad</param>
        public static void VisibilidadColumna(GridView gvGrilla, int iColumn, bool bVisible)
        {
            if (gvGrilla.Rows.Count > 0)
            {
                gvGrilla.HeaderRow.Cells[iColumn].Visible = bVisible;
                foreach (GridViewRow row in gvGrilla.Rows)
                    row.Cells[iColumn].Visible = bVisible;
            }
        }

        /// <summary>
        /// Invierte fechas separadas por guión
        /// </summary>
        /// <param name="sFecha">Fecha en formato separado por guiones (yyyy-MM-dd)</param>
        public static string InvertirFecha(string sFecha)
        {
            return string.Join("-", new string[] { sFecha.Split('-')[2].Trim(), sFecha.Split('-')[1].Trim(), sFecha.Split('-')[0].Trim() });
        }



        /// <summary>
        /// Desactiva completamente un LinkButton
        /// </summary>
        /// <param name="lbBoton"></param>
        public static void DisableLinkButton(LinkButton lbBoton)
        {

            if (!lbBoton.CssClass.Contains("disabled"))
                lbBoton.CssClass += " disabled";
            lbBoton.CssClass = lbBoton.CssClass.Replace("info", "secondary");

            lbBoton.CssClass.Replace("btn-outline-info", "btn-outline-secondary");
            lbBoton.Attributes.Remove("href");
            lbBoton.Attributes.CssStyle[HtmlTextWriterStyle.Color] = "gray";
            lbBoton.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "not-allowed";
            if (lbBoton.Enabled != false)
                lbBoton.Enabled = false;

            if (lbBoton.OnClientClick != null)
                lbBoton.OnClientClick = null;

        }

        /// <summary>
        /// Convierte un string a MD5, generalmente es usado para encriptar contraseñas
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        public static string MD5Hash(string sInput)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            StringBuilder sbBuilder = new StringBuilder();

            byte[] byData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(sInput));
            for (int i = 0; i < byData.Length; i++)
                sbBuilder.Append(byData[i].ToString("x2"));
            return sbBuilder.ToString();
        }

        public static void Credentials()
        {
            UserImpersonation impersonator = new UserImpersonation();
            impersonator.impersonateUser("desarro", "", "Hcm2016");
        }

        public static void CrearHojaExcel(ref XLWorkbook workbook, string nombreHoja, List<Dictionary<string, string>> list)
        {
            if (list.Count == 0)
            {
                workbook.Worksheets.Add(nombreHoja);
                return;
            }
            //EXCEL NO PERMITE HOJAS CON NOMBRES MAYOR A 30 CARACTERES
            if (nombreHoja.Length > 25)
                nombreHoja = nombreHoja.Substring(0, 25);
            IXLWorksheet worksheet;
            worksheet = workbook.Worksheets.Add((workbook.Worksheets.Count + 1).ToString() + "-" + nombreHoja);

            int iRow = 1;
            List<string> keys = new List<string>(list[0].Keys);
            int iColumnCount = keys.Count;

            int iC = 0;
            for (int c = 0; c < iColumnCount; c++)
            {
                worksheet.Cell(iRow, iC + 2).Value = keys[c];
                iC++;
            }

            for (int i = 0; i < list.Count; i++)
            {
                worksheet.Cell(i + 1 + iRow, 1).Value = (i + 1);
                iC = 0;

                for (int c = 0; c < iColumnCount; c++)
                {
                    string sText = list[i][keys[c]].ToString();
                    if (string.IsNullOrEmpty(sText))
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(230, 230, 230);
                    else
                        worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(sText);

                    worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(list[i][keys[c]].ToString());
                    iC++;
                }
            }

            worksheet.CellsUsed().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            worksheet.Column(1).Cells().Style.Fill.BackgroundColor = XLColor.FromArgb(188, 218, 255);
            worksheet.ColumnsUsed().AdjustToContents();

            IXLRange rangeTable = worksheet.Range(1, 2, worksheet.LastCellUsed().Address.RowNumber, worksheet.LastCellUsed().Address.ColumnNumber);
            IXLTable table = rangeTable.CreateTable();
        }

        public static void CrearHojaExcel(ref XLWorkbook workbook, string nombreHoja, GridView gv)
        {

            //EXCEL NO PERMITE HOJAS CON NOMBRES MAYOR A 30 CARACTERES
            if (nombreHoja.Length > 25)
                nombreHoja = nombreHoja.Substring(0, 25);

            IXLWorksheet worksheet;
            worksheet = workbook.Worksheets.Add((workbook.Worksheets.Count + 1).ToString() + "-" + nombreHoja);

            int iRow = 1;
            int iColumnCount = gv.Columns.Count;

            int iC = 0;
            for (int c = 0; c < iColumnCount; c++)
            {
                worksheet.Cell(iRow, iC + 2).Value = gv.Columns[c].HeaderText;
                iC++;
                if (!gv.Columns[c].Visible)
                    iC--;
            }

            for (int i = 0; i < gv.Rows.Count; i++)
            {
                worksheet.Cell(i + 1 + iRow, 1).Value = (i + 1);

                iC = 0;

                for (int c = 0; c < iColumnCount; c++)
                {
                    TableCell cell = gv.Rows[i].Cells[c];
                    if (cell.Text == "&nbsp;")
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(230, 230, 230);
                    else
                        worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(cell.Text);

                    if (cell.CssClass == "alert alert-danger")
                    {
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 215, 218);
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Font.FontColor = XLColor.FromArgb(114, 28, 36);
                    }
                    iC++;
                    if (!gv.Columns[c].Visible)
                        iC--;
                }

                if (gv.Rows[i].CssClass == "alert alert-warning")
                {
                    worksheet.Row(i + 1 + iRow).Cells(2, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 243, 205);
                    worksheet.Row(i + 1 + iRow).Cells(2, iC + 2).Style.Font.FontColor = XLColor.FromArgb(133, 100, 4);
                }
            }

            worksheet.CellsUsed().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            worksheet.Column(1).Cells().Style.Fill.BackgroundColor = XLColor.FromArgb(188, 218, 255);
            worksheet.ColumnsUsed().AdjustToContents();

            IXLRange rangeTable = worksheet.Range(1, 2, worksheet.LastCellUsed().Address.RowNumber, worksheet.LastCellUsed().Address.ColumnNumber);
            IXLTable table = rangeTable.CreateTable();

        }

        public static void CrearHojaExcel(ref XLWorkbook workbook, string nombreHoja, GridView gv, string[] sTitle, string[] sData, int iColor = -1)
        {
            //EXCEL NO PERMITE HOJAS CON NOMBRES MAYOR A 30 CARACTERES
            if (nombreHoja.Length > 25)
                nombreHoja = nombreHoja.Substring(0, 25);
            IXLWorksheet worksheet;
            worksheet = workbook.Worksheets.Add((workbook.Worksheets.Count + 1).ToString() + "-" + nombreHoja);
            if (iColor == 0)
                worksheet.SetTabColor(XLColor.FromArgb(255, 216, 202));
            if (iColor == 1)
                worksheet.SetTabColor(XLColor.FromArgb(254, 255, 199));
            if (iColor == 2)
                worksheet.SetTabColor(XLColor.FromArgb(181, 196, 255, 209));

            for (int r = 0; r < sTitle.Length; r++)
            {
                worksheet.Cell(2 + r, 2).Style.Fill.BackgroundColor = XLColor.FromArgb(188, 218, 255);
                worksheet.Cell(2 + r, 2).Value = HttpUtility.HtmlDecode(sTitle[r]);
                worksheet.Cell(2 + r, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell(2 + r, 3).Value = HttpUtility.HtmlDecode(sData[r]);
            }

            int iRow = 3 + sTitle.Length;
            int iColumnCount = gv.Columns.Count;

            int iC = 0;
            for (int c = 0; c < iColumnCount; c++)
            {
                worksheet.Cell(iRow, iC + 2).Value = gv.Columns[c].HeaderText;
                iC++;
                if (!gv.Columns[c].Visible)
                    iC--;
            }

            for (int i = 0; i < gv.Rows.Count; i++)
            {
                worksheet.Cell(i + 1 + iRow, 1).Value = (i + 1);

                if (gv.Rows[i].CssClass == "alert alert-warning")
                {
                    worksheet.Row(i + 1 + iRow).Cells(2, iColumnCount + 1).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 243, 205);
                    worksheet.Row(i + 1 + iRow).Cells(2, iColumnCount + 1).Style.Font.FontColor = XLColor.FromArgb(133, 100, 4);
                }

                iC = 0;
                //LAS DEMÁS CELDAS SON PURO TEXTO
                for (int c = 0; c < iColumnCount; c++)
                {
                    TableCell cell = gv.Rows[i].Cells[c];
                    if (cell.Text == "&nbsp;")
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(230, 230, 230);
                    else
                        worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(cell.Text);

                    if (cell.CssClass == "alert alert-danger")
                    {
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 215, 218);
                        worksheet.Cell(i + 1 + iRow, iC + 2).Style.Font.FontColor = XLColor.FromArgb(114, 28, 36);
                    }
                    iC++;
                    if (!gv.Columns[c].Visible)
                        iC--;
                }
            }

            worksheet.CellsUsed().Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            worksheet.Column(1).Cells().Style.Fill.BackgroundColor = XLColor.FromArgb(188, 218, 255);
            worksheet.ColumnsUsed().AdjustToContents();

            IXLRange rangeTable = worksheet.Range(iRow, 2, worksheet.LastCellUsed().Address.RowNumber, worksheet.LastCellUsed().Address.ColumnNumber);
            IXLTable table = rangeTable.CreateTable();
        }

        public static void ExportarTablaClosedXml(Page page, string nombreArchivo, XLWorkbook workbook)
        {
            //XLWorkbook workbook = new XLWorkbook();

            //CrearHojaExcel(ref workbook, nombreHoja, gv, sData);

            MemoryStream Stream = GetStream(workbook);

            page.Response.Clear();
            page.Response.Buffer = true;
            page.Response.AddHeader("content-disposition", "attachment; filename=" + nombreArchivo + ".xlsx");
            page.Response.ContentType = "application/vnd.ms-excel";
            page.Response.ContentEncoding = System.Text.Encoding.Unicode;
            page.Response.BinaryWrite(Stream.ToArray());
            page.Response.End();
            //workbook.SaveAs(@"c:\RCD\HelloWorld.xlsx");
        }

        public static MemoryStream GetStream(XLWorkbook workbook)
        {
            MemoryStream fs = new MemoryStream();
            workbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        #region JSON

        /// <summary>
        /// Obtiene el Json en formato String a través de una url hacia la Web Api
        /// </summary>
        /// <param name="sUrl"></param>
        /// <returns></returns>
        public static string ObtenerJsonWebApi(string sUrl, string sToken)
        {
            try
            {
                WebClient WebClient = new WebClient
                {
                    Encoding = Encoding.UTF8
                };
                WebClient.Headers.Add("Authorization", sToken);
                WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                return WebClient.DownloadString(sUrl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        public static string ObtenerJsonWebApi(string sUrl)
        {
            try
            {
                WebClient WebClient = new WebClient
                {
                    Encoding = Encoding.UTF8
                };
                WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                return WebClient.DownloadString(sUrl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        /// <summary>
        /// Obtiene los ajustes generales del Json
        /// </summary>
        /// <returns></returns>
        public static JsonSerializerSettings ObtenerAjustesJson()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            return settings;
        }

        public static string ObtenerPOSTJsonWebApi(string sUrl, NameValueCollection data)
        {
            WebClient webClient = new WebClient
            {
                Encoding = Encoding.UTF8
            };
            webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
            return Encoding.ASCII.GetString(webClient.UploadValues(sUrl, data));
        }

        public static Dictionary<string, object> ObtenerJsonFile(string path)
        {
            using (StreamReader r = new StreamReader(path, Encoding.GetEncoding("iso-8859-15"), true))
            {
                string stringFile = r.ReadToEnd();
                Dictionary<string, object> json = JsonConvert.DeserializeObject<Dictionary<string, object>>(stringFile);
                return json;
            }
        }
        
        public static T ObtenerJsonFile<T>(string path)
        {
            using (StreamReader r = new StreamReader(path, Encoding.UTF8, true))
            {
                string stringFile = r.ReadToEnd();
                T json = JsonConvert.DeserializeObject<T>(stringFile);
                return json;
            }
        }

        #endregion

        #region AJUSTES DEL SISTEMA

        /// <summary>
        /// Se establece una Cookie en el envio del Servidor al Cliente
        /// </summary>
        /// <param name="sNombreCookie"></param>
        /// <returns></returns>
        public static void SetCookie(HttpContext page, string sNombreCookie,
                    string key, string value, int duracion = 12)
        {
            HttpCookie cookie = new HttpCookie(sNombreCookie);
            cookie.Values.Add(key, value);
            cookie.Expires = DateTime.Now.AddHours(duracion);
            page.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Se obtiene el valor de la cookie desde el cliente
        /// </summary>
        /// <param name="page"></param>
        /// <param name="sNombreCookie"></param>
        /// <param name="sKey"></param>
        /// <returns></returns>
        public static String GetCookie(HttpContext page, string sNombreCookie, string sKey)
        {
            HttpCookie cookie = page.Request.Cookies.Get(sNombreCookie);
            if (cookie == null)
                return "";

            return cookie.Values[sKey].ToString();
        }

        /// <summary>
        /// Se eliminan todas las cookies contenidas
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void RemoveAllCookies(HttpContext page)
        {
            //CODIGO_PERFIL
            //TOKEN
            //PERFIL_USUARIO
            if (page.Request.Cookies.Get("DATA-TOKEN") != null)
            {
                page.Response.Cookies["DATA-CODIGO_PERFIL"].Expires = DateTime.Now.AddDays(-1);
                page.Response.Cookies["DATA-TOKEN"].Expires = DateTime.Now.AddDays(-1);
                page.Response.Cookies["DATA-PERFIL_USUARIO"].Expires = DateTime.Now.AddDays(-1);
            }
        }

        /// <summary>
        /// SE OBTIENEN LAS PROPIEDADES DEL WEB.CONFIG
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Configuration GetConfiguracion(ref HttpContext context)
        {
            return WebConfigurationManager.OpenWebConfiguration(context.Request.ApplicationPath.TrimEnd('/') + "/");
        }

        /// <summary>
        /// SE SETEA LA PROPIEDAD EN EL AppSettings.config
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static void SetPropiedad(ref HttpContext context, string sKey, string sValue)
        {
            var config = GetConfiguracion(ref context);
            config.AppSettings.Settings[sKey].Value = sValue;
            config.Save(ConfigurationSaveMode.Modified);
        }

        /// <summary>
        /// SE OBTIENE LA PROPIEDAD ESPECIFICADA
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static object GetPropiedad(ref HttpContext context, string sKey)
        {
            return GetConfiguracion(ref context).AppSettings.Settings[sKey].Value;
        }

        public static string GetUrlAbsolutataHost(Page page)
        {
            return page.Request.Url.Scheme + "://" + page.Request.Url.Authority +
                page.Request.ApplicationPath.TrimEnd('/') + "/";
        }

        // VALIDA SI LA FECHA TIENE UN FORMATO CORRECTO
        public static bool IsDate(string date)
        {

            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Calcula la edad según el rango de la fecha de nacimiento y fecha límite
        /// </summary>
        /// <param name="fechaNacimiento">Se especifica la fecha de nacimiento con formato yyyy-MM-dd</param>
        /// <param name="fechaLimite">Se especifica la fecha límite que se quiere calcular</param>
        /// <returns>Un JSON con los días, meses y años</returns>
        public static string CalculaEdad(string fechaNacimiento, DateTime fechaLimite)
        {
            if (IsDate(fechaNacimiento) && !string.IsNullOrEmpty(fechaNacimiento))
            {
                // Se establece los años de vida
                int años = CalculaEdadenAños(fechaNacimiento, fechaLimite);
                // Se establecen los meses de vida
                int meses = CalculaEdadMeses(fechaNacimiento, fechaLimite);
                // Se establecen los días de vida
                long dias = CalculaEdadDias(fechaNacimiento, meses, años, fechaLimite);

                string salida = "";

                if (años > 0)
                    salida += (años == 1) ? años.ToString() + " año " : años.ToString() + " años ";

                if (meses > 0)
                    salida += (meses == 1) ? meses.ToString() + " mes " : meses.ToString() + " meses ";

                if (dias > 0)
                    salida += (dias == 1) ? dias.ToString() + " día" : dias.ToString() + " días";
                else if (años == 0 && meses == 0 && dias == 0)
                    salida = "Recién Nacido";

                Dictionary<string, object> json = new Dictionary<string, object>
                {
                    { "años", años },
                    { "meses", meses },
                    { "dias", dias },
                    { "edad", salida }
                };

                return JsonConvert.SerializeObject(json);
            }
            return null;
        }

        /// <summary>
        /// Calcula la edad actual según la fecha de nacimiento
        /// </summary>
        /// <param name="fechaNacimiento">Se especifica la fecha de nacimiento con formato yyyy-MM-dd</param>
        /// <returns>Un JSON con los días, meses y años</returns>
        public static string CalculaEdad(string fechaNacimiento)
        {
            return CalculaEdad(fechaNacimiento, DateTime.Now);
        }

        // Obtener Edad en años
        public static int CalculaEdadenAños(string fec_nac, DateTime fechaLimite)
        {
            DateTime fechaNacimiento = DateTime.Parse(fec_nac);

            // Se calculan los años
            int anio = fechaLimite.Year - fechaNacimiento.Year;

            // Se comprueba que el mes actual sea menor al mes de nacimiento, esto para que se mantenga o se reste un año
            if (fechaLimite.Month < fechaNacimiento.Month)
                anio--;

            // Finalmente se comprueba si el mes es igual al actual y si el dia actual es menor al de nacimiento, de ser asi se resta uno a año
            if (fechaLimite.Month == fechaNacimiento.Month & fechaLimite.Day < fechaNacimiento.Day)
                anio--;

            return anio;
        }

        // Obtener la edad en meses
        public static int CalculaEdadMeses(string fec_nac, DateTime fechaLimite)
        {
            DateTime fechaNacimiento = DateTime.Parse(fec_nac);

            int meses = fechaLimite.Month - fechaNacimiento.Month;

            if (fechaLimite.Day < fechaNacimiento.Day)
                meses--;

            if (meses < 0)
                meses += 12;

            return meses;
        }

        // Obtiene los días vividos
        public static int CalculaEdadDias(string fec_nac, int meses, int años, DateTime fechaLimite)
        {

            DateTime fechaNacimiento = DateTime.Parse(fec_nac);
            fechaNacimiento = fechaNacimiento.AddMonths(meses);
            fechaNacimiento = fechaNacimiento.AddYears(años);
            return fechaLimite.Subtract(fechaNacimiento).Days;

        }

        #endregion

        #region AESENC

        /*
            * Métodos que encriptan usando AES+Llave de encriptación
            * Se usa pasa pasar por url el login y password del usuario desde
            * un sistema al otro
            * 
            * USAR AESEnc y AESDec
        */

        public static string AESEnc(string toEnc)
        {
            string message = toEnc;
            //Clave proporcionada por Eduardo Garay
            string password = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6";

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(password));

            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            string encrypted = EncryptString(message, key, iv);
            return encrypted;
        }
        public static string AESDec(string encrypted)
        {
            //Clave proporcionada por Eduardo Garay
            string password = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6";

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(password));

            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            string decrypted = DecryptString(encrypted, key, iv);
            return decrypted;
        }

        private static string EncryptString(string plainText, byte[] key, byte[] iv)
        {
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;
            encryptor.Key = key;
            encryptor.IV = iv;

            MemoryStream memoryStream = new MemoryStream();

            ICryptoTransform aesEncryptor = encryptor.CreateEncryptor();

            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write);

            byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

            cryptoStream.Write(plainBytes, 0, plainBytes.Length);
            cryptoStream.FlushFinalBlock();

            byte[] cipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);
            return cipherText;
        }

        private static string DecryptString(string cipherText, byte[] key, byte[] iv)
        {
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;

            encryptor.Key = key;
            encryptor.IV = iv;

            MemoryStream memoryStream = new MemoryStream();

            ICryptoTransform aesDecryptor = encryptor.CreateDecryptor();

            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write);

            string plainText = String.Empty;

            try
            {
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);
                cryptoStream.FlushFinalBlock();

                byte[] plainBytes = memoryStream.ToArray();
                plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
            }
            finally
            {
                memoryStream.Close();
                cryptoStream.Close();
            }

            return plainText;
        }

        #endregion

        #region "PDF" 
        //public static void ExportarPDF(string baseURL, Page page, string nombreArchivo, string sLogin, bool llevaElPie = true)
        //{
        //    try
        //    {
        //        PdfPageSize pdf_page_size = PdfPageSize.Letter;
        //        HtmlToPdf converter = new SelectPdf.HtmlToPdf();

        //        StringWriter sw = new StringWriter();
        //        HtmlTextWriter hw = new HtmlTextWriter(sw);
        //        page.RenderControl(hw);
        //        string htmlString = sw.ToString();

        //        converter.Options.PdfPageSize = pdf_page_size;
        //        converter.Options.MarginBottom = 4;
        //        converter.Options.MarginLeft = 14;
        //        converter.Options.MarginRight = 14;
        //        converter.Options.MarginTop = 14;

        //        if (llevaElPie == true)
        //        {
        //            StringBuilder sb = new StringBuilder();
        //            sb.Append("<hr style='height:2px;border:none;color:#333;background-color:#333;'/>");
        //            sb.Append("<center>Hospital Clínico Magallanes 'Dr.Lautaro Navarro Avaria'</center>");
        //            PdfHtmlSection footerHtml = new PdfHtmlSection(sb.ToString(), baseURL);
        //            converter.Footer.Add(footerHtml);

        //            sb = new StringBuilder();
        //            sb.Append("Usuario que imprime:");
        //            sb.AppendLine(sLogin);
        //            sb.Append(DateTime.Now);

        //            PdfTextSection textLeft = new PdfTextSection(0, 10,
        //                sb.ToString(),
        //                new System.Drawing.Font("Calibri", 10))
        //            {
        //                HorizontalAlign = PdfTextHorizontalAlign.Left
        //            };
        //            converter.Footer.Add(textLeft);

        //            sb = new StringBuilder();
        //            sb.AppendLine("Página: {page_number} de {total_pages}  ");
        //            //sb.Append(string.Join(" ", new string[] { "Hora IPD:", sFechaIPD }));
        //            PdfTextSection textRight = new PdfTextSection(0, 10,
        //                sb.ToString(),
        //                new System.Drawing.Font("Calibri", 10))
        //            {
        //                HorizontalAlign = PdfTextHorizontalAlign.Right
        //            };
        //            converter.Footer.Add(textRight);
        //            converter.Options.DisplayFooter = llevaElPie;
        //        }

        //        PdfDocument doc = new PdfDocument();
        //        doc = converter.ConvertHtmlString(htmlString, baseURL);
        //        MemoryStream ms = new MemoryStream();
        //        doc.Save(ms);
        //        doc.Close();

        //        page.Response.ContentType = "application/pdf";
        //        page.Response.BufferOutput = true;
        //        page.Response.AddHeader("Content-Disposition", "inline;filename=" + nombreArchivo + ".pdf");
        //        page.Response.BinaryWrite(ms.ToArray());
        //        page.Response.Flush();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message + ex.StackTrace);
        //    }

        //}

        public static void ExportarPDFAlterno(string vistaImpresion, Page page, string nombreArchivo, string usuario, bool llevaElPie = true)
        {
            try
            {

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                page.RenderControl(hw);
                string htmlString = sw.ToString();

                byte[] a = null;

                page.Response.ContentType = "application/pdf";
                page.Response.BufferOutput = true;
                page.Response.AddHeader("Content-Disposition", "inline;filename=" + nombreArchivo + ".pdf");
                page.Response.BinaryWrite(a);
                page.Response.Flush();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }



        #endregion

        public static bool IsRunLocal(HttpContext context)
        {
            string sHost = context.Request.Url.GetLeftPart(UriPartial.Authority);
            return (sHost.Contains("localhost"));
        }

        public static string GetHost(HttpContext context)
        {
            string sHost = context.Request.Url.GetLeftPart(UriPartial.Authority);
            return (IsRunLocal(context)) ? sHost : string.Format("{0}/RCE", sHost);
        }



        //https://stackoverflow.com/questions/31795456/using-httpwebrequest-and-httpwebresponse-throws-an-error-however-only-on-certai
        public static bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                request.Timeout = 5000;
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
                return (false);
            }
        }

        public static void SetCookie(HttpContext context, string key, string value)
        {

            if (context.Request.Cookies.Get("DATA-" + key) == null)
            {
                HttpCookie myHttpOnlyCookie = new HttpCookie(key, HttpUtility.UrlEncode(value))
                {
                    HttpOnly = true,
                    Name = "DATA-" + key,
                    Expires = DateTime.Now.AddHours(1)
                };
                context.Response.AppendCookie(myHttpOnlyCookie);
            }
            else
            {
                HttpCookie cookie = context.Request.Cookies["DATA-" + key];
                cookie.Expires = DateTime.Now.AddHours(1);
                context.Response.Cookies.Set(cookie);
            }
        }

        public string baseIgualmenteEspaciadaTablas()
        {
            string fila = "<tr class='base'><td></td><td></td><td></td>" +
            "<td></td><td></td><td></td><td></td><td></td><td></td><td>" +
            "</td><td></td><td></td></tr>";
            return fila;
        }
    }
}