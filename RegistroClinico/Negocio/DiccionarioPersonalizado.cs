﻿using System.Collections.Generic;

namespace RegistroClinico.Negocio
{
    public class DiccionarioPersonalizado<TKey, TValue> : Dictionary<string, TValue>
    {
        //Tratar de hacer con el Valor Generico
        public string obtenerValor(string llave)
        {
            TValue value;
            string response = "";
            if (this.TryGetValue(llave, out value))
            {
                if (value != null)
                {
                    if (value.ToString() != "")
                    {
                        response = value.ToString();
                    }
                }
            }
            else
            {
                response = " x ";
            }
            return response;
        }
    }
}