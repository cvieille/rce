﻿let idDiv;
let sSession = null;


function CargarComboGrupoPatologiaGES() {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAuge"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlSubGrupo"));
}

$('#ddlAuge').change(function () {
    idPadre = $('#ddlAuge').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#ddlSubGrupo");
    if (select.options.length == 1) { //No tiene valtblSores 
        $('#divSubGrupo').hide()
    } else {
        $('#divSubGrupo').show()
    }
});
$(document).ready(function () {
    sSession = getSession();
    //RevisarAcceso(false, sSession);
    DarFuncionRadios();

    comboEstadoIPD();
    CargarComboGrupoPatologiaGES();
    $('#divSubGrupo').hide()
    //comboGrupo('#ddlAuge', 0);
    //comboGarantia();

    idDiv = "";

    comboTipoRepresentante();
    comboIdentificacion($("#sltTipoIdentificacion"))
    getTablaIPD(sSession);


    $('#txtFiltroRut').on('keypress', function (e) {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4) {
            var charCode = (e.which) ? e.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    });

    $('#txtFiltroRut').on('blur', async function () {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4)
            $('#txtSTFiltroDV').val(await ObtenerVerificador($(this).val()));
    });

    ocultarMostrarFichaPac($("#sltTipoIdentificacion"), $("#txtFiltroRut"))

    if (sSession.CODIGO_PERFIL == 2 || sSession.CODIGO_PERFIL == 8 || sSession.CODIGO_PERFIL == 9) {
        $('#lnbNuevoIPD').hide();
        $('#divProfesional').show();
        comboProfesional();
    }
    else {
        $('#divProblemaSalud').addClass('offset-1');
    }

    // Evento del boton "Nueva Excepcion" que me lleva al div oculto que me permite agregar/editar excepciones
    $('#bntNuevaExcepcion').on('click', function (e) {
        e.preventDefault();

        // Ocultar el div visible
        $('#divExcepcionVisible').hide();

        // Mostrar el div oculto
        $('#divExcepcionOculto').show();
    })

    // Evento del botón para crear o editar excepción
    $('#btnExcepcion').on('click', function (e) {
        e.preventDefault();

        // Valido manualmente los campos requeridos
        if (validarCampos()) {
            let idExc = $('#idExcepcion').val();

            if (idExc === '0') {
                // Crear nueva excepción
                crearExcepcion();
            } else {
                // Editar excepción existente
                editarExcepcion();
            }
            // Ocultar el div despues de agregar o editar
            $('#divExcepcionOculto').hide();

            // Mostrar div visible
            $('#divExcepcionVisible').show();
        } else {
            toastr.error('Debe completar todos los campos requeridos (*)');
        }
    });

    // Evento que se dispara después de que el modal se ha ocultado completamente
    $('#mdlExcepcion').on('hidden.bs.modal', function () {
        // Mostrar el div visible
        $('#divExcepcionVisible').show();

        // Ocultar el div oculto
        $('#divExcepcionOculto').hide();
    });

    $('#switchSeguimiento').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $('#divSeguimiento').stop().fadeIn(100);
            // Aquí se le agrega un mes hacia adelante como predeterminado para le fecha del seguimiento
            $('#txtSeguimientoFecha').val(moment().add(1, 'M').format("YYYY-MM-DD"));
        } else {
            $('#divSeguimiento').stop().fadeOut(100);
        }
    });

    $('#btnFiltro').on('click', function (e) {
        getTablaIPD(sSession)
        e.preventDefault();
    });
    $('#btnLimpiarFiltro').on('click', function (e) {
        limpiarFiltros();
        getTablaIPD(sSession)
        e.preventDefault();
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3000,
        "extendedTimeOut": 1500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    $('.enterForm').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#btnFiltro').click();
            e.preventDefault();
        }
    });

    $('.enterExcepcion').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#btnExcepcion').click();
            e.preventDefault();
        }
    });

    //deleteSession('ID_IPD');
});

// Evento del boton volver de la creacion de la neuva excepcion
$('#btnVolver').on('click', function (e) {
    e.preventDefault();

    // Ocultar el div excepciones
    $('#divExcepcionOculto').hide();

    // Mostrar el div visible
    $('#divExcepcionVisible').show();

    // Volver a la grilla
    limpiarCamposExcepcion();

    // Restaurar el texto del btnExcepcion
    $('#btnExcepcion').text('Agregar Excepción');
})

// Funcion para limpiar valores de los campos de la excepcion
function limpiarCamposExcepcion() {
    $('#ddlGarantia').val('');
    $('#switchSeguimiento').bootstrapSwitch('state', false);
    $('#txtSeguimientoFecha').val('');
    $('#txtObservacion').val('');
}

function recargarBandeja() {
    getTablaIPD(sSession)
}
function limpiarFiltros() {

    $('#txtFiltroID').val('');
    $('#txtFiltroRut').val('');
    $('#txtSTFiltroDV').val('');
    $('#txtFiltroNombre').val('');
    $('#txtFiltroApe').val('');
    $('#txtFiltroSApe').val('');

    $('#ddlMedicoIPD').empty().append("<option value='0'>-Seleccione-</option>").val('0');
    $('#ddlAuge').val('0');
    $("#ddlSubGrupo").empty().append("<option value='0'>-Seleccione-</option>").val('0');
    $("#ddlEstadoIPD").val('0');

    comboProfesional();
}

function comboProfesional() {
    $('#ddlMedicoIPD').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico/Medicos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlMedicoIPD').append("<option value='" + r.GEN_idProfesional + "'>" + r.GEN_nombrePersonas + "</option>");
            });
            //$('#ddlMedicoIPD').selectpicker('refresh');
        }
    });
}

function comboEstadoIPD() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/IPD/combo`;
    setCargarDataEnCombo(url, false, $("#ddlEstadoIPD"))
}

function comboGarantia() {
    $('#ddlGarantia').empty();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'RCE_Garantias_GES/Combo',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#ddlGarantia').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (i, r) {
                $('#ddlGarantia').append("<option value='" + r.RCE_idGarantias_GES + "'>" + r.RCE_descripcionGarantias_GES + "</option>");
            });
        }
    });
}

function comboGrupo(elem, id) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo?RCE_idPatologia_GESPadre=${id}`;
    setCargarDataEnCombo(url, false, elem);
}
function comboTipoRepresentante() {
    ddlConstanciaRepresentante
    let url = `${GetWebApiUrl()}GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlConstanciaRepresentante"))
}

// ESTOY EN ESTA FUNCION PARA INTENTAR CONTROLAR LA CREACION Y LA EDICION CON LOS PERFILES 150 Y 156
function LinkVerEditarIPD(e, accion) {
    let id = $(e).data("id");
    setSession('ID_IPD', id);

    let url = ObtenerHost() + "/Vista/ModuloIPD/NuevoIPD.aspx";
    window.location.replace(url);
}

function linkVerConstancia(e) {
    $('#switchTitular').bootstrapSwitch('disabled', false);
    var id = $(e).data("id");


    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_IPD/${id}`, // Trae la data del IPD
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (dataConst) {
            console.log(dataConst)
            const { Profesional } = dataConst

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Profesional/${Profesional.Id}`, // Trae la data del profesional
                contentType: "application/json",
                dataType: "json",
                success: function (dataPro) {

                    if (dataPro !== undefined) {
                        // Deshabilitar los campos del PROFESIONAL modal
                        $('#txtConstanciaPro').prop('disabled', true);
                        $('#txtConstanciaProRut').prop('disabled', true);

                        // Informacion del profesional
                        $('#txtConstanciaPro').val(dataPro.Nombre + ' ' + dataPro.ApellidoPaterno + ' ' + dataPro.ApellidoMaterno);
                        $('#txtConstanciaProRut').val(dataPro.NumeroDocumento + '-' + dataPro.Digito);
                    }
                    else {
                        $('#txtConstanciaPro').val("");
                        $('#txtConstanciaProRut').val("");
                    }

                    // Obtener detalles de la especialidad a través de la tabla intermedia
                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Pro_Especialidad/GEN_Profesional/${Profesional.Id}`,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (proEspecialidadData) {
                            // Verificar si hay información de especialidad
                            if (proEspecialidadData && proEspecialidadData.length > 0) {
                                // Mapear especialidades a sus valores
                                const valoresEspecialidades = proEspecialidadData.map(especialidad => especialidad.Valor);

                                // Asignar los valores de las especialidades al campo de texto
                                $('#txtEspecialidadPro').val(valoresEspecialidades.join(', '));
                            } else {
                                // En caso de que no haya especialidades dejando el campo vacio
                                $('#txtEspecialidadPro').val('');
                            }
                        },
                        error: function (jqXHR, status) {
                            console.log(`Error ${JSON.stringify(jqXHR)}`);
                        }
                    });
                }
            });

            let constanciaId = dataConst.ConstanciaGES && dataConst.ConstanciaGES.Id;
            let representante = (dataConst.ConstanciaGES && dataConst.ConstanciaGES.Representante) || undefined;
            let fecha = (dataConst.ConstanciaGES && dataConst.ConstanciaGES.Fecha) ? moment(dataConst.ConstanciaGES.Fecha).format("YYYY-MM-DD") : undefined;
            let hora = (dataConst.ConstanciaGES && dataConst.ConstanciaGES.Fecha) ? moment(dataConst.ConstanciaGES.Fecha).subtract(3, 'hours').format("HH:mm:ss") : undefined;

            // Verificar si constanciaId es un valor válido y no es igual a 0
            if (constanciaId !== null && constanciaId !== undefined) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}RCE_Constancia_GES/${constanciaId}`, // Trae la data de la constancia
                    contentType: "json",
                    success: function (constanciaData) {
                        if (constanciaData !== undefined) {
                            // Deshabilitar los campos del REPRESENTANTE modal
                            $('#ddlConstanciaRepresentante').prop('disabled', true);
                            $('#txtConstanciaRepresentante').prop('disabled', true);
                            $('#txtConstanciaFecha').prop('disabled', true);
                            $('#txtConstanciaHora').prop('disabled', true);

                            // Mostrar el modal con la información de la constancia
                            $('#ddlConstanciaRepresentante').val(dataConst.ConstanciaGES.TipoRepresentante.IdTipoRepresentante);

                            if (representante) {
                                $('#txtConstanciaRepresentante').val(representante.Nombre + ' ' + representante.ApellidoPaterno + ' ' + representante.ApellidoMaterno);
                            }

                            if (fecha !== undefined) {
                                $('#txtConstanciaFecha').val(fecha);
                            }

                            if (hora !== undefined) {
                                $('#txtConstanciaHora').val(hora);
                            }

                            $('#switchTitular').bootstrapSwitch('disabled', true);
                            mostrarModal("mdlConstancia");
                        }
                    }
                });
            } else {

                // Mostrar mensaje Toastr indicando que no hay constancia
                toastr.warning('No hay constancia asociada a este IPD.');
            }
        }
    });
}

function LinkImprimirIPD(e) {

    const id = $(e).data("id");
    mostrarModal("modalCargando");
    mostrarModal("modalCargando");
    $('#frameIPD').attr('src', 'ImprimirIPD.aspx?id=' + id);
    mostrarModal("mdlImprimir");
}

function grillaExcepcion(id) {
    let adataset = [];

    // Obtener datos mediante AJAX
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'RCE_IPD_Excepcion/RCE_idIPD/' + id,
        contentType: "application/json",
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                let sDate = '';

                if (r.RCE_fechaSeguimientoIPD_Excepcion != null) {
                    sDate = moment(moment(r.RCE_fechaSeguimientoIPD_Excepcion).toDate()).format('DD-MM-YYYY');
                }

                adataset.push([
                    r.RCE_idIPD_Excepcion,
                    moment(moment(r.RCE_fechaIPD_Excepcion).toDate()).format('DD-MM-YYYY hh:mm:ss'),
                    sDate,
                    r.HOS_dias_vencimientoIPD_Excepcion,
                    r.RCE_descripcionGarantias_GES,
                    SaltoDeLinea(r.RCE_observacionIPD_Excepcion, 5)
                ]);
            });
        }
    });

    // Obtener el contenedor de la tabla
    let tableContainer = $('#tblExcepcion');

    // Establecer dimensiones fijas o porcentajes
    tableContainer.css({
        'width': '100%',
        'height': '90%'
    });

    // Destruir DataTable si existe una instancia previa
    if ($.fn.DataTable.isDataTable('#tblExcepcion')) {
        $('#tblExcepcion').DataTable().destroy();
        $('#tblExcepcion').empty(); // Limpiar el contenido del contenedor
    }

    // Inicializar DataTables
    let table = $('#tblExcepcion').DataTable({
        responsive: true,
        data: adataset,
        paging: true,
        pageLength: 3,
        order: [[1, "desc"]],
        columnDefs: [
            { targets: 0, visible: false },
            { targets: [1], type: "date-ukLong" },
            { targets: [2], type: "date-ukShort" },
            {
                targets: -1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let observacion = adataset[fila][5];
                    let botones = "<a id='linkVerEditar' data-id='" + adataset[fila][0] + "' data-fecha='" + adataset[fila][1] + "' data-observacion='" + observacion + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarExc(this)'><i class='fa fa-edit'></i> Editar</a>";

                    return botones;
                }
            },
        ],
        columns: [
            { title: "idExcp" },
            { title: "Fecha Excepción" },
            { title: "Fecha de Seguimiento" },
            { title: "Días de vencimiento" },
            { title: "Garantía" },
            { title: "Observación" },
            { title: "" }
        ],
    });

    // Destruir y recrear DataTables en el evento shown.bs.modal
    $('#mdlExcepcion').on('shown.bs.modal', function () {
        table.destroy();
        table = $('#tblExcepcion').DataTable({
            responsive: true,
            data: adataset,
            paging: true,
            pageLength: 3,
            order: [[1, "desc"]],
            columnDefs: [
                { targets: 0, visible: false },
                { targets: [1], type: "date-ukLong" },
                { targets: [2], type: "date-ukShort" },
                {
                    targets: -1,
                    data: null,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        let observacion = adataset[fila][5];
                        let botones = "<a id='linkVerEditar' data-id='" + adataset[fila][0] + "' data-fecha='" + adataset[fila][1] + "' data-observacion='" + observacion + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarExc(this)'><i class='fa fa-edit'></i> Editar</a>";

                        return botones;
                    }
                },
            ],
            columns: [
                { title: "idExcp" },
                { title: "Fecha Excepción" },
                { title: "Fecha de Seguimiento" },
                { title: "Días de vencimiento" },
                { title: "Garantía" },
                { title: "Observación" },
                { title: "" }
            ],
        });

        $(window).on('resize', function () {
            table.responsive.recalc();
        });

        // Eliminar el evento 'shown.bs.modal' después de la inicialización de DataTables
        $('#mdlExcepcion').off('shown.bs.modal');
    });

    // Abrir el modal
    $('#mdlExcepcion').modal('show');
}

function obtenerExcepcion(id, successCallback, errorCallback) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_IPD_Excepcion/${id}`,
        contentType: "application/json",
        dataType: "json",
        success: successCallback,
        error: errorCallback
    });
}

function limpiarTexto(texto) {
    // Eliminar saltos de línea y caracteres de retorno
    let textoLimpio = texto.replace(/(\r\n|\n|\r)/gm, ' ');
    // Eliminar etiquetas <br>
    textoLimpio = textoLimpio.replace(/<br\s*\/?>/gi, '');
    // Eliminar espacios en blanco duplicados
    textoLimpio = textoLimpio.replace(/\s+/g, ' ');
    // Eliminar espacios en blanco al inicio y al final
    textoLimpio = textoLimpio.trim();
    return textoLimpio;
}
function actualizarInterfazUsuario(data, observacion) {
    $('#idExcepcion').val(data.Id);
    $('#fechaExcepcion').val(moment(data.Fecha).format('DD/MM/YYYY HH:mm:ss'));
    $('#ddlGarantia').val(data.GarantiasGes.Id);

    if (data.FechaSeguimiento == null) {
        $('#switchSeguimiento').bootstrapSwitch('state', false);
        $('#divSeguimiento').hide();
    } else {
        $('#switchSeguimiento').bootstrapSwitch('state', true);
        $('#divSeguimiento').show();
        $('#txtSeguimientoFecha').val(moment(new Date(data.FechaSeguimiento)).format('YYYY-MM-DD'));
    }

    $('#btnExcepcion').text('Editar Excepción');
    // Mostrar el modal de edición
    $('#mdlExcepcion').modal('show');

    // Llamo a la funcion limpiar texto para limpiar espacios inncesarios y etiquetas html
    observacion = limpiarTexto(observacion);
    $('#txtObservacion').val(observacion).text;
}

function linkEditarExc(e) {
    let id = $(e).data("id");
    let observacion = $(e).data('observacion');

    obtenerExcepcion(
        id,
        function (data) {
            // Ocultar el div visible
            $('#divExcepcionVisible').hide();

            // Mostrar el div oculto
            $('#divExcepcionOculto').show();

            // Actualizar UX con los datos obtenidos
            actualizarInterfazUsuario(data, observacion);
        },
        function (jqXHR, textStatus, errorThrown) {
            console.log("Error de solicitud AJAX:", textStatus, errorThrown);
        }
    );

    return false;
}

// Funcion para crear nueva excepcion
function crearExcepcion() {
    let id = $('#idIPD').val();
    let fecha = moment($('#fechaExcepcion').val(), 'DD/MM/YYYY').format('YYYY-MM-DD HH:mm:ss');

    let json = {
        // NUEVO OBJETO JSON
        Fecha: moment().format('YYYY-MM-DD HH:mm:ss'),
        FechaSeguimiento: moment($('#txtSeguimientoFecha').val()).format('YYYY-MM-DD'),
        Observacion: $('#txtObservacion').val(),
        IdGarantiaGes: $('#ddlGarantia').prop('value'),
        IdIPD: id
    };

    if (!$('#switchSeguimiento').bootstrapSwitch('state')) {
        json.FechaSeguimiento = null;
    }

    $.ajax({
        type: 'POST',
        url: GetWebApiUrl() + 'RCE_IPD_Excepcion/',
        data: JSON.stringify(json),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',

        success: function (data, status, jqXHR) {

            limpiarExcepcion();
            grillaExcepcion(id);
            $('#idExcepcion').val('0');
            $('#fechaExcepcion').val('0');
            // Manejar la respuesta
            toastr.success('Excepción creada correctamente');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // Manejar errores de la solicitud AJAX
            console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
            toastr.error('Error al crear la excepción');
        }
    });
}

// Funcion para manejar la actualizacion de la excepcion
function editarExcepcion() {
    let id = $('#idExcepcion').val();

    let json = {
        Id: id,
        Fecha: moment().format('YYYY-MM-DD HH:mm:ss'),
        FechaSeguimiento: moment($('#txtSeguimientoFecha').val()).format('YYYY-MM-DD'),
        Observacion: $('#txtObservacion').val(),
        IdGarantiaGes: $('#ddlGarantia').prop('value')
    };

    if (!$('#switchSeguimiento').bootstrapSwitch('state')) {
        json.FechaSeguimiento = null;
    }

    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}RCE_IPD_Excepcion/${id}`,
        data: JSON.stringify(json),
        contentType: 'application/json; charset=utf-8',
        success: function (data, status, jqXHR) {
            // Actualiza la tabla de excepciones después de editar
            let idIPD = $('#idIPD').val();
            grillaExcepcion(idIPD);

            limpiarExcepcion();
            $('#idExcepcion').val('0');
            $('#fechaExcepcion').val('0');
            toastr.success('Excepción actualizada correctamente');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // Manejar errores de la solicitud AJAX
            console.error("Error en la solicitud AJAX:", jqXHR, textStatus, errorThrown);
            toastr.error('Error al actualizar la excepción');
        }
    });
}
function linkVerExcepcion(e) {
    var id = $(e).data("id");

    $('#idIPD').val(id);

    limpiarExcepcion();
    grillaExcepcion(id);

    $('#mdlExcepcion').modal('show');
}

function limpiarExcepcion() {
    comboGarantia();
    $('#divSeguimiento').hide();
    $('#switchSeguimiento').bootstrapSwitch('state', false);
    $('#txtSeguimientoFecha').val(moment().add(1, 'M').format("YYYY-MM-DD"));
    $('#txtObservacion').val('');
    $('#txtObservacion').removeClass('invalid');
    $('#btnExcepcion').text('Agregar Excepción');
}

// Creo funcion para validar los campos requeridos de la excepcion #btnExcepcion es el id relacionado
function validarCampos() {
    let camposValidos = true;

    // Validando campos
    if ($('#ddlGarantia').val() === '') {
        camposValidos = false;
    }
    if ($('#txtObservacion').val() === '') {
        camposValidos = false;
    }

    return camposValidos;
}

function linkVerMovimientosIPD(e) {
    let id = $(e).data("id")
    let titulo = `IPD #${id}`
    let _url = `${GetWebApiUrl()}RCE_IPD/${id}/Movimientos`

    getMovimientosSistemaGenerico(_url, titulo)
}

function linkMovimientos(e) {
    var id = $(e).data("id");
    setSession('ID_IPD', id);
    var adataset = [];
    var id = $(e).data("id");
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}RCE_IPD/${id}/Movimientos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.id,
                    moment(new Date(r.fecha)).format('DD-MM-YYYY hh:mm:ss'),
                    r.usuario,
                    r.descripcion
                ]);
            });
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarMov').addClass('disabled');
    else
        $('#lnkExportarMov').removeClass('disabled');

    $('#tblMovimientos').DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "RCE_idMov_IPD" },
            { title: "Fecha Movimiento" },
            //{ title: "GEN_idUsuarios" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            { "targets": 1, "sType": "date-ukLong" }
        ],
        "bDestroy": true
    });
    $('#mdlMovimiento').modal('show');
}

function validarIPDConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de validar el IPD?');
    $('#linkConfirmar').attr('onclick', 'validarIPD(' + id + ')');
    $('#linkConfirmar').addClass('btn-success');
    $('#mdlAlerta').modal('show');
}
function linkValidarCaso(e) {

    let id = $(e).data("id")
    let url = `${GetWebApiUrl()}RCE_IPD/Validar/${id}`


    $.ajax({
        type: 'PUT',
        url: url,
        async: false,
        success: function (status, jqXHR) {

            getTablaIPD(sSession);
            toastr.success('IPD validado correctamente');
            // Ocultar el tooltip manualmente
            $(e).tooltip("hide");
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL VALIDAR IPD <br/>' + mensaje);
        }
    });
}

function linkDesvalidarCaso(e) {

    let id = $(e).data("id")
    let url = `${GetWebApiUrl()}RCE_IPD/Desvalidar/${id}`

    $.ajax({
        type: 'PUT',
        url: url,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.error('IPD desvalidado correctamente');
            $(e).tooltip("hide");
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL DESVALIDAR IPD <br/>' + mensaje);
        }
    });
}

function cerrarIPDConfirma(id) {
    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de cerrar el IPD?');
    $('#linkConfirmar').attr('onclick', 'cerrarIPD(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');
    $('#mdlAlerta').modal('show');
}
function linkCerrarCaso(e) {

    let id = $(e).data("id")
    let url = `${GetWebApiUrl()}RCE_IPD/Cerrar/${id}`

    $.ajax({
        type: 'PUT',
        url: url,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.error('IPD cerrado correctamente');
            $('#mdlAlerta').modal('hide');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL CERRAR IPD <br/>' + mensaje);
        }
    });
}

function linkAbrirCaso(e) {

    let id = $(e).data("id")
    let url = `${GetWebApiUrl()}RCE_IPD/Reabrir/${id}`
    $.ajax({
        type: 'PUT',
        url: url,
        async: false,
        success: function (status, jqXHR) {
            //var sSession = getSession();
            getTablaIPD(sSession);
            toastr.success('IPD abierto correctamente');
        },
        error: function (jqXHR, status) {
            var mensaje = jqXHR.responseJSON.Message;
            toastr.error('ERROR AL ABRIR IPD <br/>' + mensaje);
        }
    });
}
function anularIPDConfirma(id) {

    // Limpiando el cuerpo del modal
    $('#mdlAlertaCuerpo').empty();
    // Agrego mensje de confirmacion 
    $('#mdlAlertaCuerpo').append('¿Esta seguro de anular el IPD?');
    // Evento de click del boton
    $('#linkConfirmar').off('click').on('click', function () {
        // Llamo a la funcion para anular el IPD
        anularIPD(id);
        // Ocultar modal despues de confiramr
        $('#mdlAlerta').modal('hide');
    });

    // Evento clic del boton cancelar
    $('#linkCancelar').off('click').on('click', function () {
        // Oculta modal de confirmación sin anular
        $('#mdlAlerta').modal('hide');
    });

    // Cambio estilo del boton de confirmacion
    $('#linkConfirmar').removeClass('btn-primary').addClass('btn-danger');
    // Muestra modal de confirmacion
    $('#mdlAlerta').modal('show');
}

function anularIPD(id) {

    let url = `${GetWebApiUrl()}/RCE_IPD/Anular/${id}`;

    $.ajax({
        type: 'DELETE',
        url: url,
        success: function (status, jqXHR) {
            // Actualiza la tabla IPD despues de anular
            getTablaIPD(sSession);
            // Oculta modal de confirmacion
            $('#mdlAlerta').modal('hide');
            // Mensaje de exito
            toastr.error('IPD Anulado');
        },
        error: function (jqXHR, status) {
            // Mensaje de error si falla
            toastr.error('Error al anular IPD');
            console.log(jqXHR.responseText);
        }
    })
}
function linkAnularIPD(e) {

    // Obtengo ID del elemento
    let id = $(e).data("id")

    // Muestra el modal de confirmacion antes de anular
    anularIPDConfirma(id);
}

function toggle(id) {

    if (idDiv == "") {

        $(id).fadeIn();
        idDiv = id;

    } else if (idDiv == id) {

        $(id).fadeOut();
        idDiv = "";

    } else {

        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}

// FUNCIONES SUBIR Y BAJAR CASO YA NO IRAN, MANTENGO POR AHORA COMENTADO EL CODIGO
//function linkSubirCaso(e) {

//    let id = $(e).data("id")
//    let url = `${GetWebApiUrl()}RCE_IPD/Subir/${id}`

//    $.ajax({
//        type: 'PUT',
//        url: url,
//        async: false,
//        success: function (status, jqXHR) {
//            //var sSession = getSession();
//            getTablaIPD(sSession);
//            toastr.success('IPD subido correctamente');
//        },
//        error: function (jqXHR, status) {
//            var mensaje = jqXHR.responseJSON.Message;
//            toastr.error('ERROR AL SUBIR IPD <br/>' + mensaje);
//        }
//    });
//}
//function linkBajarCaso(e) { // REVISAR ACCIONES BANDEJA IPD API

//    let id = $(e).data("id")
//    let url = `${GetWebApiUrl()}RCE_IPD/Bajar/${id}`

//    $.ajax({
//        type: 'PUT',
//        url: url,
//        async: false,
//        success: function (status, jqXHR) {
//            //var sSession = getSession();
//            getTablaIPD(sSession);
//            toastr.warning('IPD bajado correctamente');
//        },
//        error: function (jqXHR, status) {
//            var mensaje = jqXHR.responseJSON.Message;
//            toastr.error('ERROR AL BAJAR IPD <br/>' + mensaje);
//        }
//    });
//}

function LlenaGrillaIPD(adataset, grilla, sSession) {

    //if (datos.length == 0)
    //    $('#lnbExportar').addClass('disabled');
    //else
    //    $('#lnbExportar').removeClass('disabled');


    $(grilla).addClass("tblBandeja").addClass("dataTable").DataTable({
        "order": [[0, "desc"]],
        data: adataset,
        columns: [
            { title: "ID IPD" },
            { title: "Fecha IPD" },
            { title: "N° Dcto." },
            { title: "Paciente" },
            { title: "Patologia" },
            { title: "Tipo IPD" },
            { title: "Estado Sistema" },
            //{ title: "Constancia"},
            { title: "Acciones", className: "text-center" },
        ],
        columnDefs: [
            /*{ "targets": 1, "sType": "date-ukShort" },*/
            { targets: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19], visible: false, searchable: false },
            {
                targets: -1,
                data: null,
                orderable: false,
                searchable: false,
                className: 'text-right w-1',
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones = `
                        <button class='btn btn-primary btn-circle' 
                            onclick='toggle("#div_accionesST${fila}"); return false;'>
                            <i class="fa fa-list" style="font-size:15px;"></i>
                        </button>
                        <div id='div_accionesST${fila}' class='btn-container' style="display: none;">
                            <center>
                                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                <div class="rounded-actions">
                                    <center>
                    `;

                    //EDITAR
                    if (adataset[fila][7]) {
                        botones += `
                        <a
                            data-id='${adataset[fila][0]}'
                            class='btn btn-info btn-circle btn-lg load-click'
                            onclick='LinkVerEditarIPD(this);'
                            data-toggle="tooltip"
                            data-placement="left"
                            title="Editar IPD">
                            <i class="fas fa-edit"></i>   
                        </a>
                        <br>
                        `;
                    }
                    //ANULAR 
                    if (adataset[fila][8]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='anular'
                        class='btn btn-danger btn-circle btn-lg load-click'
                        onclick='linkAnularIPD(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Anular IPD">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    <br>
                    `;
                    }

                    //VER CONSTANCIA 
                    if (adataset[fila][9]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='ver-constancia'
                        class='btn btn-warning btn-circle btn-lg load-click'
                        onclick='linkVerConstancia(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Ver Constancia">
                        <i class="fa fa-clipboard"></i>
                    </a>
                    <br>
                    `;
                    }

                    //VER EXCEPCIÓN 
                    if (adataset[fila][10]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='ver-excepcion'
                        class='btn btn-info btn-circle btn-lg load-click'
                        onclick='linkVerExcepcion(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Ver Excepción">
                        <i class="fa fa-exclamation-triangle"></i>
                    </a>
                    <br>
                    `;
                    }

                    //IMPRIMIR
                    if (adataset[fila][11]) {
                        botones += `
                        <a
                            data-id='${adataset[fila][0]}'
                            class='btn btn-info btn-circle btn-lg load-click'
                            onclick='LinkImprimirIPD(this);'
                            data-toggle="tooltip"
                            data-placement="left"
                            title="Imprimir IPD">
                            <i class="fas fa-print"></i>
                        </a>
                        <br>
                        `;
                    }

                    //VER MOVIMIENTOS 
                    if (adataset[fila][12]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='ver-movimientos'
                        class='btn btn-info btn-circle btn-lg load-click'
                        onclick='linkVerMovimientosIPD(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Ver Movimientos">
                        <i class="fa fa-list"></i>
                    </a>
                    <br>
                    `;
                    }

                    //VALIDAR CASO
                    if (adataset[fila][13]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='validar-caso'
                        class='btn btn-success btn-circle btn-lg load-click'
                        onclick='linkValidarCaso(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Validar caso">
                        <i class="fa fa-calendar-check"></i>
                    </a>
                    <br>
                    `;
                    }

                    //DESVALIDAR CASO 
                    if (adataset[fila][17]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='desvalidar-caso'
                        class='btn btn-danger btn-circle btn-lg load-click'
                        onclick='linkDesvalidarCaso(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Desvalidar caso">
                        <i class="fa fa-calendar-times"></i>
                    </a>
                    <br>
                    `;
                    }

                    //CERRAR CASO 17
                    if (adataset[fila][16]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='cerrar-caso'
                        class='btn btn-danger btn-circle btn-lg load-click'
                        onclick='linkCerrarCaso(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Cerrar caso">
                        <i class="fa fa-lock"></i>
                    </a>
                    <br>
                    `;
                    }

                    //ABRIR CASO 
                    if (adataset[fila][18]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-accion='abrir-caso'
                        class='btn btn-success btn-circle btn-lg load-click'
                        onclick='linkAbrirCaso(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Abrir caso">
                        <i class="fa fa-unlock-alt"></i>
                    </a>
                    <br>
                    `;
                    }
                    // OCULTANDO DE LA BOTONERA SUBIR Y BAJAR CASO
                    ////SUBIR CASO
                    //if (adataset[fila][14]) {
                    //    botones += `
                    //<a
                    //    data-id='${adataset[fila][0]}'
                    //    data-accion='subir-caso'
                    //    class='btn btn-success btn-circle btn-lg load-click'
                    //    onclick='linkSubirCaso(this);'
                    //    data-toggle="tooltip"
                    //    data-placement="left"
                    //    title="Subir caso">
                    //    <i class="fa fa-arrow-up"></i>
                    //</a>
                    //<br>
                    //`;
                    //}

                    ////BAJAR CASO
                    //if (adataset[fila][15]) {
                    //    botones += `
                    //<a
                    //    data-id='${adataset[fila][0]}'
                    //    data-accion='bajar-caso'
                    //    class='btn btn-warning btn-circle btn-lg load-click'
                    //    onclick='linkBajarCaso(this);'
                    //    data-toggle="tooltip"
                    //    data-placement="left"
                    //    title="Bajar caso">
                    //    <i class="fa fa-arrow-down"></i>
                    //</a>
                    //<br>
                    //`;
                    //}
                    return botones;
                }
            },
            {
                visible: true
            }
        ],
        responsive: true,
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function getTablaIPD(sSession) {
    let adataset = [];

    let url = `${GetWebApiUrl()}RCE_IPD/Bandeja?`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            data.forEach(x => adataset.push([
                x.Id,
                moment(moment(x.Fecha).toDate()).format('DD-MM-YYYY'),
                x.Paciente.NumeroDocumento,
                (x.Paciente.Nombre ?? '') + ' ' + (x.Paciente.ApellidoPaterno ?? '') + ' ' + (x.Paciente.ApellidoMaterno ?? ''),
                (x.PatologiaGes.DescripcionPatologiaGES.map(x => {
                    return (`${(x.DescripcionPatologiaGESFinal ?? '-')} <br> ${(x.DescripcionPatologiaGESPadre.length > 0) ? x.DescripcionPatologiaGESPadre : ''}`)
                }).length > 0) ? (x.PatologiaGes.DescripcionPatologiaGES.map(x => {
                    return (`${(x.DescripcionPatologiaGESFinal ?? '-')} <br> ${(x.DescripcionPatologiaGESPadre.length > 0) ? x.DescripcionPatologiaGESPadre : ''}`)
                })) : "Sin información",
                x.TipoIPD.Valor,
                x.TipoEstadoSistema.Valor,
                '<button class="toggle-btn">Mostrar Detalles</button>',
                x.Acciones.Anular,
                x.Acciones.VerConstancia,
                x.Acciones.VerExcepcion,
                x.Acciones.Imprimir,
                x.Acciones.VerMovimientos,
                x.Acciones.ValidarCaso,
                x.Acciones.SubirCaso,
                x.Acciones.BajarCaso,
                x.Acciones.CerrarCaso,
                x.Acciones.DesvalidarCaso,
                x.Acciones.AbrirCaso,
                '',
            ]));

            // Llena la grilla con DataTables
            LlenaGrillaIPD(adataset, '#tblIPD', sSession);
        }
    });
}

function comboIdentificacion(selector) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $(selector).append('<option value="' + r.Id + '">' + r.Valor + '</option>');
            });
            $("#lblTipoIdentificacion").text($(selector).children("option:selected").text());
            $(selector).prepend("<option value='0'>Seleccione</option>");
            $(selector).val('1');
        }
    })
}