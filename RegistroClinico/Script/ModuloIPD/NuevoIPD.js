﻿let bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

let alertFlag = 0;
let RCE_idEventos = null;
function cargarCombos() {
    CargarComboServicioSalud();
    CargarComboEstablecimiento();
    CargarComboEspecialidad();
    CargarComboTipoIPD();
    CargarComboPrevision();
    CargarComboConfirmarDiagnostico();
    comboTipoRepresentante();
    CargarComboAugePadre();
}

let sesion = getSession()

function CargarComboServicioSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlServicio"), 'ddlServicio'); // ddlServicio para traer desde Funciones.js el id asociado a Serv Salud Mag. como predeterminado
}
function CargarComboEstablecimiento() {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlEstablecimiento"));
}
function CargarComboEspecialidad() {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlEspecialidad"));
}
function CargarComboTipoIPD() {
    let url = `${GetWebApiUrl()}GEN_Ambito/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAmbito"))
}
//Datos Clinicos
function CargarComboAugePadre() {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAuge"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlSubAuge"));
}

function CargarComboPrevision() { //NO ID NO VALOR
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlPrevision"));
}
function CargarComboConfirmarDiagnostico() { //NO ID NO VALOR
    let url = `${GetWebApiUrl()}RCE_Tipo_IPD/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlTipoIPD"));

}
//Constancia
function comboTipoRepresentante() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $('#ddlConstanciaRepresentante'));
}

function comboPrevisionTramo(id) {
    let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${id}`;
    setCargarDataEnCombo(url, false, $("#ddlPrevisionTramo"));
}

$('#ddlPrevision').change(function () {
    id = $('#ddlPrevision').val();
    if (id === "0") {
        $("#ddlPrevisionTramo").empty()
        return
    }

    comboPrevisionTramo(id);
    $('divPrevisionTramo').show()
});

$(document).ready(function () {
    // FECHA PARA INICIAR TRATAMIENTO
    let fechaAnterior = document.getElementById('txtFechaTrat');
    if (fechaAnterior) {
        // Obtener fecha actual
        let fechaActual = new Date();
        // Restar 30 dias a la fecha actual
        fechaActual.setDate(fechaActual.getDate() - 60);

        let fechaLimite = fechaActual.toISOString().split('T')[0];
        // Establecer fecha limite en el campo fecha de inicio tratamiento
        fechaAnterior.setAttribute('min', fechaLimite);
    }

    // HACER QUE NO SE PUEDAN ELEGIR DIAS POSTERIORES EN LA FECHA IPD
    let fechaInput = document.getElementById('txtIPDFecha');
    if (fechaInput) {
        // Obtengo fecha actual
        let fechaActual = new Date().toISOString().split('T')[0];
        // Estableciendo fecha actual como la fecha maxima en el campo fecha IPD
        fechaInput.setAttribute('max', fechaActual);
    }

    window.addEventListener('beforeunload', bunload, false);
    cargarCombos();

    let sSession = getSession();
    RevisarAcceso(true, sSession);


    $("#switchAuge").bootstrapSwitch();
    $("#switchTitular").bootstrapSwitch();


    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico) {
        $('#ddlPrevisionTramo').removeAttr('data-required');
        $('#divIPDFecha').removeClass('offset-1');
        //$('#divIPDFecha').addClass('offset-4');
    }
    $('#divSubAuge').hide();

    $('#txtFechaTrat').val(moment().format("YYYY-MM-DD"));
    $('#txtFechaNac').val(moment().format("YYYY-MM-DD"));

    let iIdIPD = sSession.ID_IPD;

    if (iIdIPD == undefined) {
        iIdIPD = 0;
    }

    $('#ddlPrevision').change(function () {
        //comboPrevisionTramo($(this).val(), 0);
        //$('button[data-id="ddlPrevisionTramo"]').html('Seleccione');
    });

    $('#idIPD').val(iIdIPD);

    // Inicio validacion de rut
    $("#txtRutRep, #txtRutDigitoRep").on('input', function () {
        const nuevoValorRut = $("#txtRutRep").val();
        const nuevoValorDigito = $("#txtRutDigitoRep").val();
        TxtDigitoPac_Input(nuevoValorRut, nuevoValorDigito);
    });

    async function TxtDigitoPac_Input(numeroDocumento, nuevoValorDigito) {
        const valorAnteriorDigito = $("#txtRutDigitoRep").data("valor-anterior") || "";

        if ($.trim(nuevoValorDigito) !== "") {
            const esRutCorrecto = await EsValidoDigitoVerificador(numeroDocumento, nuevoValorDigito);

            if (esRutCorrecto) {
                // Si el rut esta correcto, busca y muestra
                buscarPersonaPorDocumento(numeroDocumento);
            } else {
                mostrarErrorRutIncorrecto();
                LimpiarCampoNombre(); // Limpio el nombre si el rut es incorrecto
            }
        } else if (valorAnteriorDigito.length > 0) {
            // Se elimina el dígito verificador
            LimpiarCampoNombre(); // Limpia solo el nombre si se elimina el dígito verificador
        }

        $("#txtRutDigitoRep").data("valor-anterior", nuevoValorDigito);
    }

    function mostrarErrorRutIncorrecto() {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'RUT Incorrecto',
            showConfirmButton: false,
            timer: 1500,
            allowOutsideClick: false
        });
    }
    function LimpiarCampoNombre() {
        $("#txtNombreRep").val("");
    }
    //Fin validacion de rut

    function buscarPersonaPorDocumento(numeroDocumento) {

        let url = `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=1&numeroDocumento=${numeroDocumento}`
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {
                const { Id, Nombre, ApellidoPaterno, ApellidoMaterno } = data[0];
                const nombre = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`;
                $("#txtNombreRep").val(nombre);
                $("#idRepresentante").val(Id);
            },
            error: function (jqXHR, status) {
                if (jqXHR.status == 401) {
                    console.log("Persona no encontrado")
                }
                console.log(`Error al buscar persona ${JSON.stringify(jqXHR)}`);
            }
        });
    }

    //Hay ID
    if (iIdIPD != 0 && iIdIPD != undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_IPD/${iIdIPD}`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                //Servicio de Salud
                $('#ddlServicio').val('1'); // Pasando predeterminadamente el Serv Salud Magallanes en el editar
                $('#ddlEstablecimiento').val(data.IdEstablecimiento);
                $('#ddlEspecialidad').val(data.IdEspecialidad);
                $('#ddlAmbito').val(data.IdAmbito);
                //Paciente
                CargarPacienteActualizado(data.Paciente.Id); // AQUI VER BIEN EL Id O IdPaciente
                BloquearPaciente();
                deleteSession("ID_PACIENTE");
                //Datos Clinicos
                $('#txtDiagnostico').val(data.Diagnostico);
                $('#txtFundamentoDiag').val(data.Fundamento);
                $('#txtTratamiento').val(data.Tratamiento);
                $('#ddlTipoIPD').val(data.IdTipoIPD);
                //Traigo la prevision seleccionada
                $('#sltPrevision').val(data.IdPrevision).change()
                $('#sltPrevisionTramo').val(data.IdPrevisionTramo)

                $('#txtFechaTrat').val(moment(new Date(data.FechaInicioTratamiento)).format("YYYY-MM-DD"));
                $('#txtIPDFecha').val(moment(new Date(data.Fecha)).format("YYYY-MM-DD"));
                $('#txtIPDHora').val(moment(new Date(data.Fecha)).format("HH:mm"));
                //Datos del profesional
                datosProfesional(data.GEN_idProfesional);

                //Datos GES
                if (data.PatologiaGES != null) {
                    $('#switchAuge').bootstrapSwitch('state', true);
                    if (data.PatologiaGES.PatologiaPadre != null) {
                        $('#ddlAuge').val(data.PatologiaGES.PatologiaPadre.IdPatologiaPadre);
                        $('#ddlSubAuge').show();
                        $('#divSubAuge').stop().fadeIn(100);
                        idPadre = $('#ddlAuge').val();
                        CargarComboAugeHijo(idPadre);
                        $('#ddlSubAuge').val(data.PatologiaGES.IdPatologiaGES);
                    } else {
                        $('#ddlAuge').val(data.PatologiaGES.IdPatologiaGES);
                    }
                }
                //IDEvento
                $('#idEvento').val(data.IdEvento);
                setSession("ID_EVENTO", data.IdEvento);

                if (sSession.ID_EVENTO != undefined) {
                    RCE_idEventos = parseInt(sSession.ID_EVENTO);
                    deleteSession("ID_EVENTO");
                }
                //Datos Constancia
                if (data && data.ConstanciaGES && data.ConstanciaGES.length > 0) {
                    $('#ddlConstanciaRepresentante').val(data.ConstanciaGES[0].TipoRepresentante !== null ? data.ConstanciaGES[0].TipoRepresentante.IdTipoRepresentante : '0');
                    $('#txtRutRep').val(data.ConstanciaGES[0].Representante.NumeroDocumento);
                    $('#txtRutDigitoRep').val(data.ConstanciaGES[0].Representante.Digito);
                    $('#txtRutRep').removeClass('is-invalid');
                    $('#txtConstanciaFecha').val((moment(new Date(data.ConstanciaGES[0].Fecha)).format("YYYY-MM-DD")));
                    $('#txtConstanciaHora').val((moment(new Date(data.ConstanciaGES[0].Fecha)).format("HH:mm")));
                    $('#txtNombreRep').val(data.ConstanciaGES[0].Representante.Nombre + " " + data.ConstanciaGES[0].Representante.ApellidoPaterno + " " + data.ConstanciaGES[0].Representante.ApellidoMaterno);
                    $('#txtConstanciaProRut').val(data.ConstanciaGES[0].Profesional.NumeroDocumento + "-" + data.ConstanciaGES[0].Profesional.Digito);
                    $('#txtConstanciaPro').val(data.ConstanciaGES[0].Profesional.Nombre + " " + data.ConstanciaGES[0].Profesional.ApellidoPaterno + " " + data.ConstanciaGES[0].Profesional.ApellidoMaterno);
                    $('#idRepresentante').val(data.ConstanciaGES[0].Representante.IdRepresentante);
                    $('#idConstancia').val(data.ConstanciaGES[0].Id);
                }
            }
        });

        if (sSession.ID_IPD != undefined)
            deleteSession('ID_IPD');

    } else {
        $('#txtIPDFecha').val(moment().format("YYYY-MM-DD"));
        $('#txtIPDHora').val(moment().format("HH:mm"));

        let idPro = datosProfesional();

        if (sSession.ID_EVENTO != undefined) {
            RCE_idEventos = parseInt(sSession.ID_EVENTO);
            deleteSession("ID_EVENTO");
        }
        if (sSession.ID_PACIENTE != undefined) {
            CargarPacienteActualizado(sSession.ID_PACIENTE)
            BloquearPaciente();
            deleteSession("ID_PACIENTE");
        }

        if (sSession.ID_IPD != undefined)
            deleteSession('ID_IPD');
    }
    $('#switchAuge').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            // Si el interruptor está en ON
            $('#divAuge').stop().fadeIn(100);
            $('#ddlAuge').prop('required', true);
            $('#ddlAuge').attr("data-required", 'true');
        } else {
            // Si el interruptor está en OFF
            $('#divAuge').stop().fadeOut(100);
            $('#ddlAuge').prop('required', false);
            $('#ddlAuge').attr("data-required", 'false');
            $('#divSubAuge').stop().fadeOut(100);
            $('#ddlSubAuge').prop("required", false);
            $('#ddlSubAuge').attr("data-required", 'false');
            ReiniciarRequired();
        }
    });

    $('#switchTitular').on('switchChange.bootstrapSwitch', function (event, state) {
        let nombrePac = "";
        let nombreRep = "";
        let numeroDocumentoRep = "";
        let digitoRep = "";

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_IPD/${sSession.ID_IPD}`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {                
                const { ConstanciaGES, Paciente } = data;
                const {
                    IdRepresentante,
                    NumeroDocumento: NumeroDocumentoRep,
                    Digito: DigitoRep,
                    Nombre: NombreRep,
                    ApellidoPaterno: ApellidoPaternoRep,
                    ApellidoMaterno: ApellidoMaternoRep,
                    //IdIdentificacion,
                } = ConstanciaGES.Representante;

                // Trayendo la data del representante en el select y la fecha y hora
                let representante = (data.ConstanciaGES && data.ConstanciaGES.Representante) || undefined;
                // Trayendo la data de fecha y hora
                let fecha = (data.ConstanciaGES && data.ConstanciaGES.Fecha) ? moment(data.ConstanciaGES.Fecha).format("YYYY-MM-DD") : undefined;
                let hora = (data.ConstanciaGES && data.ConstanciaGES.Fecha) ? moment(data.ConstanciaGES.Fecha).subtract(3, 'hours').format("HH:mm:ss") : undefined;

                const {
                    IdPaciente,
                    Identificacion: IdentificacionPac,
                    NumeroDocumento: NumeroDocumentoPac,
                    Digito: DigitoPac,
                    Nombre: nombrePaciente,
                    ApellidoPaterno: ApellidoPaternoPac,
                    ApellidoMaterno: ApellidoMaternoPac
                } = Paciente;

                nombrePac = `${nombrePaciente} ${ApellidoPaternoPac} ${ApellidoMaternoPac}`;
                nombreRep = `${NombreRep} ${ApellidoPaternoRep} ${ApellidoMaternoRep}`;

                // Verificar si el paciente se representa a sí mismo
                if (!state) {
                    // Si el paciente no se representa a sí mismo, cargar la información del representante
                    $('#txtNombreRep').val(nombreRep).prop('disabled', true).addClass('disabled');
                    $('#idRepresentante').val(IdRepresentante);
                    $('#txtRutRep').val(NumeroDocumentoRep);
                    $('#txtRutRep').attr('disabled', false);
                    $('#txtRutDigitoRep').attr('disabled', false);

                    // Trayendo la data desde let representante
                    $('#ddlConstanciaRepresentante').val(data.ConstanciaGES.TipoRepresentante.IdTipoRepresentante);

                    if (fecha !== undefined) {
                        $('#txtConstanciaFecha').val(fecha);
                    }

                    if (hora !== undefined) {
                        $('#txtConstanciaHora').val(hora);
                    }

                    if (DigitoRep !== undefined) {
                        $('#txtRutDigitoRep').val(DigitoRep.toUpperCase());
                    }
                } else {
                    // Si el paciente se representa a sí mismo, resetear los campos
                    comboTipoRepresentante();
                    $('#txtRutRep').attr('disabled', false);
                    $('#txtRutDigitoRep').attr('disabled', false);
                    $('#idRepresentante').val('0');
                    $('#txtRutRep').val("");
                    $('#txtRutDigitoRep').val("");
                    $('#txtNombreRep').val("");

                    $('#ddlConstanciaRepresentante').val("");
                    $('#lblValidarRutRep').removeClass('active');
                    $('#spanRutCorrectoRep').hide();
                    $('#spanRutIncorrectoRep').show();
                }
            }
        });
    });

    $('#txtRutRep').on('change', async function () {
        if (await EsValidoDigitoVerificador($('#txtRutRep').val(), $('#txtRutDigitoRep').val())) {
            $('#spanRutCorrectoRep').show();
            $('#spanRutIncorrectoRep').hide();
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$('#txtRutRep').val()}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        $('#txtNombreRep').val(data[0].GEN_nombrePaciente + ' ' + data[0].GEN_ape_paternoPaciente + ' ' + data[0].GEN_ape_maternoPaciente);
                        $('#idRepresentante').val(data[0].GEN_idPaciente);
                    }
                }
            });
        }
        else {
            $('#spanRutCorrectoRep').hide();
            $('#spanRutIncorrectoRep').show();
        }
    });

    $('#txtrutdigitorep').on('input', async function () {
        if (await EsValidoDigitoVerificador($('#txtrutrep').val(), $('#txtrutdigitorep').val())) {
            $('#spanrutcorrectorep').show();
            $('#spanrutincorrectorep').hide();
            $.ajax({
                type: 'GET',
                url: `${getwebapiurl()}GEN_Paciente/BuscarporDocumento/1/${$('#txtrutrep').val()}`,
                contenttype: "application/json",
                datatype: "json",
                success: function (data) {
                    if (!$.isemptyobject(data)) {
                        $('#txtnombrerep').val(data[0].gen_nombrepaciente + ' ' + data[0].gen_ape_paternopaciente + ' ' + data[0].gen_ape_maternopaciente);
                        $('#idrepresentante').val(data[0].GEN_idPaciente);
                    }
                }
            });
        }
        else {
            $('#spanrutcorrectorep').hide();
            $('#spanrutincorrectorep').show();
        }
    });

    // BOTON CONTINUAR QUE NOS LLEVA A LA CONSTANCIA
    $('#btnContinuar').on('click', function (e) {
        if (sesion.ID_IPD !== undefined) {
            let url = `${GetWebApiUrl()}RCE_IPD/${sesion.ID_IPD}`;
            $.ajax({
                type: 'GET',
                url: url,
                async: false,
                success: function (data, status, jqXHR) {
                    const Paciente = data ?? {};
                    const Identificacion = Paciente.Identificacion ?? {};

                    if (Identificacion.Id !== undefined) {
                        $("#ddlConstanciaRepresentante").val(Identificacion.Id);
                    }
                },
                error: function (jqXHR, status) {
                    if (jqXHR.status == 401) {
                        console.log("No encontrado");
                    }
                    console.log(`Error ${JSON.stringify(jqXHR)}`);
                }
            });
        }

        if (paciente.GEN_idPaciente === undefined || paciente.GEN_idPaciente === null || paciente.GEN_idPaciente === 0) {
            $('#txtnumerotPac').addClass('is-invalid');
        }

        // Validando que el switch AUGE/GES venga correctamente
        if (!$('#switchAuge').bootstrapSwitch('state')) {
            $('#ddlAuge').val(null);
            $('#ddlSubAuge').val("0");
        }

        // PROBANDO LA VALIDACION DE CAMPOS
        if (validarCampos("#divIPD", true) && ($("#sltIdentificacion").val() != "NN")) {
            // Trayendo el control de usuario para actualizar datos de paciente
            let id_paciente = GuardarPaciente().then(id_paciente => {
                console.log("ID Paciente obtenido:", id_paciente);
                nuevoIPD(id_paciente);
            });

            $('#mdlConstancia').modal('show');
        } else {
            // Mostrar mensaje de error si los campos no se completan
            toastr.error("Error, falta completar campos obligatorios");
            alertFlag = 1;
        }

        e.preventDefault();
    });

    function nuevoIPD(id_paciente) {
        let dFecha = moment($('#txtIPDFecha').val() + ' ' + $('#txtIPDHora').val());
        let idIPD = parseInt($('#idIPD').val());
        let idSubGrupo = $('#ddlSubAuge').val();
        let idPrevisionTramo = ($('#sltPrevisionTramo').val() !== "0" ? $("#sltPrevisionTramo").val() : 11);

        let json = {
            IdIPD: idIPD,
            Fecha: moment(dFecha).format('YYYY-MM-DD HH:mm:ss'),
            IdEstablecimiento: parseInt($('#ddlEstablecimiento').val()),
            IdEspecialidad: parseInt($('#ddlEspecialidad').val()),
            Diagnostico: $('#txtDiagnostico').val(),
            Fundamento: $('#txtFundamentoDiag').val(),
            Tratamiento: $('#txtTratamiento').val(),
            InicioTratamiento: moment(new Date($('#txtFechaTrat').val())).add(1, 'days').format('YYYY-MM-DD'),
            IdPaciente: id_paciente,
            IdAmbito: parseInt($('#ddlAmbito').val()),
            IdTipoIPD: parseInt($('#ddlTipoIPD').val()),
            IdProfesional: parseInt($('#idProfesional').val()),
            IdPrevisionTramo: idPrevisionTramo,
            IdPatologiaGES: parseInt($("#ddlAuge").val()) ?? null
        };

        console.log("JSON QUE SE ENVIA:", json);

        if (RCE_idEventos != null) {
            json.IdEventos = RCE_idEventos;
        }
        if (idSubGrupo != '' && idSubGrupo != null) {
            //json.IdPatologiaGES = idSubGrupo;
        }
        if (id_paciente == null || id_paciente == 0) {
            // Crear nuevo paciente si no existe
            CargarPaciente();
        }
        console.log("Datos que se envían en la solicitud POST:", JSON.stringify(json));
        if (idIPD == 0) {
            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}RCE_IPD`,
                data: JSON.stringify(json),
                contentType: 'application/json; charset=utf-8',
                crossDomain: true,
                dataType: 'json',
                success: function (data, status, jqXHR) {
                    // Actualizar valor de idIPD en el formulario
                    let iIdIPD = data.IdIPD;
                    $('#idIPD').val(data.IdIPD);
                },
                error: function (jqXHR, status) {
                    console.log(jqXHR);
                }
            });
        } else {
            // Si idIPD no es cero, realizar una actualizacion
            json.IdEventos = RCE_idEventos;

            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}RCE_IPD/${idIPD}`,
                data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, jqXHR) {
                    console.log("ENTRÉ EN EL PUT")
                },
                error: function (jqXHR, status, error) {
                    console.error(jqXHR, error);
                }
            });
        }
        return idIPD;
    }

    let idx = 0;
    $('#switchTitular').on('switchChange.bootstrapSwitch', async function (event, state) {
        if (state) {
            idx = await getIdPersonaOrPaciente('PAC');
        } else {
            $('#txtRutDigitoRep').on('input', async function () {
                idx = await buscarPersonaPorDocumentoReturnId($('#txtRutRep').val())
            })
        }
    })
    $('#btnNuevaConstancia').on('click', async function (e) {

        if ($("#txtRutRep").val() === "") {
            alert("Numero de documento vacio");
            return;
        }

        if (!$('#switchTitular').bootstrapSwitch('state') && $('#ddlConstanciaRepresentante').val() == '0')
            $('#btnOmitirConstancia').addClass('disabled');
        $('#btnContinuar').addClass('disabled');

        if ($('#ddlConstanciaRepresentante').val() == '0')
            $('#idRepresentante').val(paciente.GEN_idPaciente);

        let dFecha = $('#txtConstanciaFecha').val() + ' ' + $('#txtConstanciaHora').val();
        let fechaObjeto = new Date(dFecha);
        let fechaFormateada = fechaObjeto.toISOString().replace('T', ' ').slice(0, 19);

        let json = {
            IdIPD: parseInt($('#idIPD').val()),
            Fecha: moment(fechaFormateada).format('YYYY-MM-DD HH:mm:ss'),
            IdTipoRepresentante: parseInt($('#ddlConstanciaRepresentante').val()),
            IdRepresentante: parseInt($('#idRepresentante').val())
        };

        //Agrega evento change a #ddlConstanciaRepresentante
        $('#ddlConstanciaRepresentante').on('change', function () {
            // Obtengo valor seleccionado
            let valorSeleccionado = $(this).val();

            // Verifico si el valor es 0
            if (valorSeleccionado === '0') {
                // Deshabilitar campos
                $('#txtRutRep, #txtRutDigitoRep').prop('disabled', true);
            } else {
                // Habilitar campos
                $('#txtRutRep, #txtRutDigitoRep').prop('disabled', false);
            }
        });
        // Agregar evento input al campo #txtRutDigitoRep
        $("#txtRutDigitoRep").on('input', function () {
            const nuevoValor = $("#txtRutDigitoRep").val();
            TxtDigitoPac_Input($("#txtRutRep").val(), nuevoValor);
        });

        if ($('#switchTitular').bootstrapSwitch('state')) {
            json.GEN_idPersona_Representante = idx
        }
        else {
            json.GEN_idPersona_Representante = idx
        }

        // Creando la variable que obtiene un nuevo id de IPD
        let iIdIPD = parseInt($('#idIPD').val());

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_IPD/${iIdIPD}`,
            contentType: "application/json",
            dataType: "json",
            success: function (dataConst) {

                if (iIdIPD == 0) { //nuevo IPD, crea constancia
                    nuevaConstancia('POST', json, '', iIdIPD);
                } else {
                    if (!$.isEmptyObject(dataConst.ConstanciaGES)) { //Modifica 
                        json.Id = dataConst.ConstanciaGES.Id;
                        nuevaConstancia('PUT', json, dataConst.ConstanciaGES.Id, iIdIPD);

                    } else { //Existe IPD pero no constancia
                        nuevaConstancia('POST', json, '', iIdIPD);
                    }
                }
            }
        });

        e.preventDefault()
    });

    $('#btnOmitirConstancia').on('click', function (e) {
        let bValidar = true;

        $('#btnNuevaConstancia').addClass('disabled');
        $('#btnOmitirConstancia').addClass('disabled');
        $('#btnContinuar').addClass('disabled');

        if (iIdIPD == 0)
            toastr.success('IPD Ingresado correctamente', '', {
                onHidden: function () {
                    quitarListener();
                    window.location.replace(ObtenerHost() + '/Vista/ModuloIPD/BandejaIPD.aspx');
                }
            });
        else
            toastr.success('IPD Actualizado correctamente', '', {
                onHidden: function () {
                    quitarListener();
                    windows.location.replace(ObtenerHost() + '/Vista/ModuloIPD/BandejaIPD.aspx');
                }
            })
        e.preventDefault();
    });

    if (iIdIPD != 0 && iIdIPD != undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_IPD/${iIdIPD}`,
            async: false,
            success: function (data, status, jqXHR) {

                const { ConstanciaGES } = data ?? {}

                if (ConstanciaGES != undefined) {
                    const { Representante } = ConstanciaGES[0] ?? {}
                    const { IdIdentificacion } = Representante ?? {}

                    if ($('#switchTitular').bootstrapSwitch('state', true))
                        $("#ddlConstanciaRepresentante").val(IdIdentificacion !== undefined ? IdIdentificacion.GEN_idIdentificacion : '0')
                }
            },
            error: function (jqXHR, status) {
                if (jqXHR.status == 401) {
                    console.log("No encontrado")
                }

                console.log(`Error ${JSON.stringify(jqXHR)}`);
            }
        });
    }
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 2500,
        "extendedTimeOut": 1500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }
    ShowModalCargando(false);
});

function fechaDocumento() {
    var date1 = moment($('#txtFechaTrat').val()).format("YYYY-MM-DD");
    var date2 = moment($('#fechaEvento').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

    if (date1 < date2)
        return true;
    else
        return false;
}

function nuevaConstancia(sMethod, json, idGES, idIPD) {

    $.ajax({
        type: sMethod,
        url: `${GetWebApiUrl()}RCE_Constancia_GES/${idGES}`,
        data: JSON.stringify(json),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (data, status, jqXHR) {
            if (idIPD == 0)
                toastr.success('IPD Y CONSTANCIA INGRESADO CORRECTAMENTE', '', {
                    onHidden: function () {
                        quitarListener();
                        window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                    }
                });
            else
                toastr.success('IPD Y CONSTANCIA ACTUALIZADO CORRECTAMENTE', '', {
                    onHidden: function () {
                        quitarListener();
                        window.location.replace(ObtenerHost() + "/Vista/ModuloIPD/BandejaIPD.aspx");
                    }
                });
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });
}

function cargarProfesional() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/BuscarporDocumento?GEN_rutProfesional=${$('#txtRutPro').val()}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if ($.isEmptyObject(data[0])) {
                alert('El profesional no existe en la base de datos');
                $('#idProfesional').val(0);
                $('#txtNombrePro').val('');
                $('#txtApellidoPPro').val('');
                $('#txtApellidoMPro').val('');
                $('#txtRutPro').val('');
                $('#txtRutDigitoPro').val('')
            }
            else {
                $('#idProfesional').val(data[0].GEN_idProfesional);
                $('#txtNombrePro').val(data[0].GEN_nombreProfesional);
                $('#txtApellidoPPro').val(data[0].GEN_apellidoProfesional);
                $('#txtApellidoMPro').val(data[0].GEN_sapellidoProfesional);
            };
        }
    });
}
function datosProfesional() {
    let iIdPro;

    // Obtener el ID del usuario logeado
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        success: function (data) {
            iIdPro = data[0].Profesional.Id;

            // Obtener información del profesional
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Profesional/${iIdPro}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    $('#idProfesional').val(data.Id);
                    $("#txtNumeroDocumentoProfesional").val(data.NumeroDocumento +
                        (data.Digito !== null ? '-' + data.Digito : ''));
                    $('#txtNombrePro').val(data.Nombre);
                    $('#txtApellidoPPro').val(data.ApellidoPaterno);
                    $('#txtApellidoMPro').val(data.ApellidoMaterno);
                    $('#txtConstanciaPro').val(sSession.NOMBRE_USUARIO);
                    $('#txtConstanciaProRut').val(data.NumeroDocumento + "-" + data.Digito);

                    // Obtener detalles de la especialidad a través de la tabla intermedia
                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Pro_Especialidad/GEN_Profesional/${iIdPro}`,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (proEspecialidadData) { 
                            // Verificar si hay información de especialidad
                            if (proEspecialidadData && proEspecialidadData.length > 0) {
                                // Mapear especialidades a sus valores
                                const valoresEspecialidades = proEspecialidadData.map(especialidad => especialidad.Valor);

                                // Asignar los valores de las especialidades al campo de texto
                                $('#txtEspecialidadPro').val(valoresEspecialidades.join(', '));
                            } else {
                                // En caso de que no haya especialidades dejando el campo vacio
                                $('#txtEspecialidadPro').val('');
                            }
                        },
                        error: function (jqXHR, status) {
                            console.log(`Error ${JSON.stringify(jqXHR)}`);
                        }
                    });

                    $('#spanRutIncorrectoPro').hide();
                    $('#spanRutCorrectoPro').show();
                },
                error: function (jqXHR, status) {
                    console.log(`Error ${JSON.stringify(jqXHR)}`);
                }
            });
        }
    });

    return iIdPro;
}

$('#ddlAuge').change(function () {
    idPadre = $('#ddlAuge').val()
    CargarComboAugeHijo(idPadre)

    select = document.querySelector("#ddlSubAuge");
    if (select.options.length == 1) { //No tiene valores 
        $('#divSubAuge').hide()
        $('#ddlSubAuge').attr("data-required", 'false');
        ReiniciarRequired();
    } else {
        $('#divSubAuge').show()
        $('#ddlSubAuge').attr("data-required", 'true');
        ReiniciarRequired();
    }
});

async function getIdPersonaOrPaciente(tipo) {

    let url = `${GetWebApiUrl()}RCE_IPD/${sesion.ID_IPD}`
    let idPerOrPac = 0

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            if (tipo === "PAC") {
                const { Paciente } = data ?? {}
                const { IdPersona } = Paciente ?? {}
                idPerOrPac = IdPersona
            }
            else if (tipo === "PER") {
                const { ConstanciaGES } = data ?? {}
                const { IdRepresentante } = ConstanciaGES[0].Representante
                idPerOrPac = IdRepresentante
            }
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
                console.log("No encontrado")
            }
            console.log(`Error ${JSON.stringify(jqXHR)}`);
        }
    });
    return idPerOrPac
}

async function buscarPersonaPorDocumentoReturnId(numeroDocumento) {

    let url = `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=1&numeroDocumento=${numeroDocumento}`
    let id = 0
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            const { Id } = data[0]

            id = Id
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
                console.log("No encontrado")
            }
            console.log(`Error ${JSON.stringify(jqXHR)}`);
        }
    });

    return id
}


