﻿
var sSession;
var idHos;
var idCama;
var idAislamiento;
let idUbicacionActualCamaTraslado;
let datosExportarHOS = [];
let datosExportarIM = [];
let fechaActual = moment(GetFechaActual()).format('DD-MM-YYYY')
//Variable permite saber si estoy en la bandeja o en el mapa de camas
let categorizandoDesdeMapaCamas = false

$(document).ready(async function () {

    sSession = getSession();
    let IniciarComponentes = async () => {

        ShowModalCargando(true);
        await sleep(0);

        idAislamiento = 0;
        idCama = 0;
        idHos = 0;
        idDiv = "";

        //Funciones de combo personas
        comboIdentificacion("#sltTipoIdentificacion");
        establecerValor("sltTipoIdentificacion", "0")
        ChangeTitleNumDoc("#sltTipoIdentificacion", "#txtHosFiltroRut", "#lblTipoIdentificacion")
        comboIdentificacion("#selTipoIdentificacionAcompanante", "#lblTipoIdentificacionAcompanante");
        comboIdentificacion("#sltIngTipoIdentificacion", "#lblIngTipoIdentificacion");
        ChangeTitleNumDoc("#sltIngTipoIdentificacion", "#txtIngFiltroRut", "#lblIngTipoIdentificacion")

        deshabilitarPersona()
        comboUbicacion();
        comboEstado();
        comboTipoHospitalizacion();
        InicializarInputRut("#selTipoIdentificacionAcompanante", "#txtnumeroDocAcompañante", "#txtDigAcompañante")
        $("#btnHosExportar").hide();

        cargarPerfil(sSession.CODIGO_PERFIL);

        inicioTipoIdentificacionEnZeroHospitalizacion()

        limpiarRutDigitoSegunIndentificaicon()

        inicializarRutDigitoSegunEstado()

        $("#txtHosFiltroRut").on("keypress", function (e) {
            if ($("#sltTipoIdentificacion").val() == 0)
                $("#sltTipoIdentificacion").val("1")

            if ($("#sltTipoIdentificacion").val() == 1 || $("#sltTipoIdentificacion").val() == 4) {
                var charCode = (e.which) ? e.which : e.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
            }
            return true;
        });

        $("#txtHosFiltroRut").on("blur", function () {
            manejarBlurRut($("#txtHosFiltroRut"), $("#sltTipoIdentificacion"), $("#txtHosFiltroDV"));
        });

        $("#txtIngFiltroRut").on("blur", function () {
            manejarBlurRut($("#txtIngFiltroRut"), $("#sltIngTipoIdentificacion"), $("#txtIngFiltroDV"));
        });

        ocultarMostrarFichaPac($("#sltIngTipoIdentificacion"), $("#txtIngFiltroDV"));

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": 150,
            "hideDuration": 150,
            "timeOut": 2000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "swing",
            "showMethod": "slideDown",
            "hideMethod": "slideUp"
        }

        $("#btnIngFiltro").on('click', function () {
            getTablaSolicitudesHospitalizacion();
            return false;
        });

        ocultarMostrarFichaPac($("#sltTipoIdentificacion"), $("#txtHosFiltroRut"))

        $("#btnHosLimpiarFiltro").on("click", function () {
            limpiarFiltrosHospitalizacion();
        })

        limpiarFiltrosHospitalizacion()
        limpiarFiltrosIngresoMedico()

        $("#btnIngLimpiarFiltro").on("click", function () {
            limpiarFiltrosIngresoMedico();
        });

        $("#ddlServicioOrigen").change(function () {
            CargarCama($(this).val(), "#ddlCamaOrigenNuevo");
        });

        $("#ddlServicioDestino").change(function () {
            CargarCama($(this).val(), "#ddlCamaDestino");
        });

        $('#aTraslado').click(function () {
            if (idCama == null) {
                toastr.warning("No se puede trasladar al paciente porque éste no tiene cama actual");
            }
            else if (validarCampos('#divNuevoTraslado', false)) {

                ShowModalCargando(true);
                var id = $('#idHos').val();
                var idTraslado = $('#idTraslado').val();

                var json;
                var sMetodo;
                let tipoTraslado = $("#swTrasladoExterno").is(":checked") ? 296 : 295;

                if (idTraslado == '0') {
                    sMetodo = 'POST';
                    json = {
                        HOS_idHospitalizacion: id,
                        HOS_idCamaDestino: $("#ddlCamaDestino").val(),
                        GEN_idTipo_Movimientos_Sistemas: tipoTraslado
                    };
                    idTraslado = '';
                    idCama = $("#ddlCamaDestino").val();

                } else {
                    sMetodo = 'PUT';
                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}HOS_Traslados_Cama/${idTraslado}`,
                        contentType: "application/json",
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            json = data;
                        }
                    });

                    json.HOS_idCamaDestino = $("#ddlCamaDestino").val();
                    //delete json.GEN_Cama;
                    //delete json.GEN_Cama1;
                    delete json.HOS_Cama;
                    delete json.HOS_Cama1;
                    idCama = $("#ddlCamaDestino").val();

                }

                $.ajax({
                    type: sMetodo,
                    url: GetWebApiUrl() + 'HOS_Traslados_Cama' + ((sMetodo === 'PUT') ? '/' + idTraslado : ''),
                    data: JSON.stringify(json),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: false,
                    success: function (data, status, jqXHR) {
                        llenarGrillaTraslado(false);
                        $("#aTraslado").text("Agregar Traslado");
                        $("#idTraslado").val("0");
                        CargarCama($("#ddlServicioDestino").val(), "#ddlServicioDestino");
                        idUbicacionActualCamaTraslado = $("#ddlServicioDestino").val();
                        getTablaHospitalizacion();
                    }, error: function (jqXHR, status) {
                        console.log(jqXHR);
                    }
                });
                ShowModalCargando(false);
            } else {
                toastr.error("Debe ingresar los campos requeridos!");
            }
        });

        deleteSession('ID_HOSPITALIZACION');
        deleteSession('ID_HOJAEVOLUCION');
        deleteSession('ID_INGRESO_MEDICO');
        deleteSession('ID_HOJA_ENFERMERIA');

        if (session.CODIGO_PERFIL === perfilAccesoSistema.admisionUrgencia) {
            $("#aBandejaSolicitudesHospitalizacion").trigger("click");
        }

    }
    IniciarComponentes().then(() => ShowModalCargando(false));
});

//function ingresarPacienteEnfermeria() {
//    let id = parseInt($("#txtIdHospMdlCamaOrigen").val())
//    let textoAlert = `Se ha establecido exitósamente la cama de origen para la hospitalización N° ${id}`

//    if (validarCampos("#mdlCamaOrigen", false)) {
//        json = {
//            "IdHospitalizacion": parseInt(id),
//            "IdUbicacion": valCampo($("#ddlServicioOrigen").val()),
//            "IdCama": valCampo($("#ddlCamaOrigenNuevo").val()),
//            "ObservacionMorbidos": $("#txtAntecedentesMorbidos").val(),
//            "Motivo": $("#txtMotivoConsultaPaciente").val(),
//            "Diagnostico": $("#txtDiagnosticoHospitalizacion").val(),
//            "CondicionPaciente": $("#txtCondicionActualPaciente").val(),
//            "ObservacionCirugias": $("#txtObservacionesQuirurgicos").val(),
//            "Fecha": `${$("#dpFechaIngreso").val()} ${$("#dpHoraIngreso").val()}`
//        }
//        console.log(json)
//    }

//    if (!validarCamposInputRadio("#antecedentesMorbidos", true)) {
//        toastr.error("Debe ingresar los antecedentes mórbidos")
//        $("#divAntecedentesMorb").collapse('show')
//        return false
//    }

//    if (!validarCamposInputRadio("#alergiasIngreso", true)) {
//        toastr.error("Debe ingresar las alergias")
//        $("#divAlergias").collapse('show')
//        return false
//    }
//    if (!validarCamposInputRadio("#habitosIngreso", true)) {
//        toastr.error("Debe ingresar habitos")
//        $("#divHabitos").collapse('show')
//        return false
//    }

//    let antecedentesChecks = $("#antecedentesMorbidos").find("input[type='radio']:checked")
//    let alergiasChecks = $("#alergiasIngreso").find("input[type='radio']:checked")

//    let antecedenteaApi = antecedentesChecks.map((index, item) => {
//        console.log(item)
//        return {
//            idItem: $(item).data("idantecedente"),
//            idRespuesta: item.value
//        }
//    })
//    console.log(antecedenteaApi)
//}


function limpiarFiltrosHospitalizacion() {
    $("#txtHosFiltroId").val('');
    $("#sltTipoIdentificacion").val(0);
    $("#txtHosFiltroRut").val('');
    $("#txtHosFiltroDV").val('');
    $("#txtHosFiltroNombre").val('');
    $("#txtHosFiltroApePaterno").val('');
    $("#txtHosFiltroApeMaterno").val('');
    $("#ddlHosEstado").val(0);
    $("#ddlUbicacion").val(0);
    getTablaHospitalizacion();
}
function limpiarFiltrosIngresoMedico() {
    $("#txtIngFiltroRut").val('');
    $("#txtIngFiltroNombre").val('');
    $("#txtIngFiltroApe").val('');
    $("#txtIngFiltroSApe").val('');

    comboUbicacion();
    getTablaSolicitudesHospitalizacion();
}



function inicioTipoIdentificacionEnZeroHospitalizacion() {

    const filtroBandeja = $("#btn-FiltroBandejaHospitalizacion")
    filtroBandeja.click(function () {
        toogleFiltroBandeja(
            {
                filtro: "btn-FiltroBandejaHospitalizacion",
                identificacion: "sltTipoIdentificacion",
                rut: "txtHosFiltroRut",
                dv: "txtHosFiltroDV",
                guionDv: "guionDigitoHos"
            })
    })
}

function limpiaRutDigito() {
    $("#divRutDigito input").val("")
}

function inicializarRutDigitoSegunEstado() {
    $("#ddlHosEstado").change(function () {
        if ($(this).val() === "0") {
            establecerValor("sltTipoIdentificacion", "1");
            limpiaRutDigito()
        }
    })
}

function limpiarRutDigitoSegunIndentificaicon() {
    $("#sltTipoIdentificacion").change(function () {
        if ($(this).val() === "0") {
            limpiaRutDigito()
        }
        else
            $("#txtHosFiltroRut").removeAttr("disabled")
    })

    $("#txtHosFiltroRut").on("input", function () {

        if ($(this).val() === "") {
            establecerValor("sltTipoIdentificacion", "1");
        }
        else {
            $("#sltTipoIdentificacion").val()
        }

    })
}

function ocultarBotonesSegunPerfildeAcceso() {

    if (sSession.CODIGO_PERFIL != perfilAccesoSistema.gestionCama)
        $("#ulTablist").show();

    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico || sSession.CODIGO_PERFIL === perfilAccesoSistema.admision || perfilAccesoSistema.enfermeriaMatroneria) {
        //Se muestra IM
        $("#liDatosIngreso").prop("style", "display: list-item;");
        //$("#divBandejaIngresoMedico").prop("style", "display: flex;");
        $("#aBandejaIngresoMedico").click(function () {
            getTablaSolicitudesHospitalizacion();
            if (session.CODIGO_PERFIL === perfilAccesoSistema.admision) {
                $("#divProfesional").show();
                comboProfesional();
            }
        });
    }

    if (sSession.CODIGO_PERFIL != perfilAccesoSistema.admision) {
        $("#mdlVerHojasEvolucion").on("shown.bs.modal", function () {
            $(".hhcc").attr("style", "width:1px !important;");
        });
    }
}

//carga funcionalidades y estados según perfil
function cargarPerfil(codPerfil) {
    switch (codPerfil) {
        case perfilAccesoSistema.medico:
            $("#tabBandejaHOS").show();
            break;
        case perfilAccesoSistema.enfermeriaMatroneria:
            $("#tabBandejaHOS").show();
            //$("#btnNuevoHos").show();
            break;
        case perfilAccesoSistema.admision:
        case perfilAccesoSistema.secretaria:
            $("#tabBandejaHOS").show();
            $("#btnNuevoHos").show();
            break;
        case perfilAccesoSistema.gestionCama:
            $("#tabBandejaHOS").show();
            break;
        default:
            break;
    }
    ocultarBotonesSegunPerfildeAcceso();
}



//Tabla lista hojas de enfermeria segun parametro
function getHojaEnfermeriaPorNHospitalizacion(idHospitalizacion, nomPaciente = "", nomCama = "") {

    let url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/buscar?idHospitalizacion=${idHospitalizacion}`
    let adataset = []

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            data.forEach((item, index) => {
                adataset.push([
                    item.Id,
                    moment(item.Fecha).format("DD-MM-YYYY"),
                    item.Diagnostico,
                    item.IdHospitalizacion,
                    (item.Turno.Id === 1) ? `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-brightness-high-fill" viewBox="0 0 16 16">
                                    <path d="M12 8a4 4 0 1 1-8 0 4 4 0 0 1 8 0zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z" />
                                </svg>` : `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-moon-fill" viewBox="0 0 16 16">
                                    <path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278z" />
                                </svg>`,
                    item.Fecha,
                    item.Turno.Id
                ]);
            });

            $("#tblHojaEnfermeriaPorHospitalizacion").addClass("nowrap").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        className: 'btn btn-info',
                        text: 'Exportar a Excel'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-info',
                        text: 'Exportar en PDF'
                    }
                ],
                "order": [[0, "desc"]],
                data: adataset,
                columns: [
                    { title: "ID" },
                    { title: "Fecha" },
                    { title: "Diagnostico" },
                    { title: "IdHopitalizacion" },
                    { title: "Turno", class: "text-center" },
                    { title: "IdTurno" },
                    { title: "", class: "text-center" }

                ],

                columnDefs: [
                    {
                        targets: [2],
                        render: function (data, type, full, meta) {
                            return "<div style='white-space:normal;'>" + data + "</div>";
                        }
                    },
                    {
                        targets: -1,
                        data: null,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            let fechaHojaEnfermeria = moment(adataset[fila][5]).format("DD-MM-YYYY")
                            let botones =
                                `
                                <button class="btn btn-primary btn-circle" 
                                    onclick="toggle('#div_accionesHE${fila}'); return false;">
                                    <i class="fa fa-list" style="font-size:15px;"></i>
                                </button>
                                    <div id="div_accionesHE${fila}" class="btn-container" style="display: none;">
                                        <center>
                                            <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                            <div class="rounded-actions">
                                                <center>
                            `;

                            //EDITAR
                            if (fechaHojaEnfermeria === fechaActual) {
                                botones +=
                                    `
                                <a
                                    data-id="${adataset[fila][0]}"
                                    data-idhosp="${adataset[fila][3]}"
                                    data-idturno="${adataset[fila][6]}"
                                    class="btn btn-info btn-circle btn-lg load-click"
                                    onclick="linkEditarHojaEnfermeria(this);"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Editar Hoja Enfermería/Matronería">
                                    <i class="fas fa-edit"></i>   
                                </a>
                                <br>
                            `;
                            }

                            //IMPRIMIR
                            botones +=
                                `
                                <a
                                    data-id="${adataset[fila][0]}"
                                    data-idhosp="${adataset[fila][3]}"
                                    class="btn btn-info btn-circle btn-lg load-click"
                                    onclick="linkImprimirHojaEnfermeria(this);"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Imprimir Hoja Enfermería/Matronería">
                                    <i class="fas fa-print"></i>
                                </a>
                                <br>
                            `;

                            //ANULAR
                            if (fechaHojaEnfermeria === fechaActual) {

                                botones +=
                                    `
                                <a  
                                    data-id="${adataset[fila][0]}"
                                    data-idhosp="${adataset[fila][3]}"
                                    class="btn btn-danger btn-circle btn-lg load-click"
                                    onclick="linkAnularHojaEnfermeria(this);"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Anular Hoja Enfermería/Matronería">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                <br>
                            `;
                            }

                            //MOVIMIENTOS DE SISTEMA HOJA ENFERMERIA
                            botones +=
                                `
                                <a  
                                    data-id="${adataset[fila][0]}"
                                    data-idhosp="${adataset[fila][3]}"
                                    class="btn btn-info btn-circle btn-lg load-click"
                                    onclick="linkMostrarModalMovimientosHojaEnfermeria(this);"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Movimientos Hoja Enfermería/Matronería">
                                    <i class="fas fa-list"></i>
                                </a>
                                <br>
                            `;

                            botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >
                            `;

                            return botones;
                        }
                    },

                    {
                        "targets": [3, 5],
                        "visible": false
                    }
                ],
                "bDestroy": true
            });

            if (Array.isArray(adataset) && adataset.length === 0) {

                $("#tblHojaEnfermeriaPorHospitalizacion").empty()
                $("#ocultarTablaHojaEnfermeriaPorHospitalizacion").hide()
                $("#alertHojaEnfermeriaPorHospitalizacion").html("No existe información para mostrar")
            }

            $("#ocultarTablaHojaEnfermeriaPorHospitalizacion").show()
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()

            if (err.status == 404) {

            }
        }
    });
}

const anularHojaEnfermeria = (id, idhosp) => {

    let url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`

    $.ajax({
        type: 'DELETE',
        url: url,
        success: function (status, jqXHR) {
            getHojaEnfermeriaPorNHospitalizacion(idhosp)
        },
        error: function (jqXHR, status) {
            toastr.error('Error al anular Hoja de Enfermería / Matronería')
            console.log(jqXHR.responseText)
        }
    });

    return `id=${id} id=${idhosp}`
}

function anularHospitalizacion(id) {

    $.ajax({
        type: 'DELETE',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${id}`,
        success: function (status, jqXHR) {
            getTablaHospitalizacion();
            ocultarModal("mdlAlerta");
            toastr.success('HOSPITALIZACIÓN ANULADA');
        },
        error: function (jqXHR, status) {
            toastr.error('ERROR AL ANULAR HOSPITALIZACIÓN');
            console.log(jqXHR.responseText);
        }
    });
}

//Nota esta es la que se llama actualmente
function getTablaSolicitudesHospitalizacion() {

    const url = `${GetWebApiUrl()}HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&numeroDocumento=${$('#txtIngFiltroRut').val()}` +
        `&nombre=${$('#txtIngFiltroNombre').val()}` +
        `&apellidoPaterno=${$('#txtIngFiltroApe').val()}` +
        `&apellidoMaterno=${$('#txtIngFiltroSApe').val()}`

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $('#tblSolicitudesHospitalizacion').DataTable({
                data: data,
                columns: [
                    /*0*/ { "data": "Id", "title": "ID", "name": "id" },
                    /*1*/ { "data": "Paciente", "title": "N° Documento", "render": ({ NumeroDocumento, Digito }) => `${NumeroDocumento}${Digito != null ? `-${Digito}` : ""}` },
                    /*2*/ { "data": "Paciente", "title": "Nombre", "render": (pac) => getNombreCompleto(pac) },
                    /*3*/ { "data": "Fecha", "title": "Fecha", "render": (fecha) => moment(fecha).format('DD-MM-yyyy') },
                    /*4*/ { "data": "Ubicacion.Valor", "title": "Ubicación" },
                    /*5*/ { "data": "Motivo", "title": "Motivo" },
                    /*6*/ { "data": "Acciones", "title": "Acciones", "className": "text-center" }
                ],
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            const { Id, IdEvento, IdPaciente } = row;

                            var fila = meta.row;
                            let botones = "";

                            if (sSession.CODIGO_PERFIL == perfilAccesoSistema.admision || sSession.CODIGO_PERFIL == perfilAccesoSistema.admisionUrgencia) {
                                botones += `   
                                        <a  id='aCrearPreIngresoHospitalizacion_${fila}'
                                            class='btn btn-success' 
                                            href='#/' 
                                            onclick='linkCrearPreIngresoHospitalizacion(${Id}, ${IdPaciente},${IdEvento})'
                                            data-toggle='tooltip' 
                                            data-placement='left' 
                                            title='Crear preingreso de hospitalización'>
                                            <i class='fas fa-check'></i>
                                        </a>`;
                            }

                            botones += `
                                <a  id='aImprimirSolicitudHspitalizacion_${fila}'
                                    class="btn btn-info"
                                    onclick="linkImprimirSolicitudHospitalización(${Id});"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    title="Imprimir Solicitud de hospitalización">
                                    <i class="fas fa-print"></i>
                                </a>`;

                            return botones;

                        }
                    }
                ],
                bDestroy: true
            });
            $('#tblIngreso').css("width", "100%");
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}

function linkImprimirSolicitudHospitalización(id) {
    const url = `${GetWebApiUrl()}HOS_Solicitud_Hospitalizacion/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

async function linkCrearPreIngresoHospitalizacion(idSolicitudHospitalizacion, idpaciente, idEvento) {

    const url = `HOS_Hospitalizacion/Buscar?idPaciente=${idpaciente}&idTipoEstado=87`
    let hospitalizacionesPreingresadas = await promesaAjax("GET", `${url}`)

    console.log(idpaciente)
    console.log(hospitalizacionesPreingresadas)

    if (hospitalizacionesPreingresadas.length > 0) {

        Swal.fire({
            icon: 'info',
            title: `Hospitalización`,
            html: `
        <p>El paciente ya tiene una hospitalización pre-ingresada <strong>#${hospitalizacionesPreingresadas[0].Id}</strong>, ¿Qué desea hacer?
      
        <div class="button-container">
            <button id="btnIr" class="swal2-confirm swal2-styled custom-button green-button">Ir</button>
            <button id="btnAnular" class="swal2-confirm swal2-styled custom-button red-button">Anular</button>
            <button id="btnCancelar" class="swal2-cancel swal2-styled custom-button cancel-button">Cancelar</button>
        </div>
    `,
            showConfirmButton: false,
            showCancelButton: false, 
            allowEscapeKey: false,
            allowOutsideClick: false,
        }).then(() => {
            
        });

        // Controladores para los botones
        document.getElementById('btnIr').addEventListener('click', () => {
            setSession('ID_SOLICITUD_HOSPITALIZACION', idSolicitudHospitalizacion);
            irAEditarHospitalizacion(hospitalizacionesPreingresadas[0].Id)
            //window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevoFormularioAdmision.aspx`;
            Swal.close();
        });

        document.getElementById('btnAnular').addEventListener('click', async () => {
            await anularSolicitudHospitalizacion(idSolicitudHospitalizacion)
            Swal.close();
        });
        document.getElementById('btnCancelar').addEventListener('click', () => {
            Swal.close();
        });
    }
    else {
        setSession('ID_SOLICITUD_HOSPITALIZACION', idSolicitudHospitalizacion);
        setSession('ID_PACIENTE', idpaciente);
        setSession('ID_EVENTO', idEvento);
        window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevoFormularioAdmision.aspx`;
    }
}

async function anularSolicitudHospitalizacion(idSolicitudHospitalizacion) {

    await Swal.fire({
        title: 'Solicitud de Hospitalización',
        html: `¿Desea eliminar la Solicitud de hospitalización?`,
        icon: 'info',
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
        allowEscapeKey: false,
        allowOutsideClick: false,
    }).then((result) => {
        if (result.value) {
            promesaAjax("DELETE", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}`).then(res => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Solicitud Eliminada",
                    showConfirmButton: false,
                    timer: 2000
                }).then((result) => {
                    getTablaSolicitudesHospitalizacion();
                });
            });
        }
    });
}

function toggle(id) {
    if (idDiv == "") {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = "";
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}
function getColumnsTablaHospitalizacion() {

    return [
        /*0*/ { "data": "Id", "title": "ID", "name": "id" },
        /*1*/ { "data": "Paciente.NumeroDocumento", "title": "N° Documento", "orderable": false },
        /*2*/ { "data": "Paciente", "title": "Nombre", "name": "nombre", "render": (pac) => getNombreCompleto(pac) },
        /*3*/ { "data": "Cama.Actual", "title": "Cama", "name": "cama", "render": (cama) => cama ? cama.Valor : "" },
        /*4*/ { "data": "Cama.Actual", "title": "Ubicación", "name": "ubicacion", "render": (cama) => cama ? cama.Ubicacion.Valor : "" },
        /*5*/ { "data": "FechaIngreso", "title": "Fecha", "name": "fechaIngreso", "render": (fecha) => moment(fecha).format('DD-MM-yyyy') },
        /*6*/ { "data": "HoraIngreso", "title": "Hora", "name": "horaIngreso" },
        /*7*/ { "data": "Dias", "title": "Días", "name": "dias" },
        /*8*/ { "data": "Aislamientos", "title": "Aislamiento", "orderable": false },
        /*9*/{ "data": "TipoEstadosSistemas", "title": "Alertas", "className": "text-center", "orderable": true },
        /*10*/{ "data": "Acciones", "title": "Acciones", "className": "text-center", "orderable": false }
    ];
}

function getColumnsRenderTablaHospitalizacion() {

    return [
        { targets: [3, 4, 7, 8], visible: sSession.CODIGO_PERFIL != perfilAccesoSistema.admision },
        {
            targets: 9,
            render: (data, type, row, meta) => {
                return `${getIconsEstados(row)} ${getBadgeTipoHospitalizacion(row)}`
            }
        },
        {
            targets: 10,
            render: function (data, type, row, meta) {

                const fila = meta.row;
                const { Id, Paciente, Cama, FechaIngreso, HoraIngreso, TipoEstadosSistemas } = row;
                const { EditarHospitalizacion, AnularHospitalizacion, ImprimirHospitalizacion,
                    VerFormulariosExternos, VerMovimientosHospitalizacion, LiquidarCuenta } = row.Acciones;

                let botones =
                    `<button class="btn btn-primary btn-circle"
                            onclick="toggle('#div_accionesHOS${fila}'); return false;">
                            <i class="fa fa-list" style="font-size:15px;"></i>
                        </button>
                        <div id="div_accionesHOS${fila}" class="btn-container" style="display: none;">
                            <center>
                                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                <div class="rounded-actions">
                                    <center>`;

                if (sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria) {

                    botones += `<a
                                        data-id="${Id}"
                                        data-pac="${getNombreCompleto(Paciente)}"
                                        data-idpaciente="${Paciente.Id}"
                                        data-numerodocumento="${Paciente.NumeroDocumento}"
                                        data-cama="${Cama.Actual ? Cama.Actual.Valor : ''}"
                                        data-idubicacion="${Cama.Actual ? Cama.Actual.Ubicacion.Id : ''}"
                                        data-idcama="${Cama.Actual ? Cama.Actual.Id : ''}"
                                        data-fechahosp="${FechaIngreso}"
                                        data-ubicacion="${Cama.Actual ? Cama.Actual.Ubicacion.Valor : ''}"
                                        data-horahosp="${HoraIngreso}"
                                        data-idestado=${TipoEstadosSistemas.Id}
                                        class="btn btn-info btn-circle btn-lg"
                                        onclick="linkMostrarModalGestionPaciente(this);"
                                        data-toggle="tooltip" data-placement="left"
                                        title="Acciones sobre el paciente">
                                        <i class="fas fa-user"></i>
                                    </a>
                                    <br />
                                    <a
                                        data-id="${Id}"
                                        data-idpaciente="${Paciente.Id}"
                                        data-pac="${getNombreCompleto(Paciente)}"
                                        data-cama="${Cama.Actual ? Cama.Actual.Valor : ''}"
                                        data-fechaHosp="${FechaIngreso}"
                                        data-horaHosp="${HoraIngreso}"
                                        class="btn btn-info btn-circle btn-lg"
                                        onclick="linkMostrarModalGestionHospitalizacion(this);"
                                        data-toggle="tooltip" data-placement="left"
                                        title="Acciones sobre la hospitalizacion">
                                        <i class="fa fa-medkit"></i>
                                    </a>
                                    <br />`
                } else {

                    //BOTON EDITAR INGRESO ADMINISTRATIVO
                    if (EditarHospitalizacion) {
                        botones += dibujarBotonBandeja(
                            {
                                id: "linkEditarIngreso",
                                dataId: Id,
                                dataPacId: Paciente.Id,
                                dataPacName: getNombreCompleto(Paciente),
                                btnColor: "btn-success",
                                onclick: `irAEditarHospitalizacion(${Id})`,
                                title: "Editar Ingreso Administrativo",
                                icon: "fa-pencil-alt"
                            });
                    }

                    //BOTON FORMULARIOS EXTERNOS
                    if (VerFormulariosExternos)
                        //PERFIL MEDICO 
                        //87 = Hospitalización Pre-ingresada || 88 = Hospitalización Ingresada                           
                        botones += dibujarBotonBandeja(
                            {
                                id: "linkFormulariosExternos",
                                dataId: Id,
                                dataPacId: Paciente.Id,
                                dataPacName: getNombreCompleto(Paciente),
                                dataIdCama: Cama.Actual ? Cama.Actual.Id : '',
                                btnColor: "btn-primary",
                                onclick: "linkMostrarModalFormularios(this)",
                                title: "Formularios Externos",
                                icon: "fa-edit"
                            });

                    //VER MOVIMIENTOS DE LA HOSPITALIZACION 
                    if (VerMovimientosHospitalizacion)
                        botones += dibujarBotonBandeja(
                            {
                                id: "linkVerMovimientos",
                                dataId: Id,
                                btnColor: "btn-info",
                                onclick: "linkMostrarModalMovimientosHospitalizacion(this)",
                                title: "Movimientos Hospitalización",
                                icon: "fa-list"
                            });



                    //VER HISTORIAL HOSPITALIZACIONES DEL PACIENTE
                    botones +=
                        `<a id="linkHistorialHosp" data-idpac="${Paciente.Id}" data-nompac="${getNombreCompleto(Paciente)}"
                                class="btn btn-info btn-circle btn-lg" href="#/" onclick="linkHistorialHosp(this);" 
                                data-toggle="tooltip" data-placement="left" title="Historial de Hospitalizaciones">
                                <i class="fas fa-history"></i>
                            </a><br />`;

                }


                if (ImprimirHospitalizacion) {
                    botones += dibujarBotonBandeja(
                        {
                            id: "linkImprimirHospitalizacion",
                            dataId: Id,
                            btnColor: "btn-info",
                            onclick: "linkImprimirHospitalizacion(this)",
                            title: "Imprimir hospitalización",
                            icon: "fa fa-print"
                        });
                }

                //ANULAR 
                if (AnularHospitalizacion) {
                    botones += dibujarBotonBandeja(
                        {
                            id: "linkAnularHospitalizacion",
                            dataId: Id,
                            dataPacId: Paciente.Id,
                            dataPacName: getNombreCompleto(Paciente),
                            btnColor: "btn-danger",
                            onclick: "linkMostrarModalAnularHospitalizacion(this)",
                            title: "Anular",
                            icon: "fa-trash-alt"
                        });
                }

                return `${botones}</center></div></center></div>`;

            }
        },
        { responsivePriority: 1, targets: 0 }, // Id hosp
        { responsivePriority: 2, targets: 1 }, // rut  paciente
        { responsivePriority: 3, targets: 2 }, // nombre paciente
        { responsivePriority: 4, targets: 10 }, // Botonera
        { responsivePriority: 5, targets: 3 }, // Cama
        { responsivePriority: 6, targets: 4 }, // Ubicacion
        { responsivePriority: 7, targets: 10 }, // Estado
        { responsivePriority: 8, targets: 5 }, // fecha
        { responsivePriority: 9, targets: 6 }, // Hora
        { responsivePriority: 10, targets: 8 }, // Hora
        { responsivePriority: 11, targets: 9 }, // Aislamiento
        { responsivePriority: 11, targets: 7 }, // Dias
    ];
}

//Completa la tabla hospitalización
function getTablaHospitalizacion() {

    //Cambiar Ruta cuando este lista agregar ID HOS
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/Bandeja?` +
        `idTipoEstadosSistemas=${$("#ddlHosEstado").val()}` +
        `&idTipoHospitalizacion=${$("#ddlTipoHospitalizacion").val()}` +
        `&idIdentificacion=${($.trim($("#sltTipoIdentificacion").val()) !== "") ? $.trim($("#sltTipoIdentificacion").val()) : "0"}` +
        //`&idIdentificacion=${($.trim($("#sltTipoIdentificacion").val()) !== "") ? $.trim($("#sltTipoIdentificacion").val()) : "0"}` +
        `&idHospitalizacion=${($.trim($("#txtHosFiltroId").val()) !== "") ? $.trim($("#txtHosFiltroId").val()) : ""}` +
        `&numeroDocumento=${($.trim($("#txtHosFiltroRut").val()) !== "") ? $.trim($("#txtHosFiltroRut").val()) : ""}` +
        `&nombrePaciente=${($.trim($("#txtHosFiltroNombre").val()) !== "") ? $.trim($("#txtHosFiltroNombre").val()) : ""}` +
        `&apellidopaternoPaciente=${($.trim($("#txtHosFiltroApePaterno").val()) !== "") ? $.trim($("#txtHosFiltroApePaterno").val()) : ""}` +
        `&apellidomaternoPaciente=${($.trim($("#txtHosFiltroApeMaterno").val()) !== "") ? $.trim($("#txtHosFiltroApeMaterno").val()) : ""}` +
        `&idUbicacion=${$("#ddlUbicacion").val()}`;

    const columns = getColumnsTablaHospitalizacion();

    const columnDefs = getColumnsRenderTablaHospitalizacion();

    cargarDatatableAjax('#tblHospitalizacion', url, columns, columnDefs);
    $('[data-toggle="tooltip"]').tooltip();

    // SI NO ES PERFIL MEDICO NO SE PUEDEN CREAR NUEVAS HOJAS DE EVOLUCIÓN
    if (sSession.CODIGO_PERFIL != perfilAccesoSistema.medico && sSession.CODIGO_PERFIL != perfilAccesoSistema.internoMedicina)
        $("#linkNuevaHojaEvolucion").hide();

}

function dibujarBotonBandeja({ id, dataId, dataPacId, dataPacName, dataIdCama, btnColor, onclick, title, icon }) {
    return `
            <a id="${id}" data-id="${dataId}" 
                data-pacName="${dataPacName}" data-pacId="${dataPacId}"
                class="btn ${btnColor} btn-circle btn-lg"
                data-idcama ="${dataIdCama}"
                href="#/" onclick="${onclick};" data-toggle="tooltip" data-placement="left" 
                title="${title}">
                <i class="fa ${icon} nav-icon"></i>
            </a><br>`;
}

//async function MdlMovHosp(element) {

//    let estado = {}

//    setSession('ID_HOSPITALIZACION', $(element).data("id"))
//    setSession('ID_PACIENTE', $(element).data("idpaciente"))

//    //Trabajando aca
//    ocultarModal("mdlGestionPacienteEnfermeria");
//    $("#mdlGestionPacienteEnfermeria").on('hidden.bs.modal', async function (e) {

//        $("#mdlGestionPacienteEnfermeria").unbind()
//    })
//}

async function ActualizarCamasDisponibles() {

    let idUbicacion = parseInt($("#sltServicio").val())

    if (await servicioTieneCamaDisponible(idUbicacion)) {
        CargarCama(idUbicacion, '#sltCamaServicio')
        toastr.success("Se encontraron camas disponibles")
    }
    else {
        toastr.error("No hay camas disponibles")
    }
}

async function mensajeAsignarCama() {

    const result = await Swal.fire({
        title: `Paciente no tine cama asignada`,
        text: "¿Desea asignar una cama?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {

        window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoIngresoEnfermeria.aspx";
    }
}

async function getUbicaciones() {

    const url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;

    try {
        const ubicaciones = await $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json"
        });

        return ubicaciones;

    } catch (err) {
        console.error("Error al llenar ubicaciones");
        console.error(JSON.stringify(err));
        throw err; // Puedes lanzar la excepción nuevamente si deseas propagarla hacia arriba
    }
}

async function findUbicacion(ubicaciones, valor) {

    if (parseInt(valor) === 0)
        return

    const ubicacion = ubicaciones.find(u => u.Valor === valor)

    if (ubicacion === null)
        return

    return ubicacion
}



function setJsonExportarHOS(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarHOS.push([
            Object.HOS_idHospitalizacion,
            Object.GEN_numero_documentoPaciente,
            DevolverString(Object.GEN_nombrePaciente),
            Object.HOS_Paciente_Cama_Hospitalizacion,
            Object.HOS_fecha_ingreso_realHospitalizacion,
            //formatDate(Object.HOS_fecha_ingreso_realHospitalizacion),
            Object.HOS_hora_ingreso_realHospitalizacion,
            Object.HOS_diasHospitalizacion,
            Object.HOS_fecha_egresoEpicrisis,
            //formatDate(Object.HOS_fecha_egresoEpicrisis),
            Object.HOS_Aislamiento
        ]);
        //Indices de 0 a 8 fijos...
    });
}

function setJsonExportarIM(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarIM.push([
            Object.RCE_idIngreso_Medico,
            Object.GEN_numero_documentoPersonasPaciente,
            Object.GEN_nombrePersonasPaciente,
            Object.GEN_nombrePersonasProfesional,
            Object.GEN_nombreUbicacion,
            Object.RCE_fecha_horaIngreso_Medico
            //formatDate(Object.RCE_fecha_horaIngreso_Medico)
        ]);
        //Indices de 0 a 8 fijos...
    });
}



async function LlenarGrillaIngresoMedico(HOS_idHospitalizacion) {

    var datos = [];
    await $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico/${HOS_idHospitalizacion}/Hospitalizacion`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, val) {
                datos.push([
                    val.Id,
                    moment(moment(val.FechaHora).toDate()).format('DD-MM-YYYY HH:mm:SS'),
                    val.Diagnostico,
                    `${val.Profesional.Nombre} ${val.Profesional.ApellidoPaterno}`,
                    val.Ubicacion.Valor
                ]);
            });
        }
    });

    $("#tblIngresoMedico").addClass("text-wrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        data: datos,
        columns: [
            { title: "ID" },
            { title: "Fecha/Hora Ingreso" },
            { title: "Diagnóstico" },
            { title: "Profesional" },
            { title: "Ubicación" },
            { title: "", className: "text-center" }
        ],
        lengthMenu: [[5, 10], [5, 10]],
        columnDefs: [
            {
                targets: 2,
                sType: "date-ukLong"
            },
            {
                targets: -1,
                render: function (data, type, row, meta) {

                    var fila = meta.row;
                    let idIngresoMedico = datos[fila][0];
                    var botones =
                        `
                        <button class="btn btn-primary btn-circle" 
                                onclick="toggle('#div_accionesIngresoMedico${fila}'); return false;">
                                <i class="fa fa-list" style="font-size:13px;"></i>
                        </button>
                        <div id="div_accionesIngresoMedico${fila}" class="btn-container" style="display: none; z-index:1000;">
                              <center>
                              <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                              <div class="rounded-actions">
                                <center>
                                `;

                    botones += dibujarBotonEditarIngresoMedico(idIngresoMedico);

                    botones += dibujarBotonImprimirIngresoMedico(idIngresoMedico);
                    botones += dibujarBotonMovimientosIngresoMedico(idIngresoMedico);
                    botones += dibujarBotonMovimientosIngresoMedico(idIngresoMedico);
                    `</center>
                            </div>
                        </center>
                       </div>`
                        ;
                    return botones;

                }
            }
        ],

        responsive: true,

        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
        "bDestroy": true
    });

    $("#tblIngresoMedico").css("width", "100%");

}
function dibujarBotonMovimientosIngresoMedico(id) {
    return ` <a id="linkVerMovIngresoMedico" 
                data-id="${id}"
                class="btn btn-info btn-circle btn-lg"
                href="#/"
                onclick="linkMostrarModalMovimientosIngresoMedico(this);">
                    <i class="fa fa-list" data-toggle="tooltip" data-placement="left" title="Movimientos"></i>
            </a>`;
}
function dibujarBotonImprimirIngresoMedico(id) {
    return ` <a id="linkImprimirIngresoMedico" 
                data-id="${id}"
                class="btn btn-info btn-circle btn-lg"
                href="#/"
                onclick="linkImprimirIngresoMedico(this);"
                data-print="true" data-frame="#frameImpresion">
                    <i class="fa fa-print" data-toggle="tooltip" data-placement="left" title="Imprimir"></i>
            </a>`
}
function mostrarServicioTrasladoExterno(idUbicacion = 0) {
    let checked = $("#swTrasladoExterno").is(":checked");
    if (checked) {
        $('.external-input').show();
        $("#ddlServicioDestino").val('0');
        $("#ddlServicioDestino option[value='" + idUbicacion.toString() + "']").prop('disabled', true);
        $('#ddlCamaDestino').empty().append("<option value='0'>-Seleccione-</option>");
    } else {
        $("#ddlServicioDestino option[value='" + idUbicacion.toString() + "']").prop('disabled', false);
        $("#ddlServicioDestino").val(idUbicacion);
        CargarCama(idUbicacion, '#ddlServicioDestino');
        $('.external-input').hide();
    }
}

function llenarGrillaTraslado(bMostrarEditar) {

    var adataset = [];
    let json = {};
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_hospitalizacion/${$('#idHos').val()}/Traslados`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $('#txtPacienteTraslado').val(data[0].Paciente.nombre);
            $('#txtCamaActualTraslado').val(data[0].Hospitalizacion.cama);
            if (data.length > 0)
                json = data[0];

            $.each(data[0].Traslados, function (key, val) {
                adataset.push([
                    val.id,
                    data[0].Hospitalizacion.id,
                    val.tipo,
                    moment(moment(val.fecha).toDate()).format('DD-MM-YYYY HH:mm:ss'),
                    val.origen,
                    val.destino
                ]);
            });
            //LlenarGrillaTraslados(adataset, '#tblTrasladosPaciente', sSession);
            $('#tblTrasladosPaciente').addClass("nowrap").DataTable({
                data: adataset,
                columns: [
                    { title: "ID Traslados" },
                    { title: "IDHos" },
                    { title: "Tipo de Traslado" },
                    { title: "Fecha Traslado" },
                    { title: "Cama Origen" },
                    { title: "Cama Destino" },
                    { title: "" }
                ],
                "columnDefs": [
                    { "targets": 3, "sType": "date-ukLong" },
                    {
                        "targets": -1,
                        "visible": false,
                        "data": null,
                        orderable: false,
                        "render": function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones = '';
                            if (fila == adataset.length - 1)
                                //botones = "<a id='linkEditarTraslado' data-id='" + adataset[fila][0] + "' data-idorigen='" + adataset[fila][2] + "' data-iddestino='" + adataset[fila][3] + "' class='btn btn-info btn-block' href='#/' onclick='linkEditarTraslado(this)'>Editar</a>";
                                botones = "";
                            return botones;
                        }
                    },
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [-1],
                        "visible": (sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria && bMostrarEditar) ? true : false,
                        "searchable": false
                    }
                ],

                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
                "bDestroy": true
            });
        }
    });
    return json;
}

function GetJsonHospitalizacion(HOS_idHospitalizacion) {
    var json = null;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Mapa/DetalleHospitalizacion/${HOS_idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if (data.length > 0)
                json = data[0];
        }
    });

    return json;
}

function linkVerEgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = GetJsonHospitalizacion(HOS_idHospitalizacion);

    ShowModalAlerta("CONFIRMACION", "Confirmación",
        `   
            <div class="text-left" style="font-size:medium;">
                
                <h5 class="text-center mb-4"><strong>Información de la hospitalización</strong></h5>
                
                <div class="row">
                    <div class="col-md-6">
                        <strong>${json.GEN_nombreIdentificacion}:</strong><br /> 
                        ${json.GEN_numero_documentoPaciente}
                    </div>
                    <div class="col-md-6">
                        <strong>Fecha ingreso:</strong><br />
                        ${moment(json.HOS_fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <strong>Nombre:</strong><br />
                        ${json.GEN_PersonasnombrePaciente}
                    </div>
                </div>
                <label for="txtFechaEgreso">Fecha y hora de egreso</label>
                <div class="input-group mb-3">
                    <input id="txtFechaEgreso" type="date" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <input id="txtHoraEgreso" type="time" class="form-control clockpicker"/> 
                    </div>
                </div>
            </div>

            ¿Estás seguro qué quiere egresar a este paciente?
        `,
        function () { // Botón que egresa paciente

            var fechaActual = moment(GetFechaActual());
            var fechaEgreso = moment($("#txtFechaEgreso").val());
            var GEN_idPaciente = $(this).data("idPaciente");
            var esValido = true;

            if ($("#txtFechaEgreso").val() === "" || !fechaEgreso.isBefore(fechaActual)) {
                $("#txtFechaEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtFechaEgreso").removeClass('invalid-input');

            if ($("#txtHoraEgreso").val() === "") {
                $("#txtHoraEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtHoraEgreso").removeClass('invalid-input');

            if (esValido) {
                EgresarHospitalizacion(HOS_idHospitalizacion);
                $("#modalAlerta").modal('hide');
            }

        },
        function () { // Botón cancelar
            $("#modalAlerta").modal('hide');
        }, "Egresar", "Cancelar");

    $("#btnAlertaAceptar").data("idPaciente", json.GEN_idPaciente);

    $(".clockpicker").clockpicker({ autoclose: true });

}

function HayEpicrisis(GEN_idPaciente) {

    var hayEpicrisis = false;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Epicrisis/DetallePaciente/${GEN_idPaciente}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            hayEpicrisis = (data.length > 0);
        }
    });

    return hayEpicrisis;

}

function EgresarHospitalizacion(HOS_idHospitalizacion) {

    const json = {
        "HOS_fecha_egreso_realHospitalizacion": $("#txtFechaEgreso").val(),
        "HOS_hora_egreso_realHospitalizacion": $("#txtHoraEgreso").val()
    };

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Egreso/Enfermeria/${HOS_idHospitalizacion}`,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (data, status, jqXHR) {
            toastr.success("El paciente se ha egresado correctamente.");
            getTablaHospitalizacion();
        },
        error: function (jqXHR, status) {
            console.log("Error al egresar paciente: " + JSON.stringify(jqXHR));
        }
    });
}

