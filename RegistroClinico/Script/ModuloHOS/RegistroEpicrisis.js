﻿let idPaciente, idHospitalizacion, hospitalizacionEpicrisis, objetoEpicrisis = {}, editandoEpicrisis = false
let DiagnosticosAnteriores = [], MedicamentosAnteriores = [], CitacionAnterior = null, pediatrico = false, neonatal = false

$(document).ready(async function () {
    ShowModalCargando(false)
    sSession = getSession();
    idPaciente = sSession.ID_PACIENTE
    idHospitalizacion = sSession.ID_HOSPITALIZACION

    //Cuando los parametros de la sesion no existen
    if ((idPaciente == undefined || idPaciente == null) || (idHospitalizacion == undefined || idHospitalizacion == null))
        window.location.replace(`${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`);

    objetoEpicrisis.IdPaciente = sSession.ID_PACIENTE
    objetoEpicrisis.IdHospitalizacion = sSession.ID_HOSPITALIZACION

    deleteSession('ID_PACIENTE');
    deleteSession('ID_HOSPITALIZACION');
    //Cargar paciente
    CargarPacienteActualizado(idPaciente)
    //cargar hospitalizacion
    hospitalizacionEpicrisis = await getDatosHospitalizacionPorId(idHospitalizacion, true)
    //Manejo del formulario dependiendo la ubicacion del paciente.
    if (hospitalizacionEpicrisis.Ingreso !== null) {
        objetoEpicrisis.IdUbicacion = hospitalizacionEpicrisis.Ingreso.Ubicacion.Id
        //Uti pediatrica
        if (hospitalizacionEpicrisis.Ingreso.Ubicacion.Id == 174) 
            formularioUtiPediatrica()
        else if (hospitalizacionEpicrisis.Ingreso.Ubicacion.Id == 175 || hospitalizacionEpicrisis.Ingreso.Ubicacion.Id == 173) 
            formularioUtiNeonatologia()
    }
    //carga de combos
    cargarCombos()
    //Inicializacion de bootstrapswitch
    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#chkBorrador, #chkFallecido, #chkAcv").bootstrapSwitch('state', false);
    //inicializacion de control de usuario para ingreso de CIE10
    InicializarDatosDiagnostico("Content_wuc_diagnosticoEpicrisis", false);

    //calculo de la edad del paciente para su ingreso en epicrisis
    let edadPaciente = $("#txtEdadPac").val()
    edadPaciente = edadPaciente.split(" ")
    objetoEpicrisis.Edad = parseInt(edadPaciente[0])
    objetoEpicrisis.IdTipoEdad = 1
    //Esto pasa cuando la edad es en meses
    if (parseInt(edadPaciente[0]) == 0) {
        objetoEpicrisis.IdTipoEdad = 2,
        objetoEpicrisis.Edad = parseInt(edadPaciente[2])
    }

    promesaAjax("GET", `HOS_epicrisis/${idHospitalizacion}`).then(res => {
        if (res !== null && res !== undefined) {
            cargarEpicrisis(res)
        }
    }).catch(error => {
        console.error("ocurrio un error al buscar la epicrisis")
        console.log(error)
    })

})

function cargarEpicrisis(epicrisis) {
    objetoEpicrisis.IdEpicrisis = epicrisis.IdEpicrisis
    editandoEpicrisis = true
    console.log("epicrisis", epicrisis)
    /*Datos generales*/
    if (epicrisis.FechaIngreso!==null)
        $("#dpIngreso").val(epicrisis.FechaIngreso.split("T")[0])
    if (epicrisis.FechaEgreso !== null)
        $("#dpEgreso").val(epicrisis.FechaEgreso.split("T")[0])

    $("#chkBorrador").bootstrapSwitch('state',epicrisis.Borrador)
    $("#chkFallecido").bootstrapSwitch('state', epicrisis.PacienteFallecido)
    $("#chkAcv").bootstrapSwitch('state', epicrisis.Acv)

    $("#sltAmbito").val(epicrisis.DiagnosticoPrincipal)
    $("#sltAmbito").val(epicrisis.OtrosDiagnosticos)

    $("#txtResumenHospitalizacion").val(epicrisis.Resumen)
    $("#txtComplicaciones").val(epicrisis.Complicaciones)
    $("#txtCirugias").val(epicrisis.Cirugias)
    $("#txtProcedimientos").val(epicrisis.Procedimientos)
    $("#txtInfecciones").val(epicrisis.Infecciones)

    $("#txtReposo").val(epicrisis.Reposo)
    $("#txtRegimen").val(epicrisis.Regimen)
    $("#txtOtrasIndicaciones").val(epicrisis.OtrasIndicaciones)


    /*Inicio diagnosticos de epicrisis*/
    if (epicrisis.Diagnosticos.length > 0) {
        DiagnosticosAnteriores = epicrisis.Diagnosticos
        epicrisis.Diagnosticos.forEach(x => {
            agregarDiagnostico(x)
            setDataTipoClasificacion(x.Clasificacion.Id)
            clickAgregarDiagnostico()
        })
    }
    $("#txtDiagnosticoPrincipal").val(epicrisis.DiagnosticoPrincipal)
    $("#txtOtroDiagnostico").val(epicrisis.OtrosDiagnosticos)
    /*Inicio meiamentos de la epicrisis*/
    if (epicrisis.Medicamentos.length > 0) {
        MedicamentosAnteriores = epicrisis.Medicamentos
        epicrisis.Medicamentos.forEach(medicamento => {
            medicamentosSeleccionadosUc.push(medicamento)
            tablaMedicamentosUc.row.add(medicamento).draw()
        })
    }


    /*Inicio citacion epicrisis*/
    if (epicrisis.Citacion !== null) {
        CitacionAnterior = epicrisis.Citacion
        $("#divAvisoCitacion").addClass("d-none")
        $("#divBotonCitacion").addClass("d-none")

        $("#txtLugarCitacion").val(epicrisis.Citacion.Lugar)
        $("#dpFechaCitacion").val(epicrisis.Citacion.Fecha)
    }
    /*Epicrisis pediatrica*/
    if (epicrisis.Pediatrico !== null) {
        $("#txtPesoIngreso").val(epicrisis.Pediatrico.PesoIngreso)
        $("#txtPesoEgreso").val(epicrisis.Pediatrico.PesoEgreso)
        $("#txtDiasUci").val(epicrisis.Pediatrico.DiasUci)
    }
    /*Epicrisis neonatal*/
    if (epicrisis.Neonatal !== null) {
        $("#txtNombreMadre").val(epicrisis.Neonatal.NombreMadre)
        $("#sltEscolaridadMadre").val(epicrisis.Neonatal.TipoEscolaridadMadre)
        $("#txtDetalleEscMadre").val(epicrisis.Neonatal.DetalleEscolaridadMadre)
        $("#sltRhMadre").val(epicrisis.Neonatal.IdGrupoSanguineo)
        $("#txtNombrePadre").val(epicrisis.Neonatal.NombrePadre)
        $("#sltEscolaridadPadre").val(epicrisis.Neonatal.DetalleEscolaridadPadre??0)
        $("#sltDetalleEscPadre").val(epicrisis.Neonatal.TipoEscolaridadPadre??0)
        $("#sltTipoParto").val(epicrisis.Neonatal.IdTipoParto)
        $("#txtApgar").val(epicrisis.Neonatal.Apgar)
        $("#dpFechaBgc").val(epicrisis.Neonatal.FechaBgc)
        $("#dpFechaPku").val(epicrisis.Neonatal.FechaPku)
        $("#txtPesoNacimiento").val(epicrisis.Neonatal.PesoNacimiento)
        $("#txtPesoAlta").val(epicrisis.Neonatal.PesoAlta)
    }

    $("#sltAmbito").val(epicrisis.IdAmbito)
}

function updateProgressBar(porcentaje) {
    $(".progress-bar").css("width", porcentaje + "%");
}

function cargarCombos() {
    cargarComboAmbito()
}

function formularioUtiPediatrica() {
    pediatrico = true
    $("#divPediatria").show()
}

function formularioUtiNeonatologia() {
    neonatal = true
    $("#divNeonato").show()
    //GET api/GEN_Tipo_Escolaridad/Combo
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`, false, "#sltEscolaridadPadre")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`, false, "#sltEscolaridadMadre")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Parto/Combo`, false, "#sltTipoParto")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`, false, "#sltRhMadre")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`, false, "#sltRhRn")
}

function cargarComboAmbito() {
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Ambito/Combo`, false, "#sltAmbito")
}

function nextStep(button) {
    switch ($(button).data("tab")) {
        case "general":
            let valido = validarCampos("#divInfoGeneral")
            if (pediatrico)
                valido = validarCampos("#divPediatria")
            if (neonatal)
                valido = validarCampos("#divNeonato")

            if (valido) {
                $("#diagnostico-tab").removeClass('disabled')
                $("#diagnostico-tab").click()
                updateProgressBar(20)
                objetoEpicrisis.IdAmbito = $("#sltAmbito").val()
                objetoEpicrisis.FechaIngreso = $("#dpIngreso").val()
                objetoEpicrisis.FechaEgreso = $("#dpEgreso").val()
                objetoEpicrisis.Borrador = $("#chkBorrador").bootstrapSwitch('state');
                objetoEpicrisis.PacienteFallecido = $("#chkFallecido").bootstrapSwitch('state');
                objetoEpicrisis.Acv = $("#chkAcv").bootstrapSwitch('state');
            }
            break;
        case "diagnostico":
            if (validarCampos("#divInfoDiagnostico")) {
                if (getDiagnosticosRealizados().length > 0) {
                    $("#hosp-tab").removeClass('disabled')
                    $("#hosp-tab").click()
                    updateProgressBar(40)
                    objetoEpicrisis.Diagnosticos = []
                    objetoEpicrisis.Diagnosticos = getDiagnosticosRealizados().map(x => {
                        return { Alias: { Id: x.IdAlias }, Clasificacion: { Id: x.IdClasificacion } }
                    })
                    objetoEpicrisis.DiagnosticoPrincipal = $("#txtDiagnosticoPrincipal").val()
                    objetoEpicrisis.OtrosDiagnosticos = $("#txtOtroDiagnostico").val()
                } else
                    toastr.error("Debe seleccionar un diagnostico CIE10")
            }
            break;
        case "hospitalizacion":
            if (validarCampos("#divInfoHospitalizacion")) {
                $("#indicaciones-tab").removeClass('disabled')
                $("#indicaciones-tab").click()
                updateProgressBar(60)
                objetoEpicrisis.Resumen = $("#txtResumenHospitalizacion").val()
                objetoEpicrisis.Complicaciones = $("#txtComplicaciones").val()
                objetoEpicrisis.Cirugias = $("#txtCirugias").val()
                objetoEpicrisis.Procedimientos = $("#txtProcedimientos").val()
                objetoEpicrisis.Infecciones = $("#txtInfecciones").val()
            }
            break;
        case "indicaciones":
            if (validarCampos("#divInfoIndicaciones")) {
                $("#medicamentos-tab").removeClass('disabled')
                $("#medicamentos-tab").click()
                updateProgressBar(80)
                objetoEpicrisis.Reposo = $("#txtReposo").val()
                objetoEpicrisis.Regimen = $("#txtRegimen").val()
                objetoEpicrisis.OtrasIndicaciones = $("#txtOtrasIndicaciones").val()
            }
            break;
        case "medicamentos":
            if (obtenerMedicamentosSeleccionados().length == 0)
                toastr.warning("<i class='fa fa-info'> </i> &nbsp; <b>No se han seleccionado medicamentos.</b>")
            $("#citaciones-tab").removeClass('disabled')
            $("#citaciones-tab").click()
            updateProgressBar(100)
            objetoEpicrisis.Medicamentos = obtenerMedicamentosSeleccionados()
            
            break;
    }
}
function guardarCitacion() {
    if (validarCampos("#divIngresoCitacion")) {

        $("#txtLugarCitacion, #dpFechaCitacion").attr("disabled", true)
        objetoEpicrisis.Citacion = {
            Lugar: $("#txtLugarCitacion").val(),
            Fecha: $("#dpFechaCitacion").val()
        }
        $("#divAvisoCitacion").addClass("d-none")
        $("#divBotonCitacion").addClass("d-none")

        toastr.success("Citacion guardada")
    }
}
function guardarEpicrisis() {
    let metodoPeticion = "POST"
    if (editandoEpicrisis) {
        metodoPeticion = "PUT"
        objetoEpicrisis.Diagnosticos.forEach(x => {
            let diagnosticoAnterior = DiagnosticosAnteriores.find(y => x.Alias.Id == y.Alias.Id)
            if (diagnosticoAnterior !== null && diagnosticoAnterior !== undefined)
                x.IdDiagnosticoEpicrisis = diagnosticoAnterior.IdDiagnosticoEpicrisis
        })
        
        objetoEpicrisis.Medicamentos.forEach(medicamento => {
            if (medicamento.Arsenal) {
                /*Medicamento dentro de arsenal*/
                let medicamentoEncontrado = MedicamentosAnteriores.find(x => x.Id == medicamento.Id)
                if (medicamentoEncontrado !== null && medicamentoEncontrado !== undefined)
                    medicamento.IdMedicamentoEpicrisis = medicamentoEncontrado.IdMedicamentoEpicrisis
            } else {
                /*Medicamento fuera de arsenal*/
                let medicamentoEncontrado = MedicamentosAnteriores.find(x => x.medicamento == medicamento.medicamento)
                if (medicamentoEncontrado !== null && medicamentoEncontrado !== undefined)
                    medicamento.IdMedicamentoEpicrisis = medicamentoEncontrado.IdMedicamentoEpicrisis
            }
        })
    }
    if (pediatrico) {
        objetoEpicrisis.Pediatrico = {
            PesoIngreso: $("#txtPesoIngreso").val(),
            PesoEgreso: $("#txtPesoEgreso").val(),
            DiasUci: $("#txtDiasUci").val()
        }
    }
    if (neonatal) {
        objetoEpicrisis.Neonatal = {
            NombreMadre: $("#txtNombreMadre").val(),
            TipoEscolaridadMadre: $("#sltEscolaridadMadre").val(),
            DetalleEscolaridadMadre: $("#txtDetalleEscMadre").val(),
            IdGrupoSanguineo: $("#sltRhMadre").val(),
            NombrePadre: $("#txtNombrePadre").val(),
            TipoEscolaridadPadre: $("#sltEscolaridadPadre").val(),
            DetalleEscolaridadPadre: $("#sltDetalleEscPadre").val(),
            IdTipoParto: $("#sltTipoParto").val(),
            Apgar: $("#txtApgar").val(),
            FechaBgc: $("#dpFechaBgc").val(),
            FechaPku: $("#dpFechaPku").val(),
            PesoNacimiento: $("#txtPesoNacimiento").val(),
            PesoAlta: $("#txtPesoAlta").val(),
        }
    }
    
    console.log(objetoEpicrisis)
    debugger
    $.ajax({
        method: metodoPeticion,
        url: `${GetWebApiUrl()}HOS_Epicrisis`,
        data: objetoEpicrisis,
        success: function (data) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: "La epicrisis se guardo correctamente",
                showConfirmButton: false,
                timer: 3000
            }).then(result => {
                window.location.replace(`${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`);
            })
        }, error: function (error) {
            console.error("Ha ocurrido un error al intentar guardar la epicrisis")
            console.log(error)
        }
    })
}
function eliminarDiagnosticoEpicrisis(idDiagnosticoEpicrisis, button) {
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: "Está a punto de eliminar el registro de diagnóstico de esta epicrisis",
        showConfirmButton: true,
        showCancelButton: true
    }).then(result => {
        if (result.value == true) {
            $.ajax({
                method: "delete",
                url: `${GetWebApiUrl()}HOS_Epicrisis/${idDiagnosticoEpicrisis}/Diagnostico/Borrar`,
                success: function (data) {
                    let row = $(button).closest('tr'); 
                    table.row(row).remove().draw();
                    Swal.fire({
                        position: 'center',
                        icon: 'info',
                        title: "Diagnostico ha sido eliminado",
                        timer: 3000
                    })
                }, error: function (error) {
                    console.error("Ocurrio un error al eliminar el diagnostico de la epicrisis")
                    console.log(error)
                }
            })
        }
    })
}