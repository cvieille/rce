﻿let clasificado = [
    {
        id: 129,
        valor: "Paciente categorizado",
        camas: []
    },
    {
        id: 130,
        valor: "Paciente ausente",
        camas: []
    },
    {
        id: 131,
        valor: "Cama sin paciente",
        camas: []
    },
    {
        id: 132,
        valor: "Cama no categorizada",
        camas: []
    }

]

var elementosConColor = [
    { Elemento: "A1", color: "rgba(220, 53, 69, 0.8)" },
    { Elemento: "A2", color: "rgba(220, 53, 69, 0.6)" },
    { Elemento: "A3", color: "rgba(220, 53, 69, 0.4)" },
    { Elemento: "B1", color: "rgba(219, 166, 7, 0.8)" },
    { Elemento: "B2", color: "rgba(219, 166, 7, 0.6)" },
    { Elemento: "B3", color: "rgba(219, 166, 7, 0.4)" },
    { Elemento: "C1", color: "rgba(23, 162, 184, 0.8)" },
    { Elemento: "C2", color: "rgba(23, 162, 184, 0.6)" },
    { Elemento: "C3", color: "rgba(23, 162, 184, 0.4)" },
    { Elemento: "D1", color: "rgba(40, 167, 69, 0.8)" },
    { Elemento: "D2", color: "rgba(40, 167, 69, 0.6)" },
    { Elemento: "D3", color: "rgba(40, 167, 69, 0.4)" }
];


let conteo = {}
let graficoPorCategoria, camaSeleccionada, ubicacionSeleccionada, graficoPorRiesgo

$(document).ready(function () {
    ShowModalCargando(false)
    $("#dpFechaBuscar").val(moment(GetFechaActual()).format("YYYY-MM-DD"));
    CargarComboUbicacion();
})
function CargarComboUbicacion() {
    const url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, $("#sltUbicacion"));
}

function buscarAla() {
    if (validarCampos("#divFiltrosBuscarCama", true)) {

        conteo = {}

        clasificado.map(x=>x.camas=[])

        cerrarPopOvers()
        let idUbicacion = $("#sltUbicacion").val()
        let fechaFiltro = $("#dpFechaBuscar").val()
        url = `HOS_Categorizacion/mapa?fecha=${fechaFiltro}&idUbicacion=${idUbicacion}`
       
        ShowModalCargando(true)
        //Promesa que carga el mapa de camas
        promesaAjax("GET", url).then(async res => {
            let objeto = {
                IdUbicacion: $("#sltUbicacion").val(),
                NombreUbicacion: $('#sltUbicacion option:selected').text(),
                Alas: res
            }
            await dibujarUbicacion(objeto)

            let categoriasOrdenado = Object.keys(conteo).map(valor => ({ valor, cantidad: conteo[valor] }));

            inicialitarGraficoPie(clasificado)
            inicialitarGraficoDonuts(categoriasOrdenado)


        }).catch(error => {
            console.error("ha ocurrido un error al intentar obtener las camas de la ubicacion")
            console.log(error)
            ShowModalCargando(false)
        })
    }
}

function inicialitarGraficoPie(data) {
    
    // Obtener una referencia al elemento canvas en el HTML
    var ctx = document.getElementById('miGrafico').getContext('2d');
    

    if (graficoPorCategoria) {
        graficoPorCategoria.destroy();
    }
    var data = {
        labels: clasificado.map(x=>x.valor),
        datasets: [{
            data: clasificado.map(x=>x.camas.length),
            backgroundColor: [
                '#FAF060',
                '#525252',
                '#87A887',
                '#61CFFA'
            ] 
        }]
    };
    // Configurar las opciones del gráfico
    var options = {
        responsive: true,
        maintainAspectRatio: false
    };
    // Crear el gráfico de pastel
    graficoPorCategoria = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: options
    });
}


function inicialitarGraficoDonuts(data) {

    // Obtener una referencia al elemento canvas en el HTML
    var ctx = document.getElementById('graficoCategorias').getContext('2d');


    function asignarColorPorLabel(label) {
        var elemento = elementosConColor.find(function (elem) {
            return elem.Elemento === label;
        });

        return elemento ? elemento.color : 'rgba(0, 0, 0, 0.8)';
    }


    if (graficoPorRiesgo) {
        graficoPorRiesgo.destroy();
    }
    var data = {
        labels: data.map(x => x.valor),
        datasets: [{
            data: data.map(x => x.cantidad),
            backgroundColor: function (context) {
                var label = context.chart.data.labels[context.dataIndex];
                return asignarColorPorLabel(label);
            }
        }]
    };
    // Configurar las opciones del gráfico
    var options = {
        responsive: true,
        maintainAspectRatio: false
    };
    // Crear el gráfico de pastel
    graficoPorRiesgo = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: options
    });
}



function dibujarUbicacion(data) {
    ubicacionSeleccionada = data.IdUbicacion
    $("#contenidoUbicacion").empty()
    let contenido = ""
        contenido = `
            <div class="card card-primary">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" type="button"  data-target="Ubicacion-${data.IdUbicacion}" >
                   <h4 class="text-white"> ${data.NombreUbicacion} </h4>
                </button>
              </h5>
            </div>

            <div id="Ubicacion-${data.IdUbicacion}" class="collapse show" >
                <div id="accordionAlas">
                    ${dibujarContenidoAlas(data.Alas)}
                </div>
            </div>
         </div>`
    
    $("#contenidoUbicacion").append(contenido)
    ShowModalCargando(false)
}

function dibujarContenidoAlas(Ala) {
    let contenidoAlas = ``
    Ala.map(ala => {
        contenidoAlas += `
            <div class="card card-info">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseAla-${ala.Id}" aria-expanded="true" aria-controls="collapseAla-${ala.IdAla}">
                      <h5 class="text-white">${ala.Valor}</h5>
                    </button>
                  </h5>
                </div>

                <div id="collapseAla-${ala.Id}" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordionAlas">
                  <div class="card-body">
                    <div class="row">
                        ${dibujarContenidoHabitaciones(ala.Habitaciones) }
                    </div>
                  </div>
                </div>
             </div>  
            `
    })
    return contenidoAlas
}

function dibujarContenidoHabitaciones(Habitacion) {
    let contenidoHabitaciones = ``
    if (Habitacion.length == 0)
        contenidoHabitaciones = `<div class="col-md-12"><span><i class="fa fa-info-circle"></i>No se encontraron habitaciones</span></div>`
    else {
        Habitacion.map(habitacion => {
            let contenidoCamas = ``
            contenidoHabitaciones += `
                    <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <h5 class="text-white">${habitacion.Valor}</h5>
                                    </div>
                                    <div class="card=body">
                                        <div class="row">
                                            ${dibujarContenidoCama(habitacion.Camas)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
        })
    }
    return contenidoHabitaciones
}

function dibujarContenidoCama(Cama) {
    
    

    let contenidoCamas = ``
    if (Cama.length == 0)
        contenidoCamas = `<div class="col-md-12 p-2"><h5 class="text-center"><i class="fa fa-info-circle"></i>No se encontraron camas categorizadas</h5></div>`
    else {
        Cama.map(cama => {
            
            if (cama.Categorizacion !== undefined) {
                let categoriaCama = clasificado.find(x => x.id == cama.Categorizacion.Estado.Id)
                if (categoriaCama!==undefined)
                    categoriaCama.camas.push(Cama)

                let clave = cama.Categorizacion.RiesgoDependencia == "" ? "Sin categoria" : cama.Categorizacion.RiesgoDependencia
                
                if (conteo[clave]) {
                    conteo[clave]++;
                } else {
                    conteo[clave] = 1;
                }
            }


            let clase, icono, color, claseBs=``
            if (cama.Categorizacion != null) {
                switch (cama.Categorizacion.Estado.Id) {
                    //Estoy usando los mismos colores del mapa de camas para seguir con el estilo
                    case 129://Paciente categorizado
                        clase = `cama ocupada`
                        icono = `fa fa-user-circle`
                        
                        if (cama.Categorizacion.RiesgoDependencia !== null) {
                            switch (cama.Categorizacion.RiesgoDependencia.split("")[0]) {
                                case "A":
                                    //Red
                                    color = "#dc3545";
                                    claseBs = 'danger'
                                    break
                                case "B":
                                    //Yellow
                                    color = "#DBA607";
                                    claseBs = 'warning'
                                    break
                                case "C":
                                    //blue 
                                    color = "#17a2b8";
                                    claseBs = 'info'
                                    break
                                case "D":
                                    //green
                                    color = "#28a745";
                                    claseBs = 'success'
                                    break
                                default:
                                    color = "black";
                                    claseBs = 'dark'
                                    break
                            }
                        }
                        
                        break;
                    case 130://Paciente ausente
                        clase = `cama bloqueada`
                        icono = `fa fa-user-secret`
                    break
                    case 131://Cama sin paciente
                        clase = `cama disponible`
                        icono = `fa fa-times-circle`
                        break
                    case 132://Cama no categorizada
                        clase = `cama bloqueada`
                        icono = `fa fa-question-circle`
                        break;
                }
            }
            

            contenidoCamas += `
                    <div class="col-xl-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card card-warning">
                            <div class="card=body">
                                <div class="row p-1">
                                    <div class="col-md-12">
                                        <span class="text-center"><b>${cama.Valor}</b></span>
                                    </div>
                                    <div class="col-md-12">
                                    <div class='${clase} mx-auto activepopover'
                                    data-color="${claseBs}"
                                    onclick='mostrarCategorizacionPaciente(${JSON.stringify(cama)}, this)' >
                                        <img src='../../Style/img/bed.PNG'  width='40' draggable='false' />
                                        <i id='iCama_' class='${icono}' style='z-index:1; color:${color};'></i>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
        })
    }
    
    
    return contenidoCamas
}
function mostrarCategorizacionPaciente(cama, button) {
    
    let categorizacionHijo

    cerrarPopOvers()
    if (cama.Id !== camaSeleccionada) {
        ShowModalCargando(true)
        let popoverinfo = ``

        if (cama.Categorizacion.IdCategorizacion !== null) {
            promesaAjax("get", `HOS_categorizacion/Buscar?IdCategorizacion=${cama.Categorizacion.IdCategorizacion}`).then(async res => {
                if (cama.Categorizacion.Estado.Id == 129 || cama.Categorizacion.Estado.Id == 130) {
                    popoverinfo = await promesaAjax(`GET`, `GEN_Paciente/Buscar?idPaciente=${res[0].IdPaciente}`).then(async paciente => {
                        if (ubicacionSeleccionada !== undefined && ubicacionSeleccionada == 161) {
                            categorizacionHijo = await promesaAjax("GET", `HOS_Categorizacion/RecienNacido/${cama.Categorizacion.IdCategorizacion}`).then(response => {
                                
                                if (response.length > 0) {
                                    return (`
                                        ${response.map(cat => {
                                            return `
                                            <div class="col-md-12">
                                                <h5> <b>Categorización recién nacidos:</b></br></h5>
                                            </div>
                                            <div class="col-md-12  border-bottom  border-info">
                                                <h6><b>Paciente:</b> ${cat.Paciente.Nombre}</h5>
                                            </div>
                                            <div class="col-md-12  border-bottom  border-info">
                                                <h6><b>Categorización:</b> ${cat.Categorizacion}</h5>
                                            </div>
                                            `
                                        })}
                                    `)
                                }
                            }).catch(error => {
                                console.error("Ha ocurrido un error al buscar la categorizacion de los RN")
                                console.log(error)
                            })
                        }
                        
                        return (`<div class="card">
                                <div class="card-header bg-info">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4><b>Cama:</b> ${cama.Valor}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row p-3">
                                        <div class="col-md-12">
                                            ${(cama.Categorizacion.RiesgoDependencia !== "" && cama.Categorizacion.RiesgoDependencia !== null) ?
                                                            `<h2><span class="badge badge-${$(button).data("color")}">${cama.Categorizacion.RiesgoDependencia}</span></h2>` : ``}
                                        </div>
                                        <div class="col-md-12 border-bottom border-info"> <h6> <b>Rut:</b> ${paciente[0].NumeroDocumento}-${paciente[0].Digito} </h6> </div>
                                        <div class="col-md-12  border-bottom  border-info "> <h6><b>Paciente:</b> ${paciente[0].Nombre ?? ""} ${paciente[0].ApellidoPaterno ?? ""} </h6></div>
                                        ${cama.Categorizacion.Estado.Id == 129 ?
                                                            `<div class="col-md-12  border-bottom  border-info"> <h6> <b>Riesgo:</b>${res[0].NivelRiesgo !== null ? `${res[0].NivelRiesgo.Valor}` : `No informado`}  </h6></div>
                                            <div class="col-md-12  border-bottom  border-info"> <h6> <b>Dependencia:</b>${res[0].NivelDependencia !== null ? `${res[0].NivelDependencia.Valor}` : `No informado`} </h6></div>
                                            `
                                                            : `<div class="col-md-12  border-bottom  border-info"> <h6><b>PACIENTE NO CATEGORIZADO POR AUSENCIA.</b></h6></div>`}
                                        <div class="col-md-12  border-bottom  border-info"> <h6> <b>Registro por:</b> ${res[0].Profesional !== null ? `${res[0].Profesional.Persona.Nombre} ${res[0].Profesional.Persona.ApellidoPaterno}` : "Sin profesional registrado"} </h6></div>
                                        <div class="col-md-12  border-bottom  border-info"> <h6> <b>Fecha:</b> ${moment(res[0].Fecha).format("DD-MM-YYYY")} </h6> </div>
                                        ${categorizacionHijo !== undefined ? `${categorizacionHijo}` : ``}
                                    </div>
                                </div>
                            </div>`)

                    }).catch(error => {
                        console.error("ha ocurrido un error al intentar buscar un paciente por su id")
                        console.log(error)
                    })
                } else {
                    //Cama sin paciente
                    popoverinfo = `<div class="card">
                                   <div class="card-header bg-info">
                                        <h5><b>Cama:</b> ${cama.Valor}</h5>
                                   </div>
                                    <div class="card-body">
                                        <div class="row" >
                                            <div class="col-md-12 border-bottom  border-info"><h6> SE INDICO QUE NO HABIA PACIENTE EN CAMA </h6></div>
                                            <div class="col-md-12 border-bottom  border-info"><h6> <b>Registro por:</b> ${res[0].Profesional !== null ? `${res[0].Profesional.Persona.Nombre} ${res[0].Profesional.Persona.ApellidoPaterno}` : "Sin profesional registrado"}</h6></div>
                                            <div class="col-md-12 border-bottom  border-info"><h6> <b>Fecha:</b> ${moment(res[0].Fecha).format("DD-MM-YYYY")}</h6></div>  
                                        </div>
                                    </div>
                                </div>        
                                `
                }
                //Se deberia buscar el paciente solo cuando la cama es estado 129 o 130
                $(button).popover({
                    html: true,
                    placement: 'top',
                    content: popoverinfo,
                    template: '<div class="popover popover-xl"  role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
                })
                $(button).popover('show')

                camaSeleccionada = cama.Id
                ShowModalCargando(false)

            }).catch(error => {
                console.error("HA ocurrido un error al buscar la categorizacion")
                console.log(error)
            })

        }
    } else {
        camaSeleccionada  = null
    }
}
