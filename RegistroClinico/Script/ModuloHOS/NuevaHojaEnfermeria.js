﻿const MedicamentosPacienteArray = []

let signosVitalesIngresados = [], arrayCarteraArancel = [], EgresosHidricosIngresados = []
const actividades = {}
let TipoInvasivo = [], arregloVigilancias = [], vigilanciasAntiguas = [], IngresosHidricosIngresados = [], ManejoHeridas = []
let tieneRepresentante = false
let hayEscalasClinicas = false
let objetoPrevisionTramo = {}

//Arrays para API
let vigilancias = [], SignosVitalesApi = [], valoracionesApi = [], hidricosApi = []
let planCuidadosApi = [], vigilanciasApi = [], riesgosApi = [], RespuestasApi = [], planCuidados = [], ValoracionPsicosocial = []

const sesion = getSession()
let idTipoTurno = Number(sesion.TURNO)
let fechaActual = GetFechaActual()
let fechaHospital, ObservacionValoracion = null, ExamenFisicoValoracion = null
let cantidadHorarios = 0

$(document).ready(async function () {

    // Redimenciona segun el ancho de la pantalla algunos elemntos html
    inicioElementoResponsive()
    window.addEventListener('resize', inicioElementoResponsive)

    //Colapsar acordeones HE
    colapsarElementoDOM("#collapseUserControlPaciente", false)
    colapsarElementoDOM("#collapseUserControlAcompanante", false)
    colapsarElementoDOM("#collapseInfoBrazalete", false)
    colapsarElementoDOM("#collapseInfoAlergia", false)
    colapsarElementoDOM("#collapseInfoHospitalizacion", false)
    colapsarElementoDOM("#collapseInfoPesoTalla", false)

    //defineFechaMaximayMinimaPesoyTalla()

    $('[data-toggle="tooltip"]').tooltip()

    $("#ocultarTabsHojaEnfermeria").hide()

    if (sesion.ID_HOSPITALIZACION !== undefined) {

        //GET HOSPITALIZACION
        const hospitalizacion = await getDatosHospitalizacionPorId(sesion.ID_HOSPITALIZACION, true)

        const { DiagnosticoActual,
            Enfermeria,
            Paciente,
            PersonaResponsable,
            Ingreso:
            {
                Cama: { Valor: DescripcionCama },
                Ubicacion: { Valor: ValorUbicacion },
                Prevision,
                PrevisionTramo
            }
        } = hospitalizacion

        cargarPacientePorId(Paciente.Id)

        await BloquearPacienteEnfermeria()

        const personaResponsable = PersonaResponsable ?? null

        if (personaResponsable !== null)
            getDatosAcompaniantes(personaResponsable.Id)

        $("#sltTipoRepresentante").attr("disabled", false)

        //set data boton gestion aislamientos
        $("#btnGestionAislamientos").data("idpaciente", Paciente.IdPaciente)
        $("#btnGestionAislamientos").data("id", sesion.ID_HOSPITALIZACION)
        $("#btnGestionAislamientos").data("nombrepaciente", `${Paciente.Nombre} ${Paciente.ApellidoPaterno}`)
        $("#btnGestionAislamientos").data("cama", DescripcionCama)
        $("#btnGestionAislamientos").data("ubicacion", ValorUbicacion)

        $("#txtDiagnostico").val(DiagnosticoActual)
        $("#txtMorbidos").val(Enfermeria.AntecedentesMorbidos)

        comboPrevisionTramo(Prevision.Id)
        getPrevisionPrevisionTramo(Prevision.Id, PrevisionTramo.Id)

        //if (Enfermeria.PacienteDClinico !== null)
        //    $("#txtAlergiaHojaEnfermeria").val(Enfermeria.PacienteDClinico.Alergias ?? "")

        let nombrePaciente = `${Paciente.Nombre} ${Paciente.ApellidoPaterno} ${Paciente.ApellidoMaterno}`
        let htmlPrevision = $("#divPrevision").html();

        $("#divPrevision").empty()

        //Informacion para el modal de escalas clinicas
        $(".btnMdlEscalas").data("cama", DescripcionCama)
        $(".btnMdlEscalas").data("pac", nombrePaciente)
        $(".btnMdlEscalas").data("id", sesion.ID_HOSPITALIZACION)
        //Modal signos vitales
        $("#lblPacienteSignosVitales").text(nombrePaciente)

        //FIN GET HOSPITALIZACION

        inicializarCombo()
        //Llenar la hoja aca
        $("#recetasIndicacionesMedicas").trigger("click")
        $("#Valoracion-tab").trigger("click")

        $("#cardApoyoIntegral").hide()
        $("#cardObsRecienNacido").hide()

        $("#divDestinoProfesional").hide()
        $("#alertPlanCuidadoEnfermeria").show()

        //GET BRAZALETE
        //getBrazaletePaciente()
        let brazaletes = await getBrazaletePaciente(sesion.ID_HOSPITALIZACION)
        dibujarDataTableBrazalete("#tblBrazaleteHojaEnfermeria", brazaletes, true)

        //GET ALERGIAS

        //await getAlergiasPaciente(Paciente.IdPaciente)

        //GET TIPO PESO / TALLA

        await getHojaEnfermeriaPeso()
        await getHojaEnfermeriaTalla()

        //GET EVOLUCION

        await getHojaEnfermeriaEvolucion()

        linkMostrarModalAlergias(null, sesion.ID_HOSPITALIZACION)

        //GET SIGNOS VITALES

        await dibujarHistorialSignosVitalesTable()

        //GET EXAMENES 
        await getExamenesHojaEnfermeria()

        //GET TIPO INVASIVOS
        getInvasivosHojaEnfermeria()

        //GET TABLAS
        //getInformacionClinicadePaciente()

        // GET MEDICAMENTOS
        await getMedicamentosPaciente()

        //GET HISTORIAL BALANCE HÍDRICO
        await dibujarHistorialBalanceHidrico()

        // GET HISTORIAL RIESGOS Y VALORACIONES
        await dibujarHistorialRiesgosValoraciones()

        // GET PLAN CUIDADOS
        await getTipoCuidado()

        ShowModalCargando(false)

        $("#txtEvolucionAtencion").text("")
        $("#txtObservacionesExamenes").text("")

        //OCULTA TODAS LAS PESTAÑAS EXCEPTO HOJA GENERAL
        $("#hojaEnfermeriaNav a:not(#hojaGeneral-tab)").hide();
        $("#modalCargando").hide()
    }

    $("#sltIdTipoPlano").attr("disabled", true)

    $("#aIngresosVitales").on("click", function () {
        mostrarSignosVitales(this)
    })

    arrayCarteraArancel = GetJsonCartera();
    //InicializarTypeHeadExmanenesHE()

    if (sesion.ID_HOJA_ENFERMERIA !== undefined) {

        mostrarFechaHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA)

        $("#btnCargarSignos").data("id", sesion.ID_HOJA_ENFERMERIA)
        $("#btnCargarSignos").data("tipo", "hojaenfermeria")
        $("#btnCargarSignos").removeClass("d-none")


        $("#sltTipoHojaEnfermeria").prop("disabled", true)
        $("#btnIngresoNuevaHojaEnfermeria").html("Actualizar")

        inicializarCombo()

        await llenarHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA)
        comboActividadTipoCuidado($("#sltTipoHojaEnfermeria").val())

        $("#txtDiagnostico").removeAttr("disabled")
        $("#txtMorbidos").removeAttr("disabled")
        mostrarNumeroHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA, sesion.ID_HOSPITALIZACION)
        $("#ocultarTabsHojaEnfermeria").show()

        //MUESTRA PESTAÑA SEGUN SELECCION 
        $('#sltTipoHojaEnfermeria').change(function () {

            if ($(this).val() === "0") {
                $("#ocultarTabsHojaEnfermeria").hide()
                return
            }
            // OCULTA PESTAÑA HOJA ENFERMERIA GENERAL 
            if ($(this).val() === "1") {
                $("#hojaEnfermeriaNav a:not(#hojaGeneral-tab)").hide()
                $("#ocultarTabsHojaEnfermeria").show()
                return
            }
            else {
                $("#hojaEnfermeriaNav a:not(#hojaGeneral-tab)").hide();
            }

            $("#ocultarTabsHojaEnfermeria").show()

            const navRCEnfermeria = $(`a[data-id-formulario="${$(this).val()}"]`)
            $(navRCEnfermeria).show()
        })
    }
    else {
        window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
    }

    //Inicializar vigilancia
    vigilancia()
    tableHidricos()
    ActivarAcompañante()
    setTimeout(function () {
        $("#chkAcompañante").bootstrapSwitch('disabled', false);
    }, 800)

    $("#chkAcompañante").on('switchChange.bootstrapSwitch', function (event, state) {
        if ($(this).bootstrapSwitch('state'))
            tieneRepresentante = true
        else
            tieneRepresentante = false
    })

    textboxio.replace("#txtExamenFisicoValoracion").content.set(
        `   <b>Cabeza: </b><br />
            <b>Cuello: </b><br />
            <b>Tórax: </b><br />
            <b>Abdomen: </b><br />
            <b>Columna: </b><br />
            <b>Genitales: </b><br />
            <b>Extremidad Superior: </b><br />
            <b>Extremidad Inferior: </b>`
    );

    $("#btnIngresoNuevaHojaEnfermeria").click(function (e) {
        e.preventDefault()
        edicionNuevaHojaEnfermeria(sesion.ID_HOJA_ENFERMERIA)
        //deleteSession('ID_HOJA_ENFERMERIA')
        //deleteSession('ACCION')
    })


    //Antecedentes Qx
    $("#txtAntecedentesQx").hide()
    $("input:radio[name='radioAntecedentesQx']").change(function () {

        if ($(this).val() == "SI") {
            $("#txtAntecedentesQx").show()
            $("#divInvalidInputAQx").removeClass("invalid-input")
            $("#txtAntecedentesQx").attr("data-required", true)
        }
        else {
            $("#txtAntecedentesQx").hide()
            $(this).attr("data-required", false)
            $("#txtAntecedentesQx").removeAttr("data-required")

            if ($("#divAntecendetesQx span").hasClass("invalid-input"))
                $("#divAntecendetesQx span").remove()
            $("#txtAntecedentesQx").val("")
        }
    })
})

async function BloquearPacienteEnfermeria() {

    $("#sltIdentificacion").attr("disabled", true)
    $("#txtnumerotPac").attr("disabled", true)
    $("#txtDigitoPac").attr("disabled", true)
}

async function getAlergiasPaciente(idPaciente) {

    let alergias = await getPacienteTipoAlergia(idPaciente)

    let headerHE = [{ title: 'Alergia', data: 'valor' }]
    let tabla = $("#tblAlergiaHojaEnfermeria").DataTable({
        columns: headerHE
    })


    const adataset = alergias.map(x => {
        return {
            valor: x.Alergia.Valor
        }
    })

    tabla.clear()
    tabla.rows.add(adataset).draw()
}

// arreglo listaClases ejemplo: ['col-lg-12', 'mt-4'];
function AnchoElementoResposponsive(ancho, elemento, listaClases) {
    const element = document.querySelector(elemento); //".clase"
    if (innerWidth < ancho) {
        element.classList.add(...listaClases);
    } else {
        element.classList.remove(...listaClases);
    }
}

function inicioElementoResponsive() {

    AnchoElementoResposponsive(1460, ".resize-riesgo", ['col-lg-12', 'mt-4']) // responsive riesgo
    AnchoElementoResposponsive(1199, ".resize-pac", ['col-lg-12']) // responsive control usuario pac
}

function getInvasivosHojaEnfermeria() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${sesion.ID_HOSPITALIZACION}/Invasivos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            TipoInvasivo = data

            vigilanciasAntiguas = TipoInvasivo
            dibujarHistorialInvasivos(vigilanciasAntiguas)
            //CARGA FECHA ACTUAL
            $("#dtFechaVigilancia").val(moment(GetFechaActual()).format("YYYY-MM-DD"))
            $("#dtFechaVigilancia").attr("disabled", true)

        }, error: function (err) {
            console.log("Error al buscar los Riesgos y medidas de seguridad" + err)
        }
    })
}

$("#aGuardarSignoVital").click(function (e) {
    e.preventDefault()

    if (validarCamposRegex("#divSignosVitales", false)) {
        toastr.error("Debe ingresar valores válidos")
        return false
    }

    let input = $("#divSignosVitales").find("input")
    //No han sido ingresado datos 

    for (let i = 0; i < input.length; i++) {
        if (input[i].value != "") {
            signosVitalesIngresados.push({ Id: input[i].name, Nombre: input[i].placeholder, Valor: input[i].value })
        }
    }

    //Dibujar signos vitales
    dibujarSignosVitales();
    $("#mdlSignosVitales").modal("hide");
})

function getInvasivosHojaEnfermseria() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${sesion.ID_HOSPITALIZACION}/Invasivos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            TipoInvasivo = data
            vigilanciasAntiguas = TipoInvasivo
            dibujarHistorialInvasivos(vigilanciasAntiguas)
            //CARGA FECHA ACTUAL
            $("#dtFechaVigilancia").val(moment(GetFechaActual()).format("YYYY-MM-DD"))
            $("#dtFechaVigilancia").attr("disabled", true)

        }, error: function (err) {
            console.log("Error al buscar los Riesgos y medidas de seguridad" + err)
        }
    })
}

//function defineFechaMaximayMinimaPesoyTalla() {
//    const fechaMaxima = moment(fechaActual).format("YYYY-MM-DD")
//    $("#tomaFechaPeso").prop('max', fechaMaxima)
//    $("#tomaFechaAltura").prop('max', fechaMaxima)
//}
async function validaHojaEnfermeria() {

    let valido = true

    if (!validarCampos("#divAcompañante", true)) {
        valido = false
        return
    }

    else if (!validarCampos("#hojaEnfermeriaValidacion", true)) {
        valido = false
        return
    }

    else if (!validarCampos("#validaHorariosPlan", true)) {
        valido = false
        return
    }
    else if (!validarCampos("#validaHorariosMedicamentos", true)) {
        valido = false
        return
    }
    else if (!validarCamposInputRadio("#divValoracion", true)) {
        valido = false
        return
    }
    else if (!validarCampos("#divInputValoracion", false)) {
        valido = false
        return
    }
    else if (!validarCamposInputRadio("#divRiesgos", false)) {
        valido = false
        return
    }

    return valido

    //if (validarCampos("#hojaEnfermeriaValidacion", true) === false
    //    && validarCampos("#validaHorariosPlan", true) === false
    //    && validarCampos("#validaHorariosMedicamentos", true) === false)
    //    return false
    //return true

    //let esValido = true

    //$("#hojaEnfermeriaNav a").each(async function (index, element) {
    //    if ($(element).is(":visible")) {
    //        //await sleep(250)
    //        const href = $(element).attr("href")
    //        //$(`a[href="${href}"]`).trigger("click")
    //        if (!validarCampos(href, true)) {
    //            esValido = false
    //            return false
    //        }
    //    }
    //})

    //return esValido

}

async function edicionNuevaHojaEnfermeria(id = 0) {

    const valido = await validaHojaEnfermeria()

    if (valido) {

        //Validacion de los riesgos o las valoraciones... false = faltan campos por completar.
        let validacion = ExtraerDatosApi()
        let coincidencia = validacion.find(e => e.Validacion == false) ?? true

        let fecha = ""
        let idTipoHoja = ""
        let json = {}
        let idTipoRepresentante = null

        //Se obtiene un unico id del tipo de hoja de enfermeria 
        const idTipoHojaEnfermeria = $('#sltTipoHojaEnfermeria option').map(function () {
            if ($("#sltTipoHojaEnfermeria").val() === this.value)
                return parseInt(this.value)
        }).get()

        fecha = moment($("#txtFechaHojaEnfermeria").val()).format("YYYY-MM-DD HH:mm:ss")
        idTipoHoja = idTipoHojaEnfermeria[0]

        idTipoHoja = $("#sltTipoHojaEnfermeria").val()

        if ($("#sltTipoRepresentante").val() !== "0")
            idTipoRepresentante = ($("#sltTipoRepresentante").val())

        ManejoHeridas = ManejoHeridas.map(x => {
            delete x.LocalizacionLPPText
            delete x.ClasificacionHeridaText
            delete x.TipoPlanoManejoHeridaText
            return x
        })
        let valoracion = {
            "ObservacionValoracion": ObservacionValoracion,
            "ExamenFisicoValoracion": ExamenFisicoValoracion,
        }
        RespuestasApi = [...RespuestasApi.flat()]
        //arrayBrazalete = getArrayBrazalete(arrayBrazaleteHosp)
        //arrayBrazalete = getArrayBrazalete(arrayBrazaleteHosp)

        vigilanciasApi = vigilanciasApi.map(x => {
            if (x !== null)
                delete x.IdHojaEnfermeriaCierra
            return x
        })

        json = {
            Id: id,
            Fecha: fecha,
            /*"IdPersonaResponsable": IdAcompañante ?? null, // depende de control de usuario DatosAcompañante.ascx*/
            IdHospitalizacion: sesion.ID_HOSPITALIZACION,
            Diagnostico: valCampo($("#txtDiagnostico").val()),
            Morbidos: valCampo($("#txtMorbidos").val()),
            AntecedentesQx: valCampo($("#txtAntecedentesQx").val()),
            MedicamentosHabituales: $("#txtMedicamentosHabituales").val(),
            OtrosProcedimientos: valCampo($("#txtOtrosProcedimientos").val()),
            IdTipoHojaEnfermeria: valCampo(Number(idTipoHoja)),
            IdTipoTurno: idTipoTurno,
            SignosVitales: SignosVitalesApi,
            //"TipoCuidado": arrayHorariosPlan,//planCuidadosApi,
            TipoInvasivo: vigilanciasApi,
            //"TipoPeso": pesosApi,
            //"TipoTalla": tallasApi,
            TipoBalance: hidricosApi,
            //"Evolucion": valCampo($("#txtEvolucionAtencion").val()),
            //"Examenes": ExamenesLabApi,
            ComplementoExamen: valCampo($("#txtObservacionesExamenes").val()),
            RespuestasItem: RespuestasApi,
            LocalizacionHeridas: ManejoHeridas,
            Medicamentos: arrayHorariosMed
        };

        if (!tieneRepresentante) {
            json.IdPersonaResponsable = null
            json.IdTipoRepresentante = null
        }
        else {
            const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())
            await getAcompanianteEnfermeria(personaAcompaniante, json)
        }

        await putHojaEnfermeria(id, json)
    }
    else {
        toastr.error("Faltan campos obligatorios que rellenar")
    }
}

function putHojaEnfermeria(id, json) {

    const parametrizacion = {
        type: "PUT",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
        mensaje: `Se actulizó Hoja N° ${id}`
    }

    $.ajax({
        type: parametrizacion.type,
        url: parametrizacion.url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            toastr.success(parametrizacion.mensaje)
            ShowModalCargando(false)
            window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
        },
        error: function (err) {
            //toastr.error("Error en la actualización")
            console.log(JSON.stringify(err))
        }
    })
}

async function getAcompanianteEnfermeria(personaAcompaniante, json) {

    if (personaAcompaniante === null) {

        const nuevaPersonaAcompaniante = await formatJsonPersonaAcompaniante()

        let acompaniante = await guardarPersonaAcompaniante(nuevaPersonaAcompaniante)

        if (acompaniante !== null) {
            json.IdPersonaResponsable = acompaniante.Id
            json.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
        }
        else {
            json.IdPersonaResponsable = null
            json.IdTipoRepresentante = null
        }
    }
    else {
        json.IdPersonaResponsable = personaAcompaniante.Id
        json.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
    }
}

function getValoracionPsicosocial(id, sender) {
    //id = 11 es APOYO INTEGRAL

    let boton = $(sender)
    let card = $("#cardApoyoIntegral")

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/4/${id}/Item`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if (id == 11) {
                dibujaValoracionPsicosocial(data)
            }
        }, error: function (err) {
            console.log("Error al buscar los Riesgos y medidas de seguridad" + err)
        }
    })

    if (boton.attr("data-click") === "true") {
        boton.addClass("btn-outline-warning")
        boton.removeClass("btn-outline-success")
        boton.html("Cancelar Apoyo Integral")
        card.show()
        boton.attr("data-click", false)

    }
    else {
        boton.addClass("btn-outline-success")
        boton.removeClass("btn-outline-warning")
        boton.html("Agregar Apoyo Integral")
        card.hide()
        boton.attr("data-click", true)
    }
}

function getObservacionRecienNacido(sender) {
    let boton = $(sender)
    let card = $("#cardObsRecienNacido")

    dibujaObservacionesRecienNacido()

    if (boton.attr("data-click") === "true") {
        boton.addClass("btn-outline-warning")
        boton.removeClass("btn-outline-success")
        boton.html("Cancelar Observación Recien Nacido")
        card.slideDown()
        boton.attr("data-click", false)

    }
    else {
        boton.addClass("btn-outline-success")
        boton.removeClass("btn-outline-warning")
        boton.html("Agregar Observación Recien Nacido")
        card.slideUp()
        boton.attr("data-click", true)
    }
}

function dibujaValoracionPsicosocial(data) {
    document.getElementById('titulo-vpsicosocial').innerHTML = '';
    document.getElementById('cardPsicosocial').innerHTML = '';

    datos = data[0].Item.SubItem;

    let contenido = "", alternativas = "", tipoInput = "";

    datos.map((e) => {

        titulo = `<div class="col-lg-12"><h3>${e.DescripcionItem}</h3></div>`

        e.Preguntas.map(b => {
            //Inicio Map Alternativas
            if (b.Alternativas.length > 0) {
                alternativas = "";

                b.Alternativas.map(c => {
                    alternativas += `
                            <input class="form-check-input" value="${c.IdRespuesta}"
                            type="radio" name="${b.IdPregunta}" data-idgrupo="${b.IdPregunta}" data-required="true">
                            <label class="form-check-label" for="${c.DescripcionAlternativa}">
                            ${c.DescripcionAlternativa}
                            </label>
                            </br>`

                })
            }
            //Fin Map Alternativas
            contenido += `
             <div class="col-md-3">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5>${b.DescripcionPregunta}</h5>
                    </div>
                    <div class="card-body" id="">
                        <div class="form-check" id="body-${b.IdPregunta}" style="min-height:60px;">
                               ${alternativas}
                        </div>
                    </div>
                </div>
            </div>`
        })



    })
    $("#titulo-vpsicosocial").append(titulo)
    $("#cardPsicosocial").append(contenido)

    //DERIVACION SI
    $("input:radio[name='49']:first").change(function () {

        $("#txtDestinoDerivacion").attr("data-required", true)
        $("#txtProfesionalDerivacion").attr("data-required", true)

        ReiniciarRequired()
        $("#divDestinoProfesional").show()
    })

    //DERIVACION NO
    $("input:radio[name='49']:last").change(function () {

        $("#txtDestinoDerivacion").removeAttr("data-required")
        $("#txtProfesionalDerivacion").removeAttr("data-required")
        ReiniciarRequired()
        $("#divDestinoProfesional").hide()
    })
}

function dibujaObservacionesRecienNacido() {
    document.getElementById('titulo-obsRecienNacido').innerHTML = '';
    document.getElementById('cardobsRecienNacido').innerHTML = '';

    datos = `[
        {
            "IdItem": 13,
            "DescripcionItem": "OBSERVACIONES RECIEN NACIDO",
            "Preguntas": [
                {
                    "IdPregunta": 100,
                    "DescripcionPregunta": "Traslado",
                    "MultipleSeleccion": false,
                    "Alternativas": [
                        {
                            "IdRespuesta": 267,
                            "IdAlternativa": 4,
                            "DescripcionAlternativa": "UPC NEO"
                        },
                        {
                            "IdRespuesta": 268,
                            "IdAlternativa": 5, 
                            "DescripcionAlternativa": "ALOJ CONJUNTO"
                        },
                        {
                            "IdRespuesta": 268,
                            "IdAlternativa": 5,
                            "DescripcionAlternativa": "OTRO"
                        }
                    ]
                },
                {
                    "IdPregunta": 101,
                    "DescripcionPregunta": "Caso Social",
                    "MultipleSeleccion": false,
                    "Alternativas": [
                        {
                            "IdRespuesta": 269,
                            "IdAlternativa": 4,
                            "DescripcionAlternativa": "SI"
                        },
                        {
                            "IdRespuesta": 270,
                            "IdAlternativa": 5,
                            "DescripcionAlternativa": "NO"
                        }
                    ]
                },
                {
                    "IdPregunta": 102,
                    "DescripcionPregunta": "Medida de protección",
                    "MultipleSeleccion": false,
                    "Alternativas": [
                        {
                            "IdRespuesta": 271,
                            "IdAlternativa": 4,
                            "DescripcionAlternativa": "SI"
                        },
                        {
                            "IdRespuesta": 272,
                            "IdAlternativa": 5,
                            "DescripcionAlternativa": "NO"
                        }
                    ]
                }
            ]
        }
    ]`
    obj = JSON.parse(datos);

    let contenido = "", alternativas = "", tipoInput = "";

    obj.map((e) => {

        titulo = `<div class="col-lg-12"><h3>${e.DescripcionItem}</h3></div>`

        e.Preguntas.map(b => {
            //Inicio Map Alternativas
            if (b.Alternativas.length > 0) {
                alternativas = "";

                b.Alternativas.map(c => {
                    alternativas += `
                            <input class="form-check-input" value="${c.IdRespuesta}"
                            type="radio" name="${b.IdPregunta}" data-idgrupo="${b.IdPregunta}" data-required="true">
                            <label class="form-check-label" for="${c.DescripcionAlternativa}">
                            ${c.DescripcionAlternativa}
                            </label>
                            </br>`
                })
            }
            //Fin Map Alternativas
            contenido += `
             <div class="col-md-3">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5>${b.DescripcionPregunta}</h5>
                    </div>
                    <div class="card-body" id="">
                        <div class="form-check" id="body-${b.IdPregunta}" style="min-height:70px;">
                               ${alternativas}
                        </div>
                    </div>
                </div>
            </div>`
        })
    })
    $("#titulo-obsRecienNacido").append(titulo)
    $("#cardobsRecienNacido").append(contenido)
}

function dibujaHistorialValoracionPsicosocial(data) {

    if (data.length > 0) {
        let contenido = ""
        let arrValoracionPsicosocial = []
        const arr = data.filter(x => x.IdItem == 11).find(x => x)
        arrValoracionPsicosocial.push(arr)

        if (arrValoracionPsicosocial.length > 0) {

            $("#addApoyoIntegralMatroneria").attr("disabled", true)
            $("#addApoyoIntegralMatroneria").removeClass("btn-outline-success")
            $("#addApoyoIntegralMatroneria").addClass("btn-outline-secondary")

            arrValoracionPsicosocial.map(e => {

                let fecha = moment(e.Fecha).format("LLLL")

                contenido += `<a
                            class="btn btn-outline-primary m-2"
                            type="button"
                            data-toggle="collapse"
                            href="#collapsevpsicosocial${e.IdHojaEnfermeriaRespuesta}"
                            role="button"
                            aria-expanded="false"
                            data-fecha="${fecha}"
                            id="${e.IdHojaEnfermeriaRespuesta}"
                            onclick="getHistorialValoracionPsicosocial(this,${e.IdHojaEnfermeriaRespuesta})"
                          >
                           ${fecha}
                          </a>`

            })
            arrValoracionPsicosocial.map(e => {
                contenido += `<div class="collapse" id="collapsevpsicosocial${e.IdHojaEnfermeriaRespuesta}">
                            <div class="card card-body">
                                <div class="row" id="cardVpsicosocial${e.IdHojaEnfermeriaRespuesta}">
                            </div>
                          </div>
                         </div>`
            })
        }

        $("#botonesHistorialVpsicosocial").append(contenido)
    }
}

function dibujaObservacionRecienNacido(data) {

}

function getHistorialValoracionPsicosocial(sender, id) {

    let boton = $(sender)
    boton.bind()
    let fecha = boton.data("fecha")

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/${id}/RespuestaDetalle`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            dibujarHistorialValoracionPsicosocial(data, id, fecha)

        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
}

function dibujarHistorialValoracionPsicosocial(data, id, fecha) {

    let card = ""
    let contenido = ""
    let divCardVpsicosocial = `#cardVpsicosocial${id}`

    $("#HistorialApoyoIntegral").empty()
    $(divCardVpsicosocial).empty()

    contenido += `  <div class="col-md-12 text-left mb-2">
                        <span><strong>${fecha}</strong></span>
                    </div>`

    if (id > 0) {

        data.filter(a => {

            a.Preguntas.map(b => {

                card = ""
                b.Alternativas.map(c => {
                    card += `<span class="mb-2"><strong>${c.DescripcionAlternativa}</strong></span>`
                })

                contenido += `  <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-header card bg-warning">
                                            <span><strong>${b.DescripcionPregunta}</strong></span>
                                        </div>
                                        <div class="card-body">
                                            ${card}
                                        </div>
                                    </div>
                                </div>`
            })
        })
    }

    $(divCardVpsicosocial).append(contenido)
}

function guardarValoracionoPsicosocial() {

    let Respuestas = []
    let inputRadio = $("#cardPsicosocial").find("input[type='radio']:checked")

    for (let item of inputRadio) {
        Respuestas.push(parseInt(item.value))
    }

    if (Respuestas.length > 0) {

        ValoracionPsicosocial.push({ "IdItem": 11, "Respuestas": Respuestas })
    }
    return ValoracionPsicosocial
}

function showEscalasClinicasPDF(json) {

    let escalasPDF = ""

    let urlPDF = `${GetWebApiUrl()}GEN_Escala_Clinica/Bandeja?idTipoEscala=${json.idTipoEscala}&idHospitalizacion=${sesion.ID_HOSPITALIZACION}`

    escalasPDF += `<div class="accordion col-lg-12" id="accordionPdfItemRiesgo-${json.idTipoEscala}">
                            <div class="card">
                                <div class="card-header" id="PdfItemRiesgo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                            data-target="#collapsePdfItemRiesgo" aria-expanded="true" aria-controls="collapseOne">
                                            Clasificación: ${json.resultado}, Puntaje Total: <span class="badge badge-info">${json.totalPuntaje}</span>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapsePdfItemRiesgo" class="collapse show" aria-labelledby="PdfItemRiesgo" data-parent="#accordionPdfItemRiesgo-${json.idTipoEscala}">
                                    <div class="card-body">
                                        <div class="modal-body">
                                            <div class="modal-body-print">
                                                <iframe 
                                                        id="frameEscalasPdf${json.idTipoEscala}"
                                                        frameborder="0"
                                                        style="width:100%;
                                                        height:800px;"
                                                        allowfullscreen;"
                                                        >
                                                </iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`

    $("#divpdf-" + json.idTipoEscala).append(escalasPDF)
}


// funciones oculta y muestra Zona de Contención y/o Inmovilización Item riesgo

function ocultaMuestraZonaContencionInmovilizacion() {

    const hideZonadeContencionInmovilizacion = () => $("#container-8").children().eq(2).hide()
    const showZonadeContencionInmovilizacion = () => $("#container-8").children().eq(2).show()

    $("#riesgo-51").change(hideZonadeContencionInmovilizacion)
    $("#riesgo-50, #riesgo-52").change(showZonadeContencionInmovilizacion)
}


function consultarEscalasClinicas(e) {

    // oculta zona de contencion item riesgo
    ocultaMuestraZonaContencionInmovilizacion()

    let json = {}

    let idTipoEscala = $(e).data("id-tipo-escala")
    let descripcionItem = $(e).data("descripcion-escala")
    let idItem = $(e).data("id-item")

    $("#divpdf-" + idTipoEscala).empty()

    if (idTipoEscala > 0) {

        const url = `${GetWebApiUrl()}GEN_Escala_Clinica/Bandeja?idTipoEscala=${idTipoEscala}&idHospitalizacion=${sesion.ID_HOSPITALIZACION}`

        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                const lastEscalaClinica = data[0]

                if (lastEscalaClinica !== undefined) {

                    json.idTipoEscala = idTipoEscala
                    json.descripcionItem = descripcionItem
                    json.idItem = idItem
                    json.totalPuntaje = lastEscalaClinica.TotalPuntaje
                    json.resultado = lastEscalaClinica.Clasificacion.Valor

                    showEscalasClinicasPDF(json)

                    const urlEscala = `${GetWebApiUrl()}GEN_Escala_Clinica/${lastEscalaClinica.Id}/Imprimir`

                    let id = `frameEscalasPdf${idTipoEscala}`
                    ImprimirApiExterno(urlEscala, id) // pdf escala

                }
            }, error: function (err) {
                console.log("Error al buscar las escalas cínicas " + err)
            }
        })
    }
}

//---esta funcion dibuja los pill y las preguntas para hacer una carga de evaluacion de riesgos--//
function printRiesgosSeguridad(datos) {
    $("#riesgos-pills-tab").empty()
    $("#riesgos-pills-tabContent").empty()

    let titulo = ""
    let content = "";
    let contenido = "";
    let cont = "";

    datos.map(a => {


        titulo += `<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-4"> <h3> ${a.Item.DescripcionItem} </h3> </div>`;
        a.Item.SubItem.map((e, index) => {
            contenido += `<a class="nav-link ${index == 0 ? "active" : ""}"
                            id="v-pills-${e.IdItem}-tab" data-toggle="pill"
                            href="#riesgos-pills-${e.IdItem}" role="tab"
                            aria-controls="v-pills-${e.IdItem}"
                            aria-selected="${index == 0 ? "true" : "false"}"
                            data-id-tipo-escala="${e.TipoEscalaClinica !== null ? e.TipoEscalaClinica.Id : 0}"
                            data-descripcion-escala="${e.TipoEscalaClinica !== null ? e.TipoEscalaClinica.Valor : ""}"
                            data-id-item="${e.IdItem}"
                            onclick="consultarEscalasClinicas(this);">${e.DescripcionItem}</a>`

            cont += ` <div class="tab-pane fade ${index == 0 ? "show active" : ""}"
                        id="riesgos-pills-${e.IdItem}" role="tabpanel"
                        aria-labelledby="v-pills-${e.IdItem}-tab">
                        <div class="row" id="container-${e.IdItem}">
                        </div>
                        <div class="row" id="divpdf-${e.TipoEscalaClinica !== null ? e.TipoEscalaClinica.Id : 0}">
                        </div>
                      </div>
                    `;
        })
    })
    $("#riesgos-pills-tab").append(contenido)
    $("#riesgos-pills-tabContent").append(cont)
    datos.map(a => {
        for (let item of a.Item.SubItem) {
            let tipoInput;
            content = ""
            item.Preguntas.map((e, indicex) => {

                //Preguntas con multiples respuestas

                e.MultipleSeleccion ? tipoInput = "checkbox" : tipoInput = "radio"

                content += `<div class="col-sm-12 col-md-12 col-lg-3 ml-1">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h5 class="card-title">${e.DescripcionPregunta}</h5>
                                    </div>
                                    <div class="card-body">
                                        <span>Seleccione:</span> <br>
                                        <div class="form-check" id="resp-${item.idItem}-${indicex}">`
                e.Alternativas.map((x) => {
                    content += `
                                            <input id="riesgo-${x.IdRespuesta}" class="form-check-input" value="${x.IdRespuesta}"
                                            type="${tipoInput}" name="${item.IdItem}-${e.DescripcionPregunta}" data-idgrupo="${item.IdItem}" data-required="true"
                                            >
                                              <label class="form-check-label" for="${x.DescripcionAlternativa}">
                                                ${x.DescripcionAlternativa}
                                              </label>
                                            <br>`
                })
                content += `            </div>
                                    </div>
                                </div>
                            </div>`
            })
            $("#container-" + item.IdItem).append(content)
        }
    })
}

//funcion que guarda los riesgos o las valoraciones en arrays

function guardarRiesgosValoracion(id) {
    $(`a[href="#hojaEnfermeriaGeneral"]`).trigger("click")

    let comprobar = true
    if (id == 1) {
        if (validarCamposInputRadio("#divRiesgos", false)) {
            //Obtener todos los radio y los checkbos checked
            let inputRadio = $("#divRiesgos").find("input[type='radio']:checked");
            let inputCheck = $("#divRiesgos").find("input[type='checkbox']:checked");
            //Union de todos los inputs checked
            let Respuestas = []
            for (let item of inputRadio) {
                Respuestas.push(parseInt(item.value))
            }
            for (let item of inputCheck) {
                Respuestas.push(parseInt(item.value))
            }
            //Evaluación de riesgos = IdItem = 1
            riesgosApi.push({ "IdItem": 1, "Respuestas": Respuestas })
        } else {
            comprobar = false
        }
    } else if (id == 10) {
        if (validarCamposInputRadio("#divValoracion", false) && validarCampos("#divInputValoracion", false)) {
            //Obtener todos los radio checked
            let inputRadio = $("#divValoracion").find("input[type='radio']:checked");
            let inputCheck = $("#divValoracion").find("input[type='checkbox']:checked");
            //let input = inputRadio.concat(inputCheck)
            let Respuestas = []
            for (let item of inputRadio) {
                Respuestas.push(parseInt(item.value))
            }
            for (let item of inputCheck) {
                Respuestas.push(parseInt(item.value))
            }

            ObservacionValoracion = $("#txtObservacionesValoracion").val()
            ExamenFisicoValoracion = $.trim(textboxio.replace('#txtExamenFisicoValoracion').content.get())
            //Valoraciones  = IdItem = 10
            valoracionesApi.push(
                {
                    "IdItem": 10,
                    "Respuestas": Respuestas,
                    "Valoracion": {
                        ExamenFisicoValoracion: ExamenFisicoValoracion,
                        ObservacionValoracion: ObservacionValoracion
                    }
                }
            )
        } else {
            comprobar = false
        }
    }
    return comprobar
}
//Dibuja los inputs necesarios para hacer un ingreeso de valoracion del paciente
function printValoracion(data) {
    let contenido = "", alternativas = "", tipoInput
    data.map((e) => {
        e.Item.Preguntas.map(a => {
            a.MultipleSeleccion ? tipoInput = "checkbox" : tipoInput = "radio"
            alternativas = "";
            if (a.Alternativas.length > 0) {
                a.Alternativas.map(x => {
                    alternativas += `
                            <input class="form-check-input" value="${x.IdRespuesta}"
                            type="${tipoInput}" name="${a.IdPregunta}" data-idgrupo="${a.IdPregunta}" data-required="true">
                            <label class="form-check-label" for="${x.DescripcionAlternativa}">
                            ${x.DescripcionAlternativa}
                            </label></br>`
                })
            }
            contenido += `
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5>${a.DescripcionPregunta}</h5>
                    </div>
                    <div class="card-body" id="">
                        <div class="form-check" id="body-${a.IdPregunta}" style="min-height:130px;">
                               ${alternativas} 
                        </div>
                    </div>
                </div>
            </div>
             `

        })
        $("#divValoracion").append(contenido)

    })
}
//Busca las preguntas/respuestas de riesgos o valoraciones para dibujar el cuestionario (IdHojaEnfermeriaRespuesta)
function getRiesgosSeguridad(id) {
    //id = 1 es riesgo ... id = 10 valoracion
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/1/${id}/Item`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            if (id == 1) {
                if ($("#aRiesgos").hasClass("d-none")) {
                    printRiesgosSeguridad(data)
                    $("#addRiesgos").data("clicked", true)
                    $("#addRiesgos").addClass("d-none")
                    $("#aRiesgos").removeClass("d-none")
                } else {
                    $("#addRiesgos").data("clicked", false)
                    $("#riesgos-pills-tab").empty()
                    $("#riesgos-pills-tabContent").empty()
                    $("#addRiesgos").removeClass("d-none")
                    $("#aRiesgos").addClass("d-none")
                }
            } else {
                if ($("#aValoracion").hasClass("d-none")) {
                    printValoracion(data)
                    $("#addValoracion").data("clicked", true)
                    $("#divInputValoracion").removeClass("d-none")
                    $("#addValoracion").addClass("d-none")
                    $("#aValoracion").removeClass("d-none")
                } else {
                    $("#addValoracion").data("clicked", false)
                    //Vaciar pill tab content
                    $("#divValoracion").empty()
                    $("#divInputValoracion").addClass("d-none")
                    $("#addValoracion").removeClass("d-none")
                    $("#aValoracion").addClass("d-none")
                }
            }

        }, error: function (err) {
            console.log("Error al buscar los Riesgos y medidas de seguridad" + err)
        }
    });
}

//Agregar Localización Heridas
function AgregarManejoHeridas() {


    $("#mhClasificacionHerida").attr("data-required", true)
    $("#mhTipoExtremidad").attr("data-required", true)
    $("#mhPlano").attr("data-required", true)

    if (!validarCampos("#divManHeridasForm")) {
        toastr.error("Complete todos los campos requeridos de Manejo de Heridas")
        return false
    } else {
        $("#mhClasificacionHerida").removeAttr("data-required")
        $("#mhTipoExtremidad").removeAttr("data-required")
        $("#mhPlano").removeAttr("data-required")
    }
    const sltclasificacionHerida = document.getElementById("mhClasificacionHerida")
    const slttipoExtremidadManejoHerida = document.getElementById("mhTipoExtremidad")
    const slttipoPlanoManejoHerida = document.getElementById("mhPlano")

    tipoExtremidadManejoHerida = slttipoExtremidadManejoHerida.options[slttipoExtremidadManejoHerida.selectedIndex].value;
    tipoExtremidadManejoHeridaText = slttipoExtremidadManejoHerida.options[slttipoExtremidadManejoHerida.selectedIndex].text;
    clasificacionHerida = sltclasificacionHerida.options[sltclasificacionHerida.selectedIndex].value;
    clasificacionHeridaText = sltclasificacionHerida.options[sltclasificacionHerida.selectedIndex].text;
    tipoPlanoManejoHerida = slttipoPlanoManejoHerida.options[slttipoPlanoManejoHerida.selectedIndex].value;
    tipoPlanoManejoHeridaText = slttipoPlanoManejoHerida.options[slttipoPlanoManejoHerida.selectedIndex].text;
    observacionManejoHerida = $("#txtObservacionManejoHerida").val();

    ManejoHeridas.push({
        IdLocalizacionLPP: tipoExtremidadManejoHerida,
        LocalizacionLPPText: tipoExtremidadManejoHeridaText,
        IdClasificacionLPP: clasificacionHerida,
        ClasificacionHeridaText: clasificacionHeridaText,
        IdTipoPlano: tipoPlanoManejoHerida,
        TipoPlanoManejoHeridaText: tipoPlanoManejoHeridaText,
        ObservacionLocaclizacionHerida: observacionManejoHerida
    })

    printListManejoHeridas()
}

//Dibuja lista Localización Heridas
function printListManejoHeridas() {
    let lista, content = "";
    lista = "#tbodyManejoHeridas";
    $(lista).empty();
    ManejoHeridas.map((e, index) => {
        content += `<tr>
                           <td> ${e.ClasificacionHeridaText} </td>
                           <td> ${e.LocalizacionLPPText} </td>
                           <td> ${e.TipoPlanoManejoHeridaText} </td>
                           <td> ${e.ObservacionLocaclizacionHerida} </td>
                           <td><a class="btn btn-danger" onclick="eliminarManejoHerida(${index})"><i class="fa fa-trash"></i></a></td>
                        </tr>`;
    })
    $(lista).append(content);
}

function eliminarManejoHerida(indice) {
    ManejoHeridas.splice(indice, 1);
    printListManejoHeridas();
}

//function AgregarPesoTalla(elemento) {

//    let FechaHosp = moment(fechaHospital).format("YYYY-MM-DD")
//    let fechaActual = moment(GetFechaActual()).format('YYYY-MM-DD')
//    let Valor, Medida, Hora, tipoMedida, MedidaTexto, Fecha, esValido = true
//    tipoMedida = $(elemento).data("tipo");
//    //Ingresando altura
//    if (tipoMedida == "altura") {
//        Valor = $("#txtTallaPacienteHojaEnfermeria").val();
//        MedidaTexto = $("#sltTipoAltura option:selected").text();
//        Fecha = moment($("#tomaFechaAltura").val());
//        Medida = $("#sltTipoAltura").val();
//        Hora = $("#tomaHoraAltura").val();
//    } else {
//        //Ingresando peso
//        Valor = $("#txtPesoPaciente").val();
//        MedidaTexto = $("#sltTipoPeso  option:selected").text();
//        Fecha = moment($("#tomaFechaPeso").val());
//        Medida = $("#sltTipoPeso").val();
//        Hora = $("#tomaHoraPeso").val();
//    }
//    esValido = validarDecimalesNumero(Valor)
//    //Validar campos, e informacion necesaria

//    if (Valor == "" || Medida == 0 || Medida == "0" || Hora == "" || !Fecha.isValid()) {
//        toastr.error("Información incompleta")
//    } else {
//        //Validacion de fecha
//        if (Fecha.diff(fechaActual, 'days') > 0 || Fecha.diff(FechaHosp, 'days') < 0) {
//            toastr.error("La fecha no corresponde con la  fecha de hospitalizacion")
//        } else {
//            //Validacion de los decimales
//            if (esValido) {
//                Fecha = Fecha.format("YYYY-MM-DD") + " " + Hora;

//                if (tipoMedida == "altura") {
//                    let coincidencias = tallasIngresadas.find(x => x.Fecha == Fecha)
//                    if (coincidencias) {
//                        toastr.error("Ya se ingresó talla y peso en esa fecha/hora")
//                        return
//                    } else {
//                        tallasIngresadas.push({
//                            Valor: Valor,
//                            Medida: Medida,
//                            MedidaTexto: MedidaTexto,
//                            Fecha: Fecha
//                        })
//                        toastr.success("Ingresado")
//                    }
//                } else {
//                    let coincidencias = pesosIngresados.find(x => x.Fecha == Fecha)
//                    if (coincidencias) {
//                        toastr.error("Ya se ingresó talla y peso en esa fecha/hora")
//                        return
//                    } else {
//                        pesosIngresados.push({
//                            Valor: Valor,
//                            Medida: Medida,
//                            MedidaTexto: MedidaTexto,
//                            Fecha: Fecha
//                        })
//                        toastr.success("Ingresado")
//                    }
//                }

//            } else {
//                toastr.error("El formato numerico no es válido")
//            }
//        }
//    }
//    printListPesoTalla(tipoMedida)
//}
//Dibuja un listado de las tallas o los pesos
//function printListPesoTalla(tipoMedida) {
//    let lista, content = "";
//    if (tipoMedida == "altura") {
//        lista = "#listAltura";
//        $(lista).empty();
//        tallasIngresadas.map((e, index) => {
//            content += `<tr>
//                           <td> ${e.Valor} </td>
//                           <td> ${e.MedidaTexto} </td>
//                           <td> ${moment(e.Fecha).format("DD-MM-YYYY hh:mm")}</td>
//                           <td><a class="btn btn-danger" onclick="eliminarPesoTalla('altura',${index})"><i class="fa fa-trash"></i></a></td>
//                        </tr>`;
//        })
//    } else {
//        lista = "#listPeso";
//        $(lista).empty();
//        pesosIngresados.map((e, index) => {
//            content += `<tr>
//                           <td> ${e.Valor} </td>
//                           <td> ${e.MedidaTexto} </td>
//                           <td> ${moment(e.Fecha).format("DD-MM-YYYY hh:mm")}</td>
//                           <td><a class="btn btn-danger" onclick="eliminarPesoTalla('peso',${index})"><i  class="fa fa-trash"></i></a></td>
//                        </tr>`;
//        })
//    }
//    $(lista).append(content);
//}
//function eliminarPesoTalla(tipoMedida, indice) {
//    if (tipoMedida == "altura") {
//        tallasIngresadas.splice(indice, 1);
//    } else {
//        pesosIngresados.splice(indice, 1);
//    }
//    printListPesoTalla(tipoMedida);
//}

$("#sltTipoExtremidadBrazalete").change(function () {
    let value = ""
    value = $(this).val()
    if (value === "0") {
        $("#sltIdTipoPlano").attr("disabled", true)
        $("#sltIdTipoPlano").append("<option value='0'>Seleccione</option>")
        $("#sltIdTipoPlano").val(0)
    }
    else if (EsValidaSesionToken("#sltTipoExtremidadBrazalete")) {
        $("#sltIdTipoPlano").attr("disabled", false)

        comboTipoPlano(value)
    }
    else {
        $("#sltIdTipoPlano").val(0)
        $("#sltIdTipoPlano").attr("disabled", true)
    }
})

function getIdTipoPlanoPorExtremidad(idExtremidad) {

    let url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}`

    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {

            $("#sltIdTipoPlano").val(data.Plano.Id)

        },
        error: function (err) {
            console.log("No se pudo cargar el combo tipo extremidad")
        }
    });

}

function edicionSignosVitales() {
    //Existen datos ingresados?
    if (signosVitalesIngresados.length > 0) {
        //Buscar inputs
        let input = $("#divSignosVitales").find("input");
        //Recorre los arreglos para cargar los datos que anteriormente han sido ingresados
        signosVitalesIngresados.map((e, a) => {
            input.map((_, x) => {
                if (e.Nombre == x.placeholder) {
                    $("#" + x.id).val()
                }
            })
        })
    }
}
//Carga los signos vitales de la bdd
function mostrarSignosVitales(element) {

    let regex = ""

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Medida/Combo?idTipoModulo=2`,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {
            //$('#lblPacienteSignosVitales').html($(sender).data('pac'));

            $("#divSignosVitales").empty();
            $("#divSignosVitales").append(
                `
                    <h5 class="mb-3"><strong><i class='fa fa-heartbeat'></i> Signos vitales</strong></h5>
                    <div class='row'></div>
                `);

            $.each(data, function (i, r) {

                let type = "text";

                regex = r.Regex ?? null
                if (regex !== null) {
                    regex = `/${regex}/`
                }

                if (r.Formato === "str") {
                    type = "text";
                }

                $("#divSignosVitales .row").append(
                    `<div class='col-md-2 col-sm-6 mt-2'>
                        <label>${r.Valor} (${r.Informacion})</label>
                        <input  id='sltSignoVital_${r.Id}' 
                                name='${r.Id}' 
                                type='${type}'
                                maxlength='${r.Maximo}'
                                data-regex=${r.Regex}
                                onkeypress="if(this.value.length==${r.Maximo}) return false;"
                                oninput="validarRegex(this,${regex});"
                                data-informacion='${r.Informacion}'
                                class='form-control ${type}' 
                                placeholder='${r.Valor} (${r.Informacion})'
                                autocomplete="off"                                
                        />
                    </div>`);

                $(`#sltSignoVital_${r.Id}`).data("id", r.Id);

            });

            removeDataRegex()


            $(".text").unbind();
            //validarDecimales();
            //$("#aGuardarSignoVital").data("id", $(sender).data('id'));
            //CargarSignosVitalesId($(sender).data('id'))
            $("#mdlSignosVitales").modal("show");

        },
        error: function (err) {
            console.log("No se pudo cargar el modal con los signos vitales")
        }
    });
    edicionSignosVitales()
}

function removeDataRegex() {

    $("#divSignosVitales .row").find("input").each(function (index, element) {
        if ($(element).data("regex") === null)
            $(element).removeAttr("data-regex")
    })
}

function dibujarSignosVitales() {
    //infoSignosVitales
    $("#infoSignosVitales").empty();
    $("#titleIngresoActual").empty();
    let html = "";
    if (signosVitalesIngresados.length > 0) {
        title = "<h4><b>Ingreso actual:</b></h4>";
        $("#titleIngresoActual").html(title);
        signosVitalesIngresados.map((e, index) => {
            html += `<div class="col-md-2" ><span class="btn btn-outline-dark mb-2"><b>${e.Nombre}:</b> ${e.Valor} &nbsp;<a class="fa fa-times-circle" onclick="eliminarSigno(${index})" style="cursor:pointer; color:red;"></a> </span></h5></div>`;
        })
        $("#infoSignosVitales").css("border-bottom", "1px solid black")
        $("#infoSignosVitales").append(html)
    } else {
        toastr.error("No se ingresaron signos")
    }
}

function eliminarSigno(id) {
    signosVitalesIngresados.splice(id, 1)
    dibujarSignosVitales();
}

const muestraCombosExtremidadPlano = () => {

    $("#sltTipoExtremidadBrazalete").show()
    $("#lblIdTipoExtremidad").show()
    $("#sltIdTipoPlano").show()
    $("#lblIdTipoPlano").show()
    $("#lblIdFechaPlano").show()
    $("#lblIdHoraPlano").show()
    $("#inputFechaPlano").show()
    $("#inputHoraPlano").show()
}

//pl
function validarRiesgosValoraciones() {

    let bolRiesgo = true, bolValoracion = true
    //Gestion  de arreglos para enviar a la api
    //True = Valida campos riesgos
    if ($("#addRiesgos").data("clicked") == true) {
        bolRiesgo = guardarRiesgosValoracion(1)
    }
    //True == para validar los campos de las valoraciones
    if ($("#addValoracion").data("clicked") == true) {
        bolValoracion = guardarRiesgosValoracion(10)
    }
    //concatenar riesgos y valoraciones en un solo arreglo para la API
    if (bolRiesgo == true && bolValoracion == true) {
        let riesgosLength = riesgosApi.length
        let valoracionesLength = valoracionesApi.length

        if (riesgosLength > 0) {
            //Hay riesgos
            RespuestasApi.push(riesgosApi)
            riesgosApi = []
            //Deshabilita los campos para no tener que validar de nuevo
            //En caso de que alguna validacion de la hoja falle
            let inputRiesgos = $('#divRiesgos').find("input[type='checkbox']")
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            inputRiesgos = $('#divRiesgos').find("input[type='radio']");
            for (let item of inputRiesgos) {
                item.disabled = true
            }
            $("#addRiesgos").data("clicked", false)
        }
        if (valoracionesLength > 0) {
            //Hay valoraciones
            RespuestasApi.push(valoracionesApi)
            valoracionesApi = []
            //Deshabilita los campos para no tener que validar de nuevo
            //En caso de que alguna validacion de la hoja de enfermeria falle
            let inputValoraciones = $('#divValoracion').find("input[type='checkbox']:checked")
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            inputValoraciones = $('#divValoracion').find("input[type='radio']");
            for (let item of inputValoraciones) {
                item.disabled = true
            }
            $("#addValoracion").data("clicked", false)
        }
    }
    return [
        { Nombre: "Evaluación de riesgo", Validacion: bolRiesgo },
        { Nombre: "Valoracion", Validacion: bolValoracion }
    ]
}
const generateId = () => Math.random().toString(36).substr(2, 18)

function ExtraerDatosApi() {
    let validacionRV = validarRiesgosValoraciones()

    ValoracionPsicosocial = []

    //APOYO INTEGRAL HOJA MATRONERIA
    ValoracionPsicosocial = guardarValoracionoPsicosocial()

    RespuestasApi.push(ValoracionPsicosocial)

    vigilanciasAntiguas = TipoInvasivo

    if (vigilanciasAntiguas.length > 0) {

        vigilancias = vigilanciasAntiguas.map((e, i) => {

            return {
                IdHojaEnfermeriaInvasivo: e.IdHojaEnfermeriaInvasivo,
                FechaCreacion: e.Creacion.Fecha,
                IdTipoInvasivo: e.TipoInvasivo.Id,
                IdTipoPlano: e.TipoPlano.Id,
                IdTipoExtremidad: e.TipoExtremidad.Id,
                Observacion: e.Observacion,
                FechaCierre: e.FechaCierre,
                IdProfesionalCierra: e.IdProfesionalCierra,
                IdHojaEnfermeriaCierra: e.IdHojaEnfermeriaCierra,
                IdTipoRetiro: e.TipoRetiroInvasivo.Id
            }
        })

        vigilanciasApi = arregloVigilancias.concat(vigilancias); // editar o ingresar

    } else {
        vigilanciasApi = arregloVigilancias
    }

    SignosVitalesApi = signosVitalesIngresados.map((e) => {
        return {
            "Id": e.Id,
            "Valor": e.Valor
        }
    })
    hidricosApi = IngresosHidricosIngresados.concat(EgresosHidricosIngresados)
    //console.info(planCuidados)
    planCuidadosApi = planCuidados.map(x => {
        return {
            Id: x.Id,
            Observacion: x.Observacion,
            Horarios: x.Horarios
        }
    })
    return validacionRV
}
function errorDeValidacion(validacion) {
    let camposSinCompletar = validacion.filter(e => e.Validacion == false)
    let stringSinCompletar = ""
    let separador = ""
    camposSinCompletar.map((e, index) => {
        if (index == camposSinCompletar.length - 1 || camposSinCompletar.length == 1) {
            separador = "."
        } else {
            separador = ", "
        }
        stringSinCompletar += e.Nombre + separador
    })
    toastr.error("Error faltan campos por completar:" + stringSinCompletar)
}

async function getSignosVitalesHE() {

    try {
        const signosVitales = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/SignosVitales`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return signosVitales

    } catch (error) {
        console.error("Error al cargar sisgnos vitales")
        console.log(JSON.stringify(error))
    }
}

async function llenarHojaEnfermeria(id) {

    inicializarCombo()

    //let arrayHorarios = []
    //$("#txtEvolucionAtencion").text("")

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
        contentType: "application/json",
        dataType: "json",
        success:
            function (data) {
                let {
                    //Profesional: { Nombre: NombreProfesional, ApellidoPaterno: ApellidoPaternoProfesional },
                    Fecha,
                    //Turno: { Id: IdTurno, Valor: Turno },
                    IdPersonaResponsable,
                    TipoRepresentante,
                    //TipoResponsable,
                    IdHospitalizacion,
                    Diagnostico,
                    Morbidos,
                    AntecedentesQx,
                    MedicamentosHabituales,
                    OtrosProcedimientos,
                    TipoHoja,
                    ComplementoExamen,
                    //RespuestasItem,
                    //TipoBalance,
                } = data;

                //buscarPacientePorId(IdPaciente)
                //BloquearPaciente()

                setSession('FECHA_HOJA_ENFERMERIA', data.Fecha);

                if (IdPersonaResponsable == null) {

                    $("#chkAcompañante").bootstrapSwitch('state', false)
                }
                else {
                    setTimeout(async function () {
                        $("#chkAcompañante").bootstrapSwitch('state', true)
                        ShowAcompañante(true)
                        if (IdPersonaResponsable !== null) {
                            const personaAcompanianteEnfermeria = await buscarPersonaAcompaniante(IdPersonaResponsable)
                            await cargarPersonaAcompaniante(personaAcompanianteEnfermeria);
                        }

                        if (TipoRepresentante !== null)
                            $("#sltTipoRepresentante").val(TipoRepresentante.Id)
                    }, 800)
                }

                let fechaBD = moment(Fecha).format("YYYY-MM-DD");

                //dibujarHistorialSignosVitales(SignosVitales)
                //dibujarHistorialExamenesLaboratorio(Examenes)
                //dibujarHistorialPesosTallas(TipoPeso, 0)
                //dibujarHistorialPesosTallas(TipoTalla, 1)
                //dibujarHistorialEvolucion(Evolucion)                

                $("#txtFechaHojaEnfermeria").val(fechaBD)
                $("#sltTipoHojaEnfermeria").val(TipoHoja.Id)


                comboActividadTipoCuidado(TipoHoja.Id)

                //id = 4 Hoja de Registros Clínicos de Matronería
                if (TipoHoja.Id == 4) {
                    dibujaHistorialValoracionPsicosocial(RespuestasItem)
                }

                $("#txtDiagnostico").val(Diagnostico)
                $("#txtMorbidos").val(Morbidos)
                if (AntecedentesQx !== null) {
                    $("#txtAntecedentesQx").show()
                    $("#divAntecendetesQx input:radio[name='radioAntecedentesQx']:first").attr("checked", true)
                    $("#txtAntecedentesQx").val(AntecedentesQx)
                }
                else {
                    $("#divAntecendetesQx input:radio[name='radioAntecedentesQx']:last").attr("checked", true)
                }

                $("#txtMedicamentosHabituales").val(MedicamentosHabituales)
                $("#txtOtrosProcedimientos").val(OtrosProcedimientos)
                $("#txtObservacionesExamenes").val(ComplementoExamen ?? "")


                //getDataUpdatePlanCuidados(TipoCuidado)
                getDataUpdateMedicamentos(Medicamentos)
            },
        error: function (err) {
            console.error("Error al llenar la hoja de enfermeria")
            console.log(JSON.stringify(err))
        }
    })

    //GET EXAMENES 

    //getExamenesHojaEnfermeria()

}

async function getHistorialBalanceHidrico() {

    try {
        const balance = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/BalanceHidrico`,
            contentType: 'application/json',
            dataType: 'json',
        })
        return balance

    } catch (error) {
        console.error("Error al cargar balance hídrico")
        console.log(JSON.stringify(error))
    }

}
async function dibujarHistorialBalanceHidrico() {

    const data = await getHistorialBalanceHidrico()

    let totalIngreso, totalEgreso, restaTotalesIngresoEgreso = 0
    let claseIngrsoEgreso = ""

    if (data.length > 0) {
        //Ordenar arreglo por profesional
        let contenido = ""
        let content = ""
        let contenidoTotales = "", contenidoTablaTotales = ""
        let ordenado = [...new Set(data.map(i => i.Profesional))].map(a => {
            return {
                Profesional: a,
                Balances: [...new Set(data.map(a => a.Tipo))].map(i => {
                    return {
                        Balance: i,
                        Items: data.filter(x => x.Tipo == i && x.Profesional == a).map(a => {
                            return {
                                DescripcionBalance: a.DescripcionBalance,
                                Balance: a.Balance,
                                Fecha: moment(a.Fecha).format("LLLL")
                            }
                        })
                    }
                })
            }
        })
        //Dibujar el las tablas de registros hidricos por profesional
        ordenado.map(a => {
            content = ""
            a.Balances.map(e => {
                content += `<tr class="${e.Balance == "INGRESO" ? "table-success" : "table-danger"} text-center">
                            <th colspan="3">${e.Balance}</th>
                        </tr>
                        <tr><th>Nombre</th><th>Valor</th><th>Fecha</th></tr>`
                e.Items.map(i => {
                    content += `<tr><td>${i.DescripcionBalance}</td><td>${i.Balance}</td><td>${i.Fecha}</td><tr>`
                })
            })
            let idElemento = a.Profesional.replace(/ /g, "")
            contenido += `<div class="col-md-6">
                      <h5>Profesional: ${a.Profesional}</h5>
                      <table id="table-${idElemento}"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                      ${content}
                      </table></div>`
        })
        $("#divHistorialHidricos").append(contenido)
        let totales = [... new Set(data.map(a => a.Tipo))].map(i => {
            return {
                Tipo: i,
                Balances: [...new Set(data.filter(e => e.Tipo == i).map(x => x.DescripcionBalance))].map(f => {
                    return {
                        DescripcionBalance: f,
                        Total: data.filter(j => j.DescripcionBalance == f).map(q => q.Balance).reduce((acc, item) => {
                            return acc + item
                        })
                    }
                })
            }
        })
        totales.map(e => {
            contenidoTablaTotales += `<tr class="${e.Tipo == "INGRESO" ? "table-success" : "table-danger"}" text-center">
                            <th colspan="3">${e.Tipo}</th>
                        </tr>
                        <tr><th>Nombre</th><th>Total</th></tr>`
            e.Balances.map(i => {
                contenidoTablaTotales += `<tr><td>${i.DescripcionBalance}</td><td>${i.Total}</td><tr>`
            })
        })
        contenidoTotales = ` 
                    <div class="col-md-6">
                    <h5>Totales por muestra</h5>
                    <table id="table-TotalesHidricos"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                            ${contenidoTablaTotales}
                    </table></div>`
        $("#totalHistorialHidrico").append(contenidoTotales)
        let totalesIngresoEgreso = totales.map(e => {
            return {
                Tipo: e.Tipo,
                Total: e.Balances.map(i => i.Total).reduce((acc, item) => acc + item)
            }
        })

        //Resta Totales
        totalesIngresoEgreso.map(x => {

            if (x.Tipo === "INGRESO")
                totalIngreso = x.Total
            else {
                totalEgreso = x.Total
            }

            restaTotalesIngresoEgreso = totalIngreso - totalEgreso
        })

        claseIngrsoEgreso = (restaTotalesIngresoEgreso >= 0) ? "table-success" : "table-danger"

        //Dibujar tabla tota ingresos y egresos
        content = ""
        totalesIngresoEgreso.map(a => {
            content += `<tr><td class="${a.Tipo == "INGRESO" ? "table-success" : "table-danger"}" >${a.Tipo}</td><td>${a.Total}</td></tr>`
        })
        contenido = ""
        contenido += ` 
                    <div class="col-md-6">
                    <h5>Totales por ingreso/egreso</h5>
                    <table id="table-TotalesIngresos-Egresos"
                            class="table table-striped table-bordered table-hover col-xs-12 col-md-12">
                            <tr><td>Total</td><td></td></tr>
                            ${content}
                            <tr class="${claseIngrsoEgreso}"><td class="font-weight-bold">Total</td><td class="font-weight-bold">${restaTotalesIngresoEgreso}</td></tr>
                    </table></div>`
        $("#totalHistorialHidrico").append(contenido)
    } else {
        contenido = `<div class="alert alert-secondary m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    No se ha ingresado información previamente
                   </div>`
        $("#divHistorialHidricos").append(contenido)
        $("#totalHistorialHidrico").append(contenido)
    }
}

async function getHistorialRiesgos() {

    try {
        const historialRiesgos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/Respuestas`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return historialRiesgos

    } catch (error) {
        console.error("Error al cargar historial riesgos")
        console.log(JSON.stringify(error))
    }
}

//Funcion que dibuja riesgos o valoraciones ingresadas anteriormente (Cuando se edita una hoja de enfeermeria)
async function dibujarHistorialRiesgosValoraciones() {

    const data = await getHistorialRiesgos()

    let getRiesgos = [], getValoraciones = []
    getRiesgos = data.filter(i => i.IdItem == 1)
    getValoraciones = data.filter(i => i.IdItem == 10)
    let titulo, titulo2
    let contenido = ""
    let contenido2 = ""
    let fechaTraducida
    if (getRiesgos.length > 0) {
        titulo = `<div class="alert alert-info m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    Hay ${getRiesgos.length} ${getRiesgos.length == 1 ? "evaluacion de riesgo registrada." : "evaluaciones de riesgo registradas."}
                   </div>`
        getRiesgos.map(e => {
            fechaTraducida = moment(e.Fecha).format("LLLL")
            //data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
            contenido += `
                    <button class="btn btn-outline-primary m-2"
                    type="button"
                    data-toggle="collapse"
                    href="#collapse${e.IdHojaEnfermeriaRespuesta}"
                    role="button"
                    data-focus="true"
                    aria-expanded="false"
                    data-tiporespuesta="${e.IdItem}"
                    data-fecha="${fechaTraducida}"
                    id="pop${e.IdHojaEnfermeriaRespuesta}"
                    onclick='buscarHistorialRiesgo(${e.IdHojaEnfermeriaRespuesta},this)'
                    >
                    ${fechaTraducida}
                    </button>
                    `;
        })
        getRiesgos.map(e => {
            contenido += `
                    <div class="collapse" id="collapse${e.IdHojaEnfermeriaRespuesta}">
                        <div class="card card-body">
                            <div class="row" id="cardbodyRiesgo${e.IdHojaEnfermeriaRespuesta}"></div>
                            
                            </div>
                    </div>`
        })
        $("#historialRiesgos").append(contenido)
    } else {
        titulo = `<div class="alert alert-info-mod m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i> 
                    El paciente no registra aun evaluaciones de riesgo.
                   </div>`
    }

    //Hay valoraciones
    if (getValoraciones.length > 0) {
        titulo2 = `<div class="alert alert-info m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i>
                    Hay ${getValoraciones.length} ${getValoraciones.length == 1 ? "valoracion registrada." : "valoraciones registradas."}
                   </div>`
        getValoraciones.map(e => {
            fechaTraducida = moment(e.Fecha).format("LLLL")
            //fechaTraducida = traducirMoment(moment(e.Fecha))
            //data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
            contenido2 += `
                    <button class="btn btn-outline-primary m-2"
                    type="button"
                    data-toggle="collapse"
                    href="#collapse${e.IdHojaEnfermeriaRespuesta}"
                    role="button"
                    data-focus="true"
                    aria-expanded="false"
                    data-tiporespuesta="${e.IdItem}"
                    data-fecha="${fechaTraducida}"
                    id="pop${e.IdHojaEnfermeriaRespuesta}"
                    onclick='buscarHistorialRiesgo(${e.IdHojaEnfermeriaRespuesta},this)'
                    >
                    ${fechaTraducida}
                    </button>
                    `;
        })
        getValoraciones.map(e => {
            contenido2 += `
                    <div class="collapse" id="collapse${e.IdHojaEnfermeriaRespuesta}">
                        <div class="card card-body">
                            <div class="row" id="cardbodyVal${e.IdHojaEnfermeriaRespuesta}"></div>
                            <div class="row">
                                <div class="col-md-5 mx-auto">
                                    <div class="card">
                                        <div class="card-header bg-gradient-warning">
                                            <b>Observación de valoración</b>
                                        </div>
                                        <div class="card-body" style="text-align: justify">
                                            ${e.Valoracion[0].ObservacionValoracion}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 mx-auto">
                                    <div class="card">
                                        <div class="card-header bg-gradient-warning">
                                            <b>Exámenes físicos</b>
                                        </div>
                                        <div class="card-body" style="text-align: justify">
                                             ${e.Valoracion[0].ExamenFisicoValoracion}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>`
        })
        //historialValoracion
        $("#historialValoracion").append(contenido2)
    } else {
        titulo2 = `<div class="alert alert-info-mod m-2" role="alert">
                    <i class="fa fa-info-circle fa-lg"></i> 
                    El paciente no registra aun valoraciones.
                   </div>`
    }
    $("#titleHistorialRiesgo").append(titulo)
    $("#titleHistorialValoracion").append(titulo2)

}
//Busca la informacion necesarioa para dibujar los ingresos anteriores (riesgo o valoracion)
function buscarHistorialRiesgo(id, element) {
    //focus = true, click para mostrar informacion, y hacer la peticion
    //focus = false, oculta collapse.. no hace peticion, no hace uso de recursos
    let focus = $(element).data("focus")
    if (focus == true) {
        let fechaTitulo = $(element).data("fecha")
        let tipoRespuesta = $(element).data("tiporespuesta")
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria_Item/${id}/RespuestaDetalle`,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                //Data = informaciona dibujar, id= para indentifcar contenedor donde dibujar
                //fecha = fecha del registro de la evaluacion-valoracion
                //Tipo respuesta == id para identificar si es evaluacion o es valoracion
                printHistorialRiesgo(data, id, fechaTitulo, tipoRespuesta)
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
        $(element).data("focus", false)
    } else {
        //estaba en false.. cambia a true para mostrar informacion en el proximo click 
        $(element).data("focus", true)
    }
}
//Dibuja la informacion para las enfermeras (riesgo o valoracion)
function printHistorialRiesgo(data, id, fechaTitulo, tipoRespuesta) {
    let content = "", contenido = "", categoria = "", stringRespuestas = [], separador = "", cantidadRespuestas
    let Respuestas = [], element;

    //Son riegos o valoracioness
    tipoRespuesta == 1 ? element = "#cardbodyRiesgo" : element = "#cardbodyVal"
    $(element + id).empty()
    content += `<div class="col-md-12"><h5><b>${fechaTitulo}</b></h5></div>`
    data.map(e => {
        contenido = ""
        e.Preguntas.map(i => {
            stringRespuestas = ""
            cantidadRespuestas = i.Alternativas.length
            //Mas de una respuesta
            if (cantidadRespuestas == 1) {
                //Solo una respuesta
                separador = "."
                stringRespuestas += `${i.Alternativas[0].DescripcionAlternativa + separador}`
            } else {
                i.Alternativas.map((x, index) => {
                    index == parseInt(cantidadRespuestas) - 1 ? separador = "." : separador = ", "
                    stringRespuestas += `${x.DescripcionAlternativa + separador}`
                })
            }
            if (tipoRespuesta == 1) {
                contenido += ` <span> <b> ${i.DescripcionPregunta}: </b>${stringRespuestas}</span></br>`;
            } else {
                contenido += `<div class="col-lg-4"><span> <b> ${i.DescripcionPregunta}: </b>${stringRespuestas}</span></div>`
            }

        })
        content += `<div class="col-md-4 col-lg-${tipoRespuesta == 1 ? "3" : "12"}" >
                        <div class="card">
                            <div class="card-header bg-gradient-warning">
                                <h6>${e.DescripcionItem}</h6>
                            </div>
                            <div class="card-body ${tipoRespuesta == 10 ? "row" : ""}" style="min-height:190px;">
                                ${contenido}
                            </div>
                        </div>
                   </div>`
    })
    $(element + id).append(content)

}

async function dibujarHistorialInvasivos(data) {

    $("#tbodyInvasivos").empty();
    let content = ""
    let ShowProfesionalCrea = ""
    let profesional = {};

    if (data !== undefined) {
        for (const [index, e] of data.entries()) { // Usar un bucle for en lugar de map y async
            profesional = await getProfesionalPorId(e.Creacion.IdProfesional);
            displayProfesionalCrea = `${profesional.GEN_nombreProfesional} ${profesional.GEN_apellidoProfesional} ${profesional.GEN_sapellidoProfesional}`

            content += `
                    <tr class="overflow-auto table-${e.Cierre === null ? "success" : "danger"}">
                    <td>${e.TipoInvasivo.Valor}</td>
                    <td>${e.TipoExtremidad.Valor}</td>
                    <td>${e.TipoPlano.Valor}</td>
                    <td>${moment(e.FechaCreacion).format("DD-MM-YYYY HH:mm")} &nbsp;  ${displayProfesionalCrea}</td>
                    <td>${e.IdProfesionalCierra ? moment(e.FechaCierre).format("DD-MM-YYYY HH:mm") + " " + e.NombreProfesionalCierre : "N/A"}</td>
                    <td>${e.DiasInstalado ?? ""}</td>
                    <td style='max-width:100px;overflow:hidden;white-space:normal;'>${e.Observacion}</td>
                    <td>${e.TipoRetiroInvasivo?.Valor ?? ""}</td> // Usar el operador de opcionalidad (?.) en lugar de nullish coalescing (??)
                    <td>
                        <button
                        type="button"   
                        data-toggle=${e.Cierre === null ? "tooltip" : ""}
                        data-placement="bottom"
                        data-id="${index}"
                        data-id-tipo-retiro-invasivo="${e.TipoRetiroInvasivo.Id}"
                        data-tipo-invasivo="${e.TipoInvasivo.Valor}"
                        data-fecha-cierre="${e.FechaCierre}"
                        class="btn btn-outline-danger ${e.Cierre !== null ? "disabled" : ""}"
                        style="cursor:${e.Cierre !== null ? "forbidden" : "pointer"}"
                        onclick="retirarInvasivo(this);">
                            <i class="fas fa-sign-out-alt"></i>
                        </button>
                    </td>
                    </tr>`
        }
    } else {
        content = `<tr><td colspan="9"> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>` // Corregir el número de columnas
    }
    $("#tbodyInvasivos").append(content)
    $('#tbodyInvasivos [data-toggle="tooltip"]').tooltip()
}

//async function dibujarHistorialInvasivos(data) {



//    $("#tbodyInvasivos").empty()
//    let content;
//    let estado;
//    let ShowProfesionalCrea = ""
//    let profesional = {}

//    if (data !== undefined) {
//        data.map(async (e, index) => {

//            profesional = await getProfesionalPorId(e.Creacion.IdProfesional)
//            ShowProfesionalCrea = `${profesional.GEN_nombreProfesional} ${profesional.GEN_apellidoProfesional} ${profesional.GEN_sapellidoProfesional}`
//            console.log(ShowProfesionalCrea)

//            content += `
//                    <tr class="overflow-auto table-${e.Cierre === null ? "success" :  "danger" }">
//                    <td>${e.TipoInvasivo.Valor}</td>
//                    <td>${e.TipoExtremidad.Valor}</td>
//                    <td>${e.TipoPlano.Valor}</td>
//                    <td>${moment(e.FechaCreacion).format("DD-MM-YYYY HH:mm")} &nbsp;  ${ShowProfesionalCrea}</td>
//                    <td>${await e.IdProfesionalCierra ? await moment(e.FechaCierre).format("DD-MM-YYYY HH:mm") + " " + e.NombreProfesionalCierre : "N/A"}</td>
//                    <td>${e.DiasInstalado ?? ""}</td>
//                    <td style='max-width:100px;overflow:hidden;white-space:normal;'>${e.Observacion}</td>
//                    <td>${e.TipoRetiroInvasivo.Valor ?? ""}</td>
//                    <td>
//                        <button
//                        type="button"   
//                        data-toggle=${!e.IdProfesionalCierra ? "tooltip" : ""}
//                        data-placement="bottom"
//                        data-original-title="Retirar invasivo"
//                        data-id="${index}"
//                        data-id-tipo-retiro-invasivo="${e.TipoRetiroInvasivo.Id}"
//                        data-tipo-invasivo="${e.TipoInvasivo.Valor}"
//                        data-fecha-cierre="${e.FechaCierre}"
//                        class="btn btn-outline-danger ${e.IdProfesionalCierra ? "disabled" : ""}"
//                        style="cursor:${e.IdProfesionalCierra ? "forbidden" : "pointer"}"
//                        onclick="retirarInvasivo(this);"}>
//                            <i class="fas fa-sign-out-alt"></i>
//                        </button>
//                    </td>
//                    </tr >`;


//        })
//    } else {
//        content = `<tr><td colspan="7"> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>`
//    }
//    $("#tbodyInvasivos").append(content)
//    $('#tbodyInvasivos [data-toggle="tooltip"]').tooltip()
//}

function retirarInvasivo(e) {

    let tr = $(e).parent().parent()

    let id = $(e).data("id")
    let tipoInvasivo = $(e).data("tipo-invasivo")
    let fechaCierre = $(e).data("fecha-cierre")
    let idTipoRetiroInvasivo = $(e).data("id-tipo-retiro-invasivo")

    let fechaC = fechaCierre ?? null
    if (fechaC === null)
        return

    $("#titleTipoInvasivo").html(`Motivo de retiro de ${tipoInvasivo}`)
    $("#btnTipoRetiroInvasivo").attr("data-index", id)
    $("#btnTipoRetiroInvasivo").data("id", id)
    //tr.removeClass("table-success")
    //tr.addClass("table-danger")
    $("#mdlTipoRetiroInvasivo").modal("show")
}

function cambiarEstadoInvasivo(sender) {

    let id = $(sender).data("id")
    let idTipoRetiroInvasivo = parseInt($("#sltTipoRetiroInvasivo").val())
    let tipoRetiroInvasivo = $("#sltTipoRetiroInvasivo :selected").text()

    if (idTipoRetiroInvasivo === 0) {
        toastr.error("Seleccione un motivo")
        return
    }

    vigilanciasAntiguas.map((e, i) => {
        if (i == id) {
            e.IdProfesionalCierra = sesion.ID_USUARIO;
            e.NombreProfesionalCierre = sesion.NOMBRE_USUARIO;
            e.IdHojaEnfermeriaCierra = sesion.ID_HOJA_ENFERMERIA;
            e.FechaCierre = moment().format('YYYY-MM-DD HH:mm:ss');
            //e.IdTipoRetiro = idTipoRetiroInvasivo;
            e.TipoRetiroInvasivo.Id = idTipoRetiroInvasivo
            e.TipoRetiroInvasivo.Valor = tipoRetiroInvasivo
        }
    })

    dibujarHistorialInvasivos(vigilanciasAntiguas)
    $("#mdlTipoRetiroInvasivo").modal("hide")
    $(sender).removeAttr("data-id", id)
}

//function dibujarHistorialPesosTallas(data, Tipo) {
//    let content = "";
//    let tabla;
//    if (data.length > 0) {
//        data.map((e) => {
//            content += `<tr>
//                        <td>${moment(e.Fecha).format("DD-MM-YYYY hh:mm")}</td>
//                        <td>${Tipo == 0 ? e.Peso : e.Talla} &nbsp; ${Tipo == 0 ? e.DescripcionPeso : e.DescripcionTalla} </td>
//                        </tr>`;
//        })
//    } else {
//        content += "<tr><td></td> <td> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>"
//    }
//    if (Tipo == 0)
//        tabla = "#tbodyPesos";
//    else
//        tabla = "#tbodyTallas";

//    $(tabla).append(content)
//}
//function dibujarHistorialEvolucion(data) {
//    let content = "";
//    if (data.length > 0) {
//        data.map((e) => {
//            content += `
//                    <div class="card">
//                      <div class="card-header">
//                        <b>${moment(e.Fecha).format("DD-MM-YYYY hh:mm")} &nbsp;|&nbsp; ${e.NombreProfesional}</b>
//                      </div>
//                      <div class="card-body">
//                          <div class="form-control-no-height">${e.Descripcion}</div>

//                      </div>
//                    </div>
//                    `;
//        })
//    } else {
//        content += "<tr><td></td> <td> <i class='fa fa-info'></i> &nbsp; No se han encontrado registros </td> </tr>"
//    }
//    $("#tbodyDescripcion").append(content)
//}

async function dibujarHistorialSignosVitalesTable() {

    const data = await getSignosVitalesHE()

    if (data.length > 0) {

        $("#tblHistorialSignosVitales").show()

        const SignosVitales = [...new Set(data.map(i => i.Fecha))].map(fecha => {
            return {
                Fecha: fecha,
                TipoMedida: data.filter(x => x.Fecha === fecha)
            }
        })

        await crearTablaHistorialSignosVitales(SignosVitales, 10) // 10 es el límite de columnas
        ocultaColumnasHistorialSignosVitales("#tblHistorialSignosVitales")
    } else {
        $("#tblHistorialSignosVitales").hide()
    }
}

async function crearTablaHistorialSignosVitales(data, limite) {

    let arrayNuevo = [];

    data.forEach((item, index) => {

        $("#tblHistorialSignosVitales thead tr").append(`<th>${moment(item.Fecha).format("hh:mm")}</th>`);

        item.TipoMedida.forEach((item2, index) => {
            if ($("#tblHistorialSignosVitales thead th").length <= limite) { // limite de columnas
                if (!arrayNuevo.includes(item2.Descripcion)) {
                    let fila = `<td>${item2.Descripcion}</td>`;
                    data.forEach((item3) => fila += `<td data-fecha='${item3.Fecha}' data-id='${item2.Id}'></td>`);
                    $("#tblHistorialSignosVitales tbody").append(`<tr>${fila}</tr>`);
                    arrayNuevo.push(item2.Descripcion);
                }
                $(`#tblHistorialSignosVitales tbody tr td[data-fecha='${item.Fecha}'][data-id='${item2.Id}']`).html(item2.Valor);
            }
        });
    });
}

function ocultaColumnasHistorialSignosVitales(table) {

    $(`${table} th`).each(function (i) {
        let remove = 0;
        let td = $(this).parents('table').find('tr td:nth-child(' + (i + 1) + ')')
        td.each(function () {
            if (this.innerHTML == "")
                remove++;
        });

        if (remove == ($(`${table} tr`).length - 1)) {
            $(this).hide();
            td.hide();
        }
    });
}

// MOSTRAR N° HOSPITALIZACION
const mostrarNumeroHojaEnfermeria = (idHojaEnfermeria = undefined, idHospitalizacion) => {

    const tituloHojaEnfermeria = (idHojaEnfermeria !== undefined || idHojaEnfermeria > 0)
        ?
        `${sesion.ID_HOJA_ENFERMERIA}`
        : ""
    $("#numeroHojaEnfermeria").text(tituloHojaEnfermeria)
}

const mostrarNumeroHOspitalizacion = () => {

    return `${sesion.ID_HOSPITALIZACION}`
}

$("#numeroHospitalizacion").text(mostrarNumeroHOspitalizacion())


// MOSTRAR FECHA HOJA ENFERMERIA
const mostrarFechaHojaEnfermeria = (id) => {

    console.log(`${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`)

    if (id > 0 || id !== undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}`,
            contentType: "application/json",
            dataType: "json",
            success: function ({ Fecha }) {
                $("#txtFechaHojaEnfermeria").attr('value', moment(Fecha).format("YYYY-MM-DD"))
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
    }
    else
        $("#txtFechaHojaEnfermeria").attr('value', moment(GetFechaActual()).format("YYYY-MM-DD"))
}

$("#btnCancelarHojaEnfermeria").click(function (e) {
    e.preventDefault()
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
})

$("#btnCancelarModalCuidadoEnfermeria").click(function (e) {
    e.preventDefault()
    ocultarModalHorariosPlandeCuidadosEnfermeria()
})
function ocultarModalHorariosPlandeCuidadosEnfermeria() {
    $("#mdlHorariosPlanCuidadoEnfermeria").modal("hide")
}
function mostrarModalHorariosPlandeCuidadosEnfermeria() {
    $("#mdlHorariosPlanCuidadoEnfermeria").modal("show")
}

const inicializarCombo = () => {
    //cargarComboTipoPlano()
    //cargarComboTipoExtremidad();
    cargarComboTipoInvasivo(2)
    comboInvasivoExtremidadEnfermeria("#sltTipoInvasivoVigilancia", "#sltTipoExtremidadVigilancia", "#sltTipoPlanoVigilancia")
    inicializarCombosInvasivos()
    comboTipoRetiroInvasivo()
    comboTipoPeso()
    comboTipoTalla()
    comboTipoHojaEnfermeria()
    comboClasificacionHeridas()
    comboTipoExtremidad()
    comboTipoExtremidadMH()
    comboTipoPlano()
    comboTipoPlanoHerida()
    comboHidricos()
    //comboActidadesPlanDeCuidado()
    //comboActividadTipoCuidado()

    //comboCantidadTipoCuidado()
}

const comboActividadTipoCuidado = (id) => {

    if (id > 0) {
        let url = `${GetWebApiUrl()}HOS_Tipo_Cuidado/Combo/TipoHoja/${id}`;
        setCargarDataEnCombo(url, false, $('#sltActividadesEnfermeria'))
    }
}

function vigilancia() {
    let Paciente = $("#txtnombrePac").val() + " " + $("#txtApePat").val() + " " + $("#txtApeMat").val();
    let Cama = $("#camaHosp").text();
    let idHoja;
    let data = {
        id: sesion.ID_HOJA_ENFERMERIA,
        id_hos: session.ID_HOSPITALIZACION,
        cama: Cama,
        paciente: Paciente,
    }

    cargaInicialVigilanciaIAAS(data);
    //Tabla Vigilancias IAAS por hospitalización
    crearTablaVigilanciaIAAS(data.id);
    let hoy = GetFechaActual();
    let ayer = moment(hoy).subtract(1, 'days').format("YYYY-MM-DD");
    let mañana = moment(hoy).add(1, 'days').format("YYYY-MM-DD");
    //$("#dtFechaVigilancia").prop("min", ayer);
    //$("#dtFechaVigilancia").prop("max", mañana);
    $('#btnGuardarVigilancias').data("id-hoja-enfermeria", data.id);
    $('#btnGuardarVigilancias').data("id-hospitalizacion", data.id_hos);
}

function cargaInicialVigilanciaIAAS(json) {
    $('#numHojaEnfermeriaVigilanciaIAAS').html(json.id);
    $('#numHospitalizacionVigilanciaIAAS').html(json.id_hos);
    $('#numCamaModalVigilanciaIAAS').html(json.cama);
    $('#nomPacModalVigilanciaIAAS').html(json.paciente);
}

function crearTablaVigilanciaIAAS(id) {
    let array = [];
    $("#tblVigilanciaIAAS").addClass("nowrap").DataTable({
        "pageLength": 5,
        "order": [[0, "desc"]],
        data: array,
        columns: [
            { title: "id" },
            { title: "Invasivo" },
            { title: "Extremidad" },
            { title: "Plano" },
            { title: "Fecha" },
            { title: "Observaciones" },
            { title: "Acciones", class: "text-center" },
        ],
        columnDefs: [
            {
                targets: [5],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            { targets: 0, visible: false },
            {
                targets: -1,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    //La variable data es la fila actual
                    var fila = meta.row;
                    var botones = `
                        <a id='linkEliminarVigilancia' data-id='${data[fila]}' class='btn btn-danger btn-circle btn-lg' href='#/'
                            onclick='eliminarVigilancia(this); return false;' data-toggle="tooltip"
                            data-placement="left" title="Anular">
                            <i class="fas fa-trash-alt"></i>
                        </a><br>
                    `;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

function cargarTablaVigilanciaIAAS(id) {
    let array = [];
    //Hacer llamada AJAX llenar Array
    let output = [];
    array.forEach((row) => {
        //Relleno por si faltan llaves o vienen null
        let Object = new Proxy(row, handler);
        output.push([
            Object.IdHospitalizacion,
            Object.NombreInvasivo,
            Object.NombreExtremidad,
            Object.NombrePlano,
            formatDate(Object.Fecha),
            Object.Hora
        ]);
    });
}

function agregarVigilancia() {

    if ($("#sltTipoInvasivoVigilancia").val() === "0" || $("#sltTipoPlanoVigilancia").val() === "0" || $("#tmHoraVigilancia").val() === "") {
        toastr.error("faltan campos por seleccionar")
        return false
    }


    agregrarQuitarRequiredInvasivo(true);
    comprobarRequiredInvasivo();

    let nuevoInvasivo = {
        id: $("#sltTipoInvasivoVigilancia").val(),
        idExtremidad: $("#sltTipoExtremidadVigilancia").val(),
        idPlano: $("#sltTipoPlanoVigilancia").val()
    }

    let coincidencias = arregloVigilancias.find(e =>
        e.IdTipoInvasivo == nuevoInvasivo.id && e.IdTipoExtremidad == nuevoInvasivo.idExtremidad && e.IdTipoPlano == nuevoInvasivo.idPlano)
    if (!coincidencias) {
        let table = $('#tblVigilanciaIAAS').DataTable();
        let inputs = obtenerInputsVigilancia();
        let rowArray = [inputs.arreglo];
        toastr.success("Elemento agregado")
        arregloVigilancias.push(inputs.objeto);
        table.rows.add(rowArray).draw();
    } else {
        toastr.error("Elemento ya ha sido agregado a la lista.")
    }
    function comprobarRequiredInvasivo() {
        if (!validarCampos("#cuerpoModalVigilancia"))
            return false
        else
            agregrarQuitarRequiredInvasivo(false)
    }
    function agregrarQuitarRequiredInvasivo(agregar) {
        if (agregar) {
            $("#sltTipoInvasivoVigilancia").attr("data-required", true)
            $("#sltTipoExtremidadVigilancia").attr("data-required", true)
            $("#sltTipoPlanoVigilancia").attr("data-required", true)
            $("#tmHoraVigilancia").attr("data-required", true)
        }
        else {
            $("#sltTipoInvasivoVigilancia").removeAttr("data-required")
            $("#sltTipoExtremidadVigilancia").removeAttr("data-required")
            $("#sltTipoPlanoVigilancia").removeAttr("data-required")
            //$("#dtFechaVigilancia").removeAttr("data-required")
            $("#tmHoraVigilancia").removeAttr("data-required")
        }
    }
}

function eliminarVigilancia(element) {
    let table = $('#tblVigilanciaIAAS').DataTable();
    let rIndex = $(element).parents('tr').prevAll().length;
    arregloVigilancias.splice(rIndex, 1);
    table.row(rIndex).remove().draw();
    toastr.warning("Elemento eliminado de la lista")
}

function obtenerInputsVigilancia() {

    let idHoja;
    sesion.ID_HOJA_ENFERMERIA == undefined || sesion.ID_HOJA_ENFERMERIA == null ? idHoja = 0 : idHoja = sesion.ID_HOJA_ENFERMERIA;
    let fechaHora = moment($("#dtFechaVigilancia").val() + " " + $("#tmHoraVigilancia").val());
    return {
        arreglo: [
            idHoja,
            $("#sltTipoInvasivoVigilancia :selected").text(),
            $("#sltTipoExtremidadVigilancia :selected").text(),
            $("#sltTipoPlanoVigilancia :selected").text(),
            fechaHora.format("DD-MM-YYYY HH:mm:ss"),
            $("#txtObservacionesVigilancia").val()

        ],
        //Cambiar llaves y ver Fecha
        objeto: {
            FechaCreacion: fechaHora.format('YYYY-MM-DD HH:mm:ss'),
            IdHojaEnfermeriaCrea: idHoja,
            IdHojaEnfermeriaInvasivo: null,
            IdTipoExtremidad: $("#sltTipoExtremidadVigilancia").val(),
            IdTipoInvasivo: $("#sltTipoInvasivoVigilancia").val(),
            IdTipoPlano: $("#sltTipoPlanoVigilancia").val() == "0" ? null : $("#sltTipoPlanoVigilancia").val(),
            Observacion: $("#txtObservacionesVigilancia").val(),
        }
    };
}

function remapearVigilancia(pObject) {
    return {
        IdHojaEnfermeria: pObject.IdHojaEnfermeria,
        IdTipoInvasivo: pObject.IdHojaEnfermeria,
        IdTipoExtremidad: pObject.IdHojaEnfermeria,
        IdTipoPlano: pObject.IdHojaEnfermeria,
        Fecha: pObject.IdHojaEnfermeria,
        Hora: pObject.IdHojaEnfermeria,
        Observaciones: pObject.IdHojaEnfermeria,
    }
}

function agregarHidricos() {

    $("#valorHidrico").attr("data-required", true)
    //divInputHidricos
    if (!validarCampos("#divInputHidricos", false)) {
        return false
    }
    else {
        $("#valorHidrico").removeAttr("data-required")
    }
    let Tipo = $("#selectHidrico").find('option:selected').data("tipo")
    let tabla
    if (Tipo == "INGRESO") {
        tabla = "#dataTableIngresoHidrico"
    } else {
        tabla = "#dataTableEgresoHidrico"
    }
    let idHidrico = $("#selectHidrico").val()
    let nombreHidrico = $("#selectHidrico").find('option:selected').text()
    let valorHidrico = $("#valorHidrico").val()
    if (validarDecimalesNumero(valorHidrico)) {
        if (Tipo == "INGRESO") {
            IngresosHidricosIngresados.push({
                IdTipoBalance: parseInt(idHidrico),
                Balance: parseFloat(valorHidrico)
            })
        } else {
            EgresosHidricosIngresados.push({
                IdTipoBalance: parseInt(idHidrico),
                Balance: parseFloat(valorHidrico)
            })
        }

        let table = $(tabla).DataTable();
        let rowArray = [[idHidrico, nombreHidrico, valorHidrico]];
        table.rows.add(rowArray).draw();
        toastr.success("Agregado al listado")
        $("#valorHidrico").val("")
    } else {
        toastr.error("Formato de números invalido")
    }

}

function tableHidricos() {
    let array = [];
    let dataTables = ["#dataTableIngresoHidrico", "#dataTableEgresoHidrico"]
    dataTables.map(i => {
        $(`${i}`).addClass("nowrap").DataTable({
            "pageLength": 5,
            "order": [[0, "desc"]],
            data: array,
            columns: [
                { title: "Id" },
                { title: "Nombre" },
                { title: "Valor" },
                { title: "Acciones", class: "text-center" },
            ],
            columnDefs: [
                { targets: 0, visible: false },
                {
                    targets: -1,
                    orderable: false,
                    data: null,
                    render: function (data, type, row, meta) {
                        //La variable data es la fila actual
                        var fila = meta.row;
                        var botones = `
                        <a id='linkEliminarHidrico' data-id='${data[fila]}' class='btn btn-danger btn-circle btn-lg' href='#/'
                            onclick='eliminarHidrico(this, ${data[0]}); return false;' data-table=${i} data-tipo="${i == "#dataTableIngresoHidrico" ? "INGRESO" : "EGRESO"}" data-toggle="tooltip"
                            data-placement="left" title="Anular">
                            <i class="fas fa-trash-alt"></i>
                        </a><br>
                    `;
                        return botones;
                    }
                }
            ],
            "bDestroy": true
        });
    })
}

function eliminarHidrico(element, id) {
    let table = $(element).data("table")
    let tipo = $(element).data("tipo")
    table = $(table).DataTable()
    let rIndex = $(element).parents('tr').prevAll().length;

    if (tipo == "INGRESO") {
        IngresosHidricosIngresados.splice(rIndex, 1)
    } else {
        EgresosHidricosIngresados.splice(rIndex, 1)
    }
    table.row($(element).parents('tr')).remove().draw();
    toastr.warning("Elemento eliminado de la lista")
}
