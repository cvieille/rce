﻿const sesion = getSession()
let examenesLaboratorioEgreso = []
let Hospitalizacion = null, cantidadAntecedentes = 0

$(document).ready(async function () {
    ShowModalCargando(false)

    if (sesion.ID_PACIENTE)
        await CargarPacienteActualizado(sesion.ID_PACIENTE);

    BloquearPaciente()

    if (sesion.ID_HOSPITALIZACION)
        Hospitalizacion = await getDatosHospitalizacionPorId(sesion.ID_HOSPITALIZACION, false);

    deleteSession('ID_PACIENTE');
    deleteSession('ID_HOSPITALIZACION');

    if (Hospitalizacion === null) {
        ShowModalCargando(false)
        return
    }

    //OCULTA ENTREGA DOCUMENTOS, EXAMENES, CUIDADOS DE ENFERMERIA

    $("#accordionInfoEntregaDocumentoEgreso").hide()
    $("#accordionEntregaDocumentosEgreso").hide()
    $("#accordionCuidadosEnfermeriaEgreso").hide()

    //colapsarElementoDOM("#divInfoEntregaDocumentosEgreso", false)
    //colapsarElementoDOM("#divEntregaExamenesEgreso", false)
    //colapsarElementoDOM("#divCuidadosEnfermeriaEgresoEnf", false)

    await ShowAcompañante(false)


    $("#sltTipoRetiro").on('change', async function () {
        if ($('select[name="sltTipoRegistro"] option:selected').text() == 'Familiares') {
            toastr.info("Por favor, ingrese la información del acompañante")
            InicializarMostrarAcompañante()
        }
        else {
            await ShowAcompañante(false)
        }
    })

    await resetLoadAcompaniante("#sltIdentificacionAcompañante", "#txtnumeroDocAcompañante", "#txtDigAcompañante", "divAcompañante")

    //inicializarFormularioEgresoEnfermeria()
    cargarCombosEgresoEnfermeria()
    cargarEntregaIndicacionesEgreso()
    cargarExamenesEgresoEnfermeria()

    $("#chkCondicionPaciente").bootstrapSwitch()
    $("#chkCondicionPaciente").bootstrapSwitch('state', true);
    $("#chkCondicionPaciente").on('switchChange.bootstrapSwitch', function (event, state) {
        if (state)
            $("#divDatosRetiroAltaEnfermeria").fadeIn()
        else
            $("#divDatosRetiroAltaEnfermeria").fadeOut()
    })
})

function salirAbandejaHos() {
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
}

function cargarEntregaIndicacionesEgreso() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Entrega_Indicaciones/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "ENTREGA DOCUMENTOS"), "#divEntregaDocumentosEgreso")
            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "CUIDADOS DE ENFERMERÍA"), "#divCuidadosEnfermeriaEgreso")
            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "ENTREGA EXÁMENES"), "#divExamenEgreso")
        }, error: function (err) {
            console.error("Ha ocurrido un error al traer la informacion del egreso")
            console.log(err)
        }
    })
}

function cargarExamenesEgresoEnfermeria() {

    //Inicializar busqueda de examenes
    arrayCarteraArancel = GetJsonCartera()

    let element = "#thExamenesLaboratorio", emptytempl = "No existe el examen especificado ({{query}})"
    let templ = `<span><span class="{{IdCarteraServicio}}">{{ServicioCartera.Valor}}</span></span>`, source = "GEN_cartera";

    $("#thExamenesLaboratorio").typeahead(setTypeAhead(arrayCarteraArancel, element, emptytempl, templ, source));
    $("#thExamenesLaboratorio").change(() => $("#thExamenesLaboratorio").removeData("id"));
    $("#thExamenesLaboratorio").blur(() => {
        if ($("#thExamenesLaboratorio").data("id") == undefined)
            $("#thExamenesLaboratorio").val("");
    });
    $("#addElementTableExam").on('click', function () {


        if (validarCampos("#divExamLabEgreso", false)) {
            $("#thExamenesLaboratorio").data("text-servicio", $("#thExamenesLaboratorio").val())
            let idcartera = $("#thExamenesLaboratorio").data("id");
            let examenEncontrado = arrayCarteraArancel.find(x => x.IdCarteraServicio == idcartera).Aranceles
            let idTipoArancel = examenEncontrado[0].TipoArancel.Id


            $("#thExamenesLaboratorio").data("id-tipo-cartera", idTipoArancel)
            $("#thExamenesLaboratorio").data("id-cartera", idcartera)
            //$("#thExamenesLaboratorio").data("id-tipo-arancel", idTipoArancel)

            agregarExamenLaboratorioEgreso($("#thExamenesLaboratorio"))
            $("#thExamenesLaboratorio").val("")
        }
    })
    $("#thExamenesLaboratorio").on('keydown', function (e) {
        //13===Enter 
        if (e.keyCode == 13) {
            e.preventDefault()
            $("#addElementTableExam").click()
        }
    });

    dibujarTablaExamenEgreso()
}

function cargarCombosEgresoEnfermeria() {

    setCargarDataEnCombo(`${GetWebApiUrl()}HOS_Tipo_Destino_Alta_Enfermeria/Combo`, true, "#sltTipoDestino")
    setCargarDataEnCombo(`${GetWebApiUrl()}HOS_Tipo_Retiro_Alta/Combo`, true, "#sltTipoRetiro")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Motivo_Alta/Combo`, true, "#sltTipoMotivoEgreso")
}

function inicializarFormularioEgresoEnfermeria() {

    ocultarMostrarAcompanante("#divAcompananteEgreso", false)
}

function dibujarInformacionEgresoEnfermeria(info, div) {

    $(div).empty()
    let contenido = ``
    info.map(a => {
        a.Indicaciones.map(b => {
            //let columna = 3
            //b.Valor.length > 35 ? columna = 6 : columna = 3
            contenido += `
            <div class="col-md-6">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" onchange="habilitarDeshabilitarInput(this)" value="${b.Id}" id="check-${b.Id}">
                  <label class="form-check-label" for="check-${b.Id}">
                    ${b.Valor}
                  </label>
                </div>
                <input class="form-control" id="txtObservacion-${b.Id}" disabled type="text" placeholder="Observación para:${b.Valor}" />
            </div>`
        })
        $(div).append(contenido)
    })
}
function habilitarDeshabilitarInput(check) {
    if (check.checked) {
        $(`#txtObservacion-${check.value}`).attr("disabled", false)
    } else {
        $(`#txtObservacion-${check.value}`).val("")
        $(`#txtObservacion-${check.value}`).attr("disabled", true)
    }
}
function agregarExamenLaboratorioEgreso(element) {
    let idTipoCartera = $(element).data("id-tipo-cartera")
    let idCartera = $(element).data("id-cartera")
    let nombreExamen = $(element).val();
    if (idCartera !== undefined) {
        let coincidencia = examenesLaboratorioEgreso.find(x => x[1] == idCartera)
        if (coincidencia == undefined) {
            examenesLaboratorioEgreso.push([
                nombreExamen,
                idCartera,
                idTipoCartera
            ])
            var tableExamenesEgreso = $("#tblExamenesEgreso").DataTable();
            tableExamenesEgreso.row.add([
                nombreExamen,
                idCartera,
                idTipoCartera
            ])
            tableExamenesEgreso.draw();
        } else {
            toastr.info("Examen selecionado ya se encuentra en el listado")
        }
    } else {
        toastr.error("No se ha encontrado elemento, porfavor intente de nuevo")
        console.error("ha ocurrido un errror, no se obtuvo id examen")
    }
}
//Esta es la seccion de examenes para el egreso de enfermeria
function dibujarTablaExamenEgreso() {

    $("#tblExamenesEgreso").addClass("datatable").addClass("w-100").DataTable({
        data: examenesLaboratorioEgreso,
        pageLength: 5,
        columns: [
            { title: "Nombre examen" },
            { title: "id cartera" },
            { title: "Tipo examen" },
            { title: "Acciones", className: "text-center" }
        ],
        columnDefs: [
            { targets: [1], visible: false },
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    return `<button type="button" class="btn btn-danger btn-circle" onclick="eliminarExamenEgreso(${examenesLaboratorioEgreso[meta.row][1]})"><i class="fa fa-trash"></i></button>`
                }
            },
            {
                targets: -2,
                render: function (data, type, row, meta) {
                    if (examenesLaboratorioEgreso[meta.row][2] == 5) {
                        return `<span>Exámen laboratorio</span>`
                    } else {
                        return `<span>Exámen imagenología</span>`
                    }
                }
            }
        ],
        destroy: true
    })
}

function eliminarExamenEgreso(id) {
    if (id !== undefined) {
        examenesLaboratorioEgreso = examenesLaboratorioEgreso.filter(x => x[1] !== id)
        let tableExamenesEgreso = $("#tblExamenesEgreso").DataTable();
        tableExamenesEgreso.clear();
        for (let k = 0; k < examenesLaboratorioEgreso.length; k++) {
            tableExamenesEgreso.row.add(examenesLaboratorioEgreso[k]);
        }
        tableExamenesEgreso.draw();
        toastr.warning("Examen eliminado del listado.")
    }
}
function GetJsonCartera() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/Combo?idTipoArancel=4&idTipoArancel=5`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (Examenes) {

            ExamenesLabIma = Examenes
        }
    })
    return ExamenesLabIma
}
async function realizarAltaEnfermeriaPaciente() {

    let json = {}
    let idRetiro = parseInt($("#sltTipoRetiro").val())

    if (!validarCampos("#divInfoDestinoRetiro", true))
        return

    let inputCheckDocumentos = $("#divEntregaDocumentosEgreso").find("input[type='checkbox']:checked");
    let inputCheckExamenes = $("#divExamenEgreso").find("input[type='checkbox']:checked");
    let inputCheckCuidados = $("#divCuidadosEnfermeriaEgreso").find("input[type='checkbox']:checked");
    let indices = []
    let TodosInputs = [...inputCheckDocumentos, ...inputCheckExamenes, ...inputCheckCuidados]

    indices = TodosInputs.map((a, index) => {
        return {
            idEntregaIndicaciones: parseInt(a.value),
            Observaciones: $(`#txtObservacion-${a.value}`).val()
        }
    })
    let examenesParaApi = examenesLaboratorioEgreso.map(a => a[1])

    let IdTipoRetiro = parseInt($("#sltTipoRetiro").val())
    let IdTipoDestino = parseInt($("#sltTipoDestino").val())
    let IdTipoMotivo = parseInt($("#sltTipoMotivoEgreso").val())

    json = {
        IdHospitalizacion: sesion.ID_HOSPITALIZACION,
        Fecha: `${$("#fechaEgresoEnfermeria").val()} ${$("#horaEgresoEnfermeria").val()}`,
        IdTipoRetiro: IdTipoRetiro === 0 ? null : IdTipoRetiro,
        IdTipoDestino: IdTipoDestino === 0 ? null : IdTipoDestino,
        IdTipoMotivo: IdTipoMotivo === 0 ? null : IdTipoMotivo,
        Observaciones: $("#txtObservacionesAltaEnfermeria").val(),
        //Indicaciones: indices,
        //Examenes: examenesParaApi
    }

    if (idRetiro === 2) {
        if (!validarCampos("#divAcompañante"))
            return false
        const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

        if (personaAcompaniante === null) {

            const nuevaPersonaAcompaniante = await formatJsonPersonaAcompaniante()

            let acompaniante = await guardarPersonaAcompaniante(nuevaPersonaAcompaniante)

            if (acompaniante !== null)
                json.IdPersonaAcompañante = acompaniante.Id
            else
                json.IdPersonaAcompañante = null
        }
        else {
            if (personaAcompaniante !== null)
                json.IdPersonaAcompañante = personaAcompaniante.Id
        }
    }

    await postEgresoEnfermeria(json)
}

// aqui probandooooo
function getDatosPacientePorId(idPaciente) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                resolve(data);
            },
            error: function (err) {
                reject(err);
            }
        });
    });
}

async function postEgresoEnfermeria(json) {
    let resultImp = {};

    const parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${sesion.ID_HOSPITALIZACION}/Enfermeria/Egreso`
    };


    if (sesion.ID_PACIENTE) {
        const datosPaciente = await getDatosPacientePorId(sesion.ID_PACIENTE);
        const nombrePaciente = datosPaciente.Nombre; // Trayendo nombre del GEN_Paciente
        const apePatPaciente = datosPaciente.ApellidoPaterno; // Trayendo apellido paterno del GEN_Paciente
        const apeMatPaciente = datosPaciente.ApellidoMaterno; // Trayendo apellido materno del GEN_Paciente

        // Mostrar el modal de confirmación antes de realizar el alta
        const confirmacion = await Swal.fire({
            title: `¿Está a punto de dar de Alta al paciente ${nombrePaciente} ${apePatPaciente} ${apeMatPaciente}?`,
            text: `Asegúrese de revisar que la siguiente información sea correcta: Nro. Hospitalización #${sesion.ID_HOSPITALIZACION}. El usuario que realiza la acción es ${sesion.NOMBRE_USUARIO}. La Atención ya no será visible en su Bandeja y no se podrá modificar. Puede confirmar o ignorar la acción`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'No',
        });

        if (confirmacion.value) {
            // Usuario hizo clic en 'Sí', proceder con el alta
            await $.ajax({
                type: parametrizacion.method,
                url: parametrizacion.url,
                data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: async function (data, status, jqXHR) {
                    if (Object.keys(data).length > 0) {
                        let idEgreso = data.Id;

                        if (status === "success") {
                            resultImp = await Swal.fire({
                                title: 'Paciente dado de alta.',
                                text: "¿Imprimir el documento?",
                                icon: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Imprimir',
                                cancelButtonText: 'Cerrar',
                            });

                            if (resultImp.value) {
                                await ImprimirApiExternoAsync(`${GetWebApiUrl()}HOS_Hospitalizacion/Enfermeria/Egreso/${idEgreso}/Imprimir`);
                            }

                            window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
                        }
                    }
                },
                error: async function (jqXHR, status) {
                    console.log(`Ha ocurrido un error al intentar dar de alta al paciente: ${JSON.stringify(jqXHR)} `);
                }
            });
        } else {
            // Usuario hizo clic en 'No', cancelar el alta
            Swal.fire('Operación cancelada', 'El paciente no ha sido dado de alta.', 'info');
        }
    }
}