﻿
//Obtener hospitalización por id

const getHospitalizacion = async (id) => {

    try {
        const hospitalizacion = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return hospitalizacion

    } catch (error) {
        console.log(JSON.stringify(error))
    }
}

const getPacientePorId = async (id) => {

    try {
        const paciente = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return paciente

    } catch (error) {
        console.error("Error al cargar paciente")
        console.log(JSON.stringify(error))
    }
}

async function getProfesionalPorId(id) {

    if (id === null)
        return

    try {
        const profesional = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return profesional

    } catch (error) {
        console.error("Error al cargar profesional")
        console.log(JSON.stringify(error))
    }
}

const getBrazaletePaciente = async (idHospitalizacion) => {

    try {
        const brazaletes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Brazalete`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return brazaletes

    } catch (error) {
        console.error("Error al llenar brazalete")
        console.log(JSON.stringify(error))
    }
}

async function servicioTieneCamaDisponible(idUbicacion) {

    try {
        const camas = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Cama/Hospitalizacion/GEN_Ubicacion/${idUbicacion}/Libres`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return camas.length > 0

    } catch (error) {
        console.error("Error al cargar paciente")
        console.log(JSON.stringify(error))
    }
}

function CargarComboUbicacion() {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, $("#sltUbicacion"));
}

function clearAlaMapaCama() {
    $("#sltAla").empty()
    $("#sltAla").append(`<option value="0">Seleccione</option>`)
    $("#sltAla").val("0")
}

function CargarComboAla() {

    let url = `${GetWebApiUrl()}GEN_Ala/GEN_Ubicacion/${$("#sltUbicacion").val()}`;
    let estado = setCargarDataEnCombo(url, false, $("#sltAla"))

    if ($("#sltUbicacion").val() === "0") {
        clearAlaMapaCama()
    }
    else if (estado === 404) {
        clearAlaMapaCama()
        toastr.error("Ubicación no encontrada")
    }
    else {
        setCargarDataEnCombo(url, false, $("#sltAla"))
        $("#sltAla").removeAttr("disabled");
    }
}

async function getPacienteId(id) {

    try {
        const paciente = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return paciente

    } catch (error) {
        console.error("Error al cargar paciente")
        console.log(JSON.stringify(error))
    }
}

// Controlando que no se puedan re ingresar paciente ingresados o en transito
async function getHospitalizacionesIngresadasOTransitoPacienteAdmision(idPaciente) {

    if (idPaciente === undefined)
        return
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${idPaciente}&idTipoEstado=87&idTipoEstado=88&idTipoEstado=155&idTipoEstado=156`

    try {
        const hospitalizaciones = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return hospitalizaciones

    } catch (error) {
        console.error("Error al cargar hospitalizaciones del paciente")
        console.log(JSON.stringify(error))
    }
}

function getIconsEstados(row) {

    let paddingBadge = ""

    const { TipoEstadosSistemas, Aislamientos, Cama } = row;
    let inicial = "", icons = "", className = "";

    if (TipoEstadosSistemas.Id === 156 && Cama.Actual === null) {
        className = "btn-warning";
        inicial = "T";
    }
    else if (TipoEstadosSistemas.Id === 87) {
        className = "btn-secondary";
        inicial = "P";
    }
    else if (TipoEstadosSistemas.Id === 88) {
        className = "btn-success";
        inicial = "I";
    }
    else if (TipoEstadosSistemas.Id === 89) {
        className = "btn-warning";
        inicial = "E";
    }

    if (inicial === "T")
        paddingBadge = `style="padding:3px 11px;"`
    if (inicial === "I")
        paddingBadge = `style="padding:3px 14px;"`
    if (inicial === "P")
        paddingBadge = `style="padding:3px 12px;"`

    icons = `<h5><span id="bgEstado" class="badge badge-pill ${className} badge-count" ${paddingBadge}>${inicial}</span>`; //&nbsp;
    if (Aislamientos > 0) //si tiene aislamiento pinta en rojo
        icons += `&nbsp;<span id="bgAilamiento" class="badge badge-pill btn-danger badge-count">A</span>`;

    return `${icons}</h5>`;
}

function getBadgeTipoHospitalizacion({ TipoHospitalizacion }) {

    if (TipoHospitalizacion.Id == 1)
        return `<h5><span class="badge badge-pill btn-info badge-count" style="padding:3px 10px;">U</span></h5>`;
    else if (TipoHospitalizacion.Id == 4)
        return `<h5><span class="badge badge-pill btn-primary badge-count" style="padding:3px 10px;">P</span></h5>`;
    else
        return `<br/>`;
}

function imprimirIngresoEnfermeria(e) {

    let id = $(e).data("id");
    ImprimirApiExterno(`${GetWebApiUrl()}HOS_Hospitalizacion/Enfermeria/ingreso/${id}/Imprimir`);
}

function GetJsonIngresoHos(id) {

    let json = null;
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}`

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            json = data;
        }
    });

    return json;
}

async function getUltimaCategorizacion(idHospitalizacion) {
    return $.ajax({
        url: `${GetWebApiUrl()}HOS_Categorizacion/Buscar?idHospitalizacion=${idHospitalizacion}`,
        method: "GET",
        dataType: "json"
    }).then(function (categorizacion) {
        if (categorizacion.length === 0) {
            return null;
        }
        // Ordenar por fecha descendente y tomar el primer elemento
        categorizacion.sort((a, b) => new Date(b.Fecha) - new Date(a.Fecha));
        return categorizacion[0]; // Retorna la última categorización
    }).catch(function (error) {
        console.error("Error al obtener la categorización:", error);
        return null;
    });
}