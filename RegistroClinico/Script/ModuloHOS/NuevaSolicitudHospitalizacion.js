﻿
let idEvento = null;
let idSolicitudHospitalizacion = null;
let arrAislamientos = []

const { ID_PACIENTE, ID_EVENTO, ID_DAU, ID_SOLICITUD_HOSPITALIZACION } = getSession();

$(document).ready(async function () {

    if (ID_EVENTO) {
        idEvento = ID_EVENTO;
        //deleteSession("ID_EVENTO");
        deleteSession("ID_DAU");
        deleteSession("ID_PACIENTE");
    }

    if (ID_SOLICITUD_HOSPITALIZACION) {
        idSolicitudHospitalizacion = ID_SOLICITUD_HOSPITALIZACION;
        deleteSession("ID_SOLICITUD_HOSPITALIZACION");
    }

    $("#sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("disabled", "disabled");
    $("#sltPrevision").parent().remove();
    $("#sltPrevisionTramo").parent().remove();

    $("#btnGuardarSolicitudHospitalizacion").on('click', async function (evt) {
        evt.preventDefault();

        const btnGuardarSolicitudHospitalizacion = $(this); // Hace referencia al btn que dispara el evento

        // Deshabilitar el botón para evitar múltiples clics
        btnGuardarSolicitudHospitalizacion.prop('disabled', true);

        try {
            if (await validarSolicitudHospitalizacion()) {
                guardarSolicitudHospitalizacion();
            }
        } catch (error) {
            console.error("Error al guardar la solicitud de hospitalización:", error);
        } finally {
            // Habilitar el btn nuevamente
            btnGuardarSolicitudHospitalizacion.prop('disabled', false);
        }
    });

    await cargarCombos();
    InicializarDatosDiagnostico("Content_wuc_diagnosticoAtencionUrgencia", false);

    if (idEvento != null)
        cargarInformacionDesdeUrgencia(ID_PACIENTE, ID_DAU);

    if (idSolicitudHospitalizacion != null)
        cargarSolicitudHospitalizacion(idSolicitudHospitalizacion);

    ShowModalCargando(false);
    ReiniciarRequired();

    //Aislamiento

    $("#divAislamientoSolicitudHos").hide();

    $("#rdoNoAislamiento").prop("checked", true)
    $("#rdoSiAislamiento, #rdoNoAislamiento").on('change', function () {
        $("#divAislamientoSolicitudHos").toggle($("#rdoSiAislamiento").prop("checked"));
    });

    $("#rdoSiAislamiento, #rdoNoAislamiento").on('change', function () {

        if ($(this).prop("checked"))
            $("#sltMotivoAislamiento").attr("data-required", true)
        else
            $("#sltMotivoAislamiento").attr("data-required", false)
    })

    if ($("#rdoSiAislamiento").prop("checked"))
        $("#sltMotivoAislamiento").val(0)


    await comboMotivoTipoAislamientoSolicitudHos("#sltMotivoAislamiento", "#sltipoAislamiento")

});

//devuelve primera fila de la tabla
function getMotivoAislamientoFromTable(tabla) {
    if (tabla)
        return tabla.row(0).data()
}

function cargarSolicitudHospitalizacion(idSolicitudHospitalizacion) {

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}`)
        .then((resp) => {
            cargarInfoPaciente(resp.IdPaciente);
            $("#txtMotivoConsulta").val(resp.Motivo)
            $("#txtAnamnesis").val(resp.Anamnesis)
            $("#txtExamenFisico").val(resp.ExamenFisico)
            $("#txtPlanManejo").val(resp.PlanManejo)
            $("#sltUbicacion").val(resp.Ubicacion.Id)
        });

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}/Diagnosticos`)
        .then((resp) => {
            cargarDiagnostico(resp);
        });

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}/Antecedentes`)
        .then((resp) => {
            cargarAntecedentesSolHospitalizacion(resp)
        });

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}/Aislamientos`)
        .then((resp) => {
            if (resp.length > 0) {
                $('#rdoSiAislamiento').prop('checked', true).trigger('change');
                cargarAislamientosSolHospitalizacion(resp)
            }
        });

}

async function cargarCombos() {
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`, false, $("#sltUbicacion"));
    await cargarAntecedentes();
    await comboTipoMotivoTipoAislamiento("#sltMotivoAislamiento");
}

async function cargarAntecedentes() {

    await promesaAjax("GET", `GEN_Tipo_Clasificacion_Antecedente/Combo?idModulo=9`)
        .then((resp) => {
            resp.forEach(({ Id, Valor }) => {
                $("#divClasificacionAntecedentes").append(`
                    <div class='row mt-3'>
                        <div class='col-sm-12'>
                            <label>${Valor}</label>
                            <textarea id='txtAntecedente_${Id}' data-id-antecedente='${Id}' class='form-control' rows='3' placeholder='${Valor}' ></textarea>
                        </div>
                    </div>`);
            });
        });
}

async function getAntecendentesUrg(idAtencionUrgencia) {

    try {
        const antecedentes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Antecedentes`,
            contentType: 'application/json',
            dataType: 'json',
            error: function (err) {
                console.error(err);
            }
        })

        return antecedentes

    } catch (error) {
        console.error("Error al cargar antecedentes urgencia")
        console.log(JSON.stringify(error))
    }
}

function cargarInfoPaciente(idPaciente) {
    cargarPacientePorId(idPaciente);
    DeshabilitarPaciente(true);
    quitarRequiredPaciente();
}

async function cargarInformacionDesdeUrgencia(idPaciente, idAtencionUrgencia) {

    if (idPaciente)
        cargarInfoPaciente(idPaciente);


    if (idAtencionUrgencia) {
        cargarAntecedentesUrgencia(idAtencionUrgencia);
        cargarAtencionUrgenciaDestino(idAtencionUrgencia);
        cargarAtencionUrgenciaMedico(idAtencionUrgencia);
        cargarAtencionUrgenciaMotivoConsulta(idAtencionUrgencia);
    }

}

function cargarAtencionUrgenciaDestino(idAtencionUrgencia) {

    promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Alta/Medica`)
        .then((atenciones) => {

            if (atenciones && atenciones.length > 0) {
                const { UbicacionDestino, Diagnosticos } = atenciones[atenciones.length - 1];
                if (UbicacionDestino)
                    $("#sltUbicacion").val(UbicacionDestino.Id);

                if (Diagnosticos != null && Diagnosticos.length > 0)
                    cargarDiagnostico(Diagnosticos);
            }

        })
        .catch((err) => console.error);;

}

function cargarAtencionUrgenciaMedico(idAtencionUrgencia) {

    promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Medico`)
        .then((atenciones) => {
            if (atenciones.length > 0) {
                const { Anamnesis, ExamenFisico } = atenciones[atenciones.length - 1];
                $("#txtAnamnesis").val(Anamnesis);
                $("#txtExamenFisico").val(ExamenFisico);
            }
        })
        .catch((err) => console.error);

}

function cargarAtencionUrgenciaMotivoConsulta(idAtencionUrgencia) {

    promesaAjax("GET", `URG_Atenciones_Urgencia/Buscar?idAtencionUrgencia=${idAtencionUrgencia}`)
        .then((resp) => {
            const motivoConsulta = `${resp[0].MotivoConsulta}` + ((resp[0].Categorizacion != null && resp[0].Categorizacion.Observaciones != null) ? ` (${resp[0].Categorizacion.Observaciones})` : "");
            $("#txtMotivoConsulta").val(motivoConsulta.trim());
        })
        .catch((err) => console.error);

}

async function cargarAntecedentesUrgencia(idAtencionUrgencia) {
    const antecedentes = await getAntecendentesUrg(idAtencionUrgencia);
    cargarAntecedentesSolHospitalizacion(antecedentes);
}

function cargarAntecedentesSolHospitalizacion(antecedentes) {
    if (antecedentes) {
        antecedentes.forEach(({ ClasificacionAntecedente, UrgenciaClasificacionAntecedente }, index) => {
            $(`#txtAntecedente_${ClasificacionAntecedente.Id}`).val(UrgenciaClasificacionAntecedente.Valor);
        });
    }
}

function cargarAislamientosSolHospitalizacion(aislamientos) {

    const [{ Aislamientos }] = aislamientos
    let uuid = generateUUID();
    const observaciones = Aislamientos[0].Observaciones ?? ""

    Aislamientos.forEach((a, i) => {
        arrAislamientos.push({
            "id": uuid + i,
            "idMotivo": a.Motivo.Id,
            "descMotivo": a.Motivo.Valor,
            "idTipo": a.Tipo.Id,
            "descTipo": a.Tipo.Valor
        })
    })

    $("#txtObservaciones").val(observaciones)

    dibujarTablaAislamiento(arrAislamientos)
}

async function validarSolicitudHospitalizacion() {

    $("#sltMotivoAislamiento").attr("data-required", false)

    $('a[href="#divDatosPaciente"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divDPaciente", true))
        return false;

    $('a[href="#divAntecedentes"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divClasificacionAntecedentes", true))
        return false;

    $('a[href="#divDiagnosticos"]').trigger("click");
    await sleep(250);

    if (!existenDiagnosticosAgregados())
        return false;

    $('a[href="#divDatosSolicitud"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divSolicitudHospitalizacionDatosClinicos", true))
        return false;

    return true;

}

function guardarSolicitudHospitalizacion() {

    ShowModalCargando(true);
    const method = (idSolicitudHospitalizacion) ? "PUT" : "POST";
    const url = (idSolicitudHospitalizacion) ? `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}` : `HOS_Solicitud_Hospitalizacion`;
    let antecedentes = getAntecedentes()
    let aislamientos = $("#rdoNoAislamiento").prop("checked") ? null : getAislamientos(arrAislamientos)

    const solicitudHospitalizacion = {
        IdEvento: idEvento,
        IdPaciente: GetPaciente().GEN_idPaciente,
        Motivo: valCampo($("#txtMotivoConsulta").val()),
        Anamnesis: valCampo($("#txtAnamnesis").val()),
        ExamenFisico: valCampo($("#txtExamenFisico").val()),
        PlanManejo: valCampo($("#txtPlanManejo").val()),
        IdUbicacion: valCampo($("#sltUbicacion").val()),
        Diagnosticos: getDiagnosticosRealizados(),
        Antecedentes: antecedentes ?? [],
        Aislamientos: aislamientos
    };

    promesaAjax(method, url, solicitudHospitalizacion)
        .then((resp) => {
            let titleMessage = (method === "POST") ? "Solicitud ingresada" : "Solicitud actualizada";

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: titleMessage,
                showConfirmButton: false,
                timer: 2000
            }).then((result) => {
                window.opener.postMessage('Forbidden', '*');
                window.close();
            });

            ShowModalCargando(false);
        })
        .catch((err) => {
            ShowModalCargando(false);
        });
}

function getAntecedentes() {
    let antecedentes = [];
    $("textarea[data-id-antecedente]").each(function () {
        const IdAntecedente = $(this).attr("data-id-antecedente");
        const DescripcionUrgenciaAntecedente = $.trim($(this).val());
        if (DescripcionUrgenciaAntecedente !== "")
            antecedentes.push({ IdAntecedente, DescripcionUrgenciaAntecedente });
    });
    return antecedentes;
}

function getAislamientos(data) {

    const aislamientos = data.map(a => {

        return {
            IdMotivo: a.idMotivo,
            IdTipo: a.idTipo,
            Observaciones: $("#txtObservaciones").val()
        }
    })

    return aislamientos
}

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

function SalirSolicitudHospitalizacion() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = document.referrer;
}

function comboTipoMotivoTipoAislamiento(elemento) {
    const url = `${GetWebApiUrl()}GEN_Tipo_Motivo_Aislamiento/Combo`;
    setCargarDataEnCombo(url, false, elemento);
}
async function cargarTipoAislamientoSolicitudHos(idMotivoAislamiento) {

    const url = `${GetWebApiUrl()}GEN_Tipo_Motivo_Aislamiento/${idMotivoAislamiento}/Combo`

    let tipoAislamiento = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (tipoAislamiento) {

            },
        error: function (err) {
            console.error("Error al cargar tipo ailamiento")
            console.error(JSON.stringify(err))
        }
    })

    return tipoAislamiento
}

async function comboMotivoTipoAislamientoSolicitudHos(ddlMotivoAilamiento, ddlTipoAislamiento) {

    $(ddlMotivoAilamiento).change(async function () {
        let idtipoMotivoAislamiento = parseInt($(this).val())
        arrTipoAislamiento = []

        const tipoAislamientos = await cargarTipoAislamientoSolicitudHos(idtipoMotivoAislamiento)

        $(ddlTipoAislamiento).empty()
        tipoAislamientos.map(t => {
            $(ddlTipoAislamiento).append(`<option value='${t.Id}'>${t.Valor}</option>`)
        })

        $(ddlTipoAislamiento).change(async function () {
            arrTipoAislamiento = $(this).val()
        })
    })
}

function AgregarAislamientoSolicitudHos() {

    //#sltMotivoAislamiento","#sltipoAislamiento
    $("#sltMotivoAislamiento").attr("data-required", true)
    if (!validarCampos("#divAislamientoSolicitudHos", true))
        return

    let tipo = $("#sltipoAislamiento :selected")
    let motivo = $("#sltMotivoAislamiento :selected")
    let uuid = generateUUID();

    let motivoAislamiento = {
        id: motivo.val(),
        valor: motivo.text()
    }

    let tipoAislamiento = {
        id: tipo.val(),
        valor: tipo.text()
    }

    objAislamiento = {
        idMotivo: motivoAislamiento.id,
        idTipo: tipoAislamiento.id
    }

    let existe = validaAislamientoRepetido(arrAislamientos, motivoAislamiento, tipoAislamiento)

    if (existe) {
        Swal.fire({
            position: 'center',
            icon: 'warning',
            title: "Ya existe un aislamiento en la tabla",
            showConfirmButton: true,
        })
        return
    }

    arrAislamientos.push({
        "id": uuid,
        "idMotivo": motivoAislamiento.id,
        "descMotivo": motivoAislamiento.valor,
        "idTipo": tipoAislamiento.id,
        "descTipo": tipoAislamiento.valor
    })

    dibujarTablaAislamiento(arrAislamientos)
}

async function dibujarTablaAislamiento(aislamientos) {

    $('#tblAislamientosSolicitudHos').css('width', '100%');
    const tablaAislamintos = $('#tblAislamientosSolicitudHos').DataTable({
        data: aislamientos,
        ordering: false,
        language: {
            "emptyTable": "No hay aislamientos disponibles para mostrar",
        },
        columns: [
            { data: 'id', title: 'Id' },
            { data: 'idMotivo', title: 'Id Motivo' },
            { data: 'descMotivo', title: 'Motivo' },
            { data: 'idTipo', title: 'Id Tipo' },
            { data: 'descTipo', title: 'Tipo' },
            { data: null, title: 'Acciones' }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data) {
                    const { id } = data
                    return botonesAccionTablaAislamiento(id);
                }
            },
            {
                targets: [0, 1, 3], visible: false
            }
        ],
        "bDestroy": true
    });

    const data = getMotivoAislamientoFromTable(tablaAislamintos)
    if (typeof data === 'object')
        $("#sltMotivoAislamiento").attr("data-required", false)
    else
        $("#sltMotivoAislamiento").attr("data-required", true)


}

function botonesAccionTablaAislamiento(id) {

    const html = `
        <div class="d-flex flex-row justify-content-center">
            <div>
                <button
                    type="button"
                    id="deleteBotonAislamiento_${id}"
                    class="btn btn-danger m-1"
                    onclick="deleteAislamiento('${id}')"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Eliminar"
                >
                    <i class="fa fa-trash fa-xs"></i>
                </button>
            </div>
        </div>
    `;

    return html;
}

async function deleteAislamiento(id) {

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar el aislamiento?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        arrAislamientos = arrAislamientos.filter(a => a.id !== id)
    }

    dibujarTablaAislamiento(arrAislamientos)
}

function validaAislamientoRepetido(aislamientos, motivoAislamiento, tipoAislamiento) {

    return aislamientos.some(a => a.descMotivo === motivoAislamiento.valor && a.descTipo === tipoAislamiento.valor)
}
