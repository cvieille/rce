﻿
var mouseOut = true, sSession;
var jsonCamaOrigen = null, jsonCamaDestino = null; esCargaInicial = true, categorizandoDesdeMapaCamas = true

$(document).ready(async function () {

    sSession = getSession();
    RevisarAcceso(false, sSession);
    if (sSession.CODIGO_PERFIL != perfilAccesoSistema.enfermeriaMatroneria && sSession.CODIGO_PERFIL != perfilAccesoSistema.gestionCama)
        window.location.replace(ObtenerHost() + "/Vista/Default.aspx");

    $("#txtFechaHospitalizacion").val(moment(GetFechaActual()).format("YYYY-MM-DD"));
    CargarComboUbicacion();

    $("#sltUbicacion").change(function () {
        CargarComboAla();
    });


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": true,
        "showDuration": 500,
        "hideDuration": 500,
        "timeOut": 5000,
        "extendedTimeOut": 2500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }
    funcionalidadPacienteEnTransito()
    ShowModalCargando(false)

});

async function funcionalidadPacienteEnTransito() {
    //Lista Paciente en transito
    const pacienteEntransito = await getPcienteEnTransitoSinCama()
    dibujarListaPacienteEntransito(pacienteEntransito)
    dibujarTablaPacientesEnTransito(pacienteEntransito)

    //colapsarElementoDOM("#idCardPacienteEntransito", false)
}


function getHTMLPacEnTransito(cantidad = 0) {

    if (cantidad === 0)
        return `<h3>No hay pacientes en tránsito</h3>`

    let html = `
    <div id="" class="card card-warning">
        <div class="card-header">
            
                <div class="row">
                    <div class="col-md-8 d-flex justify-content-start">
                        <div class="d-inline">
                            <h5><i class="fa fa-info-circle fa-lg latidos text-red"></i>
                            Se han encontrado <span class="badge badge-info">${cantidad}</span> paciente en tránsito, recuerde regularizar estos pacientes.</h5>
                        </div>
                    </div>
                </div>
            
        </div>
        <div id="idCardPacienteEntransito" >
            <div class="card-body">
               <table id="tblListaPacientesEnTransito" class="table table-striped table-bordered table-hover  text-wrap dataTable no-footer w-100">
                </table>
            </div>
        </div>
    </div>`

    return html
}

function dibujarTablaPacientesEnTransito(data) {

    const pacientesEnTransito = data.map(p => {
        let nombreCompleto = ` ${p.Paciente.Nombre} ${p.Paciente.ApellidoPaterno} ${p.Paciente.ApellidoMaterno ?? ""}`

        let rut = `${p.Paciente.NumeroDocumento}`
        if (p.Paciente.Digito !== null)
            rut += `-${p.Paciente.Digito}`
        let UbicacionActual = p.UbicacionActual.Valor
        let UbicacionDestino = p.UbicacionDestino == null ? "Pabellon" : p.UbicacionDestino.Valor
        return [p.IdHospitalizacion, rut, nombreCompleto, UbicacionActual, UbicacionDestino, p.Observaciones ?? "Sin Información", p.FechaHora, p.Paciente.Id]
    })

    $('#tblListaPacientesEnTransito').DataTable({
        //dom: 'Bfrtip',
        //buttons: [
        //    {
        //        extend: 'excel',
        //        className: 'btn btn-info',
        //        text: 'Exportar a Excel'
        //    },
        //    {
        //        extend: 'pdf',
        //        className: 'btn btn-info',
        //        text: 'Exportar en PDF'
        //    }
        //],
        data: pacientesEnTransito,
        columns: [
            { title: "ID", className: "text-center font-weight-bold rce-tray-id" },
            { title: "RUT" },
            { title: "Nombre" },
            { title: "Ubicacion Actual" },
            { title: "Ubicacion Destino" },
            { title: "Observacion" },
            { title: "Fecha" },
            { title: "IdPaciente" },
            { title: "Acciones", className: "text-center" }
        ],
        columnDefs: [
            {
                targets: -3,
                render: function (fecha) {
                    return moment(fecha).format("DD-MM-YYYY HH:mm")
                }
            },
            { targets: -2, visible: false },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<div>
                            <a id='realizarIngresodeEnfermeria_${fila}' 
                            data-id='${data[0]}'
                            data-idestado='156'
                            data-idatencion='${data[0]}'
                            data-pac='${data[2]}' 
                            data-idpaciente='${data[7]}' 
                            data-ubicacion='${data[3]}'
                            data-estadocama=156
                            title='Ingresar traslado'
                            class='btn btn-info mr-2' href='#/' 
                            onclick='buscarSinglePaciente(this)'
                            >
                                <i class='fa fa-bed'></i>
                            </a>


                            <a id='showIngresosEnfermeria_${fila}' 
                            data-id='${data[0]}' 
                            title='Ver ingresos de Enfermría'
                            class='btn btn-info mr-2' href='#/' 
                            onclick='showMostrarListaIngresos(this);'
                            >
                                <i class='fa fa-eye'></i>
                            </a>
                            `;
                    botones +=
                        `<a id='showMovimientosHosp_${fila}' 
                                data-id='${data[0]}' 
                                title='Movimientos de Hospitalización'
                                class='btn btn-info' href='#/' 
                                onclick='showMovimientosHOspitalizacion(this);'
                            >
                                <i class='fa fa-list'></i>
                            </a>
                         </div>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true,
        "responsive": true,
        searching: false,
        paging:false
    });
}

async function showMostrarListaIngresos(e) {

    let idHosp = $(e).data("id")

    const ingresos = await getIngresosEnfermeriaPaciente(idHosp)

    $("#tblIngresosEnfermeriaPacEnTransito").DataTable({
        "order": [],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            },
        ],
        destroy: true,
        data: ingresos,
        columns: [
            { title: "ID", data: "Id" },
            { title: "Fecha ingreso", data: "Fecha" },
            { title: "Cama", data: "Cama" },
            { title: "Ubicacion", data: "Ubicacion" },
            { title: "Motivo", data: "Motivo", className: "dt-control text-wrap" },
        ], columnDefs: [

            {
                targets: 1,//cama
                render: function (fecha) {
                    //Control de nulos
                    return moment(fecha).format('DD-MM-YYYY HH:mm:ss')
                }
            },
            {
                targets: 2,//cama
                render: function (Cama) {
                    //Control de nulos
                    if (Cama == null)
                        return `Sin cama`
                    else
                        return Cama.Valor ?? `Sin cama`
                }
            },
            {
                targets: 3,//ubicacion
                render: function (Ubicacion) {
                    //control de nullos
                    if (Ubicacion == null)
                        return `Sin ubicación`
                    else
                        return Ubicacion.Valor ?? `Sin ubicación`

                }
            }
        ]
    })
    $("#mdlIngresosPacienteEntransito").modal("show")

}

async function showMovimientosHOspitalizacion(e) {

    let idHosp = $(e).data("id")

    const movimientos = await getMoviemientosHospitalizacion(idHosp)

    const { Movimiento } = movimientos

    let adataset = []

    if (Movimiento != undefined)
        adataset = Movimiento.map(m => [moment(m.Fecha).format('DD-MM-YYYY HH:mm:ss'), m.Usuario, m.TipoMovimiento])

    $("#tblMovimientosHospitalizacion").addClass("nowrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        data: adataset,
        order: [],
        columns: [
            { title: "Fecha Movimiento" },
            { title: "Usuario" },
            { title: "Movimiento", className: "dt-control text-wrap" }
        ],
        "columnDefs": [
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });

    $("#mdlMovimientosHospitalicion").modal("show")
}

function dibujarListaPacienteEntransito(pacEnTransito) {

    if (!Array.isArray(pacEnTransito))
        return

    let html = getHTMLPacEnTransito(pacEnTransito.length)
    $("#divPacienteEnTransitoSinCama").empty()
    $("#divPacienteEnTransitoSinCama").append(html)

}

async function getPcienteEnTransitoSinCama() {

    try {
        const pacEnTransito = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Transito`,
            contentType: 'application/json',
            dataType: 'json',
        })
        return pacEnTransito

    } catch (error) {
        console.error("Error al cargar paciente en transito sin cama")
        console.log(JSON.stringify(error))
    }
}

async function getIngresosEnfermeriaPaciente(idHosp) {

    try {
        const ingresos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Enfermeria/Ingreso/Buscar?idHospitalizacion=${idHosp}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return ingresos

    } catch (error) {
        console.error("Error al cargar lista de ingresos de enfermeria")
        console.log(JSON.stringify(error))
    }
}

async function getMoviemientosHospitalizacion(idHospitalizacion) {

    try {
        const movimientos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Movimientos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return movimientos

    } catch (error) {
        console.error("Error al cargar movimientos de hospitalización del paciente")
        console.log(JSON.stringify(error))
    }
}
//Funcion ejecuta query que trae un paciente y atencion en especifico
function getAtencionPaciente(idAtencion) {
    let Pacientes = [];
    if (idAtencion) {
        const url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idHospitalizacion=${idAtencion}`;
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {
                Pacientes = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
            }
        });

    }
    return Pacientes;
}
//ESTA ES LA FUNCIÓN PRINCIPAL QUE CARGA Y ESTRUCTURA LAS CAMAS SEGÚN LOS DATOS RECIBIDOS
function CargarMapaCamas() {
    ShowModalCargando(true)

    try {

        var url = GetWebApiUrl() + "HOS_Hospitalizacion/Mapa";

        if ($("#sltUbicacion").val() != "0") {
            url += "/General/" + $("#sltUbicacion").val();
            if ($("#sltAla").val() != "0")
                url += "/" + $("#sltAla").val();
        }

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data, status, jqXHR) {
                $("[data-toggle='popover']").popover('dispose');
                var array = data, html = "", arrayCamas = [];
                var htmlSinInfo = "<h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i> Sin Información</b></h1>";
                var ubicacionSeleccionada = $("#sltUbicacion").val(); // se declara la variable para referenciar la ubicacion del combo

                // LOS FOR ANIDADOS FORMA EL HTML DEL MAPA DE CAMA
                // ORDEN: UBICACION - ALA - HABITACIÓN Y CAMAS
                for (var iUbi = 0; iUbi < array.length; iUbi++) {
                    var alas = "", jsonEstadosUbi = { disponibles: 0, bloqueadas: 0, ocupadas: 0, deshabilitadas: 0 };

                    // Verificar si la ubicaion actual coincide con la seleccion del combo
                    if (array[iUbi].IdUbicacion == ubicacionSeleccionada || ubicacionSeleccionada == "0") {

                        for (var iAla = 0; iAla < array[iUbi].Ala.length; iAla++) {
                            var habitaciones = "", jsonEstadosAla = { disponibles: 0, bloqueadas: 0, ocupadas: 0, deshabilitadas: 0 };
                            for (var iHab = 0; iHab < array[iUbi].Ala[iAla].Habitacion.length; iHab++) {

                                var camas = "";
                                for (var iCam = 0; iCam < array[iUbi].Ala[iAla].Habitacion[iHab].Cama.length; iCam++) {

                                    // HTML DE CAMAS
                                    array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam].NombreUbicacion = array[iUbi].NombreUbicacion;
                                    camas += GetHtmlCama(array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam], array[iUbi].IdUbicacion);
                                    arrayCamas.push(array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam]);
                                    switch (array[iUbi].Ala[iAla].Habitacion[iHab].Cama[iCam].IdTipoEstado) {
                                        // Cama Disponible
                                        case 84:
                                            ++jsonEstadosAla.disponibles;
                                            ++jsonEstadosUbi.disponibles;
                                            break;
                                        // Cama Ocupada
                                        case 85:
                                            ++jsonEstadosAla.ocupadas;
                                            ++jsonEstadosUbi.ocupadas;
                                            break;
                                        // Cama Bloqueada
                                        case 86:
                                            ++jsonEstadosAla.bloqueadas;
                                            ++jsonEstadosUbi.bloqueadas;
                                            break;
                                        // Cama Deshabilitada
                                        case 99:
                                            ++jsonEstadosAla.deshabilitadas;
                                            ++jsonEstadosUbi.deshabilitadas;
                                            break;
                                    }

                                }

                                // HTML DE HABITACIONES
                                habitaciones += getHtmlHabitacion(array[iUbi].Ala[iAla].Habitacion[iHab], (camas === "") ? htmlSinInfo : camas);

                            }

                            // HTML DE ALAS
                            array[iUbi].Ala[iAla].idUbicacion = array[iUbi].IdUbicacion;

                            // esCargaInicial = ES PARA DESPLEGAR ACORDIÓN O NO
                            if (esCargaInicial)
                                alas += GetHtmlAla(array[iUbi].Ala[iAla], (habitaciones === "") ? htmlSinInfo : habitaciones, jsonEstadosAla, data.length);
                            else
                                alas += GetHtmlAla(array[iUbi].Ala[iAla], (habitaciones === "") ? htmlSinInfo : habitaciones, jsonEstadosAla, data.length);

                        }

                        // HTML DE UBICACIÓN
                        html += GetHtmlUbicacion(array[iUbi], (alas === "") ? htmlSinInfo : alas, jsonEstadosUbi);
                    }
                }
                $("#divGeneralMapaCama").html(html);

                // PINTA POPUP DE CADA CAMA
                // SE DEBE HACER APARTE PORQUE PRIMERO SE DEBE PINTAR EL HTML COMPLETO EN LA PÁGINA 
                //for (var i = 0; i < arrayCamas.length; i++)
                //    SetPopup(arrayCamas[i]);
                ShowModalCargando(false)
                esCargaInicial = false
                $('[data-toggle="tooltip"]').tooltip()
            },
            error: function (jqXHR, status) {
                ShowModalCargando(false)
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
            }
        });

    } catch (err) {
        alert(err.message);
        ShowModalCargando(false)
    }
}

function GetHtmlUbicacion(json, htmlAla, jsonEstados) {

    var html =
        `   <div class="card card-primary">
                <div class="card-header" style="cursor:pointer !important;">
                    <a data-toggle="collapse" data-target="#divUbi_${json.IdUbicacion}" aria-expanded="true" 
                        aria-controls="collapseOne">
                        <div class="row">
                            <div class="col-md-7">
                                <h4 class="mb-0">${json.NombreUbicacion}</h4>
                            </div>
                            <div class="col-md-5 d-flex flex-row-reverse">
                                <div class="d-inline">
                                    Disponibles: <span class="badge badge-success">${jsonEstados.disponibles}</span> | 
                                    Ocupadas: <span class="badge badge-ocupada">${jsonEstados.ocupadas}</span> | 
                                    Bloqueadas: <span class="badge badge-bloqueada">${jsonEstados.bloqueadas}</span> | 
                                    Deshabilitadas: <span class="badge badge-deshabilitada">${jsonEstados.deshabilitadas}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="divUbi_${json.IdUbicacion}" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body p-0">
                        ${htmlAla}
                    </div>
                </div>
            </div>`;

    return html;

}

function GetHtmlAla(json, htmlHabitacion, jsonEstados, lengthAla) {
    var html =
        `   <div class="card card-info" style="margin-bottom: 0px !important;">
                <div class="card-header" style="cursor:pointer !important;">
                    <a data-toggle="collapse" data-target="#divAla_${json.idUbicacion}_${json.IdAla}" aria-expanded="true" 
                        aria-controls="collapseOne">
                        <div class="row">
                            <div class="col-md-7">
                                <h5 class="mb-0">${json.NombreAla}</h5>
                            </div>
                            <div class="col-md-5 d-flex flex-row-reverse">
                                <div class="d-inline">
                                    Disponibles: <span class="badge badge-success">${jsonEstados.disponibles}</span> | 
                                    Ocupadas: <span class="badge badge-ocupada">${jsonEstados.ocupadas}</span> | 
                                    Bloqueadas: <span class="badge badge-bloqueada">${jsonEstados.bloqueadas}</span> | 
                                    Deshabilitadas: <span class="badge badge-deshabilitada">${jsonEstados.deshabilitadas}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="divAla_${json.idUbicacion}_${json.IdAla}" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body">
                        <div class="row">
                            ${htmlHabitacion}
                        </div>
                    </div>
                </div>
            </div>`;

    return html;
}

function getHtmlHabitacion(json, htmlCamas) {
    const html =
        `   <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="card card-primary">
                    <div class="card-header text-center">
                        <strong>${json.NombreHabitacion}</strong>
                    </div>
                    <div class="card-body pl-0 pr-0">
                        <div class="row">
                            ${htmlCamas}     
                        </div>
                    </div>
                </div>
            </div>`;
    return html;
}

function buscarSinglePaciente(e) {
    var PacienteElegido = [];

    let idAtencion = $(e).data('idatencion'), ubicacion = $(e).data('ubicacion'), nombreCama = $(e).data('nombrecama'), estadoCama = $(e).data('estadocama')
    let idUbicacion = $(e).data('idubicacion'), idCama = $(e).data('idcama')

    //Obtener del paciente elegido
    PacienteElegido = getAtencionPaciente(idAtencion);
    if (estadoCama !== 85) {//no es cama ocupada
        linkMostrarModalGestionPaciente(e, false)
        if (estadoCama == 156) {//en transito
            $("#bodyModalInfoPaciente").empty()
            $("#footerModalInfoPaciente").empty()
            $("#headerModalInfoPaciente").empty()
            $("#headerModalInfoPaciente").append(`${PacienteElegido[0].Paciente.Nombre + " " + PacienteElegido[0].Paciente.ApellidoPaterno + " " + PacienteElegido[0].Paciente.ApellidoMaterno}`)

            $("#divGestionPacienteHospitalizado").removeClass("d-none")

        } else {
            $("#divGestionPacienteHospitalizado").addClass("d-none")
        }


        $("#mdlInfoPaciente").modal('show')
    } else {
        linkMostrarModalGestionPaciente(e, false)
        //Mostrar solamente la botonera cuando la cama esta ocupada
        if ($("#divGestionPacienteHospitalizado").hasClass("d-none")) {
            $("#divGestionPacienteHospitalizado").removeClass("d-none")
        }
        //Setear nombre paciente a la botonera
        if (PacienteElegido !== undefined) {
            $(`#btnVerModalHojaEnfermeria,
               #btnVerModalTraslados,
               #btnVerModalCategorizar,
               #btnVerModalEscalasClinicas,
               #btnVerModalAislamientos,
               #btnVerModalConsentimiento,
               #btnVerModalEgreso,
               #btnVerModalBrazalete,
               #btnVerFichaNeoNato,
               #btnVerModalAlergias,
               #btnVerModalPacienteEnTransito,
               #btnIrAtencionClinicaHos,
               #btnAsignarCama`).data("pac", `${PacienteElegido[0].Paciente.Nombre + " " + PacienteElegido[0].Paciente.ApellidoPaterno + " " + PacienteElegido[0].Paciente.ApellidoMaterno}`)


            $(`#btnVerModalCategorizarRN`).data("numerodocumento", PacienteElegido[0].Paciente.NumeroDocumento + "-" + PacienteElegido[0].Paciente.Digito)
        }
    }

    //Funcion que dubija el modal
    if (estadoCama !== 156)
        ModalInformacionPaciente(PacienteElegido, ubicacion, nombreCama, estadoCama, idUbicacion, idCama, $(e).data('categorizadohoy'));
}

async function ModalInformacionPaciente(Paciente, ubicacion, nombreCama, estadoCama, idUbicacion, idCama, categorizadoHoy) {

    ShowModalCargando(true)
    $("#headerModalInfoPaciente").empty();
    $("#bodyModalInfoPaciente").empty();
    $("#footerModalInfoPaciente").empty();
    let modalBody = "", categorizacionHoy = true
    //nombre de cama y ubicacion para el modal.
    let modalHeader = `
             <h5 class="modal-title" id="exampleModalLongTitle"> <strong>${ubicacion} | ${nombreCama} </strong></h5>
    `;

    let modalFooter;
    //Contenido de categorizaciones
    let contenidoCategorizacion = await promesaAjax("GET", `HOS_Categorizacion/buscar?idCama=${idCama}&fecha=${$("#txtFechaHospitalizacion").val()}`, null, false).then(res => {

        if (res.length == 0) {
            categorizacionHoy = false
            return `
                    <div class="alert alert-warning w-100" role="alert">
                        <i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                        La cama no registra categorización con fecha ${moment($("#txtFechaHospitalizacion").val()).format("DD-MM-YYYY")}
                    </div>
                    `
        } else {
            return `
                    <table class="table table-bordered table-striped">
                        <caption>Tabla del listado de categorizaciones de la cama ${nombreCama} con fecha ${moment($("#txtFechaHospitalizacion").val()).format("DD-MM-YYYY")}</caption>
                        <tr>
                           <td>Id</td>
                           <td>Fecha</td>
                           <td>Cama</td>
                           <td>Profesional</td>
                           <td>Estado</td>
                        </tr>
                        ${res.map(item => {
                return `
                            <tr>
                               <td>${item.Id}</td>
                               <td>${moment(item.Fecha).format("DD-MM-YYYY")}</td>
                               <td>${item.Cama !== null ? item.Cama.Valor : `N/A`}</td>
                               <td>${item.Profesional !== null ? `${item.Profesional.Persona.Nombre} ${item.Profesional.Persona.ApellidoPaterno}` : `N/A`}</td>
                               <td>${item.TipoEstado !== null ? item.TipoEstado.Valor : `N/A`}</td>
                            </tr>
                            `
            })}
                    </table>
                    `
        }
    }).catch(err => {
        console.error("Ha ocurrido un error al intentar obtener las categorizaciones")
        console.log(err)
    })


    switch (estadoCama) {
        //Cama disponible
        case 84:
            //no se encontro información del paciente = cama disponible

            modalBody = `
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info w-100" role="alert">
                        <i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                        Cama disponible, no registra información de paciente.
                    </div>
                </div>

                <div class="col-md-12">
                    ${contenidoCategorizacion}
                </div>

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                        <h5>Registro de categorización</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="alert alert-secondary w-100" role="alert">
                                    <h5><i class="fa fa-info fa-xl" aria-hidden="true"></i> Información</h5>
                                    <ul>
                                        <li>Si la cama no tiene paciente, categorice cama sin paciente.</li>
                                        <li>Si la cama se muestra como disponible pero está acostado un paciente, debe buscarlo y realizar el traslado a la cama actual.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <button type="button" ${categorizacionHoy == true ? `disabled` : ``}  class="btn btn-danger" data-idcama=${idCama} data-idubicacion=${idUbicacion} data-idestado=131  onClick="categorizarCamaSinPaciente(this)" >Cama sin paciente</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>`;

            modalFooter = `
            <div class="col-md-6">
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 86)" class="btn btn-dark">Bloquear <i class="fa fa-lock"></i></button>
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 99)" class="btn btn-secondary">Deshabilitar <i class="fa fa-ban"></i></button>
            <button type="button" class="btn btn-secondary m-3" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>
            </div>
            <div class="col-md-2">
            
            </div>
            `

            break
        //Cama ocupada
        case 85:
            //comprobación existencia paciente en cama seleccionada.
            let claseEstilo, riesgo, dependencia, codigo

            //En caso de que este ocupada pero no tenga Paciente debe proceder a liberarse
            if (!Paciente[0]) {
                promesaAjax("PATCH", `HOS_Cama/CambiarEstado/${idCama}/84`).then(res => {

                    CargarMapaCamas()

                    Swal.fire({
                        icon: "info",
                        title: "Cama liberada",
                        html: ` <p>Está cama presentaba inconsistencias estaba ocupada y no tenia paciente, <b>se procede a liberar la cama y actualizar el mapa de camas</b></p>`,
                    }).then(res => {
                        $("#mdlInfoPaciente").modal('hide')
                    })
                }).catch(err => {
                    console.error("Ocurrio un error al cambiar el estado de la cama a 84 cama disponible")
                    console.log(err)
                })
            }


            if (Paciente[0]) {
                //Codigo categorizacion
                if (Paciente[0].Categorizacion)
                    codigo = Paciente[0].Categorizacion.Codigo;
                    console.log("LA CATEGORIZACION", codigo)
                //Obtener nivel de riesgo
                if (codigo) {
                    riesgo = Paciente[0].Categorizacion.Riesgo.Valor, dependencia = Paciente[0].Categorizacion.Dependencia.Valor;
                    //Obtener letra de categoría
                    let code = codigo.substr(0, 1);
                    console.log("LA LETRA:", code)
                    switch (code) {
                        case "A":
                            claseEstilo = "badge badge-danger";
                            break
                        case "B":
                            claseEstilo = "badge badge-warning";
                            break
                        case "C":
                            claseEstilo = "badge badge-info";
                            break
                        case "D":
                            claseEstilo = "badge badge-success";
                            break
                        default:
                            claseEstilo = "badge badge-secondary";
                            break
                    }
                    if (categorizadoHoy)
                        modalHeader += `<span class="${claseEstilo}" style=" margin-left:15px;  font-size:1.3em;">${codigo}</span>`;
                } else {
                    //sin cateogirzación
                    riesgo = "N/A", dependencia = "N/A", codigo = "S/C";
                }
                //obtener fecha actual
                var fechaActual = GetFechaActual();
                let idPaciente = Paciente[0].Paciente.IdPaciente
                let nombrePaciente = Paciente[0].Paciente.Nombre + " " + Paciente[0].Paciente.ApellidoPaterno + " " + Paciente[0].Paciente.ApellidoMaterno;
                let NUI = Paciente[0].Paciente.Nui;
                let numeroDocumento = Paciente[0].Paciente.NumeroDocumento + "-" + Paciente[0].Paciente.Digito;
                let fechaNacimiento = Paciente[0].Paciente.FechaNacimiento;

                let genero = Paciente[0].Paciente.Genero !== null ? Paciente[0].Paciente.Genero.Valor : "Sin información."
                let edadPaciente, TiempoAtencionTranscurrido = ""
                //Fecha de atención hospitalaria
                let fechaAtencion = Paciente[0].Fecha;
                //Id de la atención hospitalaria
                let idAtencion = Paciente[0].Id;
                //Estado de la atención
                let estadoAtencion = Paciente[0].Estado.Valor;
                //Calculo edad
                if (fechaNacimiento) {
                    edadPaciente = CalcularEdad(fechaNacimiento).edad;
                } else {
                    edadPaciente = "No se registro fecha de nacimiento o edad del paciente";
                }
                //Calculo del tiempo de atención transcurrido
                if (fechaAtencion) {
                    TiempoAtencionTranscurrido = CalcularEdad(fechaAtencion)
                    fechaIngreso = fechaAtencion;
                    fechaIngreso = moment(fechaIngreso);
                    tiempoTranscurridoHoras = Math.abs(fechaIngreso.diff(fechaActual, 'hours'));
                    fechaIngreso = fechaIngreso.format("DD-MM-YY HH:mm");
                } else {
                    tiempoTranscurridoDias = 0
                    tiempoTranscurridoHoras = 0
                    fechaIngreso = " No se registro fecha de ingreso";
                }

                // otro calculo del tiempo transcurrido
                // Calcular días, horas y minutos para mostrar en el modal
                const totalMinutos = tiempoTranscurridoHoras * 60;
                const tiempoTranscurridoDias = Math.floor(totalMinutos / (60 * 24));
                const tiempoTranscurridoHorasRestantes = Math.floor((totalMinutos % (60 * 24)) / 60);
                const tiempoTranscurridoMinutos = totalMinutos % 60;

                let tiempoTranscurridoFormateado;

                if (tiempoTranscurridoDias > 0) {
                    tiempoTranscurridoFormateado = `${tiempoTranscurridoDias} días, ${tiempoTranscurridoHorasRestantes} hrs : ${tiempoTranscurridoMinutos} min`;
                } else {
                    tiempoTranscurridoFormateado = `${tiempoTranscurridoHorasRestantes} hrs : ${tiempoTranscurridoMinutos} min`;
                }
                //información de paciente a inyectar en el modal

                modalBody = `
                <div class="col-md-12"><strong><p>Nombre paciente: ${nombrePaciente} </p></strong></div>
                <div class="col-md-6"><p><b>NUI:</b> ${NUI}</p></div>
                <div class="col-md-6"><p><b>Numero de documento:</b> ${numeroDocumento}</p></div>
                <div class="col-md-6"><p><b>Edad: </b> ${edadPaciente} </p></div>
                <div class="col-md-6"><p><b>Genero :</b> ${genero}</p></div>
                <div class="col-md-6"><p><b>Estado del paciente:</b> ${estadoAtencion} </p></div>
                <div class="col-md-6"><p><b>Fecha de ingreso: </b>${fechaIngreso} </p></div>
                <div class="col-md-6"> <p><b>Tiempo atención transcurrido:</b>${tiempoTranscurridoFormateado}</p></div>
                <div class="col-md-6"><p>  ${categorizadoHoy ? `<b>Riesgo:  </b>${riesgo} &nbsp; <b>Dependencia: </b> ${dependencia}` : ``}</p></div>
                `;
                modalFooter = `
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 p-1">
                        <button class="btn btn-info btn-lg w-100"
                            type="button"
                            data-nombrepaciente="${nombrePaciente}"
                            data-pac="${nombrePaciente}"
                            data-cama="${nombreCama}"
                            data-id="${idAtencion}"
                            data-estadocama="${estadoCama}"
                            data-idcama="${idCama}"
                            data-idubicacion="${idUbicacion}"
                            data-categorizadohoy=${categorizadoHoy}
                            data-rn=false
                            onClick="linkImprimirHospitalizacion(${idAtencion})">
                            <i class="fa fa-print"></i><br/>
                            Imprimir hospitalización
                        </button>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 p-1">
                        <button  class="btn btn-info btn-lg  w-100" type="button"
                        data-idPaciente="${idPaciente}"
                        onClick='CargarMovimientosHospitalizacion(${idAtencion});'
                        >
                        <i class="fa fa-eye"></i><br/>
                        Movimientos
                        </button>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 p-1">
                        <button  class="btn btn-lg btn-info  w-100" type="button"
                         data-idPaciente="${idPaciente}"
                         onClick='verTrasladosHospitalizacion(${idAtencion});'
                        >
                        <i class="fa fa-eye"></i><br/>
                        Traslados
                        </button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 p-1">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>
                    </div>
                    `;
            } else {
                //no se encontro información del paciente = cama disponible
                modalBody = `<div class="alert alert-danger w-100" role="alert">
                              <i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                              Ha ocurrido un error al intentar buscar la información de esta cama, no se encontro el ID de la atención hospitalaria.
                            </div>`;

            }
            break
        case 86://Bloqueada
            modalBody = `
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-secondary w-100" role="alert">
                        <i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                        Cama Bloqueda, no registra información de paciente.
                    </div>
                </div>

                <div class="col-md-12">
                    ${contenidoCategorizacion}
                </div>

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                        <h5>Registro de categorización</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="alert alert-secondary w-100" role="alert">
                                    <h5><i class="fa fa-info fa-xl" aria-hidden="true"></i> Información</h5>
                                    <ul>
                                        <li>La cama está bloqueada, no puede ser usada por pacientes. Categorice cama sin paciente.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <button type="button" ${categorizacionHoy == true ? `disabled` : ``} class="btn btn-danger" data-idcama=${idCama} data-idubicacion=${idUbicacion} data-idestado=131  onClick="categorizarCamaSinPaciente(this)" >Cama sin paciente</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>`;

            modalFooter = `
            <div class="col-md-6">
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 84)" class="btn btn-success">Desbloquear <i class="fa fa-unlock"></i></button>
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 99)" class="btn btn-secondary">Deshabilitar <i class="fa fa-ban"></i></button>
            <button type="button" class="btn btn-secondary m-3" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>
            </div>
            <div class="col-md-2">
            
            </div>
            `

            break
        case 99: //Deshabilitada
            modalBody = `
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-dark w-100" role="alert">
                        <i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                        Cama deshabilitada, no registra información de paciente.
                    </div>
                </div>

                <div class="col-md-12">
                    ${contenidoCategorizacion}
                </div>

                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                        <h5>Registro de categorización</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="alert alert-secondary w-100" role="alert">
                                    <h5><i class="fa fa-info fa-xl" aria-hidden="true"></i> Información</h5>
                                    <ul>
                                        <li>La cama está deshabilitada, no puede ser usada por pacientes. Categorice cama sin paciente.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <button type="button" class="btn btn-danger" ${categorizacionHoy == true ? `disabled` : ``} data-idcama=${idCama} data-idubicacion=${idUbicacion} data-idestado=131  onClick="categorizarCamaSinPaciente(this)" >Cama sin paciente</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>`;
            modalFooter = `
            <div class="col-md-6">
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 84)" class="btn btn-success">Habilitar <i class="fa fa-check"></i></button>
            <button type="button" onclick="cambiarEstadoCama(${idCama}, 86)" class="btn btn-dark">Bloquear <i class="fa fa-lock"></i></button>
            <button type="button" class="btn btn-secondary m-3" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>
            </div>
            <div class="col-md-2">
            
            </div>
            `
            break
    }

    //if (estadoCama !== 85)
    //    modalFooter += `<button type="button" class="btn btn-secondary m-3" data-dismiss="modal" onclick="LimpiarComponentes()">Cerrar</button>`;

    //Boton de x para cerrar modal
    modalHeader += `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>`

    $("#headerModalInfoPaciente").html(modalHeader);
    $("#bodyModalInfoPaciente").html(modalBody);
    $("#footerModalInfoPaciente").html(modalFooter);
    ShowModalCargando(false)

}

const categorizarCamaSinPaciente = (sender) => {
    let handlerCategorizacion = {
        set: function (object, prop, value) {
            if (value == "" || value == undefined || value == null) {
                toastr.error(`La información de esta cama presenta inconsistencias, revisar: ${prop} `)
                return new Error(`Error, el campo ${prop} no puede estar vacio`)
            }
        }
    }
    let objetoCategorizar = {}
    new Proxy(objetoCategorizar, handlerCategorizacion)
    objetoCategorizar.IdEstado = $(sender).data('idestado')
    objetoCategorizar.IdCama = $(sender).data('idcama')
    objetoCategorizar.IdUbicacion = $(sender).data('idubicacion')
    objetoCategorizar.VersionSaludMental = false
    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}HOS_Categorizacion`,
        contentType: "application/json",
        data: JSON.stringify(objetoCategorizar),
        success: function (data, status, jqXHR) {
            Swal.fire(
                'Operación exitosa!',
                `La cama ha sido categorizada.`,
                'success'
            ).then(res => {
                CargarMapaCamas(true)
                if (res)
                    $("#mdlInfoPaciente").modal("hide")
            })
        },
        error: function (err) {
            console.error("Ha ocurrido un error al intentar categorizar un paciente")
            console.log(err)
        }
    })
}

const linkMostrarModalEgreso = async (sender) => {

    const { value: tipoAtencion } = await Swal.fire({
        title: '¿Donde se va el paciente?',
        input: 'radio',
        allowOutsideClick: false,
        inputOptions: {
            '1': 'A otro servicio',
            '2': 'A su domicilio'
        },
        inputValidator: (value) => {
            if (!value) {
                return 'Debe seleccionar una opción'
            }
        }
    });

    if (tipoAtencion == '2') {
        setSession('ID_HOSPITALIZACION', $(sender).data("id"));
        setSession('ID_PACIENTE', $(sender).data("idpaciente"));
        window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoEgresoEnfermeria.aspx";
    } else if (tipoAtencion == '1') {
        $("#sltTipoTransito").val("1");
        $("#btnVerModalPacienteEnTransito").trigger("click");
    }

}

async function irModalCategorizacion(button) {
    $("#mdlInfoPaciente").modal("hide")
    $('#mdlInfoPaciente').on('hidden.bs.modal', async function (e) {
        await sleep(100)
        linkMostrarModalCategorizacion($(button))

        $('#mdlInfoPaciente').unbind()
    })
}
function GetHtmlCama(json, idUbicacion) {

    var html =
        `   <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class='text-center overflow-scroll'>
                    <strong>${json.Nombre}</strong> 
                </div>`

    //case 84: estado: "disponible",
    //case 85: estado: "ocupada",
    //case 86: estado: "bloqueada",
    //case 99: estado: "deshabilitada",
    //default: estado: "sin-estado",

    let clase = `dark`, color = "black", categorizadoHoy = false, codigoCompleto = ``, code = ``
    if (json.Categorizacion !== null) {
        //Si la cama fue categorizada
        switch (json.Categorizacion.Estado.Id) {
            case 129: //Paciente categorizado
                code = json.Categorizacion.Codigo.split("")[0];
                codigoCompleto = json.Categorizacion.Codigo
                switch (code) {
                    case "A":
                        //Red
                        color = "#dc3545";
                        clase = `danger`
                        break
                    case "B":
                        //Yellow
                        color = "#DBA607";
                        clase = `warning`
                        break
                    case "C":
                        //Green 
                        color = "#17a2b8";
                        clase = `info`
                        break
                    case "D":
                        //Blue
                        color = "#28a745";
                        clase = `success`
                        break
                    default:
                        clase = `dark`
                        color = "black";
                        break
                }
                break;
            case 130: //Paciente ausente
                code = `Paciente ausente`
                codigoCompleto = `<i class="fa fa-user-secret"></i>`
                break;
            case 131:// Cama sin paciente
                code = `Cama sin paciente`
                codigoCompleto = `<i class="fa fa-times-circle"></i>`
                break;
        }


        fechaCategorizacion = moment(json.Categorizacion.FechaCategorizacion).format("DD-MM-YYYY")

        //Si el paciente fue categorizado hoy dia en la ubicacion actual.
        if (moment($("#txtFechaHospitalizacion").val()).format("DD-MM-YYYY") == fechaCategorizacion && parseInt($("#sltUbicacion").val()) == json.Categorizacion.IdUbicacion) {
            categorizadoHoy = true
        } else {
            color = "black";
        }

    }

    switch (json.IdTipoEstado) {
        /*
        ondragstart="Drag(event)"
        ondrop="Drop(event)"
        ondragover="OverDrag(event)"
        ondragleave="OutDrag(event)"
        ondragend="EndDrag(event)"
        */
        case 84://Disponible

            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                                <a
                                 class="cama-a"
                                data-toggle="modal"
                                data-target="#mdlInfoPaciente"
                                id='aCama_${json.Id}'
                                data-idcama="${json.Id}"
                                data-ubicacion="${json.NombreUbicacion}"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                data-idubicacion="${idUbicacion}"
                                data-info="disponible"
                                draggable='true'
                                onClick=buscarSinglePaciente(this)>

                            <div class='cama disponible' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa fa-check-circle' style='z-index:1;'></i>
                            </div>
                            <div id="divCategorizacionCama_${json.Id}">
                            ${categorizadoHoy == true ? `<h5><span class="badge badge-${clase} w-100"  data-toggle="tooltip" data-placement="bottom" title="${code}" > ${codigoCompleto}</span></h5>` : ``}
                            </div>
                            </a>
                    </div>`;
            break;

        case 85://ocupada


            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a  data-toggle="modal"
                         class="cama-a"
                                id='aCama_${json.Id}'
                                data-pac="${json.Hospitalizacion.length > 0 ? `${json.Hospitalizacion[0].Paciente.Nombre} ${json.Hospitalizacion[0].Paciente.ApellidoPaterno} ${json.Hospitalizacion[0].Paciente.ApellidoMaterno}` : ""}"
                                data-id="${json.Hospitalizacion.length > 0 ? json.Hospitalizacion[0].Atencion.Id : ""}"
                                data-ubicacion="${json.NombreUbicacion}"
                                data-cama="${json.Nombre}"
                                data-idpaciente="${json.Hospitalizacion.length > 0 ? json.Hospitalizacion[0].Paciente.Id : ""}"
                                data-idcama="${json.Id}"

                                data-idPersona="${json.Hospitalizacion.length > 0 ? json.Hospitalizacion[0].Paciente.Id : ""}"
                                data-idatencion="${json.Hospitalizacion.length > 0 ? json.Hospitalizacion[0].Atencion.Id : ""}"
                                data-target="#mdlInfoPaciente"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                data-idubicacion="${idUbicacion}"
                                data-categorizadohoy=${categorizadoHoy}
                                role='button' class='cama-a'
                                onClick=buscarSinglePaciente(this)>
                            
                            <div class='cama ocupada'>
                                <img id='imgCama_${json.Id}' src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa fa-user-circle' style='color:${color};''></i>
                            </div>
                            <div id="divCategorizacionCama_${json.Id}">
                            ${categorizadoHoy == true ? `<h5><span class="badge badge-${clase} w-100" data-toggle="tooltip" data-placement="bottom" title="${code}" >${codigoCompleto}</span></h5>` : ``}
                            </div>
                        </a>
                    </div>`;
            break;

        case 99://Deshabilitada
            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                            <a
                                class="cama-a"
                                data-toggle="modal"
                                data-target="#mdlInfoPaciente"
                                id='aCama_${json.Id}'
                                data-idcama="${json.Id}"
                                data-ubicacion="${json.NombreUbicacion}"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                data-idubicacion="${idUbicacion}"
                                data-info="disponible"
                                draggable='true'
                                onClick=buscarSinglePaciente(this)>
                            <div class='cama deshabilitada' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa fa-ban' style='z-index:1;'></i>
                            </div>
                            <div id="divCategorizacionCama_${json.Id}">
                            ${categorizadoHoy == true ? `<h5><span class="badge badge-${clase} w-100" data-toggle="tooltip" data-placement="bottom" title="${code}" >  ${codigoCompleto}</span></h5>` : ``}
                             </div>
                            </a>
                    </div>`
            break
        case 86://Bloqueada
            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                            <a
                                class="cama-a"
                                data-toggle="modal"
                                data-target="#mdlInfoPaciente"
                                id='aCama_${json.Id}'
                                data-idcama="${json.Id}"
                                data-ubicacion="${json.NombreUbicacion}"
                                data-nombrecama="${json.Nombre}"
                                data-estadocama="${json.IdTipoEstado}"
                                data-idubicacion="${idUbicacion}"
                                data-info="disponible"
                                draggable='true'
                                onClick=buscarSinglePaciente(this)>
                            <div class='cama bloqueada' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa fa-lock' style='z-index:1;'></i>
                            </div>
                            <div id="divCategorizacionCama_${json.Id}">
                            ${categorizadoHoy == true ? `<h5><span class="badge badge-${clase} w-100" data-toggle="tooltip" data-placement="bottom" title="${code}" >  ${codigoCompleto}</span></h5>` : ``}
                            </div>
                            </a>
                    </div>`
            break
        default:

            html +=
                `   <div id='divCama_${json.Id}' class='center-horizontal'>
                        <a id='aCama_${json.Id}' tabindex='0' data-toggle='popover' data-container='body' data-placement='auto' data-html='true' 
                            role='button' class='cama-a'>
                            <div class='cama sin-estado' >
                                <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                <i id='iCama_${json.Id}' class='fa fa-question-circle' style='z-index:1;'></i>
                            </div>
                        </a>
                    </div>`;
            break
    }

    return html + "</div>";
}

function LimpiarComponentes() {
    $(".popover").unbind();
    $("[data-toggle=popover]").popover('hide');
    $(".camaSeleccionada").css({ "z-index": "" }).removeClass("camaSeleccionada").addClass("cama-no-Seleccionada");
    $(".background-popover").remove();
    jsonCamaOrigen = jsonCamaDestino = null;
}

function GetHtmlPopup(json) {

    var card = "", icon = "";
    switch (json.GEN_idTipo_Estados_Sistemas) {

        // Cama Disponible
        case 84:
            icon = "fa fa-check-circle";
            card = "card-disponible";
            break;
        // Cama Ocupada
        case 85:
            icon = "fa fa-user-circle";
            card = "card-ocupada";
            break;
        // Cama Bloqueada
        case 86:
            icon = "fa fa-lock";
            card = "card-bloqueada";
            break;
        // Cama Deshabilitada
        case 99:
            icon = "fa fa-ban";
            card = "card-deshabilitada";
            break;
        // Sin Estado
        default:
            icon = "fa fa-question-circle";
            card = "card-sin-estado";
            break;

    }

    var paciente = (json.Paciente.length === 0) ? "" :
        `   <div class='mt-1'>
                <strong>${json.Paciente[0].nombreIdentificacion}</strong><br />
                <span>${json.Paciente[0].numero_documentoPaciente}</span>
            </div>
            <div class='mt-1'>
                <strong>Paciente</strong><br />
                <span>${json.Paciente[0].PersonasnombrePaciente}</span>
            </div>
            <div class='mb-0'>
                <strong>Fecha y Hora de Ingreso</strong><br />
                <span>${moment(json.Paciente[0].fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}</span>
            </div>
            <div class='text-center mt-2'>
                <a class='btn btn-light btn-block' data-l='21' onClick="VerMasInformacionHospitalizacion(${json.Paciente[0].idHospitalizacion});">
                    <i class="fa fa-info-circle"></i> Ver más 
                </a>
            </div>`;

    var html =
        `  <div class='card ${card}' style='min-width: 250px; margin-bottom: 0px !important;'>
                <div class='card-header text-center'>
                    <strong>${json.nombreCama}</strong>
                </div>
                <div class='card-body p-2'>
                    ${paciente}
                </div>
                <div class='card-footer'>
                    <div class='mb-0'>
                        <strong>Estado de Cama</strong><br />
                        <select id="sltCama_${json.Id}" class='form-control' data-idCama='${json.Id}'>
                            ${(card.includes("sin-estado")) ? "<option value='0' selected >Sin Estado</option>" : ""}
                            <option value='84' ${(card.includes("disponible")) ? "selected" : ""}>Cama Disponible</option>
                            <option value='85' ${(card.includes("ocupada")) ? "selected" : ""}>Cama Ocupada</option>
                            <option value='86' ${(card.includes("bloqueada")) ? "selected" : ""}>Cama Bloqueada</option>
                            <option value='99' ${(card.includes("deshabilitada")) ? "selected" : ""}>Cama Deshabilitada</option>
                        </select>
                    </div>
                    ${(!card.includes("disponible")) ? "" :
            `<div class="mt-3 mb-0 ml-0 mr-0 btn-group-vertical w-100">
                        <a id="aCamaSeleccionar" class='btn btn-success btn-block' onClick='SeleccionarPaciente(this,null);'>
                            <i class="fa fa-sync"></i> Seleccionar
                        </a>
                    </div>`}
                    ${(card.includes("disponible") || json.Paciente.length === 0) ? "" :
            `<div class="mt-3 mb-0 ml-0 mr-0 btn-group-vertical w-100">
                        <a id="aCamaSeleccionar" class='btn btn-success btn-block' onClick='SeleccionarPaciente(this,${json.Paciente[0].idHospitalizacion});'>
                            <i class="fa fa-sync"></i> Seleccionar
                        </a>
                        <a class='btn btn-info btn-block' onClick='CargarMovimientosHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-eye'></i> Mov. Hospitalización
                        </a>
                        <a class='btn btn-info btn-block' onClick='verTrasladosHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-eye'></i> Ver Traslados
                        </a>
                        <a class='btn btn-warning btn-block' onClick='VerEgresarHospitalizacion(${json.Paciente[0].idHospitalizacion});'>
                            <i class='fa fa-check'></i> Egresar
                        </a>
                    </div>`}
                </div>
            </div>`;

    return html;
}

function verTrasladosHospitalizacion(idHospitalizacion) {
    $("#mdlInfoPaciente").modal('hide')
    //De esta forma no se pierde el scroll del modal
    $("#mdlInfoPaciente").on("hidden.bs.modal", function (e) {
        CargarTrasladosHospitalizacion(idHospitalizacion)
    })
    setTimeout(() => {
        $("#mdlInfoPaciente").unbind();
    }, 1000)

}

function CargarMovimientosHospitalizacion(idHospitalizacion) {

    $("#mdlInfoPaciente").modal('hide')
    //De esta forma no se pierde el scroll del modal
    $("#mdlInfoPaciente").on("hidden.bs.modal", function (e) {
        const titulo = `Hospitalización #${idHospitalizacion}`;
        const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Movimientos`;
        getMovimientosSistemaGenerico(url, titulo);
    })
    setTimeout(() => {
        $("#mdlInfoPaciente").unbind();
    }, 1000)
}
function SeleccionarPaciente(sender) {

    $(".popover").unbind();
    $("[data-toggle=popover]").popover('hide');
    $(".background-popover").remove();
    var jsonCama = JSON.parse($(sender).data("jsonCama"));

    if (jsonCamaOrigen === null || jsonCamaOrigen === jsonCama) {
        jsonCamaOrigen = jsonCama;
    } else if (jsonCamaDestino === null || jsonCamaDestino === jsonCama)
        jsonCamaDestino = jsonCama;

    if (jsonCamaOrigen !== null && jsonCamaDestino !== null) {

        if (jsonCamaOrigen.GEN_idTipo_Estados_Sistemas === 85 || jsonCamaDestino.GEN_idTipo_Estados_Sistemas === 85)
            ShowTrasladoCama(jsonCamaOrigen, jsonCamaDestino);
        else
            toastr.warning('Debe seleccionar al menos una cama con una hospitalización.');

        LimpiarComponentes();
    }
}

function GetJsonHospitalizacion(idHospitalizacion) {

    var json = null;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Mapa/DetalleHospitalizacion/${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if (data.length > 0)
                json = data[0];

        }
    });

    return json;

}

function VerEgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = GetJsonHospitalizacion(HOS_idHospitalizacion);

    ShowModalAlerta("CONFIRMACION", "Confirmación",
        `   
            <div class='text-left' style='font-size:medium;'>
                
                <h5 class='text-center mb-4'><strong>Información de la hospitalización</strong></h5>
                
                <div class='row'>
                    <div class='col-md-6'>
                        <strong>${json.GEN_nombreIdentificacion}:</strong><br /> 
                        ${json.GEN_numero_documentoPaciente}
                    </div>
                    <div class='col-md-6'>
                        <strong>Fecha ingreso:</strong><br />
                        ${moment(json.HOS_fecha_ingreso_realHospitalizacion).format("DD-MM-YYYY HH:mm:SS")}
                    </div>
                </div>
            
                <div class='row'>
                    <div class='col-md-12'>
                        <strong>Nombre:</strong><br />
                        ${json.GEN_PersonasnombrePaciente}
                    </div>
                </div>
                <label for='txtFechaEgreso'>Fecha y hora de egreso</label>
                <div class="input-group mb-3">
                    <input id='txtFechaEgreso' type='date' class='form-control' aria-label="Recipient's username" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <input id='txtHoraEgreso' type='time' class='form-control clockpicker' /> 
                    </div>
                </div>
            </div>

            ¿Estás seguro qué quiere egresar a este paciente?
        `,
        function () { // Botón que egresa paciente

            var fechaActual = moment(GetFechaActual());
            var fechaEgreso = moment($("#txtFechaEgreso").val());
            var esValido = true;

            if ($("#txtFechaEgreso").val() === "" || !fechaEgreso.isBefore(fechaActual)) {
                $("#txtFechaEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtFechaEgreso").removeClass('invalid-input');

            if ($("#txtHoraEgreso").val() === "") {
                $("#txtHoraEgreso").addClass('invalid-input');
                esValido = false;
            } else $("#txtHoraEgreso").removeClass('invalid-input');

            if (esValido) {
                EgresarHospitalizacion(HOS_idHospitalizacion);
                $('#modalAlerta').modal('hide');
            }

        },
        function () { // Botón cancelar
            $('#modalAlerta').modal('hide');
        }, "Egresar", "Cancelar");

    $(".clockpicker").clockpicker({ autoclose: true });
}

function EgresarHospitalizacion(HOS_idHospitalizacion) {

    var json = {
        "HOS_fecha_egreso_realHospitalizacion": $("#txtFechaEgreso").val(),
        "HOS_hora_egreso_realHospitalizacion": $("#txtHoraEgreso").val()
    };

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Egreso/Enfermeria/${HOS_idHospitalizacion}`,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('El paciente se ha egresado correctamente.');
            // Una vez que se actualiza el mapa de camas, queda con el fondo del popover.
            $(".background-popover").remove();
            CargarMapaCamas(false);
        },
        error: function (jqXHR, status) {
            console.log("Error al egresar paciente: " + JSON.stringify(jqXHR));
        }
    });
}

async function cambiarEstadoCama(idCama, idEstado) {
    if (idCama !== null && idEstado !== null) {

        let titulo = ``, texto = `La cama <b>no estará disponible</b> para uso clínico, porfavor confirme la acción`
        switch (idEstado) {
            case 84://Hacer la cama disponible
                titulo = `Habilitando cama`
                texto = `La cama <b>estará disponible</b> para uso clínico , porfavor confirme la acción`
                break
            case 86://Bloquearla
                titulo = `Bloqueando cama`
                break;
            case 99://Deshabilitarla
                titulo = `Deshabilitando cama`
                break;

        }
        const result = await Swal.fire({
            title: titulo,
            html: texto,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
        })

        if (result.value) {
            promesaAjax("PATCH", `HOS_Cama/CambiarEstado/${idCama}/${idEstado}`).then(res => {
                CargarMapaCamas()
                let timerInterval
                Swal.fire({
                    title: 'El estado ha sido cambiado.',
                    text: 'Cambios realizados con éxito, actualizando información',
                    icon: 'success',
                    timer: 2000,
                    timerProgressBar: true
                }).then(result => {
                    $("#mdlInfoPaciente").modal("hide")
                })
            }).catch(error => {
                console.error("Ha ocurrido un erro al intentar cambiar el estado de la cama")
                console.log(error)
            })
        }
    }
}
const linkImprimirHospitalizacion = (idHosp) => {
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${idHosp}/Imprimir`;
    ImprimirApiExterno(url);
}