﻿

const irAEditarHospitalizacion = (id) => {
    setSession('ID_HOSPITALIZACION', id);
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevoFormularioAdmision.aspx`;
}
const linkMostrarModalMovimientosHospitalizacion = (sender) => {
    const id = $(sender).data("id");
    const titulo = `Hospitalización #${id}`;
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/Movimientos`;
    getMovimientosSistemaGenerico(url, titulo);
}


async function getHospitalizacionesDelPaciente(numeroDocumento) {

    if (numeroDocumento === undefined)
        return

    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Bandeja?numerodocumento=${numeroDocumento}`

    try {
        const hospitalizaciones = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return hospitalizaciones.Elementos

    } catch (error) {
        console.error("Error al cargar hospitalizaciones del paciente")
        console.log(JSON.stringify(error))
    }
}

const linkMostrarModalFormularios = (sender) => {
    $("#btnNuevoIC, #btnEpicrisis").data("idcama", $(sender).attr("data-idcama"))
    $("#btnNuevoIC, #btnEpicrisis").data("pacid", $(sender).attr("data-pacid"))
    $("#btnNuevoIPD, #btnNuevoIC, #btnNuevoEXA, #btnNuevoFQX , #btnNuevoIngresoMedico , #btnNuevaSolicitudTrasfusion").data("idEvento", $(sender).attr("data-idEvento"));
    $("#btnNuevoIPD, #btnNuevoIC, #btnNuevoEXA, #btnNuevoFQX, #btnNuevoIngresoMedico , #btnNuevaSolicitudTrasfusion, #btnEpicrisis").data("idPaciente", $(sender).attr("data-pacid"));
    //Id hopspitalizacion
    $("#btnNuevoIPD, #btnNuevoIC, #btnNuevoEXA, #btnNuevoFQX, #btnNuevoIngresoMedico , #btnNuevaSolicitudTrasfusion, #btnEpicrisis").data("id", $(sender).data("id"));
    $("#mdlFormularios").modal("show");
}

const linkBandejaIngresoMedico = (sender) => {


    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico) {
        $('#aIngresoMedico').show();
    } else {
        $('#aIngresoMedico').hide();
    }
    $("#aIngresoMedico").data("idPaciente", $(sender).data("pacid"));
    $("#aIngresoMedico").data("idHospitalizacion", $(sender).data("id"));


    $("#txtIdHospMdlIngresoMedico").text($(sender).data("id"))
    $("#txtNombrePacienteMdlIngresoMedico").text($(sender).data("pac"))

    LlenarGrillaIngresoMedico($(sender).data("id"));
    $('#divCarouselIngresoMedico').carousel(0);
    mostrarModal("mdlMovimientosIngresoMedico");
}

const irACrearNuevoIngresoMedico = () => {
    setSession('ID_EVENTO', $("#aIngresoMedico").data("idEvento"));
    setSession('ID_PACIENTE', $("#aIngresoMedico").data("idPaciente"));
    setSession('ID_HOSPITALIZACION', $("#aIngresoMedico").data("idHospitalizacion"));
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoIngresoMedico.aspx";
}

const irANuevaSolicitudST = (sender) => {
    deleteSession("ID_SOLICITUD_TRANSFUSION");
    setSession('SISTEMA', 'HOSPITALIZACION');
    setSession('ID_HOSPITALIZACION', $(sender).data("id"));
    setSession('ID_PACIENTE', $(sender).data("idpaciente"));
    window.location.href = ObtenerHost() + "/Vista/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.aspx";
}
// Anular Hoja de Enfermería
const linkAnularHojaEnfermeria = (sender) => {

    const id = $(sender).data("id")
    const idhosp = $(sender).data("idhosp")

    Swal.fire({
        title: '¿Seguro quieres anular la hoja?',
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {

            anularHojaEnfermeria(id, idhosp)
        }
    })

}
// Abrir formulario nueva hoja Enfermería
const irANuevaHojaEnfermeria = (idHospitalizacion, turno) => {

    const accion = "INGRESO"
    setSession('ID_HOSPITALIZACION', idHospitalizacion);
    setSession('ACCION', accion)
    setSession('TURNO', turno)
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevaHojaEnfermeria.aspx`;
}

// Editar Hoja de Enfermería
const linkEditarHojaEnfermeria = (sender) => {
    const id = $(sender).data("id")
    const idhosp = $(sender).data("idhosp")
    const idturno = $(sender).data("idturno")
    setSession('ID_HOJA_ENFERMERIA', id)
    setSession('ID_HOSPITALIZACION', idhosp)
    setSession('TURNO', idturno)
    //setSession('ACCION', "EDITAR")
    window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/NuevaHojaEnfermeria.aspx`
}

// Imprimir de Enfermería
const linkImprimirHojaEnfermeria = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

// Mostrar modal Hoja Enfermeria
const linkMostrarModalHojaEnfermeria = (sender) => {
    $("#mdlGestionPacienteEnfermeria").modal('hide')
    $('#mdlGestionPacienteEnfermeria').on('hidden.bs.modal', function (e) {
        const idHospitalizacion = $(sender).data("id");
        const pac = $(sender).data("pac");
        const cama = $(sender).data("cama");
        mostrarModal("mdlVerHojaEnfermeria");
        //$("#mdlVerHojaEnfermeria").modal("show")

        $('#nomPacModalHojaEnfermeria').html(pac);
        $('#numCamaModalHojaEnfermeria').html(cama);
        $('#numHospitalizacion').html(idHospitalizacion);

        //Tabla Hoja Enfermería por hospitalización
        getHojaEnfermeriaPorNHospitalizacion(idHospitalizacion, pac, cama);

        $("#btnNuevaHojaEnfermeria").unbind().click(function (e) {
            e.preventDefault();
            $("#titleHeaderTipoTurnoHE").text("Turnos Hoja de Enfemería / Matronería")
            $("#turnoDiurnoHE").data("id", idHospitalizacion)
            $("#turnoNocturnoHE").data("id", idHospitalizacion)
            $("#mdlVerHojaEnfermeria").modal("hide");
            $("#mdlNuevoTurnoHE").modal("show");
            //irANuevaHojaEnfermeria(idHospitalizacion);
        });

        $('#mdlGestionPacienteEnfermeria').unbind();
    })
}
// { Fecha, IdHospitalizacion, Diagnostico, Morbidos, IdTipoHojaEnfermeria, IdTipoTurno }
const postHojaEnfermeria = async (objHojaEnfermeria) => {

    ShowModalCargando(true)

    let parametrizacion = {
        type: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria`,
        mensaje: "Ingresado Correctamente"
    }

    $.ajax({
        type: parametrizacion.type,
        url: parametrizacion.url,
        data: JSON.stringify(objHojaEnfermeria),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data) {

            toastr.success(parametrizacion.mensaje)

            ShowModalCargando(false)
            getHojaEnfermeriaPorNHospitalizacion(objHojaEnfermeria.IdHospitalizacion)
            $("#mdlNuevoTurnoHE").modal("hide")
            showModalHojaEnfermeria()
        },
        error: function (err, status) {
            console.log("Error al ingresar hoja de enfermeria: " + JSON.stringify(err));
        }
    });
}

const creaNuevahojaEnfermeria = async (sender, turno) => {

    const idHospitalizacion = $(sender).data("id")

    const hospitalizacion = await getHospitalizacion(idHospitalizacion)

    const json = {
        Fecha: moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:ss"),
        IdHospitalizacion: idHospitalizacion,
        Diagnostico: hospitalizacion.DiagnosticoActual,
        Morbidos: hospitalizacion.Enfermeria.AntecedentesMorbidos,
        IdTipoHojaEnfermeria: 1, // deber ser null en el backend
        IdTipoTurno: turno
    }

    postHojaEnfermeria(json)


    //irANuevaHojaEnfermeria(idHospitalizacion, turno);
}

//const getTurnoNocturnoHE = (sender) => {
//    const idHospitalizacion = $(sender).data("id")
//    const nocturno = 2;

//    irANuevaHojaEnfermeria(idHospitalizacion, nocturno);
//}

const showModalHojaEnfermeria = () => {
    $("#mdlVerHojaEnfermeria").modal("show")
}

const showMensajeNoTieneCama = (id) => {
    const mensaje = `El paciente no registra servicio`

    Swal.fire({
        title: mensaje,
        text: "¿Desea ingresar al paciente?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            irAEditarHospitalizacion(id)
        }
    })
}

const linkVerTraslados = (sender) => {
    'use strict';
    // GEN_idUbicacion_Ingreso
    const id = $(sender).data("id");
    const egreso = $(sender).data("egreso");
    let bMostrarEditar;
    idUbicacionActualCamaTraslado = 0;

    idCama = $(sender).data('idcama');
    $('#idHos').val(id);
    const data = llenarGrillaTraslado(bMostrarEditar);


    if (!($.isEmptyObject(data)))
        idUbicacionActualCamaTraslado = data.Hospitalizacion.idUbicacion;

    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria && egreso == '') {
        bMostrarEditar = true;
        comboServicios('#ddlServicioDestino');
        mostrarServicioTrasladoExterno(idUbicacionActualCamaTraslado);
        $('#divNuevoTraslado').show();
        $('#dvSwitch, #ddlServicioDestino, #lblServicioDestinoTraslado, #ddlCamaDestino, #lblCamaDestinoTraslado, #aTraslado').show();
        $("#swTrasladoExterno").on("change", (e) => {
            mostrarServicioTrasladoExterno(idUbicacionActualCamaTraslado);
        });
    } else {
        bMostrarEditar = false;
        $('#dvSwitch, #ddlServicioDestino, #lblServicioDestinoTraslado, #ddlCamaDestino, #lblCamaDestinoTraslado, #aTraslado').hide();
    }

    $('#mdlTraslados').on('hidden.bs.modal', function () {
        $("#swTrasladosExterno").off();
    });

    $('#aTraslado').text('Agregar Traslado');
    mostrarModal("mdlTraslados");
}
const linkEditarTraslado = (sender) => {

    const idTraslado = $(sender).data("id");
    const idOrigen = $(sender).data("idorigen");

    $('#idTraslado').val(idTraslado);
    $('#aTraslado').text('Editar Traslado');

    $('#ddlServicioDestino').val('0');
    $('#ddlCamaDestino').val('0');
    $('button[data-id="ddlServicioDestino"]').html('Seleccione');
    $('button[data-id="ddlCamaDestino"]').html('Seleccione');

}

const BtnNuevoFormRCE = (sender) => {

    setSession('ID_CAMA', $(sender).data("idcama"));
    setSession('ID_PACIENTE', $(sender).data("idPaciente"));
    setSession('ID_HOSPITALIZACION', $(sender).data("id"));

    window.location.href = ObtenerHost() + "/" + $(sender).attr("data-href");

}


const linkMostrarModalEgreso = async (sender) => {

    const { value: tipoAtencion } = await Swal.fire({
        title: '¿Donde se va el paciente?',
        input: 'radio',
        allowOutsideClick: false,
        inputOptions: {
            '1': 'A otro servicio',
            '2': 'A su domicilio'
        },
        inputValidator: (value) => {
            if (!value) {
                return 'Debe seleccionar una opción'
            }
        }
    });

    if (tipoAtencion == '2') {
        setSession('ID_HOSPITALIZACION', $(sender).data("id"));
        setSession('ID_PACIENTE', $(sender).data("idpaciente"));
        window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoEgresoEnfermeria.aspx";
    } else if (tipoAtencion == '1') {
        $("#btnVerModalPacienteEnTransito").trigger("click");
    }

}

const linkMostrarModalGestionHospitalizacion = (sender) => {

    $(`#btnVerModalMovimiento,
       #btnVerModalHistorial,
       #btnVerModalIngresoMedico`).data("pac", $(sender).data("pac"))

    $(`#btnVerModalMovimiento,
       #btnVerModalHistorial,
       #btnVerModalIngresoMedico`).data("id", $(sender).data("id"))

    $(`#btnVerModalMovimiento,
       #btnVerModalHistorial,
       #btnVerModalIngresoMedico`).data("cama", $(sender).data("cama"))

    $(`#btnVerModalMovimiento,
       #btnVerModalHistorial,
       #btnVerModalIngresoMedico`).data("idpaciente", $(sender).data("idpaciente"))

    $("#txtModalGestionHospNombrePaciente").val($(sender).data("pac"))
    $("#txtModalGestionHospIdHospitalizacion").val($(sender).data("id"))
    $("#txtModalGestionHospNombreCama").val($(sender).data("cama"))

    $("#mdlGestionHospitalizacionEnfermeria").modal("show")
}


const BtnCargarNuevoForm = (sender) => {
    const vURL = 'http://10.6.180.235/PABELLON/ING_Form_IQX_SP.aspx?id=0&c=';
    const json = GetLoginPass();
    const txtEncrypt = GetTextoEncriptado(json.ID_USUARIO + '|' + json.login + '|' + json.pass + '|' + $(sender).data("idPaciente"));

    window.open(vURL + txtEncrypt, '_blank');
}

const irACrearNuevaHospitalizacion = (sender) => {

    setSession('ID_PACIENTE', $(sender).data("idpaciente"));
    setSession('ID_INGRESO_MEDICO', $(sender).data("idmed"));
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoFormularioAdmision.aspx";
}

const linkMostrarModalMovimientosHojaEnfermeria = (sender) => {
    const id = $(sender).data("id");
    const titulo = `Hoja Enfermería #${id}`;
    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/Movimientos`;

    getMovimientosSistemaGenerico(url, titulo)
}

const linkMostrarModalMovimientosIngresoMedico = (sender) => {
    const id = $(sender).data("id")
    const titulo = `Ingreso Médico #${id}`
    const _url = `${GetWebApiUrl()}RCE_Ingreso_Medico/${id}/Movimientos`

    getMovimientosSistemaGenerico(_url, titulo)
}
const linkImprimirHospitalizacion = (sender) => {
    const id = $(sender).data("id");
    
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/Imprimir`;
    console.log(url)
    ImprimirApiExterno(url);
}
const linkImprimirAltaEnfermeria = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion_Alta_Enfermeria/${id}/Imprimir`;
    ImprimirApiExterno(url);
}
const linkMostrarModalAnularHospitalizacion = (sender) => {
    const id = $(sender).data("id");

    $('#mdlAlertaCuerpo').empty();
    $('#mdlAlertaCuerpo').append('¿Esta seguro de anular la hospitalización?');
    $('#linkConfirmar').attr('onclick', 'anularHospitalizacion(' + id + ')');
    $('#linkConfirmar').addClass('btn-danger');

    mostrarModal("mdlAlerta");
}
const linkMostrarModalNeoNato = (sender) => {
    let pac = $(sender).data("pac")
    let cama = $(sender).data("cama")
    let idHospitalizacion = $(sender).data("id")

    $("#btnIrNuevaFechaNeoNatal").data("idpaciente", $(sender).data("idpaciente"))
    $("#btnIrNuevaFechaNeoNatal").data("idhosp", idHospitalizacion)
    $("#mdlGestionPacienteEnfermeria").modal("hide")
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}HOS_Preingreso_Neonatal/Buscar?idHospitalizacion=${idHospitalizacion}`,
        success: function (data) {

            $("#tblNeoNatal").addClass("tblBandeja").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        className: 'btn btn-info',
                        text: 'Exportar a Excel'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-info',
                        text: 'Exportar en PDF'
                    }
                ],
                destroy: true,
                autowidth: false,
                data: data,
                columns: [
                    { title: "Id", data: "Id" },
                    { title: "Anticuerpos", data: "TipoCantidadAnticuerpo.Valor" },
                    { title: "Nutricion", data: "TipoDatoNutricionales.Valor" },
                    { title: "Acciones", data: "Id", className: "text-center" }
                ], columnDefs: [
                    {
                        targets: -1,
                        render: function (id, row, data) {
                            return `
                            <button class="btn btn-primary btn-circle" onclick="toggle('#div_accionesNeoNatal${id}'); return false;">
                                        <i class="fa fa-list" style="font-size:15px;"></i>
                            </button>
                            <div id="div_accionesNeoNatal${id}" class="btn-container" style="display:none; z-index:2;">
                                 <center>
                                 <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                    <div class="rounded-actions">
                                       <center>
                                            <a class="btn btn-info btn-circle" onclick="irFichaNeoNatal(this)" data-id=${id} data-idpaciente="${$(sender).data("idpaciente")}" data-idhosp="${idHospitalizacion}" data-edit="true"
                                            data-toggle="tooltip" data-placement="left" title="Editar" data-original-title="Editar">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a data-id="${id}" class="btn btn-info btn-circle" onclick="linkImprimirFichaNeoNatal(this)"
                                            data-toggle="tooltip" data-placement="left" title="Imprimir" data-original-title="Imprimir">
                                                <i class="fas fa-print"></i>
                                            </a>
                                            <a data-id="${id}" class="btn btn-danger btn-circle" onclick="eliminarFichaNeoNatal(${id})"
                                            data-toggle="tooltip" data-placement="left" title="Eliminar" data-original-title="Eliminar">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </center>
                                    </div>
                                </center>
                            </div>  `
                        }
                    }
                ]
            })
        }, error: function (err) {
            console.error("ha courrido un errro al buscar los ingresos pre natales")
            console.log(err)
        }
    })


    $("#mdlFichaNeoNato").modal("show")
    $('#txtModalNeoNatoNombrePaciente').val(pac);
    $('#txtModalNeoNatoIdHospitalizacion').val(idHospitalizacion);
    $('#txtModalNeoNatoHospNombreCama').val(cama);
}





const irFichaNeoNatal = (sender) => {

    if ($(sender).data("edit") == true) {
        setSession('ID_FICHANEO', $(sender).data("id"));
    }
    setSession('ID_PACIENTE', $(sender).data("idpaciente"));
    setSession('ID_HOSPITALIZACION', $(sender).data("idhosp"));
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/FichaNeoNatal.aspx";


}
const eliminarFichaNeoNatal = (idFichaNeoNatal) => {
    Swal.fire({
        title: 'Desea eliminar el registro?',
        text: `Eliminando ficha con id ${idFichaNeoNatal}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Confirmar'
    }).then((result) => {
        if (result) {
            $.ajax({
                url: `${GetWebApiUrl()}HOS_Preingreso_Neonatal/${idFichaNeoNatal}`,
                method: "DELETE",
                success: function (data) {
                    Swal.fire({
                        title: 'Acción realizada',
                        html: "Ficha eliminada",
                        icon: 'success',
                        timer: 2000,
                        timerProgressBar: true,
                    }).then(result => {
                        if (result) {
                            $("#mdlFichaNeoNato").modal("hide")
                        }
                    })
                }, error: function (err) {
                    console.log("Ha ocurrido un error al intentar eliminar un ingreso pre natal")
                    console.log(err)
                }
            })
        }
    })
}
const linkImprimirIngresoMedico = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}RCE_Ingreso_Medico/${id}/Imprimir`;
    ImprimirApiExterno(url);
}
const linkImprimirFichaNeoNatal = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}HOS_Preingreso_Neonatal/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

