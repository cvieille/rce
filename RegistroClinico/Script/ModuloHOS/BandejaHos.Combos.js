﻿function comboUbicacion() {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, $('#ddlUbicacion'))
}

function comboEstado() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/HOSPITALIZACION`;
    let elemento = "#ddlHosEstado"
    setCargarDataEnCombo(url, false, elemento)
}

function comboTipoHospitalizacion() {
    let url = `${GetWebApiUrl()}HOS_Tipo_Hospitalizacion/Combo`;
    let elemento = "#ddlTipoHospitalizacion"
    setCargarDataEnCombo(url, false, elemento)
}

function comboServicios(element) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, element);
}