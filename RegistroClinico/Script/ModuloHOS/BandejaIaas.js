﻿let idDiv = "";
let sSession = getSession();
$(document).ready(function () {
    ShowModalCargando(false)
    obtenerAislamientos()
    obtenerInfoInvasivos()
})

function obtenerAislamientos() {
    let datasetAislamientos = []
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamientos/Bandeja?`+
        `&idIdentificacion=${$("#sltTipoIdentificacion").val() != 0 ? $("#sltTipoIdentificacion").val():""}`+
                `&numerodocumento=${$("#txtHosFiltroRut").val() ?? ""}`+
                `&nombrePaciente=${$("#txtHosFiltroNombre").val() ?? ""}`+
                `&apellidopaternoPaciente=${$("#txtHosFiltroApePaterno").val() ?? ""}`+
        `&apellidomaternoPaciente=${$("#txtHosFiltroApeMaterno").val() ?? ""}`
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key, item) {
                datasetAislamientos.push([
                    item.Paciente.Id,
                    item.IdHospitalizacion,
                    `${item.Paciente.Nombre ?? ""} ${item.Paciente.ApellidoPaterno ?? ""} ${item.Paciente.ApellidoMaterno ?? ""}`,
                    item.Paciente.NumeroDocumento,
                    item.Ubicacion != null ? item.Ubicacion.Valor : "N/A",
                    item.Cama != null ? item.Cama.Valor:"N/A"
                ]);
            });
            llenarGrilla("#tblAislamientos",datasetAislamientos)
        }, error: function (res) {
            console.log("Ha ocurrido un error al solicitar los aislamientos", res)
        }
    })
}
function obtenerInfoInvasivos() {
    let datasetInvasivos = []
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Invasivos/Bandeja`
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key, item) {
                datasetInvasivos.push([
                    item.Paciente.Id,
                    item.IdHospitalizacion,
                    `${item.Paciente.Nombre ?? ""} ${item.Paciente.ApellidoPaterno ?? ""} ${item.Paciente.ApellidoMaterno ?? ""}`,
                    item.Paciente.NumeroDocumento,
                    item.Ubicacion != null ? item.Ubicacion.Valor : "N/A",
                    item.Cama != null ? item.Cama.Valor : "N/A"
                ]);
            });
            llenarGrilla("#tblInvasivos", datasetInvasivos)
            //llenarGrilla(datasetAislamientos)
        }, error: function (res) {
            console.log("Ha ocurrido un error al solicitar los Invasivos", res)
        }
    })
}
function llenarGrilla(table,adataset){
    $(table).addClass('text-wrap').addClass('dataTable').DataTable({
        data: adataset,
        responsive: {
            details: false,
            type: 'column'
        },
        bDestroy: true,
        sDom: 'ltip',
        columns: [
              { title: "idPaciente" },
              { title: "Id Hosp" },
              { title: "Nombre paciente" },
              { title: "Numero documento" },
              { title: "Ubicacion" },
              { title: "Cama actual" },
              { title: "", className: "text-center" }
        ],
        columnDefs: [
            { targets: 0, visible: false },
            {
                targets: -1,
                data: null,
                orderable: false,
                visible:true,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones =
                        `<button class='btn btn-primary btn-circle' 
                               onclick='toggle("${table == "#tblAislamientos" ? `#div_accionesIM${fila}` : `#div_accionesInvasivo${fila}`}"); return false;'>
                               <i class="fa fa-list" style="font-size:15px;"></i>
                        </button>
                        <div id=${table == "#tblAislamientos" ? `div_accionesIM${fila}` : `div_accionesInvasivo${fila}`} class='btn-container' style="display: none;">
                             <center>
                             <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                             <div class="rounded-actions">
                             <center>
                                `;


                    botones += `<a id='linkNuevoAislamiento' data-id='${adataset[fila][1]}'
                                   data-pac="${adataset[fila][2]}"
                                   data-cama="${adataset[fila][5]}"
                                   data-ubicacion="${adataset[fila][4]}"
                                   data-idpaciente='${adataset[fila][0]}'
                                   data-nuevo='1' class='btn btn-warning btn-circle btn-lg' href='#/'
                                   data-tipo="${table == "#tblAislamientos" ? "aislamiento" : "invasivo"}"
                                   onclick='linkVerAislamiento(this);' data-toggle="tooltip" data-placement="left" 
                                   title="${table == "#tblAislamientos" ? `Ver Aislamiento` : `Ver invasivos`}">
                                   <i class='fa fa-${table == "#tblAislamientos" ? `low-vision`:`user-md`} nav-icon'></i>
                                </a><br>`;

                    botones += `
                             </center >
                             </div >
                             </center >
                         </div >`
                        ;
                    return botones
                }
            }
        ]
    })
}

function toggle(id) {
    if (idDiv == "") {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = "";
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}
function limpiarFiltros() {
    $("#txtHosFiltroId").val("")
    $("#sltTipoIdentificacion").val(0)
    $("#txtHosFiltroRut").val("")
    $("#txtHosFiltroNombre").val("")
    $("#txtHosFiltroApePaterno").val("")
    $("#txtHosFiltroApeMaterno").val("")
    obtenerAislamientos()
}