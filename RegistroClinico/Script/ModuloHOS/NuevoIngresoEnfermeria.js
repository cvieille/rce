﻿const sesion = getSession()
let Hospitalizacion = null, cantidadAntecedentes = 0
$(document).ready(async function () {

    if (sesion.ID_PACIENTE) {
        CargarPacienteActualizado(sesion.ID_PACIENTE)
    }

    if (sesion.ID_HOSPITALIZACION)
        Hospitalizacion = await getDatosHospitalizacionPorId(sesion.ID_HOSPITALIZACION, false)
    deleteSession('ID_PACIENTE');
    deleteSession('ID_HOSPITALIZACION');
    if (Hospitalizacion === null) {
        ShowModalCargando(false)
        return

    }

    //linkMostrarModalAlergias(null, 0, sesion.ID_PACIENTE)
    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#chkIngresoCompleto").bootstrapSwitch('state', false);

    $("#toltipInfo").tooltip()
    delete sesion.ID_HOSPITALIZACION
    delete sesion.ID_PACIENTE

    let fechaActualIngreso = await moment(GetFechaActual()).format("YYYY-MM-DD")
    let HoraActualIngreso = await moment(GetFechaActual()).format("HH:mm")

    $('#dpFechaIngreso').val(fechaActualIngreso)

    $("#dpFechaIngreso").attr("min", AgregarDias(fechaActualIngreso, -7));
    $("#dpFechaIngreso").attr("max", AgregarDias(fechaActualIngreso, 1));


    $("#dbFechaIngreso").val(fechaActualIngreso)
    $("#dbHoraIngreso").val(HoraActualIngreso)

    $("#txtMotivoConsultaPaciente").val(Hospitalizacion.MotivoHospitalizacion)

    $("#ddlServicioOrigen").change(function () {
        CargarCama($(this).val(), "#ddlCamaOrigenNuevo");
    });
    //Deshabilitar campos de busqueda de paciente
    $("#sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("disabled", "disabled");
    comboServicios('#ddlServicioOrigen');
    let option = $(`#ddlServicioOrigen option[value="${Hospitalizacion.Ingreso.Ubicacion.Id}"`)[0]
    console.log(Hospitalizacion)
    if (Hospitalizacion.EstadoSistema.Id!==156)
        option.disabled = true
    
    //$("#ddlServicioOrigen").val(Hospitalizacion.Ingreso.Ubicacion.Id)

    //$("#ddlServicioOrigen").change()

    //let textoServicioOrigen = $("#ddlServicioOrigen").find("option:selected").text()
    //if (!verificarCombo("ddlCamaOrigenNuevo"))
    //    mostrarMensajeCamaNoDisponible(textoServicioOrigen)

    //mostrarMensajeCamaNodisponibleChange("#ddlServicioOrigen")

    //dibujarAlternativasAntecedentesMorbidos()

    // CONTROLANDO LA FECHA PARA QUE NO SE PUEDA TECLEAR Y SOLAMENTE SE UTILICE EL CALENDARIO
    const dateInput = document.getElementById("dpFechaIngreso");

    // Evitar la entrada manual en el campo de fecha
    dateInput.addEventListener("keydown", function (event) {
        event.preventDefault();
    });

    // Configurar el selector de fecha
    dateInput.addEventListener("focus", function () {
        this.type = "date";
    });


    dibujarAntecedentes(1, "#antecedentesMorbidos")
    dibujarAntecedentes(2, "#habitosIngreso")
    dibujarAntecedentes(3, "#alergiasIngreso")

    ShowModalCargando(false);
})

function verificarCombo(camaOrigen) {
    var combo = document.getElementById(camaOrigen);
    return combo.options.length > 1;
}

function mostrarMensajeCamaNodisponibleChange(servicioOrigen) {

    $(servicioOrigen).change(function () {

        let textoServicioOrigenSeleccionado = $(this).find("option:selected").text()
        if (!verificarCombo("ddlCamaOrigenNuevo"))
            mostrarMensajeCamaNoDisponible(textoServicioOrigenSeleccionado)
    })
}

function mostrarMensajeCamaNoDisponible(servicio = "") {
    toastr.warning(`El servicio ${servicio} no tiene camas disponibles`);
}

function requiredAntecedentes(check) {
    let inputsAntecedentes = $(".chkAntecedentes")
    const divIngresoCompleto = document.getElementById('ingresoCompletoDiv');

    inputsAntecedentes.map((index, item) => {
        if ($(check).bootstrapSwitch('state')) {
            $(item).attr('data-required', true)
        } else {
            $(item).removeAttr('data-required')
            limpiarErorresCampos("antecedentesMorbidos")
            limpiarErorresCampos("habitosIngreso")
        }
    })

    if ($(check).bootstrapSwitch('state')) {
        divIngresoCompleto.style.display = "block";
    } else {
        divIngresoCompleto.style.display = "none";
    }

}
function comboServicios(element) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, element);
}
function CargarCama(GEN_idUbicacion, sltCama) {
    let url = `${GetWebApiUrl()}HOS_Cama/Hospitalizacion/GEN_Ubicacion/${GEN_idUbicacion}/Libres`;
    setCargarDataEnCombo(url, false, sltCama);
}

function dibujarAntecedentes(idClasificacion, div) {
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo?idClasificacion=${idClasificacion}&idModulo=4`,
        success: function (data) {
            cantidadAntecedentes += data.length
            $(div).empty()
            let contenido = ``
            data.map(antecedente => {
                let contenidoAlternativas = ``
                contenidoAlternativas += obtenerAlternativasAntecedentes(antecedente.Id)
                contenido += `
                <div class="col-md-3 col-sm-4">
                    <div class="col-sm-12 card">
                        <label>${antecedente.Valor}</label></br>
                        ${contenidoAlternativas}
                    </div>
                </div>
                `
            })
            $(div).append(contenido)
        }, error: function (error) {
            console.error(`Ha ocurido un error al obtener los antecedentes con id= ${idClasificacion}`)
        }
    })
}

function obtenerAlternativasAntecedentes(idAntecedente) {
    let alternativasAntecedente = `<table>`
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/${idAntecedente}/alternativas`,
        async: false,
        success: function (data) {
            data.map(alternativa => {
                alternativasAntecedente += `

                <button type="button"
                                class="btn btn-outline-info mt-2"
                                data-toggle="tooltip"
                                data-placement="right"
                                onClick="seleccionarRadio(${idAntecedente},'${alternativa.Valor}')"
                                data-html="true"
                                title="<div style='padding:2;'>
                    <td>
                        <div class="form-check form-check-inline">
                          <input id="checkAntecedente-${idAntecedente}-${alternativa.Valor}"  class="form-check-input chkAntecedentes" type="radio" data-required="true" name="checkAntecedente-${idAntecedente}"  value="${alternativa.Id}" data-idantecedente="${idAntecedente}" >
                          <label class="form-check-label" > ${alternativa.Valor} </label>
                         </div>
                    </td>

                 </button>

                `
            })

            alternativasAntecedente += '</table>'
        }, error: function (error) {
            console.error("ha ocurridoun error al intetar obtener las alternativas del antecedente")
            console.log(error)
        }
    })
    return alternativasAntecedente
}

function seleccionarRadio(id, puntaje) {
    let inputRadio = document.getElementById(`checkAntecedente-${id}-${puntaje}`)
    inputRadio.checked = true
}

function ingresarPacienteEnfermeria() {
    //let textoAlert = `Se ha establecido exitósamente la cama de origen para la hospitalización N° ${id}`

    if (validarCampos("#divCamaOrigen", false)) {
        json = {
            "IdHospitalizacion": Hospitalizacion.Id,
            "IdUbicacion": parseInt($("#ddlServicioOrigen").val()),
            "IdCama": parseInt($("#ddlCamaOrigenNuevo").val()),
            "ObservacionMorbidos": $("#txtAntecedentesMorbidos").val(),
            "Motivo": $("#txtMotivoConsultaPaciente").val(),
            "Diagnostico": $("#txtDiagnosticoHospitalizacion").val(),
            "ObservacionAlergias": $("#txtObservacionesAlergias").val(),
            "CondicionPaciente": $("#txtCondicionActualPaciente").val(),
            "ObservacionCirugias": $("#txtObservacionesQuirurgicos").val(),
            "Fecha": `${$("#dpFechaIngreso").val()} ${$("#dpHoraIngreso").val()}`
        }

        if (!validarCamposInputRadio("#antecedentesMorbidos", true) && $("#chkIngresoCompleto").bootstrapSwitch('state') === true) {
            toastr.error("Debe ingresar los antecedentes mórbidos")
            $("#divAntecedentesMorb").collapse('show')
            return false
        }

        if (!validarCamposInputRadio("#habitosIngreso", true) && $("#chkIngresoCompleto").bootstrapSwitch('state') === true) {
            toastr.error("Debe ingresar habitos")
            $("#divHabitos").collapse('show')
            return false
        }

        if (!validarCamposInputRadio("#alergiasIngreso", true) && $("#chkIngresoCompleto").bootstrapSwitch('state') === true) {
            toastr.error("Debe ingresar alergias")
            $("#divAlergias").collapse('show')
            return false
        }


        let antecedentesMorbidos = $("#antecedentesMorbidos").find("input[type='radio']:checked")
        let antecedentesHabitos = $("#divHabitos").find("input[type='radio']:checked")
        let antecedentesAlergias = $("#divAlergias").find("input[type='radio']:checked")
        let antecedentesChecks = [...antecedentesMorbidos, ...antecedentesHabitos, ...antecedentesAlergias]

        if (antecedentesChecks.length > 0) {
            if (antecedentesChecks.length !== cantidadAntecedentes) {
                toastr.error("Se debe rellenar el cuestionario de antecedentes mórbidos y hábitos completamente")
                return false
            }
            let antecedenteaApi = antecedentesChecks.map((item, index) => {
                return {
                    IdAntecedente: $(item).data("idantecedente"),
                    IdAlternativa: parseInt(item.value)
                }
            })
            json.Antecedentes = [...antecedenteaApi]
        }

        try {
            $.ajax({
                method: "POST",
                url: `${GetWebApiUrl()}HOS_Hospitalizacion/${Hospitalizacion.Id}/Enfermeria/Ingreso`,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                success: function (data) {
                    Swal.fire({
                        title: 'Ingresado con éxito',

                        html: `Hopitalizacion con id: ${Hospitalizacion.Id} ha sido ingresado con éxito`,
                        timer: 2000,
                        icon: 'success',
                        showConfirmButton: false
                    }).then((result) => {
                        window.location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
                    })
                }, error: function (error) {
                    console.error("Ha ocurrido un error al intentar ingresar al paciente")
                    console.log(error)
                }
            })
        } catch (error) {
            console.log(error)
        }
    }

}

//const linkIrIngresoEnfermeria = async (sender) => {

//    const idpaciente = $(sender).data("idpaciente")
//    const idhospitalizacion = $(sender).data("id")

//    const hospitalizacionesIngresadas = await getHospitalizacionesPaciente(idpaciente)

//    const hospitalizacionesEncontradas = pacTieneHospitalizacionesIngresadas(hospitalizacionesIngresadas, idhospitalizacion)

//    if (hospitalizacionesEncontradas !== undefined) {
//        const { Cama } = hospitalizacionesEncontradas
//        const servicio = (Cama !== undefined) ? Cama.Actual.Ubicacion.Valor : "No disponible"
//        const cama = (Cama !== undefined) ? Cama.Actual.Valor : "No disponible"
//        alertaPacienteYatieneHospitalizacion(servicio, cama)
//        return
//    }

//    try {
//        let idHosp = idhospitalizacion
//        $.ajax({
//            method: "GET",
//            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Enfermeria/Ingreso/Buscar?idHospitalizacion=${idHosp}`,
//            success: function (data) {

//                if (data.length == 0) {
//                    ingresarPacienteEnfermeria(sender)
//                } else {

//                    // Función para formatear la fecha
//                    function formatDate(dateString) {
//                        const options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' };
//                        return new Date(dateString).toLocaleDateString('es-ES', options);
//                    }

//                    // Recorrer y modificar los registros
//                    data.forEach(record => {
//                        record.Fecha = formatDate(record.Fecha);
//                    });

//                    $("#tblIngresosEnfermeria").DataTable({
//                        "order": [],
//                        dom: 'Bfrtip',
//                        buttons: [
//                            {
//                                extend: 'excel',
//                                className: 'btn btn-info',
//                                text: 'Exportar a Excel'
//                            },
//                            {
//                                extend: 'pdf',
//                                className: 'btn btn-info',
//                                text: 'Exportar en PDF'
//                            },
//                            {
//                                text: 'Nuevo Ingreso',
//                                className: 'btn btn-success',
//                                action: function () {
//                                    ingresarPacienteEnfermeria(sender)
//                                }
//                            }
//                        ],
//                        destroy: true,
//                        data: data,
//                        columns: [
//                            { title: "Id", data: "Id" },
//                            { title: "Fecha ingreso", data: "Fecha" },
//                            { title: "Cama", data: "Cama" },
//                            { title: "Ubicacion", data: "Ubicacion" },
//                            { title: "Motivo", data: "Motivo" },
//                            { title: "", data: "Id" },
//                        ], columnDefs: [
//                            {
//                                orderable: false,
//                                targets: -1,
//                                render: function (data, type, row, meta) {
//                                    let fila = meta.row;
//                                    //Renderizar botones de impresion
//                                    return `<a 
//                                        class="btn btn-outline-info btn-xl" 
//                                        data-id="${data}"
//                                        onclick="imprimirIngresoEnfermeria(this)"
//                                        >
//                                            <i class="fa fa-print"></i>
//                                       </a>`
//                                }
//                            },
//                            {
//                                targets: 2,//cama
//                                render: function (Cama) {
//                                    //Control de nulos
//                                    if (Cama == null)
//                                        return `Sin cama`
//                                    else
//                                        return Cama.Valor ?? `Sin cama`
//                                }
//                            },
//                            {
//                                targets: 3,//ubicacion
//                                render: function (Ubicacion) {
//                                    //control de nullos
//                                    if (Ubicacion == null)
//                                        return `Sin ubicación`
//                                    else
//                                        return Ubicacion.Valor ?? `Sin ubicación`

//                                }
//                            }
//                        ]
//                    })
//                    linkIrUserControlModal(sender, '#mdlGestionPacienteEnfermeria')

//                }
//            }, error: function (error) {
//                console.error(" ha ocurrido un error al intentar obtener los ingresos de enferrmeria")
//                console.log(error)
//            }
//        })
//    } catch (error) {
//        console.log(error)
//    }
//}