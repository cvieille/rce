﻿let idHospitalizacion, idFichaNeo, selectedDiagnostics = [], antecedentesObstetricos = [], habitos = [], antecedentesArray = [], dataTableExamenes, examenesArray = []
$(document).ready(async function () {

    cargarAntecedentesCombo()
    cargarGrupoSanguineo()
    cargarExamenes()

    if (getSession().ID_PACIENTE !== undefined) {
        CargarPacienteActualizado(getSession().ID_PACIENTE)
        BloquearPaciente()
    }
    $("#checkExamen").bootstrapSwitch({
        onColor: 'info',
        offColor: 'danger',
        onText: 'Positivo',
        offText: 'Negativo'
    })

    $("#sulfmag").bootstrapSwitch({
        onColor: 'info',
        offColor: 'danger',
        onText: 'Si',
        offText: 'No'
    })

    if (getSession().ID_HOSPITALIZACION !== undefined) {
        idHospitalizacion = getSession().ID_HOSPITALIZACION;
        getDatosHospitalizacionPorId(getSession().ID_HOSPITALIZACION)
    }

    ShowModalCargando(false)

    document.getElementById("chkControlPrenatal").addEventListener('change', function () {
        if (this.checked) {
            $("#divControlPrenatal").removeClass("d-none")
        } else {
            $("#divControlPrenatal").addClass("d-none")
        }
    })
    ValidarNumeros()


    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Datos_Nutricionales/Combo`, true, "#sltEstadoNutricional")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Tipo_Cantidad_Anticuerpo/Combo`, true, "#sltCoombs")
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Vacuna/Combo`,
        success: function (data) {
            $("#sltVacunas").empty()
            data.forEach(item => {
                $("#sltVacunas").append(`<option value="${item.Id}">${item.Valor}</option>`)

            })
            $("#sltVacunas").selectpicker();
        }, error: function (error) {
            console.error("Error alc argar vacunas")
            console.log(error)
        }
    })

    CargarDiagnosticosCIE10()
    if (getSession().ID_FICHANEO !== undefined) {
        idFichaNeo = getSession().ID_FICHANEO
        cargarFichaNatal(idFichaNeo)
    }
    deleteSession("ID_FICHANEO")

    dataTableExamenes = $("#tlbExamenes").DataTable({
        data: examenesArray,
        destroy: true,
        columns: [
            { title: "Id", data: "IdCartera" },
            { title: "Examen", data: "Nombre" },
            { title: "Fecha", data: "Fecha" },
            { title: "Resultado", data: "Resultado" },
            { title: "Acciones", data: "IdCartera" }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (id, row, data, meta) {
                    return `<button class="btn btn-circle btn-danger btn-sm" type="button" onclick="eliminarExamen(${id},this)"><i class="fa fa-trash"></i></button>`
                }
            },
            {
                targets: -2,
                render: function (resultado) {
                    if (resultado)
                        return `<span class="badge badge-success">Positivo</span>`
                    else
                        return `<span class="badge badge-danger">Negativo</span>`
                }
            }
        ]
    })
})

function cargarExamenes() {
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/Combo?idTipoArancel=5`,
        success: function (res) {
            let contenido = ``
            $("#sltExamenes").empty()
            res.map(examen => {
                contenido += `<option value=${examen.IdCarteraServicio}> ${examen.ServicioCartera.Valor}</option>`
            })
            $("#sltExamenes").append(contenido)
        }, error: function (error) {
            console.error("Ha ocurrido un error al intentar buscar los examenes")
            console.log(error)
        }
    })
}

function agregarExamen() {
    let esValido = true
    let handlerExamenes = {
        set: function (obj, prop, value) {
            if (prop !== "Resultado") {
                if (value == "") {
                    toastr.error(`Error: ${prop} es obligatorio`)
                    esValido = false
                    return false
                }
            }

            obj[prop] = value;
        }
    }
    const examenSeleccionado = new Proxy({}, handlerExamenes)
    let existe = examenesArray.find(exm => exm.IdCartera == parseInt($("#sltExamenes").val()))
    console.log(existe)
    if (existe == undefined) {
        examenSeleccionado.IdCartera = parseInt($("#sltExamenes").val())
        examenSeleccionado.Nombre = $("#sltExamenes>option:selected").text()
        examenSeleccionado.Resultado = $("#checkExamen").bootstrapSwitch('state')
        examenSeleccionado.Fecha = $("#dbFechaExamen").val()
        if (esValido) {
            examenesArray.push(examenSeleccionado)
            dataTableExamenes.row.add(examenSeleccionado).draw();
        }
    } else {
        toastr.error("El examen seleccionado ya ha sido ingresado")
    }
}
function eliminarExamen(idExamen, button) {
    examenesArray = examenesArray.filter(examen => examen.IdCartera !== idExamen)
    dataTableExamenes.row(button.parentNode.parentNode).remove().draw(false)
}
function cargarGrupoSanguineo() {
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`, true, "#sltGrupoSanguineoMadre")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`, true, "#sltGrupoSanguineoHijo")
}
function cargarAntecedentesCombo() {
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo`,
        success: function (data) {
            let contenidoObstetricos = ``, contenidoHabitos = ""
            antecedentesObstetricos = data.find(x => x.Valor == "Antecedentes Obstétricos")
            habitos = data.find(x => x.Valor == "Habitos")
            contenidoObstetricos = antecedentesObstetricos.Antecedentes.map(item => {
                return `<div class="col-md-2">
                            <label>${item.Valor}</label>
                            <input class="form-control" id="antecedentes-${item.Id}" number" maxlength="2" data-idantecedente=${item.Id} type="text" placeholder="${item.Valor}" value="" />
                        </div>
                       `
            })
            $("#divAntecedentesObstetricos").append(contenidoObstetricos)

            contenidoHabitos = habitos.Antecedentes.map(item => {
                return `
                        <div class="col-md-2">
                            <label>${item.Valor}</label><br />
                            <input type="radio" id="checkTrueAntecedentes-${item.Id}" name="${item.Id}"  data-name="${item.Valor}" data-id="${item.Id}"  onchange="mostrarAntecedentes(this)" value=true />SI
                            <input type="radio" id="checkFalseAntecedentes-${item.Id}" name="${item.Id}" data-name="${item.Valor}" data-id="${item.Id}"  onchange="mostrarAntecedentes(this)" value=false checked="checked" />NO
                            <input type="text" id="antecedentes-${item.Id}" data-id="${item.Id}" placeholder="Descripción de ${item.Valor}" onchange="mostrarCheck(this)" class="form-control d-none" /> 
                        </div>
                       `
            })
            $("#divHabitos").append(contenidoHabitos)


        }, error: function (error) {
            console.error("Ha ocurrido un error al intentar obtener los antecedentes")
            console.log(error)
        }
    })
}

function guardarPreNatal() {
    let metodo = "POST", url = `${GetWebApiUrl()}HOS_Preingreso_Neonatal`, mensaje = "Ficha prenatal ingresada"
    let inputsAntecedentes = $("#divAntecedentesObstetricos").find("input")
    let AntecedentesObs = inputsAntecedentes.map((index, item) => {
        if (item.value !== "") {
            return { IdAntecedente: $(item).data("idantecedente"), Descripcion: parseInt($(item).val()) }
        }
    })
    let inputsHabitos = $("#divHabitos").find("input:checked")
    habitos = inputsHabitos.map((index, item) => {
        if (item.value == "true") {
            let elemento = $(item).attr("name")
            return {
                IdAntecedente: parseInt(item.dataset.id),
                Descripcion: $(`#antecedentes-${elemento}`).val()
            }
        }
    })

    antecedentesArray = [...AntecedentesObs, ...habitos]

    let vacunas = $("#sltVacunas").selectpicker("val").map(vacuna => parseInt(vacuna))

    let ingresoPreNatal = {
        IdHospitalizacion: idHospitalizacion,
        FechaFur: $("#dataFurOperacional").val(),
        ControlPrenatal: document.getElementById("chkControlPrenatal").checked,
        EdadGestional: $("#txtEdadGestacional").val(),
        NumerosControles: $("#txtNumControles").val(),
        DaroPreingreso: $("#txtDaro").val(),
        IdTipoCantidadAnticuerpo: $("#sltCoombs").val(),
        IdTipoDatoNutricionale: $("#sltEstadoNutricional").val(),
        Farmacos: $("#txtFarmacoEmbarazo").val(),
        Antibioticos: $("#txtAntibiotico").val(),
        Fecha1raEcografia: $("#datePrimeraEco").val(),
        FechaUltima_ecografia: $("#dateUltimaEco").val(),
        Epf1raEcografia: $("#epfPrimeraEco").val(),
        EpfUltimaEcografia: $("#epfUltimaEco").val(),
        OtrosAntecedentes: $("#txtOtrosAntecedentes").val(),
        DescripcionMalformacion: $("#txtDescripcionMalformacion").val(),
        SulfatoMagnesio: $("#sulfmag").bootstrapSwitch('state'),
        Fecha1raCorticoides: $("#dpPrimerCorticoide").val(),
        Detalles1raCorticoides: $("#txtPrimerCorticoide").val(),
        Fecha2daCorticoides: $("#dpSegundoCorticoide").val(),
        Detalles2daCorticoides: $("#txtSegundoCorticoide").val(),
        Diagnosticos: selectedDiagnostics,
        IdGrupoSanguineoMadre: parseInt($("#sltGrupoSanguineoMadre").val()),
        IdGrupoSanguineoHijo: parseInt($("#sltGrupoSanguineoHijo").val()),
        Antecedentes: antecedentesArray,
        Vacunas: vacunas,
        Examenes: examenesArray
    }
    if (idFichaNeo) {
        metodo = "PUT", url = `${GetWebApiUrl()}HOS_Preingreso_Neonatal/${idFichaNeo}`, mensaje = "Ficha prenatal etidata"
    }
    $.ajax({
        url: url,
        method: metodo,
        data: JSON.stringify(ingresoPreNatal),
        contentType: "application/json",
        success: function (data) {
            swal.fire({
                title: 'Acción realizada',
                html: mensaje,
                timer: 2000,
                timerProgressBar: true,
            }).then((result) => {
                console.log(result)
                //if (result.dismiss === Swal.DismissReason.timer) {
                //    //window.location = "Vista/ModuloHOS/BandejaHOS.aspx"
                //    location.href = ObtenerHost() + "/Vista/ModuloIC/BandejaHOS.aspx";
                //}
                if (result) {
                    location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
                }
            })
        }, error: function (err) {
            console.log("Ha ocurrido un error al intentar guardar un ingreso pre natal")
            console.log(err)
        }
    })
}
function cargarFichaNatal(idFichaNeo) {
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}HOS_Preingreso_Neonatal/${idFichaNeo}`,
        success: function (data) {

            $("#dataFurOperacional").val(moment(data.FechaUltima_ecografia).format("YYYY-MM-DD"))
            if (data.ControlPrenatal) {
                $("#chkControlPrenatal").attr("checked", true).trigger('change')
            }
            $("#sltCoombs").val(data.TipoCantidadAnticuerpo !== null ? data.TipoCantidadAnticuerpo.Id : "0")
            $("#sltEstadoNutricional").val(data.TipoDatoNutricionales != null ? data.TipoDatoNutricionales.Id : "0")
            $("#datePrimeraEco").val(moment(data.Fecha1raEcografia).format("YYYY-MM-DD"))
            $("#dateUltimaEco").val(moment(data.FechaUltima_ecografia).format("YYYY-MM-DD"))
            $("#epfPrimeraEco").val(data.Epf1raEcografia)
            $("#epfUltimaEco").val(data.EpfUltimaEcografia)
            $("#txtAntibiotico").val(data.Antibioticos)
            $("#txtFarmacoEmbarazo").val(data.Farmacos)
            $("#txtOtrosAntecedentes").val(data.OtrosAntecedentes)
            $("#txtDescripcionMalformacion").val(data.DescripcionMalformacion)
            $("#sltGrupoSanguineoHijo").val(data.GrupoSanguineoHijo != null ? data.GrupoSanguineoHijo.Id : "0")
            $("#sltGrupoSanguineoMadre").val(data.GrupoSanguineoMadre != null ? data.GrupoSanguineoMadre.Id : "0")


            $("#txtEdadGestacional").val(data.EdadGestional)
            $("#txtNumControles").val(data.NumerosControles)
            $("#txtDaro").val(data.DaroPreingreso)
            if (data.Vacunas.length > 0) {
                let vacunasCargar = data.Vacunas.map(vac => vac.Id)
                $("#sltVacunas").selectpicker("val", vacunasCargar)
            }

            //carga de examenes
            if (data.Examenes.length > 0) {
                examenesArray = data.Examenes.map(examen => {
                    return {
                        IdCartera: examen.Cartera.Id,
                        Nombre: examen.Cartera.Valor,
                        Resultado: examen.Resultado,
                        Fecha: moment(examen.Fecha).format("YYYY-MM-DD")
                    }
                })
                examenesArray.map(item => dataTableExamenes.row.add(item).draw())
            }

            //Carga de diagnostico CIE10
            if (data.Diagnosticos.length > 0) {
                data.Diagnosticos.forEach(diagnostico => {
                    addRow({ GEN_idDiagnostico_CIE10: diagnostico.Id, GEN_descripcionDiagnostico_CIE10: diagnostico.Valor })
                })
            }
            //Carga de antecedentes obstetricos y habitos
            if (data.Antecedentes.length > 0) {
                data.Antecedentes.forEach(ant => {
                    $(`#antecedentes-${ant.IdAntecedente}`).val(ant.Descripcion).trigger("change")
                })
            }
        }, error: function (err) {
            console.error("Ha ocurrido al intentar obtener una fecha neo natal por su id")
        }
    })
}

function CargarDiagnosticosCIE10() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $("#txtDiagnosticoCIE10").typeahead(setTypeAhead(data));
        }
    });
}
function setTypeAhead(data) {
    var typeAhObject = {
        input: '#txtDiagnosticoCIE10',
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnóstico especificado ({{query}})",
        group: {
            key: "GEN_descripcion_padreDiagnostico_CIE10",
            template: function (item) {
                return item.GEN_descripcion_padreDiagnostico_CIE10;
            }
        },
        display: ["GEN_descripcionDiagnostico_CIE10"],
        template:
            '<span>' +
            '<span class="GEN_descripcionDiagnostico_CIE10">{{GEN_descripcionDiagnostico_CIE10}}</span>' +
            '</span>',
        correlativeTemplate: true,
        source: {
            GEN_descripcionDiagnostico_CIE10: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                if (!diagnosticExists(item.GEN_idDiagnostico_CIE10))
                    addRow(item);
                var time = setTimeout(function () {
                    $("#txtDiagnosticoCIE10").val("");
                    clearTimeout(time);
                }, 10);
            }
        }
    };

    $("#txtDiagnosticoCIE10").on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    });

    return typeAhObject;
}
function diagnosticExists(id) {
    var exist = false;
    $("#tblDiagnosticos tbody tr").each(function (index) {
        if ($(this).attr("data-idDiagnostico") == id)
            exist = true;
    });
    return exist;
}
function addRow(json) {
    var idTr = "trDiagnosticos_" + $("#tblDiagnosticos tbody").length;
    var colspan = 1;

    var tr =
        `
        <tr id='${idTr}' data-idDiagnostico = '${json.GEN_idDiagnostico_CIE10}' class='text-center'>
            <td class='pt-1 pb-1' style='font-size:20px !important;' colspan= ${colspan}>${json.GEN_descripcionDiagnostico_CIE10}</td>
            <td class='text-center pt-1 pb-1'>
                <a class='btn btn-circle-pretty-small btn-danger' data-idDiagnostico = '${json.GEN_idDiagnostico_CIE10}' style='color: #ffffff;'>
                    <i class='fa fa-times'></i>
                </a>
            </td>
        </tr>`;

    $("#tblDiagnosticos tbody").append(tr);
    $("#tblDiagnosticos").prop("style", "display: default;");
    $("#txtDiagnosticoCIE10").removeClass("invalid-input");

    selectedDiagnostics.push(json.GEN_idDiagnostico_CIE10);

    $("#" + idTr + " td a").click(function () {

        $(this).parent().parent().remove();
        for (var i = 0; i < selectedDiagnostics.length; i++) {
            if (selectedDiagnostics[i] == $(this).attr("data-idDiagnostico")) {
                selectedDiagnostics.splice(i, 1);
                break;
            }
        }

        if ($.trim($("#tblDiagnosticos tbody").html()) === "")
            $("#tblDiagnosticos").hide();

        $(this).unbind();

    });
}
function limpiarDiagnosticos() {
    $('#tblDiagnosticos').hide();
    $('#tblDiagnosticos tbody').empty();
    $('#txaIndicacionesAlta').val("");
    selectedDiagnostics = [];
}
function validarDiagnosticos() {
    if (selectedDiagnostics.length === 0) {
        $("#txtDiagnosticoCIE10").addClass("invalid-input");
        return false;
    } else {
        $("#txtDiagnosticoCIE10").removeClass("invalid-input");
        return true;
    }
}
function mostrarAntecedentes(sender) {
    let tipoAntecedente = $(sender).attr("name")

    if (sender.value == "true") {
        $(`#antecedentes-${tipoAntecedente}`).removeClass("d-none")
        $(`#antecedentes-${tipoAntecedente}`).data("required", true)
    } else {
        $(`#antecedentes-${tipoAntecedente}`).data("required", false)
        $(`#antecedentes-${tipoAntecedente}`).addClass("d-none")
    }
}
function mostrarCheck(sender) {
    let id = sender.dataset.id
    $(`#checkTrueAntecedentes-${id}`).attr("checked", true).trigger("change")
}