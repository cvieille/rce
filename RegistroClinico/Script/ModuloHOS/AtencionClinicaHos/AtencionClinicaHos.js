﻿// PROBANDO
let categorizacion = {};
let IdAtencionClinicaHospitalizacion;

$(document).ready(async function () {

    // Agrega clase col-d-12 si el ancho es 1207 para responsive
    window.addEventListener('resize', function () {
        checkResolucionUrg(1207);
    });

    $("#ultimaTomaSignosHos").hide()

    ShowModalCargando(false)

    nSession = getSession();

    let json = GetJsonIngresoHos(nSession.ID_HOSPITALIZACION);

    console.log("LA HOSPITALIZACION", json);

    // Mostrar alerta al cargar la pagina para validar usuario logeado
    Swal.fire({
        title: 'Usted ha iniciado sesi&oacute;n como',
        html: `${nSession.NOMBRE_USUARIO} <br><br>
        <p style="font-size: 14px;">Si este usuario no corresponde, por favor haga click en el bot&oacute;n <strong>"Cambiar Usuario"</strong>
        para evitar que el formulario salga a nombre de otra persona.</p>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cambiar usuario',
        allowEscapeKey: false,
        allowOutsideClick: false,
    }).then((result) => {
        if (result.isConfirmed) {
            // Se cierra alerta y entramos en la atencion clinic hos
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Si hace click en "Cambiar usuario" devuelve al login
            window.location.replace(`${ObtenerHost()}/Vista/Default.aspx`);
        }
    });

    IdAtencionClinicaHospitalizacion = nSession.ID_HOSPITALIZACION;

    // Paciente
    await cargarPaciente(json.Paciente.Id);

    // Profesional
    await cargarProfesional();

    categorizacionHos = await getUltimaCategorizacion(nSession.ID_HOSPITALIZACION)

    await cargarCategorizacion(categorizacionHos);

    await cargarInformacionAdicionalHos();

    console.log("LA CATEGORIZACION", categorizacionHos)
})


// Profesional que atiende la atencion
async function cargarProfesional() {
    const profesional = await getProfesionalPorId(nSession.ID_PROFESIONAL);

    // Asignar rut al span
    document.getElementById("txtNumeroDocumentoProfesional").textContent = profesional.NumeroDocumento ? `${profesional.NumeroDocumento}${profesional.Digito ? `-${profesional.Digito}` : ''}` : '';

    // Asignar nombre a span
    document.getElementById("txtNombreProfesional").textContent = profesional.Nombre;
    // Asignar apepat a span
    document.getElementById("txtApePatProfesional").textContent = profesional.ApellidoPaterno;
    // Asignar apemat a span
    document.getElementById("txtApeMatProfesional").textContent = profesional.ApellidoMaterno;
}

// Paciente
async function cargarPaciente(id) {
    const paciente = await getPacientePorId(id);

    const hospitalizacion = await GetJsonIngresoHos(IdAtencionClinicaHospitalizacion);

    const profesionalId = hospitalizacion?.DatosSolicitud?.Profesional?.Id;

    const profesional = await getProfesionalPorId(profesionalId);

    let nombreCompletoPac = paciente?.NombreSocial !== null ?
        `(${paciente.NombreSocial}) ${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}` :
        `${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}`;

    // Asignar nombre completo al span
    document.getElementById("txtNombrePacienteHos").textContent = nombreCompletoPac;

    // Cambiar valor del rut a span
    document.getElementById("txtRutPacienteHos").textContent = paciente.NumeroDocumento ? `${paciente.NumeroDocumento}${paciente.Digito ? `-${paciente.Digito}` : ''}` : '';

    // Asignar y Formatear fecha nacimiento
    document.getElementById("txtFechaNacHos").textContent = paciente.FechaNacimiento ? moment(paciente.FechaNacimiento).format("DD-MM-YYYY") : '';

    // Asignar edad al span
    document.getElementById("txtEdadHos").textContent = paciente.Edad?.EdadCompleta || '';

    // Asignar sexo al span
    document.getElementById("txtSexoHos").textContent = paciente.Sexo?.Valor || '';

    // Asignar genero al span
    document.getElementById("txtGeneroHos").textContent = paciente.Genero?.Valor || '';

    // Asignar el valor de prevision al span
    document.getElementById("txtPrevisionHos").textContent = `${paciente.Prevision?.Valor || ''} ${paciente.PrevisionTramo?.Valor || ''}`.trim();

    // Asignar fecha y hora de ingreso de admision
    document.getElementById("txtHoraIngresoHos").textContent = paciente.FechaActualizacion
        ? `${moment(paciente.FechaActualizacion).format("HH:mm")} | ${moment(paciente.FechaActualizacion).format("DD-MM-YYYY")}`
        : '';

    // Profesional que solicita
    document.getElementById("txtAdmisorHos").textContent = `${profesional.Nombre} ${profesional.ApellidoPaterno} ${profesional.ApellidoMaterno}`.trim();

    console.log("EL PACIENTE", paciente);

    // Devuelve el nombre completo del paciente
    return nombreCompletoPac;
}

async function cargarCategorizacion(categorizacion) {

    if (!categorizacion || categorizacion.Categorizacion === undefined) {
        $("#divCategorizacionVacioHos").show();
        $("#divCategorizacionHos").hide();
        $("#divCategorizacionInfoHos").hide();
        return;
    }

    const categorizacionCodigo = categorizacion.Categorizacion;
    const fechaHora = categorizacion.Fecha ? moment(categorizacion.Fecha).format("HH:mm | DD-MM-YYYY") : "Sin información";
    const usuarioCategorizador = categorizacion.Profesional.Persona.Nombre + " " + categorizacion.Profesional.Persona.ApellidoPaterno + " " + categorizacion.Profesional.Persona.ApellidoMaterno;

    let colorDivClass = '';
    let textColorClass = '';

    // Asignando clases de color según la categorización con un switch
    switch (categorizacionCodigo.charAt(0)) {
        case 'A':
            colorDivClass = 'bg-danger';
            textColorClass = 'text-white';
            break;
        case 'B':
            colorDivClass = 'bg-warning';
            textColorClass = 'text-black';
            break;
        case 'C':
            colorDivClass = 'bg-info';
            textColorClass = 'text-white';
            break;
        case 'D':
            colorDivClass = 'bg-success';
            textColorClass = 'text-white';
            break;
        default:
            colorDivClass = 'bg-secondary';
            break;
    }

    const divHTML = `<div class='p-1 ${colorDivClass} rounded'>
                        <h1 class='text-center m-0 ${textColorClass}'><strong>${categorizacionCodigo}</strong></h1>
                    </div>`;
    $("#divCategorizacionHos").append(divHTML);
    $("#divCategorizacionVacioHos").hide();

    $("#txtFechaHoraCategorizacionHos").text(fechaHora);
    $("#txtUsuarioCategorizadorHos").text(usuarioCategorizador);
}

async function cargarInformacionAdicionalHos() {

    const hospitalizacion = await GetJsonIngresoHos(IdAtencionClinicaHospitalizacion);

    // Asignar span acompañante
    document.getElementById("txtAcompananteHos").textContent = hospitalizacion["Acompañante"] || 'Sin informaci\u00F3n de acompa\u00F1ante';

    // Asignar motivo de hospitalizacion desde GetJsonIngresoHos
    document.getElementById("txtMotivoConsultaHos").textContent = hospitalizacion.MotivoHospitalizacion || 'Sin motivo registrado';

    // Asignar span ubicacion y cama
    document.getElementById("txtLugarPacienteHos").textContent = `${hospitalizacion.Ingreso.Ubicacion?.Valor} - ${hospitalizacion.CamaActual?.Valor}`;

    // Asignar antecedentes mórbidos
    const antecedentesMorbidos = hospitalizacion.Enfermeria?.AntecedentesMorbidos || 'Sin antecedentes mórbidos registrados';
    $("#divAntecedentesMorbidosHos").html(antecedentesMorbidos);
}