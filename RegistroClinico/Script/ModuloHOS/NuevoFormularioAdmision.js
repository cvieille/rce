﻿
let idHospitalizacion = null;
let idEvento = null;
var sSession = {};
var HOS_Hospitalizacion = {};
var hosFlag = false;
//const FILAS_DIAGNOSTICOS = 1
let idMedicoSolicita = null
let coincide = false
let profesionalesIdValor = []
let idSolicitudHospitalizacion = 0

const fechaHoy = moment(GetFechaActual()).format("YYYY-MM-DD");
var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

// DOCUMENT
$(document).ready(async function () {

    let IniciarComponentes = async () => {

        ShowModalCargando(true);
        defineFechaMaximayMinimadeIngreso();
        defineFechaMaximadeSolicitud();


        $("textarea").val("");
        $(window).scroll(function () {
            if (hosFlag) {
                ($($(this)).scrollTop() > 150) ? $('#alertHospitalizacionActiva').stop().show('fast') : $('#alertHospitalizacionActiva').stop().hide('fast');
            }
        });

        window.addEventListener('beforeunload', bunload, false);

        // Llena combos
        EstablecerSesion();
        RevisarAcceso(true, sSession);

        LlenarCombos();

        $.fn.bootstrapSwitch.defaults.onColor = 'info';
        $.fn.bootstrapSwitch.defaults.offColor = 'danger';
        $.fn.bootstrapSwitch.defaults.onText = 'SI';
        $.fn.bootstrapSwitch.defaults.offText = 'NO';
        $.fn.bootstrapSwitch.defaults.labelWidth = '1';
        DarFuncionRadios();
        await InicializarComponentes();
        ocultarPuebloOriginarioNivelEducacional()
        //CargarDiagnosticosCIE10(FILAS_DIAGNOSTICOS)

        if (sSession.ID_HOSPITALIZACION != undefined) {
            $("#txtIdHospitalizacion").show();
            $("#lblIdHospitalizacion").show();
            $("#txtIdHospitalizacion").val(sSession.ID_HOSPITALIZACION);
        }
        else {
            $("#txtIdHospitalizacion").hide();
            $("#lblIdHospitalizacion").hide();
            $("#txtIdHospitalizacion").val(0);
        }

        // PREVISION
        if (sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria) {
            //$("#sltPrevision").removeAttr("data-required")
            //$("#sltPrevisionTramo").removeAttr("data-required")
            $("#divPrevisionPaciente b")
        }

        $('#sltPrevision').change(function () {
            CargarComboHijo('#sltPrevision', '#sltPrevisionTramo', ComboTramo);
        });

        // SERVICIO SALUD
        $("#sltServicioSaludIngreso").change(function () {
            if ($("#sltServicioSaludIngreso").val() == "0") {
                $("#sltUbicacionIngreso").attr("disabled", true)
                $("#sltUbicacionIngreso").val(0)
            } else {
                $("#sltUbicacionIngreso").attr("disabled", false)
            }
            CargarComboHijo("#sltServicioSaludIngreso", "#sltEstablecimientoIngreso", ComboEstablecimiento);
        });

        $('#sltEstablecimientoIngreso').change(function () {
            CargarComboHijo('#sltEstablecimientoIngreso', "#sltUbicacionIngreso", ComboUbicacion);
        });

        $("#divEspecialidades").hide()
        $("#divEstablecimientos").hide()

        // PROCEDENCIA...
        $("#sltProcedenciaIngreso").change(function () {
            if ($(this).val() == '1') {
                $('#sltTipoHospitalizacion').val("1").change()
                $('#sltTipoHospitalizacion').prop('disabled', true);
            }
            else {
                $('#sltTipoHospitalizacion').prop('disabled', false);
            }
            if ($(this).val() == '2') {
                $('#sltTipoHospitalizacion').val("4").change()
                $('#sltEspecialidades').prop('disabled', false);
                $('#sltTipoHospitalizacion').prop('disabled', true);
                $('#sltEspecialidades').attr('data-required', 'true');
                $("#divEspecialidades").show()
                ComboEspecialidades();
            } else {
                $('#sltEspecialidades').prop('disabled', true);
                $('#sltEspecialidades').attr('data-required', 'false');
                $('#sltEspecialidades').val('0');
                $("#divEspecialidades").hide()
            }

            if ($(this).val() == '3' || $(this).val() == '6') {
                $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', false);
                $('#sltEstablecimientoProcedenciaIngreso').attr('data-required', 'true');
                $("#divEstablecimientos").show()
                if ($(this).val() == '3')
                    ComboEstablecimientoPorTipo($('#sltEstablecimientoProcedenciaIngreso'), 2); // Secundaria
                else if ($(this).val() == '6')
                    ComboEstablecimientoPorTipo($('#sltEstablecimientoProcedenciaIngreso'), 1); // Primaria
            } else {
                $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', true);
                $('#sltEstablecimientoProcedenciaIngreso').attr('data-required', 'false');
                $('#sltEstablecimientoProcedenciaIngreso').val('0');
                $("#divEstablecimientos").hide()
            }

            ReiniciarRequired();
        });

        // CheckBox LEY PREVISIONAL INGRESO
        $('#chbLeyPrevisionalIngreso').on('switchChange.bootstrapSwitch', function (event, state) {
            $('#sltLeyPrevisionalIngreso').prop('disabled', !state).attr("data-required", state);
            $('#sltLeyPrevisionalIngreso').val('0');
            ReiniciarRequired();
        });

        $('#btnGuardar').on('click', async function () {


            const fechaSolicitudIngreso = {
                valor: $("#fechaDatosSolicitudIngreso").val(),
                max: $("#fechaDatosSolicitudIngreso").attr("max")
            }

            if (parseInt(fechaSolicitudIngreso.valor) > parseInt(fechaSolicitudIngreso.max)) {
                $("#fechaDatosSolicitudIngreso").addClass('is-invalid');
                return false;
            }

            const fechaIngreso = {
                valor: $("#txtFechaIngreso").val(),
                min: $("#txtFechaIngreso").attr("min"),
                max: $("#txtFechaIngreso").attr("max")
            }

            if (parseInt(fechaIngreso.valor) > parseInt(fechaIngreso.max) || parseInt(fechaIngreso.valor) < parseInt(fechaIngreso.min)) {
                $("#txtFechaIngreso").addClass('is-invalid');
                return false;
            }

            if ($("#chkAcompañante").bootstrapSwitch('state')) {
                if (!validarCampos("#divAcompañante", true))
                    return;
            }

            let validarFonasa = await validaAppSettingFonasa()


            if (!$("#btnCargarPrevisionFonasaPaciente").hasClass("click-prevision") && $("#sltIdentificacion").val() === '1' && validarFonasa) {
                alertaNoActualizaPrevision();
                return;
            }
            await GuardarHospitalizacion()

        });

        $("#sltTipoDocumento").on('change', function () {
            if ($(this).val() != 0 && $("#txtNumeroDocumento").val()) {
                CargarPaciente($("#sltTipoDocumento").val(), $("#txtNumeroDocumento").val());
            }
        });

        if (sSession.ID_PACIENTE != null) {
            CargarPacienteActualizado(sSession.ID_PACIENTE)
            avisoHospitalizaciones(sSession.ID_PACIENTE);
            if (sSession.ID_SOLICITUD_HOSPITALIZACION !== undefined) {
                DeshabilitarPaciente(false);
                $("#sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("disabled", "disabled");
            }
        }

        if (sSession.ID_SOLICITUD_HOSPITALIZACION != undefined) {
            promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/${sSession.ID_SOLICITUD_HOSPITALIZACION}`)
                .then((resp) => {
                    cargarInfoHospitalizacion(resp)
                })
                .catch((err) => console.error);
        }

        // Pregunta si es un formulario nuevo o esta editando
        if (sSession.ID_HOSPITALIZACION != null) {
            await CargarHospitalizacionAdmision(sSession.ID_HOSPITALIZACION);
            $('#sltIdentificacion').attr('disabled', 'disabled');
            $('#txtnumerotPac').attr('disabled', 'disabled');
            $('#txtDigitoPac').attr('disabled', 'disabled');
        }

        $("#btnCancelar").on('click', function () { CancelarFormularioAdmision(); });
        $("#aNuevaHospitalizacion").click(function () {
            ReiniciarComponentesClinicos();
            //delete sSession.ID_HOSPITALIZACION;
            $("#mdlHosActiva").modal("hide");
        });

        $('#aEditarHospitalizacion').click(function () {
            let PacienteHosp = GetPaciente()
            let idPaciente = PacienteHosp.GEN_idPaciente
            let nombrePac = PacienteHosp.GEN_nombrePaciente;
            let apellidoPaternoPaciente = PacienteHosp.GEN_ape_paternoPaciente;
            let apellidoMaternoPaciente = PacienteHosp.GEN_ape_maternoPaciente;

            let nombrePaciente = `${nombrePac} ${apellidoPaternoPaciente} ${apellidoMaternoPaciente}`;
            $("#mdlHosActiva").modal("hide");
            ShowModalHospitalizaciones(idPaciente, nombrePaciente)
        });

        $("#aCancelarIngresoHospitalizacion").click(function () {

            ReiniciarComponentesPaciente();
            ReiniciarComponentesClinicos();
            delete sSession.ID_HOSPITALIZACION;
            $("#mdlHosActiva").modal("hide");
        });

        $("#txtDigitoPac").on("input", async function () {
            const numeroDocumento = $("#txtnumerotPac").val();
            const tipoIdentificacion = $("#sltIdentificacion").val();
            if (tipoIdentificacion !== "4")
                validarDocumentoIngreso(numeroDocumento, tipoIdentificacion);
        })
        $("#txtnumerotPac").on("input", async function () {
            const numeroDocumento = $("#txtnumerotPac").val();
            const tipoIdentificacion = $("#sltIdentificacion").val();
            if (tipoIdentificacion !== "1" && tipoIdentificacion !== "4" )
                validarDocumentoIngreso(numeroDocumento, tipoIdentificacion);
        })

        await CargarPacienteNN()
    }
    IniciarComponentes().then(() => ShowModalCargando(false));

});
async function validarDocumentoIngreso(numeroDocumento, tipoIdentificacion ) {
   
    // Obtener el paciente según el tipo de identificación
    const paciente = await buscarPacientePorDocumentoEIdentificacion(numeroDocumento, tipoIdentificacion);


    if (paciente !== undefined) {
        const { IdPaciente } = paciente[0];
        const hospitalizaciones = await getHospitalizacionesIngresadasOTransitoPacienteAdmision(IdPaciente);

        // Verificar si paciente tiene hospitalizaciones
        if (hospitalizaciones.length > 0) {
            buscarHospitalizacionesPreviasActivas(hospitalizaciones);
        } else {
            //si no tiene hospitalizaciones, busca solicitudes de hospitalizacion
            obtenerSolicitudesHospitalizacion(IdPaciente)
        }
    }
}
function cargarInfoHospitalizacion(resp) {
    $("#fechaDatosSolicitudIngreso").val(moment(resp.Fecha).format("YYYY-MM-DD"));
    $("#fechaDatosSolicitudIngreso").attr("disabled", "disabled");
    $("#horaDatosSolicitudIngreso").val(moment(resp.Fecha).format("HH:mm"));
    $("#horaDatosSolicitudIngreso").attr("disabled", "disabled");
    idEvento = sSession.ID_EVENTO !== null ? sSession.ID_EVENTO : resp.IdEvento;

    const { Id, Nombre, PrimerApellido, SegundoApellido } = resp.Profesional;
    $("#sltMedicoSolicitudIngreso").val(`${Nombre} ${PrimerApellido} ${SegundoApellido}`)
    idMedicoSolicita = Id
    $("#sltMedicoSolicitudIngreso").attr("disabled", "disabled");
    $("#sltUbicacionIngreso").val(resp.Ubicacion.Id);
    $("#txtMotivoHospitalizacion").val(resp.Motivo);
}
async function validaAppSettingFonasa() {
    const verificarFonasa = await getverificarFonasa()
    const validarFonasa = JSON.parse(verificarFonasa.toLowerCase())
    return validarFonasa
}

async function obtenerSolicitudesHospitalizacion(IdPaciente) {
    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&idPaciente=${IdPaciente}`).then(res => {
        if (res.length > 0) {

            $("#tlbSolicitudesHospitalizacion").DataTable({
                data: res,
                destroy: true,
                columns: [
                    { title: "Fecha solicitud", data: "Fecha" },
                    { title: "Motivo", data: "Motivo" },
                    { title: "Profesional", data: "Profesional" },
                    { title: "Ubicacion", data: "Ubicacion" },
                    { title: "Acciones", data: "Id" },
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (fecha) {
                            return moment(fecha).format("DD-MM-YYYY HH:mm")
                        }
                    },
                    {
                        targets: 2,
                        render: function (profesional) {
                            return `${profesional.Nombre} ${profesional.PrimerApellido} ${profesional.SegundoApellido}`
                        }
                    },
                    {
                        targets: 3,
                        render: function (ubicacion) {
                            return `${ubicacion.Valor}`
                        }
                    },
                    {
                        targets: -1,
                        render: function (id, row, data) {
                            return `<button type="button" onclick='cargarInfoHospitalizacionDesdeModal(${JSON.stringify(data)})' class="btn btn-info btn-sm">Cargar info</button>`
                        }
                    }
                ]
            })
            $("#mldSolicitudesHospitalizacion").modal("show")
        }
    }).catch(error => {
        console.error("Ocurrio un errro al  buscar las solicitudes de hospitalizacion")
        console.log(error)
    })
}
function cargarInfoHospitalizacionDesdeModal(hospitalizacion) {
    $("#mldSolicitudesHospitalizacion").modal("hide")
    toastr.success("La información ha sido cargada.")
    cargarInfoHospitalizacion(hospitalizacion)
}

// Función para mostrar el mensaje y manejar el temporizador-redirección
async function mostrarMensaje(texto) {
    let result = await Swal.fire({
        icon: 'warning',
        title: texto,
        confirmButtonText: "Volver a la bandeja",
        timerProgressBar: true,
        confirmButtonColor: "#0069d9",
        allowEscapeKey: false,
        allowOutsideClick: false,
        timer: 4000
    });

    if (result && (result.dismiss === "timer" || result.value === true)) {
        ShowModalCargando(true);
        quitarListener();
        window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
        ShowModalCargando(false);
    }
}

// Función principal para buscar hospitalizaciones previas activas, preingresos o transitos
async function buscarHospitalizacionesPreviasActivas(hospitalizaciones) {
    if (hospitalizaciones.length === 0) return;

    const { Id: idHospitalizacion, Estado: { Id: estadoId } } = hospitalizaciones[0];

    switch (estadoId) {
        case 87: // Hospitalización Pre-ingresada
            await mostrarMensaje(`El paciente está actualmente pre-ingresado, por lo que no es posible generar un nuevo pre-ingreso. Hospitalización N°${idHospitalizacion}`);
            break;
        case 155: // Hospitalización egresado por enfermero/a
            await ReactivarHospitalizacion(hospitalizaciones);
            break;
        case 156: // Paciente en tránsito
            await mostrarMensaje(`El paciente está actualmente en tránsito, por lo que no es posible generar un nuevo pre-ingreso. Hospitalización N°${idHospitalizacion}`);
            break;
        case 97: // Paciente egresado
            await mostrarMensaje(`El paciente está actualmente egresado, por lo que no es posible generar un nuevo pre-ingreso. Hospitalización N°${idHospitalizacion}`);
            break;
        default: // Cualquier otro estado
            await mostrarMensaje(`El paciente está actualmente hospitalizado, por lo que no es posible generar un nuevo ingreso en este momento. Hospitalización N°${idHospitalizacion}`);
    }
}

async function alertaNoActualizaPrevision() {

    let result = await Swal.fire({
        icon: 'warning',
        title: 'No ha actualizado la previsión del paciente.',
        confirmButtonText: "Aceptar",
        allowOutsideClick: false
    })
}

function ocultarPuebloOriginarioNivelEducacional() {
    $("#puebloOriginarioNivelEducacional").hide()
}

function defineFechaMaximayMinimadeIngreso() {
    $("#txtFechaIngreso").attr("min", AgregarDias(fechaHoy, -7));
    $("#txtFechaIngreso").attr("max", AgregarDias(fechaHoy, 7));
    $("#txtFechaIngreso").val(GetFechaActual().format("yyyy-MM-dd"));
}

function defineFechaMaximadeSolicitud() {
    $("#fechaDatosSolicitudIngreso").attr("max", fechaHoy);
    $("#fechaDatosSolicitudIngreso").val(GetFechaActual().format("yyyy-MM-dd"));
}

async function InicializarComponentes() {
    $('#sltServicioSaludIngreso').val('1');
    ComboEstablecimiento('#sltEstablecimientoIngreso', '1');
    ComboUbicacion("#sltUbicacion", '1');
    ComboPuebloOrigination("#sltPuebloOriginario");
    ComboProcedencia("#sltProcedenciaIngreso");
    ComboTipoHospitalizacion("#sltTipoHospitalizacion");
    ComboModalidadFonasa("#sltModalidadFonasaIngreso");
    //CargarComboOrigenSolicitudMedico()
    //Combo profesionales
    await inicializarAutoCompleteMedicos()
    $('#sltEstablecimientoIngreso').val('1');
    CargarCombosUbicacionPaciente();
    ActivarAcompañante()
}
async function getProfesionalesAdmisionHosp() {

    const parametrizacion = {
        url: `${GetWebApiUrl()}GEN_Profesional/Buscar?idProfesion=1&idEstablecimiento=1`,
        method: 'GET'
    }

    const profesionales = await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        async: false,
        success: function (data) {
        }
    });

    return profesionales
}

async function getProfesionalesIdValor(profesionales) {

    const profesionalIdValor = profesionales.map(profesional => {
        return {
            Id: profesional.Id,
            Valor: `${profesional.Persona.Nombre} ${profesional.Persona.ApellidoPaterno} ${profesional.Persona.ApellidoMaterno ?? ""}`
        }
    })

    return profesionalIdValor
}

function setTypeAheadAdmisionHosp(data, element) {

    let templ =
        '<span>' +
        '<span class="Id" value="{{Id}}">{{Valor}}</span>' +
        '</span>';

    let typeAhObject = {
        input: element,
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el profesional especificado ({{query}})",
        display: ["Valor"],
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                idMedicoSolicita = item.Id

                $(element).on("input", function () {
                    let textoIngresado = $(this).val();
                    let elementosLista = $(".typeahead__list .typeahead__item");

                    elementosLista.each(function (index, e) {

                        if (textoIngresado.toUpperCase() === $(e).text())
                            coincide = true
                        else
                            coincide = false
                    })

                });

                $(element).attr("data-valor")
                $(element).data("valor", item.Valor)

            },
            onCancel: function (node, item, event) {
                $(element).val(""); // Vaciar el input de búsqueda
            }
        }
    };

    $(element).on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    })

    return typeAhObject;
}

async function inicializarAutoCompleteMedicos() {

    const profesionales = await getProfesionalesAdmisionHosp()
    profesionalesIdValor = await getProfesionalesIdValor(profesionales)

    $("#sltMedicoSolicitudIngreso").typeahead(setTypeAheadAdmisionHosp(profesionalesIdValor, '#sltMedicoSolicitudIngreso'));

}
function ReiniciarComponentesPaciente() {
    LimpiarPaciente();
    DeshabilitarPaciente(true);
    $("#txtnumerotPac, #txtDigitoPac").val('');
}
function ReiniciarComponentesClinicos() {
    $("#fechaDatosSolicitudIngreso, #txtFechaIngreso, #txtHoraIngreso, #txtMotivoHospitalizacion").val('');
    $("#sltEstablecimientoProcedenciaIngreso, sltLeyPrevisionalIngreso").val('0');
    $("#sltProcedenciaIngreso").val('0').change();
    $("#sltServicioSaludIngreso").change().val('1');
    $("#sltEstablecimientoIngreso").change().val('1');
    $("#sltUbicacionIngreso, #sltTipoHospitalizacion, #sltModalidadFonasaIngreso").val('0');
    $("#sltUbicacionIngreso, #sltEstablecimientoIngreso").removeAttr("disabled");
    $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', false);
}
function EstablecerSesion() {
    sSession = getSession();
    idHospitalizacion = sSession.ID_HOSPITALIZACION
    deleteSession("ID_HOSPITALIZACION");
    deleteSession("ID_PACIENTE");
    deleteSession("ID_EVENTO");
    idSolicitudHospitalizacion = sSession.ID_SOLICITUD_HOSPITALIZACION
    deleteSession("ID_SOLICITUD_HOSPITALIZACION");
}

function mostrarModalHospitalizacionPrevia() {

    if (GetPaciente().GEN_idPaciente != undefined) {
        let PacienteHosp = GetPaciente()

        let nombrePaciente = `${PacienteHosp.GEN_nombrePaciente ?? ''}
                              ${PacienteHosp.GEN_ape_paternoPaciente ?? ''} 
                              ${PacienteHosp.GEN_ape_maternoPaciente ?? ''}`;

        $("#stgHospitalizacion").html(`<h4><i class="fas fa-user">&nbsp;&nbsp;</i>Paciente: ${nombrePaciente}</h4>`);
        avisoHospitalizaciones();
    }
}
async function CargarHospitalizacionAdmision(idHos) {

    let json = {}

    if (idHos != undefined) {

        try {
            json = await $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}/HOS_Hospitalizacion/${idHos}`,
                contentType: 'application/json',
                dataType: 'json',
            })

        } catch (error) {
            console.error("Error al cargar hospitalización")
            console.log(JSON.stringify(error))
        }


        if (json !== undefined || json !== null) {

            // CARGA PACIENTE
            cargarPacientePorId(json.Paciente?.Id);
            DeshabilitarPaciente(false);
            idEvento = json.IdEvento;

            // PROCEDENCIA
            $("#sltProcedenciaIngreso").val(json.Ingreso.Procedencia.Id).change()

            // OTRO ESTABLECIMIENTO
            if (json.Ingreso.Establecimiento != null)
                $("#sltEstablecimientoProcedenciaIngreso").val(json.Ingreso.Establecimiento.Id);

            // DATOS SOLICITUD (MEDICO)
            let fechaSolicitud = json.DatosSolicitud !== null ? moment(json.DatosSolicitud.FechaSolicita, "YYYY.MM.DD HH.mm.ss").toDate() : ""

            $("#fechaDatosSolicitudIngreso").val(moment(fechaSolicitud).format('YYYY-MM-DD'))
            $("#horaDatosSolicitudIngreso").val(moment(fechaSolicitud).format('HH:mm'))

            const profesional = await getProfesionalPorId(json.DatosSolicitud.Profesional !== null ? json.DatosSolicitud.Profesional?.Id : null)

            if (profesional !== undefined) {

                const { Nombre, ApellidoPaterno, ApellidoMaterno } = profesional
                const NombreProfesional = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`

                $("#sltMedicoSolicitudIngreso").val(NombreProfesional)
                if ($("#sltMedicoSolicitudIngreso").val() !== "")
                    idMedicoSolicita = profesional.Id
            }

            //FIN DATOS SOLICITUD (MEDICO)

            // FECHA INGRESO
            let fechaIngreso = moment(json.Ingreso.Fecha, "YYYY.MM.DD HH.mm.ss").toDate();
            $("#txtFechaIngreso").val(moment(fechaIngreso).format('YYYY-MM-DD'));

            // HORA INGRESO
            $("#txtHoraIngreso").val(json.Ingreso.Hora);

            // SERVICIO DE SALUD
            $("#sltServicioSaludIngreso").val('1');

            // ESTABLECIMIENTO
            CargarComboHijo("#sltServicioSaludIngreso", "#sltEstablecimientoIngreso", ComboEstablecimiento);
            $("#sltEstablecimientoIngreso").val(1);

            // Responsable o acompanante

            if (json.Acompañante !== null) {

                $("#chkAcompañante").bootstrapSwitch('state', true)

                ShowAcompañante(true)

                const personaAcompanianteAdmision = await buscarPersonaAcompaniante(json.Acompañante.PersonaResponsable.IdPersona)
                $("#sltTipoRepresentante").val(json.Acompañante.TipoRepresentante.Id)

                await cargarPersonaAcompaniante(personaAcompanianteAdmision)
            }

            //Especialidad

            if (json.EspecialidadSolicitud !== null) {

                if (json.EspecialidadSolicitud.Id === 2) // Atencion especialidades
                    $("#sltProcedenciaIngreso").change().val("2")

                $("#sltEspecialidades").val(json.EspecialidadSolicitud.Id)
            }

            //Ley pprevisional
            if (json.Ingreso.LeyPrevisional != null) {
                $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', true);
                $("#sltLeyPrevisionalIngreso").val(json.Ingreso.LeyPrevisional.Id);
            } else {
                $("#chbLeyPrevisionalIngreso").bootstrapSwitch('state', false);
            }

            // UBICACION INGRESO
            if (json.Ingreso !== null)
                $("#sltUbicacionIngreso").val(json.Ingreso.Ubicacion.Id);

            // PREVISION Y TRAMO DE INGRESO
            if (json.Ingreso.Prevision !== null)
                $("#sltPrevision").val(json.Ingreso.Prevision.Id).change();
            if (json.Ingreso.PrevisionTramo !== null)
                $("#sltPrevisionTramo").val(json.Ingreso.PrevisionTramo.Id);
            if (json.Ingreso.Modalidad !== null)
                $("#sltModalidadFonasaIngreso").val(json.Ingreso.Modalidad.Id);
            if (json.TipoHospitalizacion !== null)
                $("#sltTipoHospitalizacion").val(json.TipoHospitalizacion.Id);
            $("#txtMotivoHospitalizacion").val(json.MotivoHospitalizacion);

            // PACIENTE AMBULATORIO
            $("#chbPacienteAmbulatorio").bootstrapSwitch('state', (json.IdAmbito == 1) ? false : true);
            idHospitalizacion = json.Id;

            $("label").removeClass("active");
            $("label").addClass("active");
        }
    }
}

// COMBOS
function LlenarCombos() {

    ComboServicioDeSalud();
    ComboEstablecimiento("#sltEstablecimiento", "1");
    ComboEspecialidades();
    ComboTipoEscolaridad();
    ComboPrevision();
    ComboLeyPrevisional();
    ComboCategoriaOcupacion();
    ComboOcupacion();

    $("#sltServicioSalud, #sltEstablecimiento").val("1");
    $("#sltServicioSalud, #sltEstablecimiento").attr("disabled", "disabled");
    $("#sltPrevisionTramo").prop('disabled', true).append("<option value='0'>Seleccione</option>");
    $('#sltLeyPrevisionalIngreso').prop('disabled', true);
    $('#sltEstablecimientoProcedenciaIngreso').prop('disabled', true).append("<option value='0'>Seleccione</option>");

}

function validarProfesionalSolicita(idProfesional) {
    return idProfesional !== 0
}

async function addIsInvalidClass(element, pinta) {

    if (pinta) {
        $(element).addClass("is-invalid")
        $(element).next(".invalid-input").remove()
        $(element).after("<span class='invalid-input'>Este campo es obligatorio</span>")
    }
    else if ($(element).data("data-required") === true) {
        $(element).after("<span class='invalid-input'>Este campo es obligatorio</span>")
    }
    else {
        $(element).removeClass("is-invalid")
        $(element).next(".invalid-input").remove()
        $(element).attr("data-required", false)
    }
}

function validaMinCarcteres(element, numCaracteres) {
    return $(element).val().length >= numCaracteres
}

async function GuardarHospitalizacion() {

    $("#btnGuardar").prop('disabled', true);
    // Espera 1 segundo antes de proceder
    //await new Promise(resolve => setTimeout(resolve, 1000));
    $("#sltMedicoSolicitudIngreso").attr("data-required", (idEvento == null));

    if (!validarCampos("#divDatosPaciente", true)) {
        $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        console.error("Lo datos de paciente no son validos")
        return
    }
    if (!validarCampos("#divDatosSolicitudIngreso", true)) {
        $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        console.error("Lo datos del ingreso de la solicitud no son validos")
        return
    }
    if (!validarCampos("#divDatosSolicitudIngresoMedico", true, profesionalesIdValor)) {
        $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        console.error("Lo datos del ingreso medico no son validos")
        return
    }
    if (!validarCampos("#divIngreso", true)) {
        $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        console.error("Falta informacion en la solicitud")
        return
    }
    //if (!validarCampos("#txtDiagnosticoCIE10", true))
    //    return
    if (!validaMinCarcteres("#txtMotivoHospitalizacion", 3)) {
        $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        console.error("Ingrese el motivo de la hospitalizacion")
        addIsInvalidClass("#txtMotivoHospitalizacion", true)
        return
    }
    else {
        addIsInvalidClass("#txtMotivoHospitalizacion", false)
    }

    //const tablaDiagnostico = $("#tblDiagnosticos tbody")
    //const numTr = tablaDiagnostico.find("tr").length

    //if (numTr === 1) {
    //    let spans = $("#txtDiagnosticoCIE10").parent().find("span")
    //    spans.remove()
    //}

    let idpaciente = null
    let acompaniante = null
    let json = {}
    let parametrizacion = {}

    parametrizacion = {
        metodo: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion`
    }

    idpaciente = await GuardarPaciente()
    //Antes de proceder verifica si el paciente tiene hospitalizaciones ingresadas
    let hospitalizacionesRecienEgresadas = await promesaAjax("get", `HOS_Hospitalizacion/Buscar?idPaciente=${idpaciente}&idEstado=155`)

    //await ReactivarHospitalizacion(hospitalizacionesRecienEgresadas)

    json = await transformarVistaHospitalizacionAJson(idpaciente)

    if (idHospitalizacion !== undefined) {
        parametrizacion.metodo = 'PUT';
        parametrizacion.url += `/${idHospitalizacion}`;
        json.IdHospitalizacion = idHospitalizacion;
    }

    const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

    if (personaAcompaniante === null) {

        const nuevaPersonaAcompaniante = await formatJsonPersonaAcompaniante()

        acompaniante = await guardarPersonaAcompaniante(nuevaPersonaAcompaniante)

        if (acompaniante !== null) {
            json.IdPersonaResponsable = acompaniante.Id
            json.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
        }
        else {
            json.IdPersonaResponsable = null
            json.IdTipoRepresentante = null
        }
    }
    else {
        if ($("#chkAcompañante").bootstrapSwitch('state')) {

            if (personaAcompaniante !== null)
                json.IdPersonaResponsable = personaAcompaniante.Id

            json.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
        }
        else {
            json.IdPersonaResponsable = null
            json.IdTipoRepresentante = null
        }
    }

    ShowModalCargando(true)

    const url = `HOS_Hospitalizacion/Buscar?idPaciente=${idpaciente}&idTipoEstado=87`
    let hospitalizacionesPreingresadas = await promesaAjax("GET", `${url}`)


    await $.ajax({
        type: parametrizacion.metodo,
        url: parametrizacion.url,
        data: JSON.stringify(json),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: async function (data, status, jqXHR) {
            toastr.success('La Hospitalizacion se ha editado exitósamente.');

            if (hospitalizacionesPreingresadas.length > 0) {
                let idPaciente = hospitalizacionesPreingresadas[0].Paciente.IdPaciente;
                await anularSiExistenSolicitudes(idPaciente);
            }

            //$("#btnGuardar").prop('disabled', false); // Habilitar el botón
            ShowModalCargando(false)

            setTimeout(() => {
                quitarListener();
                window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
            }, 2500);
        },
        error: function (jqXHR, status) {
            console.log(`Error al Actualizar Hospitalizacion ${JSON.stringify(jqXHR)}`);
            $("#btnGuardar").prop('disabled', false); // Habilitar el botón
        }
    })
}

async function anularSiExistenSolicitudes(IdPaciente) {
    try {
        let solicitudesHospitalizacion = await promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&idPaciente=${IdPaciente}`);


        if (solicitudesHospitalizacion.length > 0) {
            console.log(`Existen ${solicitudesHospitalizacion.length} solicitudes activas. Se debe ANULAR LA NUEVA QUE INTENTA CREAR`);

            for (let solicitud of solicitudesHospitalizacion) {
                await anularSolicitudHospitalizacionAdmision(solicitud.Id);
                console.log(`Solicitud de hospitalizacion ${solicitud.Id} anulada con éxito`);
            }
        } else {
            console.warn("No se encontraron solicitudes de hospitalización activas");
        }
    } catch (error) {
        console.error("Error al buscar o anular la solicitud de hospitalización:", error);
    }
}

async function anularSolicitudHospitalizacionAdmision(idSolicitudHospitalizacion) {

    await promesaAjax("DELETE", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}`);
}

async function transformarVistaHospitalizacionAJson(idPaciente) {

    const hospitalizacion = {
        IdPaciente: idPaciente,
        FechaIngreso: $("#txtFechaIngreso").val(),
        HoraIngreso: $("#txtHoraIngreso").val(),
        IdPrevisionIngreso: valCampo(parseInt($("#sltPrevision").val())),
        IdPrevisionTramoIngreso: valCampo(parseInt($("#sltPrevisionTramo").val())),
        IdModalidadFonasaIngreso: valCampo(parseInt($("#sltModalidadFonasaIngreso").val())),
        IdLeyPrevisionalIngreso: valCampo(parseInt($("#sltLeyPrevisionalIngreso").val())),
        IdEstablecimientoProcedencia: valCampo(parseInt($("#sltEstablecimientoProcedenciaIngreso").val())),
        IdProcedenciasIngreso: valCampo(parseInt($("#sltProcedenciaIngreso").val())),
        IdUbicacionIngreso: valCampo(parseInt($("#sltUbicacionIngreso").val())),
        //IdOcupaciones: paciente.GEN_idOcupaciones,
        //IdCategoriasOcupacion: paciente.GEN_idTipo_Categoria_Ocupacion,
        IdTipoHospitalizacion: valCampo(parseInt($("#sltTipoHospitalizacion").val())),
        MotivoHospitalizacion: valCampo($("#txtMotivoHospitalizacion").val()),
        IdPersonaResponsable: valCampo(parseInt($("#txtIdAcom").val())),
        IdEstablecimiento: valCampo(parseInt($("#sltEstablecimiento").val())),
        FechaSolicita: $("#fechaDatosSolicitudIngreso").val() === "" ? null : `${$("#fechaDatosSolicitudIngreso").val()} ${$("#horaDatosSolicitudIngreso").val()}`,
        IdProfesionalSolicita: $("#sltMedicoSolicitudIngreso").val() === "" ? null : idMedicoSolicita,
        IdEspecialidadSolicitud: valCampo(parseInt($("#sltEspecialidades").val())),
        IdEvento: idEvento
        //IdUbicacionOrigenSolicitud: parseInt($("#sltOrigenSolicitudMedico").val()) === 0 ? null : parseInt($("#sltOrigenSolicitudMedico").val())//,
        //IdDiagnosticoCIE10: $("#tblDiagnosticos tbody tr[data-iddiagnostico]").data("iddiagnostico")
    };
    return hospitalizacion;
}

async function ReactivarHospitalizacion(hospitalizacionesEgresadas) {

    let esRutCorrecto = await EsValidoDigitoVerificador($("#txtnumerotPac").val(), $("#txtDigitoPac").val());

    if (hospitalizacionesEgresadas === null || hospitalizacionesEgresadas.length === 0)
        return;

    if (!esRutCorrecto)
        return;

    const fechaActual = new Date();
    const { Id, Fecha, FechaEgreso, HoraEgreso, Motivo } = hospitalizacionesEgresadas[0]
    let fechaEgreso = FechaEgreso.split("T")[0]

    // Comprobar si la hora de egreso no es nula y concatenarla
    if (HoraEgreso !== null)
        fechaEgreso += ` ${HoraEgreso}`

    fechaEgreso = new Date(fechaEgreso);

    // Calcular la diferencia en milisegundos y convertirla a horas
    const diferenciaMilisegundos = fechaActual - fechaEgreso;
    const horasTranscurridas = diferenciaMilisegundos / (1000 * 60 * 60);
    let fechaHoraEgreso = `${FechaEgreso}T${HoraEgreso}`;
    const fechaEgresoFormateada = fechaHoraEgreso = fechaHoraEgreso.replace("T00:00:00T", "T");

    if (horasTranscurridas <= 24) {
        result = await Swal.fire({

            icon: 'info',
            title: `El paciente tiene hospitalizaciones recientes`,
            html: getHtmlMensajeTieneHospitalizaciones(Id, Fecha, Motivo, fechaEgresoFormateada),
            confirmButtonColor: "#0069d9",
            confirmButtonColor: "green",
            allowEscapeKey: false,
            allowOutsideClick: false,
            showCancelButton: true,
            confirmButtonText: 'Reactivar hospitalización',
            cancelButtonText: 'Continuar con el nuevo pre ingreso'
        }).then((result) => {
            if (result.value) {
                mensajeConfirmacionRectivar(hospitalizacionesEgresadas, Id)
            }
            else if (result.dismiss === 'cancel') {
                mensajeContinuarPreIngreso(hospitalizacionesEgresadas)
            }
        });
    }


}

function getHtmlMensajeTieneHospitalizaciones(Id, Fecha, Motivo, fechaEgresoFormateada) {

    return `<p>El paciente tiene hospitalizaciones cerradas recientemente, 
                    si es que fue cerrada por error puede reactivar la ultima hospitalizacion. De lo contrario puede crear un nuevo pre ingreso</p>
                    <table class="table table-striped">
                        <tr>
                            <th>Id</th>
                            <th>Fecha ingreso</th>
                            <th>Motivo</th>
                            <th>Fecha agreso</th>
                        </tr>
                        <tr>
                            <td>${Id}</td>
                            <td>${moment(Fecha).format("DD-MM-YYYY HH:mm")}</td>
                            <td>${Motivo ?? "No ingresado"}</td>
                            <td>${moment(fechaEgresoFormateada).format("DD-MM-YYYY HH:mm")}</td>
                        </tr>
                    </table>`
}

function MensajeHospitalizacionReingresada() {

    Swal.fire({
        icon: 'info',
        title: `Hospitalizacion reingresada`,
        html: `La hospitalizacion del paciente que fue egresada por error, ha sido <strong>Re-Activada</strong> y el paciente ha sido ingresado como paciente en tránsito,
                                lo encontrará en el mapa de camas en la sección de pacientes en tránsito`,
        confirmButtonColor: "#0069d9",
        confirmButtonColor: "green",
        allowEscapeKey: false,
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonText: 'Ir al mapa de camas',
        cancelButtonText: 'ir a la bandeja de hospitalización'
    }).then((resultado) => {
        if (resultado.value == true) {
            if (resultado.value) {
                window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/VistaMapaCamas.aspx`;
            }
            else if (resultado.dismiss === 'cancel') {
                window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
            }
        }
    })
}

async function mensajeContinuarPreIngreso(hospitalizacionesEgresadas) {

    Swal.fire({
        icon: 'info',
        title: `Hospitalizacion `,
        html: `Este paciente ya tiene una hospitalización egresada, ¿Seguro quiere realizar un nuevo pre-ingreso?`,
        confirmButtonColor: "#0069d9",
        confirmButtonColor: "green",
        allowEscapeKey: false,
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'Cancelar'
    }).then((resultado) => {

        //window.onbeforeunload = null
        if (resultado.value)
            Swal.close()
        else if (resultado.dismiss === 'cancel')
            ReactivarHospitalizacion(hospitalizacionesEgresadas)
    })
}

function mensajeConfirmacionRectivar(hospitalizacionesEgresadas, id) {

    Swal.fire({
        icon: 'info',
        title: `Hospitalizacion `,
        html: `Este paciente ya tiene una hospitalizacion egresada recientemente, ¿seguro quiere reactivar?`,
        confirmButtonColor: "#0069d9",
        confirmButtonColor: "green",
        allowEscapeKey: false,
        allowOutsideClick: false,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'Cancelar'
    }).then((resultado) => {
        if (resultado.value) {
            patchReactivarHospitalizacion(id)
            window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
        }
        else if (resultado.dismiss === 'cancel') {
            ReactivarHospitalizacion(hospitalizacionesEgresadas)
        }
    })
}



async function patchReactivarHospitalizacion(id) {
    $.ajax({
        method: "PATCH",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/reingresar`,
        success: function (data) {

        }, error: function (error) {
            console.error("Ha ocurrido un errro al intentar realizar el re ingreso de la hospitalizacion")
            console.log(error)
        }
    })
}
function CancelarFormularioAdmision() {
    ShowModalAlerta('CONFIRMACION',
        'Salir del Formulario',
        '¿Está seguro que desea cancelar el registro?', cerrarModalConfirmacion,
        null);
}

function cerrarModalConfirmacion() {
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
}
function avisoHospitalizaciones(idPaciente) {
    const hayHospitalizacion = (Array.isArray(hospitalizacionesPreviasPaciente(idPaciente)));
    idHospitalizacion = getSession().ID_HOSPITALIZACION;
    hosFlag = (hayHospitalizacion && idHospitalizacion != undefined);
    if (hayHospitalizacion)
        $("#mdlHosActiva").modal("show");
}
const linkImprimirHospitalizacion = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}/Imprimir`;
    ImprimirApiExterno(url);
}