﻿$(document).ready(function () {

    llenarRangoFecha("#fechaInicioInformeHos", "#fechaFinInformeHos", 3);
    ShowModalCargando(false);
    $("#fechaInicioInformeHos").val(moment(GetFechaActual()).format("YYYY-MM-DD"));
    $("#fechaFinInformeHos").val(moment(GetFechaActual()).format("YYYY-MM-DD"));
    $("#btnBuscarReporte").on('click', () => {
        descargarReporte();
    });
    $("#sltListaInformesHos").on('change', function () {
        if ($(this).val() === "Diario") {
            $("#fechaFinInformeHos").parent().hide();
            $("#fechaInicioInformeHos").unbind().blur(function () {
                $("#fechaFinInformeHos").val($("#fechaInicioInformeHos").val());
            });
        } else if ($(this).val() === "ListadoDeTrabajo") {
            $("#fechaInicioInformeHos, #fechaFinInformeHos").parent().hide();
        } else {
            $("#fechaInicioInformeHos").unbind();
            $("#fechaInicioInformeHos, #fechaFinInformeHos").parent().show();
        }
    });
});

const descargarReporte = async () => {

    ShowModalCargando(true);

    const nombreInforme = $("#sltListaInformesHos").val();
    const parametros = `?fechaInicial=${$("#fechaInicioInformeHos").val()}&fechaFinal=${$("#fechaFinInformeHos").val()}`;
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/Informe/${nombreInforme}${parametros}`;
    const nombreArchivo = `Informe_${nombreInforme}_${$("#fechaInicioInformeHos").val()}_${$("#fechaFinInformeHos").val()}.xlsx`
    const existeReporte = await descargarReporteEnExcel(url, nombreArchivo);

    showMensajeAlert(existeReporte);
    ShowModalCargando(false);

}

const showMensajeAlert = (existeReporte) => {
    if (existeReporte) {
        $("#divResultadoReporte").attr("class", "alert alert-success text-center m-4");
        $("#divResultadoReporte").html("<i class='fa fa-check'></i> El reporte se ha descargado exitosamente.");
    } else {
        $("#divResultadoReporte").attr("class", "alert alert-warning text-center m-4");
        $("#divResultadoReporte").html("<i class='fa fa-exclamation'></i> No existen datos a mostrar.");
    }
}