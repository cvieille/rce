﻿function comboTiposAccidente() {
    let url = `${GetWebApiUrl()}/URG_Tipos_Accidente/combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoAccidente'));
}
function comboMediosTransporte() {
    let url = `${GetWebApiUrl()}/URG_Medios_Transporte/combo`;
    setCargarDataEnCombo(url, false, $('#sltMedioTransporte'));
}
function comboPrevision() {
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, $("#sltPrevision"));
}
function comboTipoAtencion() {
    let url = `${GetWebApiUrl()}URG_Tipo_Atencion/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoAtencion");
}
function CombosServiciosSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, "#sltServicioSalud");
    $("#sltServicioSalud").val('1').attr("disabled", "disabled");
}
function comboEstablecimiento(idServicioSalud) {
    const url = `${GetWebApiUrl()}GEN_Establecimiento/combo?idServicio=${idServicioSalud}`;
    setCargarDataEnCombo(url, false, "#sltEstablecimiento");
    $("#sltEstablecimiento").val("1").attr("disabled", "disabled");
}
function comboPrevisionTramo(GEN_idPrevision) {
    $('#sltPrevisionTramo').empty().append(`<option value='0'>Seleccione</option>`);

    if (GEN_idPrevision != 0) {
        let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${GEN_idPrevision}`;
        setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
    }
}

function comboActidadesPlanDeCuidado() {
    const url = `${GetWebApiUrl()}HOS_Tipo_Cuidado/Combo/TipoHoja/1`;
    setCargarDataEnCombo(url, false, "#sltTipoCuidado");
}

function comboProfesionEvolucionAtencion() {

    $("#sltProfesionEvolucionAtencion").empty();
    $("#sltProfesionEvolucionAtencion").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${sSession.ID_PROFESIONAL}/Profesion`,
        success: function (data, status, jqXHR) {

            data.forEach((item, index) => {
                $("#sltProfesionEvolucionAtencion").append(`<option value='${item.GEN_idProfesion}'>${item.GEN_nombreProfesion}</option>`);
            });

            if ($("#sltProfesionEvolucionAtencion option").length == 2) {
                $("#sltProfesionEvolucionAtencion option:eq(1)").attr('selected', 'selected');
                $("#sltProfesionEvolucionAtencion").attr("disabled", "disabled");
                $("#spnProfesionEvolucionAtencion").text($("#sltProfesionEvolucionAtencion :selected").text());
            }
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Profesion de Profesional Atención clínica ${JSON.stringify(jqXHR)}`);
        }
    });
}

//Combo invasivo
function cargarComboTipoInvasivo(idModulo) {
    let url = `GEN_Tipo_Invasivo/Combo?idModulo=${idModulo}`;
    let id = "#sltTipoInvasivoVigilancia";

    if (!setCargarDataEnCombo(GetWebApiUrl() + url, false, id)) {
    }
}
//combo extremidad invasivo
//function cargarComboTipoExtremidad() {
//    let url = "/GEN_Tipo_Extremidad_Tipo_Plano/Combo?idClasificacion=2";
//    let id = "#sltTipoExtremidadVigilancia";

//    if (!setCargarDataEnCombo(GetWebApiUrl() + url, false, id)) {
//    }
//}
//combo tipo plano invasivo
//function cargarComboTipoPlano() {
//    setCargarDataEnCombo(GetWebApiUrl() + "/GEN_Tipo_Plano/Combo", false, "#sltTipoPlanoVigilancia");
//}

function comboHidricos() {
    let elemento = "#selectHidrico"
    let url = `${GetWebApiUrl()}HOS_Tipo_Balance/1/Combo`;
    setCargarDataEnComboGroup(url, true, elemento)
}
//const imprimeFechaHora = (data) => {

//    const { Ingreso: { Fecha, Hora } } = data
//    fechaHospital = Fecha
//    let fechaHosp = (moment(Fecha).format('DD-MM-YYYY') != null) ? moment(Fecha).format('DD-MM-YYYY') : ""
//    let horaHosp = (Hora != null) ? moment(Hora, "HH:mm:ss").format("hh:mm A") : ""

//    return `${fechaHosp} ${horaHosp}`
//}

const comboClasificacionHeridas = () => {
    let url = `${GetWebApiUrl()}/NEA_Clasificacion_LPP/Combo`;
    setCargarDataEnCombo(url, false, $('#mhClasificacionHerida'))
}
const comboTipoHojaEnfermeria = () => {
    let url = `${GetWebApiUrl()}HOS_Tipo_Hoja_Enfermeria/combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoHojaEnfermeria'))
}
//Combo talla del paciente para la evolucion
const comboTipoTalla = () => {
    let url = `${GetWebApiUrl()}GEN_Tipo_Talla/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoTallaHE'))
}
//Combo peso del paciente para la evolucion
const comboTipoPeso = () => {
    let url = `${GetWebApiUrl()}GEN_Tipo_Peso/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoPesoHE'))
}
const comboTipoExtremidadMH = () => {
    let selector = mhTipoExtremidad

    $(selector).empty();


    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}/NEA_Localizacion_LPP/Combo`,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {

            $(selector).empty()
            $(selector).append("<option value='0'>Seleccione</option>");
            data.map(a => {
                a.Subclasificacion.map(b => {
                    $(selector).append(`<option value='${b.Id}'>${b.Valor}</option>`);
                })
            })
            return true;
        },
        error: function (err) {
            console.log("No se pudo cargar el modal con los signos vitales")
        }
    });
}

const comboTipoExtremidad = () => {
    const url = `${GetWebApiUrl()}/GEN_Tipo_Extremidad_Tipo_Plano/Combo?idClasificacion=1`
    setCargarDataEnCombo(url, false, $('#sltTipoExtremidadBrazalete'))
}
const comboTipoPlano = (idExtremidad = 0, idPlano = 0) => {

    $("#sltIdTipoPlano").empty()
    $("#sltIdTipoPlano").append("<option value='0'>Seleccione</option>")
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}/GEN_Tipo_Extremidad_Tipo_Plano/combo?idclasificacion=1&idExtremidad=${idExtremidad}`,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {
            $("#sltIdTipoPlano").empty()
            $("#sltIdTipoPlano").append("<option value='0'>Seleccione</option>")
            data.map(x => {
                if (x.Id == idExtremidad) {
                    x.Plano.find(p => {
                        $("#sltIdTipoPlano").append(`<option value="${p.Id}">${p.Valor}</option>`)
                    })
                }
            })

            $("#sltIdTipoPlano").val(idPlano)
        },
        error: function (err) {
            console.log("No se pudo cargar el combo tipo extremidad")
        }
    });
}
const comboTipoPlanoHerida = () => {

    let url = `${GetWebApiUrl()}GEN_Tipo_Plano/Combo`;
    setCargarDataEnCombo(url, false, $('#mhPlano'))

}

function inicializarCombosInvasivos() {

    $("#sltTipoExtremidadVigilancia").append("<option value='0'>Seleccione</option>");
    $("#sltTipoExtremidadVigilancia").val("0")
    $("#sltTipoExtremidadVigilancia").attr("disabled", true)

    $("#sltTipoPlanoVigilancia").append("<option value='0'>Seleccione</option>");
    $("#sltTipoPlanoVigilancia").val("0")
    $("#sltTipoPlanoVigilancia").attr("disabled", true)
}

function comboInvasivoExtremidadEnfermeria(sltInvasivo, sltExtremidadInvasivo, sltPlanoInvasivo) {

    $(sltInvasivo).change(function () {

        let idInvasivo = ""
        idInvasivo = $(this).val()
        if (idInvasivo === "0") {
            $(sltExtremidadInvasivo).attr("disabled", true)
            $(sltPlanoInvasivo).attr("disabled", true)
            $(sltExtremidadInvasivo).val("0")
            $(sltPlanoInvasivo).val("0")
        }
        else {
            $(sltExtremidadInvasivo).attr("disabled", false)
            $(sltPlanoInvasivo).empty()
            $(sltPlanoInvasivo).append("<option value='0'>Seleccione</option>");
            $(sltPlanoInvasivo).attr("disabled", true)

            let url = `${GetWebApiUrl()}GEN_Tipo_Invasivo_Extremidad_Plano/${idInvasivo}/Combo`

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.length > 0) {
                        $(sltExtremidadInvasivo).empty()
                        $(sltExtremidadInvasivo).append("<option value='0'>Seleccione</option>");
                        data.map(x => {
                            $(sltExtremidadInvasivo).append(`<option value="${x.IdExtremidad}">${x.DescripcionExtremidad}</option>`)
                            $(sltExtremidadInvasivo).change(function () {
                                let value = parseInt($(this).val())
                                if (value === x.IdExtremidad) {
                                    $(sltPlanoInvasivo).empty()
                                    $(sltPlanoInvasivo).append("<option value='0'>Seleccione</option>");
                                    x.Planos.map(x => {
                                        $(sltPlanoInvasivo).append(`<option value="${x.Id}">${x.Valor}</option>`)
                                    })
                                }

                            })
                        })
                    }
                    else {
                        $(sltExtremidadInvasivo).empty()
                        $(sltExtremidadInvasivo).append("<option value='0'>Seleccione</option>");
                        $(sltExtremidadInvasivo).removeAttr("data-required")
                        $(sltExtremidadInvasivo).attr("disabled", true)

                        $(sltPlanoInvasivo).empty()
                        $(sltPlanoInvasivo).append("<option value='0'>Seleccione</option>");
                        $(sltPlanoInvasivo).removeAttr("data-required")
                        $(sltPlanoInvasivo).attr("disabled", true)
                    }
                    

                    
                },
                error: function (err) {
                    console.log("No se pudo cargar el combo tipo extremidad")
                }
            });
        }

    })

    $(sltExtremidadInvasivo).change(function () {
        let idExtremidad = $(this).val()
        if (idExtremidad === "0") {
            $(sltPlanoInvasivo).attr("disabled", true)
            $(sltPlanoInvasivo).val("0")
        } else {
            $(sltPlanoInvasivo).attr("disabled", false)
        }
    })


}

function comboTipoRetiroInvasivo() {
    let url = `${GetWebApiUrl()}HOS_Tipo_Retiro_Invasivo/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoRetiroInvasivo");
}

