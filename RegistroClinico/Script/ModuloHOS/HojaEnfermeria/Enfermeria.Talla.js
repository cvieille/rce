﻿let tablaTipoTalla = $("#tblHojaEnfemeriaTalla").DataTable({
    columns: [
        { title: 'ID', data: 'id' },
        { title: 'Talla', data: 'talla' },
        { title: 'Medida', data: 'medida' },
        { title: 'Hora', data: 'hora' },
        { title: 'Acciones', data: 'acciones' }],
    order: [[3, 'desc']]
})

//const ID_HOJA_ENFERMERIA = sesion.ID_HOJA_ENFERMERIA
//const ID_HOSPITALIZACION = sesion.ID_HOSPITALIZACION

function dibujaAccionesTipoTalla(id) {

    let acciones = `
			<div class="text-center">
				<button
				type="button"
				class="btn btn-danger btn-sm"
				data-id="${id}"
				id="btn-talla-${id}"
				data-toggle="tooltip"
				data-placement="bottom"
				title="Eliminar"
				style="width:60px;display:inline;"
				onclick="elminarTipoTallaHE(this)"
			>
				<i class="fas fa-trash-alt fa-sm"></i>
				</button>
			</div>`

    return acciones
}

function configuracionTablaTipoTalla() {

    tablaTipoTalla.columns(0).visible(false)
    tablaTipoTalla.column(1).header().style.width = "10%"
    tablaTipoTalla.column(2).header().style.width = "10%"
    tablaTipoTalla.column(3).header().style.width = "10%"
    tablaTipoTalla.column(4).header().style.width = "10%"
}

async function getHojaEnfermeriaTalla() {

    configuracionTablaTipoTalla()
    tablaTipoTalla.draw()
    await actualizarDatosTablaTipoTalla()
}

async function actualizarDatosTablaTipoTalla() {
    let tipotalla = await getTallaHojaEnfermeria()
    tablaTipoTalla.clear()
    await dibujarTablaTipoTalla(tipotalla)
}

function limpiarCamposTipoTalla() {

    $("#txtTallaHE").val("")
    $("#sltTipoTallaHE").val("0")
}

async function agregarTipoTalla() {

    if (!validarCampos("#divTipoTallaHE", false))
        return

    let talla = $("#txtTallaHE").val()
    let medida = $("#sltTipoTallaHE").val()

    let tipoTalla = {
        "IdHojaEnfermeria": ID_HOJA_ENFERMERIA,
        "idTipoTalla": medida,
        "TipoTalla": talla
    }

    await guardarTallaHojaEnfermeria(tipoTalla)
    await actualizarDatosTablaTipoTalla()

    limpiarCamposTipoTalla()
}

async function guardarTallaHojaEnfermeria(tipoTalla) {

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/TipoTalla`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(tipoTalla),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se agregó nueva talla`)
        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar talla en hoja de enfermería: ${JSON.stringify(jqXHR)} `)
        }
    })
}

async function getTallaHojaEnfermeria() {

    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/Hospitalizacion/${ID_HOSPITALIZACION}/TipoTalla`

    let TipoTalla = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (tipoTalla) {

            },
        error: function (err) {
            console.error("Error al llenar tipo talla")
            console.error(JSON.stringify(err))
        }
    })

    return TipoTalla
}

async function dibujarTablaTipoTalla(data) {

    data.forEach(x => {
        let row = x

        let acciones = dibujaAccionesTipoTalla(row.Id)
        tablaTipoTalla.row.add({
            id: row.Id,
            talla: row.TipoTalla,
            medida: row.TipoMedida.Valor,
            hora: moment(row.Fecha).format("DD-MM-YYYY HH:mm"),
            acciones: acciones
        });
    })

    tablaTipoTalla.draw();
}

async function elminarTipoTallaHE(e) {

    let id = $(e).data("id")

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/TipoTalla`
    }

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el registro de talla?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, jqXHR) {
                toastr.success(`Talla eliminada correctamente`)
                actualizarDatosTablaTipoTalla()
            },
            error: function (jqXHR, status) {
                console.log(`Error al eliminar tipo talla hoja enfermería: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}