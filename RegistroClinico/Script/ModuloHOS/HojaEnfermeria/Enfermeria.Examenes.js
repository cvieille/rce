﻿let tablaExamenesImgHE = $("#tblExamenesImgHE").DataTable({
    columns: [
        { title: 'ID', data: 'id' },
        { title: 'Exámen', data: 'examen' },
        { title: 'Fecha y profesional solicitud', data: 'fecha' },
        { title: 'Estado', data: 'estado' },
        { title: 'Fecha y prof. cierre solicitud', data: 'fechacierre' },
        { title: 'Acciones', data: 'acciones' }
    ],
    columnDefs: [

        { className: "text-center", targets: [3,5]}
    ]
})

let tablaExamenesLabHE = $("#tblExamenesLabHE").DataTable({
    columns: [
        { title: 'ID', data: 'id' },
        { title: 'Exámen', data: 'examen' },
        { title: 'Fecha y profesional solicitud', data: 'fecha' },
        { title: 'Estado', data: 'estado' },
        { title: 'Fecha y prof. cierre solicitud', data: 'fechacierre' },
        { title: 'Acciones', data: 'acciones' }
    ],
    columnDefs: [

        { className: "text-center", targets: [3,5] }
    ]
})

//115 14  Examen tomado
//116 14  Resultado pendiente
//117 14  Informado a medico
//118 14  Examen solicitado
//128 14  Examen suspendido

const TipoEstadoExamen = {
    Tomado: 115,
    ResultadoPendiente: 116,
    InformadoAmedico: 117,
    Solicitado: 118,
    Suspendido: 128
}


function dibujaAccionesExamenesHE({ id, idEstado, examen, idServicioCartera }) {

    let disabled = (idEstado === TipoEstadoExamen.InformadoAmedico) ? "disabled" : ""

    let acciones = `<button
            type="button"
            class="btn btn-info btn-sm"
            data-id="${id}"
            data-id="fila-${id}"
            data-id-estado="${idEstado}"
            data-id-servicio-cartera="${idServicioCartera}"
            data-nombre-examen="${examen}"
            data-toggle="tooltip"
            data-placement="bottom"
            title="cambiar estado examen: ${examen}"
            onclick="cambiarEstadoExamenHE(this)"
        >
            <i class="fas fa-exchange-alt fa-md"></i>
        </button>
        <button
            type="button"
            class="btn btn-danger btn-sm"
            data-id="${id}"
            data-id="fila-${id}"
            data-id-estado="${idEstado}"
            data-nombre-examen="${examen}"
            data-toggle="tooltip"
            data-placement="bottom"
            ${disabled}
            title="eliminar examen: ${examen}"
            onclick="eliminarExamenHE(this)"
        >
            <i class="fa fa-trash fa-md"></i>
        </button>`
    return acciones
}

async function dibujarModalEstadoExamen(id) {
    const estadosExamenOptions = [
        { value: TipoEstadoExamen.Solicitado, label: 'Examen solicitado' },
        { value: TipoEstadoExamen.Tomado, label: 'Examen tomado' },
        { value: TipoEstadoExamen.ResultadoPendiente, label: 'Resultado pendiente' },
        { value: TipoEstadoExamen.InformadoAmedico, label: 'Informado a médico' },
        { value: TipoEstadoExamen.Suspendido, label: 'Examen suspendido' }
    ];

    const html = `
        <div class="modal" tabindex="-1" role="dialog" id="mdl-examen-${id}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Cambiando estado del examen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModalEstadoExamenHE();">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="mdlBodyCambioEstado">
                        <h5 id="TitleNombreExamen"></h5>
                        <label>Seleccione estado</label>
                        <select id="sltEstadosExamen" class="form-control" data-required="true">
                            ${estadosExamenOptions.map(option => `<option value="${option.value}">${option.label}</option>`).join('')}
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnCambioEstado" onclick="modificarEstadoExamen(this);">Cambiar estado</button>
                        <button type="button" class="btn btn-secondary closeModalExamenHE" data-dismiss="modal" onclick="closeModalEstadoExamenHE();">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>`;

    $("#displayModalEstadoExamen").append(html);
    $(`#mdl-examen-${id}`).show();
}


async function closeModalEstadoExamenHE() {

    $("#displayModalEstadoExamen").children().hide()

}

async function getExamenesHojaEnfermeria() {
    InicializarTypeHeadExmanenesHE()


    tablaExamenesImgHE.column(3).nodes().to$().css('text-align', 'center');

    $('#thExamenesLaboratorio').on('keydown', async function (event) {
        if (event.which === 13) {
            if ($(this).val() === "") {
                toastr.error(`Debe ingresar un exámen <strong>minimo 3</strong> caracteres`)
                return
            }
            const examen = $(this)
            const IdHojaEnfermeriaCrea = sesion.ID_HOJA_ENFERMERIA
            const IdServicioCartera = examen.data("id")

            const examenEnfermeria = {

                IdHojaEnfermeriaCrea,
                IdServicioCartera,
                IdEstado: TipoEstadoExamen.Solicitado
            }

            await GuardarExamenHojaEnfermeria(examenEnfermeria)
            await actualizarTablaExamenesHE()
        }
    })

    await actualizarTablaExamenesHE()
}

async function actualizarTablaExamenesHE() {

    const examenes = await getExamenesHE()

    tablaExamenesImgHE.clear()
    tablaExamenesLabHE.clear()

    await dibujarTablaExamenesHE(examenes)

    $("#thExamenesLaboratorio").val("")
}

async function agregarExamenHE() {

    const examen = $("#thExamenesLaboratorio")
    const IdHojaEnfermeriaCrea = sesion.ID_HOJA_ENFERMERIA
    const IdServicioCartera = examen.data("id")

    const examenEnfermeria = {

        IdHojaEnfermeriaCrea,
        IdServicioCartera,
        IdEstado: TipoEstadoExamen.Solicitado
    }

    if (examen.val() === "") {
        toastr.error(`Debe ingresar un exámen <strong>minimo 3</strong> caracteres`)
        return
    }

    await GuardarExamenHojaEnfermeria(examenEnfermeria)
    await actualizarTablaExamenesHE()
}

async function GuardarExamenHojaEnfermeria(examen) {

    const { IdHojaEnfermeriaCrea } = examen

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${IdHojaEnfermeriaCrea}/Examen`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(examen),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se agregó nuevo exámen`)
        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar exámen de hoja enfermería: ${JSON.stringify(jqXHR)} `)
        }
    })
}

async function dibujarTablaExamenesHE(data) {

    data.forEach(x => {
        let row = x

        let json = {
            id: x.IdHojaEnfermeriaExamen,
            idEstado: x.TipoEstado.Id,
            examen: x.ServicioCartera.Valor,
            idServicioCartera: x.ServicioCartera.Id
        }

        //115 14  Examen tomado
        //116 14  Resultado pendiente
        //117 14  Informado a medico
        //118 14  Examen solicitado
        //128 14  Examen suspendido

        let estadoExamenHE = {
            115: createIcon("far fa-thumbs-up", "info"),
            116: createIcon("fa fa-hourglass", "dark"),
            117: createIcon("fa fa-check-square", "success"),
            118: createIcon("fa fa-clock", "secondary"),
            128: createIcon("fa fa-check-square", "warning")
        };

        let acciones = dibujaAccionesExamenesHE(json)

        if (row.Aranceles[0].TipoArancel.Id === 4) {
            tablaExamenesImgHE.row.add({
                id: row.IdHojaEnfermeriaExamen,
                examen: row.ServicioCartera.Valor,
                fecha: `${moment(row.FechaSolicitud).format("DD-MM-YYYY HH:mm:ss")} </br> ${row.ProfesionalSolicitud}`,
                estado: estadoExamenHE[row.TipoEstado.Id],
                fechacierre: (row.FechaTermino !== null) ? `${moment(row.FechaTermino).format("DD-MM-YYYY HH:mm:ss")} </br> ${row.ProfesionalTermina}` : "N/A",
                acciones: acciones
            });
        } else if (row.Aranceles[0].TipoArancel.Id === 5) {
            tablaExamenesLabHE.row.add({
                id: row.IdHojaEnfermeriaExamen,
                examen: row.ServicioCartera.Valor,
                fecha: row.FechaSolicitud,
                estado: estadoExamenHE[row.TipoEstado.Id],
                fechacierre: row.FechaTermino ?? "N/A",
                acciones: acciones
            })
        }

    })

    tablaExamenesImgHE.rows().nodes().each(function (row, index) {

        let fila = tablaExamenesImgHE.row(row).data()
        let idExamenEnfermeria = fila.id
        $(row).attr('id', 'fila-' + idExamenEnfermeria)
    });

    // Dibujar tabla actualizada
    tablaExamenesImgHE.draw()
    tablaExamenesLabHE.draw()
}
function createIcon(iconClasses, outlineClass) {
    return `<i class="${outlineClass ? `btn btn-outline-${outlineClass}` : ''} ${iconClasses} fa-lg" 
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="${row.TipoEstado.Valor}"></i>`;
}
async function getExamenesHE() {

    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${sesion.ID_HOSPITALIZACION}/Examenes`

    let examenes = $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (examenes) {

            },
        error: function (err) {
            console.error("Error al llenar examenes")
            console.error(JSON.stringify(err))
        }
    })

    return examenes
}

async function eliminarExamenHE(e) {

    const id = $(e).data("id")
    const examen = $(e).data("nombre-examen")

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el examen: ${examen}?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {

        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/Examen`
        }

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {

                toastr.error(`Se ha eliminado el exámen: <strong>${examen}</strong>`)
                $(e).attr("disabled", true)
                await actualizarTablaExamenesHE()

            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar el exámen: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}

async function cambiarEstadoExamenHE(e) {

    const id = $(e).data("id")
    const idServicioCartera = $(e).data("id-servicio-cartera")
    const idEstado = $(e).data("id-estado")
    const nombreExamen = $(e).data("nombre-examen")

    await dibujarModalEstadoExamen(id)

    $("#btnCambioEstado").data("id", id)
    $("#btnCambioEstado").data("id-servicio-cartera", idServicioCartera)
    $("#btnCambioEstado").data("nombre-examen", nombreExamen)

    let idModal = $(`#mdl-examen-${id}`).attr("id")
    let arrModal = idModal.split("-")
    let idModalEstadoExamen = parseInt(arrModal[2])

    if (idModalEstadoExamen === id)
        $("#sltEstadosExamen").val(idEstado)
}

async function modificarEstadoExamen(e) {

    const { id, idServicioCartera } = $(e).data()
    const idEstado = parseInt($("#sltEstadosExamen").val())
        
    let examen = {
        IdHojaEnfermeriaExamen: id,
        IdHojaEnfermeria: sesion.ID_HOJA_ENFERMERIA,
        IdServicioCartera: idServicioCartera,
        IdEstado: idEstado
    }

    await actualizarEstadoExamenHE(examen)
          
}

async function actualizarEstadoExamenHE(examen) {

    const { IdHojaEnfermeriaExamen, IdEstado } = examen

    let parametrizacion = {
        method: "PUT",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/Examen/${IdHojaEnfermeriaExamen}`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(examen),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {

        },
        error: function (jqXHR, status) {
            console.log(`Error al actualizar estado exámen: ${JSON.stringify(jqXHR)} `)
        }
    });

    const estadoExamenHE = {
        115: `Examen tomado`,
        116: `Resultado pendiente`,
        117: `Informado a médico`,
        118: `Examen solicitado`,
        128: `Examen suspendido`
    }

    $("#displayModalEstadoExamen").children().hide()
    $("#displayModalEstadoExamen").children().remove()
    await actualizarTablaExamenesHE()
    toastr.success(`Se ha cambiado el estado del exámen a: <b>${estadoExamenHE[IdEstado]}</b>`)
}

function setTypeAheadExamenesHE(data, element) {

    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{ServicioCartera.Valor}}</span>
                </span>`;

    let typeAhObject = {
        input: element,
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el exámen especificado ({{query}})",
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                $(element).data('id', item.ServicioCartera.Id)
                $(element).data('descripcion-examen', item.ServicioCartera.Valor)
            }
        }
    };

    return typeAhObject;
}

function GetJsonCartera() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/Combo?idTipoArancel=4&idTipoArancel=5`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (Examenes) {

            ExamenesLabIma = Examenes
        }
    })

    return ExamenesLabIma
}

//Inicializar typeahead examenes laboratorio

function InicializarTypeHeadExmanenesHE() {

    arrayCarteraArancel = GetJsonCartera()

    const ExamenesFiltradosImagenologia = arrayCarteraArancel.filter(examen => examen.Aranceles.some(arancel => arancel.TipoArancel.Id === 4))
    const ExamenesFiltradosLaboratorio = arrayCarteraArancel.filter(examen => examen.Aranceles.some(arancel => arancel.TipoArancel.Id === 5))

    $("#thExamenesLaboratorio").typeahead(setTypeAheadExamenesHE(arrayCarteraArancel, '#thExamenesLaboratorio'));
    $("#thExamenesLaboratorio").change(() => $("#thExamenesLaboratorio").removeData("id"));
    $("#thExamenesLaboratorio").blur(() => {
        if ($("#thExamenesLaboratorio").data("id") == undefined)
            $("#thExamenesLaboratorio").val("");
    });
}

function existeExamenLaboratorio(arr, examen) {

    const respuesta = arr.includes(x => x.ServicioCartera.Valor === examen)

    return respuesta
}


