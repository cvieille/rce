﻿// PLAN DE CUIDADOS ENFERMERIA

let arrayHorariosPlan = []

// Fetch de datos Plan de Cuidados
async function getPlanCuidado(id) {

    if (id > 0) {
        cargarPlanCuidadoporTipoHoja(id)
    }
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}`,
        contentType: "application/json",
        dataType: "json",
        success:
            function (data) {
                const {
                    TipoCuidado,
                } = data;

                llenarHorariosPorActividad(TipoCuidado)
            },
        error: function (err) {
            console.error("Error al llenar la hoja de enfermeria")
            console.log(JSON.stringify(err))
        }
    })

    function cargarPlanCuidadoporTipoHoja(id) {
        const url = `${GetWebApiUrl()}HOS_Tipo_Cuidado/Combo/TipoHoja/${id}`

        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json",
            dataType: "json",
            async: true,
            success: function (data) {

                dibujarTablaPlanCuidado(data)
            },
            error: function (err) {
                console.log("No se pudieron cargar las actividades Plan de Cuidados")
            }
        })
    }
}

function getDataUpdatePlanCuidados(TipoCuidado) {

    TipoCuidado.map(x => {
        return {
            Id: x.Id,
            Horarios: { IdEstado: x.IdEstado, Horario: x.Horario, ObservacionCierre: x.ObservacionCierre },
            Observacion: x.Observacion
        }
    })

    let arrayTipoCuidados = [...new Set(TipoCuidado.map(x => x.Id))].map(id => {
        return {
            Id: id,
            Descripcion: TipoCuidado.find(x => x.Id == id).Descripcion,
            Horarios: TipoCuidado.filter(x => x.Id == id).map(x => {
                return {
                    IdEstado: x.IdEstado,
                    Horario: x.Horario,
                    ObservacionCierre: x.ObservacionCierre
                }
            }),
            Observacion: TipoCuidado.find(x => x.Id == id).Observacion
        }
    })
    arrayHorariosPlan = [...arrayTipoCuidados]
    return arrayHorariosPlan
}

//dibuja tabla plan cuidados
function dibujarTablaPlanCuidado(data) {

    let contenido = ""

    contenido += `<thead>
                    <tr>
                        <th>Actividad</th>
                        <th>Horarios</th>
                        <th>Acciones</th>
                    </tr>
                </thead> <tbody>`

    data.map((x, i) => {

        contenido += `<tr data-tr-id="${i}" data-tr-idActividad="${x.Id}">
                        <td class="pl-3 pr-3" style="width: 1%; white-space:nowrap;">
                            <div class="row">
                                <strong><span class="spanIdPlanCuidado">${x.Valor}</span></strong>
                            </div>
                        </td>
                        <td data-td-id="${i}">                           
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-outline-success btn-sm m-1"                                
                                data-toggle="tooltip" data-placement="bottom"
                                title=""
                                onclick="agregarHorariosPlan(this)"
                                data-agregar-plan="${i}"
                                data-id-plan="${x.Id}"
                                data-nombre-actividad="${x.Valor}"
                                data-original-title="Agregar"
                                style="cursor:pointer;width:80px;">
                                <i class="fa fa-plus fa-lg"></i>
                            </button>
                            <button type="button" class="btn btn-outline-danger btn-sm m-1"
                                onclick="limpiarHorariosPlanCuidados(this,${i})"                                
                                data-toggle="tooltip" data-placement="bottom"
                                title="Eliminar"
                                data-limpiar-plan="${i}"
                                data-id-plan="${x.Id}"
                                data-nombre-actividad="${x.Valor}"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                style="cursor:pointer;width:80px;">
                                <i class="fa fa-trash fa-lg"></i>
                            </button>                          
                        </td>
                      </tr>
        `
    })
    contenido += `</tbody>`
    $("#tblPlancuidados").append(contenido)
}

function llenarHorariosPorActividad(arr) {

    const arrayPlanCuidados = getDataUpdatePlanCuidados(arr)

    let tr = $("#tblPlancuidados tr")
    let json = {}
    let clase = ""

    tr.each(function (index, element) {
        if ($(element).data("tr-idactividad")) {
            let trIdActividad = $(element).data("tr-idactividad")

            arrayPlanCuidados.find(x => {
                if (x.Id === trIdActividad) {
                    x.Horarios.map((e, i) => {
                        json =
                        {
                            idHorario: $(element).data("tr-id"),
                            idPlan: x.Id,
                            nombreActividad: x.Descripcion,
                            hora: moment(e.Horario, 'HH:mm').format('HH:mm'),
                            id: $(element).data("tr-id"),
                            idEstado: e.IdEstado,
                            ObservacionCierre: null ?? e.ObservacionCierre
                        }

                        if (json.idEstado === 119) {
                            clase = "btn btn-secondary"
                        }
                        else if (json.idEstado === 120) {
                            clase = "btn btn-success"
                        }
                        else if (json.idEstado === 121) {
                            clase = "btn btn-danger"
                        }

                        $(element).find("td:eq(1)").append(`
                        <div class="m-1" data-div-id-plan="${json.idHorario}div${i}" style="display:inline-block;">
                            <button type="button"
                                data-id="${json.idHorario}div${i}"
                                data-id-hora="${json.idHorario}"
                                data-id-eliminar-hora="${i}"
                                data-id-editar-hora="${i}"
                                data-nombre-actividad="${json.nombreActividad}"
                                data-estado-plan-id="${json.idEstado}"
							    data-observacion-id-estado="${json.idEstado}"
							    data-observacion-cierre="${json.ObservacionCierre}"
                                data-hora="${json.hora}"
                                data-id-plan="${json.idPlan}"
                                data-toggle="tooltip"
                                title="${(json.ObservacionCierre === null) ? "" : json.ObservacionCierre}"
                                onclick="mostrarEstadoHorarioPlanCuidado(this)"
                                class="${clase} hora"
                             >
                                ${json.hora}
                        </button>
                        <div style="display:inline-block;">
                            <i class="fa fa-times mr-2"
                                data-id-eliminar-hora="${i}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Eliminar Hora"
                                style="cursor:pointer;color:#dc3545;display:block;" onclick="eliminarHoraPlanCuidados(this)">
                            </i>

                            <i class="fas fa-pencil-alt"
                                data-id-editar-hora="${i}"
                                data-id-editar-tr="${json.id}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Editar hora"
                                style="cursor:pointer;color:#007bff;display:block;" onclick="editarHoraPlanCuidados(this)">
                            </i>
                        </div>
                    </div>
                    `)
                    })
                }
            })
        }
    })
}

function agregarHorariosPlan(e) {

    let idAgregar = $(e).data("agregar-plan")
    let idPlan = $(e).data("id-plan")
    let nombreActividad = $(e).data("nombre-actividad")

    const totalInput = $(`#tblPlancuidados tr[data-tr-id="${idAgregar}"] td input`).length
    const totalButton = $(`#tblPlancuidados tr[data-tr-id="${idAgregar}"] td button.hora`).length
    if (totalInput < 12 && totalButton < 12 && validarCampos("#validaHorariosPlan", true)) {

        $(`#tblPlancuidados td[data-td-id="${idAgregar}"]`)
            .append(
                `
                    <input id='txtHorarioPlan_${idAgregar}_${totalInput}' type="time"
                        class="form-control m-2"                    
                        placeholder="horario"
                        data-id-horarios-plan="${idAgregar}"
                        data-id-plan="${idPlan}"
                        data-nombre-actividad="${nombreActividad}"
                        style="width:100px;display:inline;"
                        data-required="true"
                     >`
            )

        $(`#txtHorarioPlan_${idAgregar}_${totalInput}`).on('blur', function () {

            const json =
            {
                idHorario: $(this).data("id-horarios-plan"),
                idPlan: $(this).data("id-plan"),
                nombreActividad: $(this).data("nombre-actividad"),
                hora: $(this).val(),
                id: $(this).parent().data("td-id"),
                idEstado: 119,
                ObservacionCierre: ""
            }
            confirmarHorarioPlan(json, this);
            $("#sltEstadoHoraPlanCuidados").val(json.idEstado)
            $("#txtObservacionesEstadoPlanCuidados").val(json.ObservacionCierre)
        });

    } else if ($(`#tblPlancuidados tr[data-tr-id="${idAgregar}"] td button.hora`).length == 12) {
        toastr.warning("El Maximo es 12")
        return false
    }
    else {
        toastr.error("Debe ingresar una hora")
    }
}

function confirmarHorarioPlan(json, e = null) {

    let contenido = ""

    const totalButton = $(`#tblPlancuidados tr[data-tr-id="${json.idHorario}"] td button.hora`).length

    if (json.hora !== "") {

        contenido += `<div class="m-1" data-div-id-plan="${json.idHorario}div${totalButton}" style="display:inline-block;">
                        <button type="button"
                            data-id="${json.idHorario}div${totalButton}"
                            data-id-hora="${json.idHorario}"
                            data-id-eliminar-hora="${totalButton}"
                            data-id-editar-hora="${totalButton}"
                            data-nombre-actividad="${json.nombreActividad}"
                            data-estado-plan-id="119"
							data-observacion-id-estado="${json.idEstado}"
							data-observacion-cierre="${json.ObservacionCierre}"
                            data-hora="${json.hora}"
                            data-id-plan="${json.idPlan}"
                            data-toggle="tooltip"
                            onclick="mostrarEstadoHorarioPlanCuidado(this)"
                            class="btn btn-secondary m-0 hora"
                         >
                                ${json.hora}
                        </button>
                        <div style="display:inline-block;">
                            <i class="fa fa-times mr-2"
                                data-id-eliminar-hora="${totalButton}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Eliminar Hora"
                                style="cursor:pointer;color:#dc3545;display:block;" onclick="eliminarHoraPlanCuidados(this)">
                            </i>

                            <i class="fas fa-pencil-alt"
                                data-id-editar-hora="${totalButton}"
                                data-id-editar-tr="${json.idHorario}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Editar hora"
                                style="cursor:pointer;color:#007bff;display:block;" onclick="editarHoraPlanCuidados(this)">
                            </i>
                        </div>                       
                    </div>`
        $(`#tblPlancuidados td[data-td-id="${json.id}"]`).append(contenido)

        if (e != null)
            $(e).remove()
        $(`#tblPlancuidados td[data-td-id="${json.id}"] .invalid-input`).remove()

        toastr.success(`confirma hora: <strong>${json.hora}</strong>`)
    }

    llenarDatosPlanCuidados()
}

function mostrarEstadoHorarioPlanCuidado(e) {

    let idHoraPlan = $(e).data("id-hora") // TR
    let horaPlan = $(e).data("hora")
    let nombreActividad = $(e).data("nombre-actividad")
    let index = $(e).data("id-editar-hora") // index
    let val = parseInt($(e).data("estado-plan-id"))
    let observacionIdEstado = $(e).data("observacion-id-estado")
    let obs = $(e).attr("title")

    if (observacionIdEstado === 119) {
        $("#sltEstadoHoraPlanCuidados").val(val)
    }
    else if (observacionIdEstado === 120)
        $("#sltEstadoHoraPlanCuidados").val(val)
    else if (observacionIdEstado === 121)
        $("#sltEstadoHoraPlanCuidados").val(val)
    else
        $("#sltEstadoHoraPlanCuidados").val(val)

    $("#hiddenEstadoPlanCuidadosTR").val(idHoraPlan)
    $("#hiddenEstadoHoraPlanCuidados").val(index)
    $("#txtObservacionesEstadoPlanCuidados").val(obs)

    let tituloItemMdlEstado = `Hora ${nombreActividad}: ${horaPlan} `

    $("#tituloItemPlanCuidado").text(tituloItemMdlEstado)
    $("#hiddenEstadoPlanCuidadosTR").val(idHoraPlan)
    $("#hiddenEstadoPlanCuidados").val(index)
    $("#mdlEstadosItemPlanCuidados").modal("show")
}

function guardartEstadoPlanCuidados(e) {



    $("#sltEstadoHoraPlanCuidados").change(function () {
        if ($(this).val === "0") {
            $(this).attr("data-required", true)
        }
        if ($(this).val() === "121") {
            $("#txtObservacionesEstadoPlanCuidados").attr("data-required", true)
        } else if ($(this).val() === "119") {
            $("#txtObservacionesEstadoPlanCuidados").attr("data-required", false)
        }
        else if ($(this).val() === "120") {
            $("#txtObservacionesEstadoPlanCuidados").attr("data-required", false)

        }
    })

    if (!validarCampos("#divMdlEstadosItemPlanCuidados", false)) {
        return false
    }


    let trd = parseInt($("#hiddenEstadoPlanCuidadosTR").val())
    let index = parseInt($("#hiddenEstadoPlanCuidados").val())
    let tr = $(`#tblPlancuidados tr`)
    let titleToolTip = ""

    $("#hiddenEstadoPlanCuidadosTR").val(trd)
    $("#hiddenEstadoPlanCuidados").val(index)

    tr.find("td .spanIdPlanCuidado").each(function (indice, element) {
        buttons = tr.find(`td:eq(1) button[data-id-hora="${indice}"]`)
        buttons.each(function (i, element) {
            if (i === index && indice === trd) {
                let idEstado = parseInt($("#sltEstadoHoraPlanCuidados").val())

                if (idEstado === 119) {
                    $(element).removeClass("btn btn-success")
                    $(element).removeClass("btn btn-danger")
                    $(element).addClass("btn btn-secondary")
                    titleToolTip = $("#txtObservacionesEstadoPlanCuidados").val()
                    $("#sltEstadoHoraPlanCuidados").val(idEstado)
                }
                else if (idEstado === 120) {
                    $(element).removeClass("btn btn-secondary")
                    $(element).removeClass("btn btn-danger")
                    $(element).addClass("btn btn-success")
                    titleToolTip = $("#txtObservacionesEstadoPlanCuidados").val()
                    $("#sltEstadoHoraPlanCuidados").val(idEstado)
                }
                else if (idEstado === 121) {
                    $(element).removeClass("btn btn-secondary")
                    $(element).removeClass("btn btn-success")
                    $(element).addClass("btn btn-danger")
                    titleToolTip = $("#txtObservacionesEstadoPlanCuidados").val()
                    $("#sltEstadoHoraPlanCuidados").val(idEstado)
                }

                $(element).attr("title", titleToolTip)

                $("#sltEstadoHoraPlanCuidados").val()
                $(element).data("estado-plan-id", idEstado)
                $(element).data("observacion-estado", "hola")
            }
        })
    })

    $("#mdlEstadosItemPlanCuidados").modal("hide")

    llenarDatosPlanCuidados()
}

function editarHoraPlanCuidados(e) {

    let idEditarHoraPlanTR = $(e).data("id-editar-tr")
    let idEditarHoraPlanCuidados = $(e).data("id-editar-hora")

    $("#hiddenHoraPlanCuidadosTR").val(idEditarHoraPlanTR)
    $("#hiddenHoraPlanCuidados").val(idEditarHoraPlanCuidados)

    $(e).parent().parent().find("button").each(function (index, element) {
        if ($(element).data("id-eliminar-hora") === idEditarHoraPlanCuidados)
            $("#txtHoraPlanCuidados").val($(element).data("hora"))
    })

    $("#mdlEditarHoraPlanCuidados").modal("show")
}

function updateHoraPlanCuidados(e) {


    if (validarCampos("#divMdlEditarHorarioPlanCuidados", false)) {

        let hiddenHoraPlanCuidadosTR = parseInt($("#hiddenHoraPlanCuidadosTR").val())
        let hiddenHoraPlanCuidados = parseInt($("#hiddenHoraPlanCuidados").val())

        $(`#tblPlancuidados tr[data-tr-id="${hiddenHoraPlanCuidadosTR}"] td button.hora`).each(function (index, element) {

            if ($(element).data("id-editar-hora") === hiddenHoraPlanCuidados) {

                $(element).data("hora", $("#txtHoraPlanCuidados").val())
                $(element).text($("#txtHoraPlanCuidados").val())
            }
        })

        $("#txtHoraPlanCuidados").val("")
        $("#mdlEditarHoraPlanCuidados").modal("hide")

        toastr.success("Hora actualizada correctamente")
        llenarDatosPlanCuidados()
    }
    else {
        toastr.error("Debe ingresar una Hora")
    }
}

function eliminarHoraPlanCuidados(e) {

    let idEliminarHoraPlanCuidados = $(e).data("id-eliminar-hora")

    Swal.fire({
        title: `¿Seguro quieres eliminar la hora?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            $(e).parent().parent().find("button").each(function (index, element) {
                if ($(element).data("id-eliminar-hora") === idEliminarHoraPlanCuidados) {
                    $(element).parent().remove()
                }
            })
            toastr.error("Hora eliminada")
            llenarDatosPlanCuidados()
        }
    })
}

function llenarDatosPlanCuidados() {

    let json = []
    let IdActividad = []
    let Horarios = []
    let buttons = []
    let tr = $(`#tblPlancuidados tr`)
    let id = 0
    let observacionCierre = null

    tr.find("td .spanIdPlanCuidado").each(function (index, element) {
        IdActividad = $(element).text()
        buttons = tr.find(`td:eq(1) button[data-id-hora="${index}"]`)

        Horarios = [...buttons.map((index, element) => {
            id = $(element).data("id-plan")
            let idEstado = parseInt($(element).data("estado-plan-id"))
            idEstado = (idEstado === 0) ? 119 : idEstado
            observacionCierre = ($(element).attr("title") === "") ? null : $(element).attr("title")

            let horarios = []
            horarios =
                [{
                    IdEstado: idEstado,
                    Horario: $(element).data("hora"),
                    ObservacionCierre: observacionCierre ?? null
                }]
            return horarios
        })]



        json.push({ Id: id, Horarios: Horarios, Observacion: null })
    })

    arrayHorariosPlan = [...json]
    const horarioFiltrado = arrayHorariosPlan.filter(x => x.Horarios.length > 0)
    arrayHorariosPlan = [...horarioFiltrado]
}

function limpiarHorariosPlanCuidados(e, id) {

    Swal.fire({
        title: `¿Seguro quieres eliminar la programación de la actividad?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            id = $(e).data("limpiar-plan")
            arrayHorariosPlan.find(x => x.Id == id)
            $(`#tblPlancuidados td[data-td-id="${id}"]`).empty()
            arrayHorariosPlan = []
            llenarDatosPlanCuidados()
        }
    })

    llenarDatosPlanCuidados()
}
