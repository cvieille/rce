﻿let tablaEvolucion = $("#tblHojaEnfemeriaEvolucion").DataTable({
    columns: [
        { title: 'ID', data: 'id' },
        { title: 'Descripcion', data: 'descripcion' },
        { title: 'Fecha', data: 'fecha' },
        { title: 'Profesional', data: 'profesional' },
        { title: 'Acciones', data: 'acciones' }],
    order: [[2, 'desc']]
})

function configuracionTablaEvolucionHE() {

    tablaEvolucion.columns(0).visible(false)
    tablaEvolucion.column(1).header().style.width = "50%"
    tablaEvolucion.column(2).header().style.width = "10%"
    tablaEvolucion.column(4).header().style.width = "20%"
}

function dibujaAccionesEvolucion(id) {

    let acciones = `
			<div class="text-center">
				<button
				type="button"
				class="btn btn-danger btn-sm"
				data-id="${id}"
				id="btn-evolucion-${id}"
				data-toggle="tooltip"
				data-placement="bottom"
				title="Eliminar"
				style="width:60px;display:inline;"
				onclick="elminarEvolucionHE(this)"
			>
				<i class="fas fa-trash-alt fa-sm"></i>
				</button>
			</div>`

    return acciones
}

async function getHojaEnfermeriaEvolucion() {

    configuracionTablaEvolucionHE()

    tablaEvolucion.draw()
    await actualizarDatosTablaEvolucion()
}

async function actualizarDatosTablaEvolucion() {
    let evolucion = await getEvolucionHojaEnfermeria()
    tablaEvolucion.clear()
    await dibujarTablaEvolucion(evolucion)
}

async function agregarEvolucionHojaEnfermeria() {

    if (!validarCampos("#divEvolucionesHE", false))
        return

    let IdHojaEnfermeria = sesion.ID_HOJA_ENFERMERIA
    let Descripcion = $("#txtEvolucionAtencion").val()

    let evolucion = {
        IdHojaEnfermeria,
        Descripcion
    }

    await guardarEvolucionHojaEnfermeria(evolucion)
    await actualizarDatosTablaEvolucion()

    await sleep(500)
    $("#nav-historialEvolucion-tab").trigger("click")

    $("#txtEvolucionAtencion").val("")

}

async function guardarEvolucionHojaEnfermeria(evolucion) {

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/Evolucion`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(evolucion),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se agregó nueva evolución`)
        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar evolución en hoja de enfermería: ${JSON.stringify(jqXHR)} `)
        }
    })
}

async function getEvolucionHojaEnfermeria() {

    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/Hospitalizacion/${ID_HOSPITALIZACION}/Evolucion`

    let evolucion = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (evolucion) {

            },
        error: function (err) {
            console.error("Error al llenar evolución")
            console.error(JSON.stringify(err))
        }
    })

    return evolucion
}

async function dibujarTablaEvolucion(data) {

    data.forEach(x => {
        let row = x

        let acciones = dibujaAccionesEvolucion(row.Id)
        tablaEvolucion.row.add({
            id: row.Id,
            descripcion: row.Descripcion,
            fecha: moment(row.Fecha).format("DD-MM-YYYY HH:mm"),
            profesional: `${row.Profesional.Nombre} ${row.Profesional.ApellidoPaterno} ${row.Profesional.ApellidoMaterno}`,
            acciones: acciones
        });
    })

    tablaEvolucion.draw();
}

async function elminarEvolucionHE(e) {

    let id = $(e).data("id")

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/Evolucion`
    }

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el registro evolución?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, jqXHR) {
                toastr.success(`Evolución eliminada correctamente`)
                actualizarDatosTablaEvolucion()
            },
            error: function (jqXHR, status) {
                console.log(`Error al eliminar evolución hoja enfermería: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}
	