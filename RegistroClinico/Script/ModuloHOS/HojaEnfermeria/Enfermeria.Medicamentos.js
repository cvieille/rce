﻿let arrayHorariosMed = []

// Fetch de datos Medicamentos
async function getMedicamentosPaciente() {

    const fechaHojaEnfermeria = sesion.FECHA_HOJA_ENFERMERIA
    let fechaHojaEnfermeriaFormateada = `${moment(fechaHojaEnfermeria).format("YYYY-MM-DD")}`

    //url desarrollo
    const urlDesarrollo = `${GetWebApiUrl()}HOS_Medicamento_Arsenal_Receta_Indicaciones/Buscar?idHospitalizacion=${sesion.ID_HOSPITALIZACION}&fecha=${fechaHojaEnfermeriaFormateada}`
    //url ejemplo
    const urlEjemplo = `${GetWebApiUrl()}HOS_Medicamento_Arsenal_Receta_Indicaciones/Buscar?idHospitalizacion=98816&fecha=2022-07-20`

    $.ajax({
        type: "GET",
        url: urlEjemplo,
        contentType: "application/json",
        dataType: "json",
        async: true,
        success: function (data) {
            if (data.length > 0) {
                $("#tblMedicamentos").show()
                dibujarTablaMedicamentosPaciente(data)
                comboCantidadMedicamentos()

            }
            else {
                $("#tblMedicamentos").hide()
                $("#tituloMedicamentos").text("Nada para mostrar")
            }
        },
        error: function (err) {
            console.log("No se pudieron cargar los medicamentos")
        }
    })

    const medicamentos = await getMedicamentosHE()

    await llenarHorariosMedicamentos(medicamentos, [])
}

async function getMedicamentosHE() {

    try {
        const medicamentos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/Medicamentos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return medicamentos

    } catch (error) {
        console.error("Error al cargar medicamentos")
        console.log(JSON.stringify(error))
    }
}

function getDataUpdateMedicamentos(Medicamentos) {

    if (!Medicamentos.length > 0)
        return

    Medicamentos.map(x => {
        return {
            Id: x.Id,
            IdMedicamento: x.Medicamento.Id,
            IdMedicamentoReceta: x.Indicaciones.Id,
            Horarios: { TipoEstado: x.TipoEstado.Id, Horario: x.Horario, Observaciones: x.Observaciones }
        }
    })

    let arrayMedicamentos = [...new Set(Medicamentos.map(x => x.Indicaciones.Id))].map(id => {
        return {
            IdMedicamentoReceta: Medicamentos.find(x => x.Indicaciones.Id == id).Indicaciones.Id,
            IdMedicamento: Medicamentos.find(x => x.Indicaciones.Id == id).Medicamento.Id,
            NombreMedicamento: Medicamentos.find(x => x.Indicaciones.Id == id).Medicamento.Valor,
            Horarios: Medicamentos.filter(x => x.Indicaciones.Id == id).map(x => {
                return {
                    Id: x.Id,
                    IdTipoEstado: x.TipoEstado.Id,
                    Horario: x.Horario,
                    Observaciones: x.Observaciones
                }
            }),
        }
    })

    return arrayMedicamentos
}

//dibuja tabla medicamentos del paciente
function dibujarTablaMedicamentosPaciente(data) {

    let contenido = ""

    data.map((x, i) => {

        contenido += `<tr data-tr-id="${i}" data-tr-medicameto-id="${x.Id}">
                        <td class="pl-3 pr-3" style="width: 1%; white-space:nowrap;">
                            <div class="row">
                                <strong><span class="spanIdMedicamento"></span> ${x.Nombre} (${x.Forma})</strong>
                            </div>
                            <div class="row">
                                <span>
                                    ${x.Indicaciones.Dosis}
                                    ${x.Indicaciones.Frecuencia},
                                    ${x.Indicaciones.Duracion ?? ""}
                                    ${x.Indicaciones.ViaMedicamento}
                                </span>
                            </div>
                        </td>
                        <td data-td-id="${i}">                           
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-outline-success btn-sm m-1"
                                onclick='agregarHorariosMedicamentos(this)'
                                data-agregar-medicamento="${i}"
                                data-id-medicamento="${x.Id}"
                                data-id-medicamento-receta="${x.Indicaciones.Id}"
                                data-nombre-medicamento="${x.Nombre}"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Agregar"
                                style="cursor:pointer;width:80px;">
                                <i class="fa fa-plus fa-lg"></i>
                            </button>
                            <button type="button" class="btn btn-outline-danger btn-sm m-1"
                                onclick='limpiarHorariosMedicamentos(this,${i})'
                                data-limpiar-medicamento="${i}"
                                data-id-medicamento="${x.Id}"
                                data-id-medicamento-receta="${x.Indicaciones.Id}"
                                data-nombre-medicamento="${x.Nombre}"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Limpiar"
                                style="cursor:pointer;width:80px;">
                                <i class="fa fa-trash fa-lg"></i>
                            </button>
                        </td>
                      </tr>
        `
    })

    $("#tbodyMedicamentos").append(contenido)
}

async function llenarHorariosMedicamentos(arrMedicamentosSinFormato = [], Medicamentos = []) {

    let arrayMedicamentos = []

    if (arrMedicamentosSinFormato.length > 0) {
        arrayMedicamentos = getDataUpdateMedicamentos(arrMedicamentosSinFormato)
    }
    else if (Medicamentos.length > 0) {
        arrayMedicamentos = Medicamentos
    }

    let tr = $("#tblMedicamentos tr")
    let json = {}

    tr.each(function (index, element) {
        if ($(element).data("tr-medicameto-id")) {
            let trMedicamentoId = $(element).data("tr-medicameto-id")
            arrayMedicamentos.map(x => {
                if (x.IdMedicamento === trMedicamentoId) {
                    x.Horarios.map((e, i) => {
                        json =
                        {
                            id: e.Id,
                            idHorario: $(element).data("tr-id"),
                            idMedicamento: x.IdMedicamento,
                            idMedicamentoReceta: x.IdMedicamentoReceta,
                            NombreMedicamento: x.NombreMedicamento,
                            hora: moment(e.Horario, 'HH:mm').format('HH:mm'),
                            trId: $(element).data("tr-id"),
                            idTipoEstado: e.IdTipoEstado,
                            observaciones: null ?? e.Observaciones
                        }


                        if (json.idTipoEstado === 133) { // Indicado por médico
                            clase = "btn btn-secondary"
                        }
                        else if (json.idTipoEstado === 134) { // Administrado
                            clase = "btn btn-success"
                        }
                        else if (json.idTipoEstado === 135) { // Suspendido por médico
                            clase = "btn btn-warning"
                        }
                        else if (json.idTipoEstado === 136) { // Rechazado por paciente
                            clase = "btn btn-danger"
                        }
                        else if (json.idTipoEstado === 139) { // Paciente en ayunas
                            clase = "btn btn-info"
                        }
                        else if (json.idTipoEstado === 140) { // paciente en regimen 0
                            clase = "btn btn-primary"
                        }
                        else if (json.idTipoEstado === 141) { // No se cuenta con el medicamento
                            clase = "btn btn-dark"
                        }
                        else if (json.idTipoEstado === 150) { // Paciente en procedimiento
                            clase = "btn btn-success"
                        }
                        else if (json.idTipoEstado === 151) { // Paciente por alteracion de conciencia
                            clase = "btn btn-danger"
                        }

                        $(element).find("td:eq(1)").append(`
                        <div class="m-1" data-div-id-medicamento="${json.idHorario}div${i}" style="display:inline-block;">
                            <button type="button"
                                data-id="${json.id}"
                                data-id-div="${json.idHorario}div${i}"
                                data-id-hora="${json.idHorario}"
                                data-id-eliminar-hora="${i}"
                                data-id-editar-hora="${i}"
                                data-id-medicamento="${json.idMedicamento}"
                                data-id-medicamento-receta="${json.idMedicamentoReceta}"
                                data-nombre-medicamento="${json.NombreMedicamento}"
                                data-tipo-estado-id="${json.idTipoEstado}"
							    data-observaciones="${json.observaciones ?? ""}"
                                data-hora="${json.hora}"
                                data-toggle="tooltip"
                                title="${json.observaciones ?? ""}"
                                onclick="mostrarEstadoHorarioMedicamento(this)"
                                class="${clase} hora"
                             >
                                ${json.hora}
                        </button>
                        <div style="display:inline-block;">
                            <i class="fa fa-times mr-2"
                                data-id-eliminar-hora="${i}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Eliminar Hora"
                                style="cursor:pointer;color:#dc3545;display:block;" onclick="eliminarHoraMedicamento(this)">
                            </i>

                            <i class="fas fa-pencil-alt"
                                data-id-editar-hora="${i}"
                                data-id-editar-tr="${json.trId}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Editar hora"
                                style="cursor:pointer;color:#007bff;display:block;" onclick="editarHoraMedicamento(this)">
                            </i>
                        </div>
                    </div>
                    `)
                    })
                }
            })
        }
    })

    $('[data-toggle="tooltip"]').tooltip()
}

function agregarHorariosMedicamentos(e) {

    let idAgregar = $(e).data("agregar-medicamento")
    let idMedicamento = $(e).data("id-medicamento")
    let idMedicamentoReceta = $(e).data("id-medicamento-receta")
    let nombreMedicamento = $(e).data("nombre-medicamento")

    const totalInput = $(`#tblMedicamentos tr[data-tr-id="${idAgregar}"] td input`).length
    const totalButton = $(`#tblMedicamentos tr[data-tr-id="${idAgregar}"] td button.hora`).length

    if (totalInput < 12 && totalButton < 12 && validarCampos("#validaHorariosMedicamentos", true)) {

        $(`#tblMedicamentos td[data-td-id="${idAgregar}"]`)
            .append(
                `
                    <input id='txtHorarioMedicamento_${idAgregar}_${totalInput}' type="time"
                        class="form-control m-2"                    
                        placeholder="horario"
                        data-id-horarios-medicamentos="${idAgregar}"
                        data-id-medicamento="${idMedicamento}"
                        data-id-medicamento-receta="${idMedicamentoReceta}"
                        data-nombre-medicamento="${nombreMedicamento}"
						data-tipo-estado-id="133"
                        style="width:100px;display:inline;"
                        data-required="true"
                     >`
            )

        $(`#txtHorarioMedicamento_${idAgregar}_${totalInput}`).on('blur', function () {

            json =
            {
                idHorario: idAgregar,
                id: $(this).data("id"),
                idMedicamento: $(this).data("id-medicamento"),
                idMedicamentoReceta: $(this).data("id-medicamento-receta"),
                NombreMedicamento: $(this).data("nombre-medicamento"),
                hora: $(this).val(),
                trId: $(this).parent().data("td-id"),
                idTipoEstado: 133,
                observaciones: ""
            }

            if (json.id === undefined)
                delete json.id

            confirmarHorarioMedicamento(json, this);
            $("#sltEstadoMedicamento").val(json.idTipoEstado)
            $("#txtObservacionesEstadoMedicamento").val(json.observaciones)
        });


    } else if ($(`#tblMedicamentos tr[data-tr-id="${json.idHorario}"] td button.hora`).length == 12) {
        toastr.warning("El Maximo es 12")
    }
    else {
        toastr.error("Debe ingresar una hora")
    }
}

function confirmarHorarioMedicamento(json, e = null) {

    let contenido = ""
    let idHorario = $(e).data("id-horarios-medicamentos")

    let hora = $(e).val()
    const totalButton = $(`#tblMedicamentos tr[data-tr-id="${idHorario}"] td button.hora`).length

    if (hora !== "") {

        contenido += `<div class="m-1" data-div-id-medicamento="${json.idHorario}div${totalButton}" style="display:inline-block;">
                        <button type="button"
                            data-id="0"
                            data-id-div="${json.idHorario}div${totalButton}"
                            data-id-hora="${json.idHorario}"
                            data-id-eliminar-hora="${totalButton}"
                            data-id-editar-hora="${totalButton}"
                            data-nombre-medicamento="${json.NombreMedicamento}"
                            data-tipo-estado-id="133"
							data-observaciones="${json.observaciones ?? ""}"
                            data-hora="${json.hora}"
                            data-id-medicamento="${json.idMedicamento}"
                            data-id-medicamento-receta="${json.idMedicamentoReceta}"
                            data-toggle="tooltip"
                            title="${json.observaciones ?? ""}"
                            onclick="mostrarEstadoHorarioMedicamento(this)"
                            class="btn btn-secondary m-0 hora"
                         >
                                ${json.hora}
                        </button>
                        <div style="display:inline-block;">
                            <i class="fa fa-times mr-2"
                                data-id-eliminar-hora="${totalButton}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Eliminar Hora"
                                style="cursor:pointer;color:#dc3545;display:block;" onclick="eliminarHoraMedicamento(this)">
                            </i>

                            <i class="fas fa-pencil-alt"
                                data-id-editar-hora="${totalButton}"
                                data-id-editar-tr="${json.idHorario}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Editar hora"
                                style="cursor:pointer;color:#007bff;display:block;" onclick="editarHoraMedicamento(this)">
                            </i>
                        </div>
                       
                    </div>`
        $(`#tblMedicamentos td[data-td-id="${json.trId}"]`).append(contenido)
        if (e != null)
            $(e).remove()
        $(`#tblMedicamentos td[data-td-id="${json.trId}"] .invalid-input`).remove()
        toastr.success(`confirma hora: <strong>${json.hora}</strong>`)
        $('[data-toggle="tooltip"]').tooltip()
    }

    llenarDatosMedicamentos()
}

function mostrarEstadoHorarioMedicamento(e) {

    let idHoraMedicamento = $(e).data("id-hora") // TR
    let horaMedicamento = $(e).data("hora")
    let nombreMedicamento = $(e).data("nombre-medicamento")
    let index = $(e).data("id-editar-hora") // index
    let idTipoEstadoMedicamento = $(e).data("tipo-estado-id")
    let obs = $(e).data("observaciones")

    if (idTipoEstadoMedicamento === 133) { // Indicado por médico
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 133)
    }
    else if (idTipoEstadoMedicamento === 134) { // Administrado
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 134)
    }
    else if (idTipoEstadoMedicamento === 135) { // Suspendido por médico
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 135)
    }
    else if (idTipoEstadoMedicamento === 136) { // Rechazado por paciente
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 136)

    }
    else if (idTipoEstadoMedicamento === 139) { // Paciente en ayunas
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 139)
    }
    else if (idTipoEstadoMedicamento === 140) { // Paciente en regimen 0
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 140)
    }
    else if (idTipoEstadoMedicamento === 141) { // No se cuenta con el medicamento
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 141)
    }
    else if (idTipoEstadoMedicamento === 150) { // Paciente en procedimiento
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 150)
    }
    else if (idTipoEstadoMedicamento === 151) { // Paciente con alteracion de conciencia
        $("#sltEstadoMedicamento").val(idTipoEstadoMedicamento)
        $(e).data("tipo-estado-id", 151)
    }
    else {
        console.log("Estado medicamento invalido")
    }




    $("#hiddenEstadoMedicacmentoTR").val(idHoraMedicamento)
    $("#hiddenEstadoHoraMedicacmento").val(index)
    $("#txtObservacionesEstadoMedicamento").val(obs)

    let tituloItemMdlEstado = `Hora ${nombreMedicamento}: ${horaMedicamento} `

    $("#tituloItemMedicamentos").text(tituloItemMdlEstado)
    $("#hiddenEstadoMedicamentoTR").val(idHoraMedicamento)
    $("#hiddenEstadoMedicamento").val(index)
    $("#mdlEstadosItemMedicamentos").modal("show")
}

function removeClasesBtnColor(e) {

    $(e).removeClass("btn-secondary btn-success btn-warning btn-danger btn-info btn-primary btn-dark btn-success btn-danger")
}

function cambiarColorEstadoBoton(e) {

    let idEstado = Number($("#sltEstadoMedicamento").val())

    removeClasesBtnColor($(e))

    let objeto = {

        133: "btn-secondary",
        134: "btn-success",
        135: "btn-warning",
        136: "btn-danger",
        139: "btn-info",
        140: "btn-primary",
        141: "btn-dark",
        150: "btn-success",
        151: "btn-danger"
    }

    $(e).addClass(objeto[idEstado])
}

function guardartEstadoMedicamento(e) {

    let trd = parseInt($("#hiddenEstadoMedicamentoTR").val()) // fila
    let index = parseInt($("#hiddenEstadoMedicamento").val()) // indice de horario por fila este debe comenzar en 0
    let tr = $(`#tblMedicamentos tr`)

    $("#hiddenEstadoMedicamentoTR").val(trd)
    $("#hiddenEstadoMedicamento").val(index)

    tr.find("td .spanIdMedicamento").each(function (indice, element) {

        buttons = tr.find(`td:eq(1) button[data-id-hora="${indice}"]`)

        buttons.each(function (i, element) {
            if (i === index && indice === trd) {

                $(element).data("tipo-estado-id", parseInt($("#sltEstadoMedicamento").val()))
                $(element).data("observaciones", $("#txtObservacionesEstadoMedicamento").val())
                $(element).attr("title", $("#txtObservacionesEstadoMedicamento").val())

                cambiarColorEstadoBoton($(element))
            }
        })
    })

    $("#mdlEstadosItemMedicamentos").modal("hide")
    llenarHorariosMedicamentos([], arrayHorariosMed)
    llenarDatosMedicamentos()
}

function editarHoraMedicamento(e) {

    let idEditarHoraMedicamentoTR = $(e).data("id-editar-tr")
    let idEditarHoraMedicamento = $(e).data("id-editar-hora")

    $("#hiddenHoraMedicacmentoTR").val(idEditarHoraMedicamentoTR)
    $("#hiddenHoraMedicacmento").val(idEditarHoraMedicamento)

    $(e).parent().parent().find("button").each(function (index, element) {
        if ($(element).data("id-editar-hora") === idEditarHoraMedicamento)
            $("#txtHoraMedicamento").val($(element).data("hora"))
    })

    $("#mdlEditarHoraMedicamento").modal("show")
}

function updateHoraMedicamento(e) {

    if (validarCampos("#divMdlEditarHorarioMedicamento", false)) {

        let hiddenHoraMedicacmentoTR = parseInt($("#hiddenHoraMedicacmentoTR").val())
        let hiddenHoraMedicacmento = parseInt($("#hiddenHoraMedicacmento").val())

        $(`#tblMedicamentos tr[data-tr-id="${hiddenHoraMedicacmentoTR}"] td button`).each(function (index, element) {
            if ($(element).data("id-editar-hora") === hiddenHoraMedicacmento) {

                $(element).data("hora", $("#txtHoraMedicamento").val())
                $(element).text($("#txtHoraMedicamento").val())
            }
        })

        $("#txtHoraMedicamento").val("")
        $("#mdlEditarHoraMedicamento").modal("hide")

        toastr.success("Hora actualizada correctamente")
        llenarDatosMedicamentos()
    }
    else {
        toastr.error("Debe ingresar una Hora")
    }
}

function eliminarHoraMedicamento(e) {

    let idEliminarHoraMedicamento = $(e).data("id-eliminar-hora")

    Swal.fire({
        title: `¿Seguro quieres eliminar la hora?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            $(e).parent().parent().find("button").each(function (index, element) {
                if ($(element).data("id-eliminar-hora") === idEliminarHoraMedicamento) {
                    $(element).parent().remove()
                }
            })
            toastr.error("Hora eliminada")
            llenarDatosMedicamentos()
        }
    })
}

function llenarDatosMedicamentos() {

    let json = []
    let buttons = []
    let tr = $(`#tblMedicamentos tr`)

    tr.find("td .spanIdMedicamento").each(function (i, element) {

        buttons = tr.find(`td:eq(1) button[data-id-hora="${i}"]`)

        buttons.each(function (index, element) {

            json.push(
                {
                    Id: $(element).data("id"),
                    IdMedicamentoReceta: $(element).data("id-medicamento-receta"),
                    Horario: $(element).data("hora"),
                    IdTipoEstado: $(element).data("tipo-estado-id"),
                    Observaciones: ($(element).data("observaciones") === "") ? null : $(element).data("observaciones")
                })
        })
    })

    json = deleteIdUnicoMedicamento(json)

    arrayHorariosMed = [...json]
}

//Elimina Id si es una hora nueva
function deleteIdUnicoMedicamento(json) {
    json.map(medicamento => {
        if (medicamento.Id === 0)
            delete medicamento.Id
        return medicamento
    })

    return json
}

function limpiarHorariosMedicamentos(e, id) {

    Swal.fire({
        title: `¿Seguro quieres eliminar la programación?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            id = $(e).data("limpiar-medicamento")
            arrayHorariosMed.find(x => x.Id == id)
            $(`#tblMedicamentos td[data-td-id="${id}"]`).empty()
            arrayHorariosMed = []
            llenarDatosMedicamentos()
        }
    })
}

// llena combo del 1-12
function comboCantidadMedicamentos() {

    let arr = []
    for (let i = 1; i <= 12; i++) {
        arr.push(i)
    }

    arr.forEach(x => {
        if (x !== 0)
            $("#sltCantidadHorariosMedicamentos").append(`<option value="${x}">${x}</option>`)
    })
}