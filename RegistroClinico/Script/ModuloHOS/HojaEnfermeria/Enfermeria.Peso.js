﻿const sesionPeso = getSession()

let tablaTipoPeso = $("#tblHojaEnfemeriaPeso").DataTable({
    columns: [
        { title: 'ID', data: 'id' },
        { title: 'Peso', data: 'peso' },
        { title: 'Medida', data: 'medida' },
        { title: 'Hora', data: 'hora' },
        { title: 'Acciones', data: 'acciones' }],
    order: [[3, 'desc']]
})



const ID_HOJA_ENFERMERIA = sesionPeso.ID_HOJA_ENFERMERIA
const ID_HOSPITALIZACION = sesionPeso.ID_HOSPITALIZACION

function dibujaAccionesTipoPeso(id) {

    let acciones = `
			<div class="text-center">
				<button 
				type="button"
				class="btn btn-danger btn-sm"
				data-id="${id}"
				id="btn-peso-${id}"
				data-toggle="tooltip"
				data-placement="bottom"
				title="Eliminar"
				style="width:60px;display:inline;"
				onclick="elminarTipoPesoHE(this)"
			>
				<i class="fas fa-trash-alt fa-sm"></i>
				</button>
			</div>`

    return acciones
}

function configuracionTablaTipoPeso() {

    tablaTipoPeso.columns(0).visible(false)
    tablaTipoPeso.column(1).header().style.width = "10%"
    tablaTipoPeso.column(2).header().style.width = "10%"
    tablaTipoPeso.column(3).header().style.width = "10%"
    tablaTipoPeso.column(4).header().style.width = "10%"
}

async function getHojaEnfermeriaPeso() {

    configuracionTablaTipoPeso()
    tablaTipoPeso.draw()
    await actualizarDatosTablaTipoPeso()
}

async function actualizarDatosTablaTipoPeso() {
    let tipoPeso = await getPesoHojaEnfermeria()
    tablaTipoPeso.clear()
    await dibujarTablaTipoPeso(tipoPeso)
}

function limpiarCamposTipoPeso() {

    $("#txtPesoHE").val("")
    $("#sltTipoPesoHE").val("0")
}

async function agregarTipoPeso() {

    if (!validarCampos("#divTipoPesoHE", false))
        return

    let peso = $("#txtPesoHE").val()
    let medida = $("#sltTipoPesoHE").val()

    let tipoPeso = {
        "IdHojaEnfermeria": ID_HOJA_ENFERMERIA,
        "idTipoPeso": medida,
        "TipoPeso": peso
    }

    await guardarPesoHojaEnfermeria(tipoPeso)
    await actualizarDatosTablaTipoPeso()

    limpiarCamposTipoPeso()
}

async function guardarPesoHojaEnfermeria(tipoPeso) {

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/TipoPeso`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(tipoPeso),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se agregó nuevo peso`)
        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar peso en hoja de enfermería: ${JSON.stringify(jqXHR)} `)
        }
    })
}

async function getPesoHojaEnfermeria() {

    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/Hospitalizacion/${ID_HOSPITALIZACION}/TipoPeso`

    let TipoPeso = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (tipoPeso) {

            },
        error: function (err) {
            console.error("Error al llenar tipo peso")
            console.error(JSON.stringify(err))
        }
    })

    return TipoPeso
}

async function dibujarTablaTipoPeso(data) {

    data.forEach(x => {
        let row = x

        let acciones = dibujaAccionesTipoPeso(row.Id)
        tablaTipoPeso.row.add({
            id: row.Id,
            peso: row.TipoPeso,
            medida: row.TipoMedida.Valor,
            hora: moment(row.Fecha).format("DD-MM-YYYY HH:mm"),
            acciones: acciones
        });
    })

    tablaTipoPeso.draw();
}

async function elminarTipoPesoHE(e) {

    let id = $(e).data("id")

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/TipoPeso`
    }

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el registro de peso?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, jqXHR) {
                toastr.success(`Peso eliminado correctamente`)
                actualizarDatosTablaTipoPeso()
            },
            error: function (jqXHR, status) {
                console.log(`Error al eliminar tipo peso hoja enfermería: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}
