﻿//$(document).ready(function () {
//    ShowModalCargando(false)
//    getExamenesLab()

//})

//function getExamenesLab() {
//    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/Dashboard`

//    $.ajax({
//        type: "GET",
//        url: url,
//        contentType: "application/json",
//        dataType: "json",
//        async: true,
//        success: function (data) {
//            const { Examen } = data
//            dibujarExamenesLab(Examen)
//        },
//        error: function (err) {
//            console.log("No se pudieron cargar los Examenes Dashboard")
//        }
//    })
//}

//function agruparExamenesPorEstado(examen) {

//    const arrayExamenesAgrupados = [...new Set(examen.map(x => {
//        return x.TipoEstadosSistemas.Valor
//    }))].map(estado => {
//        let cont = 0
//        return {
//            Estado: estado,
//            Examenes: Object.values(examen.filter(x => {
//                if (x.TipoEstadosSistemas.Valor === estado) {
//                    cont++
//                    return x
//                }
//            }).map(x => x.TipoCartera)),
//            Cantidad: cont
//        }
//    })

//    return arrayExamenesAgrupados
//}

//function dibujarExamenesLab(examen) {

//    const examenesAgrupados = agruparExamenesPorEstado(examen)

//    let contenido = ""

//    examenesAgrupados.map(x => {
//        contenido += `<tr>
//                        <td>${x.Estado}</td>
//                        <td><span class="badge badge-pill badge-success">${x.Cantidad}</span></td>

//                        <td>
//                            <button type="button" class="btn btn-outline-primary btn-sm" onclick="verDetalleExamenesPorEstado(this)">Ver mas</button>
//                        </td>
//                      </tr>
//                     `
//    })

//    $("#tblExamenHEDashboard").append(contenido)
//}

//function verDetalleExamenesPorEstado(e) {
//    $("#mdlExamenesPorEstado").modal("show")
//}

let idUbicacion = 0
$(document).ready(async function () {

    ShowModalCargando(false)

    comboUbicacionSituacionActual("#seltUbicacionDashBoard")

    const dashboard = await getSituacionActualHospitalizacion(idUbicacion)
    await dibujarTablaSituacionActualHosp(dashboard)

    $("#seltUbicacionDashBoard").change(async function () {
        let value = parseInt($(this).val())

        idUbicacion = value
        ShowModalCargando(true)
        const dashboard = await getSituacionActualHospitalizacion(idUbicacion)
        await dibujarTablaSituacionActualHosp(dashboard)
        ShowModalCargando(false)
    })
})

async function getSituacionActualHospitalizacion(idUbicacion = 0) {

    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Dashboard?idUbicacion=${idUbicacion}`

    try {
        const resultados = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return resultados

    } catch (error) {
        console.error("Error al cargar situacion actual hospitalizacion")
        console.log(JSON.stringify(error))
    }
}

function dibujarTablaSituacionActualHosp(dashboard) {

    if (!Array.isArray(dashboard))
        return

    const adataset = dashboard.map(d => [d.Estado.Id, d.Estado.Id, d.Estado.Valor, d.CantidadHospitalizaciones])

    $("#tblDashboardSituacionActual").addClass("nowrap").DataTable({

        data: adataset,
        order: [],
        columns: [
            { title: "Id" },
            { title: "IdEstado" },
            { title: "Estado" },
            { title: "Cantidad de Atenciones" },
            { title: "Acciones", className: "text-center" }
        ],
        "columnDefs": [
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    let fila = meta.row;

                    botones =
                        `<div>
                            <a id='detalleSituacionActualHop-${fila}' 
                            data-id='${row[0]}'
                            data-id-estado='${row[1]}'
                            data-nombre-estado='${row[2]}'
                            title='Ver más'
                            class='btn btn-info mr-2' href='#/' 
                            onclick='showDetalleSituacionActualHosp(this);'
                            >
                                <i class='fa fa-eye'></i>
                            </a>`;
                    return botones;
                }
            },
            {
                targets: 2,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    return `${getIconsEstadosSituacionActual(row[0])} ${row[2]}`
                }
            },
            {
                "targets": [0, 1],
                "visible": false,
                "searchable": false
            }
        ],
        "bDestroy": true
    });
}

function comboUbicacionSituacionActual(element) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, element);
}

async function showDetalleSituacionActualHosp(e) {

    //let Estado = $(e).data()
    let idEstado = $(e).data("id-estado")
    let nombreEstado = $(e).data("nombre-estado")
    let idUbicacionHosp = idUbicacion

    ShowModalCargando(true)
    const detalle = await getDetalleSituacionActual(idEstado, idUbicacionHosp)
    dibujarTablaSituacionActuaDetallelHosp(detalle, nombreEstado)
    ShowModalCargando(false)

    $("#mdlDetalleSituacionActualHosp").modal("show")
}

async function getDetalleSituacionActual(idEstado = 0, idUbicacion = 0) {

    let url = ""

    if (idUbicacion === 0)
        url = `${GetWebApiUrl()}HOS_Hospitalizacion/Dashboard/Buscar?idEstado=${idEstado}`
    if (idUbicacion > 0)
        url = `${GetWebApiUrl()}HOS_Hospitalizacion/Dashboard/Buscar?idEstado=${idEstado}&idUbicacion=${idUbicacion}`

    try {
        const resultados = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return resultados

    } catch (error) {
        console.error("Error al cargar Detalle situacion actual hospitalizacion")
        console.log(JSON.stringify(error))
    }

}

function dibujarTablaSituacionActuaDetallelHosp(detalle, Estado) {

    if (!Array.isArray(detalle))
        return

    const adataset = detalle.map(d =>
        [
            d.Id, moment(d.Fecha).format('DD-MM-YYYY'),
            d.Paciente.Nombre,
            d.Paciente.Rut,
            d.Ubicacion.Valor,
            d.Cama.Id,
            d.Cama.Valor ?? "No asignada",
            d.Estado.Id,
            d.Estado.Valor
        ]
    )

    $("#tituloModalDetalleSituacionActual").text(Estado)

    $("#tblDetalleSituacionActualHosp").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        data: adataset,
        processing: true,
        responsive: true,
        searching: false,
        autoWidth: true,
        columns: [
            { title: "ID" },
            { title: "Fecha" },
            { title: "Nombre" },
            { title: "RUT" },
            { title: "Ubicación" },
            { title: "idCama" },
            { title: "Cama" },
            { title: "IdEstado" },
            { title: "Estado" },
            { title: "Acciones", className: "text-center" }
        ],
        "columnDefs": [
            {
                targets: [5, 7],
                visible: false,
                searchable: false
            },
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    let fila = meta.row;

                    botones =
                        `<div>
                            <a id='detalleSituacionActualHop-${fila}' 
                            data-id='${row[0]}' 
                            title='Ver Detalle'
                            class='btn btn-info mr-2' href='#/' 
                            onclick='printModalDetalleHospitalizacion(this);'
                            >
                                <i class='fa fa-print'></i>
                            </a>`;
                    return botones;
                }
            },
            {
                targets: 2,
                render: function (data, type, full, meta) {
                    return "<div style='white-space:pre-line;'>" + data + "</div>";
                }
            },
            {
                targets: 3,
                render: function (data, type, full, meta) {
                    return "<div style='white-space:nowrap;'>" + data + "</div>";
                }
            },
            {
                targets: 4,
                render: function (data, type, full, meta) {
                    return "<div style='white-space:pre-line'>" + data + "</div>";
                }
            },
            {
                targets: 4,
                render: function (data, type, full, meta) {
                    return "<div style='white-space:pre-line'>" + data + "</div>";
                }
            },
            //ocultar columnas en responsivo.
            { responsivePriority: 1, targets: 0 }, // ID HOSPITALIZACION
            { responsivePriority: 2, targets: 2 }, // NOMBRE
            { responsivePriority: 3, targets: -1 }, // ACCIONES

        ],
        "bDestroy": true
    });
}

function getIconsEstadosSituacionActual(idEstadoSistema) {

    let paddingBadge = ""
    let inicial = "", icons = "", className = "";

    if (idEstadoSistema === 156) {
        className = "btn-warning";
        inicial = "T";
    }
    else if (idEstadoSistema === 87) {
        className = "btn-secondary";
        inicial = "P";
    }
    else if (idEstadoSistema === 88) {
        className = "btn-success";
        inicial = "I";
    }

    if (inicial === "T")
        paddingBadge = `style="padding:3px 11px;"`
    if (inicial === "I")
        paddingBadge = `style="padding:3px 14px;"`
    if (inicial === "P")
        paddingBadge = `style="padding:3px 12px;"`

    icons = `<span id="bgEstado" class="badge badge-pill ${className} badge-count" ${paddingBadge}>${inicial}</span>`; //&nbsp;


    return `${icons}</h5>`;
}

function printModalDetalleHospitalizacion(e) {

    let idHospitalizacion = $(e).data("id")

    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Imprimir`
    ImprimirApiExterno(url)
}