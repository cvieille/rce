﻿
let tablaPlanCuidado = $("#tblPlancuidadosHE").DataTable({
    columns: [{ title: 'ID', data: 'id' }, { title: 'Actividad', data: 'valor' }, { title: 'Horarios', data: 'horarios' }, { title: 'Acciones', data: 'acciones' }]
})


let selectorPlan = document.querySelector("#sltTipoCuidado")
const TOTAL_HORARIOS = 12

const TipoEstado = {
    PENDIENTE: 119,
    REALIZADO: 120,
    NO_REALIZADO: 121
}


function dibujaAcciones(id, valor) {

    let acciones = `<button
            type="button"
            class="btn btn-success btn-sm"
            data-id="fila-${id}"
            data-id-actividad="${id}"
            data-valor-actividad="${valor}"
            id="add-${id}"
            data-toggle="tooltip"
            data-placement="bottom"
            title="Agregar"
            style="width:100px;display:inline;"
            onclick="AgregarHorarioTipoCuidado(this)"
        >
            <i class="fas fa-plus fa-sm"></i>
        </button>
        <button
            type="button"
            class="btn btn-danger btn-sm"
            data-id-actividad="${id}"
            id="del-${id}"
            data-toggle="tooltip"
            data-placement="bottom"
            title="Eliminar"
            style="width:100px;display:inline;"
            onclick="EliminarHorariosTipoCuidado(this)"
        >
            <i class="fas fa-trash-alt fa-sm"></i>
        </button>`

    return acciones
}


async function getTipoCuidado() {

    tablaPlanCuidado.column(3).header().style.width = "20%"

    comboActidadesPlanDeCuidado()
    tablaPlanCuidado.draw()

    await ActualizarDatosTabla()

}

function agregarActividadTipoCuidado() {



    let id = Number(selectorPlan.options[selectorPlan.selectedIndex].value)

    if (!validarCampos("#validaSelectorTipoCuidado"))
        return

    let valor = selectorPlan.options[selectorPlan.selectedIndex].textContent
    let horarios = ""
    let acciones = dibujaAcciones(id, valor)

    agregarFilaTablaTipoCuidado({ id, valor, horarios, acciones })
    eliminaElementoSeleccionadoCombo()
}

async function agregarFilaTablaTipoCuidado(actividad) {

    let nodoNuevaFila = {}
    let ids = tablaPlanCuidado.column(0).data().toArray();

    const { id } = actividad

    let plan = await getPlanCuidado()

    let existeTipoPlan = await existeTipoCuidado(plan, Number(id)) // Comprueba id cargados de BD
    let existeIdEnTabla = existeIdEntabla(ids, Number(id)) // Comprueba id de tabla en el front

    if (existeTipoPlan || existeIdEnTabla) {
        toastr.error("La actividad ya existe")
        return
    }

    if (id !== "0")
        nodoNuevaFila = tablaPlanCuidado.row.add(actividad).draw().node()
    let idFila = `fila-${id}`
    nodoNuevaFila.setAttribute('id', idFila)

    $("#btnAgregarActividadTipoCuidado").unbind()
}

async function AgregarHorarioTipoCuidado(e) {

    let idFila = $(e).data("id")
    let idActividad = $(e).data("id-actividad")
    let nombreActividad = $(e).data("valor-actividad")
    let totalInput = $(`#tblPlancuidadosHE  tr#${idFila} td input`).length

    if (totalInput > 0)
        totalInput++

    if (validarCampos("#validaHorariosPlan", true) && totalInput < TOTAL_HORARIOS) {

        let fila = $(`#tblPlancuidadosHE tr#fila-${idActividad}`)

        fila.find("td:eq(2)").each(function () {

            let divContenedor = $('<div>', {
                id: "divInputTimeTipoCuidado",
                style: 'display:inline-block;'
            });

            let txtHorario = $('<input>', {
                type: 'time',
                class: 'form-control m-1',
                style: 'width:100px;display:inline;',
                click: function () {

                },
                'data-required': true,
                id: `horario-${idActividad}-${totalInput}`
            })

            let iEliminar = $('<i>', {
                type: 'time',
                class: 'fa fa-times mr-2 text-danger',
                'data-toggle': "tooltip",
                'data-placement': "bottom",
                'title': "Eliminar",
                'aria-hidden': true,
                style: 'cursor:pointer;display:inline;',
                click: function () {

                },
                'data-required': true,
                id: `horario-${idActividad}-${totalInput}`
            })

            iEliminar.attr('onclick', 'EliminarInputTime(this)');

            divContenedor.append(txtHorario)
            divContenedor.append(`<div style="display:inline-block;">`)
            divContenedor.append(iEliminar)

            $(this).append(divContenedor)

        })
    }

    if (totalInput === TOTAL_HORARIOS) {
        toastr.warning("El maximo son 12 horarios")
    }

    $(`#horario-${idActividad}-${totalInput}`).on("blur", async function () {

        if ($(this).val() === "")
            $(this).parent().remove()

        const json =
        {
            idActividad,
            totalInput,
            nombreActividad,
            hora: $(this).val(),
            idEstado: TipoEstado.PENDIENTE,
            Observaciones: "",
            ObservacionCierre: ""
        }

        await confirmarHorarioTipoCuidado(json, this);
    })
}

function EliminarInputTime(e) {

    $(e).parent().remove()

}

async function confirmarHorarioTipoCuidado(json, e) {

    const IdTipoCuidado = json.idActividad
    const Horario = json.hora

    if (json.hora !== "") {

        let btnHorario = $('<input>', {
            type: 'button',
            class: 'btn btn-secondary m-1',
            style: 'width:66.27px;display:inline;',
            'data-id-actividad': json.idActividad,
            'data-valor-actividad': json.nombreActividad,
            'data-required': true,
            id: `horario-${json.idActividad}-${json.totalInput}`,
            value: json.hora,
            text: json.hora
        })
        btnHorario.attr('onclick', 'mostrarModalEstadoTipoCuidado(this)');

        let fila = $(`#tblPlancuidadosHE tr#fila-${json.idActividad}`)
        let input = fila.find("td:eq(2)").find(`input#horario-${json.idActividad}-${json.totalInput}`);
        input.remove()
        fila.find("td:eq(2)").append(btnHorario)

        await GuardarHorarioTipoCuidado({ IdTipoCuidado, Horario })

        await ActualizarDatosTabla()
    }
}

async function GuardarHorarioTipoCuidado(horario) {

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/TipoCuidado`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(horario),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se agregó nuevo horario`)
        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar horario del plan de cuidado: ${JSON.stringify(jqXHR)} `)
        }
    })
}

async function EliminarHorarioTipoCuidado(e) {

    id = $(e).data("id-eliminar-hora")

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/TipoCuidado`
    }

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar la hora?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, jqXHR) {
                if (data != undefined)
                    toastr.success(`Hora eliminada correctamente`)

                ActualizarDatosTabla()
            },
            error: function (jqXHR, status) {
                console.log(`Error al eliminar el horario Tipo Cuidado: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}

async function EliminarHorariosTipoCuidado(e) {

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar la hora?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
    })

    if (result.value) {

        let id = $(e).data("id-actividad")
        let plan = await getPlanCuidado()

        await plan.map(x => {
            if (x.TipoCuidado.Id === id)
                x.Horarios.map(h => EliminaHoraTipoCuidadoPorId(h.Id))
        })

        await sleep(200)
        await ActualizarDatosTabla()
    }
}


async function EliminaHoraTipoCuidadoPorId(id) {

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${id}/TipoCuidado`
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
        },
        error: function (jqXHR, status) {
            console.log(`Error al eliminar el horario Tipo Cuidado: ${JSON.stringify(jqXHR)} `)
        }
    })

    await ActualizarDatosTabla()
}

async function getPlanCuidado() {

    const url = `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${sesion.ID_HOJA_ENFERMERIA}/TipoCuidado`

    let planCuidados = $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (tipoCuidado) {

            },
        error: function (err) {
            console.error("Error al llenar plan de cuidados")
            console.error(JSON.stringify(err))
        }
    })

    return planCuidados
}

async function dibujarTablaTipoCuidados(data) {

    let btnHorarios = ""

    data.forEach(x => {
        let row = x
        let horarios = row.Horarios.map(h => {

            btnHorarios = `
                            <button
                            class="btn btn-secondary m-1"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="${(h.TipoEstado == TipoEstado.NO_REALIZADO && h.ObservacionesCierre !== null) ? h.ObservacionesCierre ?? "" : h.Observaciones ?? ""}"
                            data-id="${h.Id}"
                            id="${h.Id}"
                            data-horario="${h.Horario}"
                            data-observaciones="${h.Observaciones ?? null}"
                            data-estado="${h.TipoEstado}"
                            data-observaciones-cierre="${h.ObservacionesCierre ?? null}"
                            style="width:66.27px;display:inline;"
                            onclick="event.preventDefault();mostrarModalEstadoTipoCuidado(this)"
                            >
                                ${moment(h.Horario, 'HH:mm').format('HH:mm')}
                           </button>
                            <div style="display:inline-block;">
                            <i class="fa fa-times mr-2"
                                data-id-eliminar-hora="${h.Id}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Eliminar Hora"
                                style="cursor:pointer;color:#dc3545;display:block;" onclick="EliminarHorarioTipoCuidado(this)">
                            </i>

                            <i class="fas fa-pencil-alt text-info"
                                data-id-editar-hora="${h.Id}"
                                data-horario="${moment(h.Horario, 'HH:mm').format('HH:mm')}"
                                aria-hidden="true"
                                data-toggle="tooltip" data-placement="bottom" title="Editar hora"
                                style="cursor:pointer;display:block;" onclick="EditarHoraTipoCuidado(this)">
                            </i>
                        </div>
                           `

            return btnHorarios
        })

        let acciones = dibujaAcciones(x.TipoCuidado.Id, x.TipoCuidado.Valor)
        tablaPlanCuidado.row.add({
            id: row.TipoCuidado.Id,
            valor: row.TipoCuidado.Valor,
            horarios: horarios.join(" "),
            acciones: acciones
        });
    })

    tablaPlanCuidado.rows().nodes().each(function (row, index) {

        let fila = tablaPlanCuidado.row(row).data();
        let idtipocuidado = fila.id;
        $(row).attr('id', 'fila-' + idtipocuidado);
    });

    // Dibujar tabla actualizada
    tablaPlanCuidado.draw();
}

async function EditarHoraTipoCuidado(e) {

    let Id = $(e).data("id-editar-hora")
    let Horario = $(e).data("horario")

    $("#txtHoraPlanCuidados").val(Horario)
    $("#hiddenIdTipoCuidadoHE").val(Id)
    $("#mdlEditarHoraPlanCuidados").modal("show")

}

async function updateHoraPlanCuidados() {

    let Id = $("#hiddenIdTipoCuidadoHE").val()
    let Horario = $("#txtHoraPlanCuidados").val()

    await ActualizarHorarioTipoCuidado({ Id, Horario })
    await sleep(200)
    await ActualizarDatosTabla()
    $("#mdlEditarHoraPlanCuidados").modal("hide")
}

function mostrarModalEstadoTipoCuidado(e) {

    let json = {}

    $("#txtObservacionesEstadoPlanCuidados").val("")

    let btnGuardar = $("#guardarEstadoPlanCuidados")
    let hora = $(e).text()
    let tituloModalHoraTipoPlan = `Hora: ${hora}`

    json = {
        Id: $(e).data("id"),
        Horario: $(e).data("horario"),
        Observaciones: $(e).data("observaciones"),
        TipoEstado: $(e).data("estado"),
        ObservacionesCierre: $(e).data("observaciones-cierre")
    }

    $("#sltEstadoHoraPlanCuidados").val(json.TipoEstado)

    let txtObservaciones = (json.TipoEstado === TipoEstado.NO_REALIZADO && json.ObservacionesCierre !== null) ? json.ObservacionesCierre ?? "" : json.Observaciones ?? ""
    $("#txtObservacionesEstadoPlanCuidados").val(txtObservaciones)

    $("#tituloItemPlanCuidado").text(tituloModalHoraTipoPlan)

    btnGuardar.attr("data-id")
    btnGuardar.data("id", json.Id)

    btnGuardar.attr("data-horario")
    btnGuardar.data("horario", json.Horario)

    btnGuardar.attr("data-observaciones")
    btnGuardar.data("observaciones", json.Observaciones)

    btnGuardar.attr("data-estado")
    btnGuardar.data("estado", json.TipoEstado)

    btnGuardar.attr("data-observaciones-cierre")
    btnGuardar.data("observaciones-cierre", json.ObservacionesCierre)

    $("#mdlEstadosItemPlanCuidados").modal("show")

}

async function guardartEstadoPlanCuidados(e) {

    let Observaciones = null
    let ObservacionesCierre = null

    let json = {}

    let idTipoEstado = Number($("#sltEstadoHoraPlanCuidados").val())

    if (idTipoEstado === TipoEstado.NO_REALIZADO) {
        $("#txtObservacionesEstadoPlanCuidados").attr("data-required", true)
        ObservacionesCierre = $("#txtObservacionesEstadoPlanCuidados").val()
    }
    else {
        $("#txtObservacionesEstadoPlanCuidados").attr("data-required", false)
        Observaciones = $("#txtObservacionesEstadoPlanCuidados").val() ?? null
    }


    if (!validarCampos("#divMdlEstadosItemPlanCuidados", true))
        return

    json = {
        Id: $(e).data("id"),
        Observaciones: Observaciones,
        TipoEstado: idTipoEstado,
        ObservacionesCierre: ObservacionesCierre
    }

    $("#txtObservacionesEstadoPlanCuidados").val("")

    await ActualizarHorarioTipoCuidado(json)
    await sleep(200)
    await ActualizarDatosTabla()
    await ActualizaColorEstado(json.TipoEstado, $(`#${json.Id}`))
    $("#mdlEstadosItemPlanCuidados").modal("hide")
}

async function ActualizarHorarioTipoCuidado(json) {

    const { Id } = json

    let parametrizacion = {
        method: "PUT",
        url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/TipoCuidado/${Id}`
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {

        },
        error: function (jqXHR, status) {
            console.log(`Error al actualizar tipo cuidado: ${JSON.stringify(jqXHR)} `)
        }
    });
    toastr.success(`Horario actualizado`)
}

async function ActualizarDatosTabla() {

    let plan = await getPlanCuidado()
    tablaPlanCuidado.clear()
    await dibujarTablaTipoCuidados(plan)

    plan.map(x => {
        x.Horarios.map(h => {
            ActualizaColorEstado(h.TipoEstado, $(`#${h.Id}`))
        })
    })
}

async function ActualizaColorEstado(idTipoEstado, element) {

    let id = Number(idTipoEstado)
    let clase = ""

    if (id === TipoEstado.PENDIENTE)
        $(element).removeClass("btn-secondary")

    if (id != 0) {

        switch (id) {
            case TipoEstado.PENDIENTE: 
                clase = "btn-secondary"
                break
            case TipoEstado.REALIZADO:
                clase = "btn-success"
                break
            case TipoEstado.NO_REALIZADO:
                clase = "btn-danger"
                break
            default:
                console.log("Error al cargar color al estado")
                break
        }
    }

    $(element).addClass(clase)
}

async function existeTipoCuidado(tipoPlanCuidado, id) {

    let tipoPlanEncontrado = await tipoPlanCuidado.find(x => x.TipoCuidado.Id === id)
    return (tipoPlanEncontrado !== undefined)
}

function existeIdEntabla(array, id) {

    let existe = array.find(x => x === id)

    return (existe !== undefined)
}

function eliminaElementoSeleccionadoCombo() {

    let value = selectorPlan.value

    if (value !== "0") {
        let opcion = selectorPlan.querySelector(`[value="${value}"]`)
        selectorPlan.removeChild(opcion)
        selectorPlan.value = "0"
    }
}

function getCantidadInputTime(tabla) {
    let celdas = tabla.column(2).nodes()
    let totalDeInputTime = 0;
    $(celdas).each(function () {
        let celda = $(this)
        let entradasDeTiempo = celda.find('input[type="time"]')
        totalDeInputTime += entradasDeTiempo.length
    })

    return totalDeInputTime
}




