﻿
function ComboServicioDeSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#sltServicioSaludIngreso, #sltServicioSalud"));
}
function ComboEstablecimiento(slt, idServicioSalud) {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/combo?idServicio=${idServicioSalud}`;
    setCargarDataEnCombo(url, false, $(slt));
}
function ComboEstablecimientoPorTipo(slt,idTipoEstablecimiento=0) {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo?idTipoEstablecimiento=${idTipoEstablecimiento}`;
    setCargarDataEnCombo(url, false, $(slt));
}
function ComboLeyPrevisional() {
    let url = `${GetWebApiUrl()}GEN_Ley_Previsional/Combo`;
    setCargarDataEnCombo(url, false, $("#sltLeyPrevisionalIngreso"));
}
function ComboUbicacion(slt, idEstablecimiento) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/${idEstablecimiento}`;
    setCargarDataEnCombo(url, false, $("#sltUbicacionIngreso"));
}
function ComboEspecialidades() {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Establecimiento/1/Combo`;
    setCargarDataEnCombo(url, false, $("#sltEspecialidades"));
}
function ComboPuebloOrigination(slt) {
    let url = `${GetWebApiUrl()}GEN_Pueblo_originario/Combo`;
    setCargarDataEnCombo(url, false, $(slt));
}
function ComboProcedencia(slt) {
    let url = `${GetWebApiUrl()}HOS_Procedencias_Ingreso/Combo`;
    setCargarDataEnCombo(url, false, $(slt));
}
function ComboTipoHospitalizacion(slt) {
    let url = `${GetWebApiUrl()}HOS_Tipo_Hospitalizacion/Combo`;
    setCargarDataEnCombo(url, false, $(slt));
}
function ComboModalidadFonasa(slt) {
    let url = `${GetWebApiUrl()}GEN_Modalidad_Fonasa/Combo`;
    setCargarDataEnCombo(url, false, $(slt));
}