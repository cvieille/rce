﻿var sSession = null;
var idPro;
var hojaEvolucion = {};
var arrayFlujoExamen = [];
var plantilla = null;

$(document).ready(async function () {
    await IniciarComponentes();
    ShowModalCargando(false)
});

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}

async function IniciarComponentes() {

    sSession = getSession();
    $("textarea").val("");
    //RevisarAcceso(true, sSession);
    //ReiniciarRequired();
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].Profesional.Id;
        }
    });

    window.addEventListener('beforeunload', bunload, false);
    CargarComboUbicacion();
    BloquearPaciente();
    //Cargar hospitalizacion
    cargarHospitalizacion();
    await getDatosHospitalizacionPorId(sSession.ID_HOSPITALIZACION, false)
    colapsarElementoDOM("#collapseInfoHospitalizacion", false)
    //CargarExamenes();
    await CargarUsuario();
    $("#txtnumerotPac").attr("disabled", "disabled")
    document.getElementById('txtEvolucion').onpaste = function (e) {
        e.preventDefault();
    }

    ValidarNumeros();

    $("#btnEditarExamen").hide();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "preventDuplicates": false,
        "onclick": null,
        "positionClass": "toast-bottom-full-width",
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 6000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

}

function CargarComboUbicacion() {

    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, "#sltUbicacion");

}

//$("#btnCargarUltimo").on('click', function () {
//    CargarDiagnosticosAnteriores();
//});

async function CargarUsuario() {

    $("#txtFechaEvolucion").val(moment(sSession.FECHA_ACTUAL).format('YYYY-MM-DD'));


    if (sSession.ID_HOJAEVOLUCION != undefined) {
        $("#btnCargarUltimo").hide();

        await CargarHojaEvolucion(sSession.ID_HOJAEVOLUCION);

    } else {
        if (sSession.ID_PACIENTE != undefined) {

            cargarPacientePorId(sSession.ID_PACIENTE)

            //if (sSession.ID_EVENTO != undefined) {
            //    hojaEvolucion.RCE_idEventos = parseInt(sSession.ID_EVENTO);
            //}

            //if (sSession.ID_PROFESIONAL != undefined)
            CargarProfesional(idPro);

        } else
            quitarListener();

    }

    if (sSession.ID_HOJAEVOLUCION != undefined)
        deleteSession('ID_HOJAEVOLUCION');

    if (sSession.ID_PACIENTE != undefined)
        deleteSession('ID_PACIENTE');

}

function CargarExamenes() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Tipo_Examen/combo`,
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (i, item) {
                arrayFlujoExamen.push({
                    RCE_idHoja_Evolucion_Tipo_Examen: null,
                    RCE_idTipo_Examen: item.RCE_idTipo_Examen,
                    RCE_descripcionTipo_Examen: item.RCE_descripcionTipo_Examen,
                    RCE_valorHoja_Evolucion_Tipo_Examen: null
                });
            });

        }
    });


}

function CargarDiagnosticos() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Tipo_Examen/combo`,
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function (i, item) {
                arrayFlujoExamen.push({
                    RCE_idHoja_Evolucion_Tipo_Examen: null,
                    RCE_idTipo_Examen: item.RCE_idTipo_Examen,
                    RCE_descripcionTipo_Examen: item.RCE_descripcionTipo_Examen,
                    RCE_valorHoja_Evolucion_Tipo_Examen: null
                });
            });

        }
    });

}

function cargarHospitalizacion() {

    idHospitalizacion = sSession.ID_HOSPITALIZACION;

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}`,
        async: false,
        success: function (data, status, jqXHR) {
            var json = data;
            if (json.DiagnosticoActual !== null)
                $("#txtDiagnosticoPrincipal").val(json.DiagnosticoActual);

            document.getElementById("sltUbicacion").value = json.Ingreso.Ubicacion.Id

            if (json.Ingreso.Cama !== undefined)
                document.getElementById("txtCamaActual").value = json.Ingreso.Cama.Valor
            else
                document.getElementById("txtCamaActual").value = "No informado"

        }, error: function (jqXHR, status) {
            console.log("ERROR al cargar la hospitalizacion: " + JSON.stringify(jqXHR));
        }
    });
}

function CargarProfesional(idProfesional) {

    try {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/${idProfesional}`,
            async: false,
            success: function (data, status, jqXHR) {

                //hojaEvolucion.GEN_idProfesional = data.GEN_idProfesional;
                $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
                    (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
                $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
                $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
                $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

            },
            error: function (jqXHR, status) {
                console.log("Error al cargar Profesional: " + jqXHR.responseText);
            }
        });

    } catch (err) {
        alert(err.message);
    }

}

function BloquearHojaEvolucion(enabled) {

    if (enabled)
        $("#divHojaEvolucion select," +
            "#divHojaEvolucion input," +
            "#divHojaEvolucion textarea," +
            "#tblExamenes tbody tr td input").attr("disabled", "disabled");
    else
        $("#divHojaEvolucion select," +
            "#divHojaEvolucion input," +
            "#divHojaEvolucion textarea," +
            "#tblExamenes tbody tr td input").removeAttr("disabled");

}

async function CargarDatosHojaEvolucion(data) {

    CargarJsonHojaEvolucionNuevo(data);

    let fechaLimite =
        moment(hojaEvolucion.FechaLimite, "YYYY-MM-DD HH:mm:ss").toDate();

    if (fechaLimite < GetFechaActual()) {
        $('#txtDetEvolucion').attr('readonly', 'readonly');

        BloquearHojaEvolucion(true);
        ShowModalAlerta("ADVERTENCIA", "Fecha límite sobrepasada", "La Hoja de Evolución ya no puede ser editada " +
            "porque se sobrepasó el timpo límite de modificación.", null, null, '', 'Cerrar');
        $('#aGuardarHojaEvolucion').hide();


    } else
        BloquearHojaEvolucion(false);

    $("#txtEvolucion").val($.trim(hojaEvolucion.Evolucion));
    $("#txtExamenFisico").val($.trim(hojaEvolucion.ExamenFisico));
    $("#txtFechaEvolucion").val(moment(hojaEvolucion.Fecha).format('YYYY-MM-DD'));
    $("#sltUbicacion").val(hojaEvolucion.IdUbicacion);
    $("#txtPlanManejo").val($.trim(hojaEvolucion.PlanManejo));
    $("#strFechaCreacion").text(moment(hojaEvolucion.FechaLimite).format('DD-MM-YYYY HH:mm:ss'));
    $("#txtDiagnosticoPrincipal").val(hojaEvolucion.Diagnostico);
    $("#txtOtrosDiagnosticos").val(hojaEvolucion.OtrosDiagnostico);
    $("#txtDescripcionHE").val($.trim(hojaEvolucion.Descripcion));
    CargarPacienteActualizado(hojaEvolucion.idPaciente);
    CargarProfesional(hojaEvolucion.idProfesional);
    cargarArchivosExistentes(hojaEvolucion.idHojaEvolucion, 'HOS');
}

async function CargarHojaEvolucion(id) {

    const url = `${GetWebApiUrl()}HOS_Hoja_Evolucion/${id}`

    try {
        const he = await $.ajax({ 
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        await CargarDatosHojaEvolucion(he)

    } catch (error) {
        console.error("Error al cargar hoja de evolucion")
        console.log(JSON.stringify(error))
    }


    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hoja_Evolucion/${sSession.ID_HOJAEVOLUCION}`,
        async: false,
        success: function (data, status, jqXHR) {



        },
        error: function (jqXHR, status) {
            console.log("Error al cargar Profesional: " + jqXHR.responseText);
        }
    });

}

function CargarJsonHojaEvolucionNuevo(json) {
    hojaEvolucion.idPaciente = json.Paciente.Id;
    hojaEvolucion.idHojaEvolucion = json.Id;
    hojaEvolucion.idProfesional = json.Profesional.Id;
    hojaEvolucion.IdUbicacion = json.Ubicacion.Id;
    hojaEvolucion.Fecha = json.Fecha;
    hojaEvolucion.PlanManejo = json.PlanManejo;
    hojaEvolucion.Evolucion = json.Evolucion;
    hojaEvolucion.ExamenFisico = json.ExamenFisico;
    //hojaEvolucion.GEN_idTipo_Estados_Sistemas = json.TipoEstadoSistema.Id;
    hojaEvolucion.FechaLimite = json.FechaLimite;
    hojaEvolucion.Diagnostico = json.DiagnosticoPrincipal;
    hojaEvolucion.OtrosDiagnostico = json.OtrosDiagnostico;
    hojaEvolucion.Descripcion = json.Descripcion;
    hojaEvolucion.IdHospitalizacion = sSession.ID_HOSPITALIZACION;
}

function CargarJsonHojaEvolucion() {

    hojaEvolucion.IdUbicacion = parseInt($("#sltUbicacion").val());
    hojaEvolucion.Fecha = $("#txtFechaEvolucion").val();
    hojaEvolucion.PlanManejo = $.trim($('#txtPlanManejo').val());
    hojaEvolucion.Evolucion = $.trim($("#txtEvolucion").val());
    hojaEvolucion.ExamenFisico = $.trim($("#txtExamenFisico").val());
    hojaEvolucion.Diagnostico = $.trim($("#txtDiagnosticoPrincipal").val());
    hojaEvolucion.OtrosDiagnostico = valCampo($("#txtOtrosDiagnosticos").val());
    hojaEvolucion.Descripcion = valCampo($("#txtDescripcionHE").val());
    hojaEvolucion.IdHospitalizacion = sSession.ID_HOSPITALIZACION;

}

function GuardarHojaEvolucion() {

    CargarJsonHojaEvolucion();

    let method = (hojaEvolucion.idHojaEvolucion !== undefined) ? 'PUT' : 'POST';

    let url = (method === 'PUT')
        ?
        `${GetWebApiUrl()}HOS_Hoja_Evolucion/${hojaEvolucion.idHojaEvolucion}`
        :
        `${GetWebApiUrl()}HOS_Hoja_Evolucion`

    const parametrizacion = {
        url,
        method
    }

    let mensaje = parametrizacion.method === 'PUT' ? 'Actualizado Correctamente' : 'Ingresado Correctamente'

    delete hojaEvolucion.idPaciente
    delete hojaEvolucion.idProfesional
    delete hojaEvolucion.FechaLimite

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(hojaEvolucion),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: mensaje,
                showConfirmButton: false,
                timer: 2000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    SalirHojaEvolucion();
                }
            });
        },
        error: function (err) {
            console.log("Error al guardar hoja de evolución: ");
            console.log(err)
            alert("Error al guardar hoja de evolución: ");
        }
    });

}

function SalirHojaEvolucion() {
    quitarListener();
    location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
}

async function GuardarGeneral() {

    if (validarCampos("#divHojaEvolucion", true)) {
        if (hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion != undefined &&
            hojaEvolucion.RCE_fecha_limite_edicionHoja_Evolucion < GetFechaActual()) {
            ShowModalAlerta("ADVERTENCIA",
                "Tiempo límite sobrepasado",
                "La Hoja de Evolución ya no puede ser editada " +
                "porque se sobrepaso el tiempo límite de modificación.", null);
        } else {
            GuardarHojaEvolucion();
        }
    }
}

function CancelarHojaEvolucion() {
    ShowModalAlerta('CONFIRMACION',
        'Salir de la hoja de evolución',
        '¿Está seguro que desea cancelar el registro?',
        quitarListener);
}

function SalirHojaEvolucion() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = document.referrer;
}
