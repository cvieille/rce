﻿
var ingresoMedicoUCI = {};

$(document).ready(function () {
    
    $("#txtApache2").blur(function () {
        GetMortalidad();
    });

});

function CargarCombosUCI() {
    CargarComboRankin();
}

function GetJsonIMUCI() {
    return ingresoMedicoUCI;
}

function GetNumeroMortalidad(apache2) {

    let mortalidad;

    if (apache2 >= 0 && apache2 <= 4)
        mortalidad = 4;
    else if (apache2 >= 5 && apache2 <= 9)
        mortalidad = 8;
    else if (apache2 >= 10 && apache2 <= 14)
        mortalidad = 15;
    else if (apache2 >= 15 && apache2 <= 19)
        mortalidad = 25;
    else if (apache2 >= 20 && apache2 <= 24)
        mortalidad = 40;
    else if (apache2 >= 25 && apache2 <= 29)
        mortalidad = 55;
    else if (apache2 >= 30 && apache2 <= 34)
        mortalidad = 75;
    else if (apache2 > 34)
        mortalidad = 85;
    else
        mortalidad = null;

    return mortalidad;
}

function AsignarJsonIMUCI() {

    ingresoMedicoUCI.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;
    
    if ($("#rdoViaAeraDificilSi").bootstrapSwitch('state'))
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = "SI";
    else if ($("#rdoViaAeraDificilNo").bootstrapSwitch('state'))
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = "NO";
    else
        ingresoMedicoUCI.RCE_via_aerea_dificilIngreso_Medico_UCI = null;
    
    ingresoMedicoUCI.RCE_apache2Ingreso_Medico_UCI = parseInt($("#txtApache2").val());
    ingresoMedicoUCI.GEN_idEscala_Rankin = parseInt(valCampo($("#sltEscalaRankin").val()));
    ingresoMedicoUCI.RCE_estadoIngreso_Medico_UCI = "Activo";

}

function CargarIMUCI(IngresoMedicoUCI) {
    if (IngresoMedicoUCI.Id !== undefined) {
        CargarCombosUCI();
        //En edicion se obtiene el id para identificar el registro a editar
        $("#idUciEditar").val(IngresoMedicoUCI.Id);
        $("#rdoViaAeraDificilSi").bootstrapSwitch('state', IngresoMedicoUCI.ViaAereaDificil);
        $("#txtApache2").val(IngresoMedicoUCI.Apache2);
        $("#sltEscalaRankin").val(IngresoMedicoUCI.IdEscalaRankin);
        GetMortalidad();

    }
}

function GetMortalidad() {
    if ($("#txtApache2").val() !== '') {
        if (GetNumeroMortalidad($("#txtApache2").val()) != null)
            $("#txtMortalidad").val(GetNumeroMortalidad($("#txtApache2").val()) + " % Mortalidad.");
        else
            $("#txtMortalidad").val("Puntaje inválido.");
    }
}

function CargarComboRankin() {

    if ($('#sltEscalaRankin').children('option').length === 0) { 
        const url = `${GetWebApiUrl()}GEN_Escala_Rankin/Combo`;
        setCargarDataEnCombo(url, false, "#sltEscalaRankin");
    }
}