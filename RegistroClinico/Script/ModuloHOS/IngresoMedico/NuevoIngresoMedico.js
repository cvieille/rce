﻿

var sSession = null;
var idPro, editandoIngresoMedico = false
var ingresoMedico = {};
var HOS_idHospitalizacion = 0;
var HOS_Hospitalizacion = {};
var arrayDiagnosticos = [];
var alertFlag = 0;
let formatFlag = 0;

var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
};

$(document).ready(async function () {

    $("textarea").val("");

    sSession = getSession();
    //ReiniciarRequired();

    //Al scrollear hacia abajo se muestran el alertas de Campos Obligatorios y/o formato
    //$(window).scroll(function () {
    //    if (alertFlag > 0) {
    //        ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
    //    } else {
    //        $('#alertObligatorios').stop().hide('fast')
    //    }
    //    if (formatFlag > 0) {
    //        ($($(this)).scrollTop() > 150) ? $('#alertFormatos').stop().show('fast') : $('#alertFormatos').stop().hide('fast');
    //    } else {
    //        $('#alertFormatos').stop().hide('fast')
    //    }
    //});

    if (sSession.ID_HOSPITALIZACION !== undefined)
        HOS_idHospitalizacion = parseInt(sSession.ID_HOSPITALIZACION);

    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#checkViaAerea").bootstrapSwitch()
    $("#checkConvulsiones").bootstrapSwitch()
    $("#checkSbor").bootstrapSwitch()
    $("#checkNeumonias").bootstrapSwitch()
    $("#checkGastrointest").bootstrapSwitch()
    $("#checkUrinario").bootstrapSwitch()
    ActivarAcompañante()
    $("#sltTipoRepresentante").attr("data-required", true)

    if (HOS_idHospitalizacion != 0) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${HOS_idHospitalizacion}`,
            async: false,
            success: function (data, status, jqXHR) {
                HOS_Hospitalizacion = data;

                //if (data.Paciente !== undefined) {
                //    console.info(data.Paciente.IdPaciente)
                //    CargarPacienteActualizado(data.Paciente.IdPaciente)
                //} else {
                //    console.error("La hospitalizacion no trajo ningun paciente")
                //}
            },
            error: function (jqXHR, status) {
                console.log("Error al consultar hospitalización: " + jqXHR.responseText);
            }
        });
    }

    //textboxio.replaceAll('#txtExamenFisicoSegmentado',
    //    {
    //        paste: { style: 'clean' },
    //        images: { allowLocal: false }
    //    });

    $(".divTipoIngresoMedico, #tblDiagnosticos, #liTablist").hide();

    $.typeahead({
        input: '#txtDiagnosticoCIE10',
        minLength: 1,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnóstico especificado ({{query}})",
        group: {
            key: "GEN_descripcion_padreDiagnostico_CIE10",
            template: function (item) {
                return item.GEN_descripcion_padreDiagnostico_CIE10;
            }
        },
        display: ["GEN_descripcionDiagnostico_CIE10"],
        template:
            '<span>' +
            '<span class="GEN_descripcionDiagnostico_CIE10">{{GEN_descripcionDiagnostico_CIE10}}</span>' +
            '</span>',
        correlativeTemplate: true,
        source: {
            GEN_descripcionDiagnostico_CIE10: {
                ajax: {
                    type: "GET",
                    url: `${GetWebApiUrl()}GEN_diagnostico_CIE10/Combo`,
                    beforeSend: function (jqXHR, options) {
                        jqXHR.setRequestHeader('Authorization', GetToken());
                    }
                }
            }
        },
        callback: {
            onClick: function (node, a, item, event) {

                if (!ExisteDiagnosticoSeleccionado(item.GEN_idDiagnostico_CIE10)) {
                    item.RCE_idIngreso_Medico_Diagnostico_CIE10 = 0;
                    AgregarFila(item);
                } else {
                    toastr.info("Este elemento ya ha sido agregado al listado.")
                }

            }
        }
    });

    $("#divDescripcionEcoAnormal").hide();
    $("#rdoEcoNormal").change(function () {
        $("#divDescripcionEcoAnormal").hide();
        $("#txtDescripcionEcoAnormal").attr("data-required", "false");
    });
    $("#rdoEcoAnormal").change(function () {
        $("#divDescripcionEcoAnormal").show();
        $("#txtDescripcionEcoAnormal").attr("data-required", "true");
    });

    $('#txtDiagnosticoCIE10').focus(function () { $(this).val(''); });

    RevisarAcceso('');
    window.addEventListener('beforeunload', bunload, false);

    $("input.bootstrapSwitch").bootstrapSwitch();
    $("#divMedicos, #divCirugias, #divAlergias, #divOh, #divTabaco, #divActividadFisica, #divDrogas, " +
        "#divHipertension, #divDiabetes, #divAsma").hide();

    $("input.bootstrapSwitch").on('switchChange.bootstrapSwitch', async function (event, state) {
        if ($(this).attr("data-descripcion") !== undefined) {
            (state) ? $("#" + $(this).attr("data-descripcion")).show() : $("#" + $(this).attr("data-descripcion")).hide();
            $("#" + $(this).attr("data-descripcion") + " input[type='text']").attr("data-required", state);
        }
    });

    $('body').on('click', '#btnNuevoIngresoExistente', async function (e) {
        await CargarUsuario();
        $('label[for]').addClass('active');
        $('#mdlIngresoExistente').modal('hide');
        e.preventDefault();
    });

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].Profesional.Id;
        }
    });

    CargarComboUbicacion();
    CargarComboEstablecimientos();
    CargarComboTipoIngresoMedico();

    $('#sltTipoIngresoMedico').change(function () {
        CambiarTipoIngresoMedico($(this));
    });

    $('#mdlIngresoExistente').on('shown.bs.modal', function () {
        $('.hhcc').attr('style', 'width:1px !important;');
    });

    //Esto tambien carga el ingreso medico
    if (!obtenerIngreso())
        CargarUsuario();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3500,
        "extendedTimeOut": 2500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    await dibujarAntecenteAlternativasMorbidos()
    await dibujarAntecendenteAlternativasHabitos()

    ShowModalCargando(false);
});

async function dibujarAntecenteAlternativasMorbidos() {

    try {
        const antecedentes = await $.ajax({
            method: 'GET',
            url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo?idClasificacion=1&idModulo=5`
        });

        $("#antecedentesMorbidosIM").empty()

        const contenidoPromises = antecedentes.map(async (antecedente) => {
            try {
                const alternativas = await $.ajax({
                    method: 'GET',
                    url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/${antecedente.Id}/alternativas`
                })

                const contenidoAlternativas = alternativas.map((alternativa) => `
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" value="${antecedente.Id}" id="checkAntecedenteMorbidoIM-${antecedente.Id}-${alternativa.Valor}" name="checkAntecedenteMorbidoIM-${antecedente.Id}" data-required="true" class="custom-control-input">
            <label class="custom-control-label" for="checkAntecedenteMorbidoIM-${antecedente.Id}-${alternativa.Valor}">${alternativa.Valor}</label>
          </div>
        `).join('')

                return `
          <div class="col-md-3 col-sm-4">
            <div class="card">
                <div class="card-header bg-info text-white text-center">
                    ${antecedente.Valor}
                </div>
                <div class="card-body text-center">
                    ${contenidoAlternativas}
                </div>
            </div>
          </div>
        `
            } catch (error) {
                console.error('Ha ocurrido un error al intentar obtener las alternativas de Morbidos')
                console.log(error)
                return ''
            }
        })

        const contenido = await Promise.all(contenidoPromises)

        $("#antecedentesMorbidosIM").append(contenido.join(''))
    } catch (error) {
        console.error('Ha ocurrido un error al obtener los antecedentes')
        console.log(error)
    }
}

async function dibujarAntecendenteAlternativasHabitos() {

    try {
        const antecedentes = await $.ajax({
            method: 'GET',
            url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo?idClasificacion=2&idModulo=5`
        });

        $("#habitosIngresoIM").empty()

        const contenidoPromises = antecedentes.map(async (antecedente) => {
            try {
                const alternativas = await $.ajax({
                    method: 'GET',
                    url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/${antecedente.Id}/alternativas`
                })

                const contenidoAlternativas = alternativas.map((alternativa) => `
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" value="${antecedente.Id}" id="checkAntecedenteHabitoIM-${antecedente.Id}-${alternativa.Valor}" name="checkAntecedenteHabitoIM-${antecedente.Id}" data-required="true" class="custom-control-input">
            <label class="custom-control-label" for="checkAntecedenteHabitoIM-${antecedente.Id}-${alternativa.Valor}">${alternativa.Valor}</label>
          </div>
        `).join('')

                return `
          <div class="col-md-3 col-sm-4">
            <div class="card">
                <div class="card-header bg-info text-white text-center">
                    ${antecedente.Valor}
                </div>
                <div class="card-body text-center">
                    ${contenidoAlternativas}
                </div>
            </div>
          </div>
        `
            } catch (error) {
                console.error('Ha ocurrido un error al intentar obtener las alternativas de habitos')
                console.log(error)
                return ''
            }
        })

        const contenido = await Promise.all(contenidoPromises)

        $("#habitosIngresoIM").append(contenido.join(''))
    } catch (error) {
        console.error('Ha ocurrido un error al obtener los antecedentes')
        console.log(error)
    }
}

//async function dibujarAntecendenteAlternativasHabitos() {

//    $.ajax({
//        method: "GET",
//        url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo?idClasificacion=2`,
//        //url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/Combo?idClasificacion=2&idModulo=5` A LA ESPERA DEL INSERT INGRESO MEDICO
//        success: function (data) {
//            $("#habitosIngresoIM").empty()
//            let contenido = ``
//            data.map(habito => {
//                let contenidoAlternativas = ``
//                $.ajax({
//                    method: "GET",
//                    url: `${GetWebApiUrl()}GEN_Tipo_Antecedente/${habito.Id}/alternativas`,
//                    async: false,
//                    success: function (data) {
//                        //data.map(alternativa => {
//                        //    console.log(alternativa)
//                        //    contenidoAlternativas += `
//                        //    <div class="custom-control custom-radio custom-control-inline">
//                        //    <input type="radio" id="checkAntecedente-${antecedente.Id}-${alternativa.Valor}" name="checkAntecedente-${antecedente.Id}" data-required=true class="custom-control-input">
//                        //    <label class="custom-control-label" for="checkAntecedente-${antecedente.Id}-${alternativa.Valor}">${alternativa.Valor}</label>
//                        //    </div>
//                        //    `
//                        //})
//                    }, error: function (error) {
//                        console.error("ha ocurridoun error al intetar obtener las alternativas del antecedente")
//                        console.log(error)
//                    }
//                })


//                contenido += `
//                <div class="col-md-3 col-sm-4">
//                <div class="col-sm-12 card">
//                <label>${habito.Valor}</label></br>
//                    <div class="custom-control custom-radio custom-control-inline">
//                      <input type="radio" id="checkHabito-${habito.Id}-si" name="checkHabito-${habito.Id}" data-required="true" class="custom-control-input">
//                      <label class="custom-control-label" for="checkHabito-${habito.Id}-si">Si</label>
//                    </div>
//                    <div class="custom-control custom-radio custom-control-inline">
//                      <input type="radio" id="checkHabito-${habito.Id}-no" name="checkHabito-${habito.Id}" data-required="true" class="custom-control-input">
//                      <label class="custom-control-label" for="checkHabito-${habito.Id}-no">No</label>
//                    </div>
//                </div>
//                </div>
//                `
//            })
//            $("#habitosIngresoIM").append(contenido)
//        }, error: function (error) {
//            console.error("Ha ocurido un error al obtener los antecedentes")
//        }
//    })
//}

//Alterna botones y campos dependiendo del tipo del ingreso medico
async function CambiarTipoIngresoMedico(sender) {

    if ($(sender).val() != 0) {

        $("#liTablist").show();
        $("#divExamenFisicoPediatria, #divPersonalesPediatria").hide();
        $("#liTipoIngresoMedico").remove();
        $("#liAntecedentesClinicos a").trigger("click");

        // Quitar cualquier estilo en el textarea
        $("textarea").removeAttr("style");

        // Asignar directamente el contenido al textarea sin usar textboxio
        $("#txtExamenFisicoSegmentado").val(
            `Cabeza: \n
        Cuello: \n
        Tórax: \n
        Abdomen: \n
        Extremidades: `
        );

        if ($(`#div${$("#sltTipoIngresoMedico option:selected").text()}`).length > 0) {

            $('#ulTablist').append($(
                `<li id='liTipoIngresoMedico' class='nav-item'>
                    <a id="profile-tab-just" class='nav-link' data-toggle="pill" href= "#div${$("#sltTipoIngresoMedico option:selected").text()}">
                        ${$("#sltTipoIngresoMedico option:selected").text()}
                    </a>
                 </li>`));
        }

        switch (parseInt($(sender).val())) {

            case 1: //GENERAL
                $('#btn-siguiente-IM').hide();
                break;
            case 2: // GINECOLOGÍA
                CargarCombosIMGinecologia();
                $('#btn-siguiente-IM').show();
                break;
            case 3: // OBSTETRICIA
                CargarCombosIMObstetricia();
                $('#btn-siguiente-IM').show();
                break;
            case 4: // PEDIATRÍA

                $("#chkAcompañante").bootstrapSwitch('state', true)

                $("#divExamenFisicoPediatria, #divPersonalesPediatria").show();

                // Asignar directamente el contenido al textarea sin usar textboxio
                $("#txtExamenFisicoSegmentado").val(
                    `General: \n
                    Piel: \n
                    Cabeza/boca/cuello: \n
                    Faringe: \n
                    Otoscopia: \n
                    Cardiopulmonar: \n
                    Abdomen: \n
                    Genitales: \n
                    Extremidades: \n
                    Neurológico: `
                );
                await CargarPediatria();
                $('#btn-siguiente-IM').show();
                break;
            case 6: // UCI
                CargarCombosUCI();
                $('#btn-siguiente-IM').show();
                //$("a[href='#divUCI']").trigger("click");
                //$("#aSiguienteUCI").show();
                /*$("#aSiguienteUCI").click(function () {
                    $('a[href="#divAntecedentesClinicos"]').trigger('click');
                    $(this).hide();
                });*/

                break;
            case 5: //Neonatal
                $('#btn-siguiente-IM').hide();
                break;
        }

        $("#txtExamenFisicoSegmentado, #txtExamenFisicoObstetricia, #txtEcografiaObstetrica").hide();

    } else
        $("#liTablist").hide();

}

function resetTab() {

    var tabs = $("#ulTablist li:not(:first)");
    var len = 1;

    $(tabs).each(function (k, v) {
        len++;
        $(this).find('a').html($(this).find('a').text());
    });

    tabID--;
}

function IrSiguiente() {
    $(".nav-link.active[data-toggle='pill']").parent().next().children("a").trigger('click');
}

function IrAnterior() {
    $(".nav-link.active[data-toggle='pill']").parent().prev().children("a").trigger('click');
}

function AgregarFila(json) {
    var idTr = "trDiagnosticos_" + $("#txtDiagnosticoCIE10 tbody").length;

    var tr =
        `
            <tr id='${idTr}' data-idDiagnostico='${json.GEN_idDiagnostico_CIE10}' data-idIngresoDiagnostico='${json.RCE_idIngreso_Medico_Diagnostico_CIE10}'>
                <td class='pt-1 pb-1' style='font-size:20px !important;'>${json.GEN_descripcionDiagnostico_CIE10}</td>
                <td class='text-center pt-1 pb-1'>
                    <a class='btn btn-danger' onclick="EliminarDiagnostico(${json.GEN_idDiagnostico_CIE10})" data-idIngresoDiagnostico='${json.RCE_idIngreso_Medico_Diagnostico_CIE10}'>
                        <i class='fa fa-minus-circle'></i>
                    </a>
                </td>
            </tr>
        `;

    $("#tblDiagnosticos tbody").append(tr);
    $("#tblDiagnosticos").show();

    arrayDiagnosticos.push({
        IdIngresoMedico: json.RCE_idIngreso_Medico_Diagnostico_CIE10,
        IdDiagnostico: json.GEN_idDiagnostico_CIE10,
        EstadoIngresoMedico: true
    });

    $("#" + idTr + " td a").click(function () {

        $(this).parent().parent().remove();

        if ($.trim($("#tblDiagnosticos tbody").html()) === "")
            $("#tblDiagnosticos").hide();

        $(this).unbind();

    });

}
function EliminarDiagnostico(IdDiagnosticoBorrar) {
    arrayDiagnosticos = arrayDiagnosticos.filter(item => item.IdDiagnostico !== IdDiagnosticoBorrar)
    toastr.warning("Elemento eliminado del listado")
}
function ExisteDiagnosticoSeleccionado(GEN_idDiagnostico) {

    var existeDiagnostico = false;

    $("#tblDiagnosticos tbody tr").each(function (index) {
        if ($(this).attr("data-idDiagnostico") == GEN_idDiagnostico)
            existeDiagnostico = true;
    });

    return existeDiagnostico;

}

function obtenerIngreso() {
    var bExistente = false;
    if (sSession.ID_PACIENTE != null && sSession.ID_PACIENTE != undefined) {
        var adataset = [];

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Ingreso_Medico/${sSession.ID_PACIENTE}/Paciente`,
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    bExistente = true;
                    $.each(data, function () {
                        adataset.push([
                            this.Id,
                            moment(this.FechaHora).format("YYYY-MM-DD"),
                            (this.Diagnostico == null) ? '' : this.Diagnostico,
                            `${this.Profesional.Nombre ?? ""} ${this.Profesional.ApellidoPaterno}`,
                            `${this.Ubicacion !== null ? "No informado" : this.Ubicacion.Valor}`,
                            ""
                        ]);
                    });

                }
                //Datatable con ingreso medicos anteriores del paciente
                if (bExistente) {
                    $('#tblHistorialIngresosMedicos').addClass("text-wrap").DataTable({
                        data: adataset,
                        order: [],
                        columns: [
                            { title: 'ID' },
                            { title: 'Fecha Ingreso' },
                            { title: 'Diagnóstico Principal' },
                            { title: 'Profesional' },
                            { title: 'Ubicación' },
                            { title: '' }
                        ],
                        columnDefs: [
                            {
                                targets: -1,
                                orderable: false,
                                searchable: false,
                                render: function (data, type, row, meta) {
                                    var fila = meta.row;
                                    var botones;
                                    botones = "<a id='linkAsignarIngreso' data-id='" + adataset[fila][0] + "' class='btn btn-info' href='#/' onclick='linkAsignarIngreso(this);' style='margin:-3px; padding:5px; font-size:0.9rem;'><i class='fa fa-edit'></i> Editar</a>";
                                    return botones;
                                }
                            },
                        ],
                        bDestroy: true
                    });
                    $('#mdlIngresoExistente').modal('show');
                }
            }, error: function (err) {
                console.log(err)
                console.error("Ha ocurrido un error al intentar cargar el historial de ingresos medicos")
            }
        });
    }

    return bExistente;

}

function linkAsignarIngreso(sender) {

    var idIngreso = $(sender).data("id");
    sSession.ID_INGRESO_MEDICO = idIngreso;
    setSession(idIngreso);
    CargarUsuario();
    $('label[for]').addClass('active');
    $('#mdlIngresoExistente').modal('hide');

}

async function CargarUsuario() {
    if (sSession.ID_INGRESO_MEDICO == undefined) {

        $("#txtFechaIngreso").val(moment(sSession.FECHA_ACTUAL).format("YYYY-MM-DD"));
        $("#txtHoraIngreso").val(moment(sSession.FECHA_ACTUAL).format("HH:mm"))
        $("#hFechaCreacion").hide();

        if (sSession.ID_PACIENTE != undefined) {
            CargarPacienteActualizado(sSession.ID_PACIENTE);
            BloquearPaciente();
        }

        if ($.isEmptyObject(HOS_Hospitalizacion) == false) {
            if (HOS_Hospitalizacion.Ingreso.Ubicacion != null) {
                $("#sltUbicacion").val(HOS_Hospitalizacion.Ingreso.Ubicacion.Id); //JAVIER

            }
        }

        if (idPro != undefined) {
            const profesionalIM = await buscarProfesionalPorIdIM(idPro)
            await cargarProfesionalIM(profesionalIM)
        }

    } else//Aca cargamos el ingreso medico
        await CargarIngresoMedico();

    if (sSession.ID_EVENTO != undefined)
        deleteSession('ID_EVENTO');

    if (sSession.ID_PACIENTE != undefined)
        deleteSession('ID_PACIENTE');

    if (sSession.ID_HOSPITALIZACION != undefined)
        deleteSession('ID_HOSPITALIZACION');

}

async function buscarProfesionalPorIdIM(id) {

    try {
        const profesional = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/Buscar?idProfesional=${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return profesional

    } catch (error) {
        console.error("Error al cargar profesional IM")
        console.log(JSON.stringify(error))
    }
}

async function cargarProfesionalIM(profesional) {

    const { Persona } = profesional[0]

    const rutProfesional = `${Persona.NumeroDocumento}-${Persona.Digito}`

    $("#txtNumeroDocumentoProfesional").val(rutProfesional)
    $("#txtNombreProfesional").val(Persona.Nombre)
    $("#txtApePatProfesional").val(Persona.ApellidoPaterno)
    $("#txtApeMatProfesional").val(Persona.ApellidoMaterno)
}

//function CargarProfesional() {

//    try {
//        $.ajax({
//            type: 'GET',
//            url: `${GetWebApiUrl()}GEN_Profesional/${idPro}`,
//            async: false,
//            success: function (data, status, jqXHR) {

//                //ingresoMedico.IdProfesional = data.GEN_idProfesional;
//                $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
//                    (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
//                $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
//                $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
//                $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

//            },
//            error: function (jqXHR, status) {
//                alert("Error al cargar Profesional: " + JSON.stringify(jqXHR));
//            }
//        });

//    } catch (err) {
//        alert(err.message);
//    }

//}

function CargarComboTipoIngresoMedico() {
    let url = `${GetWebApiUrl()}RCE_Tipo_Ingreso_Medico/combo`;
    setCargarDataEnCombo(url, false, "#sltTipoIngresoMedico");
}

function CargarComboUbicacion() {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, "#sltUbicacion");
}

function CargarComboEstablecimientos() {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, "#sltConsultorioOrigen");
}

function GetIngresoMedico() {
    return ingresoMedico;
}

async function CargarIngresoMedico() {

    editandoIngresoMedico = true
    ingresoMedico.Id = parseInt(sSession.ID_INGRESO_MEDICO);
    let json = GetJsonIngresoMedico();

    // Datos Paciente

    cargarPacientePorId(json.Paciente.Id);
    BloquearPaciente();

    if (json.IdPersonaAcompaniante === null)
        $("#chkAcompañante").bootstrapSwitch('state', false)
    else {
        $("#chkAcompañante").bootstrapSwitch('state', true)
        const personaAcompaniante = await buscarPersonaAcompaniante(json.IdPersonaAcompaniante)
        await cargarPersonaAcompaniante(personaAcompaniante)
        $("#sltTipoRepresentante").val(json.TipoRepresentante === null ? "0" : json.TipoRepresentante.Id)
        $("#sltTipoRepresentante").attr("disabled", false)
    }

    //RCE_idTipo_Ingreso_Medico
    //$("#sltTipoIngresoMedico").val(json.RCE_Tipo_Ingreso_Medico.RCE_idTipo_Ingreso_Medico);
    $("#sltTipoIngresoMedico").val(json.TipoIngreso.Id);
    CambiarTipoIngresoMedico($("#sltTipoIngresoMedico"));
    $("#sltTipoIngresoMedico").attr("disabled", "disabled");

    // UCI
    if (json.TipoIngreso.Id == 6) {
        if (json.IngresoMedicoUCI !== null && json.IngresoMedicoUCI !== undefined)
            CargarIMUCI(json.IngresoMedicoUCI);
        //Obstetricia
    } else if (json.TipoIngreso.Id == 3) {

        //if (json.RCE_Ingreso_Medico_Obstetricia!==null  && json.RCE_Ingreso_Medico_Obstetricia !== undefined)
        //    CargarIMObstetricia(json.RCE_Ingreso_Medico_Obstetricia[0].RCE_idIngreso_Medico_Obstetricia);

        // Ginecología
    } else if (json.TipoIngreso.Id == 2) {

        //if (json.RCE_Ingreso_Medico_Ginecologia.length > 0)
        //    CargarIMGinecologia(json.RCE_Ingreso_Medico_Ginecologia[0].RCE_idIngreso_Medico_Ginecologia);

    }// Pediatrico
    else if (json.TipoIngreso.Id == 4) {

        const ingresoMedicoPediatrico = await getIngrsoMedicoPediatrico(sSession.ID_INGRESO_MEDICO)

        if (ingresoMedicoPediatrico !== undefined) {
            console.log(ingresoMedicoPediatrico)
            await cargarIngresoMedicoPediatrico(ingresoMedicoPediatrico)
        }

        //if (json.RCE_Ingreso_Medico_Pediatrico.length > 0)
        //    CargariMPediatrico(json.RCE_Ingreso_Medico_Pediatrico[0].RCE_idIngreso_Medico_Pediatrico);

    }
    // Antecedentes Clínicos 
    // 1. MORBIDOS
    $("#txtMedicos").val(json.Medicos);
    $("#txtCirugias").val(json.Cirugias);
    $("#txtAlergias").val(json.Alergias);
    $("#txtHipertension").val(json.Hipertension);
    $("#txtDiabetes").val(json.Diabetes);
    $("#txtAsma").val(json.Asma);

    // 2. HÁBITOS
    $("#chbTabaco").bootstrapSwitch('state', (json.Tabaco !== null));
    $("#txtTabaco").val(json.Tabaco);

    $("#chbOh").bootstrapSwitch('state', (json.OH !== null));
    $("#txtOh").val(json.OH);

    $("#chbDrogas").bootstrapSwitch('state', (json.Drogas !== null));

    $("#txtDrogas").val(json.Drogas);


    // 3. EXAMEN FÍSICO GENERAL
    $("#txtPesoExamenFisico").val(json.Peso ?? "");
    $("#txtTallaExamenFisico").val(json.Talla ?? "");
    $("#txtPresionArterialExamenFisico").val(json.PresionArterial ?? "");
    $("#txtFrecuenciaCardiacaExamenFisico").val(json.FrecuenciaArterial ?? "");
    $("#txtTemperaturaExamenFisico").val(json.Temperatura ?? "");
    $("#txtFrecuenciaRespiratoriaCardiacaExamenFisico").val(json.FrecuenciaRespiratoria ?? "");
    $("#txtExamenFisico").val(json.ExamenFisico ?? "");

    // Asignar directamente el valor al textarea sin usar textboxio
    $("#txtExamenFisicoSegmentado").val((json.ExamenSegmentado === null) ? '' : json.ExamenSegmentado);
    // 4. MEDICAMENTOS
    $("#txtMedicamentos").val(json.Medicamentos ?? "");

    // 5. OTROS
    $("#txtDescripcionOtros").val(json.Otros ?? "");

    // Datos de Ingreso Médico

    $("#txtFechaIngreso").val(moment(json.FechaHora).format("YYYY-MM-DD"));
    $("#txtHoraIngreso").val(moment(json.FechaHora).format("HH:mm"));


    $("#sltUbicacion").val(json.Ubicacion.Id);
    $("#sltConsultorioOrigen").val(json.EstablecimientoOrigen === null ? 0 : json.EstablecimientoOrigen.Id);
    $("#txtMotivoConsulta").val(json.MotivoConsulta ?? "");
    $("#txtAnamnesis").val(json.Anamnesis ?? "");
    //$("#txtExamenFisico").val(json.ExamenFisico??"");Repetido

    // Hipotesis diagnóstica o Diagnóstico de Ingreso
    $("#txtDiagnostico").val(json.Diagnostico ?? "");
    $("#txtPlanManejo").val(json.PlanManejo ?? "");

    const profesionalIM = await buscarProfesionalPorIdIM(json.Profesional.Id)
    await cargarProfesionalIM(profesionalIM)

    // Datos del Profesional 
    //$("#txtNumeroDocumentoProfesional").val(json.Profesional.GEN_numero_documentoPersonasProfesional);
    //$("#txtNumeroDocumentoProfesional").val(json.Profesional.Persona.NumeroDocumento);
    //$("#txtNombreProfesional").val(json.Profesional.Persona.Nombre);
    //$("#txtApePatProfesional").val(json.Profesional.Persona.ApellidoPaterno);
    //$("#txtApeMatProfesional").val(json.Profesional.Persona.ApellidoMaterno);

    $("#hFechaCreacion").show();
    $("#strFechaCreacion").text(moment(json.FechaHoraCreacion).format("DD-MM-YYYY HH:mm:SS"));

    dibujarDiagnosticoCIE10(json.DiagnosticoCIE10)

    //CargarDiagnosticosCIE10(sSession.ID_INGRESO_MEDICO);

    deleteSession('ID_INGRESO_MEDICO');

    //solamente podra editar el medico que la creo

    var vEditable = json.Profesional.Id == idPro;

    if (!vEditable) {

        //pasar todos los que tengan X clase a inactivo
        $('.m_editable').find('input').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('textarea').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('textarea').css('background', '#eeeeee');

        $("#tblDiagnosticos tbody tr, #tblDiagnosticos thead tr").each(function () {
            $(this).find("td:eq(1)").remove();
            $(this).find("th:eq(1)").remove();
        });

        //textboxio.replace("#txtExamenFisicoSegmentado").content.documentElement().body.setAttribute("contenteditable", "false");

        $("#txtExamenFisicoSegmentado").attr("readonly", true);

        var v = $('.m_editable').find('select');
        for (var i = 0; i < v.length; i++) {
            $("button[data-id='" + v[i].getAttribute('id') + "']").addClass('disabled').attr('disabled', true);
        }
        $('.m_editable').find('.btn-info').addClass('disabled').attr('disabled', true);
        $('.m_editable').find('.bootstrap-switch').addClass('disabled').attr('disabled', true);
        $('#aGuardarIngresoMedico').hide();
    }
}

function dibujarDiagnosticoCIE10(Diagnosticos) {
    //Se debe cambiar a la version antigua para no romper el modelo
    DiagnosticosVersionAntigua = Diagnosticos.map(item => {
        AgregarFila({
            GEN_idDiagnostico_CIE10: item.DiagnosticoCIE10.Id,
            GEN_descripcionDiagnostico_CIE10: item.DiagnosticoCIE10.Valor,
            RCE_idIngreso_Medico_Diagnostico_CIE10: item.IdIngresoMedicoDiagnostico
        })
    })
    //for(let item )

}
function CargarDiagnosticosCIE10(RCE_idIngreso_Medico) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico_Diagnostico_CIE10/RCE_idIngreso_Medico/${RCE_idIngreso_Medico}`,
        headers: { 'Authorization': GetToken() },
        success: function (data) {
            console.log("Exto es examenes ", data)
            for (var i = 0; i < data.length; i++)
                AgregarFila(data[i]);

        }
    });

}

function GetJsonIngresoMedico() {



    var json = {};

    if (ingresoMedico.Id !== undefined) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Ingreso_Medico/${ingresoMedico.Id}`,
            async: false,
            success: function (data, status, jqXHR) {

                json = data;
            },
            error: function (jqXHR, status) {
                console.log("Error al cargar JSON Ingreso Médico: " + console.log(JSON.stringify(jqXHR)));
            }
        });

    } else {
        json = {
            IngresoMedico: {
                TipoIngreso: {},
                Estados: {}
            }
        }
    }


    return json;

}

function CargarJsonIngresoMedico() {

    var json = GetJsonIngresoMedico();
    //ingresoMedico.RCE_idTipo_Ingreso_Medico = valCampo(json.RCE_Tipo_Ingreso_Medico.RCE_idTipo_Ingreso_Medico);
    ingresoMedico.IdTipoIngresoMedico = valCampo(json.RCE_idTipo_Ingreso_Medico);

    // Antecedentes Clínicos
    // 1. Morbidos

    ingresoMedico.Medicos = valCampo(json.RCE_medicosIngreso_Medico);
    ingresoMedico.Cirugias = valCampo(json.RCE_cirugiasIngreso_Medico);
    ingresoMedico.Alergias = valCampo(json.RCE_alergiasIngreso_Medico);
    ingresoMedico.Hipertension = valCampo(json.RCE_hipertensionIngreso_Medico);
    ingresoMedico.Diabetes = valCampo(json.RCE_diabetesIngreso_Medico);
    ingresoMedico.Asma = valCampo(json.RCE_asmaIngreso_Medico);

    // 2. Hábitos
    ingresoMedico.Tabaco = valCampo(json.RCE_tabacoIngreso_Medico);
    ingresoMedico.OH = valCampo(json.RCE_OHIngreso_Medico);
    ingresoMedico.Drogas = valCampo(json.RCE_drogasIngreso_Medico);

    // 3. Examen físico general
    ingresoMedico.Peso = valCampo(json.RCE_pesoIngreso_Medico);
    ingresoMedico.Talla = valCampo(json.RCE_tallaIngreso_Medico);
    ingresoMedico.FrecuenciaRespiratoria = valCampo(json.RCE_frecuencia_respiratoriaIngreso_Medico);
    ingresoMedico.PresionArterial = valCampo(json.RCE_presion_arterialIngreso_Medico);
    ingresoMedico.FrecuenciaArterial = valCampo(json.RCE_frecuencia_arterialIngreso_Medico);
    ingresoMedico.Temperatura = valCampo(json.RCE_temperaturaIngreso_Medico);
    ingresoMedico.ExamenFisico = valCampo(json.RCE_examen_fisicoIngreso_Medico);
    ingresoMedico.ExamenSegmentado = valCampo(json.RCE_examen_segmentadoIngreso_medico);

    // 4. Medicamentos
    ingresoMedico.Medicamentos = valCampo(json.RCE_medicamentosIngreso_Medico);

    // 5. Otros
    ingresoMedico.Otros = valCampo(json.RCE_otrosIngreso_Medico);

    // INFORMACIÓN DE REGISTRO
    ingresoMedico.FechaHoraCreacion = valCampo(json.RCE_fecha_hora_creacionIngreso_Medico);
    ingresoMedico.Estado = valCampo(json.RCE_estadoIngreso_Medico);
    ingresoMedico.IdEstado = valCampo(json.TipoEstado.Id);

    // Datos de Ingreso Médico
    ingresoMedico.FechaHora = json.RCE_fecha_horaIngreso_Medico;
    ingresoMedico.IdUbicacion = valCampo(json.GEN_idUbicacion);
    ingresoMedico.MotivoConsulta = valCampo(json.RCE_motivo_consultaIngreso_Medico);
    ingresoMedico.Anamnesis = valCampo(json.RCE_anamnesisIngreso_Medico);
    ingresoMedico.Diagnostico = valCampo(json.RCE_diagnosticoIngreso_Medico);
    ingresoMedico.PlanManejo = valCampo(json.RCE_plan_manejoIngreso_Medico);

    //if (ingresoMedico.IdProfesional === undefined)
    //    ingresoMedico.idProfesional = json.IdProfesional;

}

function ValidarDiagnosticosCIE10() {

    if ($("#tblDiagnosticos tbody tr").length === 0) {
        $("#txtDiagnosticoCIE10").addClass('invalid-input');
        $('html, body').animate({ scrollTop: $("#txtDiagnosticoCIE10").offset().top - 40 }, 500);
        return false;
    } else {
        $("#txtDiagnosticoCIE10").removeClass('invalid-input');
    }

    return true;

}

async function validarCamposIngresoMedico() {
    //Bandera en caso de que se deba mostrar el Alert
    alertFlag = 0;
    formatFlag = 0;
    let valido = true

    ////Aca podría llamarse al modalCargando pero habría que ver la manera para que los Clicks funcionen

    if (!validarCampos("#divTipoIngresoMedico", true) | !validarCampos("#divDatosPaciente", true)) {
        //Mostrar mensaje de campos obligatorios
        alertFlag = 1;
        valido = false
        return valido
    }

    if (!validarCamposInputRadio("#antecedentesMorbidosIM", true)) {
        toastr.error("Debe ingresar los antecedentes mórbidos")
        $("#divAntecedentesMorb").collapse('show')
        return false
    }

    if (!validarCamposInputRadio("#habitosIngresoIM", true)) {
        toastr.error("Debe ingresar habitos")
        $("#divHabitos").collapse('show')
        return false
    }

    $('a[href="#divAntecedentesClinicos"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divAntecedentesClinicos", true)) {
        alertFlag = 1;
        valido = false
        return valido
    }

    $('a[href="#divDatosIngreso"]').trigger("click");
    await sleep(250);

    if (!validarCampos("#divDatosIngreso", true)) {
        alertFlag = 1;
        valido = false
        return valido
    }


    // GINECOLOGÍA
    if ($('#sltTipoIngresoMedico').val() == 2) {

        $('a[href="#divGinecología"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divGinecología", true)) {
            alertFlag = 1;
            valido = false
            return valido
        }

        // OBSTETRICIA
    } else if ($('#sltTipoIngresoMedico').val() == 3) {

        $('a[href="#divObstetricia"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divObstetricia", true)) {
            alertFlag = 1;
            valido = false
            return valido
        }


        // UCI
    } else if ($('#sltTipoIngresoMedico').val() == 6) {

        $('a[href="#divUCI"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divUCI", true)) {
            alertFlag = 1;
            valido = false
            return valido
        }

        // Pediatría
    } else if ($('#sltTipoIngresoMedico').val() == 4) {

        $('a[href="#divPediatría"]').trigger("click");
        await sleep(250);

        if (!validarCampos("#divPediatría", true) | !validarCampos("#divAcompañante", true)) {
            alertFlag = 1;
            valido = false
            return valido
        }

    }

    //Formato de presion arteria 111/11

    if ($("#txtPresionArterialExamenFisico").val() !== "") {
        if (!validarValorPresionArterial(document.getElementById("txtPresionArterialExamenFisico"))) {
            alertFlag = 1;
            valido = false
            return valido
        }
    }


    if (alertFlag > 0 || formatFlag > 0) {
        Swal.fire(
            'Error al validar la información',
            'Ha fallado la validación de uno o más elementos en el formulario, por favor revise que la información ingresada este en el formato correcto.',
            'error'
        )

        valido = false
        return valido
    }

    return valido
}

async function cargarIngresoMedicoParaGuardar() {

    ingresoMedico.IdTipoIngresoMedico = parseInt($("#sltTipoIngresoMedico").val())

    //Persona acompañante
    const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

    if (personaAcompaniante === null) {

        const nuevaPersonaAcompaniante = await formatJsonPersonaAcompaniante()

        const acompaniante = await guardarPersonaAcompaniante(nuevaPersonaAcompaniante)

        if (acompaniante !== null) {
            ingresoMedico.IdPersonaAcompaniante = acompaniante.Id
            ingresoMedico.IdTipoRepresentante = valCampo($("#sltTipoRepresentante").val())
        }
        else {
            ingresoMedico.IdPersonaAcompaniante = null
            ingresoMedico.IdTipoRepresentante = null
        }
    }
    else {
        if ($("#chkAcompañante").bootstrapSwitch('state')) {

            if (personaAcompaniante !== null) {
                if (validarCampos("#divAcompañante", true)) {
                    ingresoMedico.IdPersonaAcompaniante = personaAcompaniante?.Id ?? null
                    ingresoMedico.IdTipoRepresentante = valCampo($("#sltTipoRepresentante").val())
                }
            }
        }
        else {
            ingresoMedico.IdPersonaAcompaniante = null
            ingresoMedico.IdTipoRepresentante = null
        }
    }

    // Fin Persona acompañante

    // Antecedentes Clínicos
    // 1. Morbidos
    ingresoMedico.Medicos = valCampo($("#txtMedicos").val());
    ingresoMedico.Cirugias = valCampo($("#txtCirugias").val());
    ingresoMedico.Alergias = valCampo($("#txtAlergias").val());
    ingresoMedico.Hipertension = valCampo($("#txtHipertension").val());
    ingresoMedico.Diabetes = valCampo($("#txtDiabetes").val());
    ingresoMedico.Asma = valCampo($("#txtAsma").val());

    // 2. Hábitos
    ingresoMedico.Tabaco = $("#chbTabaco").bootstrapSwitch('state') ? valCampo($("#txtTabaco").val()) : null;
    ingresoMedico.OH = $("#chbOh").bootstrapSwitch('state') ? valCampo($("#txtOh").val()) : null;
    ingresoMedico.Drogas = $("#chbDrogas").bootstrapSwitch('state') ? valCampo($("#txtDrogas").val()) : null;

    // 3. Examen físico general
    ingresoMedico.Peso = parseInt($("#txtPesoExamenFisico").val());
    ingresoMedico.Talla = parseInt($("#txtTallaExamenFisico").val());
    ingresoMedico.PresionArterial = valCampo($("#txtPresionArterialExamenFisico").val());
    ingresoMedico.FrecuenciaArterial = parseInt($("#txtFrecuenciaCardiacaExamenFisico").val());
    ingresoMedico.Temperatura = parseInt($("#txtTemperaturaExamenFisico").val());
    ingresoMedico.ExamenFisico = valCampo($("#txtExamenFisico").val());
    ingresoMedico.ExamenSegmentado = $.trim($('#txtExamenFisicoSegmentado').val());
    ingresoMedico.FrecuenciaRespiratoria = parseInt($("#txtFrecuenciaRespiratoriaCardiacaExamenFisico").val());

    // 4. Medicamentos
    ingresoMedico.Medicamentos = valCampo($("#txtMedicamentos").val());

    // 5. Otros
    ingresoMedico.Otros = valCampo($("#txtDescripcionOtros").val());

    // Datos de Ingreso Médico
    ingresoMedico.FechaHora = `${$("#txtFechaIngreso").val()} ${$("#txtHoraIngreso").val()}:00`;
    ingresoMedico.IdUbicacion = parseInt(valCampo($("#sltUbicacion").val()));
    ingresoMedico.IdEstablecimientoOrigen = parseInt($("#sltConsultorioOrigen").val()) === 0 ? null : parseInt($("#sltConsultorioOrigen").val())
    ingresoMedico.MotivoConsulta = valCampo($("#txtMotivoConsulta").val());
    ingresoMedico.Anamnesis = valCampo($("#txtAnamnesis").val());

    // Hipotesis diagnóstica o Diagnóstico de Ingreso
    ingresoMedico.Diagnostico = valCampo($("#txtDiagnostico").val());
    ingresoMedico.PlanManejo = valCampo($("#txtPlanManejo").val());
    ingresoMedico.IdPrevisionTramo = valCampo($("#sltPrevisionTramo").val());

    // 94 = Ingreso Médico No Validado
    // 95 = Ingreso Médico Validado
    // VALIDA SI ES MÉDICO

    //Cargando los examenes para guardarlos
    if (arrayDiagnosticos.length > 0) {
        IdsDiagnosticosExtraidos = arrayDiagnosticos.map(item => item.IdDiagnostico)
        ingresoMedico.Diagnosticos = IdsDiagnosticosExtraidos
    }

    const idPaciente = await GuardarPaciente();

    ingresoMedico.IdPaciente = idPaciente
    // Datos de Registro
    if (ingresoMedico.FechaHoraCreacion === null)
        ingresoMedico.FechaHoraCreacion = moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:SS");
    switch (ingresoMedico.IdTipoIngresoMedico) {
        case 6://UCI
            ingresoMedico.IngresoMedicoUCI = {}
            document.getElementById("idUciEditar").value !== "" ? ingresoMedico.IngresoMedicoUCI.Id = parseInt(document.getElementById("idUciEditar").value) : ""
            ingresoMedico.IngresoMedicoUCI.ViaAereaDificil = $("#checkViaAerea").bootstrapSwitch('state')
            ingresoMedico.IngresoMedicoUCI.Apache2 = parseInt($("#txtApache2").val())
            ingresoMedico.IngresoMedicoUCI.IdEscalaRankin = parseInt($("#sltEscalaRankin").val())
            break
    }

    if (HOS_idHospitalizacion !== 0)
        ingresoMedico.IdHospitalizacion = HOS_idHospitalizacion;
}

async function getIngresoMedicoAsync(id) {

    const url = `${GetWebApiUrl()}RCE_Ingreso_Medico/${id}`

    try {
        const ingresoMedico = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return ingresoMedico

    } catch (error) {
        console.error("Error al cargar Ingreso Médico")
        console.log(JSON.stringify(error))
    }
}

async function GuardarIngresoMedico() {

    let valido = await validarCamposIngresoMedico()

    if (!valido)
        return

    let ingresoMedicoData = {}
    let method = (ingresoMedico.Id !== undefined) ? 'PUT' : 'POST';//RCE_Ingreso_Medico
    let url = `${GetWebApiUrl()}RCE_Ingreso_Medico`

    //Edicion de IM
    if (ingresoMedico.Id !== undefined) {
        url += `/${ingresoMedico.Id}`
        //CargarJsonIngresoMedico();
    }

    await cargarIngresoMedicoParaGuardar()


    $("#antecedentesMorbidosIM").find("input:checked").each(function (index, element) {
        console.log($(element).val())
    })

    return

    await $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ingresoMedico),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {

            const id = data !== undefined ? data.Id : ingresoMedico.Id;

            ingresoMedicoData = await getIngresoMedicoAsync(id)

            if (ingresoMedicoData !== null) {
                await GuardarDiagnosticos(ingresoMedicoData.Id)
            }
            // Pediatría
            if (ingresoMedicoData !== null && $('#sltTipoIngresoMedico').val() == 4) {

                await GuardarVacunasPaciente()
                await GuardarIngresoPediatrico(ingresoMedicoData.Id)
            }


            if (status === "nocontent") {
                await sleep(250)
                toastr.success("Ingreso medico modificado con exito")
            }
            else if (status === "success") {
                await sleep(250)
                toastr.success("Ingreso medico guardado con exito")
            }

            window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`
        },
        error: async function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico: " + JSON.stringify(jqXHR));
        }
    });

    //await GuardarIMGinecologia()

}
//Deprecated
async function GuardarDiagnosticos(RCE_idIngreso_Medico) {

    console.log(RCE_idIngreso_Medico)
    console.log(arrayDiagnosticos)

    if (RCE_idIngreso_Medico !== undefined) {

        for (var i = 0; i < arrayDiagnosticos.length; i++) {

            arrayDiagnosticos[i].RCE_idIngreso_Medico = RCE_idIngreso_Medico;

            var url = `${GetWebApiUrl()}RCE_Ingreso_Medico_Diagnostico_CIE10`;
            var method = "POST";

            if (arrayDiagnosticos[i].RCE_activoIngreso_Medico_Diagnostico_CIE10 === false) {
                url += "/" + arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10;
                method = "PUT";
            } else if (arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10 !== 0) {
                continue;
            } else
                delete arrayDiagnosticos[i].RCE_idIngreso_Medico_Diagnostico_CIE10;

            $.ajax({
                type: method,
                url: url,
                data: JSON.stringify(arrayDiagnosticos[i]),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {
                    toastr.success('Se registra Diagnóstico CIE-10.');
                },
                error: function (jqXHR, status) {
                    alert("Error al guardar Diagnostico CIE10: " + jqXHR.responseText);
                }
            });

        }

    }

}

function CancelarIngresoMedico() {
    ShowModalAlerta('CONFIRMACION',
        'Salir del Ingreso Médico',
        '¿Está seguro que desea cancelar el registro?',
        SalirIngresoMedico);
}

function SalirIngresoMedico() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = ObtenerHost() + "/Vista/ModuloHOS/BandejaHOS.aspx";
}