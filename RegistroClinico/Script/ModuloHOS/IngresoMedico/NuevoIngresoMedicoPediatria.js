﻿
let ingresoMedicoPediatria = {};
let arrayVacunasPaciente = [];

$(document).ready(async function () {

    ingresoMedicoPediatria.GEN_idPersona_Cuidador = null;
    ingresoMedicoPediatria.GEN_idPersona_parental_1 = null;
    ingresoMedicoPediatria.GEN_idPersona_parental_2 = null;

    //$("#aAgregarPaciente, #sltVacunasPaciente").attr("disabled", "disabled");
    //$("#aAgregarPaciente").addClass("disabled");

    $("#tblVacunasPaciente, #tblHospitalizaciones").hide();

    await findPacienteParental(
        "#sltIdentificacionParental1",
        "#txtnumeroDocParental1",
        "#txtDigParental1",
        "#divParental1"
    )
    await findPacienteParental(
        "#sltIdentificacionParental2",
        "#txtnumeroDocParental2",
        "#txtDigParental2",
        "#divParental2"
    )

    await buscarPersonaParental(
        "#sltIdentificacionParental1",
        "#txtnumeroDocParental1",
        "#txtDigParental1",
        "#txtnombreParental1",
        "#txtApePatParental1",
        "#txtApeMatParental1",
        "#txtFechaNacimientoParental1",
        "#txtEdadParental1",
        "#sltActividadParental1"
    )
    await buscarPersonaParental(
        "#sltIdentificacionParental2",
        "#txtnumeroDocParental2",
        "#txtDigParental2",
        "#txtnombreParental2",
        "#txtApePatParental2",
        "#txtApeMatParental2",
        "#txtFechaNacimientoParental2",
        "#txtEdadParental2",
        "#sltActividadParental2"
    )


    $("#txtFechaNacimientoParental1, #txtFechaNacimientoParental2").blur(function () {

        var fechaActual = moment(moment()).format("YYYY-MM-DD");
        var fechaNacimiento = moment(moment($(this).val()).toDate()).format("YYYY-MM-DD");
        var parent = $(this).parent().parent();

        if (moment(fechaActual).isSameOrAfter(fechaNacimiento)) {
            parent.children(".edad").val(CalcularEdad(moment($(this).val()).format("YYYY-MM-DD")).edad);
        } else {
            parent.children(".edad").val('');
            $(this).val('');
        }

    });

    $("#aAgregarPaciente").click(function () {
        if ($("#sltVacunasPaciente").val() !== '0')
            AgregarFilaVacunaPaciente("sltVacunasPaciente");
    });

    $("#txtDigitoPac").on('input', function () {
        if ($("#sltTipoIngresoMedico").val() == '4') {
            CargarTablaHospitalizaciones();
            CargarVacunasPacientes();
        }
    });

    $("#txtnumerotPac").on('blur', function () {
        if ($("#sltTipoIngresoMedico").val() == '4') {
            CargarTablaHospitalizaciones();
            CargarVacunasPacientes();
        }
    });

    $("#btnAgregarPersona").click(async function () {

        let div = $(this).data("obterner-div")

        const personaparental = await cargarPersonaParentalAsync()

        await GuardarPersonaParentalAysnc(personaparental)
        await findLoadParentalPediatrico(div, "#sltIdentificacionPersona", "#txtnumeroDocPersona", false)

        $("#mdlPersona").modal("hide")
    })

    $("#btnCancelarModalCuidadoEnfermeria").click(function () {
        $("#mdlPersona").modal("hide")
    })
});

async function buscarPersonaParental(indentificacion, numeroDocumeto, digito, nombre, apePaterno, apeMaterno, fechaNacimiento, edad, actividad, escolaridad) {

    $(numeroDocumeto).on('blur', async function () {

        if ($(indentificacion).val() === '2' || $(indentificacion).val() === '3') {
            if ($.trim($(numeroDocumeto).val()) !== '') {

                await ReiniciarPersona()
                const personaParental = await buscarPersonaAcompaniante(null, $(indentificacion).val(), $(numeroDocumeto).val())

                if (personaParental !== null) {
                    await cargarPersonaParentalPediatrico(
                        personaParental,
                        indentificacion,
                        numeroDocumeto,
                        digito,
                        nombre,
                        apePaterno,
                        apeMaterno,
                        fechaNacimiento,
                        edad,
                        actividad,
                        escolaridad
                    )
                }
            }
            else {
                await ReiniciarPersona();
                await DeshabilitarPersona("divAcompañante", true)
            }
        }
        else {
            await DeshabilitarPersona("divAcompañante", true)
        }
    })
}

async function cargarPersonaParentalPediatrico(persona,
    sltIdentificacionParental,
    txtnumeroDocParental,
    txtDigParental,
    txtnombreParental,
    txtApePatParental,
    txtApeMatParental,
    txtFechaNacimientoParental,
    txtEdadParental,
    sltActividadParental,
    sltEducacionParental
) {

    if (persona !== null) {

        let idIdentificacion = persona.Identificacion !== undefined ? persona.Identificacion.Id : "0";
        $(sltIdentificacionParental).val(idIdentificacion);
        $(txtnumeroDocParental).val(persona.NumeroDocumento ?? "");
        //$(txtDigParental).val(persona.Digito ?? "");
        $(txtnombreParental).val(persona.Nombre ?? "");
        $(txtApePatParental).val(persona.ApellidoPaterno ?? "");
        $(txtApeMatParental).val(persona.ApellidoMaterno ?? "");
        $(txtFechaNacimientoParental).val(moment(persona.FechaNacimiento).format('YYYY-MM-DD') ?? "");
        $(txtEdadParental).val(persona.Edad?.edad ?? "");
        $(txtEdadParental).attr("disabled", true);

        if (persona.TipoCategoriaOcupacion !== null)
            $(sltActividadParental).val(persona.TipoCategoriaOcupacion !== undefined ? persona.TipoCategoriaOcupacion.Id : "0")
        if (persona.TipoEscolaridad !== null)
            $(sltEducacionParental).val(persona.TipoEscolaridad !== undefined ? persona.TipoEscolaridad.Id : "0")
    }
}

async function findPacienteParental(identificacion, numDocumento, digito, divValidar) {

    $(digito).on('input', async function () {

        if ($(this).val() === "") {
            $(`${divValidar} input`).not(`${numDocumento}, ${digito}`).val("")
        }

        let esRutCorrecto = await EsValidoDigitoVerificador($(numDocumento).val(), $(digito).val());

        if (!esRutCorrecto)
            $(`${divValidar} input, ${divValidar} select`).not(`${identificacion}, ${numDocumento}, ${digito}`).attr("disabled", true)
        else
            $(`${divValidar} input, ${divValidar} select`).attr("disabled", false)

        if (!validarCampos(divValidar, true))
            return

        await findLoadParentalPediatrico(divValidar, identificacion, numDocumento, true)

    })
}

async function findLoadParentalPediatrico(divValidar, identificacion, numDocumento, isModalVisible) {

    const buscarPacienteParental = await buscarPersonaAcompaniante(null, $(identificacion).val(), $(numDocumento).val());

    const sltIdentificacionParental = (divValidar === "#divParental1") ? "#sltIdentificacionParental1" : "#sltIdentificacionParental2"
    const txtnumeroDocParental = (divValidar === "#divParental1") ? "#txtnumeroDocParental1" : "#txtnumeroDocParental2"
    const txtDigParental = (divValidar === "#divParental1") ? "#txtDigParental1" : "#txtDigParental2"
    const txtnombreParental = (divValidar === "#divParental1") ? "#txtnombreParental1" : "#txtnombreParental2"
    const txtApePatParental = (divValidar === "#divParental1") ? "#txtApePatParental1" : "#txtApePatParental2"
    const txtApeMatParental = (divValidar === "#divParental1") ? "#txtApeMatParental1" : "#txtApeMatParental2"
    const txtFechaNacimientoParental = (divValidar === "#divParental1") ? "#txtFechaNacimientoParental1" : "#txtFechaNacimientoParental2"
    const txtEdadParental = (divValidar === "#divParental1") ? "#txtEdadParental1" : "#txtEdadParental2"
    const sltActividadParental = (divValidar === "#divParental1") ? "#sltActividadParental1" : "#sltActividadParental2"
    const sltEducacionParental = (divValidar === "#divParental1") ? "#sltEducaciónParental1" : "#sltEducaciónParental2"

    if (buscarPacienteParental === null) {
        await cargarDatosInicialesPersonaAsync(
            sltIdentificacionParental,
            txtnumeroDocParental,
            txtDigParental,
        )
        if (isModalVisible)
            await cargarModalPersonaPariente(divValidar)

    } else {
        await cargarPersonaParentalPediatrico(
            buscarPacienteParental,
            sltIdentificacionParental,
            txtnumeroDocParental,
            txtDigParental,
            txtnombreParental,
            txtApePatParental,
            txtApeMatParental,
            txtFechaNacimientoParental,
            txtEdadParental,
            sltActividadParental,
            sltEducacionParental
        )
    }
}

async function cargarModalPersonaPariente(div) {

    const result = await Swal.fire({
        title: `No se encontró la persona en nuestros registros`,
        text: "¿Desea ingresar una nueva persona?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        await cargarInfoInicialPersona()

        $("#btnAgregarPersona").attr("data-obterner-div", div)
        $("#btnAgregarPersona").data("obterner-div", div)

        $("#mdlPersona").modal("show")
    }
}

async function CargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documentoPersonas, tipoPersona) {

    let json = GetJsonCargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documentoPersonas);

    if (json !== null) {

        if (tipoPersona === "Cuidador") {
            $("#divResponsable [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_Cuidador = json.GEN_idPersonas;
            $("#txtnombreCuidador").val(json.GEN_nombrePersonas);
            $("#txtApePatCuidador").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatCuidador").val(json.GEN_apellido_maternoPersonas);
            $("#txtTelefonoCuidador").val(json.GEN_telefonoPersonas);
            $("#txtDireccionCuidador").val(json.GEN_dir_callePersonas);
            if (GEN_idPersona !== null) {
                DeshabilitarPersona("divResponsable", false);
                $("#sltIdentificacionCuidador").val(json.GEN_idIdentificacion);
                $("#txtnumeroDocCuidador").val(json.GEN_numero_documentoPersonas);
                if (json.GEN_idIdentificacion === 1 || json.ID === 4)
                    $("#txtDigCuidador").val(json.GEN_digitoPersonas);
            }
        } else if (tipoPersona === "Parental1") {
            $("#divParental1 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_1 = json.GEN_idPersonas;
            $("#txtnombreParental1").val(json.GEN_nombrePersonas);
            $("#txtApePatParental1").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatParental1").val(json.GEN_apellido_maternoPersonas);
            $("#txtFechaNacimientoParental1").val(moment(json.GEN_fecha_nacimientoPersonas).format('YYYY-MM-DD'));
            $("#txtEdadParental1").val(CalcularEdad(moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD")).edad);
            ValTipoCampo($("#sltActividadParental1"), json.GEN_idCategorias_Ocupacion);
            ValTipoCampo($("#sltEducaciónParental1"), json.GEN_idTipo_Escolaridad);
            //if (GEN_idPersona !== null) {
            //    DeshabilitarPersona("divParental1", false);
            //    $("#sltIdentificacionParental1").val(json.GEN_idIdentificacion);
            //    $("#txtnumeroDocParental1").val(json.GEN_numero_documentoPersonas);
            //    if (json.GEN_idIdentificacion === 1 || json.GEN_idIdentificacion === 4)
            //        $("#txtDigParental1").val(json.GEN_digitoPersonas);
            //}
        } else if (tipoPersona === "Parental2") {
            $("#divParental2 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_2 = json.GEN_idPersonas;
            $("#txtnombreParental2").val(json.GEN_nombrePersonas);
            $("#txtApePatParental2").val(json.GEN_apellido_paternoPersonas);
            $("#txtApeMatParental2").val(json.GEN_apellido_maternoPersonas);
            $("#txtFechaNacimientoParental2").val(moment(json.GEN_fecha_nacimientoPersonas).format('YYYY-MM-DD'));
            $("#txtEdadParental2").val(CalcularEdad(moment($("#txtFechaNacimientoParental2").val()).format("YYYY-MM-DD")).edad);
            ValTipoCampo($("#sltActividadParental2"), json.GEN_idCategorias_Ocupacion);
            ValTipoCampo($("#sltEducaciónParental2"), json.GEN_idTipo_Escolaridad);
            if (GEN_idPersona !== null) {
                DeshabilitarPersona("divParental2", false);
                $("#sltIdentificacionParental2").val(json.GEN_idIdentificacion);
                $("#txtnumeroDocParental2").val(json.GEN_numero_documentoPersonas);
                if (json.GEN_idIdentificacion === 1 || json.GEN_idIdentificacion === 4)
                    $("#txtDigParental2").val(json.GEN_digitoPersonas);
            }
        }

    } else {

        if (tipoPersona === "Cuidador") {
            $("#divResponsable [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_Cuidador = 0;
        } else if (tipoPersona === "Parental1") {
            $("#divParental1 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_1 = 0;
        } else if (tipoPersona === "Parental2") {
            $("#divParental2 [data-required='false']").attr("data-required", "true");
            ingresoMedicoPediatria.GEN_idPersona_parental_2 = 0;
        }

    }
}

async function CargarPediatria() {
    CargarComboEstablecimientos();
    CargarComboTipoRecienNacido();
    CargarComboTipoParto();
    CargarComboTipoVacuna();
    CargarComboTipoEdad();
    CargarComboCategoriaOcupacion();
    CargarComboTipoEscolaridad();
    CargarComboEstadoConyugal();
    CargarComboTipoDesarrolloPsicoMotor();
    CargarComboTipoDatosNutricionales();

    await cargarComboIdentificacionParental("#sltIdentificacionParental1")

    await onInputNumeroDocumento(
        "#sltIdentificacionParental1",
        "#txtnumeroDocParental1",
        "#txtDigParental1",
        "#txtDigParental1",
        "#divParental1"
    )

    await ocultarMostrarFichaPacParental(
        "#sltIdentificacionParental1",
        "txtnumeroDocParental1",
        "#divParental1"
    )

    await cargarComboIdentificacionParental("#sltIdentificacionParental2")
    await onInputNumeroDocumento(
        "#sltIdentificacionParental2",
        "#txtnumeroDocParental2",
        "#txtDigParental2",
        "txtDigParental2",
        "#divParental2"
    )

    await ocultarMostrarFichaPacParental(
        "#sltIdentificacionParental2",
        "txtnumeroDocParental2",
        "#divParental2"
    )

    await CargarVacunasPacientes();

    $("#sltIdentificacionParental1").val("1")
    $("#sltIdentificacionParental2").val("1")

}

async function onInputNumeroDocumento(identificacion, numDocumento, digito, lblInfoRut, divValidar) {

    let textInfoRut = "RUT"

    $(numDocumento).on("input", async function () {
        let maxlength = parseInt($(this).attr('maxlength'))

        if (maxlength === 15) {
            if ($(this).val() === "") {
                $(`${divValidar} input, ${divValidar} select`).not(`${identificacion}, ${numDocumento}, ${digito}`).attr("disabled", true)
                textInfoRut = "RUT"
            }
            else {
                $(`${divValidar} input, ${divValidar} select`).attr("disabled", false)
                textInfoRut = "Ficha Clínica"
            }

            $(`label[for="${lblInfoRut}"]`).text(textInfoRut)
        }
        else {
            textInfoRut = "RUT"
        }
    })
}

async function ocultarMostrarFichaPacParental(identificador, rut, div) {

    $(identificador).on('change', function () {

        const valor = $(this).val();
        const digito = $(`${div} .digito`);
        const maxLength = (valor === '2' || valor === '3' || valor === '5') ? 15 : 8;
        const placeholder = (valor === '2' || valor === '3' || valor === '5') ? "Ficha Clínica" : "";

        digito.toggle(!(valor === '2' || valor === '3' || valor === '5'));
        $(`${div} input[id="${rut}"]`).attr("maxlength", maxLength);
        $(`${div} input[id="${rut}"]`).attr("placeholder", placeholder);
    });
}

//function CargarIdentificacionPersonas() {

//    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").empty();
//    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").append($("#sltIdentificacion").html());
//    $("#sltIdentificacionCuidador, #sltIdentificacionParental1, #sltIdentificacionParental2").val("1");
//    $(`label[for='sltIdentificacionCuidador']`).text($("#sltIdentificacionCuidador").children("option:selected").text());
//    $(`label[for='sltIdentificacionParental1']`).text($("#sltIdentificacionParental1").children("option:selected").text());
//    $(`label[for='sltIdentificacionParental2']`).text($("#sltIdentificacionParental2").children("option:selected").text());
//    $("#sltIdentificacionCuidador option[value='5'], #sltIdentificacionParental1 option[value='5'], #sltIdentificacionParental2 option[value='5']").remove();

//}

function DeshabilitarPersona(divPadre, esDisabled) {
    (!esDisabled) ? $(`#${divPadre} .datos-persona`).removeAttr("disabled") : $(`#${divPadre} .datos-persona`).attr("disabled", "disabled");
}

function ReiniciarPersona(divPadre) {
    $(`#${divPadre} input.datos-persona`).val("");
    $(`#${divPadre} select.datos-persona`).val("0");
    $(`#${divPadre} [data-required='true']`).attr("data-required", "false");
}

function GetJsonCargarPersona(GEN_idPersona, GEN_idIdentificacion, GEN_numero_documento) {

    var url = (GEN_idPersona === null) ? `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${GEN_idIdentificacion}&numeroDocumento=${GEN_numero_documento}` :
        `${GetWebApiUrl()}GEN_Personas/${GEN_idPersona}`;
    var d = null;

    // SI ES 0 ES PORQUE SE CREARÁ UNA PERSONA NUEVA
    if (GEN_idPersona !== 0) {
        $.ajax({
            type: "GET",
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                if ($.isArray(data)) {
                    if (data.length > 0)
                        d = data[0];
                } else {
                    d = data;
                }
            },
            error: function (request, status) {
                console.log("Error: " + JSON.stringify(request));
            }
        });

    } else {
        d = {
            GEN_numero_documentoPersonas: null,
            GEN_digitoPersonas: null,
            GEN_nombrePersonas: null,
            GEN_apellido_paternoPersonas: null,
            GEN_apellido_maternoPersonas: null,
            GEN_telefonoPersonas: null,
            GEN_emailPersonas: null,
            GEN_idIdentificacion: null,
            GEN_idSexo: null,
            GEN_estadoPersonas: "Activo",
            GEN_dir_callePersonas: null,
            GEN_idCategorias_Ocupacion: null,
            GEN_idTipo_Escolaridad: null,
            GEN_fecha_nacimientoPersonas: null
        }
    }

    if (d !== null)
        d.GEN_fecha_actualizacionPersonas = GetFechaActual();

    return d;

}

function CargarComboTipoRecienNacido() {
    let url = `${GetWebApiUrl()}RCE_Tipo_Recien_Nacido/Combo`;
    setCargarDataEnCombo(url, true, "#sltRecienNacido");
}

function CargarComboTipoParto() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Parto/Combo`;
    setCargarDataEnCombo(url, true, "#sltParto")
}

function CargarComboTipoVacuna() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Vacuna/Combo`;
    setCargarDataEnCombo(url, true, "#sltVacunasPaciente")
}

function CargarComboTipoEdad() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Edad/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoEdadDuracionLMaterna, #sltInicioLArtificial");
}

function CargarComboCategoriaOcupacion() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Categoria_Ocupacion/Combo`;
    setCargarDataEnCombo(url, false, "#sltActividadParental1, #sltActividadParental2");
}

function CargarComboTipoEscolaridad() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`;
    setCargarDataEnCombo(url, false, "#sltEducaciónParental1, #sltEducaciónParental2");
}

function CargarComboEstadoConyugal() {
    let url = `${GetWebApiUrl()}GEN_Estado_Conyugal/Combo`;
    setCargarDataEnCombo(url, true, "#sltSituacionMarital");
}

function CargarComboTipoDesarrolloPsicoMotor() {
    let url = GetWebApiUrl() + "RCE_Tipo_Desarrollo_Psico_Motor/Combo"
    setCargarDataEnCombo(url, true, "#sltDSM")
}

function CargarComboTipoDatosNutricionales() {
    let url = GetWebApiUrl() + "GEN_Tipo_Datos_Nutricionales/Combo"
    setCargarDataEnCombo(url, true, "#sltNutricional")
}

async function cargarComboIdentificacionParental(element) {
    let url = `${GetWebApiUrl()}GEN_Identificacion/Combo`
    setCargarDataEnCombo(url, true, element)
    $(element).val("1")
}

async function getIngrsoMedicoPediatrico(id) {

    const url = `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${id}`

    try {
        const imp = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json'
        });

        return imp

    } catch (err) {
        console.error('Error al llenar ingreso médico pediátrico')
        console.error(JSON.stringify(err))
    }

}

async function CargariMPediatrico(RCE_idIngreso_Medico_Pediatrico) {

    

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${RCE_idIngreso_Medico_Pediatrico}`,
        async: false,
        success: async function (data, status, jqXHR) {

            let json = data;

            ingresoMedicoPediatria.RCE_idIngreso_Medico_Pediatrico = RCE_idIngreso_Medico_Pediatrico;

            // INFORMACIÓN CUIDADOR
            await CargarPersona(json.GEN_idPersona_Cuidador, null, null, "Cuidador");
            $("#txtparentescoCuidador").val(json.RCE_parentesco_cuidadorIngreso_Medico_Pediatrico);

            // ANTECEDENTES PERSONALES
            //$("#sltConsultorioOrigen").val(json.GEN_idEstablecimiento_Origen);
            $("#sltRecienNacido").val(json.RCE_idTipo_Recien_Nacido);
            $("#sltParto").val(json.GEN_idTipo_Parto);
            $("#txtPatologiaPerinatal").val(json.RCE_patologia_perinatalIngreso_Medico_Pediatrico);
            $("#txtOtrosPediatria").val(json.RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico);
            $("#txtDuracionLMaterna").val(json.RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico);
            $("#sltTipoEdadDuracionLMaterna").val(json.GEN_idTipo_Edad_duracion_lactancia_añosIngreso_Medico_Pediatrico);
            $("#txtInicioLArtificial").val(json.RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico);
            $("#sltInicioLArtificial").val(json.GEN_idTipo_Edad_inicio_lactancia_añosIngreso_Medico_Pediatrico);
            $("#txt1Comida").val(moment(json.RCE_fecha_primera_comidaIngreso_Medico_Pediatrico).format('YYYY-MM-DD'));
            $("#txt2Comida").val(moment(json.RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico).format('YYYY-MM-DD'));

            $("#rdoConvulsionesSi").bootstrapSwitch('state', (json.RCE_convulsiones_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoConvulsionesNo").bootstrapSwitch('state', (json.RCE_convulsiones_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoSBORSi").bootstrapSwitch('state', (json.RCE_SBOR_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoSBORNo").bootstrapSwitch('state', (json.RCE_SBOR_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoNeumoniasSi").bootstrapSwitch('state', (json.RCE_neumonias_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoNeumoniasNo").bootstrapSwitch('state', (json.RCE_neumonias_personalIngreso_Medico_Pediatrico === "NO"));

            $("#rdoGastrointestSi").bootstrapSwitch('state', (json.RCE_gastrointest_personaltIngreso_Medico_Pediatrico === "SI"));
            $("#rdoGastrointestNo").bootstrapSwitch('state', (json.RCE_gastrointest_personaltIngreso_Medico_Pediatrico === "NO"));

            $("#rdoUrinarioSi").bootstrapSwitch('state', (json.RCE_urinario_personalIngreso_Medico_Pediatrico === "SI"));
            $("#rdoUrinarioNo").bootstrapSwitch('state', (json.RCE_urinario_personalIngreso_Medico_Pediatrico === "NO"));

            // EXAMEN FÍSICO
            $("#txtTannerExamenFisico").val(json.RCE_tannerIngreso_Medico_Pediatrico);
            $("#txtDenticionExamenFisico").val(json.RCE_denticionIngreso_Medico_Pediatrico);
            $("#txtPArtExamenFisico").val(json.RCE_partIngreso_Medico_Pediatrico);
            $("#txtSaturometriaExamenFisico").val(json.RCE_saturometriaIngreso_Medico_Pediatrico);
            $("#txtScoreExamenFisico").val(json.RCE_scoreIngreso_Medico_Pediatrico);

            // ANTECEDENTES FAMILIARES
            await CargarVacunasPacientes();

            $("#rdoAlergiasSi").bootstrapSwitch('state', (json.RCE_alergias_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoAlergiasNo").bootstrapSwitch('state', (json.RCE_alergias_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoAsmaSi").bootstrapSwitch('state', (json.RCE_asma_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoAsmaNo").bootstrapSwitch('state', (json.RCE_asma_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoEpilepsiaSi").bootstrapSwitch('state', (json.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoEpilepsiaNo").bootstrapSwitch('state', (json.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoDiabetesSi").bootstrapSwitch('state', (json.RCE_diabetes_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoDiabetesNo").bootstrapSwitch('state', (json.RCE_diabetes_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoCardiovascSi").bootstrapSwitch('state', (json.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoCardiovascNo").bootstrapSwitch('state', (json.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#rdoHTASi").bootstrapSwitch('state', (json.RCE_hta_familiaresIngreso_Medico_Pediatrico === "SI"));
            $("#rdoHTANo").bootstrapSwitch('state', (json.RCE_hta_familiaresIngreso_Medico_Pediatrico === "NO"));

            $("#txtMuertesFamiliares").val(json.RCE_muertes_familiaresIngreso_Medico_Pediatrico);
            $("#txtAlergiaFaramco").val(json.RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico);
            $("#txtOtrosAntecendentesPersonales").val(json.RCE_otros_familiaresIngreso_Medico_Pediatrico);

            CargarTablaHospitalizaciones();

            CargarPersona(json.GEN_idPersona_parental_1, null, null, "Parental1");
            CargarPersona(json.GEN_idPersona_parental_2, null, null, "Parental2");

            $("#sltSituacionMarital").val(json.GEN_idEstado_Conyugal);
            $("#sltDSM").val(json.RCE_idTipo_Desarrollo_Psico_Motor);
            $("#sltNutricional").val(json.GEN_idTipo_Datos_Nutricionales);

        }
    });
}

function CargarTablaHospitalizaciones() {

    if (buscarPacientePorDocumentoEIdentificacion().GEN_idPaciente !== undefined) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/DetallePaciente/${GetPaciente().GEN_idPaciente}`,
            async: false,
            success: function (data, status, jqXHR) {

                $("#tblHospitalizaciones tbody").empty();

                if (data.length > 0) {

                    var html = "";

                    $.each(data, function () {
                        $("#tblHospitalizaciones tbody").append(
                            `<tr class='text-center'>
                            <td>${(this.RCE_diagnosticoIngreso_Medico !== undefined) ? this.RCE_diagnosticoIngreso_Medico : this.HOS_diagnostico_principalEpicrisis}</td>
                            <td>${CalcularEdad(this.GEN_fec_nacimientoPaciente).edad}</td>
                            <td>${moment(this.HOS_fecha_ingreso_realHospitalizacion).format('DD-MM-YYYY')}</td>
                            <td>${(this.HOS_fecha_egreso_realHospitalizacion === null) ? "" : moment(this.HOS_fecha_egreso_realHospitalizacion).format('DD-MM-YYYY')}</td>
                            <td>${this.HOS_diasHospitalizacion}</td>
                        </tr>`);
                    });

                    $("#tblHospitalizaciones").show();
                    $("#divHospitalizaciones").hide();

                } else {

                    $("#tblHospitalizaciones").hide();
                    $("#divHospitalizaciones").show();

                }

            }
        });

    } else {

        $("#tblHospitalizaciones").hide();
        $("#divHospitalizaciones").show();

    }

}

async function getVacunasPacientePediatria(idPaciente) {

    try {
        const vacunas = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}/GEN_Paciente/${idPaciente}/vacunas`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return vacunas

    } catch (error) {
        if (error.status === 404) {
            toastr.warning("El paciente no registra vacunas")
        }
        else {
            console.error("Error al cargar vacunas del paciente pediatrico")
            console.log(JSON.stringify(error))

        }
    }
}

async function CargarVacunasPacientes() {

    const pacienteEncontrado = await buscarPacientePorDocumentoEIdentificacion($("#txtnumerotPac").val(), $("#sltIdentificacion").val())


    if (pacienteEncontrado.length === 0) {
        toastr.error("No ha ingresado un paciente")
        return
    }

    const { IdPaciente } = pacienteEncontrado[0]

    if (IdPaciente === undefined) {
        return
    }

    const vacunasPaciente = await getVacunasPacientePediatria(IdPaciente)

    if (vacunasPaciente === undefined)
        return

    arrayVacunasPaciente = [...vacunasPaciente]

    //$("#aAgregarPaciente, #sltVacunasPaciente").removeAttr("disabled");
    //$("#aAgregarPaciente").removeClass("disabled");

    //await AgregarFilaVP(vacunasPaciente);
    await dibujarTablaVacunasPaciente(arrayVacunasPaciente)
}


async function AgregarFilaVacunaPaciente(slt) {

    let vacuna = {}
    let Tipo = {}
    let PacienteTipoVacuna = {}

    if (!ExisteVacunaPaciente($(`#${slt}`).val())) {

        Tipo.Id = parseInt($(`#${slt} option:selected`).val())
        Tipo.Nombre = $(`#${slt} option:selected`).text()
        PacienteTipoVacuna.Id = 0

        vacuna.Tipo = Tipo
        vacuna.PacienteTipoVacuna = PacienteTipoVacuna

        await AgregarFilaVP(vacuna);

    }
    else {
        toastr.error("El paciente ya registra esta vacuna")
    }
}

async function dibujarTablaVacunasPaciente(vacunasPaciente) {

    if (vacunasPaciente === null)
        return

    let tr = ""

    let idTr = "trVacunaPaciente_" + $("#tblVacunasPaciente tbody").length;

    $("#tblVacunasPaciente tbody").empty();

    vacunasPaciente.map(v => {
        tr +=
            `<tr id='${idTr}' class='text-center' data-idTipoVacuna='${v.Tipo.Id}' data-idPacienteTipoVacuna = '${v.PacienteTipoVacuna.Id}'>
            <td>${v.Tipo.Edad ?? ""}</td>
            <td>${v.Tipo.Nombre}</td>  
            <td>${v.Tipo.Descripcion ?? ""}</td>
            <td class='text-center'>
                <a class='btn btn-danger' data-idpaciente-tipo-vacuna='${v.PacienteTipoVacuna.Id}' onclick="deleteFilaTablaVacunaPaciente(this);">
                    <i class='fa fa-minus-circle'></i>
                </a>
            </td>
        </tr>`;
    })

    $("#tblVacunasPaciente tbody").append(tr);
    $("#tblVacunasPaciente").show();
    $("#divVacunasPaciente").hide();
}

async function deleteFilaTablaVacunaPaciente(e) {

    let id = $(e).data("idpaciente-tipo-vacuna")
    $(e).parent().parent().remove()
    $(e).parent()

    await deleteElementoArrayVacunasPaciente(id)

}

async function deleteElementoArrayVacunasPaciente(id) {

    if (arrayVacunasPaciente.length <= 0)
        return

    const elementoBuscado = arrayVacunasPaciente.filter(x => x.PacienteTipoVacuna.Id === id)[0];
    const indice = arrayVacunasPaciente.findIndex(elemento => elemento === elementoBuscado);
    if (indice !== -1)
        arrayVacunasPaciente.splice(indice, 1)
}

async function AgregarFilaVP(vacunasPaciente) {

    if (vacunasPaciente === undefined)
        return

    if (GetPaciente().GEN_idPaciente === undefined) {
        return
    }

    arrayVacunasPaciente.push(vacunasPaciente)

    await dibujarTablaVacunasPaciente(arrayVacunasPaciente)

}

function ExisteVacunaPaciente(GEN_idTipo_Vacuna) {

    var existeVacuna = false;

    $("#tblVacunasPaciente tbody tr").each(function (index) {
        if ($(this).attr("data-idTipoVacuna") == GEN_idTipo_Vacuna)
            existeVacuna = true;
    });

    return existeVacuna;
}

async function GuardarPersona(GEN_idPersonas, tipoPersona) {

    let url = (GEN_idPersonas === 0) ? `${GetWebApiUrl()}GEN_Personas` : `${GetWebApiUrl()}GEN_Personas/${GEN_idPersonas}`;
    let method = (GEN_idPersonas === 0) ? "POST" : "PUT";
    let json = await GetJsonCargarPersona(GEN_idPersonas, null, null);

    let persona = {
        GEN_telefonoPersonas: json.GEN_telefonoPersonas ?? "",
        GEN_emailPersonas: json.GEN_emailPersonas,
        GEN_idSexo: json.GEN_idSexo,
        GEN_estadoPersonas: json.GEN_estadoPersonas,
        GEN_dir_callePersonas: json.GEN_dir_callePersonas,
        GEN_idCategorias_Ocupacion: json.GEN_idCategorias_Ocupacion,
        GEN_idTipo_Escolaridad: json.GEN_idTipo_Escolaridad,
        GEN_fecha_nacimientoPersonas: json.GEN_fecha_nacimientoPersonas,
        GEN_fecha_actualizacionPersonas: json.GEN_fecha_actualizacionPersonas
    };

    if (tipoPersona === "Cuidador") {
        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionCuidador").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocCuidador").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigCuidador").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreCuidador").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatCuidador").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatCuidador").val());
        persona.GEN_telefonoPersonas = valCampo($("#txtTelefonoCuidador").val());
        persona.GEN_dir_callePersonas = valCampo($("#txtDireccionCuidador").val());

    } else if (tipoPersona === "Parental1") {

        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionParental1").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocParental1").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigParental1").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreParental1").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatParental1").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatParental1").val());
        persona.GEN_fecha_nacimientoPersonas = ($("#txtFechaNacimientoParental1").val() === "") ? null : moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD");
        persona.GEN_idCategorias_Ocupacion = valCampo($("#sltActividadParental1").val());
        persona.GEN_idTipo_Escolaridad = valCampo($("#sltEducaciónParental1").val());

    } else if (tipoPersona === "Parental2") {

        persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionParental2").val());
        persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocParental2").val());
        persona.GEN_digitoPersonas = valCampo($("#txtDigParental2").val());
        persona.GEN_nombrePersonas = valCampo($("#txtnombreParental2").val());
        persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatParental2").val());
        persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatParental2").val());
        persona.GEN_fecha_nacimientoPersonas = ($("#txtFechaNacimientoParental2").val() === "") ? null : moment($("#txtFechaNacimientoParental1").val()).format("YYYY-MM-DD");
        persona.GEN_idCategorias_Ocupacion = valCampo($("#sltActividadParental2").val());
        persona.GEN_idTipo_Escolaridad = valCampo($("#sltEducaciónParental2").val());

    }

    if (GEN_idPersonas !== 0)
        persona.GEN_idPersonas = GEN_idPersonas;

    await $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(persona),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {

            if (data != undefined) {
                if (tipoPersona === "Cuidador")
                    ingresoMedicoPediatria.GEN_idPersona_Cuidador = data.GEN_idPersonas;
                else if (tipoPersona === "Parental1")
                    ingresoMedicoPediatria.GEN_idPersona_parental_1 = data.GEN_idPersonas;
                else if (tipoPersona === "Parental2")
                    ingresoMedicoPediatria.GEN_idPersona_parental_2 = data.GEN_idPersonas;
            }

        },
        error: async function (jqXHR, status) {
            console.log("Error al guardar Persona: " + jqXHR.responseText);
        }
    });
}

async function GuardarVacunasPaciente() {

    if (!validarCampos("#divDatosPaciente", true)) {
        toastr.error("No ha ingresado un paciente")
        return
    }

    const arrayVacunasPacienteParaGuardar = {
        Vacunas: arrayVacunasPaciente.map(v => ({
            IdPaciente: GetPaciente().GEN_idPaciente,
            IdTipoVacuna: v.Tipo.Id
        }))
    };

    let parametrizacion = {
        url: `${GetWebApiUrl()}GEN_Paciente_Tipo_Vacuna/${GetPaciente().GEN_idPaciente}`,
        method: "PUT"
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(arrayVacunasPacienteParaGuardar),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            toastr.success('Se registra Vacunas de Paciente.');
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Vacuna de Paciente: " + jqXHR.responseText);
        }
    });
}

async function CargarJsonIngresoMedicoPediatria() {

    //// INFORMACIÓN CUIDADOR
    ingresoMedicoPediatria.RCE_parentesco_cuidadorIngreso_Medico_Pediatrico = valCampo($("#txtparentescoCuidador").val());

    // ANTECEDENTES PERSONALES
    //ingresoMedicoPediatria.GEN_idEstablecimiento_Origen = valCampo($("#sltConsultorioOrigen").val());
    ingresoMedicoPediatria.RCE_idTipo_Recien_Nacido = valCampo($("#sltRecienNacido").val());
    ingresoMedicoPediatria.GEN_idTipo_Parto = valCampo($("#sltParto").val());
    ingresoMedicoPediatria.RCE_patologia_perinatalIngreso_Medico_Pediatrico = valCampo($("#txtPatologiaPerinatal").val());
    ingresoMedicoPediatria.RCE_otros_antecedentes_personalesIngreso_Medico_Pediatrico = valCampo($("#txtOtrosPediatria").val());
    ingresoMedicoPediatria.RCE_duracion_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#txtDuracionLMaterna").val());
    ingresoMedicoPediatria.GEN_idTipo_Edad_duracion_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#sltTipoEdadDuracionLMaterna").val());
    ingresoMedicoPediatria.RCE_inicio_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#txtInicioLArtificial").val());
    ingresoMedicoPediatria.GEN_idTipo_Edad_inicio_lactancia_añosIngreso_Medico_Pediatrico = valCampo($("#sltInicioLArtificial").val());
    ingresoMedicoPediatria.RCE_fecha_primera_comidaIngreso_Medico_Pediatrico = ($("#txt1Comida").val() === "") ? null : moment($("#txt1Comida").val()).format('YYYY-MM-DD');
    ingresoMedicoPediatria.RCE_fecha_segunda_comidaIngreso_Medico_Pediatrico = ($("#txt2Comida").val() === "") ? null : moment($("#txt2Comida").val()).format('YYYY-MM-DD');

    ingresoMedicoPediatria.RCE_convulsiones_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoConvulsionesSi"), $("#rdoConvulsionesNo"));
    ingresoMedicoPediatria.RCE_SBOR_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoSBORSi"), $("#rdoSBORNo"));
    ingresoMedicoPediatria.RCE_neumonias_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoNeumoniasSi"), $("#rdoNeumoniasNo"));
    ingresoMedicoPediatria.RCE_gastrointest_personaltIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoGastrointestSi"), $("#rdoGastrointestNo"));
    ingresoMedicoPediatria.RCE_urinario_personalIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoUrinarioSi"), $("#rdoUrinarioNo"));

    // EXAMEN FÍSICO
    ingresoMedicoPediatria.RCE_tannerIngreso_Medico_Pediatrico = valCampo($("#txtTannerExamenFisico").val());
    ingresoMedicoPediatria.RCE_denticionIngreso_Medico_Pediatrico = valCampo($("#txtDenticionExamenFisico").val());
    ingresoMedicoPediatria.RCE_partIngreso_Medico_Pediatrico = valCampo($("#txtPArtExamenFisico").val());
    ingresoMedicoPediatria.RCE_saturometriaIngreso_Medico_Pediatrico = valCampo($("#txtSaturometriaExamenFisico").val());
    ingresoMedicoPediatria.RCE_scoreIngreso_Medico_Pediatrico = valCampo($("#txtScoreExamenFisico").val());

    // ANTECEDENTES FAMILIARES

    ingresoMedicoPediatria.RCE_alergias_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoAlergiasSi"), $("#rdoAlergiasNo"));
    ingresoMedicoPediatria.RCE_asma_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoAsmaSi"), $("#rdoAsmaNo"));
    ingresoMedicoPediatria.RCE_epilepsia_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoEpilepsiaSi"), $("#rdoEpilepsiaNo"));
    ingresoMedicoPediatria.RCE_diabetes_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoDiabetesSi"), $("#rdoDiabetesNo"));
    ingresoMedicoPediatria.RCE_cardiovasc_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoCardiovascSi"), $("#rdoCardiovascNo"));
    ingresoMedicoPediatria.RCE_hta_familiaresIngreso_Medico_Pediatrico = ObtenerValRadio($("#rdoHTASi"), $("#rdoHTANo"));

    ingresoMedicoPediatria.RCE_muertes_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtMuertesFamiliares").val());
    ingresoMedicoPediatria.RCE_alergias_farmacos_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtAlergiaFaramco").val());
    ingresoMedicoPediatria.RCE_otros_familiaresIngreso_Medico_Pediatrico = valCampo($("#txtOtrosAntecendentesPersonales").val());

    ingresoMedicoPediatria.GEN_idEstado_Conyugal = valCampo($("#sltSituacionMarital").val());
    ingresoMedicoPediatria.RCE_idTipo_Desarrollo_Psico_Motor = valCampo($("#sltDSM").val());
    ingresoMedicoPediatria.GEN_idTipo_Datos_Nutricionales = valCampo($("#sltNutricional").val());
    ingresoMedicoPediatria.RCE_idIngreso_Medico = GetIngresoMedico().RCE_idIngreso_Medico;

}

async function CargarJsonIngresoMedicoPediatriaParaGuardar() {

    json = {}

    //// INFORMACIÓN CUIDADOR

    json.ParentescoCuidador = 1
    json.IdPersonaParental1 = 2
    json.IdPersonaParental2 = 3

    // ANTECEDENTES PERSONALES
    //ingresoMedicoPediatria.GEN_idEstablecimiento_Origen = valCampo($("#sltConsultorioOrigen").val());
    json.IdTipoRecienNacido = valCampo($("#sltRecienNacido").val())
    json.IdTipoParto = valCampo($("#sltParto").val())
    json.PatologiaPerinatal = $("#txtPatologiaPerinatal").val() === "" ? null : $("#txtPatologiaPerinatal").val()
    json.OtrosAntecedentesPersonales = $("#txtOtrosPediatria").val() === "" ? null : $("#txtOtrosPediatria").val()
    json.DuracionLactanciaAnios = $("#txtDuracionLMaterna").val() === "" ? null : $("#txtDuracionLMaterna").val()
    json.IdTipoEdadDuracionLactanciaAnios = $("#sltTipoEdadDuracionLMaterna").val() === "0" ? null : parseInt($("#sltTipoEdadDuracionLMaterna").val())
    json.InicioLactanciaAnios = $("#txtInicioLArtificial").val() === "" ? null : $("#txtInicioLArtificial").val()
    json.IdTipoEdadInicioLactanciaAnios = $("#sltInicioLArtificial").val() === "0" ? null : parseInt($("#sltInicioLArtificial").val())
    json.FechaPrimeraComida = ($("#txt1Comida").val() === "") ? null : moment($("#txt1Comida").val()).format('YYYY-MM-DD')
    json.FechaSegundaComida = ($("#txt2Comida").val() === "") ? null : moment($("#txt2Comida").val()).format('YYYY-MM-DD')

    json.ConvulsionesPersonal = ObtenerValRadio($("#rdoConvulsionesSi"), $("#rdoConvulsionesNo"))
    json.SBORPersonal = ObtenerValRadio($("#rdoSBORSi"), $("#rdoSBORNo"))
    json.NeumoniaPersonal = ObtenerValRadio($("#rdoNeumoniasSi"), $("#rdoNeumoniasNo"))
    json.GastroIntestPersonal = ObtenerValRadio($("#rdoGastrointestSi"), $("#rdoGastrointestNo"))
    json.UrinarioPersonal = ObtenerValRadio($("#rdoUrinarioSi"), $("#rdoUrinarioNo"))

    // EXAMEN FÍSICO
    json.Tanner = $("#txtTannerExamenFisico").val() === "" ? null : $("#txtTannerExamenFisico").val()
    json.Denticion = $("#txtDenticionExamenFisico").val() === "" ? null : $("#txtDenticionExamenFisico").val()
    json.Part = $("#txtPArtExamenFisico").val() === "" ? null : $("#txtPArtExamenFisico").val()
    json.Saturometria = $("#txtSaturometriaExamenFisico").val() === "" ? null : $("#txtSaturometriaExamenFisico").val()
    json.Score = $("#txtScoreExamenFisico").val() === "" ? null : $("#txtScoreExamenFisico").val()

    // ANTECEDENTES FAMILIARES

    json.AlergiasFamiliares = ObtenerValRadio($("#rdoAlergiasSi"), $("#rdoAlergiasNo"))
    json.AsmaFamiliares = ObtenerValRadio($("#rdoAsmaSi"), $("#rdoAsmaNo"))
    json.EpilepsiaFamiliares = ObtenerValRadio($("#rdoEpilepsiaSi"), $("#rdoEpilepsiaNo"))
    json.DiabetesFamiliares = ObtenerValRadio($("#rdoDiabetesSi"), $("#rdoDiabetesNo"))
    json.CardiovascFamiliares = ObtenerValRadio($("#rdoCardiovascSi"), $("#rdoCardiovascNo"))
    json.HtaFamiliares = ObtenerValRadio($("#rdoHTASi"), $("#rdoHTANo"))

    json.MuertesFamiliares = $("#txtMuertesFamiliares").val() === "" ? null : $("#txtMuertesFamiliares").val()
    json.AlergiasFarmacosFamiliares = $("#txtAlergiaFaramco").val() === "" ? null : $("#txtAlergiaFaramco").val()
    json.OtrosFamiliares = $("#txtOtrosAntecendentesPersonales").val() === "" ? null : $("#txtOtrosAntecendentesPersonales").val()

    json.IdEstadoConyugal = $("#sltSituacionMarital").val() === "0" ? null : parseInt($("#sltSituacionMarital").val())
    json.IdTipoDesarrolloPsicoMotor = $("#sltDSM").val() === "0" ? null : parseInt($("#sltDSM").val())
    json.IdTipoDatosNutricionales = $("#sltNutricional").val() === "0" ? null : parseInt($("#sltNutricional").val())

    return json
}

function ObtenerValRadio(radioSi, radioNo) {

    if (radioSi.bootstrapSwitch('state'))
        return "SI";
    else if (radioNo.bootstrapSwitch('state'))
        return "NO";

    return null;
}

async function cargarPersonaParentalAsync() {



    let persona = {}
    persona.IdIdentificacion = $("#sltIdentificacionPersona").val()
    persona.NumeroDocumento = $("#txtnumeroDocPersona").val()
    persona.Digito = $("#txtDigPersona").val()
    persona.Nombre = $("#txtNombrePersona").val()
    persona.ApellidoPaterno = $("#txtApePatPersona").val()
    persona.ApellidoMaterno = $("#txtApeMatPersona").val()
    persona.IdTipoGenero = $("#sltTipoGeneroPersona").val()
    persona.IdSexo = $("#sltSexoPersona").val()

    return persona

}

async function GuardarPersonaParentalAysnc(persona) {

    let parametrizacion = {
        url: `${GetWebApiUrl()}GEN_Personas`,
        method: "POST"
    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(persona),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            toastr.success('Se registra persona parental');

        },
        error: async function (jqXHR, status) {
            console.log("Error al guardar persona parental: " + JSON.stringify(jqXHR));
        }
    });

}

async function GuardarIngresoPediatrico(idIngresoMedico) {

    let url = ""
    let method = ""

    let json = await CargarJsonIngresoMedicoPediatriaParaGuardar()

    const personaParental1 = await buscarPersonaAcompaniante(null, $("#sltIdentificacionParental1").val(), $("#txtnumeroDocParental1").val())
    const personaParental2 = await buscarPersonaAcompaniante(null, $("#sltIdentificacionParental2").val(), $("#txtnumeroDocParental2").val())

    json.IdPersonaParental1 = personaParental1?.Id ?? null
    json.IdPersonaParental2 = personaParental2?.Id ?? null

    json.IdIngresoMedico = idIngresoMedico

    if (json.Id === undefined) {
        url = `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico`
        method = "POST"
    }
    else {
        url = `${GetWebApiUrl()}RCE_Ingreso_Medico_Pediatrico/${json.IdIngresoMedico}`
        method = "PUT"
    }

    await $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            toastr.success('Se registra Ingreso Médico Pediatrico.');
        },
        error: async function (jqXHR, status) {
            console.log("Error al guardar Ingreso Médico pediatrico: " + JSON.stringify(jqXHR));
        }
    });
}