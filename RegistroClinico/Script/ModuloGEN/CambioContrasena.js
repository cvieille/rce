﻿
$(document).ready(function () {

    //if (getSession().ES_CAMBIO_CONTRASEÑA === "true")
    //    $("#pnl_menuModulos a").removeAttr("href");
    if (getSession().ES_CAMBIO_CONTRASEÑA === true) {
        // Deshabilitar los enlaces de los módulos
        $("#pnl_menuModulos a").each(function () {
            $(this).on("click", function (e) {
                e.preventDefault();
                Swal.fire({
                    icon: 'warning',
                    title: 'Recuerde que debe cambiar su contraseña para continuar.',
                    showConfirmButton: false,
                    timer: 2000
                });
            })
        });
        // Mostrar alerta de cambio de contraseña
        Swal.fire({
            icon: 'warning',
            title: 'Debe cambiar su contraseña para nevegar por los módulos.',
            showConfirmButton: false,
            timer: 2000
        });
    }
    
    var exp = new RegExp("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,16})$");

    $('#txtContraseñaNueva').on('input', function () {

        var parent = $('#txtContraseñaNueva').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();

        if ($('#txtContraseñaNueva').val() !== "") {

            if (exp.test(this.value)) {
                $('#txtContraseñaNueva').removeClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-success'><i class='fa fa-check'> Clave Correcta.</label>");
            } else {
                $('#txtContraseñaNueva').addClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> La clave no cumple con los requisitos mínimos.</label>");
            }

        } else $('#txtContraseñaNueva').removeClass("invalid-input");
    });

    $("#txtContraseñaNuevaRepetida").on('input', function () {

        var parent = $('#txtContraseñaNuevaRepetida').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();

        if ($("#txtContraseñaNuevaRepetida").val() !== '') {
            if ($("#txtContraseñaNuevaRepetida").val() === $('#txtContraseñaNueva').val()) {
                $('#txtContraseñaNuevaRepetida').removeClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-success'><i class='fa fa-check'> Las claves coinciden</label>");
            } else {
                $('#txtContraseñaNuevaRepetida').addClass("invalid-input");
                parent.after("<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> Las claves no coinciden</label>");
            }
        } else $('#txtContraseñaNuevaRepetida').removeClass("invalid-input");

    });

    $("#txtContraseñaActual").on('input', function () {
        $('#txtContraseñaActual').removeClass("invalid-input");
        var parent = $('#txtContraseñaActual').parent();
        parent.next(".color-error").remove();
        parent.next(".color-success").remove();
    });

    $("#btnCambiarContraseña").click(function () {

        if (validarCampos("#divReinicioContraseña", false) && exp.test($('#txtContraseñaNueva').val()) &&
            ($("#txtContraseñaNuevaRepetida").val() === $('#txtContraseñaNueva').val())) {

            var parent = $('#txtContraseñaActual').parent();
            parent.next(".color-error").remove();
            parent.next(".color-success").remove();

            var sSession = getSession();
            var json = {
                "ClaveNueva": '' + CryptoJS.MD5($("#txtContraseñaNueva").val()),
                "ClaveAntigua": '' + CryptoJS.MD5($('#txtContraseñaActual').val())
            };

            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}GEN_Usuarios/${sSession.ID_USUARIO}/CambiarClave`,
                data: JSON.stringify(json),
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (data, status, jqXHR) {

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Contraseña actualizada.',
                        showConfirmButton: false,
                        timer: 2000
                    }).then((result) => {
                        setSession("ES_CAMBIO_CONTRASEÑA", false);
                        window.location.replace(`${ObtenerHost()}/Vista/Inicio.aspx`);
                    });

                },
                error: function (jqXHR, status) {
                    if (jqXHR.status === 409) {
                        $('#txtContraseñaActual').addClass("invalid-input");
                        parent.after(`<label class='ml-5 mb-1 mt-1 color-error'><i class='fa fa-times'> ${JSON.parse(jqXHR.responseText).Message}</label>`);
                    }
                }
            });

        }

        return false;

    });

    ShowModalCargando(false);
    
});
