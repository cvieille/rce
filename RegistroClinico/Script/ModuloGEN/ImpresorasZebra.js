﻿
let impresoraSeleccionada = null;
let impresorasZebra = [];
const TipoImpresora = {
    General_Adulto: 'General_Adulto',
    General_Pediatrico: 'General_Pediatrico',
    GinecoObtetrico_Adulto: 'GinecoObtetrico_Adulto'
};

$(document).ready(function () {
    $("#divEstadoImpresoras").hide();
});

async function ConectarImpresoras() {

    impresorasZebra = await getImpresorasZebra();
    let contador = 1;
    while (contador <= 3) {
        const impresoras = await getImpresoras();
        if (impresoras) break;
        contador++;
    }
    
}

function getImpresoras() {
    return new Promise((resolve, reject) => {
        BrowserPrint.getLocalDevices(
            function (deviceList) {
                const { printer } = deviceList;
                console.log("Impresoras conectadas: " + JSON.stringify(printer));

                if (printer) {
                    printer.forEach(function (p) {
                        const impresora = impresorasZebra.find(({ NombreImpresora }) => NombreImpresora === p.name);
                        if (impresora)
                            impresora.printer = p;
                    });
                }

                pintarEstadoImpresoras();

                // Aquí resolvemos la promesa indicando que la función terminó exitosamente
                resolve(printer);
            },
            function (errorMessage) {
                // Aquí rechazamos la promesa en caso de error
                reject("Error: " + errorMessage);
            }
        );
    });
}

function pintarEstadoImpresoras() {

    $("#divEstadoImpresoras").html("");
    $("#divEstadoImpresoras").show();

    let html = "";
    impresorasZebra.forEach((impresora) => {
        html += `<div class='col-sm-4'>
                    <div class='alert alert-${(impresora.printer) ? 'success' : 'danger'} p-2 text-center'>
                        <strong><i class="fa fa-print"></i> Impresora: ${ impresora.Nombre } | Estado: ${ (impresora.printer) ? 'Conectada' : 'Desconectada' }</strong>
                    </div>
                </div>`;
    });

    $("#divEstadoImpresoras").html(html);
}

async function imprimirBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete, tipoImpresora) {

    await ConectarImpresoras();
    const impresora = impresorasZebra.find(({ Tipo }) => Tipo === tipoImpresora);
    if (!impresora) {
        Swal.fire({ position: 'center', icon: 'warning', title: 'Impresora sin conexión' });
        return;
    }
    if (impresora && !impresora.printer) {
        Swal.fire({ position: 'center', icon: 'warning', title: 'Impresora sin conexión' });
        return;
    }

    const zplBrazalete = await getBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete);
    impresoraSeleccionada = impresora.printer;
    console.log(zplBrazalete);
    impresoraSeleccionada.send(zplBrazalete, function () {
        Swal.fire({ position: 'center', icon: 'success', title: 'impresión realizada exitosamente.' });
    }, function (error) {
        Swal.fire({ position: 'center', icon: 'danger', title: 'Error al imprimir brazalete.'});
        console.log(error);
    });

}

async function getBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete) {
    const { Plantilla } = await promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/${tipoAtencion}/${tipoBrazalete}/Brazalete`);
    return Plantilla;
}