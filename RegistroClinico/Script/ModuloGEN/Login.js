﻿
let userlogged = {}

$(document).ready(function () {

    reiniciarPerfiles();
    $("#txtUsuario").on('input', () => {
        $("#txtClave").val("");
        reiniciarPerfiles()
    });
    $("#txtClave").on('input', () => reiniciarPerfiles());
    $("#txtClave").on('keypress', function (evt) {
        if (evt.which == 13)
            buscarUsuario(this);
    });
});

function buscarUsuario(input) {

    //Por si ingresa primero la password y luego el usuario
    let otroInput = document.getElementById("txtClave");
    if (input.id == "txtClave")
        otroInput = document.getElementById("txtUsuario");



    //Si ingresa con el botón Ingresar como
    if (sessionStorage.getItem("IngresarComo")) {



        usuario = document.getElementById("txtUsuario").value;

        $.ajax({
            type: "POST",
            url: GetWebApiUrl() + "GEN_Usuarios/Autentificacion?login=" + usuario,
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (dataClave) {

                $("#txtClave").val(dataClave);

                let usuarioBuscar = {
                    idSistema: 14,
                    Login: usuario,
                    Clave: dataClave
                }

                $.ajax({
                    method: "POST",
                    url: `${GetWebApiUrl()}GEN_usuarios/perfiles`,
                    data: usuarioBuscar,
                    success: function (perfiles) {

                        llenarPerfiles(perfiles);

                        if (perfiles.length > 1) {
                            ShowModalCargando(false);
                        } else {
                            $("#sltPerfiles").val(perfiles[0].Id);
                            iniciarSesion();
                        }

                    }, error: function (error) {

                        console.error("Ha ocurrido un error al buscar el usuario y los perfiles")
                        console.log(error)
                        reiniciarPerfiles();
                        if (error.status == 404)
                            toastr.error(error.responseJSON.message)

                        ShowModalCargando(false);
                        sessionStorage.removeItem("IngresarComo"); //Revisar donde agregar esto
                    }
                });



            }, error: function (error) {
                console.error('error; ' + eval(error));
            }
        });

    } else {




        //validacion de campos vacios
        if (input.value !== "" && otroInput.value !== "") {

            let usuarioBuscar = {
                idSistema: 14,
                Login: document.getElementById("txtUsuario").value,
                Clave: "" + CryptoJS.MD5(document.getElementById("txtClave").value)
            }

            ShowModalCargando(true);

            $.ajax({
                method: "POST",
                url: `${GetWebApiUrl()}GEN_usuarios/perfiles`,
                data: usuarioBuscar,
                success: function (perfiles) {

                    llenarPerfiles(perfiles);

                    if (perfiles.length > 1) {
                        ShowModalCargando(false);
                    } else {
                        $("#sltPerfiles").val(perfiles[0].Id);
                        iniciarSesion();
                    }

                }, error: function (error) {

                    console.error("Ha ocurrido un error al buscar el usuario y los perfiles")
                    console.log(error)
                    reiniciarPerfiles();
                    if (error.status == 404)
                        toastr.error(error.responseJSON.message)

                    ShowModalCargando(false);

                }
            });
        }
    }

}




function reiniciarPerfiles() {
    $("#sltPerfiles").empty()
    $("#sltPerfiles").append(`<option value="0">Seleccione un perfil</option>`)
}

function llenarPerfiles(perfiles) {
    let contenido = "";
    reiniciarPerfiles();
    perfiles.forEach(item => { contenido += `<option value="${item.Id}">${item.Valor}</option>` })
    $("#sltPerfiles").append(contenido);
}

/** Nueva funcion de inicio de sesion **/
function iniciarSesion() {

    if (validarCampos("#divLogin", false)) {

        //Validacion de campos
        let handler = {
            set: function (obj, prop, value) {
                if (value == "") {
                    toastr.error(`Error: faltan campos obligatorios.`)
                    return false
                }
                obj[prop] = value;
            }
        }
        const usuarioLogin = new Proxy({}, handler)
        //Armando usuario a logear
        usuarioLogin.grant_type = "password"
        usuarioLogin.username = document.getElementById("txtUsuario").value
        if (sessionStorage.getItem("IngresarComo")) {
            usuarioLogin.password = document.getElementById("txtClave").value
        } else {
            usuarioLogin.password = "" + CryptoJS.MD5(document.getElementById("txtClave").value)
        }
        sessionStorage.removeItem("IngresarComo"); //Revisar donde agregar esto

        usuarioLogin.clientid = 14
        usuarioLogin.idPerfil = parseInt(document.getElementById("sltPerfiles").value)

        delete $.ajaxSettings.headers['Authorization'];
        $.ajax({
            type: "POST",
            url: `${GetWebApiUrl()}recuperarToken`,
            data: usuarioLogin,
            success: function (response) {
                resp = response;
                setSession("TOKEN", resp.access_token);
                setSession("LOGIN_USUARIO", usuarioLogin.username);
                setSession("FECHA_FINAL_SESION", moment(GetFechaActual()).add(parseInt(resp.expires_in) -5, 'seconds').format('YYYY-MM-DD HH:mm:ss'));
                setSession("PASSWORD", usuarioLogin.password)

                $.ajaxSetup({
                    headers: { 'Authorization': GetToken() }
                });
                userlogged = usuarioLogin
                getUsuario(false);
            },
            error: function (err) {
                LoginIncorrecto();
                console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
            }
        });

    }
}

function getUsuario(esLoginModal) {

    var bool = true;

    try {
        sessionStorage.removeItem('menuRCE');
        let url = `${GetWebApiUrl()}GEN_Usuarios/Acceso`;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {
                try {
                    if ($.isEmptyObject(data)) {

                        //No se encontro informacion del usuario
                        LoginIncorrecto();
                        bool = false;

                    } else {

                        //Obtener nombre completo
                        const nombreUsuario = data[0].GEN_nombrePersonas + " " +
                            data[0].GEN_apellido_paternoPersonas +
                            ((data[0].GEN_apellido_maternoPersonas == null) ? "" : " " + data[0].GEN_apellido_maternoPersonas);

                        setSession("ID_USUARIO", data[0].GEN_idUsuarios);
                        setSession("ID_ESPECIALIDAD", data[0].GEN_idEspecialidad[0]);
                        setSession("NOMBRE_USUARIO", nombreUsuario);

                        if (!($.isEmptyObject(data[0].GEN_idProfesional))) {
                            setSession("ID_PROFESIONAL", data[0].GEN_idProfesional);
                        }
                        //Obtener perfil seleccinado


                        $.each(data, function (key, val) {
                            let perfiles = [];
                            $.each(val.GEN_Perfiles, function () {
                                perfiles.push({
                                    "GEN_idPerfil": this.GEN_idPerfil,
                                    "GEN_codigoPerfil": this.GEN_codigoPerfil,
                                    "GEN_descripcionPerfil": this.GEN_descripcionPerfil
                                });
                            });
                            let perfilSeleccionado = perfiles.find(z => z.GEN_idPerfil == userlogged.idPerfil)
                            //Valida si es cambio de contraseña
                            if (!esLoginModal)
                                setSession("ES_CAMBIO_CONTRASEÑA", ($('#txtClave').val() === val.GEN_numero_documentoPersonas));

                            setSession('PERFILES', JSON.stringify(perfiles));
                            SetSessionServer({
                                LOGIN_USUARIO: getSession().LOGIN_USUARIO,
                                NOMBRE_USUARIO: nombreUsuario,
                                CODIGO_PERFIL: perfilSeleccionado.GEN_codigoPerfil,
                                PERFIL_USUARIO: perfilSeleccionado.GEN_descripcionPerfil,
                                TOKEN: getSession().TOKEN
                            });

                            if (esLoginModal) {
                                $('#mdlLogin').modal('hide');
                                $('#txtUsuario').val('');
                                $('#txtClave').val('');
                            } else {

                                setSession("PERFIL_USUARIO", perfilSeleccionado.GEN_descripcionPerfil);
                                setSession("CODIGO_PERFIL", perfilSeleccionado.GEN_codigoPerfil);
                                setSession("ID_PERFIL", perfilSeleccionado.GEN_idPerfil);
                                //Valida cambio de contrasena
                                if ($('#txtClave').val() === val.GEN_numero_documentoPersonas) {
                                    window.location.replace(`${ObtenerHost()}/Vista/CambioContrasena.aspx`);
                                } else {
                                    window.location.replace(`${ObtenerHost()}/Vista/Inicio.aspx`);
                                }

                            }
                        });
                    }
                } catch (err) {
                    console.log("error: " + err.message);
                    bool = false;
                }
            }
        });
    } catch (err) {
        console.log("Error:" + err.message);
        bool = false;
    }
    return bool;
}
function getProfesional(id) {
    var url = GetWebApiUrl() + "/GEN_Profesional/" + id;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                var idEspecialidad = null;
                if (!($.isEmptyObject(data.GEN_Pro_Especialidad))) {
                    $.each(data.GEN_Pro_Especialidad, function () {
                        $.each(this.GEN_Especialidad, function (esp) {
                            if (esp.GEN_estadoPro_Especialidad != "Inactivo")
                                idEspecialidad = esp.GEN_idEspecialidad;
                        });
                    });
                    if (idEspecialidad != null)
                        setSession("ID_ESPECIALIDAD", idEspecialidad);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });
}
function LoginIncorrecto() {

    Swal.fire({
        position: 'center',
        icon: 'error',
        title: "Usuario o contraseña incorrecta",
        showConfirmButton: false,
        timer: 2000
    });

    $('#divLogin').addClass('shake');
    $("#modalCargando").hide();

}
