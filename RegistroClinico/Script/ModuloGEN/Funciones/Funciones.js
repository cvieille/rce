﻿
$(document).ready(function () {

    // MÉTODOS DROPDOWN
    $('body').on('click', '.dropdown-menu a', function (e) {
        $(this).parents('.dropdown').find('.btn').text($(this).text());
        e.preventDefault();
    });

    $('body').on('click', '.dropdown', function (e) {
        if ($('.dropdown-menu', this).children().length == 0)
            e.stopPropagation();
    });

    // MÉTODOS DROPDOWN 
    $('body').on('click', '.menuModulos', function (e) {
        // Guardar el id del menú clickeado o algo
        sessionStorage.idMenu = $(this).prop('id');
    });

    $('.ui.tabular > .item').click((e) => {
        $('.arrowright.active').removeClass('active');
        $(this).children('.arrowright').addClass('active');
    });

});

function toggle(id) {
    if (idDiv == "") {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = "";
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}

function imprimeFormularioIQX(id) {
    const url = `${GetWebApiUrl()}PAB_Formulario_Ind_Qx/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

function ImprimirDocumento(id, tipoDocumento) {

    ShowModalCargando(true);

    $.ajax({
        cache: false,
        type: 'GET',
        url: `${GetWebApiUrl()}hl7/Patient/${tipoDocumento}/${id}/Imprimir`,
        contentType: false,
        processData: false,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (response, status, xhr) {

            try {

                ShowModalCargando(false);
                var blob = new Blob([response], { type: 'application/pdf' });
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);

            } catch (ex) {
                ShowModalCargando(false);
                console.log(ex);
            }

        }
    });
}
function ShowModalCargando(esVisible) {
    if (esVisible) {
        $("#modalCargando").show();
    } else
        $("#modalCargando").hide();
}
function GetRUTConFormato(rut, dv) {
    const rutArray = rut.split('');
    let rutConPuntos = "";
    for (let i = rutArray.length - 1, j = 1; i >= 0; i--, j++)
        rutConPuntos = ((j % 3 == 0) ? " " + rutArray[i] : rutArray[i]) + rutConPuntos;
    return `${ rutConPuntos.trim().replaceAll(" ", ".") }-${ dv }`;
}
function firstUpper(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); });
    return str;
}

function fadeInLeft(e) {
    e.css('visibility', 'visible');
    e.addClass('animated fadeInLeft');
    setTimeout(function () { e.removeClass('animated fadeInLeft') }, 2000);
}

function fadeOutLeft(e) {
    e.addClass('animated fadeOutLeft');
    setTimeout(function () { e.removeClass('animated fadeOutLeft').css('visibility', 'hidden'); }, 2000);
}

function fadeIn(e) {
    e.css('visibility', 'visible');
    e.addClass('animated fadeIn');
    setTimeout(function () { e.removeClass('animated fadeIn') }, 2000);
}

function fadeOut(e) {
    e.addClass('animated fadeOut');
    setTimeout(function () { e.removeClass('animated fadeOut').css('visibility', 'hidden'); }, 2000);
}

function DarFuncionRadios() {
    $("input[type='checkbox'], input[type='radio']").bootstrapSwitch();

    $("input[type='checkbox'], input[type='radio']").each(function () {
        $(this).on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                var radioChecked = $(this);
                $(this).attr('checked', 'checked');

                $("input[name='" + radioChecked.attr("name") + "']").each(function () {
                    if (radioChecked.attr("id") !== $(this).attr("id"))
                        $(this).removeAttr("checked");
                });
            } else
                $(this).removeAttr("checked");
        });
    });
}

// funcion para validar el correo
function caracteresCorreoValido(email, b) {

    //var email = $(email).val();
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    var correo = email.val();
    var valido = true;

    if (!b)
        return false;

    if (correo === "" || correo === null) {
        if (email.hasClass('is-invalid'))
            email.removeClass('is-invalid');
    } else {
        if (!caract.test(correo)) {
            valido = false;
            if (!email.hasClass('is-invalid'))
                email.addClass('is-invalid');
        } else {
            if (email.hasClass('is-invalid'))
                email.removeClass('is-invalid');
        }
    }

    return valido;

}

async function ObtenerVerificador(sRut) {
    let resultado
    if (sRut.length >= 1 && /^[0-9]*$/.test(sRut)) {
        await $.ajax({
            url: `${GetWebApiFuncionesUrl()}Persona/digitoVerificador?numeroDocumento=${sRut}`,
            method: "GET",
            success: function (res) {
                resultado = res.resultado
            }, error: function (err) {
                console.log("Ha ocurrido un error al intentar obtener el digito verificador")
                console.log(err)
            }
        })
        return resultado
    }
}
//Traducir el mes de una fecha en-es
function traducirMoment(fecha) {
    moment.locale('es')
    let mes = fecha.format("MMMM")
    return mes

}

function CalcularEdad(fechaNac) {

    var edad;
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Paciente/CalcularEdad?fecha=${fechaNac}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            edad = data;
        },
        error: function (err) {
            console.log(`Error al calcular edad paciente: ${JSON.stringify(err)}`)
        }
    });

    return edad;
}

function GuardarError(sistema, modulo, formulario, detalle, stackTrace, codigo) {

    var edad;

    $.ajax({
        type: "POST",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GuardarLog`,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            sistema: sistema,
            modulo: modulo,
            formulario: formulario,
            log: JSON.stringify({
                detalle: detalle,
                stackTrace: stackTrace,
                codigo: codigo
            })
        }),
        async: false,
        success: function (data) { }
    });

    return edad;

}

function AgregarHoras(cantHoras) {

    var fecha = null;

    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=AddHorasFechaActual&cantHoras=${cantHoras}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fecha = data.fecha;
        }
    });

    return fecha;

}

function AgregarDias(fechaSeleccionada, cantDias) {

    var fecha = null;

    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=AddDiasFechaActual&fecha=${fechaSeleccionada}&cant=${cantDias}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fecha = data.fecha;
        }
    });

    return fecha;

}

function ConvertirBytes(tipo, bytes) {
    if (tipo === 'kb')
        return parseFloat(Math.round((bytes / 1024) * 100) / 100).toFixed(2);
    else if (tipo === 'mb')
        return parseFloat(Math.round((bytes / Math.pow(1024, 2)) * 100) / 100).toFixed(3);
    else if (tipo === 'gb')
        return parseFloat(Math.round((bytes / Math.pow(1024, 3)) * 100) / 100).toFixed(3);
}

function ObtenerHost() {
    var host = "";
    if (window.location.origin.indexOf("localhost") > -1)
        return window.location.origin;
    return window.location.origin + "/RCE";
}

async function EsValidoDigitoVerificador(rut, digito) {

    try {

        let { esExitoso } = await $.ajax({
            type: 'GET',
            url: `${GetWebApiFuncionesUrl()}Persona/validarRut/${rut}/${digito}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return esExitoso

    } catch (error) {
        if (error.status === 400)
            toastr.error(`El RUT <strong>${rut}-${digito}</strong> es inválido.`)
    }
}

function visibleDesdeOyente(component) {

    if (component.data("print") === true) {
        $(component.data("frame")).on('load', function () { $("#modalCargando").hide(); });
        $("#modalCargando").show();
    }

}

function estiloTabla(tabla) {

    tabla.prepend($("<thead></thead>").append(tabla.find("tr:first")))
        .addClass('nowrap')
        .dataTable({
            'stateSave': true,
            'iStateDuration': 120,
            'lengthMenu': [[10, 20], [10, 20]],
            responsive: true,
            columnDefs: [
                { targets: [-1, -3], className: 'dt-body-right' }
            ]
        });

}

function obtenerNombreUsuario() {
    return $(".nombreUsuario").html();
}

function AgregarSeleccioneSelect(array, id, value) {
    array.unshift({ id: 0, value: "Seleccione" });
    return array;
}


//Validación para campos Input = radio
function validarCamposInputRadio(idDivPadre, irAPrimerComponente) {
    var esValido = true;
    var nameInput = "";
    valoresInput = [];
    ultimoDiv = "";
    var primerComponente = null;


    $(`${idDivPadre} input[type='radio'][data-required='true']`).each(function (index) {
        $(this).removeClass("invalid-input")
    })
    //Selecciona los Input/Campos dentro del Padre con idDivPadre que tienen data-required = true
    $(`${idDivPadre} input[type='radio'][data-required='true']`).each(function (index) {

        if (nameInput != $(this).attr("name")) {

            if (nameInput == "") {  //Valor Inicia.
                nameInput = $(this).attr("name")
                valoresInput.push($(this).is(':checked'))
                ultimoDiv = $(this)
            } else {                //Si Cambia de name input, se revisa areglo
                if (valoresInput.indexOf(true) == -1) { //Si falla la validación
                    esValido = false
                    ultimoDiv.parent().parent().parent().addClass("invalid-input")
                    ultimoDiv = $(this)
                    nameInput = $(this).attr("name")
                } else { //Si pasa la validación se sigue con lo iguientes inputs
                    valoresInput = [];
                    nameInput = $(this).attr("name")
                    valoresInput.push($(this).is(':checked'))
                    ultimoDiv.parent().parent().parent().removeClass("invalid-input")
                    ultimoDiv = $(this)
                }
            }
        } else {
            valoresInput.push($(this).is(':checked'))

            if ($(`${idDivPadre} input[type='radio'][data-required='true']`).length == index + 1) { //Si es la ultima iteración
                if (valoresInput.indexOf(true) == -1) { //Si falla la validación
                    esValido = false
                    $(this).parent().parent().parent().addClass("invalid-input")
                } else {
                    $(this).parent().parent().parent().removeClass("invalid-input")
                }
            }

        }

        if (!esValido && primerComponente === null)
            primerComponente = $(this);
    })

    //if (primerComponente !== null && irAPrimerComponente)
    //    $(`html, ${idDivPadre}`).animate({ scrollTop: primerComponente.offset().top - 40 }, 500);

    return esValido
}

function encontrarDatoSeleccionado(datos, valor) {

    return datos.some(x => x.Valor === valor)
}


//Validación para campos en general
function validarCampos(idDivPadre, irAPrimerComponente, datos = null) {
    var esValido = true;
    var primerComponente = null;

    //Selecciona los Input/Campos dentro del Padre con idDivPadre que tienen data-required = true
    $(`${idDivPadre}  input[type='text'][data-required='true'],
        ${idDivPadre} input[type='password'][data-required='true'],
        ${idDivPadre} select[data-required='true'],
        ${idDivPadre} input[type='time'][data-required='true'],
        ${idDivPadre} input[type='date'][data-required='true'],
        ${idDivPadre} input[type='number'][data-required='true'],
        ${idDivPadre} input[type='search'][data-required='true'],
        ${idDivPadre} input[type='radio'][data-required='true'],
        ${idDivPadre} textarea[data-required='true'],
        ${idDivPadre} div[data-required='true'],
        ${idDivPadre} input[type='checkbox'][data-required='true'],
        ${idDivPadre} table[data-required='true']`).each(function () {

        //excepción cuando el select tiene solo 1 opción (-Seleccione-)
        //excepción cuando el elemento es visible
        //excepción cuando el elemento está deshabilitado
        switch ($(this).prop('nodeName')) {
            case 'INPUT':
                //validacion de bootstrapSwitch
                if ($(this).parent().hasClass("bootstrap-switch-container")) {
                    if ($(this).bootstrapSwitch('state') == undefined || $(this).bootstrapSwitch('state') == null)
                        if (!esValidoRadio) {
                            esValido = false;
                            $(this).addClass('bootstrapSwitch-invalid');
                        } else $(this).removeClass('bootstrapSwitch-invalid');
                }

                //Valida input de libreria jQuery Typeahead
                if (datos !== null && $(this).attr("name") === "hockey_v1[query]") {
                    esValido = encontrarDatoSeleccionado(datos, $(this).val())
                    //console.log(esValido)
                    if (!esValido) {
                        $(this).addClass("invalid-input")
                        let spans = $(this).parent().find("span")
                        spans.remove()
                        let html = "<span style='color:red;'>Este campo es obligatorio</span>"
                        $(this).after(html)
                    }
                    else {
                        $(this).removeClass("invalid-input")
                        //$(this).next(".invalid-input").remove()
                        let spans = $(this).parent().find("span")
                        spans.remove()
                    }
                }
            // Validación de Textarea
            case 'TEXTAREA':

                //if ($(this).hasClass('textboxio')) {

                //    var txt = textboxio.replace('#' + $(this).attr("id")).content.get().replace(/<(?:.|\n)*?>/gm, '').replace(/\&nbsp;/g, '');

                //    if ($.trim(txt) === '') {
                //        esValido = false;
                //        $("#ephox_" + $(this).attr("id")).addClass("textboxio-invalid");
                //    } else $("#ephox_" + $(this).attr("id")).removeClass("textboxio-invalid");

                //    break;
                //}

                if (!AgregarEstiloErrorTxt(this)) {
                    esValido = false;
                } else if ($(this).attr("data-regex")) { // regex
                    esValido = false
                }
                break;
            //Validación de Select
            case 'SELECT':
                if (!AgregarEstiloErrorSlt(this)) esValido = false;
                break;
            case 'DIV':
                //Validación de Switches de Bootstrap
                if ($(this).hasClass("bootstrapSwitch")) {

                    var array = $("div.bootstrapSwitch input[type='radio'][name='" + $(this).attr("data-name") + "']");
                    var esValidoRadio = false;

                    for (var i = 0; i < array.length; i++) {
                        if ($(array[i]).bootstrapSwitch('state')) {
                            esValidoRadio = true;
                            break;
                        }
                    }

                    if (!esValidoRadio) {
                        esValido = false;
                        $(this).addClass('bootstrapSwitch-invalid');
                    } else $(this).removeClass('bootstrapSwitch-invalid');
                }

                if (!$(this).hasClass("bootstrapSwitch") && ($('button', this).attr('val') === '0' || $('button', this).attr('val') === 'string:0' || $('button', this).attr('val') === 'number:0') &&
                    $('.dropdown-menu', this).children().length > 0 && $(this).is(":visible") && !$(this).hasClass('disabled')) {
                    esValido = false;
                    $('button', this).addClass('invalid');
                } else $('button', this).removeClass('invalid');
                break;
            case 'TABLE':
                if ($(this).children("tbody").length == 0) {
                    $(this).children("tbody").addClass('invalid');
                } else $(this).children("tbody").removeClass('invalid');
                break;
            default:
                //Validación de tipo Checkbox
                if ($(this).attr("type") === "checkbox") {
                    if ($(this).hasClass('custom-control-input') && !$(this).is(':checked')) {
                        $(this).parent().css({ "border": "2px solid red" });
                        esValido = false;
                    } else
                        $(this).parent().css({ "border": "" });
                }
                //Validacion de radio
                if ($(this).attr("type") === "radio") {

                    esValido = false;

                    if ($(this).hasClass("bootstrapSwitch")) {

                        $(this).parent().parent().parent().addClass("invalid-input");

                        for (const rdo of $(`[name='${$(this).attr("name")}']`)) {
                            if ($(rdo).bootstrapSwitch('state'))
                                esValido = true;
                        }

                        (!esValido) ?
                            $(this).parent().parent().parent().addClass("invalid-input") :
                            $(this).parent().parent().parent().removeClass('invalid-input');

                    } else {

                        for (const rdo of $(`[name='${$(this).attr("name")}']`)) {


                            if ($(rdo).is(':checked'))
                                esValido = true;
                        }

                        (!esValido) ?
                            $(this).parent().parent().addClass("invalid-input") :
                            $(this).parent().parent().removeClass('invalid-input');

                    }

                }
                break;//Fin default
        }

        if (!esValido && primerComponente === null)
            primerComponente = $(this);

    });

    if (primerComponente !== null && irAPrimerComponente)
        $('html, body').animate({ scrollTop: primerComponente.offset().top - 40 }, 500);

    return esValido;

}

function validarAutocompletar(datos, e) {

    console.log(datos)

    //return datos.filter(x => x.Valor === e)
}

function validarCamposRegex(idDivPadre) {

    let esValido = false;

    let arrayValidos = []
    let inputs = $(`${idDivPadre} input`)

    inputs.map((index, e) => {

        let valorIngresado = e.value
        let dataRegex = e.dataset.regex
        let rege = new RegExp(dataRegex)
        let valido = rege.test(valorIngresado)

        if (valorIngresado !== "")
            arrayValidos.push(valido)
    })

    esValido = arrayValidos.some(valor => !valor)

    return esValido
}
function validarRegex(e, regex) {

    let reg = new RegExp(regex)
    const next = $(e).next()

    if ($(e).data("regex") !== null) {

        if (reg.test($(e).val()) || $(e).val() === "") {
            $(e).removeAttr("data-regex")
            $(e).removeClass("invalid-input")

            if (next != undefined && $(next).hasClass("invalid-input")) {
                $(next).remove();
                $(e).removeClass('invalid-input');
            }

        }
        else {
            $(next).empty()
            $(e).attr("data-regex", regex)
            $(e).addClass("invalid-input")
            let informacionData = $(e).data("informacion") !== undefined ? `(${$(e).data("informacion")})` : ""
            $(e).after(`<span class='invalid-input'>Medida Invalida ${informacionData}</span>`);
            return
        }
    }
    else {
        $(e).removeAttr("data-regex")
        $(e).removeClass("invalid-input")
    }
}

function AgregarEstiloErrorTxt(comp) {

    let esValido = true;
    //console.log( " 3: " + $(comp).is(":visible"))
    if (($.trim($(comp).val()) === '' || $(comp).val() === null) && $(comp).is(":visible") && !$(comp).hasClass('disabled')) {
        esValido = false;
        $(comp).addClass('invalid-input');
        if ($(comp).next().prop('nodeName') == undefined)
            $(comp).after("<span class='invalid-input'>Este campo es obligatorio</span>");
    } else {
        const next = $(comp).next()
        if (next != undefined && $(next).hasClass("invalid-input")) {
            $(next).remove();
            $(comp).removeClass('invalid-input');
        }
    }

    return esValido;
}

function AgregarEstiloErrorSlt(comp) {

    let esValido = true;

    if ($(comp).val() == null || $(comp).val() == '' || ($(comp).val() === '0' || $(comp).val().includes(':0')) &&
        ($(comp)[0].length > 0 && $(comp).is(":visible") && !$(comp).hasClass('disabled'))) {
        esValido = false;
        $(comp).addClass('is-invalid');
        if ($(comp).next().prop('nodeName') == undefined)
            $(comp).after("<span class='invalid-input'>Este campo es obligatorio</span>");
    } else {
        $(comp).next().remove();
        $(comp).removeClass('is-invalid');
    }

    return esValido;

}

function SaltoDeLinea(t, c) {

    var r = '';
    var res = t.split(' ');

    for (var i = c; i < res.length; i = i + (c + 1)) {
        res.splice(i, 0, '<br />');
    }

    return res.join(' ');

}

// Esta ruta se obtiene desde AppSetting.json y se establece en Default.aspx
function GetWebApiUrl() {

    if (localStorage.getItem("urlWebApi") == null || localStorage.getItem("urlWebApi") == undefined) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetUrlWebApi`,
            async: false,
            success: function (data) {
                localStorage.setItem("urlWebApi", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });
    }

    return localStorage.getItem("urlWebApi");

}

function GetWebApiFuncionesUrl() {

    if (localStorage.getItem("urlWebApiFunciones") == null || localStorage.getItem("urlWebApiFunciones") == undefined) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetUrlWebApiFunciones`,
            async: false,
            success: function (data) {
                localStorage.setItem("urlWebApiFunciones", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });
    }

    return localStorage.getItem("urlWebApiFunciones");

}

function GetIpServers() {

    if (localStorage.getItem("IpServer") == null || localStorage.getItem("IpServers") == undefined) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetIpServers`,
            async: false,
            success: function (data) {
                localStorage.setItem("IpServers", JSON.stringify(data));
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });

    }
    return localStorage.getItem("IpServers");
}

async function getverificarFonasa() {

    try {
        const data = await $.ajax({
            type: 'GET',
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetverificarFonasa`,
            contentType: 'application/json',
        })

        return data

    } catch (error) {
        console.error("Error al cargar appsetting")
        console.log(JSON.stringify(error))
    }
}

async function getRutasJson() {

    try {
        const data = await $.ajax({
            type: 'GET',
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetRutas`,
            contentType: 'application/json',
        })

        return data

    } catch (error) {
        console.error("Error al cargar rutas")
        console.log(JSON.stringify(error))
    }
}

async function getImpresorasZebra() {

    try {

        const data = await $.ajax({
            type: 'GET',
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetImpresorasZebra`,
            contentType: 'application/json',
        })

        return JSON.parse(data);

    } catch (error) {
        console.error("Error al cargar rutas")
        console.log(JSON.stringify(error))
    }

}

async function getUrlPabellon() {

    try {
        const data = await $.ajax({
            type: 'GET',
            url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=GetUrlPabellon`,
            contentType: 'application/json',
        })

        return data

    } catch (error) {
        console.error("Error al cargar appsetting")
        console.log(JSON.stringify(error))
    }
}

function getCookie(cname, key) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            var cookie = c.substring(name.length, c.length);
            return cookie.split("=")[1];
        }
    }
    return null;
}

function EliminarCookie(cookiename) {
    var d = new Date();
    d.setDate(d.getDate() - 1);
    var expires = ";expires=" + d;
    var name = cookiename;
    var value = "";
    document.cookie = name + "=" + value + expires + "; path=/acc/html";
}

function ValTipoCampo(component, value) {
    switch (component.prop('nodeName')) {
        case 'SELECT':
            (value === null) ? component.val('0') : component.val(value)
            break;
    }
}

function valCampo(obj) {
    switch (typeof obj) {
        case 'string':
            return ($.trim(obj) !== '') ? $.trim(obj) : null;
        case 'number':
            return (obj !== 0) ? obj : null;
        case 'undefined':
            return null;
    }
    return null;
}

function DevolverString(obj) {
    switch (typeof obj) {
        case 'undefined':
        case 'null':
            return '';
            break;
    }

    return obj.toString();
}

function SetSessionServer(session) {
    $.ajax({
        type: "POST",
        url: `${ObtenerHost()}/Vista/Handlers/MetodosGenerales.ashx?method=SetSession`,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        cache: false,
        data: JSON.stringify(session),
        async: false,
        success: function (data) { }
    });
}

// MÉTODOS VARIABLES DE SESIÓN
function deleteSession(key) {
    let json = JSON.parse(localStorage.getItem("sSession"));
    delete json[key];
    localStorage.setItem("sSession", JSON.stringify(json));
}

function setSession(key, value) {
    let json = (localStorage.getItem("sSession") != null) ? JSON.parse(localStorage.getItem("sSession")) : {};
    json[key] = value;
    localStorage.setItem("sSession", JSON.stringify(json));
}

function RevisarAcceso(quitarAcceso, session) {
    //permite reciclar la sesión si ya la he cargado
    var vSession;
    if (session == undefined || session == null)
        vSession = getSession();
    else
        vSession = session;

    if (vSession.ID_USUARIO != undefined) {

        //excepción por si no esta seteado nMODULO (Modal de perfiles se caía)
        if (vSession.nMODULO != undefined) {
            var vRedireccionar = true;
            //var vModulo = vSession.nMODULO.split(',');
            var cModulo = vSession.cMODULO.split(',');

            for (var i = 0; i < cModulo.length; i++) {
                //for (var i = 0; i < vModulo.length; i++) {
                var url = window.location.pathname;
                url = url.replace('/RCE', '');
                //console.log('modulo: ' + cModulo[i]);
                //console.log('url: ' + url);
                if (cModulo[i] == url)
                    vRedireccionar = false;
            }
            if (vRedireccionar) {
                if (quitarAcceso)
                    quitarListener();
            }
        }
    } else {
        if (quitarAcceso)
            quitarListener();

        window.location.replace(ObtenerHost() + "/Vista/Default.aspx");
    }
}

function quitarListener() {
    window.removeEventListener('beforeunload', bunload, false);
    location.href = document.referrer;
}

function getSession() {
    if (localStorage.getItem("sSession") != null)
        return JSON.parse(localStorage.getItem("sSession"));
    return {};
}

function deleteSession(item) {
    if (localStorage.getItem("sSession") != null) {
        const sSession = JSON.parse(localStorage.getItem("sSession"));
        delete sSession[item];
        localStorage.setItem("sSession", JSON.stringify(sSession));
    }
}

function getSessionName(sesionName) {
    if (localStorage.getItem(sesionName) != null)
        return JSON.parse(localStorage.getItem(sesionName));
    return {};
}

// MÉTODOS VARIABLES DE SESIÓN//////////////////////////////////

function GetFechaActual() {
    let fechaActual = {}
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=GetFechaActual',
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            fechaActual = moment(data.fecha, "YYYY.MM.DD HH.mm.ss").toDate();
        }
    });

    return fechaActual
}

function GetDiferenciaFechas(fechaInicio, fechaFinal) {

    var iDif = null;
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx' +
            '?method=GetDiferenciaEnDias' +
            '&fechaInicio=' + fechaInicio +
            '&fechaFinal=' + fechaFinal,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            iDif = data.diferenciaFechas;
        }
    });

    return iDif;

}

function GetLoginPass() {
    try {
        var sSession = getSession();
        //if (sSession.ID_PROFESIONAL == undefined)
        RevisarAcceso(true, sSession);
        //window.location.replace(ObtenerHost() + "/Vista/Default.aspx");
        var json = {};
        //OBTIENE LOGIN Y CONTRASEÑA ENCRIPTADA
        $.ajax({
            type: "GET",
            url: GetWebApiUrl() + 'GEN_Usuarios/Login',
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (data) {

                const { GEN_idUsuarios, GEN_loginUsuarios, GEN_claveUsuarios } = data

                json.login = GEN_loginUsuarios
                json.pass = GEN_claveUsuarios
                json.ID_USUARIO = GEN_idUsuarios

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('error:' + xhr.responseText);
            }
        });
    } catch (err) {
        console.log("Error:" + err.message);
    }

    return json;
}


// METODOS DE ENCRIPTACIÓN AES /////////////////////////////////

function GetTextoEncriptado(txt) {
    var txtEncriptado;
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Vista/Handlers/MetodosGenerales.ashx?method=Encriptar&texto=' + txt,
        async: false,
        success: function (data) {
            txtEncriptado = data;
        },
        error: function (err) {
            console.log("error");
        }
    })
    return txtEncriptado;
}

// METODOS DE ENCRIPTACIÓN AES /////////////////////////////////

function GetToken() {
    if (getSession().TOKEN != undefined)
        return 'Bearer ' + getSession().TOKEN;
    return null;
}

function HtmlAString(html) {
    var div = document.createElement("div");
    div.innerHTML = html;
    return div.textContent || div.innerText || "";
}

function sortByKeyDesc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

function sortByKeyAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function filterItems(query, array) {
    return array.filter(function (el) {
        return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
    });
}

function ReiniciarRequired() {

    $("[data-required]").each(function (index, item) {

        if ($(this).attr("data-required") == "true") {

            let label = "";

            if ($(this).attr("type") === "radio")
                label = $(this).parent().parent().parent().children("label");
            else {

                label = $(this).parent().children("label");

                if (!label.length > 0)
                    label = $(this).parent().parent().children("label");

                if (!label.length > 0) {
                    //console.log(`Este elemento no se pudo marcar ${$(this).attr("id")} (*)`);
                    //console.log($(this).parent());//este elemento no se pudo marcar(*)
                }

            }

            label.children("b").remove();
            label.html($.trim(label.text()) + " <b class='color-error'>(*)</b>");

        } else {
            $(this).parent().children("label").children("b").remove();
        }
    });

}
//Recibe el input al cual se le setea el event (keydown, keypress), longitud maxima del input, y el evento
function validarLongitud(input, longitud, evento) {
    if (input.value.length >= longitud) {
        //Para que pueda borrar el contenido
        if (evento.keyCode !== 8 && evento.keyCode !== 46 && evento.keyCode !== 9)
            evento.preventDefault()
    }

}
function ValidarNumeros() {

    $(".number").keypress(function (event) {

        // 103 = e // 44 = ,
        if (event.which == 101 || event.which == 44)
            event.preventDefault();

        // 43 = + // 45 = - // 46 = .
        switch (event.which) {
            case 43:
            case 45:

                if (($(this).val().includes('+') || $(this).val().includes('-')) || $(this).val().length > 0)
                    event.preventDefault();
                break;

            case 46:

                if ($(this).val().includes('.') || $(this).val() == '')
                    event.preventDefault();
                break;

            default:

                if (!/^[0-9]*$/.test(String.fromCharCode(event.which)))
                    event.preventDefault();
                else if ($(this).val().includes('.') && $(this).val().substring($(this).val().indexOf("."), $(this).val().length).length == 3)
                    event.preventDefault();
                break;

        }

    }).on('input', function () {
        if ($(this).val().includes('.') && $(this).val().substring($(this).val().indexOf("."), $(this).val().length).length > 3)
            $(this).val($(this).val().replace(".", ""));
    }).blur(function () {
        if ($(this).val() != '') {
            if ($(this).val().charAt($(this).val().length - 1) == '.')
                $(this).val($(this).val() + '0');
            else if ($(this).val().startsWith("."))
                $(this).val('0' + $(this).val());
            else if (($(this).val().includes('+') || $(this).val().includes('-')) && $(this).val().length < 2)
                $(this).val('');
        }
    });
}

function validarDecimales(event) {

    let keycode = event.keyCode
    let string = "!@#$%^/[]{}`&*()_=+<>:;´¨"
    //Concateno porque se rompe el string
    string = string + `"`
    //Entre 48 y 57- entre 96 y 105--8, 190, 110,16
    if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105) || keycode == 8 || keycode == 9 || keycode == 190 || keycode == 110 || keycode == 16) {
        if (!string.includes(event.key))
            return true
    }
    event.preventDefault();
}
function validarValorPresionArterial(input) {

    //let esValido = /^(\d+)$|^(\d+\/{1}\d+)$/;
    if (input.value != "") {
        let esValido = /^\d{1,3}\/\d{1,3}$/;
        if (!esValido.test(input.value)) {
            $(input).addClass("invalid-input")
            return false
        } else {
            $(input).removeClass("invalid-input")
            return true
        }
    }
}
//Se le debe pasar el valor a validar.
function validarDecimalesNumero(number) {

    let esValido = /^(\d+)$|^(\d+\.{1}\d+)$/;
    if (!esValido.test(number)) {
        return false
    } else {
        return true
    }
}
async function getDataDesdeApi(url) {
    try {
        const response = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: function (data) {
                return data;
            },
            error: function (jqXHR, status) {
                console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
                estado = jqXHR.status;
            }
        });
        return response;
    } catch (error) {
        console.log(`Error URL: ${url} ${JSON.stringify(error)}`);
        throw error;
    }
}
function setIdValorEnCombo(data, selector) {
    $.each(data, function (key, val) {
        $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
    });
}
//function setCargarDataEnCombo(url, asincrono, selector, select) {
//    let estado = {}
//    $(selector).empty();
//    $.ajax({
//        type: 'GET',
//        url: url,
//        contentType: 'application/json',
//        dataType: 'json',
//        async: asincrono,
//        success: function (data) {
//            $(selector).append("<option value='0'>Seleccione</option>");
//            $.each(data, function (key, val) {
//                if (select == 'ddlServicio') {
//                    if (val.Id == 1) {
//                        $(selector).append(`<option selected value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
//                    } else {
//                        $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
//                    }
//                } else {
//                    $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
//                }
//            });
//            return true;
//        },
//        error: function (jqXHR, status) {
//            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
//            estado = jqXHR.status;
//        }
//    });

//    return estado
//}

function setCargarDataEnCombo(url, asincrono, selector, select) {
    let estado = {};

    // Verificar si la URL contiene un idTipoAtencion=null
    if (url.includes("idTipoAtencion=null")) {
        return estado;  // Retorna sin hacer la solicitud
    }

    $(selector).empty();

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            $(selector).append("<option value='0'>Seleccione</option>");
            $.each(data, function (key, val) {
                if (select == 'ddlServicio') {
                    if (val.Id == 1) {
                        $(selector).append(`<option selected value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
                    } else {
                        $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
                    }
                } else {
                    $(selector).append(`<option value='${(val.id == undefined) ? val.Id : val.id}'>${(val.valor == undefined) ? val.Valor : val.valor}</option>`);
                }
            });
            return true;
        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
            estado = jqXHR.status;
        }
    });

    return estado;
}

function setCargarDataEnRadios(url, asincrono, selector, grupo) {
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            let htmlString = "";
            $.each(data, function (key, val) {
                htmlString += `
                    <div class="row">
                        <div style="display: inline !important; margin: 5px 10px;">
                            <input type="radio" class="form-check checkbox-1x" name="${grupo}" value="${(val.id == undefined) ? val.Id : val.id}">
                        </div>
                        <label>&nbsp;&nbsp;${(val.valor == undefined) ? val.Valor : val.valor}</label>
                    </div>
                `;
            });
            $(selector).append(htmlString);
            return true;
        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
            return false;
        }
    });
}
//funciona como el setCargarDataEnCombo:{id, valor}, se diferencia en que este tiene grupo =Tipo(grupo)...Balances=subitems
function setCargarDataEnComboGroup(url, asincrono, selector) {

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            $(selector).empty();
            let contenido = ""
            data.map(a => {
                contenido += `<optgroup label="${a.Tipo}" >`
                a.Balances.map(i => {
                    contenido += `<option value="${i.Id}" data-Tipo="${a.Tipo}">${i.Valor}</option>`
                })
                contenido += `</optgroup>`
            })
            $(selector).append(contenido)
            return true;
        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
            return false;
        }
    });

}
function CargarComboHijo(padre, hijo, funcionCargadoHijo) {
    if ($(padre).val() != '0') {
        $(hijo).removeAttr('disabled');
        //descomentar
        //$(hijo).attr('data-required', 'false');
        funcionCargadoHijo($(hijo), $(padre).val());
    } else {
        $(hijo).attr('disabled', 'disabled');
        $(hijo).val('0');
    }

}
function showModalCargandoComponente(componente) {
    $(componente).append(
        `<div class="componente-cargando center-horizontal-vertical">
            <i class="fa fa-cog fa-spin fa-3x"></i>
        </div>`);
}

function hideModalCargandoComponente(componente) {
    componente.find('.componente-cargando').remove();
}

function formatDate(dateString) {
    let date = moment(dateString);
    if (date.isValid()) {
        date = date.toDate().format('dd-MM-yyyy');
    } else {
        date = dateString;
    }
    return date;
}

// Validación de tiempo de sesión del Token
function EsValidaSesionToken(element = null) {

    const fechaActual = moment().toDate();
    const fechaExpiracionToken = moment(getSession().FECHA_FINAL_SESION).toDate();

    if (fechaActual >= fechaExpiracionToken) {

        $("#txtUsuarioLogin").val(getSession().LOGIN_USUARIO);
        mostrarModal("mdlLogin");


        if (element != null) {
            switch ($(element).prop('nodeName')) {
                case 'INPUT':
                case 'TEXTAREA':
                    $(element).val('');
                    break;
                case 'SELECT':
                    $(element).val('0');
                    break;
            }
        }
        return false;
    }
    return true;
}

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": 150,
    "hideDuration": 150,
    "timeOut": 5000,
    "extendedTimeOut": 1500,
    "showEasing": "linear",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function promesaAjax(metodoHttp, url, json = null, esAsincrona = true) {
    let urlCompleta = GetWebApiUrl() + url;
    let promesa = new Promise((resolve, reject) => {
        $.ajax({
            type: metodoHttp,
            url: urlCompleta,
            dataType: 'json',
            data: json,
            async: esAsincrona,
            success: function (data) {
                resolve(data);
            },
            error: function (xhr, error) {
                let response = {
                    "metodo": metodoHttp,
                    "url": urlCompleta,
                    "jsonEnviado": json,
                    "respuesta": error,
                    "xhr": xhr,
                }
                reject(response);
            }
        });
    });
    return promesa;
}

async function ImprimirApiExterno(url, elemento = null, method = "GET", body = null) {

    ShowModalCargando(true);

    let ventana;

    await $.ajax({
        cache: false,
        contentType: "application/json",
        type: method,
        data: body,
        url: url,
        processData: false,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (response, status, xhr) {

            try {

                ShowModalCargando(false);

                let blob = new Blob([response], { type: 'application/pdf' });
                let URL = window.URL || window.webkitURL;
                let downloadUrl = URL.createObjectURL(blob);

                if (elemento === null)
                    ventana = window.open(downloadUrl);
                else {

                    let iFrame = document.getElementById(elemento);
                    if (iFrame !== null)
                        iFrame.src = downloadUrl;
                }

            } catch (ex) {
                console.log(ex);
            }
        },
        error: function (err) {
            ShowModalCargando(false);
            console.log("Error al intentar realizar la impresion: " + JSON.stringify(err));
        }
    });

    return ventana;
}

async function ImprimirApiExternoAsync(url, elemento = null) {

    ShowModalCargando(true);

    await $.ajax({
        cache: false,
        contentType: "application/json",
        type: "GET",
        url: url,
        processData: false,
        xhrFields: {
            responseType: 'blob'
        },
        success: async function (response, status, xhr) {

            try {

                ShowModalCargando(false);

                let blob = new Blob([response], { type: 'application/pdf' });
                let URL = window.URL || window.webkitURL;
                let downloadUrl = URL.createObjectURL(blob);

                if (elemento === null)
                    window.open(downloadUrl);
                else {

                    let iFrame = document.getElementById(elemento);
                    if (iFrame !== null)
                        iFrame.src = downloadUrl;
                }

            } catch (ex) {
                console.log(ex);
            }
        },
        error: async function (err) {
            ShowModalCargando(false);
            console.log("Error al intentar realizar la impresion: " + JSON.stringify(err));
        }
    });
}


function InicializarTypeAhead(element, arrayCarteraArancel, emptytempl, templ, source) {
    /* Element = input que realiza busqueda, 
     * ArrayCartelArancel = datos de la bdd.
     * emptytempl= Mensaje por defecto cuando no hay coincidencias, 
     * templ = Mensaje cuando hay coincidencias.
     */
    $(element).typeahead(setTypeAhead(arrayCarteraArancel, element, emptytempl, templ, source));
    $(element).change(() => {
        $(element).removeData("id")
        $(element).removeData("id-cartera")
    });
    $(element).blur(() => {
        if ($(element).data("id") == undefined)
            $(element).val("");
    });
}

//TypeAhead para select2, filtro que busca coincidencias en el texto ingresado
function setTypeAhead(data, input, emptytempl, templ, source) {
    var typeAhObject = {
        input: input,
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: emptytempl,
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                //Tener presente la estructura informacion para setear esto
                //Actualmente esta funcionando con la estructura que devuelve  GEN_Cartera_Servicio/Combo?idTipoArancel[0]=5
                $(input).data('id', item.ServicioCartera.Id);
                $(input).data('id-cartera', item.Aranceles[0].IdCarteraServicioArancel);
            }
        }
    };

    return typeAhObject;
}

const imprimeFechaHora = (data) => {
    const { Ingreso: { Fecha, Hora } } = data
    fechaHospital = Fecha
    let fechaHosp = (moment(Fecha).format('DD-MM-YYYY') != null) ? moment(Fecha).format('DD-MM-YYYY') : ""
    let horaHosp = (Hora != null) ? moment(Hora, "HH:mm:ss").format("hh:mm A") : ""

    return `${fechaHosp} ${horaHosp}`
}

//function ocultarMostrarFichaPac(identificador, rut, func = null) {

//    $(identificador).on('change', function () {

//        if (func !== null) {
//            func()
//        }

//        switch ($(this).val()) {
//            case '2':
//            case '3':
//            case '5':
//                $(".digito").hide();
//                $(rut).attr("maxlength", 15);
//                $(rut).attr("placeholder", "Ficha Clínica")
//                break;
//            default:
//                $(".digito").show();
//                $(rut).attr("maxlength", 8);
//                break;
//        }
//    });
//}

function ocultarMostrarFichaPac(identificador, rut, func = null) {

    $(identificador).on('change', function () {
        if (typeof func === 'function') {
            func();
        }

        const valor = $(this).val();
        //console.log($(identificador).data("tipo") !== undefined)
        let digitoContainer = $(".digito");
        if ($(identificador).data("tipo") !== undefined)
            digitoContainer = $(".digitoAcompanante")

        const maxLength = (valor === '2' || valor === '3' || valor === '5') ? 15 : 8;
        const placeholder = (valor === '2' || valor === '3' || valor === '5') ? "Ficha Clínica" : "";

        digitoContainer.toggle(!(valor === '2' || valor === '3' || valor === '5'));
        $(rut).attr("maxlength", maxLength);
        $(rut).attr("placeholder", placeholder);
    });
}

function ChangeTitleNumDoc(identificacion, numDocumento, label) {
    let textoModificar = ``
    $(identificacion).change(function () {
        textoModificar = $(identificacion).children("option").filter(":selected").text()
        $(numDocumento).attr("placeholder", "Del Paciente")
        $(label).text(textoModificar)
    })
}

function colapsarElementoDOM(elemento, desplegado) {

    let opcion = ""
    opcion = (desplegado) ? "show" : "hide"

    if (elemento !== null) {
        $(elemento).collapse(opcion)
    }
}
function limpiarErorresCampos(divPadre) {
    const divLimpiar = document.getElementById(divPadre);
    let elementosConIsInvalid = divLimpiar.querySelectorAll('.is-invalid');
    let elementosConInvalidInput = divLimpiar.querySelectorAll('.invalid-input');

    let totalElementos = [...elementosConIsInvalid, ...elementosConInvalidInput]

    totalElementos.forEach(elemento => {
        //Si es un span, eliminarlo
        if (elemento.nodeName == "SPAN") {
            elemento.remove()
        } else {
            //Cuando no es span, elimina solamente la clase
            if (elemento.classList.contains('is-invalid')) {
                elemento.classList.remove('is-invalid');
            } else {
                elemento.classList.remove('invalid-input');
            }
        }
    });
}

function validarFechaLimite(input, diasPasado, diasFuturo) {
    try {
        let fechaActual = moment(GetFechaActual())
        let fechaIngresada = moment(input.value)
        let fechaLimiteFuturo = moment(fechaActual).add(diasPasado, 'days').format('DD-MM-YYYY')
        let fechaLimitePasado = moment(fechaActual).subtract(diasFuturo, 'days').format('DD-MM-YYYY')
        let diferencia = fechaActual.diff(fechaIngresada, 'days')
        if (diferencia > 0) {
            if (Math.abs(diferencia) > diasPasado) {
                toastr.error(`La fecha no puede ser antes que: ${fechaLimitePasado}`)
                input.value = fechaActual.format("YYYY-MM-DD")
            }
        } else {
            if (Math.abs(diferencia) > diasFuturo) {
                toastr.error(`La fecha no puede ser después que: ${fechaLimiteFuturo}`)
                input.value = fechaActual.format("YYYY-MM-DD")
            }
        }

    } catch (error) {
        console.error(error);
    }
}

function cargarDatatableAjax(idTabla, url, columns, columnDefs) {

    $.fn.dataTable.ext.errMode = 'throw';

    if ($.fn.DataTable.isDataTable(idTabla))
        $(idTabla).DataTable().destroy();

    $(idTabla).DataTable({
        language: {
            "processing": `<i class="fa fa-cog fa-spin fa-3x"></i>`
        },
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        autoWidth: true,
        ajax: {
            url: url,
            type: "GET",
            data: function (d) {

                const columnIndex = d.order[0].column;
                const descendente = (d.order[0].dir === "desc");
                const column = d.columns[columnIndex];

                // Para incluir un sorting hay que agregarle las propiedades "orderable": true, "name": "nombre". Este name es distinto al title.
                // Este name esta perzonalizado desde la API
                // Ej: { "data": "Paciente", "title": "Nombre", "orderable": true, "name": "nombre", "render": (pac) => getNombreCompleto(pac) }
                if (column.name != null) {
                    d.orden = column.name;
                    d.descendente = descendente;
                } 

                d.pagina = (d.start / d.length) + 1;
                d.filas = d.length;

                delete d["_"];
                delete d["draw"];
                delete d["start"];
                delete d["length"];
                delete d["order"];
                delete d["columns"];
                delete d["search"];

            },
            dataSrc: function (response) {
                response.recordsTotal = response.TotalElementos;
                response.recordsFiltered = response.TotalElementos;
                return response.Elementos;
            },
            error: function (xhr, error, thrown) {

            }
        },
        columns: columns,
        columnDefs: columnDefs,
        destroy: true,
        retrieve: true
    });

}

function getNombreCompleto({ Nombre, ApellidoPaterno, ApellidoMaterno, NombreSocial }) {

    let nombreCompleto = $.trim(`${Nombre ?? ""} ${ApellidoPaterno ?? ""} ${ApellidoMaterno ?? ""}`);
    if (NombreSocial != null)
        nombreCompleto = `(${NombreSocial}) ${nombreCompleto}`;

    return nombreCompleto;

}

async function descargarReporteEnExcel(url, nombreArchivo) {

    let existeReporte = true;

    try {

        await $.ajax({
            type: "GET",
            url: url,
            xhrFields: {
                responseType: 'blob'
            },
            success: function (result) {
                var blob = result;
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = nombreArchivo;
                document.body.appendChild(a);
                a.click();
            },
            error: function (result) {
                existeReporte = false;
                console.log(JSON.stringify(result));
            }
        });
    } catch (ex) {
        console.log(ex)
    }

    return existeReporte;

}

async function llenarRangoFecha(dtFechaDesde, dtFechaHasta, meses) {

    try {

        let fechaActual = await GetFechaActual();
        fechaActual = moment(fechaActual, 'YYYY/MM/DD HH:mm:SS');
        const fechaMesesAtras = moment(fechaActual, 'YYYY/MM/DD HH:mm:SS').subtract(meses, 'months');

        $(dtFechaHasta).val(fechaActual.format('YYYY-MM-DD'));
        $(dtFechaDesde).val(fechaMesesAtras.format('YYYY-MM-DD'));

    } catch (ex) {
        console.log(ex)
    }

}

async function GetApiFonasa(numeroDocumento) {

    if (numeroDocumento === "")
        return

    try {
        const personaFonasa = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${numeroDocumento}/Fonasa`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return personaFonasa

    } catch (error) {
        console.error("Error al cargar persona Fonasa")
        console.log(JSON.stringify(error))
        ShowModalCargando(false);
        throw error;
    }
}
async function getCodigoIsapreApiFonasa(numeroDocumento, codigoIsapre) {
    if (numeroDocumento === "" || codigoIsapre === 0)
        return

    try {
        const codigo = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Prevision/CodigoIsapre/${codigoIsapre}/Fonasa`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return codigo

    } catch (error) {
        console.error("Error al cargar codigo Isapre")
        console.log(JSON.stringify(error))
    }
}

async function cargarPrevisionIsapre(personaFonasa, numeroDocumento, sltAseguradora, sltTramo) {

    switch (personaFonasa.coddesc) {
        case "BLOQUEADO CARGA ISAPRE":
        case "BLOQUEADO POR ISAPRE":
            if (personaFonasa.cdgIsapre !== "") {
                const codigoIsapreFonasa = parseInt(personaFonasa.cdgIsapre, 10).toString()
                const codigoIsapre = await getCodigoIsapreApiFonasa(numeroDocumento, codigoIsapreFonasa);
                await sleep(100)
                $(sltAseguradora).val(2).trigger("change")
                $(sltTramo).val(codigoIsapre === null ? 0 : codigoIsapre.Id).trigger("change");
            }
            break;
        case "BLOQUEADO DIPRECA":
            await sleep(100)
            $(sltAseguradora).val(4).trigger("change")
            $(sltTramo).val(13).trigger("change")
            break;
        case "BLOQUEADO CAPREDENA":
            await sleep(100)
            $(sltAseguradora).val(4).trigger("change")
            $(sltTramo).val(16).trigger("change")
            break;
    }
}

async function cargarPrevisionFonasa(personaFonasa, sltAseguradora, sltTramo) {

    let tramos = []

    if (personaFonasa.afiliadoTO.tramo !== "") {

        await sleep(100)
        $(sltAseguradora).val(1).trigger("change")

        $(`${sltTramo} option`).each(function (index, element) {

            let id = $(element).val()
            let valor = $(element).text()
            tramos.push({ id, valor })

        })

        tramos.shift()

        const tramo = tramos.find(t => t.valor === personaFonasa.afiliadoTO.tramo)

        $(sltTramo).val(tramo.id).trigger("change")
    }
}

function toogleFiltroBandeja(objetoBandeja) {

    if (Object.keys(objetoBandeja).length === 0) // comprueba si el objeto tiene propiedades
        return

    try {
        if ($(`#${objetoBandeja.filtro}`).attr("aria-expanded") === "true")
            establecerValor(objetoBandeja.identificacion, "0");
        else if ($(`#${objetoBandeja.filtro}`).attr("aria-expanded") === "false") {
            establecerValor(objetoBandeja.identificacion, "0");
            $(`#${objetoBandeja.rut}`).attr("maxlength", 8)
            $(`#${objetoBandeja.rut}`).val("")
            $(`#${objetoBandeja.dv}`).show()
            $(`#${objetoBandeja.guionDv}`).show()
        }
        else if ($(`#${objetoBandeja.filtro}`).attr("aria-expanded") === undefined)
            establecerValor(objetoBandeja.identificacion, "0")
        else
            throw new Error("Error en toogle de bandeja ")
    }
    catch (error) {
        console.error(`Se capturó el error ${error.message}`)
    }
}
function cerrarPopOvers() {
    $('.popover').popover('dispose')
}

function capturarExtractoParrafo(funcion, parrafo, maxPalabras, id) {

    let palabras = parrafo.split(' ')
    let extracto = palabras.slice(0, maxPalabras).join(' ')

    // Agregar puntos suspensivos
    if (palabras.length > maxPalabras) {

        extracto += `... <a href="javascript:void(0)" id="${id}" onclick="${funcion.name}('${parrafo.replace("'", "\\'")}','${id}')">Ver más</a>`;
    }

    return extracto
}

// si es pac NN cambia prevision y tramo a particular
async function CargarPacienteNN() {

    $("#sltIdentificacion").change(async function (e) {
        if ($(this).val() === '5') {
            await sleep(150)
            $('#sltPrevision').val('3').prop("disabled", true).trigger('change');
            $('#sltPrevisionTramo').val('11').prop("disabled", true).trigger('change');
            $("#btnCargarPrevisionFonasaPaciente").prop("disabled", true)
            $("#sltsexoPac").attr("data-required", true)
            $("#sltgeneroPac").attr("data-required", true)
        }
        else {
            $('#sltPrevision').prop("disabled", false)
            $('#sltPrevisionTramo').prop("disabled", false)
            $("#btnCargarPrevisionFonasaPaciente").prop("disabled", false)
        }
    });
}

async function pedirNuevoTokenCincoMinutosAntes(minutos) {

    const fechaServidor = moment(getSession().FECHA_FINAL_SESION).format("YYYY-MM-DD HH:mm")
    const fechaFinalSesion = new Date(fechaServidor)
    const FechaMinutosAntes = new Date(fechaFinalSesion.getTime() - minutos * 60 * 1000)
    const ahora = GetFechaActual()
    //const tiempoRestante = FechaMinutosAntes.getTime() - ahora.getTime() //en ms
    //const minutosRestante = tiempoRestante / 60000

    if (ahora.getTime() >= FechaMinutosAntes.getTime() && ahora.getTime() <= fechaFinalSesion.getTime()) {

        const tokenAntiguo = getSession().TOKEN

        const datos = {
            "grant_type": "password",
            "username": getSession().LOGIN_USUARIO,
            "password": getSession().PASSWORD,
            "clientid": '14',
            "idperfil": getSession().ID_PERFIL
        }

        await pedirNuevoToken(datos)

        const tokenNuevo = getSession().TOKEN
        console.log(tokenNuevo)


    }
}

async function pedirNuevoToken(datos) {

    delete $.ajaxSettings.headers['Authorization'];

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}recuperarToken`,
        data: datos,
        async: false,
        success: function (response) {
            setSession("TOKEN", response.access_token)
            setSession("FECHA_FINAL_SESION", moment(GetFechaActual()).add(parseInt(response.expires_in) - 5, 'seconds').format('YYYY-MM-DD HH:mm:ss'))
            setSession("LOGIN_USUARIO", getSession().LOGIN_USUARIO)
            $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + response.access_token } })
            SetSessionServer({
                FECHA_FINAL_SESION: getSession().FECHA_FINAL_SESION,
                LOGIN_USUARIO: getSession().LOGIN_USUARIO,
                NOMBRE_USUARIO: getSession().LOGIN_USUARIO,
                CODIGO_PERFIL: getSession().CODIGO_PERFIL,
                PERFIL_USUARIO: getSession().PERFIL_USUARIO,
                TOKEN: response.access_token
            })

        },
        error: function (err) {
            console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err))
        }
    })
}

async function validarRutaExistente(rutaBuscada) {
    const rutas = await getRutasJson()
    const rutaExiste = rutas.some(ruta => ruta.pagina === rutaBuscada)

    return rutaExiste;
}

function generateUUID() {
    // Verifica si `crypto.randomUUID` está disponible
    if (typeof crypto !== 'undefined' && typeof crypto.randomUUID === 'function') {
        return crypto.randomUUID();
    }

    // Si `crypto.randomUUID` no está disponible, usa una función manual
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = (Math.random() * 16) | 0,
            v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}
