﻿
function establecerValor(id, valor) {

    if (!id || !valor) {
        return;
    }
    try {
        document.getElementById(id).value = valor;
    } catch (error) {
        console.error(`No se pudo establecer el valor: ${error}`);
    }
}

function obtenerValor(id) {

    try {

        let valor = null

        if (id !== null || id !== undefined)
            valor = document.getElementById(id).value
        else
            return

        return valor

    } catch (error) {

        console.error(` No se pudo obtener el valor: ${error}`)
    }
}

function establecerValorHtml(id, valor) {

    try {

        if ((id === null || id === undefined) || (valor === null || valor === undefined))
            return

        document.getElementById(id).innerHTML = valor

    } catch (error) {

        console.error(` No se pudo establecer el valor: ${error}`)
    }
}
function ocultarModal(id) {
    try {

        const element = document.getElementById(id)

        if (element === null)
            throw new Error("id no encontrado")
        $(`#${id}`).modal("hide")

    } catch (error) {

        console.error(` No se pudo ocultar el modal: ${error}`)
    }
}
function mostrarModal(id) {
    try {
        const element = document.getElementById(id)

        if (element === null)
            throw new Error("id no encontrado")
        $(`#${id}`).modal("show")

    } catch (error) {
        console.error(` No se pudo mostrar el modal: ${error}`)
    }
}
function habilitarElementoPorClase(clase) {
    $("." + clase).prop("disabled", false)
}
function deshabilitarElementoPorClase(clase) {
    $("." + clase).prop("disabled", true)
}

function eliminarFilaDatatable(tabla, rowId) {
    let tablaOriginal = $(tabla).DataTable()
    tablaOriginal.row(rowId).remove().draw(false);
}