﻿async function getDataDesdeApiAsync(url) {
    const data = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}${url}`,
        contentType: 'application/json',
        dataType: 'json',
    })
}
function getDataMovimientosporApi(url, tabla, modal) {
    
    var adataset = [];
    $("#lnkExportarMovimientos").click(function (event) {
        event.preventDefault();
    });

    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            let response = data;
           //$("#idHosModalMovimientos").html(response.Paciente.Hospitalizacion.id);
            $("#nomPacModalMovimientos").html(`Paciente: <b>${response.Paciente.Nombre} ${response.Paciente.ApellidoPaterno}</b>`);
            $.each(response.Movimiento, function (i, r) {
                adataset.push([
                    moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss'),
                    r.Usuario,
                    r.TipoMovimiento
                ]);
            });

            if (adataset.length == 0)
                $('#lnkExportarMovimientos').addClass('disabled');

            else
                $('#lnkExportarMovimientos').removeClass('disabled');
            console.log(tabla)
            $(tabla).empty()
            $(tabla).addClass("nowrap").DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: "Fecha Movimiento" },
                    { title: "Usuario" },
                    { title: "Movimiento" }
                ],
                "columnDefs": [
                    {
                        "targets": 1,
                        "sType": "date-ukLong"
                    }
                ],
                "bDestroy": true
            });
            $(modal).modal('show');
        }, error: function (error) {
            console.error("Ha ocurrido un error al intentar obtener los movimientos ")
            console.log(error)
        }
    });
}