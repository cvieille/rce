﻿let paciente = {}
let validarFonasa = false;

$(document).ready(async function () {

    //collapseUserControlPaciente
    $('#collapseUserControlPaciente').on('hidden.bs.collapse', function () {
        if ($("#txtnombrePac").val() !== "")
            $("#txtNombrePacienteCollapsive").text(`Nombre: ${$("#txtnombrePac").val()} ${$("#txtApePat").val()} ${$("#txtApeMat").val()}`)
    })
    $('#collapseUserControlPaciente').on('shown.bs.collapse', function () {//txtApePat
        $("#txtNombrePacienteCollapsive").empty()
    })

    const sSession = getSession();
    DeshabilitarPaciente(true);
    //Funcion CargarComboIdentificadores("#sltIdentificacion") duplicada en comboPersonas
    //Nuva forma de hacerlo: comboIdentificacion("#sltIdentificacion")
    await CargarComboIdentificadoresPaciente("#sltTipoIdentificacionBusqueda");
    await CargarComboIdentificadoresPaciente("#sltIdentificacion");

    CargarComboSexosPacientes();
    CargarComboGenerosPacientes();
    ComboNacionalidad("#sltNacionalidad")

    await CargarComboPrevisionTramoPac("#sltPrevision", "#sltPrevisionTramo")

    $("#divPacienteAcompañante, #divAcompañante").hide();
    $("#lnbEvento").hide();
    await $("#chkAcompañante").bootstrapSwitch();

    // VERIFICA SI EL PERFIL ES MÉDICO 
    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico)
        $("#txtFecNacPac, #txtDireccion, #txtTelefono").attr("data-required", false);

    $("#txtnumerotPac").on('blur', function () {
        TxtRutPac_Blur();
    });

    $("#sltIdentificacion").on('change', function () {
        SltIdentificacion_Change();
    })

    //ocultarMostrarFichaPac($("#sltIdentificacion"), $("#txtnumerotPac"), SltIdentificacion_Change)

    //$("#sltgeneroPac").on('change', function () {
    //    SltgeneroPac_Change();
    //});
    $("#txtnumerotPac").on('input', function () {
        var valorInput = document.getElementById('txtnumerotPac').value;
        var cantidadLetras = valorInput.replace(/\s/g, '').length;
        var digitoInput = document.getElementById('txtDigitoPac').value;

        if (cantidadLetras >= 7 && digitoInput !== '') {
            if ($(this).val() === '')
                $("#txtNuiPac").val("")

            // 1 = RUT
            // 4 = RUT MATERNO
            if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
                LimpiarPaciente();
                $("#txtDigitoPac").val("");
                DeshabilitarPaciente(true);
                //$("#btnCargarPrevisionFonasaPaciente, #sltPrevisionTramo").prop("disabled", true)
            }
        } 
    });

    $("#txtDigitoPac").on('input', function () {
        TxtDigitoPac_Input();
    });
    $('#btnNuevoPacienteRN').click(function (e) {
        DeshabilitarPaciente(false);
        $('#mdlRutMaterno').modal('hide');
        $('#cardPacienteDuplicado').slideUp();
        LimpiarPaciente();
    });

    // Esto debe quedar al final de la función
    const verificarFonasa = await getverificarFonasa()
    validarFonasa = JSON.parse(verificarFonasa.toLowerCase())
    if (!validarFonasa)
        $("#btnCargarPrevisionFonasaPaciente").hide()

});

async function CargarComboPrevisionTramoPac(prevision, tramo) {

    ComboPrevision()

    $(prevision).change(function () {
        CargarComboHijo(prevision, tramo, ComboTramo);
    });
}


// Carga de combos
function CargarCombosUbicacionPaciente() {

    // PAÍS
    if ($("#sltPais").length > 0) {
        ComboPais();
        $("#sltPais").on('change', function () {
            CargarComboHijo("#sltPais", "#sltRegion", ComboRegion);
            CargarComboHijo("#sltRegion", "#sltProvincia", ComboProvincia);
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboComuna);
        });
    }

    // REGIÓN
    if ($("#sltRegion").length > 0) {
        
        $("#sltRegion").append("<option value='0'>Seleccione</option>");
        $("#sltRegion").change(function () {
            CargarComboHijo("#sltRegion", "#sltProvincia", ComboProvincia);
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboComuna);
        });
    }

    // PROVINCIA
    if ($("#sltProvincia").length > 0) {
        
        $("#sltProvincia").append("<option value='0'>Seleccione</option>");
        $("#sltProvincia").change(function () {
            CargarComboHijo("#sltProvincia", "#sltCiudad", ComboComuna);
        });
    }

    if ($("#sltCiudad").length > 0) {
        
        $("#sltCiudad").append("<option value='0'>Seleccione</option>");
    }
    setearPaisRegionPorDefecto()
}


function setearPaisRegionPorDefecto() {
    $("#sltPais").val(1).trigger('change')
    $("#sltRegion").val(14).trigger('change')
    $("#sltProvincia").val(45).trigger('change')

    //$("#sltRegion").attr('disabled', 'disabled');
    $("#sltProvincia").attr('disabled', 'disabled');
    //$("#sltCiudad").attr('disabled', 'disabled');
}

async function CargarComboIdentificadoresPaciente(element) {
    $(element).empty()
    const url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    setCargarDataEnCombo(url, false, element);
    $(element).val("1")
}

function ComboNacionalidad(element) {
    $(element).empty()
    const url = `${GetWebApiUrl()}GEN_Nacionalidad/Combo`;
    setCargarDataEnCombo(url, false, element);
}

// Oyentes 
function TxtRutPac_Blur() {

    if ($("#sltIdentificacion").val() === '2' || $("#sltIdentificacion").val() === '3' && EsValidaSesionToken($("#txtnumerotPac").val())) {
        if ($.trim($("#txtnumerotPac").val()) !== '') {
            buscarPacientePorDocumento($("#sltIdentificacion").val(), $.trim($("#txtnumerotPac").val()));
            if (paciente.GEN_idPaciente == undefined)
                LimpiarPaciente();
            $(".md-form .form").addClass("active");
            DeshabilitarPaciente(false);
        } else {
            LimpiarPaciente();
            DeshabilitarPaciente(true);
        }
    }
}
function TxtRutPac_Input() {
    LimpiarPaciente();
    $("#txtDigitoPac").val('');
    $("#txtnumerotPac").next().addClass("active");
    DeshabilitarPaciente(true);
}
async function TxtDigitoPac_Input() {

    LimpiarPaciente();
    DeshabilitarPaciente(true);

    //$("#btnCargarPrevisionFonasaPaciente").prop("disabled", true)

    if ($.trim($("#txtDigitoPac").val()) != "" && EsValidaSesionToken($("#txtDigitoPac"))) {

        // 1 = RUT
        // 4 = RUT MATERNO
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {

            // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
            let esRutCorrecto = await EsValidoDigitoVerificador($("#txtnumerotPac").val(), $("#txtDigitoPac").val());

            // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA 
            //TRAER LA INFO DEL PACIENTE

            if (esRutCorrecto) {

                buscarPacientePorDocumento($("#sltIdentificacion").val(), $("#txtnumerotPac").val());
                DeshabilitarPaciente(false);
                $("#cargarPrevisionApiFonasa").prop("disabled", false)

            } else {
                $("#cargarPrevisionApiFonasa").prop("disabled", true)

                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'RUT Incorrecto',
                    showConfirmButton: false,
                    timer: 1000,
                    allowOutsideClick: false
                });

            }
        }
    }

}

async function SltIdentificacion_Change() {

    $("#txtnumerotPac, #txtDigitoPac").val('');
    $("#txtnumerotPac").removeAttr("disabled");
    LimpiarPaciente();
    DeshabilitarPaciente(true);

    if (validarFonasa) 
        $("#btnCargarPrevisionFonasaPaciente").show()
        

    switch ($("#sltIdentificacion").val()) {
        case '2':
        case '3':
        case '5':
            $(".digito").hide();
            $("#txtnumerotPac").attr("maxlength", 15);
            break;
        default:
            $(".digito").show();
            $("#txtnumerotPac").attr("maxlength", 8);
            break;
    }

    //if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
    //    if (validarFonasa)
    //        $("#btnCargarPrevisionFonasaPaciente").show()
    //}
    //else {
    //     if (!validarFonasa)
    //        $("#btnCargarPrevisionFonasaPaciente").hide()
    //}

    const sSession = getSession();

    if ($("#sltIdentificacion").val() === '5') {

        paciente = {};

        DeshabilitarPaciente(false);
        $("#txtnumerotPac").attr("disabled", "disabled");
        $("#divDatosPaciente input[data-required='true'], " +
            "#divDatosPaciente select[data-required='true']").not("#sltsexoPac, #sltgeneroPac").attr("data-required", "false");

    } else if ($("#sltIdentificacion").val() === '4') {
        $("#txtApePat").attr("data-required", false);
    }
    else {

        $("#divDatosPaciente input[data-required='false'], " +
            "#divDatosPaciente select[data-required='false']").attr("data-required", "true");

        if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico)
            $("#txtFecNacPac, #txtDireccion, #txtTelefono").attr("data-required", "false");
    }

    if ($("#sltIdentificacion").val() == '2' || $("#sltIdentificacion").val() == '3') {

        await sleep(200)
        $("#sltPrevision").val(3).trigger("click").change()
        $("#sltPrevisionTramo").val(11).trigger("click").change()
        
    }

    if ($("#sltIdentificacion").val() === '5') {
        $("#sltsexoPac").val("4")
        //$("#sltsexoPac").attr("data-required", true)
        $("#sltgeneroPac").val("7")
        //$("#sltgeneroPac").attr("data-required", true)
    }

    if ($("#sltIdentificacion").val() === '5') {
        $("#sltsexoPac").val("4")
        //$("#sltsexoPac").attr("data-required", true)
        $("#sltgeneroPac").val("7")
        //$("#sltgeneroPac").attr("data-required", true)
    }
    else {

        $("#sltsexoPac").val("0")
       // $("#sltsexoPac").attr("data-required", false)
        $("#sltgeneroPac").val("0")
        //$("#sltgeneroPac").val("0").attr("data-required", false)
    }

    ReiniciarRequired();
}
function SltgeneroPac_Change() {

    //1 = Hombre
    //2 = Mujer
    //3 = Intersex
    //4 = Desconocido

    if (($("#sltgeneroPac").val() != '0') &&
        (($("#sltsexoPac").val() == '1' && $("#sltgeneroPac").val() != '2') ||
            ($("#sltsexoPac").val() == '2' && $("#sltgeneroPac").val() != '1'))) {

        $("#txtNombreSocial").parent().show();
        //$("#txtNombreSocial").attr("data-required", true);

    } else if ($("#sltsexoPac").val() == '3' && $("#sltgeneroPac").val() != '0') {

        $("#txtNombreSocial").parent().show();
        //$("#txtNombreSocial").attr("data-required", false);

    } else {

        //$("#txtNombreSocial").parent().hide();
        //$("#txtNombreSocial").attr("data-required", false);

    }

    ReiniciarRequired();

}
function TxtFecNacPac_CalculaEdad() {

    let fechaActual = moment(moment()).format("YYYY-MM-DD");
    let fechaNacimiento = moment(moment($("#txtFecNacPac").val()).toDate()).format("YYYY-MM-DD");

    if (moment(fechaActual).isSameOrAfter(fechaNacimiento) && EsValidaSesionToken($("#txtFecNacPac"))) {
        $("#txtEdadPac").val(CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).edad);
        let years = CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).años;
        if (years > 120) {
            toastr.error("Fecha de Nacimiento de Paciente no es valida");
            $("#txtEdadPac").val('');
            $("#txtFecNacPac").val('');
        }
    } else {
        $("#txtEdadPac").val('');
        $("#txtFecNacPac").val('');
    }
}

// Acciones en paciente
function BloquearPaciente() {
    $("#divDatosPaciente select, #divDatosPaciente input").not("#divPrevisionPaciente").attr("disabled", "disabled");
    //$("#chkAcompañante").bootstrapSwitch('disabled', true);
    $("#chkAcompañante").removeAttr("disabled");
    ValidarRequired(false);
}
function ValidarRequired(bEstado) {
    $("#divDatosPaciente input[data-required], #divDatosPaciente select[data-required]").attr("data-required", bEstado);
}
function LimpiarPaciente() {
    $("#txtNombrePacienteCollapsive").text('')
    $("#txtNuiPac, #txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial").val('');
    $("#txtFecNacPac, #txtEdadPac, #txtDireccion, #txtNumDireccionPaciente").val('');
    $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").val('');
    $("#sltsexoPac, #sltgeneroPac").val('0');
    $("#chkAcompañante").bootstrapSwitch('disabled', false);
    $("#chkAcompañante").bootstrapSwitch('state', false);

    $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());
    $(".md-form .form").removeClass("active");
    $("#lnbEvento").hide();
    paciente = {};

    if ($("#tblHospitalizaciones").length > 0) {
        if ($("#tblHospitalizaciones tbody").length > 0) {
            $("#tblHospitalizaciones").hide();
            $("#divHospitalizaciones").show();
        }
    }

    if ($("#tblVacunasPaciente").length > 0) {
        //$("#aAgregarPaciente, #sltVacunasPaciente").attr("disabled", "disabled");
        //$("#aAgregarPaciente").addClass("disabled");
        if ($("#tblVacunasPaciente tbody").length > 0) {
            $("#tblVacunasPaciente").hide();
            $("#divVacunasPaciente").show();
        }
    }
    setearPaisRegionPorDefecto()
    //if ($("#sltPais").length > 0) {
    //    $("#sltPais").val('0');
    //}
    //if ($("#sltRegion").length > 0) {
    //    $("#sltRegion").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    //}
    //if ($("#sltProvincia").length > 0) {
    //    $("#sltProvincia").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    //}
    //if ($("#sltCiudad").length > 0) {
    //    $("#sltCiudad").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    //}
    if ($("#sltPrevision").length > 0) {
        $("#sltPrevision").val('0');
    }
    if ($("#sltPrevisionTramo").length > 0) {
        $("#sltPrevisionTramo").empty().append("<option value='0'>-Seleccione-</option>").val(0);
    }

}

function GetPaciente() {
    return paciente;
}
function DeshabilitarPaciente(esDisabled) {

    if (!esDisabled) {

        $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial, #txtNuiPac").removeAttr("disabled");
        $("#txtFecNacPac, #txtDireccion, #txtNumDireccionPaciente").removeAttr("disabled");
        $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").removeAttr("disabled");
        $("#sltsexoPac, #sltgeneroPac").removeAttr('disabled');
        $("#chkAcompañante").bootstrapSwitch('disabled', false);

        if ($("#sltNacionalidad").length > 0)
            $("#sltNacionalidad").removeAttr("disabled");

        if ($("#sltPais").length > 0)
            $("#sltPais").removeAttr("disabled");

        if ($("#sltRegion").length > 0)
            $("#sltRegion").removeAttr('disabled');

        if ($("#sltProvincia").length > 0)
            $("#sltProvincia").removeAttr('disabled');

        if ($("#sltCiudad").length > 0)
            $("#sltCiudad").removeAttr('disabled');

        if ($("#sltPrevision").length > 0)
            $("#sltPrevision").removeAttr('disabled');

        if ($("#sltPrevisionTramo").length > 0)
            $("#sltPrevisionTramo").removeAttr('disabled');

    } else {

        $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial, #txtNuiPac").attr("disabled", "disabled");
        $("#txtFecNacPac, #txtDireccion, #txtNumDireccionPaciente").attr("disabled", "disabled");
        $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico").attr("disabled", "disabled");
        $("#sltsexoPac, #sltgeneroPac").attr("disabled", "disabled");

        if ($("#sltNacionalidad").length > 0)
            $("#sltNacionalidad").attr("disabled", "disabled");

        if ($("#sltPais").length > 0)
            $("#sltPais").attr("disabled", "disabled");

        if ($("#sltRegion").length > 0)
            $("#sltRegion").attr("disabled", "disabled");

        if ($("#sltProvincia").length > 0)
            $("#sltProvincia").attr("disabled", "disabled");

        if ($("#sltCiudad").length > 0)
            $("#sltCiudad").attr("disabled", "disabled");

        //if ($("#sltPrevision").length > 0)
        //    $("#sltPrevision").attr("disabled", "disabled");

        //if ($("#sltPrevisionTramo").length > 0)
        //    $("#sltPrevisionTramo").attr("disabled", "disabled");
    }
}

function quitarRequiredPaciente() {
    $("#txtnombrePac, #txtApePat, #txtApeMat, #txtNombreSocial, #txtNuiPac, #sltNacionalidad").attr("data-required", false);
    $("#txtFecNacPac, #txtDireccion, #txtNumDireccionPaciente, #sltPrevision").attr("data-required", false);
    $("#txtTelefono, #txtOtroTelefono, #txtCorreoElectronico, #sltPrevisionTramo").attr("data-required", false);
    $("#sltsexoPac, #sltgeneroPac, #sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("data-required", false);
}

// Obtener Información de Paciente

function trasformarVistaPacienteAJSON() {

    paciente = {
        IdIdentificacion: valCampo(parseInt($("#sltIdentificacion").val())),
        NumeroDocumento: valCampo($("#txtnumerotPac").val()),
        Digito: valCampo($("#txtDigitoPac").val()),
        Nombre: valCampo($("#txtnombrePac").val()),
        ApellidoPaterno: valCampo($("#txtApePat").val()),
        ApellidoMaterno: valCampo($("#txtApeMat").val()),
        Nui: $("#txtNuiPac").val(),
        DireccionCalle: valCampo($("#txtDireccion").val()),
        Telefono: valCampo($("#txtTelefono").val()),
        IdSexo: valCampo(parseInt($("#sltsexoPac").val())),
        IdTipoGenero: valCampo(parseInt($("#sltgeneroPac").val())),
        NombreSocial: valCampo($("#txtNombreSocial").val()),
        FechaNacimiento: valCampo($("#txtFecNacPac").val()),
        OtrosTelefonos: valCampo($("#txtOtroTelefono").val()),
        Email: valCampo($("#txtCorreoElectronico").val()),
        DireccionNumero: valCampo($("#txtNumDireccionPaciente").val())
    }

    if ($("#sltNacionalidad").length > 0)
        paciente.IdNacionalidad = valCampo(parseInt($("#sltNacionalidad").val()));
    if ($("#sltPais").length > 0)
        paciente.IdPais = valCampo(parseInt($("#sltPais").val()));
    if ($("#sltRegion").length > 0)
        paciente.IdRegion = valCampo(parseInt($("#sltRegion").val()));
    if ($("#sltProvincia").length > 0)
        paciente.IdProvincia = valCampo(parseInt($("#sltProvincia").val()));
    if ($("#sltCiudad").length > 0)
        paciente.IdComuna = valCampo(parseInt($("#sltCiudad").val()));
    if ($("#sltPuebloOriginario").length > 0)
        paciente.IdPuebloOriginario = $("#sltPuebloOriginario").val() == 0 ? null : $("#sltPuebloOriginario").val();
    if ($("#sltCategoriaOcupacional").length > 0)
        paciente.IdTipoCategoriaOcupacion = $("#sltCategoriaOcupacional").val() == 0 ? null : $("#sltCategoriaOcupacional").val();
    if ($("#sltOcupacion").length > 0)
        paciente.IdTipoOcupaciones = $("#sltOcupacion").val() == 0 ? null : $("#sltOcupacion").val();
    if ($("#sltEscolaridad").length > 0)
        paciente.IdTipoEscolaridad = $("#sltEscolaridad").val() == 0 ? null : $("#sltEscolaridad").val();
    if ($("#sltPrevision").length > 0)
        paciente.IdPrevision = $("#sltPrevision").val() == 0 ? null : Number($("#sltPrevision").val());
    if ($("#sltPrevisionTramo").length > 0)
        paciente.IdPrevisionTramo = $("#sltPrevisionTramo").val() == 0 ? null : Number($("#sltPrevisionTramo").val());

    return paciente;
}
function cargarPacientePorId(idPaciente) {

    ShowModalCargando(true)
    let url = `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            CargarPaciente(data);
            ShowModalCargando(false)
        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });
}
async function buscarPacientePorDocumento(idIdentificacion, numero) {

    if (numero.length > 0) {

        let url = `${GetWebApiUrl()}GEN_Paciente/Buscar?numeroDocumento=${numero}&idIdentificacion=${idIdentificacion}`
        ShowModalCargando(true);

        $.ajax({
            type: 'GET',
            url: url,
            success: async function (data) {

                ShowModalCargando(false);

                if (idIdentificacion == 4) {
                    tablaRutMaterno(idIdentificacion, numero);
                } else {

                    CargarPaciente(data[0]);
                    DeshabilitarPaciente(false);

                    let verificarFonasa = await getverificarFonasa();
                    validarFonasa = JSON.parse(verificarFonasa.toLowerCase());
                    if (validarFonasa) {

                        if ($.isEmptyObject(data[0]) && idIdentificacion == 1) {

                            Swal.fire({
                                title: 'Paciente Nuevo',
                                text: 'El paciente no existe, ¿Desea buscar desde Fonasa? ',
                                icon: 'info',
                                showCancelButton: true,
                                confirmButtonText: 'Buscar en Fonasa',
                                cancelButtonText: 'Cancelar',
                            }).then(async (result) => {

                                ShowModalCargando(true);
                                if (result.value) {
                                    const paciente = await getJsonDesdeFonasa(numero);
                                    if (paciente != null)
                                        CargarPaciente(paciente);
                                }
                                ShowModalCargando(false);

                            });
                        }
                    }

                    
                }

            },
            error: function (jqXHR, status) {
                if (jqXHR.status == 404) {
                    LimpiarPaciente();
                    $("#txtnumerotPac, #txtDigitoPac").val("");
                    DeshabilitarPaciente(true);
                } else
                    console.log(`Error al cargar JSON de Persona: ${JSON.stringify(request)}`);
                //
                console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
            }
        });

    }

    //return encontrado
}
async function getJsonDesdeFonasa(numeroDocumento) {

    let paciente = null;

    await $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}GEN_Paciente/${numeroDocumento}/Fonasa`,
        success: function (data) {
            paciente = data;
        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente Fonasa: ${JSON.stringify(jqXHR)}`);
            Swal.fire({
                position: 'center',
                icon: 'warning',
                title: `No se ha encontrado paciente.`,
                showConfirmButton: false,
                timer: 1500
            });
            ShowModalCargando(false);
        }
    });

    return paciente;

}
function hospitalizacionesPreviasPaciente(id = null) {
    const ID = id || paciente.GEN_idPaciente;
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${ID}`;
    if (!id) {
        url += `&idTipoEstado=87&idTipoEstado=88`;
    }
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            return data;
        },
        error: function (jqXHR, status) {
            if (jqXHR.status != 401) {
                console.log(`Error al cargar JSON: ${JSON.stringify(request)}`);
            }
            console.log(`Error al buscar Hospitalización ${JSON.stringify(jqXHR)}`);
        }
    });
}

function ocultarMostrarFichaPaciente(identificador, rut,) {

    const valor = $(identificador).val();
    const digitoContainer = $(".digito");
    const maxLength = (valor === '2' || valor === '3' || valor === '5') ? 15 : 8;
    const placeholder = (valor === '2' || valor === '3' || valor === '5') ? "Ficha Clínica" : "";

    digitoContainer.toggle(!(valor === '2' || valor === '3' || valor === '5'));
    $(rut).attr("maxlength", maxLength);
    $(rut).attr("placeholder", placeholder);

}
function CargarPaciente(pac) {

    let id = 0;
    if ($.isEmptyObject(pac)) {

        //si no existe en base de datos, termina limpiando input 
        //para crear paciente nuevo.
        toastr.info('Se está creando un nuevo paciente.');

        if ($("#sltIdentificacion").val() === "1")
            $("#btnCargarPrevisionFonasaPaciente").show()
        else
            $("#btnCargarPrevisionFonasaPaciente").hide()

        if (pac === undefined) {
            $("#sltNacionalidad").val(42)
            $("#sltPais").val(1).change()
            $("#sltRegion").val(14).change()
            $("#sltRegion").val(14).change()
            $("#sltProvincia").val(45).change()
            $("#sltCiudad").val(284).change()
        }

        paciente = {};
        if ($("#tblVacunasPaciente").length > 0) {
            //$("#aAgregarPaciente, #sltVacunasPaciente").removeAttr("disabled");
            //$("#aAgregarPaciente").removeClass("disabled");
        }
        $("#divSiTieneHospitalizaciones").prop("style", "display: none;");
        $("#divNoTieneHospitalizaciones").fadeIn();
    }
    else {

        paciente = {};
        paciente.GEN_idPaciente = pac.IdPaciente;
        establecerValor("txtIdPaciente", pac.IdPaciente)
        establecerValor("sltIdentificacion", pac.Identificacion.Id)
        $("#sltIdentificacion").val(pac.Identificacion.Id)
        ocultarMostrarFichaPaciente("#sltIdentificacion", "#txtnumerotPac")
        establecerValor("txtnumerotPac", pac.NumeroDocumento)
        establecerValor("txtDigitoPac", pac.Digito)
        establecerValor("txtNuiPac", pac.Nui)
        establecerValor("txtnombrePac", pac.Nombre)
        establecerValor("txtApePat", pac.ApellidoPaterno)
        establecerValor("txtApeMat", pac.ApellidoMaterno)
        establecerValor("txtDireccion", pac.DireccionCalle)
        establecerValor("txtNumDireccionPaciente", pac.DireccionNumero)
        establecerValor("txtTelefono", pac.Telefono)
        establecerValor("txtNombreSocial", pac.NombreSocial)
        establecerValor("txtOtroTelefono", pac.OtrosFonos)
        establecerValor("txtCorreoElectronico", pac.Email)

        if (pac.Sexo !== null)
            ValTipoCampo($("#sltsexoPac"), pac.Sexo.Id);

        if (pac.Genero !== null)
            ValTipoCampo($("#sltgeneroPac"), pac.Genero.Id);

        SltgeneroPac_Change();

        if (pac.FechaNacimiento != null) {
            $("#txtFecNacPac").val(moment(pac.FechaNacimiento).format('YYYY-MM-DD'));
            $("#txtEdadPac").val(pac.Edad.edad);
        }

        $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());

        //DATOS EXTRAS
        if ($("#sltPais").length > 0) {
            if (pac.Pais != null)
                $("#sltPais").val(pac.Pais.Id).change();
            
        }

        if ($("#sltRegion").length > 0) {
            if (pac.Region != null)
                $("#sltRegion").val(pac.Region.Id).change();
            
        }

        if ($("#sltProvincia").length > 0) {
            if (pac.Provincia != null)
                $("#sltProvincia").val(pac.Provincia.Id).change();
            
        }

        if ($("#sltCiudad").length > 0) {
            if (pac.Comuna != null)
                $("#sltCiudad").val(pac.Comuna.Id);
            
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.Nacionalidad != null)
                $("#sltPuebloOriginario").val(pac.Nacionalidad.Id);
            else
                $("#sltPuebloOriginario").val(0);
        }

        if ($("#sltPuebloOriginario").length > 0) {
            if (pac.PuebloOriginario != null)
                $("#sltPuebloOriginario").val(pac.PuebloOriginario.Id);
            else
                $("#sltPuebloOriginario").val(0);
        }


        if ($("#sltCategoriaOcupacional").length > 0) {
            if (pac.TipoCategoriaOcupacion != null)
                $("#sltCategoriaOcupacional").val(pac.TipoCategoriaOcupacion.Id);
            else
                $("#sltCategoriaOcupacional").val(0);
        }

        if ($("#sltOcupacion").length > 0) {
            if (pac.Ocupaciones != null)
                $("#sltOcupacion").val(pac.Ocupaciones.Id);
            else
                $("#sltOcupacion").val(0);
        }

        if ($("#sltEscolaridad").length > 0) { //nivelEducacional
            if (pac.NivelEducacional != null)
                $("#sltEscolaridad").val(pac.NivelEducacional.Id);
            else
                $("#sltEscolaridad").val(0);
        }

        if ($("#sltPrevision").length > 0) {

            if (pac.Prevision !== null)
                $("#sltPrevision").val(pac.Prevision.Id);
            else
                $("#sltPrevision").val(0).change();

        }

        if ($("#sltPrevisionTramo").length > 0) {

            if (pac.PrevisionTramo !== null) {
                CargarComboHijo("#sltPrevision", "#sltPrevisionTramo", () => {
                    ComboTramo($("#sltPrevisionTramo"), $("#sltPrevision").val());
                    $("#sltPrevisionTramo").val(pac.PrevisionTramo.Id);
                });
            }
            else
                $("#sltPrevisionTramo").val(0);
        }

        if ($("#sltTipoAtencion").length > 0) {
            if (pac.FechaNacimiento != null) {
                let anhos = CalcularEdad(pac.FechaNacimiento).años;
                if (anhos < 15) {
                    $("#sltTipoAtencion").val(3).change();
                    $("#msjPediatrica").prop("style", "display: block;");
                    $("#msjAdulto").prop("style", "display: none;");
                } else {
                    $("#msjAdulto").prop("style", "display: block;");
                    $("#msjPediatrica").prop("style", "display: none;");
                }
            }
        }

        valCampo($("#sltNacionalidad").val(pac.Nacionalidad?.Id ?? "0"))

        //if ($("#sltNacionalidad").length > 0) {
        //    if (pac.Nacionalidad != null)
        //        $("#sltNacionalidad").val(pac.Nacionalidad.Id);
        //    else
        //        $("#sltNacionaliad").val(0);
        //}

        $(".md-form .form").addClass("active");
    }


}
function CargarPacienteActualizado(idPaciente) {
    url = `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`

    //Inicio
    try {

        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function (data, status, jqXHR) {

                cargarDataPacienteActualizado(data)
                //pac = data;
            },
            error: function (jqXHR, status) {
                ShowModalCargando(false)
                console.log("Error al cargar paciente: " + jqXHR.responseText);
                return
            }
        });

    } catch (err) {
        alert(err.message);
        return
    }
}

function cargarDataPacienteActualizado(pac) {

    paciente = {};

    //FIN
    if (pac.Identificacion != null)
        $("#sltIdentificacion").val(pac.Identificacion.Id);

    SltIdentificacion_Change();

    paciente = GetJsonPacienteNuevo(pac);
    paciente.GEN_idPaciente = pac.IdPaciente;
    id = pac.IdPaciente;
    $("#txtnumerotPac").val(pac.NumeroDocumento);
    $("#txtDigitoPac").val(pac.Digito);
    $("#txtnombrePac").val(pac.Nombre);
    $("#txtApePat").val(pac.ApellidoPaterno);
    $("#txtApeMat").val(pac.ApellidoMaterno);
    $("#txtDireccion").val(pac.DireccionCalle);
    $("#txtNumDireccionPaciente").val(pac.DireccionNumero);
    $("#txtTelefono").val(pac.Telefono);
    $("#txtNuiPac").val(pac.Nui);

    if (pac.Sexo !== null)
        ValTipoCampo($("#sltsexoPac"), pac.Sexo.Id);
    if (pac.Genero)
        ValTipoCampo($("#sltgeneroPac"), pac.Genero.Id);

    $("#txtNombreSocial").val(pac.NombreSocial);

    SltgeneroPac_Change();

    if (pac.FechaNacimiento != null) {
        $("#txtFecNacPac").val(moment(pac.FechaNacimiento).format('YYYY-MM-DD'));
        let edad = CalcularEdad(moment($("#txtFecNacPac").val()).format("YYYY-MM-DD")).edad;
        $("#txtEdadPac").val(edad);
    }

    $("#txtOtroTelefono").val(pac.OtrosTelefonos);
    $("#txtCorreoElectronico").val(pac.Email);
    $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());

    //DATOS EXTRAS
    if ($("#sltPais").length > 0) {
        $("#sltPais").val(pac.Pais !== null ? pac.Pais.Id : "0").change();
    }

    if ($("#sltRegion").length > 0) {
        $("#sltRegion").val(pac.Region !== null ? pac.Region.Id : "0").change();
    }

    if ($("#sltProvincia").length > 0) {
        $("#sltProvincia").val(pac.Provincia !== null ? pac.Provincia.Id : "0").change();
    }

    if ($("#sltCiudad").length > 0) {
        $("#sltCiudad").val(pac.Comuna !== null ? pac.Comuna.Id : "0");
    }

    if ($("#sltNacionalidad").length > 0) {
        $("#sltNacionalidad").val(pac.Nacionalidad !== null ? pac.Nacionalidad.Id : "0");
    }

    if ($("#sltPuebloOriginario").length > 0) {
        $("#sltPuebloOriginario").val(pac.PuebloOriginario !== null ? pac.PuebloOriginario.Id : "0");
    }

    if ($("#sltCategoriaOcupacional").length > 0) {
        $("#sltCategoriaOcupacional").val(pac.TipoCategoriaOcupacion ?? "0");
    }

    if ($("#sltOcupacion").length > 0)
        $("#sltOcupacion").val(pac.IdTipoOcupaciones ?? "0");

    if ($("#sltEscolaridad").length > 0) {
        $("#sltEscolaridad").val(pac.IdTipoEscolaridad ?? "0");

        if (pac.IdTipoEscolaridad != null)
            $("#sltEscolaridad").val(pac.IdTipoEscolaridad);
        else
            $("#sltEscolaridad").val(0);
    }

    if ($("#sltPrevision").length > 0) {
        if (pac.Prevision !== null)
            $("#sltPrevision").val((pac.Prevision !== null || pac.PrevisionTramo != null) ? pac.Prevision.Id : "0").change()
    }

    if ($("#sltPrevisionTramo").length > 0)
        $("#sltPrevisionTramo").val((pac.Prevision !== null && pac.PrevisionTramo != null) ? pac.PrevisionTramo.Id : "0");

    if ($("#sltTipoAtencion").length > 0) {
        if (pac.FechaNacimiento != null) {
            let anhos = CalcularEdad(pac.FechaNacimiento).años;
            if (anhos < 15) {
                $("#sltTipoAtencion").val(3).change();
                $("#msjPediatrica").prop("style", "display: block;");
                $("#msjAdulto").prop("style", "display: none;");
            } else {
                $("#msjAdulto").prop("style", "display: block;");
                $("#msjPediatrica").prop("style", "display: none;");
            }
        }
    }
    $(".md-form .form").addClass("active");
}


async function buscarPacientePorId(idPaciente) {

    const paciente = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`,
        async: false,
        success: function (data, status, jqXHR) {
            return data;
        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });

    return paciente
}

async function buscarPacientePorDocumentoEIdentificacion(numeroDocumento, idIdentificacion) {

    let paciente = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?numeroDocumento=${numeroDocumento}&idIdentificacion=${idIdentificacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            return data
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    });
    //alert(paciente)
    return paciente
}

async function GuardarPaciente() {

    let pacienteJson = {}, pac
    let idPaciente = 0
    let parametrizacion = {}

    if (paciente.GEN_idPaciente !== undefined) {
        pac = await buscarPacientePorId(paciente.GEN_idPaciente);
    } else {
        pac = ($("#sltIdentificacion").val() != 5) ?
            await buscarPacientePorDocumentoEIdentificacion($("#txtnumerotPac").val(), $("#sltIdentificacion").val())
            : [];
    }

    pacienteJson = pac;

    //Encontro paciente
    if (pac.IdPaciente !== undefined) {

        //const { IdPaciente } = pac[0]
        parametrizacion = {
            method: "PUT",
            url: `${GetWebApiUrl()}GEN_Paciente/${pac.IdPaciente}`
        }

        pacienteJson = trasformarVistaPacienteAJSON();
        pacienteJson.Id = pac.IdPaciente

    } else {

        parametrizacion = {
            method: "POST",
            url: `${GetWebApiUrl()}GEN_Paciente`
        }
        pacienteJson = trasformarVistaPacienteAJSON();

    }

    await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(pacienteJson),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            if (data === undefined)
                idPaciente = pacienteJson.Id
            else if (status === 'success')
                idPaciente = data.Id
            else
                console.error("Error al guardar Paciente")
        },
        error: async function (jqXHR, status) {
            console.log(`Error al guardar Paciente: ${JSON.stringify(jqXHR)} `)
        }
    })

    return idPaciente;

}

function tablaRutMaterno(idIdentificacion, numeroDocumento) {

    const url = `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=${idIdentificacion}&numeroDocumento=${numeroDocumento}`;


    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {

            $('#tblListadoPacientes').empty()
            var adataset = [];
            $.each(data, function (i, r) {
                adataset.push([
                    r.IdPaciente,
                    r.Nombre,
                    r.ApellidoPaterno,
                    r.ApellidoMaterno,
                    moment(r.FechaNacimiento).format('DD-MM-YYYY')
                ]);
            });

            $('#tblListadoPacientes').DataTable({
                data: adataset,
                order: [],
                columns: [
                    { title: 'ID paciente' },
                    { title: 'Nombre' },
                    { title: 'Primer Apellido' },
                    { title: 'Segundo Apellido' },
                    { title: 'Fecha de nacimiento' },
                    { title: '' }
                ],
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            let botones;
                            botones = `<a id = 'linkseleccionar' data-id='${adataset[fila][0]}' class='btn btn-success' onclick = 'linkseleccionar(this)' > <i class='fa fa-edit'></i> Seleccionar</a >`;
                            return botones;
                        }
                    },
                ],
                bDestroy: true
            });
            //$('#mdlRutMaterno').modal('show');
            $('#cardPacienteDuplicado').slideDown();
            if (data.length > 0)
                $('#collapseUserControlPaciente').collapse('hide');
            else {
                $('#collapseUserControlPaciente').collapse('show');
                $('#cardPacienteDuplicado').slideUp();
            }

        },
        error: function (jqXHR, status) {
            console.log(`Error al buscar paciente ${JSON.stringify(jqXHR)}`);
        }
    });
}

// RUT MATERNO
function linkseleccionar(e) {
    var id = $(e).data('id');
    cargarPacientePorId(id);
    DeshabilitarPaciente(false);
    $('#mdlRutMaterno').modal('hide');
    $('#cardPacienteDuplicado').slideUp();
    $('#collapseUserControlPaciente').collapse('show');
}

// ACOMPAÑANTE
function ActivarAcompañante() {
    $("#divPacienteAcompañante").show();
}

function getPrevisionPrevisionTramo(prevision = "", tramo = "") {
    $("#sltPrevision").val(prevision)
    $("#sltPrevisionTramo").val(tramo)
}

async function buscarPacientePorIdentificacionYNumDocumento(idIdentificacion, numeroDocumento) {

    let url = `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${idIdentificacion}/${numeroDocumento}`
    const paciente = await $.ajax({
        type: 'GET',
        url: url,
        success: async function (data, status, jqXHR) {

            pac = data

            const { IdPrevision, IdPrevisionTramo } = data
            getPrevisionPrevisionTramo(IdPrevision, IdPrevisionTramo);

        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
                console.log("Persona no encontrado")
            }
            //
            console.log(`Error al buscar persona ${JSON.stringify(jqXHR)}`);
        }
    });

    return paciente
}
// Obtener Información de Paciente
function GetJsonPacienteNuevo(paciente) {

    return {
        DireccionNumero: valCampo(paciente.DireccionNumero),
        DireccionRural: valCampo(paciente.GEN_dir_ruralidadPaciente),
        IdPais: valCampo(paciente.IdPais),
        IdRegion: valCampo(paciente.IdRegion),
        IdProvincia: valCampo(paciente.IdRegion),
        IdComuna: valCampo(paciente.IdComuna),
        IdPrevision: valCampo(paciente.IdPrevision),
        IdPrevisionTramo: valCampo(paciente.IdPrevisionTramo),
        Nui: valCampo(paciente.Nui),
        Prais: valCampo(paciente.Prais),
        NumeroDocumento: valCampo(paciente.NumeroDocumento),
        Digito: valCampo(paciente.Digito),
        Nombre: valCampo(paciente.Nombre),
        ApellidoPaterno: valCampo(paciente.ApellidoPaterno),
        ApellidoMaterno: valCampo(paciente.ApellidoMaterno),
        DireccionCalle: valCampo(paciente.DireccionCalle),
        Telefono: valCampo(paciente.Telefono),
        IdSexo: valCampo(paciente.IdSexo),
        IdTipoGenero: valCampo(paciente.IdTipoGenero),
        FechaNacimiento: valCampo(paciente.FechaNacimiento),
        OtrosTelefonos: valCampo(paciente.OtrosTelefonos),
        Email: valCampo(paciente.Email),
        IdIdentificacion: valCampo(paciente.GEN_idIdentificacion),
        IdEstadoConyugal: valCampo(paciente.GEN_idEstado_Conyugal),
        IdPuebloOriginario: valCampo(paciente.IdPuebloOriginario),
        IdNacionalidad: valCampo(paciente.IdNacionalidad),
        IdTipoEscolaridad: valCampo(paciente.IdTipoEscolaridad),
        IdTipoOcupaciones: valCampo(paciente.IdTipoOcupaciones),
        IdTipoCategoriaOcupacion: valCampo(paciente.IdTipoCategoriaOcupacion),
        FechaFallecimiento: valCampo(paciente.FechaFallecimiento),
        NombreSocial: valCampo(paciente.NombreSocial)
    }
}

async function cargarPrevisionApiFonasa(e) {

    $(e).addClass("click-prevision")

    const numeroDocumento = $("#txtnumerotPac").val()
    let mensaje = ""

    ShowModalCargando(true)

    const personaFonasa = await GetApiFonasa(numeroDocumento)

    if (personaFonasa === undefined) {
        mensaje = "PREVISIÓN NO DISPONIBLE"
    }
    else if (personaFonasa.coddesc === 'BLOQUEADO SIN INFORMACION')
        mensaje = "BLOQUEADO SIN INFORMACION"


    if (mensaje !== "") {
        toastr.error(mensaje)
        ShowModalCargando(false)
        return
    }

    if (personaFonasa.replyTO.estado === 0) {
        if (personaFonasa.beneficiarioTO.rutbenef === null) {
            ShowModalCargando(false)
            return
        }

        // ISAPRE Y TRAMO
        await cargarPrevisionIsapre(personaFonasa, numeroDocumento, "#sltPrevision", "#sltPrevisionTramo")

        // FONASA Y TRAMO
        await cargarPrevisionFonasa(personaFonasa, "#sltPrevision", "#sltPrevisionTramo")
    }

    ShowModalCargando(false)
}
