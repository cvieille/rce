﻿async function getDatosHospitalizacionPorId(id, mostrarAntecedentes) {
    
    if (mostrarAntecedentes)
        getInformacionClinicadePaciente(id)

    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/${id}`

    const datos = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            
            const {
                Id: Id,
                Enfermeria: { AntecedentesMorbidos, Diagnostico },
                CamaActual,
                Ingreso: {
                    Ubicacion: { Valor: DescripcionUbicacionCama },
                    Procedencia: { Valor: DescripcionProcedencia }
                },
                Dias: diasEstadiaHospitalizacion
            } = data

            json = data

            //div
            $("#txtUbicacionHospitalizacion").val(Id)
            $("#txtUbicacion").val(DescripcionUbicacionCama)
            $("#fechaHosp").val(imprimeFechaHora(data))
            $("#diasEstadia").val(diasEstadiaHospitalizacion)

            $("#camaHosp").val(CamaActual !== null ? CamaActual.Valor : "Sin información")
            $("#txtProcedencia").val(DescripcionProcedencia)
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()
        }
    });
    

    return datos
}


function getInformacionClinicadePaciente(idHospitalizacion) {
    
    $("#antecedentesClinicosUserControlHospitalizacion").slideDown()

    getIntervencionesPaciente(idHospitalizacion)
    getProcedimientosProgramados(idHospitalizacion)
    getInterconsultasPaciente(idHospitalizacion)
}

//TABLA INTERCONSULTA
function getInterconsultasPaciente(idHospitalizacion) {
    
    let adataset = []
    cantidadFilas = 0

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/RCE_Interconsulta/Buscar?idHospitalizacion=${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if (data.length != 0) {

                data.forEach((item, index) => {
                    adataset.push([
                        item.Id,
                        moment(item.Fecha).format("DD-MM-YYYY"),
                        item.Especialidad.Origen.Valor,
                        item.Especialidad.Destino.Valor,
                        item.Diagnostico,
                        item.Estado.Valor
                    ]);

                });

                if (!$.isEmptyObject(adataset)) {

                    $("#tblInterconsultas").DataTable({
                        responsive: {
                            details: false,
                            type: 'column'
                        },
                        data: adataset,
                        columns: [
                            { title: "ID" },
                            { title: "Fecha" },
                            { title: "Origen" },
                            { title: "Destino" },
                            { title: "Diagnostico", class: "overflow-auto" },
                            { title: "Estado" },
                            { title: "" }
                        ],
                        columnDefs: [
                            { responsivePriority: 5, targets: 5 }, // ESTADO
                            { responsivePriority: 4, targets: 4 }, // DIAGNOSTICO
                            { responsivePriority: 1, targets: 3 }, // DESTINO
                            { responsivePriority: 2, targets: 2 }, // ORIGEN
                            { responsivePriority: 3, targets: 1 }, // FECHA
                            { responsivePriority: 0, targets: 0 }, // ID
                            { responsivePriority: -1, targets: -1 },
                            {
                                targets: -1,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    const fila = meta.row;
                                    const boton = `<button type='button' class='btn btn-outline-primary' onclick='ImprimirApiExterno("${GetWebApiUrl()}RCE_Interconsulta/${adataset[fila][0]}/Imprimir");'>Imprimir</button>`;
                                    return boton;
                                }
                            }
                        ]
                    })
                }

                cantidadFilas = $("#tblInterconsultas").DataTable().rows().count()
                $("#cantidadInterconsultas").text(cantidadFilas)

            }
            else {
                $('#alertInterconsulta').text("No existe información para mostrar")
                $('#cantidadInterconsultas').append("No existe información para mostrar")
                $("#cantidadInterconsultas").removeClass("badge-success")
                $("#cantidadInterconsultas").addClass("badge-secondary")
                $("#cantidadInterconsultas").text(0)
            }
        },
        error: function (err) {
            console.log(JSON.stringify(err))
            $("#modalCargando").hide()
        }
    });
}

//TABLA PROCEDIMIENTOS PROGRAMADOS
const getProcedimientosProgramados = (idHospitalizacion) => {
    

    let adataset = []
    let cantidadFilas = 0

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/PAB_Formulario_Ind_Qx/Buscar/Soliciitud?idHospitalizacion=${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            if (data.length !== 0) {

                //const { IdFormulario, Diagnostico, Cirugia, FechaPropuesta, Especialidad, Estado, Modalidad } = data

                data.forEach((item, index) => {
                    adataset.push([
                        item.IdFormulario,
                        item.Diagnostico,
                        item.Cirugia,
                        moment(item.FechaPropuesta).format("DD-MM-YYYY"),
                        item.Especialidad.Valor,
                        item.Estado.Valor,
                        item.Modalidad.Valor
                    ]);

                });

                if (!$.isEmptyObject(adataset)) {

                    $("#tblProcedimientosProgramados").DataTable({
                        responsive: {
                            details: false,
                            type: 'column'
                        },
                        data: adataset,
                        columns: [
                            { title: "ID" },
                            { title: "Diagnostico", class: "overflow-auto" },
                            { title: "Cirugia" },
                            { title: "Fecha Propuesta" },
                            { title: "Especialidad" },
                            { title: "Estado" },
                            { title: "Modalidad" },
                            { title: "" }
                        ],

                        columnDefs: [
                            { responsivePriority: 6, targets: 6 }, // MODALIDAD
                            { responsivePriority: 5, targets: 5 }, // ESTADO
                            { responsivePriority: 4, targets: 4 }, // ESPECIALIDAD
                            { responsivePriority: 1, targets: 3 }, // FECHA PROPUESTA
                            { responsivePriority: 2, targets: 2 }, // CIRUGIA
                            { responsivePriority: 3, targets: 1 }, // DIAGNOSTICO
                            { responsivePriority: 0, targets: 0 }, // ID
                            { responsivePriority: -1, targets: -1 }, // BOTON
                            {
                                targets: -1,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    const fila = meta.row;
                                    const boton = `<button type='button' class='btn btn-outline-primary' onclick='imprimeFormularioIQX(${adataset[fila][0]});'>Imprimir</button>`;
                                    return boton;
                                }
                            }
                        ]
                    })

                }
                cantidadFilas = $("#tblProcedimientosProgramados").DataTable().rows().count()
                $("#cantidadProcedimientosProgramados").text(cantidadFilas)
            }
            else {
                $("#cantidadProcedimientosProgramados").text(cantidadFilas)
                $("#cantidadProcedimientosProgramados").removeClass("badge-success")
                $("#cantidadProcedimientosProgramados").addClass("badge-secondary")
                $("#alertProcedimientos").append("No existe información para mostrar")
            }
        },
        error: function (err) {

            if (err.status == 404) {

                $("#cantidadProcedimientosProgramados").removeClass("badge-success")
                $("#cantidadProcedimientosProgramados").addClass("badge-secondary")
                $("#cantidadProcedimientosProgramados").text(cantidadFilas)
            }

            $("#modalCargando").hide()
        }
    });
}

//TABLA INTERVENCIONES PACIENTE
const getIntervencionesPaciente = (idHospitalizacion) => {
    
    let adataset = []

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}PAB_Protocolo_Operatorio/buscar?idHospitalizacion=${idHospitalizacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            const fechaActual = moment(GetFechaActual()).format("YYYY-MM-DD")

            if (data.length !== 0) {
                data.forEach((item, index) => {
                    adataset.push([
                        item.Id,
                        moment(item.Fecha).format("DD-MM-YYYY"),
                        (item.Diagnostico != null) ? item.Diagnostico : "",
                        (item.OperacionRealizada != null) ? item.OperacionRealizada : "",
                        item.DiasOperados = moment(fechaActual).diff(moment(item.Fecha).format("YYYY-MM-DD"), 'days'),
                        (item.HoraInicio != null) ? item.HoraInicio : "",
                        (item.HoraFin != null) ? item.HoraFin : ""
                    ]);
                });

                $("#tblIntervenciones").DataTable({
                    responsive: {
                        details: false,
                        type: 'column'
                    },
                    data: adataset,
                    columns: [
                        { title: "ID" },
                        { title: "Fecha" },
                        { title: "Diagnostico" },
                        { title: "Operacion Realizada" },
                        { title: "Días Operados" },
                        { title: "Hora Inicio" },
                        { title: "Hora Final" },
                        { title: "" }
                    ],

                    columnDefs: [

                        // -1 ES DE MAYOR PRIORIDAD LUEGO 0...                                        
                        { responsivePriority: 5, targets: 6 }, // DIAS OPERADOS
                        { responsivePriority: 4, targets: 5 }, // HORA INICIO
                        { responsivePriority: 6, targets: 4 }, // HORA FINAL
                        { responsivePriority: 2, targets: 3 }, // OPERACION REALIZADA
                        { responsivePriority: 3, targets: 2 }, // DIAGNOSTICO
                        { responsivePriority: 1, targets: 1 }, // FECHA
                        { responsivePriority: 0, targets: 0 }, // ID
                        { responsivePriority: -1, targets: -1 },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render:
                                function (data, type, row, meta) {
                                    const fila = meta.row;
                                    const boton = `<button type='button' class='btn btn-outline-primary' 
                                                            onclick="ImprimirDocumento(${adataset[fila][0]}, 'ProtocoloOperatorio');">
                                                            Imprimir</button>`;
                                    return boton;
                                }
                        },
                    ]
                })
            }
            cantidadFilas = $("#tblIntervenciones").DataTable().rows().count()

            $("#cantidadIntervenciones").text(cantidadFilas)
        },
        error: function (err) {

            if (err.status == 404) {

                $("#alertIntervenciones").append("No existe información para mostrar")
                $("#cantidadIntervenciones").removeClass("badge-success")
                $("#cantidadIntervenciones").addClass("badge-secondary")
                $("#cantidadIntervenciones").text(0)
            }

            $("#modalCargando").hide()
        }
    });
}