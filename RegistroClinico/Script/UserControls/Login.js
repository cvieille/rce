﻿
$(document).ready(function () {
    $("#aLoginModal").click(function () {
        LoginModal();
    });

    // Descativar el autocompletar en los campos de usuario y contraseña
    $('#txtUsuarioLogin').attr('autocomplete', false);
    $('#txtClaveLogin').attr('autocomplete', false);

});

function LoginModal() {

    const user = $('#txtUsuarioLogin').val();
    const pass = CryptoJS.MD5($('#txtClaveLogin').val());
    const datos = {
        "grant_type": "password",
        "username": user,
        "password": '' + pass,
        "clientid": '14',
        "idperfil": getSession().ID_PERFIL
    }

    delete $.ajaxSettings.headers['Authorization'];

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}recuperarToken`,
        data: datos,
        async: false,
        success: function (response) {
            setSession("TOKEN", response.access_token);
            setSession("FECHA_FINAL_SESION", moment(GetFechaActual()).add(parseInt(response.expires_in) - 5, 'seconds').format('YYYY-MM-DD HH:mm:ss'));
            setSession("LOGIN_USUARIO", user);
            $.ajaxSetup({ headers: { 'Authorization': GetToken() } });
            GetUsuarioModal(user);
        },
        error: function (err) {
            console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: "Usuario o contraseña incorrecta",
                showConfirmButton: false,
                timer: 2000
            });

            window.location.replace(`${ObtenerHost()}/Vista/Default.aspx`);
        }
    });

}

function GetUsuarioModal(user) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Usuarios/Acceso`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            if ($.isEmptyObject(data)) {

                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Usuario o contraseña incorrecta 22",
                    showConfirmButton: false,
                    timer: 2000
                });

            } else {

                const [{ GEN_idUsuarios, GEN_idProfesional, GEN_nombrePersonas, GEN_apellido_paternoPersonas, GEN_apellido_maternoPersonas }] = data
                const nombreUsuario = `${GEN_nombrePersonas} ${GEN_apellido_paternoPersonas} ${GEN_apellido_maternoPersonas}`

                deleteSession("ID_USUARIO")
                setSession("ID_USUARIO", GEN_idUsuarios)

                deleteSession("LOGIN_USUARIO")
                setSession("LOGIN_USUARIO", user)

                deleteSession("NOMBRE_USUARIO")
                setSession("NOMBRE_USUARIO", nombreUsuario)

                deleteSession("ID_PROFESIONAL")
                setSession("ID_PROFESIONAL", GEN_idProfesional)

                SetSessionServer({
                    LOGIN_USUARIO: user,
                    NOMBRE_USUARIO: nombreUsuario,
                    CODIGO_PERFIL: getSession().CODIGO_PERFIL,
                    PERFIL_USUARIO: getSession().PERFIL_USUARIO,
                    TOKEN: getSession().TOKEN
                });

                $('.nombreUsuario').html(nombreUsuario)

                if (location.href.includes("BandejaUrgencia.aspx")) {
                    CargarTablaUrgencia()
                }

                ShowModalCargando(false)
                $('#mdlLogin').modal('hide');
                $('#txtUsuarioLogin').val('');
                $('#txtClaveLogin').val('');

            }

        }
    });

}
