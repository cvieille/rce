﻿

//let idHospitalizacion = 0

async function linkMostrarModalBrazalete(sender) {

    idHospitalizacion = $(sender).data("id")
    let nombre = $(sender).data("pac")
    let cama = $(sender).data("cama")

    $("#txtModalBrazalete").html("<i class='fa fa-barcode' aria-hidden='true'></i> Instalación de brazalete")
    $("#txtNumHospModalBrazalete").val(idHospitalizacion)
    $("#txtNomPacModalBrazalete").val(nombre)
    $("#txtNumCamaModalBrazalete").val(cama)
    $("#divBrazalete").attr("data-id", idHospitalizacion)

    cargarCombosBrazalete("#sltTipoExtremidadBrazalete", "#sltIdTipoPlano")
    defineFechaMaximayMinimaBrazalete(GetFechaActual(), "#inputFechaPlano")

    limpiarInputsBrazalete()
    limpiaRequiredBrazalete("#divBrazalete", idHospitalizacion, true)

    let brazaletes = await getBrazaletePaciente(idHospitalizacion)
    dibujarDataTableBrazalete("#tblBrazalete",brazaletes, false)

    $("#mdlBrazalete").modal('show');
}

// Elimina o agrega data-required y pinta o no los elementos en rojo
function limpiaRequiredBrazalete(selector, id, remueve) { // remueve es boolean

    let element = $(selector).filter(`[data-id='${id}']`)
    let inputs = $(element).find("select, input")
    let span = $(element).find("span.invalid-input")
    let labels = $(element).find("label")

    labels.find("b").remove()
    labels.append("<b class='color-error'> (*)</b>")

    inputs.each(function (index, e) {
        if (remueve) {
            $(e).removeAttr("data-required")
            if ($(e).hasClass("is-invalid") || $(e).hasClass("invalid-input")) {
                $(e).removeClass("is-invalid")
                $(e).removeClass("invalid-input")
            }
            span.each(function (index, e) {
                $(e).remove()
            })
        }
        else
            $(e).attr("data-required", true)
    })
}

function defineFechaMaximayMinimaBrazalete(fechaActual, inputFechaBrazalete) {

    let fechaHoyConFormato = moment(fechaActual).format("YYYY-MM-DD");
    $(inputFechaBrazalete).attr("min", AgregarDias(fechaHoyConFormato, -3));
    $(inputFechaBrazalete).attr("max", fechaHoyConFormato);
    $(inputFechaBrazalete).val(fechaActual.format("yyyy-MM-dd"));
}

function cargarCombosBrazalete(extremidad, plano) {

    $(plano).prop("disabled", true)
    comboTipoExtremidad()

    $(extremidad).change(function () {
        let value = ""

        value = $(this).val()
        if (value === "0") {
            $(plano).attr("disabled", true)
            $(plano).append("<option value='0'>Seleccione</option>")
            $(plano).val(0)
        }
        else if (EsValidaSesionToken(extremidad)) {
            $(plano).attr("disabled", false)
            comboTipoPlano(value)
        }
        else {
            $(plano).val(0)
            $(plano).attr("disabled", true)
        }
    })
}

function getMovimientosBrazalete(idHospitalizacion, idBrazalete) {

    data = []
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Brazalete/${idBrazalete}/Movimientos`

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success:
            function (Brazalete) {
                data.push(Brazalete)
            },
        error: function (err) {
            console.error("Error al llenar brazalete")
            console.error(JSON.stringify(err))
        }
    })

    return data
}

//"#tblBrazalete"
function dibujarDataTableBrazalete(elemento, data, readonly) {

    let botonAcciones = ""
    let brazaletes = []

    data.map((brazalete, index) => {
        brazaletes.push([
            index,
            brazalete.Id,
            brazalete.Extremidad.Valor,
            brazalete.Plano.Valor,
            brazalete.Instalado
        ])
    })

    $(elemento).addClass("nowrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],

        responsive: false,
        "rowCallback": function (row, data, index) {

            $(row).attr("data-index", data[0])

            let idRow = $(row).find("td:eq(2)").find("input")
            if (data[4] === true) {
                idRow.bootstrapSwitch("state", true)
                if (readonly)
                    idRow.bootstrapSwitch('disabled', true)
            }
            else if (data[4] === false) {
                idRow.bootstrapSwitch("state", false)
                idRow.bootstrapSwitch('disabled', true)
            }
        },
        "order": [[0, "asc"]],
        "pageLength": 10,
        info: true,
        data: brazaletes,
        fnCreatedRow: function (rowEl, adataset, index) {
            $(rowEl).attr('id', `row-${brazaletes[index][1]}`)
        },
        columns: [
            { title: "INDEX" },
            { title: "ID" },
            { title: "Extremidad" },
            { title: "Plano" },
            { title: "Estado" }, //true or false
            { title: "Instalado" },
            { title: "Acciones", class: "text-center" },
        ],
        columnDefs: [
            { targets: [0, 1, 4], visible: false },
            { targets: [6], visible: !readonly },
            //{ "width": "30%", "targets": [2, 3] },
            {
                targets: -1,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    cantidadFilas = $(elemento).DataTable().rows().count() - 1

                    if (data[1] > 0) {
                        botonAcciones = `<div class="row"><div class="col-md-6">
                                        <button
                                            type="button"
                                            class="btn btn-outline-primary btn-sm form-control"
                                            data-id="${data[1]}"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Ver Mas"
                                            onclick="showMovimientos(this)"
                                        >
                                            <i class="fas fa-info fa-sm"></i>
                                        </button>
                                        </div>
                                    `
                    } else if (data[1] === 0) {
                        botonAcciones = `<div class="row"><div class="col-md-6">
                                        <button
                                            type="button"
                                            class="btn btn-outline-secondary btn-sm form-control"
                                            data-id="${data[1]}"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Ver Mas"
                                            onclick="showMovimientos(this)"
                                            disabled="disabled"
                                        >
                                            <i class="fas fa-info fa-sm"></i>
                                        </button>
                                        </div>
                                    `
                    } else {
                        botonAcciones = `<div class="row"><div class="col-md-6">

                                        </div>`
                    }
                    if (data[1] == 0) {
                        botonAcciones += `<div class="col-md-6">
                                        <button
                                            type="button"
                                            class="btn btn-outline-danger btn-sm form-control eliminar"
                                            id="eliminar-${data[1]}"
                                            data-id="${data[1]}"
                                            data-index="${cantidadFilas}"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Eliminar"
                                            onclick="deleteBrazalete(this)"
                                        >
                                          <i class="fas fa-trash-alt fa-sm"></i>
                                        </button>
                                        </div></div>
                                    `
                    }
                    else if (data[1] === row[1] && data[4] === true && data[1] > 0) {
                        botonAcciones += `<div class="col-md-6">
                                        <button
                                            type="button"
                                            class="btn btn-outline-danger btn-sm form-control eliminar"
                                            id="eliminar-${data[1]}"
                                            data-id="${data[1]}"
                                            data-index="${cantidadFilas}"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Eliminar"
                                            onclick="deleteBrazalete(this)"
                                        >
                                          <i class="fas fa-trash-alt fa-sm"></i>
                                        </button>
                                        </div></div>
                                    `
                    }

                    return botonAcciones
                }
            },
            {
                targets: -2,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {

                    /*
                     * data[0] INDEX
                     * data[1] ID
                     * data[2] EXTREMIDAD
                     * data[3] PLANO
                     * data[4] INSTALADO (TRUE OR FALSE)
                     * data[5] BOTON INSTALADO
                     * data[6] ACCIONES
                     */

                    cantidadFilas = $(elemento).DataTable().rows().count() - 1
                    index = $(elemento).DataTable().row().index()

                    let botones = `
                                    <div class="col-sm-4">
                                        <input
                                            type="checkbox"
                                            class="bootstrap-switch"
							                id="brazalete-${data[1]}"
										    data-estado="${data[4]}"
                                            data-id="${data[1]}"
										    data-extremidad="${data[2]}"
										    data-plano="${data[3]}"
										    data-toggle="tooltip"
										    data-placement="bottom"
                                            data-on-text="SI"
                                            data-off-text="NO"
                                            data-on-color="primary" data-off-color="danger" data-size="normal"
										    onchange="cambioEstadoBrazalete(this);"
                                            
                                        />                                    
                                     </div>
                                    `
                    return botones
                }
            },
        ],
        "bDestroy": true
    });
}

async function deleteBrazalete(e) {

    let id = $(e).data("id")
    let row = `#row-${id}`

    Swal.fire({
        title: `¿Seguro quieres eliminar el brazalete?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {
            if (id > 0) {
                EliminarBrazalete(id)
                eliminarFilaDatatable("#tblBrazalete", row)
                toastr.success("Brazalete eliminado correctamente")
            }
        }
        else {
            setTimeout(() => {
                $(e).blur()
            }, 270)
        }
    })
}

function showMovimientos(e) {

    let id = $(e).data("id")

    let contenido = ""
    $("#contenidoMovimientoBrazalete").empty()
    const movimientosBrazalete = getMovimientosBrazalete(idHospitalizacion, id)

    contenido += `<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover col-xs-12 col-md-12 w-100"
                        id="tblMovimientosBrazsalete">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Movimiento</th>
                            </tr>
                        </thead>
                        <tdody>
                        `
    movimientosBrazalete.map(movBrazalete => {
        movBrazalete.map(m => {
            contenido += `<tr>
                            <td>${moment(m.Fecha).format("DD-MM-YYYY HH:mm")}</td>
                            <td>${m.Usuario}</td>
                            <td>${m.TipoMovimiento}</td>
                          </tr>`
        })
    })

    contenido += `</tbody></table></div>`
    $("#contenidoMovimientoBrazalete").append(contenido)
    $("#mdlMovimientosBrazalete").modal("show")
}

function ocultarBotonEliminar(tabla, id) {
    let rowId = `row-${id}`
    let tdEliminar = $(`${tabla} tbody tr[id="${rowId}"]`).find("td:eq(3)").find(`button#eliminar-${id}`)
    tdEliminar.hide()
}

function cambioEstadoBrazalete(e) {

    let id = $(e).data("id")

    $(e).on('switchChange.bootstrapSwitch', function (event, state) {
        event.preventDefault()
        if (state === false) {
            Swal.fire({
                title: `¿Seguro quieres cambiar el brazalete?`,
                text: "No se podran revertir los cambios",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
            }).then((result) => {
                if (result.value) {
                    DesinstalarBrazalete(id)
                    $(e).bootstrapSwitch("state", false)
                    $(e).bootstrapSwitch('disabled', true)
                    toastr.success("Brazalete cambiado correctamente")
                    ocultarBotonEliminar("#tblBrazalete", id)
                }
            })
        }
    })
}

function limpiarInputsBrazalete() {

    $("#sltTipoExtremidadBrazalete").val("0")
    $("#sltIdTipoPlano").val("0")
    $("#sltIdTipoPlano").attr("disabled", true)
    $("#inputHoraPlano").val("")

}

async function GuardarBrazalete(brazalete) {


    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Brazalete`
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(brazalete),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se ha instalado el brazalete correctamente`)

        },
        error: function (jqXHR, status) {
            console.log(`Error al guardar brazalete: ${JSON.stringify(jqXHR)} `)
        }
    });
}

async function DesinstalarBrazalete(id) {

    let parametrizacion = {
        method: "PATCH",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Brazalete/${id}/Desinstalar`
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se ha desinstalado el brazalete correctamente`)

        },
        error: function (jqXHR, status) {
            console.log(`Error al desinstalar el brazalete: ${JSON.stringify(jqXHR)} `)
        }
    });
}

async function EliminarBrazalete(id) {

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Brazalete/${id}/Eliminar`
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se ha eliminado el brazalete correctamente`)

        },
        error: function (jqXHR, status) {
            console.log(`Error al eliminar el brazalete: ${JSON.stringify(jqXHR)} `)
        }
    });
}

async function agregarBrazalete() {

    limpiaRequiredBrazalete("#divBrazalete", idHospitalizacion, false)

    if (!validarCampos(`#divBrazalete`, false)) {
        return
    }

    const Extremidad = $("#sltTipoExtremidadBrazalete option:selected").text()
    const Plano = $("#sltIdTipoPlano option:selected").text()

    const nuevoBrazale = {
        IdHospitalizacion: idHospitalizacion,
        IdTipoExtremidad: parseInt($("#sltTipoExtremidadBrazalete").val()),
        idTipoPlano: parseInt($("#sltIdTipoPlano").val()),
        Instalado: true
    }

    limpiarInputsBrazalete()

    let brazaletes = await getBrazaletePaciente(idHospitalizacion)
    let brazaleteEncontrado = await existeBrazalete(brazaletes, nuevoBrazale)

    if (!brazaleteEncontrado) {
        await GuardarBrazalete(nuevoBrazale)
        brazaletes = await getBrazaletePaciente(idHospitalizacion)

    }
    else {
        toastr.error(`El paciente ya tiene brazalete en: ${Extremidad} ${Plano}`)
    }

    dibujarDataTableBrazalete("#tblBrazalete",brazaletes, false)
}

async function existeBrazalete(brazaletes, obj) {

    let brazaleteEncontrado = await brazaletes.find(x => x.Extremidad.Id === obj.IdTipoExtremidad && x.Plano.Id === obj.idTipoPlano && x.Instalado === true)

    if (brazaleteEncontrado !== undefined)
        return true
    return false
}


