﻿
//let IdAcompañante = null;

let sesionAcompaniante = getSession()

let estadoAcompaniante = false

$(document).ready(async function () {

    $("#chkAcompañante").on('switchChange.bootstrapSwitch', async function (event, state) {

        estadoAcompaniante = $(this).bootstrapSwitch('state')

        if (estadoAcompaniante) {
            InicializarMostrarAcompañante()
        }
        else {
            await ShowAcompañante(false)
        }
    })
})

async function InicializarMostrarAcompañante() {
    await ShowAcompañante(true)
    await InicializarDatosAcompañante()
    await resetLoadAcompaniante("#sltIdentificacionAcompañante", "#txtnumeroDocAcompañante", "#txtDigAcompañante", "divAcompañante")
    ocultarMostrarFichaPac($("#sltIdentificacionAcompañante"), $("#txtnumeroDocAcompañante"))
    ChangeTitleNumDoc("#sltIdentificacionAcompañante", "#txtnumeroDocAcompañante", "#lblIdentificacionAcompañante")
}

async function resetLoadAcompaniante(identificacion, numDoc, digito, div) {

    $(digito).on('input', async function () {

        if ($(this).val() === "") {
            //await DeshabilitarPersona(div, false)
            await ReiniciarAcompanante()
        }
        else {
            $(`${div} input`).not(`${numDoc}, ${digito}`).attr("disabled", false)
            $(`${div} select`).not(`${identificacion}`).attr("disabled", false)
        }


        if ($.trim($(digito).val()) != "") {

            // 1 = RUT
            // 4 = RUT MATERNO
            if ($(identificacion).val() === '1' || $(identificacion).val() === '4') {

                // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
                let esRutCorrecto = await EsValidoDigitoVerificador($("#txtnumeroDocAcompañante").val(), $("#txtDigAcompañante").val())

                // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA
                //TRAER LA INFO DEL PACIENTE

                if (esRutCorrecto) {
                    await DeshabilitarPersona("divAcompañante", false)
                    const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())
                    await cargarPersonaAcompaniante(personaAcompaniante);
                }
                else {
                    await DeshabilitarPersona("divAcompañante", true)
                }
            }
        }
    })
}

async function buscarPersonaAcompaniante(idPersona = null, identificacion = null, numDocumento = null) {

    let personaAcompaniante = {}

    if (numDocumento === "") {
        personaAcompaniante == null
        return
    }


    let url = (idPersona === null)
        ? `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${identificacion}&numeroDocumento=${numDocumento}`
        : `${GetWebApiUrl()}GEN_Personas/${idPersona}`

    try {
        let persona = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        personaAcompaniante = (Array.isArray(persona)) ? persona[0] : persona

        return personaAcompaniante

    } catch (error) {
        if (error.status === 404) {
            //toastr.error(`No se encontró numero de documento: <strong>${numDocumento}</strong>`)
            await DeshabilitarPersona("divAcompañante", false);
            return null
        }
    }
}

async function cargarPersonaAcompaniante(persona) {

    if (persona === null || persona === undefined)
        return

    let idIdentificacion = (persona?.Identificacion !== null) ? persona.Identificacion.Id : 0
    $("#sltIdentificacionAcompañante").val(idIdentificacion)
    $("#sltIdentificacionAcompañante").change()
    $("#txtnumeroDocAcompañante").val(persona.NumeroDocumento ?? "")
    $("#txtDigAcompañante").val(persona.Digito ?? "")
    $("#txtNombreAcompañante").val(persona.Nombre ?? "")
    $("#txtApePatAcompañante").val(persona.ApellidoPaterno ?? "")
    $("#txtApeMatAcompañante").val(persona.ApellidoMaterno ?? "")
    $("#txtTelAcompañante").val(persona.Telefono ?? "")
    let idGenero = (persona.Genero !== null) ? persona.Genero.Id : 0
    $("#sltTipoGeneroAcompaniante").val(idGenero)
    let idSexo = (persona.Sexo !== null) ? persona.Sexo.Id : 0
    $("#sltSexoAcompañante").val(idSexo)

}

async function formatJsonPersonaAcompaniante() {

    let persona =
    {
        IdIdentificacion: $("#sltIdentificacionAcompañante").val(),
        NumeroDocumento: $("#txtnumeroDocAcompañante").val(),
        Digito: $("#txtDigAcompañante").val(),
        Nombre: $("#txtNombreAcompañante").val(),
        ApellidoPaterno: $("#txtApePatAcompañante").val(),
        ApellidoMaterno: $("#txtApeMatAcompañante").val(),
        Telefono: $("#txtTelAcompañante").val(),
        IdTipoGenero: $("#sltTipoGeneroAcompaniante").val(),
        IdSexo: $("#sltSexoAcompañante").val()
    }

    return persona
}


async function guardarPersonaAcompaniante(persona) {
    if (persona === null)
        return;

    let parametrizacion = {
        method: (persona.Id === undefined) ? "POST" : "PUT",
        url: (persona.Id === undefined)
            ? `${GetWebApiUrl()}GEN_Personas`
            : `${GetWebApiUrl()}GEN_Personas/${persona.Id}`
    };

    try {
        const personaAcompaniante = await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(persona),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });

        toastr.success("Se agregó nueva persona");
        return personaAcompaniante;
    } catch (error) {
        console.log(`Error al guardar persona acompañante: ${JSON.stringify(error)}`);
        return null;
    }
}

// Inicio/Reinicio de Acompañante
async function InicializarDatosAcompañante() {

    IdAcompañante = null;

    //$("#chkAcompañante").on('switchChange.bootstrapSwitch', async function (event, state) {
    //    ShowAcompañante($(this).bootstrapSwitch('state'));
    //});
    $('#txtFiltroRut').blur(async function (e) {
        $('#dvAcompañante').val('');
        if ($(this).val().length > 4)
            $('#dvAcompañante').val(await ($(this).val()));
    });

    //$("#sltIdentificacionAcompañante").change(async function () {
    //    SltIdentificacionAcompañante_Change(this);
    //});

    $("#txtnumeroDocAcompañante").on('input', async function () {
        if ($("#sltIdentificacionAcompañante").val() === '1' || $("#sltIdentificacionAcompañante").val() === '4') {
            //$("#txtDigAcompañante").val('');
            await ReiniciarAcompanante();
            await DeshabilitarPersona("divAcompañante", true);
        }
    });
    $("#txtnumeroDocAcompañante").on('blur', async function () {

        if ($("#sltIdentificacionAcompañante").val() === '2' || $("#sltIdentificacionAcompañante").val() === '3') {
            if ($.trim($("#txtnumeroDocAcompañante").val()) !== '') {

                await ReiniciarPersona()
                const personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

                if (personaAcompaniante !== null) {
                    await DeshabilitarPersona("divAcompañante", false)
                    await cargarPersonaAcompaniante(personaAcompaniante)
                }
            }
            else {
                await ReiniciarPersona();
                await DeshabilitarPersona("divAcompañante", true)
            }
        }
        else {
            await DeshabilitarPersona("divAcompañante", true)
        }

    });
}

// ACOMPAÑANTE
//async function CargarPersona(idPersona, idIdentificacion, numeroDocumentoPersonas) {

//    let json = await buscarAcompaniante(idIdentificacion, numeroDocumentoPersonas)


//    //let data = await GetJsonCargarPersona(idPersona, idIdentificacion, numeroDocumentoPersonas);

//    //if (sesionAcompaniante.ID_HOSPITALIZACION > 0) {
//    //    json = data
//    //}
//    //else {
//    //    json = data[0]
//    //}

//    //if (Array.isArray(json))
//    //    json = json[0]

//    if (json !== null || json !== undefined) {

//        $("#txtNombreAcompañante").val(json.Nombre);
//        $("#txtApePatAcompañante").val(json.ApellidoPaterno);
//        $("#txtApeMatAcompañante").val(json.ApellidoMaterno);
//        $("#txtTelAcompañante").val(json.Telefono);

//        let sexo = 0

//        if (json.GEN_idSexo === undefined)
//            sexo = json.Sexo.Id
//        else if (json.GEN_idSexo === null)
//            sexo = json.GEN_idSexo

//        $("#sltSexoAcompañante").val(sexo ?? 0);


//        $("#txtIdAcom").val(json.Id);

//        IdAcompañante = json.Id;
//        if (idPersona !== null) {

//            await DeshabilitarPersona("divAcompañante", false);
//            $("#sltIdentificacionAcompañante").val(json.Identificacion.Id);
//            $("#txtnumeroDocAcompañante").val(json.NumeroDocumento);
//            if (json.Identificacion.Id === 1 || json.Identificacion.Id === 4) {

//                if (json.Digito == null) {
//                    let dig = await ObtenerVerificador($("#txtnumeroDocAcompañante").val());
//                    $(`#txtDigAcompañante`).val(dig);
//                } else
//                    $("#txtDigAcompañante").val(json.Digito);

//            }
//        }

//    } else {
//        IdAcompañante = 0;
//    }

//}
async function GetJsonCargarPersona(idPersona, idIdentificacion, numeroDocumento) {

    let d = {}

    let url = (idPersona === null) ?
        `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${idIdentificacion}&numeroDocumento=${numeroDocumento}` :
        `${GetWebApiUrl()}GEN_Personas/${idPersona}`;

    try {

        const persona = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return persona

    } catch (error) {

        if (error.status === 404) {
            d = {

                GEN_numero_documentoPersonas: null,
                GEN_digitoPersonas: null,
                GEN_nombrePersonas: null,
                GEN_apellido_paternoPersonas: null,
                GEN_apellido_maternoPersonas: null,
                GEN_telefonoPersonas: null,
                GEN_emailPersonas: null,
                GEN_idIdentificacion: null,
                GEN_idSexo: null,
                GEN_estadoPersonas: "Activo",
                GEN_dir_callePersonas: null,
                GEN_idCategorias_Ocupacion: null,
                GEN_idTipo_Escolaridad: null,
                GEN_fecha_nacimientoPersonas: null
            }

            if (d !== null)
                d.GEN_fecha_actualizacionPersonas = GetFechaActual();

            return [d];
        }

        console.error("Error al cargar persona")
        console.log(JSON.stringify(error))
    }
}
async function DeshabilitarPersona(divPadre, esDisabled) {
    (!esDisabled) ? $(`#${divPadre} .datos-persona`).removeAttr("disabled") : $(`#${divPadre} .datos-persona`).attr("disabled", "disabled");
}
async function ReiniciarPersona() {

    $(`#divAcompañante input.datos-persona`).val("");
    $(`#divAcompañante select.datos-persona`).val("0");
    ReiniciarRequired();

}
async function ReiniciarAcompanante() {

    //const parent = $("#txtnumeroDocAcompañante").parent().parent().parent().parent();
    //await DeshabilitarPersona("divAcompañante", true);
    //await ReiniciarPersona();
    IdAcompañante = null;
    //$("#divAcompañante input").not("#txtnumeroDocAcompañante").val("");
    //$("#sltTipoGeneroAcompaniante, #sltSexoAcompañante, #sltTipoRepresentante").val("0");
    DeshabilitarPersona("divAcompañante", true)
    limpiarDatosAcompaniante()
}

function limpiarDatosAcompaniante() {
    $("#divAcompañante input").not("#txtnumeroDocAcompañante").val("");
    $("#sltTipoGeneroAcompaniante, #sltSexoAcompañante").val("0");
}

//async function GuardarPersona() {

//    //await CargarPersona(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val());

//    var url = (IdAcompañante === undefined) ? `${GetWebApiUrl()}GEN_Personas` : `${GetWebApiUrl()}GEN_Personas/${IdAcompañante}`;
//    var method = (IdAcompañante === undefined) ? "POST" : "PUT";


//    var json = await GetJsonCargarPersona(IdAcompañante, null, null);

//    var persona = {
//        GEN_telefonoPersonas: json.GEN_telefonoPersonas,
//        GEN_emailPersonas: json.GEN_emailPersonas,
//        GEN_idSexo: 4,
//        GEN_estadoPersonas: "Activo",
//        GEN_dir_callePersonas: json.GEN_dir_callePersonas,
//        GEN_idCategorias_Ocupacion: json.GEN_idCategorias_Ocupacion,
//        GEN_idTipo_Escolaridad: json.GEN_idTipo_Escolaridad,
//        GEN_fecha_nacimientoPersonas: json.GEN_fecha_nacimientoPersonas,
//        GEN_fecha_actualizacionPersonas: json.GEN_fecha_actualizacionPersonas
//    };

//    persona.GEN_idIdentificacion = valCampo($("#sltIdentificacionAcompañante").val());
//    persona.GEN_numero_documentoPersonas = valCampo($("#txtnumeroDocAcompañante").val());
//    persona.GEN_digitoPersonas = valCampo($("#txtDigAcompañante").val());
//    persona.GEN_nombrePersonas = valCampo($("#txtNombreAcompañante").val());
//    persona.GEN_apellido_paternoPersonas = valCampo($("#txtApePatAcompañante").val());
//    persona.GEN_apellido_maternoPersonas = valCampo($("#txtApeMatAcompañante").val());
//    persona.GEN_telefonoPersonas = valCampo($("#txtTelAcompañante").val());
//    persona.GEN_idSexo = valCampo($("#sltSexoAcompañante").val());



//    if (IdAcompañante !== undefined) {
//        persona.GEN_idPersonas = IdAcompañante;
//    }

//    //console.log(url)
//    //console.log(method)
//    //console.log(persona)
//    //return 

//    $.ajax({
//        type: method,
//        url: url,
//        data: JSON.stringify(persona),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        async: false,
//        success: function (data, status, jqXHR) {
//            if (data != undefined) {
//                IdAcompañante = data.GEN_idPersonas;
//            }

//        },
//        error: function (jqXHR, status) {
//            console.log(`Error al guardar Persona: ${JSON.stringify(jqXHR)}`);
//        }
//    });


//    return IdAcompañante
//}
//Esta funcion no se esta usando
//async function SltIdentificacionAcompañante_Change(element) {

//    //$("#sltIdentificacionAcompañante").val("0")

//    $("#txtnumeroDocAcompañante, #txtDigAcompañante").val('');

//    await ReiniciarPersona();
//    await DeshabilitarPersona("divAcompañante", true);//$("#sltIdentificacionAcompañante").children("option:selected").text()

//    $(`label[for='sltIdentificacionAcompañante']`).text($("#sltIdentificacionAcompañante > option:selected").text());



//    switch ($("#sltIdentificacionAcompañante").val()) {
//        case '2':
//        case '3':
//            $("#txtnumeroDocAcompañante").parent().children(".digitoAcompanante").hide();
//            $("#txtnumeroDocAcompañante").attr("maxlength", 15);
//            break;
//        default:
//            $("#txtnumeroDocAcompañante").parent().children(".digitoAcompanante").show();
//            $("#txtnumeroDocAcompañante").attr("maxlength", 8);
//            break;
//    }

//    ReiniciarRequired();
//}

// COMBOS
function CargarCombosPersona() {
    ComboIdentificacionAcompañante();
    ComboSexoAcompañante();
    ComboTipoGeneroAcompaniante();
    ComboRepresentanteAcompañante();
}
function ComboIdentificacionAcompañante() {
    $("#sltIdentificacionAcompañante").empty();
    $("#sltIdentificacionAcompañante").append($("#sltIdentificacion").html());
    $("#sltIdentificacionAcompañante").val("1");
    $(`label[for='sltIdentificacionAcompañante']`).text($("#sltIdentificacionAcompañante").children("option:selected").text());
    $("#sltIdentificacionAcompañante option[value='5']").remove();
    $("#sltIdentificacionAcompañante option[value='4']").remove();
}
function ComboSexoAcompañante() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, false, $('#sltSexoAcompañante'));
}
function ComboTipoGeneroAcompaniante() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoGeneroAcompaniante'));
}

function ComboRepresentanteAcompañante() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoRepresentante'));
}

// ACOMPAÑANTE
function GetIdAcompañante() {
    return IdAcompañante;
}
function SetIdAcompañante(IdPersona) {
    IdAcompañante = IdPersona;
}
async function ShowAcompañante(esVisible) {

    IdAcompañante = null;
    if (esVisible) {
        CargarCombosPersona();
        $('#divAcompañante').slideDown();
        $('#txtnumeroDocAcompañante, #txtDigAcompañante').val("");
        $(`#txtnumeroDocAcompañante, #txtNombreAcompañante, #txtApePatAcompañante, #sltTipoGeneroAcompaniante,
            #sltSexoAcompañante, #sltTipoRepresentante`).attr("data-required", "true");

        await ReiniciarPersona();
        await DeshabilitarPersona("divAcompañante", true);
    } else {
        $('#divAcompañante').slideUp();
        $(`#txtnumeroDocAcompañante, #txtnombreAcompañante, #txtApePatAcompañante, #sltTipoGeneroAcompaniante,
            #sltSexoAcompañante, #sltTipoRepresentante`).attr("data-required", "false");

        await ReiniciarPersona();
    }

}
async function BloquerAcompañante() {
    $('#txtnumeroDocAcompañante, #txtDigAcompañante, #sltIdentificacionAcompañante').attr("disabled", "disabled");
    await DeshabilitarPersona("divAcompañante", true);
}

//function cargarRepresentante() {
//    let idRepresentante = GetIdAcompañante()

//    const jsonRepresentante = {
//        Id: idRepresentante,
//        IdIdentificacion: parseInt($("#sltIdentificacionAcompañante").val()),
//        NumeroDocumento: $("#txtnumeroDocAcompañante").val(),
//        Digito: $("#txtDigAcompañante").val(),
//        Nombre: $("#txtNombreAcompañante").val(),
//        ApellidoPaterno: $("#txtApePatAcompañante").val(),
//        ApellidoMaterno: $("#txtApeMatAcompañante").val(),
//        Telefono: $("#txtTelAcompañante").val(),
//        IdSexo: parseInt($("#sltSexoAcompañante").val()),
//    }

//    if (idRepresentante !== 0) // existe
//        jsonRepresentante.Id = idRepresentante // REVISAR
//    else
//        delete jsonRepresentante.Id // elimina o null

//    return jsonRepresentante
//}

function getDatosAcompaniantes(id) {

    id = id ?? null
    if (id !== null && id !== undefined && id > 0) {

        const url = `${GetWebApiUrl()}GEN_Personas/${id}`

        //$("#chkAcompañante").bootstrapSwitch('disabled', false)
        $("#chkAcompañante").bootstrapSwitch('state', true)
        ShowAcompañante(true)

        $.ajax({
            type: "GET",
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {

                const { Identificacion, NumeroDocumento, Digito, Nombre, ApellidoPaterno, ApellidoMaterno, Telefono, Sexo } = data
                const idIdentificacion = Identificacion ?? ""
                $("#sltIdentificacionAcompañante").val(idIdentificacion.Id)
                $("#txtnumeroDocAcompañante").val(NumeroDocumento ?? "")
                $("#txtDigAcompañante").val(Digito ?? "")
                $("#txtNombreAcompañante").val(Nombre ?? "")
                $("#txtApePatAcompañante").val(ApellidoPaterno ?? "")
                $("#txtApeMatAcompañante").val(ApellidoMaterno ?? "")
                $("#txtTelAcompañante").val(Telefono ?? "")
                const sexo = Sexo ?? ""
                $("#sltSexoAcompañante").val(sexo.Id)
            },
            error: function (err) {

                console.log(JSON.stringify(err))
            }
        })
    }
    else {
        $("#chkAcompañante").bootstrapSwitch('disabled', false)
        $("#chkAcompañante").bootstrapSwitch('state', false)
        $('#divAcompañante').hide();
    }
}
