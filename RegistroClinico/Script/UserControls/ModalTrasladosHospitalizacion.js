﻿$(document).ready(function () {
    $('#lnkExportarTrasladosExcel').click(function () {
        var tblObj = $('#tblTrasladosHospitalizacion').DataTable();
        __doPostBack($(this).data("btnExportarH"), JSON.stringify(tblObj.rows().data().toArray()));
    });
})

function CargarTrasladosHospitalizacion(idHospitalizacion) {

    var adataset = [];
    $("#mdlTrasladosHospitalizacion .heading").text("Traslados de la Hospitalización");
   
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Traslados`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            data.map((e) => {
                adataset = e.Traslados.map((a) => a)
            })
        }
    });

    if (adataset.length == 0)
        $('#lnkExportarTrasladosExcel').addClass('disabled');
    else
        $('#lnkExportarTrasladosExcel').removeClass('disabled');

    $('#lnkExportarTrasladosExcel').data("btnExportarH", "ctl00$Content$btnExportarMovTrasladosH");

    //$('#tblMovHospitalizacion').DataTable().destroy();
    //$('#tblMovHospitalizacion').empty();

    $('#tblTrasladosHospitalizacion').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        destroy: true,
        columns: [
            { title: "Fecha traslado", data: "Fecha" },
            { title: "Cama origen", data: "Origen" },
            { title: "cama destino", data: "Destino" }
        ]
    });

    $('#mdlTrasladosHospitalizacion').modal('show');
    console.log($('#mdlTrasladosHospitalizacion'))
    console.log("Abriendo modal")
}