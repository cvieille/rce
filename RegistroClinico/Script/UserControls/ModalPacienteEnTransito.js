﻿$(document).ready(async function () {

    inicializarCombosPacienteEnTransito()
    mostrarOcultarCombosPacEnTransito()
})

const linkMostrarModalPacienteEnTransito = (sender) => {

    let nombrePaciente = $(sender).data("pac")
    let id = $(sender).data("id")
    let ubicacion = $(sender).data("ubicacion")
    let cama = $(sender).data("cama")

    $('#txtHospitalizacionPacienteEntransito').val(id);
    $('#txtNombrePacienteEntransito').val(nombrePaciente);
    $('#txtNombreCamaPacienteEntransito').val(cama);

    $('#sltTipoTransito').val("0")
    $('#sltUbicacionPacienteTransito').val("0")
    $('#sltEstablecimientoPacienteTransito').val("0")
    $("#txtObservacionPacienteEnTransito").val("")
    $('#btnGuardarTransitoPaciente').data("cama", cama)
    $('#btnGuardarTransitoPaciente').data("id", id)

    mostrarModal("mdlPacienteEnTransito");
}


function validarModalPacienteEnTransito() {

    let valido = true

    if (!validarCampos("#divDatosPacienteEntransito", false))
        return

    return valido
}

async function guardarPacienteEnTransito(e) {

    if (!validarModalPacienteEnTransito())
        return

    let result = {}
    let json = {}
    let liberar = false
    let idHospitalizacion = $(e).data("id")

    result = await Swal.fire({
        title: '¿Desea realizar el traslado?',
        text: "",
        allowOutsideClick: false,
        icon: 'warning',
        //html: mensajeHtml,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI',
        cancelButtonText: 'NO',
    })

    liberar = result.value === true ? true : false

    if (result.value) {

        let fecha = moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:ss");
        let idUbicacion = $("#sltTipoTransito").val() === "1" ? parseInt($("#sltUbicacionPacienteTransito").val()) : null;
        let IdEstablecimiento = parseInt($("#sltTipoTransito").val()) === 2
            ? parseInt($("#sltEstablecimientoPacienteTransito").val())
            : 1;
        let observaciones = $("#txtObservacionPacienteEnTransito").val() !== ""
            ? $("#txtObservacionPacienteEnTransito").val()
            : null;

        json.IdHospitalizacion = idHospitalizacion;
        json.Fecha = fecha;
        json.IdUbicacion = idUbicacion;
        json.IdEstablecimiento = IdEstablecimiento;
        json.Observaciones = observaciones;
        json.liberar = liberar;

        const pacienteEnTransito = await postPacienteEnTransito(json)

        if (pacienteEnTransito.Id > 0) {
            toastr.success('Se creó paciente en tránsito')
            if (!categorizandoDesdeMapaCamas)
                $("#tblHospitalizacion").DataTable().ajax.reload();
            else {
                CargarMapaCamas()
                funcionalidadPacienteEnTransito()
            }
            ocultarModal('mdlPacienteEnTransito')
        }

    }
}

async function postPacienteEnTransito(json) {

    const parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Transito`
    }
    ShowModalCargando(true)
    const respuesta = await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {

        },
        error: async function (jqXHR, status) {
            console.log(`Ha ocurrido un error al intentar crear un paciente en tránsito: ${JSON.stringify(jqXHR)} `)
        }
    })
    ShowModalCargando(false)
    return respuesta
}

function inicializarCombosPacienteEnTransito() {
    comboUbicacionPacienteTransito()
    comboEstablecimientoPacienteTransito()
}

function comboUbicacionPacienteTransito() {

    const url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, $('#sltUbicacionPacienteTransito'))
}

function comboEstablecimientoPacienteTransito() {

    const url = `${GetWebApiUrl()}GEN_Establecimiento/Combo?idTipoEstablecimiento=2`;
    setCargarDataEnCombo(url, false, $('#sltEstablecimientoPacienteTransito'))
}

function mostrarOcultarCombosPacEnTransito() {
    // NUEVA LOGICA EN LA FUNCION, PARA ASOCIAR EL PABELLON

    // Función para restablecer el estado inicial
    function resetearEstado() {
        $("#divUbicacionTransito").hide();
        $("#divEstablecimientoTransito").hide();
        $("#sltUbicacionPacienteTransito").attr("data-required", true);
        $("#sltEstablecimientoPacienteTransito").attr("data-required", false);
        // Ocultar mensajes de validación
        $(".invalid-input").hide();
        // Limpiar clases de validación en los inputs
        $(".form-control").removeClass("is-invalid");
    }

    // Llamando a la función para configurar el estado inicial
    resetearEstado();

    $("#sltTipoTransito").change(function () {
        let value = $(this).val();

        if (value === "1") {
            $("#sltUbicacionPacienteTransito").attr("data-required", true);
            $("#sltEstablecimientoPacienteTransito").attr("data-required", false);
            $("#divUbicacionTransito").show();
            $("#divEstablecimientoTransito").hide();
        } else {
            $("#sltUbicacionPacienteTransito").attr("data-required", false);
            $("#divUbicacionTransito").hide();
            $("#divEstablecimientoTransito").hide();
        }
    });

    // Controlar el evento de apertura del modal para restablecer el estado
    $('#mdlPacienteEnTransito').on('shown.bs.modal', function (e) {
        resetearEstado();
    });
    // Controlar el evento de cierre del modal para restablecer el estado
    $('#mdlPacienteEnTransito').on('hidden.bs.modal', function (e) {
        resetearEstado();
    });

    // LOGICA DE LA FUNCION ORIGINAL
    ////$("#divUbicacionTransito").hide()
    //$("#divEstablecimientoTransito").hide()
    //$("#sltTipoTransito").change(function () {

    //    let value = $(this).val()

    //    if (value === "1") {
    //        $("#sltUbicacionPacienteTransito").attr("data-required", true)
    //        $("#sltEstablecimientoPacienteTransito").attr("data-required", false)
    //        //$("#divUbicacionTransito").show()
    //        //$("#divEstablecimientoTransito").hide()
    //    }
    //    //else if (value === "2") {
    //    //    $("#sltEstablecimientoPacienteTransito").attr("data-required", true)
    //    //    $("#sltUbicacionPacienteTransito").attr("data-required", false)
    //    //    $("#divEstablecimientoTransito").show()
    //    //    $("#divUbicacionTransito").hide()
    //    //}
    //})
}