﻿$(document).ready(function () {
    //funciones que estan en la bandejaHOS.js
    $("#sltCamaServicio").append("<option value=0>Seleccione</option>")
    $('#sltServicio').change(function () {
        CargarCama($(this).val(), "#sltCamaServicio");
    });
    comboServicios('#sltServicio');
})

async function linkMostrarModalCambioCama(element) {
    
    $("#txtMdlTrasladoCama").val($(element).data("pac"))
    $("#inputIdCamaActual").val($(element).data("idcama"))
    $("#inputIdHospActual").val($(element).data("id"))
    $("#txtUbicacionActual").val($(element).data("ubicacion"))
    let camaActual = $(element).data("cama") ?? "N/A"
    let idCamaActual = $(element).data("idcama")
    let idUbicacion = $(element).data("idubicacion")
    $("#txtCamaActualMov").val(camaActual)

    $("#sltServicio").val(idUbicacion)

    if (idUbicacion !== null && idUbicacion !== undefined)
        CargarCama(idUbicacion, '#sltCamaServicio');

    //if (idCamaActual > 0) {
    //    let UbicacionActual = await $.ajax({
    //        type: 'GET',
    //        url: `${GetWebApiUrl()}/HOS_Cama/${idCamaActual}`,
    //        async: false,
    //        success: function (data) {
    //            return data
    //        }, error: function (jqXHR) {
    //            if (jqXHR.status == 404)
    //                toastr.error("Cama no encontrada")
    //        }
    //    });
    //    if (UbicacionActual !== undefined) {
    //        const ubicaicones = await getUbicaciones()
    //        const ubicacion = await findUbicacion(ubicaicones, UbicacionActual.Ubicacion.Valor)
    //        

    //        let camasDisponible = await servicioTieneCamaDisponible(UbicacionActual.Ubicacion.Id)

    //        if (!camasDisponible)
    //            toastr.error("No hay camas disponibles")




    //        $("#txtUbicacionActual").val(UbicacionActual.Ubicacion.Valor)
    //    } else {
    //        $("#txtUbicacionActual").val("No se encontro información.")
    //    }
    //}
    //else {
    //    console.log("no hay cama",idCamaActual)
    //    if (idCamaActual !== undefined)
    //        await mensajeAsignarCama()
    //    return
    //}
    //$("#mdlMovimientoHospitalizacion").modal("show")
    mostrarModal("mdlMovimientoHospitalizacion");
}

async function realizarMovimiento() {
    if ($("#sltCamaServicio").val() == "0") {
        toastr.error("Debe seleccionar una cama")        
    }
    else {
        let idUbicacion = parseInt($("#sltServicio").val())
        let idCama = parseInt($("#sltCamaServicio").val())
        if (!await servicioTieneCamaDisponible(idUbicacion)) {
            toastr.error("No hay camas disponibles")
            return
        }
        else {
            if (idCama === 0)
                toastr.error("Debe seleccionar una cama")
        }
        let json = {}
        //Obtengo los valores desde input con la propiedad hidden divFormMovimiento
        let idHosp = parseInt($("#inputIdHospActual").val())
        let idCamaOrigen = parseInt($("#inputIdCamaActual").val())
        let idCamaDestino = parseInt($("#sltCamaServicio").val())
        //Debo consultar el estado de la cama para saber si esta ocupada.
        await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Cama/${idCamaDestino}`,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                json = data
                json.idHosp = idHosp
                json.idCamaOrigen = idCamaOrigen
                json.idCamaDestino = idCamaDestino
                postTrasladoCamaPaciente(json)
                ShowModalCargando(false)
            }, error: function (err) {
                console.log(err)
            }
        })
    }
    
}
function postTrasladoCamaPaciente(json) {
    let body = {
        "IdHospitalizacion": json.idHosp,
        "IdCamaOrigen": json.idCamaOrigen,
        "IdCamaDestino": json.idCamaDestino,
        "IdTipoTraslado": 3 // Interservicio
    }

    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}HOS_Traslados_Cama`,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Traslado realizado con éxito.',
                showConfirmButton: false,
                timer: 1000,
                allowOutsideClick: false
            }).then((result) => {
                if (!categorizandoDesdeMapaCamas)
                    getTablaHospitalizacion()
                else
                    CargarMapaCamas()
                $("#sltServicio").val(0)
                $("#sltCamaServicio").val(0)
                $("#mdlMovimientoHospitalizacion").modal("hide")
            });
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Traslado: " + JSON.stringify(jqXHR));
        }
    });
}
function comboServicios(element) {
    let url = `${GetWebApiUrl()}GEN_Ubicacion/Hospitalizacion/1`;
    setCargarDataEnCombo(url, false, element);
}