﻿
async function cargarInfoInicialPersona() {

    await cargarCombosPersona()
    
}

async function cargarDatosInicialesPersonaAsync(identificacion, numeroDoc, digito) {

    await comboIdentificacionPersona()

    $("#txtNombrePersona").val("")
    $("#txtApePatPersona").val("")
    $("#txtApeMatPersona").val("")
    $("#txtTelPersona").val("")
    $("#sltTipoGeneroPersona").val("0")
    $("#sltSexoPersona").val("0")

    $("#sltIdentificacionPersona").val($(identificacion).val())
    $("#txtnumeroDocPersona").val($(numeroDoc).val())
    $("#txtDigPersona").val($(digito).val())

    ocultarMostrarFichaPac("#sltIdentificacionPersona", "#txtnumeroDocPersona")
    ChangeTitleNumDoc("#sltIdentificacionPersona", "#txtnumeroDocPersona", "#lblRutPersona")
    ChangeTitleNumDoc()
}

async function cargarCombosPersona() {

    comboTipoGeneroPersona()
    comboSexoPersona()
}

async function comboIdentificacionPersona() {
    
    const url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    setCargarDataEnCombo(url, false, "#sltIdentificacionPersona");
}

async function comboTipoGeneroPersona() {

    const url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoGeneroPersona");
}

async function comboSexoPersona() {

    const url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, false, "#sltSexoPersona");
}
