﻿function mostrarModalComentarios(button) {
    let idAtencionUrgenciaComentarios = $(button).data("id");
    let nombrePacientecomentariosAtencion = $(button).data("nombrepaciente");
    $("#txtNombrePacienteModalComentarios").empty()
    console.log(nombrePacientecomentariosAtencion)
    if (nombrePacientecomentariosAtencion !== null && nombrePacientecomentariosAtencion !== undefined)
        $("#txtNombrePacienteModalComentarios").append(`Paciente: ${nombrePacientecomentariosAtencion}`)

    if (idAtencionUrgenciaComentarios == null || idAtencionUrgenciaComentarios == undefined)
        throw new Error("No se paso id atencion urgencia para cargar comentarios")
    $("#txtIdAtecionUrgenciaComentarios").val(idAtencionUrgenciaComentarios)

    mostrarUsuarioLogueadoEnModales("#modalComentariosAtencion")

    $("#modalComentariosAtencion").modal("show")
    buscarComentarios(idAtencionUrgenciaComentarios)
}
function buscarComentarios(idAtencionUrgencia) {
    promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Comentarios`).then(res => {
        $("#divComentariosAtencion").empty()
        let clase = res.lenght==1? "col-12": "col-6"
        res.forEach(comentario => {
            $("#divComentariosAtencion").append(`
            <div class="${clase}"> 
                <div class="card">
                   <div class="card-header bg-dark"> 
                    <small>Fecha: ${moment(comentario.FechaHora).format("DD-MM-YYYY HH:mm")}, realizado por ${comentario.Profesional.Valor}  </small>
                    </div>
                    <div class="card-body"> 
                    ${comentario.Comentario}
                    </div>
                </div>
            </div>
            `)
        })
    }).catch(error => {
        $("#divComentariosAtencion").empty()
        console.error("Ocurrio un error al intentar buscar los comentarios de la atencion")
        console.log(error)
    })
}
function guardarComentario() {
    if ($("#txtComentarioAtencion").val() == "") {
        toastr.error("Debe ingresar un comentario")
        throw new Error("No se ingreso comentario.")
    }
    let idAtencionUrgenciaComentarios = $("#txtIdAtecionUrgenciaComentarios").val()

    if (idAtencionUrgenciaComentarios == null || idAtencionUrgenciaComentarios == undefined)
        throw new Error("No hay un id atencion de urgencia al cual asociar el comentario")

    $.ajax({
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgenciaComentarios}/Comentario`,
        data: { comentario: $("#txtComentarioAtencion").val() },
        success: function (data) {
            toastr.success("Comentario guardardo")
            $("#txtComentarioAtencion").val("")
            buscarComentarios(idAtencionUrgenciaComentarios)
        }, error: function (error) {
            console.error("Error al intentar guardar un comentario")
            console.log(error)
        }
    })
}