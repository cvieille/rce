﻿
//Inicializa datatable como objeto global

let tabla = $("#tblTipoAlergias").DataTable({
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excel',
            className: 'btn btn-info',
            text: 'Exportar a Excel'
        },
        {
            extend: 'pdf',
            className: 'btn btn-info',
            text: 'Exportar en PDF'
        }
    ],
    columns: [{ title: 'ID', data: 'id' }, { title: 'Alergia', data: 'valor' }, { title: 'Acciones', data: 'acciones' }]
})

if (tabla.column(2).header() !== undefined) {
    tabla.column(1).header().style.width = "80%"
    tabla.column(2).header().style.width = "20%"
}

let selector = document.querySelector("#sltTipoAlergias")
let idPaciente = 0
let idHospitalizacionPac = 0
let hospitalizacion = {}

async function linkMostrarModalAlergias(sender = null, idHospitalizacion = 0, idPacienteAlergia = 0) {
    tabla.column(0).visible(false)
    tabla.column(2).header().style.width = "20%"
    tabla.draw()

    if (sender !== null) {
        idHospitalizacionPac = $(sender).data("id")
        idPaciente = $(sender).data("idpaciente")
    }
    else if (idHospitalizacion !== 0) {
        idHospitalizacionPac = idHospitalizacion
        $("#divHideAlergiaPac").hide()
        tabla.column(2).visible(false)
        tabla.draw()
        $("#divContainerTablaAlergia").removeClass("col-md-12")
        $("#divContainerTablaAlergia").addClass("col-md-8")
    }
    else if (idPacienteAlergia !== 0) {
        idPaciente = idPacienteAlergia
        $("#divDatosHospPac").hide()
    }

    if (idPacienteAlergia === 0) {
        hospitalizacion = await getHospitalizacion(idHospitalizacionPac)
        idPaciente = hospitalizacion.Paciente.Id
    }

    if (idPacienteAlergia !== 0)
        $("#divDatosHosp").hide()

    let alergiasPac = await getPacienteTipoAlergia(idPaciente)

    const adataset = alergiasPac.map(x => {
        return {
            id: x.Alergia.Id,
            valor: x.Alergia.Valor,
            acciones: `
                <button
                    type="button"
                    class="btn btn-outline-danger btn-sm form-control"
                    data-id-alergia="${x.Alergia.Id}"
                    id="delete-${idPaciente}-${x.Alergia.Id}"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Eliminar"
                    onclick="deleteAlergia(this)"
                >
                <i class="fas fa-trash-alt fa-sm"></i>
                </button>`
        }
    })

    tabla.clear()
    tabla.rows.add(adataset).draw()

    if (idPacienteAlergia === 0) {
        let nombre = (sender !== null) ? $(sender).data("pac") : `${hospitalizacion.Paciente.Nombre} ${hospitalizacion.Paciente.ApellidoPaterno} ${hospitalizacion.Paciente.ApellidoMaterno}`
        let cama = (sender !== null) ? $(sender).data("cama") : `${hospitalizacion.Ingreso.Cama.Valor}`

        $("#txtModalAlergias").html(" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Alergias del paciente")
        $("#txtNumHospModalAlergias").val(idHospitalizacionPac)
        $("#txtNomPacModalAlergias").val(nombre)
        $("#txtNumCamaModalAlergias").val(cama)
    }

    comboTipoAlergias()

    if (sender !== null) {
        $("#mdlAlergiasPaciente").modal('show')
    }
}

function comboTipoAlergias() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Alergia/Combo`
    setCargarDataEnCombo(url, false, "#sltTipoAlergias")
}

function actualizarTablaAlergias(alergia) {

    const { id } = alergia
    if (id !== "0")
        tabla.row.add(alergia)

    tabla.draw()
}

async function agregarAlergia() {

    let id = selector.options[selector.selectedIndex].value
    let valor = selector.options[selector.selectedIndex].textContent
    let idAlergia = Number(id)
    let acciones = [
        `<button
            type="button"
            class="btn btn-outline-danger btn-sm form-control"
            data-id-alergia="${id}"
            id="delete-${idPaciente}-${id}"
            data-toggle="tooltip"
            data-placement="bottom"
            title="Eliminar"
            onclick="deleteAlergia(this)"
        >
            <i class="fas fa-trash-alt fa-sm"></i>
        </button>`
    ]

    if (id === "0")
        toastr.warning("Seleccione una opción")

    let alergiasPac = await getPacienteTipoAlergia(idPaciente)

    let alergiaEncontrada = await existeAlergia(alergiasPac, { idPaciente, idAlergia })

    if (!alergiaEncontrada) {

        await GuardarAlergiaPaciente({ idPaciente, idAlergia })
        actualizarTablaAlergias({ id, valor, acciones })
        eliminaElementoSeleccionadoCombo()
    }
    else {
        toastr.error(`El paciente ya posee ${valor}`)
    }
}

const getPacienteTipoAlergia = async (idPaciente) => {

    try {
        const alergias = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/alergias`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return alergias

    } catch (error) {
        console.error("Error al llenar alergias")
        console.log(JSON.stringify(error))
    }
}

async function deleteAlergia(e) {

    const idAlergia = $(e).data("id-alergia")
    const tr = $(e).parent().parent()

    Swal.fire({
        title: `¿Seguro quieres eliminar la alergia?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.value) {

            EliminarAlergia(idPaciente, idAlergia)
            let row = tr
            tabla.row(row).remove().draw();
            toastr.success("Alergia eliminada correctamente")
        }
    })
}

function eliminaElementoSeleccionadoCombo() {

    let value = selector.value

    if (value !== "0") {
        let opcion = selector.querySelector(`[value="${value}"]`)
        selector.removeChild(opcion)
        selector.value = "0"
    }
}

async function GuardarAlergiaPaciente(alergia) {

    const { idPaciente, idAlergia } = alergia

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/Alergia/${idAlergia}`
    }

    if (idAlergia > 0) {
        $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(alergia),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                if (data != undefined)
                    toastr.success(`Se agregó alergia correctamente`)
            },
            error: function (jqXHR, status) {
                console.log(`Error al guardar alergia: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}


async function existeAlergia(alergias, obj) {

    let alergiaEncontrada = await alergias.find(x => x.Alergia.Id === obj.idAlergia && x.Activo === true)

    if (alergiaEncontrada !== undefined)
        return true
    return false
}

async function EliminarAlergia(idPaciente, idAlergia) {

    let parametrizacion = {
        method: "DELETE",
        url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/Alergia/${idAlergia}`
    }

    $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            if (data != undefined)
                toastr.success(`Se ha eliminado la alergia correctamente`)

        },
        error: function (jqXHR, status) {
            console.log(`Error al eliminar la alergia: ${JSON.stringify(jqXHR)} `)
        }
    });
}




