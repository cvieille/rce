﻿
// SIGNOS VITALES
function CargarSignosVitalesId(id) {

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}?extens=SIGNOSVITALES&extens=CATEGORIZACION`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            CargarSignosVitalesArray(data);
        },
        error: function (err) {
            console.log(JSON.stringify(err));
        }
    });

}
function CargarSignosVitalesArray(json) {
    $("#divDetalleSignosVitales").empty();
    if (json.SignosVitales.length > 0)
        CargarCuerpoSignosVitales(json.SignosVitales, json.Categorizacion);
    else if (json.Categorizacion.length > 0)
        CargarCuerpoSignosVitales([], json.Categorizacion);

}
function CargarCuerpoSignosVitales(arraySignosVitales, arrayCategorizacion) {

    if (arrayCategorizacion !== undefined) {
        for (const sv of arrayCategorizacion) {
            if (sv.Datos.length > 0) {
                for (const svc of sv.Datos) {
                    SetTipoMedida(sv.FechaHoraCategorizacion, "FC", svc.FrecuenciaCardiaca, arraySignosVitales);
                    SetTipoMedida(sv.FechaHoraCategorizacion, "FR", svc.FrecuenciaRespiratoria, arraySignosVitales);
                    SetTipoMedida(sv.FechaHoraCategorizacion, "Saturación", svc.Saturacion, arraySignosVitales);
                    SetTipoMedida(sv.FechaHoraCategorizacion, "Temperatura", svc.Temperatura, arraySignosVitales);
                    SetTipoMedida(sv.FechaHoraCategorizacion, "Recursos", svc.Recursos, arraySignosVitales);
                    SetTipoMedida(sv.FechaHoraCategorizacion, "Antecedentes Epidemiologicos", svc.AntecedentesEpidemiologicos, arraySignosVitales);
                }
            }
        }
    }
    

    if (arraySignosVitales.length > 0) {
        $("#divDetalleSignosVitales").append(`<h4 class='mt-2 text-black'>Signos vitales</h4>${GetHtmlSignoVital(arraySignosVitales)}`);
        $("#divSignosVitalesVacio").hide();
        $("#divDetalleSignosVitales").show();
    } else {
        $("#divDetalleSignosVitales").hide();
        $("#divSignosVitalesVacio").show();
    }


}
function GetHtmlSignoVital(arraySignosVitales) {

    let fechaSignoVital = "", html = "";

    for (let i = 0; i < arraySignosVitales.length; i++) {

        if (fechaSignoVital !== arraySignosVitales[i].FechaHoraTipoMedida) {
            html += `<strong class="text-black" style="font-size: large;">
                        <i class='fa fa-angle-double-right'></i> ${moment(arraySignosVitales[i].FechaHoraTipoMedida).format("DD-MM-YYYY HH:mm:ss")}
                    </strong>
                    <div class='w-100 mt-1 p-0'>`;
        }

        html +=
            `<div class="card" style="display:inline-block">
                <div class="card-horizontal">
                    <div class="card-header p-2 text-white bg-dark">${arraySignosVitales[i].NombreTipoMedida}</div>
                    <div class="card-body p-2">${arraySignosVitales[i].ValorTipoMedida}</div>
                </div>
            </div>`;

        fechaSignoVital = arraySignosVitales[i].FechaHoraTipoMedida;

        if ((i + 1) === arraySignosVitales.length || fechaSignoVital !== arraySignosVitales[i + 1].FechaHoraTipoMedida)
            html += "</div>";
    }

    return html;

}
function SetTipoMedida(FechaHoraTipoMedida, NombreTipoMedida, ValorTipoMedida, array) {

    if (ValorTipoMedida != null)
        array.push({
            FechaHoraTipoMedida: FechaHoraTipoMedida,
            NombreTipoMedida: NombreTipoMedida,
            ValorTipoMedida: ValorTipoMedida
        });

}

function MostrarSignosVitales(sender) {

    $("#modalAlerta").modal("hide");
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Tipo_Medida/Combo?idTipoModulo=1`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            $('#lblPacienteSignosVitales').html($(sender).data('pac'));

            $("#divSignosVitales").empty();
            $("#divSignosVitales").append(
                `
                    <h5 class="mb-3"><strong><i class='fa fa-heartbeat'></i> Signos vitales</strong></h5>
                    <div class='row'></div>
                `);

            $.each(data, function (i, r) {
                let type = "number";
                if (r.Formato === "str") type = "text";

                let regex = r.Regex ?? null
                if (regex !== null) regex = `/${regex}/`;

                $("#divSignosVitales .row").append(
                    `<div class='col-md-2 col-sm-6 mt-2'>
                        <label>${r.Valor}</label>
                        <input  id='sltSignoVital_${r.Id}' 
                                name='${r.Id}' 
                                type='${type}'
                                maxlength='${r.Maximo}'
                                data-id='${r.Id}'
                                data-informacion='${r.Informacion}'
                                data-regex=${r.Regex}
                                onkeypress="if(this.value.length==${r.Maximo}) return false;"
                                oninput="validarRegex(this,${regex});"
                                class='form-control ${type}' 
                                placeholder='${r.Valor} (${r.Informacion})'
                                autocomplete="off"                                
                        />
                    </div>`);

                $(`#sltSignoVital_${r.Id}`).data("id", r.Id);

            });

            $(".number").unbind();
            ValidarNumeros();
            $("#aGuardarSignoVital").data("id", $(sender).data('id'));
            $("#mdlSignosVitales").modal("show");

        }
    });

}