﻿$(document).ready(function () {
    
});

function ShowModalAlerta(tipo, titulo, mensaje, functionAceptar, functionCancelar, txtAceptar, txtCerrar) {

    $("#iMensaje").attr('class', 'fa fa-4x mb-3 animated rotateIn');
    $('#modalAlerta .modal-header').attr('class','modal-header');
    $('#btnAlertaAceptar').hide();
    $('#btnAlertaCerrar').attr('data-dismiss', 'modal');
    $('#btnAlertaCerrar').off('click');
    $('#btnAlertaAceptar').text('ACEPTAR');
    $('#btnAlertaCerrar').text('CANCELAR');

    if (txtAceptar != undefined)
        $('#btnAlertaAceptar').text(txtAceptar);
    
    if (txtCerrar != undefined)
        $('#btnAlertaCerrar').text(txtCerrar);

    switch (tipo) {
        case 'INFORMACION':
            $('#iMensaje').addClass('fa-info-circle text-info');
            $('#modalAlerta .modal-header').addClass('modal-header-info');
            break;
        case 'ERROR':
            $('#iMensaje').addClass('fa-remove text-danger');
            $('#modalAlerta .modal-header').addClass('modal-header-danger');
            break;
        case 'ADVERTENCIA':
            $('#iMensaje').addClass('fa-exclamation-triangle text-warning');
            $('#modalAlerta .modal-header').addClass('modal-header-warning');
            break;
        case 'EXITO':
            $('#iMensaje').addClass('fa-check text-success');
            $('#modalAlerta .modal-header').addClass('modal-header-success');
            break;
        case 'CONFIRMACION':
            $('#iMensaje').addClass('fa-question-circle text-warning');
            $('#modalAlerta .modal-header').addClass('modal-header-warning');
            $('#btnAlertaAceptar').show();
            break;
    }

    $('#hTitulo').html(titulo);
    $('#pMensaje').html(mensaje);

    if (functionAceptar !== null) {
        $('#btnAlertaAceptar').off('click');
        $('#btnAlertaAceptar').on('click', function () {
            functionAceptar();
        });
    }
    
    if (functionCancelar !== null && functionCancelar != undefined) {
        $('#btnAlertaCerrar').removeAttr('data-dismiss');        
        $('#btnAlertaCerrar').on('click', function () {
            functionCancelar();
            $('#modalAlerta').modal('hide');
        });
    }

    $('#modalAlerta').modal('show');

}