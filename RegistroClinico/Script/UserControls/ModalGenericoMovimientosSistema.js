﻿$(document).ready(async function () {
    nSession = getSession();

    $('#mdlGenericoMovimientosSistema').on('hidden.bs.modal', function () {
        volverDetallesUsuarioVista();
    });
});

// OBTIENE LOS MOVIMIENTOS DE SISTEMA Y DIBUJA LA TABLA
function getMovimientosSistemaGenerico(url, id) {
    let adataset = [];
    let titulo = "";
    let nombrePaciente = "";

    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            const { Paciente, Movimiento } = data ?? {};

            if (Paciente != undefined) {
                nombrePaciente = document.getElementById("nombrePaciente");
                nombrePaciente.innerHTML = `<span>Paciente: </span><strong> ${getNombreCompleto(Paciente)}</strong>`;
            } else {
                nombrePaciente = document.getElementById("nombrePaciente");
                nombrePaciente.innerHTML = "";
            }

            if (Movimiento != undefined) {
                Movimiento.forEach(x => adataset.push([x.Id, moment(x.Fecha).format('DD-MM-YYYY HH:mm:ss'), x.Usuario, x.TipoMovimiento,x.Detalle]));
            }

            $("#detallesTablaGenerico").show();
            $("#tblGenericoMovimientosSistema").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        className: 'btn btn-info',
                        text: 'Exportar a Excel'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-info',
                        text: 'Exportar en PDF'
                    }
                ],
                data: adataset,
                order: [],
                columns: [
                    { title: "ID" },
                    { title: "Fecha Movimiento" },
                    { title: "Usuario" },
                    { title: "Movimiento" },
                    { title: "Detalle" }
                ],
                "columnDefs": [
                    {
                        "targets": 0,
                        "visible": false
                    },
                    {
                        "targets": 2,
                        "sType": "date-ukLong"
                    },
                    {
                        targets: [2],
                        render: function (data, type, row, meta) {
                            return `<a href="javascript:void(0);" data-usuario="${data}" 
                                    data-toggle="tooltip" data-placement="top" 
                                    onclick="mostrarDetallesUsuario(event, this);">
                                    ${data}
                                    </a>`;
                        }
                    },
                    {
                        targets: [4],
                        render: function (data, type, row, meta) {
                            const [Id, Fecha, , TipoMovimiento] = row;
                            const existenDetalles = data;

                            const idAtencionUrgencia = id.replace("Urgencia #", "");
                            return (existenDetalles) ?
                                    `<a  data-toggle="tooltip"
                                        data-placement="top"
                                        title="Ver Detalles"
                                        class="btn btn-info"
                                        onclick="mostrarDetalles(${ idAtencionUrgencia}, ${Id}, '${Fecha}', '${TipoMovimiento}');">
                                        <i class="fa fa-list nav-icon"></i>
                                    </a>` : "No hay detalles";
                        }
                    }
                ],
                responsive: true,
                "bDestroy": true
            });

            titulo = document.getElementById("tituloMovimientos");
            titulo.textContent = id;
            $("#mdlGenericoMovimientosSistema").modal('show');
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
                console.log("No encontrado");
            }
            console.log(`Error ${JSON.stringify(jqXHR)}`);
        }
    });
}

// Función para llamar a la vista del detalle usuario

function mostrarDetalles(idAtencionUrgencia, idUrgenciaMov, fecha, tipoMov) {
    //$("#datos-Movimiento-tab").trigger("click");
    getMovimientosDetalle(idAtencionUrgencia, idUrgenciaMov)
        .then(mov => {
            $("#divDetalleMovimientos h5").html(`Movimiento (${fecha} - ${tipoMov})`);
            const { Usuario } = mov;
            $("#divDetalles").html((Usuario != null) ? Usuario.Detalle : "<strong>No hay detalles a mostrar</strong>");
            $("#datos-detalle-movimientos-tab").trigger("click");
        })
        .catch(error => {
            console.error("Error al cargar Detalle Movimientos");
            console.log(JSON.stringify(error));
        });
    
}
function mostrarDetallesUsuario(event, e) {
    event.preventDefault();
    let login = $(e).data("usuario");
    getUsuarioMovimientosGenericos(login)
        .then(usuarioSeleccionado => {
            mostrarDetallesUsuarioVista(usuarioSeleccionado);
        })
        .catch(error => {
            console.error("Error al cargar usuario");
            console.log(JSON.stringify(error));
        });
}

// Función para mostrar la vista de detalles del usuario
function mostrarDetallesUsuarioVista(usuarioSeleccionado) {
    if (usuarioSeleccionado.length === 1) {
        const { Nombre, ApellidoPaterno, ApellidoMaterno, Numerodocumento, Digito, Email, Sexo } = usuarioSeleccionado[0];
        const nombreCompleto = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`;
        const rutCompleto = `${Numerodocumento}-${Digito}`;
        $("#detalleNombre").val(nombreCompleto);
        $("#detalleNumeroDocumento").val(rutCompleto);
        $("#detalleEmail").val(Email || "No posee");
        $("#detalleSexo").val(Sexo.Valor);
        // Oculta la tabla y la paginación
        $("#detallesTablaGenerico").hide();
        // Muestra la vista de detalles del usuario
        $("#detallesUsuarioVista").show();
    }
}

// Función para cerrar la vista de detalles del usuario y volver al cuerpo principal
function volverDetallesUsuarioVista() {
    // Muestra la tabla y la paginación
    $("#detallesTablaGenerico").show();
    // Oculta la vista de detalles del usuario
    $("#detallesUsuarioVista").hide();
}

// Función para obtener detalles del usuario mediante su login
async function getMovimientosDetalle(idAtencionUrgencia, idUrgenciaMov) {
    try {
        const movimiento = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Movimientos/${idUrgenciaMov}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return movimiento;
    } catch (error) {
        throw error;
    }
}

async function getUsuarioMovimientosGenericos(login) {
    try {
        const usuario = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Usuarios?login=${login}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return usuario;
    } catch (error) {
        throw error;
    }
}