﻿let datosExportarHE = [];


function GetTablaHojaEvualuacion(idPaciente, idEvento) {
    var adataset = [];
    var sBaseURL = GetWebApiUrl() + "HOS_Hoja_Evolucion/Bandeja?idPaciente=" + idPaciente;
    $.ajax({
        type: "GET",
        url: sBaseURL,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    /*0*/val.Id,
                    /*1*/val.TipoEstado.Id,
                    /*2*/moment(moment(val.Fecha).toDate()).format('DD-MM-YYYY'),
                    /*3*/val.Diagnostico,
                    /*4*/val.NombreProfesional,
                    /*5*/val.TipoEstado.Valor,
                    //Acciones
                    /*6*/val.Acciones.Editar,
                    /*7*/val.Acciones.Imprimir,
                    /*8*/val.Acciones.Validar,
                    /*9*/val.Acciones.VerMovimientos
                ]);
            });

            setJsonExportarHOSHE(data)
            LlenarGrillaHojasEvolucion(adataset, '#tblHojasEvolucion', sSession);
        }
    });

    $('body').on('click', '#btnVolverCarousel', function(e){
        e.preventDefault();
    });
}

function LlenarGrillaHojasEvolucion(datos, grilla, sSession) {
    
    $(grilla).DataTable({
        data: datos,
        columns: [
            { title: "id_HojaEvolucion" },
            { title: "GEN_idTipo_Estados_Sistemas" },
            { title: "Fecha H.E." },
            { title: "Diagnóstico" },
            { title: "Profesional" },
            { title: "Estado" },
            { title: "", className: "text-center"}
        ],
        lengthMenu: [[5, 10], [5, 10]],
        columnDefs: [
            {
                targets: 2,
                sType: "date-ukLong"
            },
            {
                targets: -1,
                data: null,
                orderable: false,
                searchable: false,
                render: function (data, type, row, meta)
                {
                    var fila = meta.row;
                    var botones;

                    botones = `
                                <button class='btn btn-primary btn-circle'
                                    onclick='toggle("#div_accionesHojaEvolucion${fila}"); return false;'>
                                    <i class="fa fa-list" style="font-size:15px;"></i>
                                </button>
                                    <div id='div_accionesHojaEvolucion${fila}' class='btn-container' style="position:absolute; z-index: 2000; display: none;">
                                        <center>
                                            <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                            <div class="rounded-actions">
                                                <center>
                            `;

                    if (datos[fila][6])//editar 
                        botones += `    <a data-id='${datos[fila][0]}' type='button' class='btn-circle btn btn-success load-click' onclick='linkEditarHojaEvolucion(this);'
                                            data-toggle='tooltip' data-placement='top' title='Editar H.E.'>
                                            <i class='fa fa-edit'></i>
                                        </a>`;
                    if (datos[fila][7])//Imprimir
                        botones += `    <a data-id='${datos[fila][0] }' type='button' class='btn-circle btn btn-info load-click' data-print='true' data-frame='#frameHojaEvolucion'
                                            data-toggle='tooltip' data-placement='top' title='Imprimir H.E.' onclick='linkImprimirHojasEvolucion(this);'>
                                            <i class='fa fa-print'></i>
                                        </a>`;
                    if (datos[fila][8])//Validar
                        botones += `    <a data-id='${datos[fila][0]}' type='button' class='btn-circle btn btn-success load-click' onclick='linkValidarHojaEvolucion(this);'
                                        data-toggle='tooltip' data-placement='top' title='Validar H.E.' >
                                            <i class='fa fa-check'></i>
                                        </a>`;
                    if (datos[fila][9])//Ver movimientos
                        botones += `     <a data-id='${datos[fila][0]}' type='button' class='btn-circle btn btn-info' data-toggle='tooltip' data-placement='top'
                                            title='Ver movimientos' onclick='linkVerMovimientosHojaEvolucion(this);'>
                                            <i class='fa fa-list'></i>
                                        </a>`;

                    botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >
                            `;

                    return botones;
                }
            },
            {
                "targets": [0, 1],
                "visible": false
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) { },
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
    $(grilla).css("width", "100%");

}      
function linkVerHojasEvolucion(sender) {

    $('#txtTitle').html('Hoja de evolución');
    $('#divCarouselHos').carousel(0);
    let nombrePaciente = $(sender).data("nombrepaciente")
    var idPaciente = ($(sender).data("id") === undefined ? '' : $(sender).data("id"));
    var idEvento = ($(sender).attr("data-idEvento") === undefined ? '' : $(sender).attr("data-idEvento"));

    var idhos = ($(sender).data("idhos") === undefined ? '' : $(sender).data("idhos"));
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/HojaIngresoEgreso/${idhos}`,
        async: false,
        success: function (data, status, jqXHR) {
            //GEN_idUbicacion = data[0].GEN_idUbicacion_Ingreso;
            $('#txtPacienteHE').val(data[0].GEN_nombrePaciente + ' ' + data[0].GEN_ape_paternoPaciente + ' ' + data[0].GEN_ape_maternoPaciente);
            $('#txtCamaActualHE').val(data[0].HOS_nombreCamaActual);
        }
    });
    if (idhos == '') {

        $('#txtPacienteHE').val(nombrePaciente)
    }

    $("#linkNuevaHojaEvolucion").data("idPaciente", idPaciente);
    $("#linkNuevaHojaEvolucion").data("idEvento", idEvento);
    $("#linkNuevaHojaEvolucion").data("idHospitalizacion", idhos);

    GetTablaHojaEvualuacion(idPaciente, idEvento);
    $("#mdlVerHojasEvolucion").modal('show');

}
function linkCrearNuevaHojaEvolucion() {

    var idPaciente = $("#linkNuevaHojaEvolucion").data("idPaciente");
    var idEvento = $("#linkNuevaHojaEvolucion").data("idEvento");
    var idHospitalizacion = $("#linkNuevaHojaEvolucion").data("idHospitalizacion");
    setSession('ID_PACIENTE', idPaciente);
    if (idHospitalizacion != undefined)
        setSession('ID_HOSPITALIZACION', idHospitalizacion);
    if (idEvento != undefined)
        setSession('ID_EVENTO', idEvento);
    
    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevaHojaEvolucionHOS.aspx";

}
function linkEditarHojaEvolucion(sender) {
    var idHospitalizacion = $("#linkNuevaHojaEvolucion").data("idHospitalizacion");
    if (idHospitalizacion != undefined)
        setSession('ID_HOSPITALIZACION', idHospitalizacion);
    var id = $(sender).data("id");
    setSession('ID_HOJAEVOLUCION', id);    
    window.location.replace(ObtenerHost() + "/Vista/ModuloHOS/NuevaHojaEvolucionHOS.aspx");
}
function linkValidarHojaEvolucion(sender) {

    var idHE = $(sender).data("id");
    var jsonHE = null;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Hoja_Evolucion/${idHE}`,
        async: false,
        success: function (data) {
            // 81 = Hoja de Evolución Validada
            json = data;
            json.GEN_idTipo_Estados_Sistemas = 81;
        }
    });
    
    var jsonr = {
        "idPaciente" : json.Paciente.IdPaciente,
        "idHojaEvolucion" : json.Id,
        "idProfesional" : json.Profesional.IdProfesional,
        "IdUbicacion" : json.Ubicacion.Id,
        "Fecha" : json.Fecha,
        "Descripcion" : json.Descripcion,
        "Evolucion" : json.Evolucion,
        "ExamenFisico" : json.ExamenFisico,
        "GEN_idTipo_Estados_Sistemas" : json.TipoEstadoSistema.Id,
        "FechaLimite" : json.FechaLimite,
        "Diagnostico" : json.DiagnosticoPrincipal,
        "OtrosDiagnostico": json.OtrosDiagnostico,
        "IdHospitalizacion" : sSession.ID_HOSPITALIZACION

    }


    
    $.ajax({
        type: "PATCH",
        url: `${GetWebApiUrl()}HOS_Hoja_Evolucion/Validar/${idHE}`,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonr),
        async: false,
        success: function (data) {
            toastr.success('La Hoja de Evolución se a validado exitosamente.');
        }
    });

    GetTablaHojaEvualuacion(jsonr.idPaciente, '');

}

function linkImprimirHojasEvolucion(sender) {
    let id = $(sender).data("id");
    const url = `${GetWebApiUrl()}HOS_Hoja_Evolucion/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

async function linkVerMovimientosHojaEvolucion(sender) { 

    var id = $(sender).data("id");
    var adataset = [];
    console.log(id)
    await $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}HOS_Hoja_Evolucion/${id}/Movimientos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.Id,
                    moment(moment(r.Fecha).toDate()).format('DD-MM-YYYY HH:mm:ss'),
                    r.NombreUsuario,
                    r.TipoMovimiento.Valor
                ]);
            });
        }, error: function (err) {
            console.error("Ha ocurrido un error al intentar buscar los movimientos de la hoja de evolucion")
            console.log(err)
        }
    });
    $('#tblMovHE').addClass("nowrap").DataTable({
        data: adataset,
        lengthMenu: [[5, 10], [5, 10]],
        columns: [
            { title: "RCE_idMovimientos_Interconsulta" },
            { title: "Fecha Movimiento" },
            { title: "Usuario" },
            { title: "Movimiento" }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            { "targets": 1, "sType": "date-ukLong" }
        ],
        "bDestroy": true
    });
    $('#txtTitle').html('Movimientos');
    $('#divCarouselHos').carousel(1);
    $("#tblMovHE").css("width", "100%");
    
    //$('#mdlVerHojasEvolucion').modal('hide');
    $('#mdlMovimientoHE').modal('show');
}

function setJsonExportarHOSHE(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarHE.push([
            formatDate(Object.Fecha),
            DevolverString(Object.Diagnostico),
            DevolverString(Object.NombreProfesional)
        ]);
    });
}