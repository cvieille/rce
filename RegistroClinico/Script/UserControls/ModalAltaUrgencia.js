﻿$(document).ready(function () {
    InicializarAlta();
});

// INICIAR COMPONENTES
function InicializarAlta() {

    const { CODIGO_PERFIL } = getSession();

    ComboDestinoAltaPaciente()
    ComboPronosticoMedicoLegal()
    comboMotivoCierre()
    comboProcesoFallecimiento();
    CargarComboUbicacion();

    if (CODIGO_PERFIL !== perfilAccesoSistema.digitadorUrgencia) {
        $("#divFechaHoraAlta, #divFechaHoraAtencion, #btnEgresarAltaDigitador").hide();
        $("#divFechaHoraAlta input, #divFechaHoraAtencion input").removeAttr("data-required");
    } else {
        $("#btnEgresarAltaDigitador").show();
        $("#aSiguienteTabDiagnostico").hide();
        $("#datos-indicaciones-tab").parent().hide();
    }

    if (CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) {
        $("#divFechaHoraAlta, #divFechaHoraAtencion").hide();
        $("#divFechaHoraAlta input, #divFechaHoraAtencion input").attr("data-required", "true");
    }

    // TIPO EGRESO NORMAL
    $("input[name='TE']").on('switchChange.bootstrapSwitch', function (event, state) {

        const id = $(this).attr("id");
        ReiniciarAlta();

        if (id === "rdoTipoEgresoNormal" && $(this).bootstrapSwitch('state')) {

            if (CODIGO_PERFIL == perfilAccesoSistema.medico ||
                CODIGO_PERFIL == perfilAccesoSistema.internoMedicina ||
                CODIGO_PERFIL == perfilAccesoSistema.digitadorUrgencia ||
                CODIGO_PERFIL == perfilAccesoSistema.matroneriaUrgencia ||
                CODIGO_PERFIL == perfilAccesoSistema.administradorDeUrgencia) {

                $("#divCondicionPaciente").show();
                $("#divEgresoNormal, #divEgresoIrregular").hide();

            }

        } else {

            $("#divEgresoIrregular, #btnEgresarAlta").show();
            $("#divEgresoNormal, #divCondicionPaciente").hide();
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('radioAllOff', true);
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('state', false);
            $("#rdoVivo, #rdoFallecido").bootstrapSwitch('radioAllOff', false);

        }

    });

    // TIPO EGRESO ADMINISTRATIVO
    $("input[name='CP']").on('switchChange.bootstrapSwitch', function (event, state) {
        const id = $(this).attr("id");
        $("#btnEgresarAlta").show();

        if ($("#rdoTipoEgresoNormal").bootstrapSwitch('state')) {
            $("#divEgresoNormal").show();
        }

        if (id === "rdoFallecido" && $(this).bootstrapSwitch('state')) {
            $("#sltDestinoAltaPaciente option[value='5']").show();
            $("#sltDestinoAltaPaciente").val("5").attr("disabled", "disabled").change();
        } else {
            $("#sltDestinoAltaPaciente").val("0").removeAttr("disabled");
            $("#sltDestinoAltaPaciente option[value='5']").hide();
            $("#sltDestinoAltaPaciente").val("0");
            $('#divFallecimiento').hide();
            $('#sltProcesoFallecimiento').removeAttr('data-required');
        }
    });

}


// ALTA
async function linkEgresoIngresoUrgencia(objAtencionUrgencia, esHipotesisDiagnostica = false) {

    //reset de los elementos para editar
    $("#alertEditandoAlta").empty()
    $("#txtIdAltaUrgencia").val("")

    ShowModalCargando(true);
    const { CODIGO_PERFIL } = getSession();
    _idDau = objAtencionUrgencia.idAtencion;
    if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia) {
        if (await dauTieneAltasMedica(_idDau))
            return;
    }

    ComboDestinoAltaPaciente()
    $("#ComboPronosticoMedicoLegal").empty()
    $("#sltUbicacionDerivado").empty()
    ComboDestinoDerivacion()
    CargarComboUbicacion();
    ComboPronosticoMedicoLegal()
    $("#sltMotivoCierre").empty()
    comboMotivoCierre()
    $("#sltProcesoFallecimiento").empty()
    comboProcesoFallecimiento()


    InicializarDatosDiagnostico("Content_ModalAltaUrgencia_wuc_diagnosticoAltaUrgencia");
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divMasInformacionUrgencia");

    $("#sltDestinoAltaPaciente").val("0")

    const { tipoEstado } = objAtencionUrgencia;

    // Configura los switches
    if ((CODIGO_PERFIL !== perfilAccesoSistema.digitadorUrgencia && [100, 101, 102, 103].includes(tipoEstado.Id))
        || CODIGO_PERFIL === perfilAccesoSistema.enfermeraUrgencia) {
        $("#txtMotivoCierrePaciente").val("");
        $("#rdoTipoEgresoNormal").bootstrapSwitch('state', false);
        $("#rdoTipoEgresoIrregular").bootstrapSwitch('state', true); // Desactivar switch administrativo
        $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('disabled', true);
    }
    else {

        if (!await validarDiagnosticosAtencionMedica(_idDau)) {
            ShowModalCargando(false);
            return;
        }
        //egreso administrativo deshabilitado
        $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('disabled', false);
        $("#rdoTipoEgresoIrregular, #rdoFallecido").bootstrapSwitch('disabled', false);
        $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('radioAllOff', true);
        $("#rdoTipoEgresoNormal").bootstrapSwitch('state', true);  // Activar switch clínico
        $("#rdoTipoEgresoIrregular").bootstrapSwitch('state', false); // Desactivar switch administrativo
        $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular").bootstrapSwitch('radioAllOff', false);
        $("#rdoVivo").bootstrapSwitch('state', true); // Activar switch "vivo"

        if (CODIGO_PERFIL == 12)//interno de medicina deshabilitar alta administrativa
            $("#rdoTipoEgresoIrregular").bootstrapSwitch('disabled', true);
        // Dispara manualmente los eventos
        $("#rdoTipoEgresoNormal").trigger('switchChange.bootstrapSwitch');
        $("#rdoVivo").trigger('switchChange.bootstrapSwitch');

        // Mostrar divEgresoMedico si ambos switches están en ON
        if ($("#rdoTipoEgresoNormal").bootstrapSwitch('state') && $("#rdoVivo").bootstrapSwitch('state')) {
            $("#divEgresoNormal, #divEgresoMedico").css("display", "block");
        }
    }

    // Limpia los valores de los campos
    $(`#txtDNumeroDau, #txtDPaciente, #txtDMedicoTratante, #txaHipotesis, #txaExamenFisico, #txaAnamnesis, #txaIndicacionesAlta, #txaMedicamentosAlta`).val("");

    // Maneja el evento click para egresar el paciente metodo antiguo con a
    //$("#btnEgresarAlta, #btnEgresarAltaDigitador").unbind().click(function () {
    //    EgresarDarAltaPaciente(objAtencionUrgencia.idAtencion);
    //});

    //$("#btnEgresarAltaAdministrativa").unbind().click(function () {
    //    EgresarDarAltaAdministrativaPaciente(objAtencionUrgencia.idAtencion);
    //});

    // Maneja el evento click para egresar el paciente button
    $("#btnEgresarAlta, #btnEgresarAltaDigitador").unbind().click(async function (event) {
        event.preventDefault();
        $(this).prop('disabled', true); // Deshabilita el botón

        try {
            await EgresarDarAltaPaciente(objAtencionUrgencia.idAtencion);
        } catch (error) {
            console.error(error);
        } finally {
            $(this).prop('disabled', false); // Habilita el botón después de la acción
        }
    });

    // Maneja el evento click para el alta administrativa del paciente
    $("#btnEgresarAltaAdministrativa").unbind().click(async function (event) {
        event.preventDefault();
        $(this).prop('disabled', true); // Deshabilita el botón

        try {
            await EgresarDarAltaAdministrativaPaciente(objAtencionUrgencia.idAtencion);
        } catch (error) {
            console.error(error);
        } finally {
            $(this).prop('disabled', false); // Habilita el botón después de la acción
        }
    });

    // Maneja el evento click para pasar a la siguiente pestaña de datos generales
    $("#aSiguienteDatosGenerales").click(function () {
        const { CODIGO_PERFIL } = getSession();
        if (validarCampos("#divDatosAdministrativos", false)) {
            if ((CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia ||
                CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) && !validarFechas()) return;
            $('#datos-diagnostico-tab').trigger('click');
            $('#mdlEgresarAtencion').animate({ scrollTop: $("#divEgresoNormal").offset().top }, 500);
        }
    });

    //cargarDiagnostico(objAtencionUrgencia.idAtencion);
    if (esHipotesisDiagnostica)
        cargarHipotesisDiagnostica(objAtencionUrgencia.idAtencion);

    // Maneja el evento click para pasar a la siguiente pestaña de diagnóstico
    $("#aSiguienteTabDiagnostico").click(function () {
        if (existenDiagnosticosAgregados())
            $('#datos-indicaciones-tab').trigger('click');
    });

    // Realiza cualquier cambio necesario en el destino de alta
    ChangeDestinoAlta();

    // Carga la información del DAU
    CargarInfoDAU(objAtencionUrgencia.idAtencion);

    if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia) {
        $("#txtFechaHoraAtencion, #txtHoraAtencion").val("");
        $("#txtFechaHoraAtencion, #txtHoraAtencion").removeAttr("disabled");
        await cargarDatosDigitadora(_idDau);
    }

    // Muestra el modal
    $('#mdlEgresarAtencion').modal('show');
    ShowModalCargando(false);

}

async function validarDiagnosticosAtencionMedica(idDau) {

    let esValido = true

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Diagnosticos`,
        success: function (data) {

        }, error: function (jqXHR) {

            if (jqXHR.status === 404) {

                ShowModalCargando(false);
                Swal.fire({
                    title: 'Error',
                    text: "Se debe agregar al menos un diagnóstico.",
                    icon: 'warning',
                    position: 'center',
                    showConfirmButton: true,
                })

                esValido = false
            }
        }
    })

    return esValido;
}

async function cargarAtencionMedico(idDau) {

    let ingresoMedico = null;
    try {

        await $.ajax({
            method: "GET",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idDau}/Medico`,
            success: function (ingresosmedico) {
                ingresoMedico = ingresosmedico[0];
            }, error: function (jqXHR) {
                if (jqXHR.status === 404);
            }
        });

    } catch (ex) {

    }

    return ingresoMedico;
}

async function cargarDatosDigitadora(idDau) {
    const ingresoMedico = await cargarAtencionMedico(idDau);
    if (ingresoMedico != null) {
        const fecha = moment(ingresoMedico.Fecha);
        $("#txtFechaHoraAtencion").val(fecha.format("YYYY-MM-DD"));
        $("#txtHoraAtencion").val(fecha.format("HH:mm"));
        $("#txtFechaHoraAtencion, #txtHoraAtencion").attr("disabled", "disabled");
    }
}
async function dauTieneAltasMedica(idDau) {

    let esValido = true;
    try {

        await $.ajax({
            method: "GET",
            url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Alta/Medica`,
            success: function (data) {

                ShowModalCargando(false)

                Swal.fire({
                    title: 'Error',
                    text: "Paciente ya tiene un alta médica.",
                    icon: 'warning',
                    position: 'center',
                    showConfirmButton: true,
                });

            }, error: function (jqXHR) {

                if (jqXHR.status === 404) {
                    esValido = false;
                }
            }
        });

    } catch (ex) {

    }

    return esValido;
}


function validarFechas() {

    const fechaActual = GetFechaActual();
    const fechaLlegada = moment($("#txtFechaLlegada").val(), "DD-MM-YYYY HH:mm");
    const fechaCategorizacion = $("#txtFechaCategorizacion").val() != "" ? moment($("#txtFechaCategorizacion").val(), "DD-MM-YYYY HH:mm") : null;
    const fechaAtencion = moment(`${$("#txtFechaHoraAtencion").val()} ${$("#txtHoraAtencion").val()}`);
    const fechaAlta = moment(`${$("#txtFechaAlta").val()} ${$("#txtHoraAlta").val()}`);

    if (fechaCategorizacion == null) {
        Swal.fire({ title: 'Error', text: 'No se puede egresar un paciente que no se haya categorizado.', icon: 'warning' });
        return false;
    }

    if (fechaAtencion > fechaActual) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Atención no pueden ser mayor a fecha actual', icon: 'warning' });
        return false;
    }
    else if (fechaAlta > fechaActual) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Alta no pueden ser mayor a fecha actual', icon: 'warning' });
        return false;
    }
    else if (fechaAtencion > fechaAlta) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Atención no pueden ser mayor a la fecha de Alta', icon: 'warning' });
        return false;
    }
    else if (fechaCategorizacion != null && fechaAlta < fechaCategorizacion) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Alta no puede ser menor a la Fecha de categorización', icon: 'warning' });
        return false;
    }
    else if (fechaAlta < fechaLlegada) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Alta no puede ser menor a la Fecha de Llegada', icon: 'warning' });
        return false;
    }
    else if (fechaCategorizacion != null && fechaAtencion < fechaCategorizacion) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Atención no puede ser menor a la Fecha de categorización', icon: 'warning' });
        return false;
    }
    else if (fechaAtencion < fechaLlegada) {
        Swal.fire({ title: 'Error', text: 'La Fecha y hora de Atención no puede ser menor a la Fecha de Llegada', icon: 'warning' });
        return false;
    }

    return true;

}

function ReiniciarAlta() {

    const { CODIGO_PERFIL } = getSession();
    if (CODIGO_PERFIL === perfilAccesoSistema.medico ||
        CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia ||
        CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia ||
        CODIGO_PERFIL === perfilAccesoSistema.administradorDeUrgencia) {
        CargarDatosMedicoAlta();
    }

    if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia)
        CargarMedicosParaEgreso();

    //comboMotivoCierre();

    $("#datos-administartivos-tab").trigger('click');

    $(`#sltMotivoCierre, #sltDestinoAltaPaciente, #sltEstablecimientoAltaPaciente, #sltUbicacionDerivado,
        #sltDestinoDerivacion, #sltPronosticoMedicoLegal, #sltProcesoFallecimiento`).val("0");

    $("#txtMotivoCierrePaciente").val("");

    $("#divEgresoNormal, #divEgresoIrregular, #divCondicionPaciente, #btnEgresarAlta, #divAtencionClinica").hide();
    $(`#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion, #sltProcesoFallecimiento`).parent().hide();

    limpiarDiagnosticos();

}

async function EgresarDarAltaPaciente(idAtencionUrgencia) {
    const esValidaTodaInformacion = await validarAltaCompleta();
    if (esValidaTodaInformacion)
        ShowMensajeEgreso(idAtencionUrgencia);
}

function EgresarDarAltaAdministrativaPaciente(idAtencionUrgencia) {
    if (validarCampos("#divEgresoIrregular", false))
        ShowMensajeEgreso(idAtencionUrgencia);
}
function ShowMensajeEgreso(id) {

    let nombreUsuario = obtenerNombreUsuario();
    let idAltaUrgenciaEditar = $("#txtIdAltaUrgencia").val()
    let tituloAdvertencia = 'Está a punto de dar de Alta a este paciente'
    if (idAltaUrgenciaEditar !== "")
        tituloAdvertencia = `Usted está editando un alta médica.`


    Swal.fire({
        title: tituloAdvertencia,
        text: 'Asegúrese de revisar que la siguiente información sea correcta: Nro. de Atención de Urgencia #' + `${id}` + '. '
            + 'El usuario que realiza la acción es ' + nombreUsuario + '. '
            + 'Puede confirmar o ignorar la acción',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then((result) => {

        if (result.value) {

            ShowModalCargando(true);
            let json = {};
            let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}`;
            let metodoPeticion = "PATCH"
            let mensajeFinal = "";
            const { CODIGO_PERFIL } = getSession();

            if ($(`#rdoTipoEgresoNormal`).bootstrapSwitch('state')) { // Medico

                switch (CODIGO_PERFIL) {
                    case perfilAccesoSistema.medico:
                    case perfilAccesoSistema.matroneriaUrgencia:
                    case perfilAccesoSistema.administradorDeUrgencia:
                        if (idAltaUrgenciaEditar !== ``) {
                            url += `/Alta/Medico/${idAltaUrgenciaEditar}`
                            metodoPeticion = `PUT`
                        } else {
                            url += "/Alta/Medico"
                        }

                        json = {
                            IdDestinoAlta: valCampo(parseInt($("#sltDestinoAltaPaciente").val())),
                            IdEstablecimientoTraslado: valCampo(parseInt($("#sltEstablecimientoAltaPaciente").val() ?? "0")),
                            IdUbicacion: valCampo(parseInt($("#sltUbicacionDerivado").val())),
                            IdDestinoDerivacion: valCampo(parseInt($("#sltDestinoDerivacion").val())),
                            IdPronosticoMedicoLegal: valCampo(parseInt($("#sltPronosticoMedicoLegal").val())),
                            Diagnosticos: getDiagnosticosRealizados(),
                            IndicacionesAlta: valCampo($("#txaIndicacionesAlta").val()),
                            MedicamentosAlta: valCampo($("#txaMedicamentosAlta").val()),
                            IdTipoProcesoFallecimiento: valCampo(parseInt($("#sltProcesoFallecimiento").val()))
                        };
                        break;
                    //case perfilAccesoSistema.tensUrgencia:
                    //case perfilAccesoSistema.enfermeraUrgencia:
                    //    url += `/Cierre`;
                    //    json = { "Observaciones": $("#txtObservacionEnfermera").val() !== "" ? $("#txtObservacionEnfermera").val() : null };
                    //    break;
                    case perfilAccesoSistema.digitadorUrgencia:
                        url += "/Alta/Digitador";
                        json = {
                            IdDestinoAlta: valCampo(parseInt($("#sltDestinoAltaPaciente").val())),
                            IdEstablecimientoTraslado: valCampo(parseInt($("#sltEstablecimientoAltaPaciente").val() ?? "0")),
                            IdUbicacion: valCampo(parseInt($("#sltUbicacionDerivado").val())),
                            IdDestinoDerivacion: valCampo(parseInt($("#sltDestinoDerivacion").val())),
                            IdPronosticoMedicoLegal: valCampo(parseInt($("#sltPronosticoMedicoLegal").val())),
                            FechaHoraAtencionClinica: $("#txtFechaHoraAtencion").val() + " " + $("#txtHoraAtencion").val(),
                            Diagnosticos: getDiagnosticosRealizados(),
                            FechaHoraAlta: $("#txtFechaAlta").val() + " " + $("#txtHoraAlta").val(),
                            IdTipoProcesoFallecimiento: valCampo(parseInt($("#sltProcesoFallecimiento").val()))
                        };

                        if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia)
                            json.IdProfesional = valCampo($("#thMedicoEgresa").data('id'));
                        else {
                            json.IndicacionesAlta = valCampo($("#txaIndicacionesAlta").val());
                            json.MedicamentosAlta = valCampo($("#txaMedicamentosAlta").val());
                        }

                        break;
                    default: return;
                }

                // 7 = Si el destino es Pabellón
                if (parseInt($("#sltDestinoAltaPaciente").val()) === 7) {
                    json.IdDestinoAlta = 4;
                    json.IdDestinoDerivacion = 7;
                    // 8 = Si el destino es Atención Primaria
                } else if (parseInt($("#sltDestinoAltaPaciente").val()) === 8) {
                    json.IdDestinoAlta = 4;
                    json.IdDestinoDerivacion = 4;
                }

                mensajeFinal = idAltaUrgenciaEditar !== "" ? `Alta de urgencia editada` : "El paciente se ha dado de alta.";

            } else {

                url += `/Alta/Administrativa`;
                mensajeFinal = "Se ha cerrado el DAU.";
                json = {
                    IdMotivoCierre: valCampo(parseInt($("#sltMotivoCierre").val())),
                    Observaciones: valCampo($("#txtMotivoCierrePaciente").val())
                }
            }


            $.ajax({
                type: metodoPeticion,
                url: url,
                data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, jqXHR) {

                    //CargarTablaUrgencia();
                    $('#mdlEgresarAtencion').modal('hide');

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: mensajeFinal,
                        showConfirmButton: false,
                        timer: 3000
                    }).then((result) => {

                        $("#mdlEgresarAtencion").hide()

                        if (location.href.includes("AtencionClinica.aspx") &&
                            (CODIGO_PERFIL == perfilAccesoSistema.medico ||
                                CODIGO_PERFIL == perfilAccesoSistema.matroneriaUrgencia ||
                                CODIGO_PERFIL == perfilAccesoSistema.administradorDeUrgencia)) {
                            if (!$(`#rdoTipoEgresoNormal`).bootstrapSwitch('state')) {
                                window.location.href = ObtenerHost() + "/Vista/ModuloUrgencia/VistaMapaCamasUrgencia.aspx";
                                return;
                            }
                            actualizarHistorialAltas(id)
                        }
                        else if (location.href.includes("BandejaUrgencia.aspx")) {
                            CargarTablaUrgencia()
                        }

                        if ((json.IdDestinoAlta == 4 && json.IdDestinoDerivacion == 7) || json.IdDestinoAlta == 2) {
                            promesaAjax("GET", `URG_Atenciones_Urgencia/${id}`).then(async res => {
                                if (json.IdDestinoAlta == 4 && json.IdDestinoDerivacion == 7)
                                    IrAPabellon(res.IdPaciente, res.IdEvento);
                                else if (json.IdDestinoAlta == 2)
                                    IrASolicitudHospitalizacion(res.IdPaciente, res.IdEvento, id);
                            }).catch(error => {
                                console.error(`Error al obtener ID DAU: ${id}  -  ${JSON.stringify(error)}`);
                            });
                        }

                    });

                    ShowModalCargando(false);

                },
                error: function (jqXHR, status) {
                    console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
                    ShowModalCargando(false);
                }
            });
        }
    })
}

function IrAPabellon(idPaciente, idEventoUrgencia) {

    promesaAjax("GET", `PAB_Formulario_Ind_Qx/Buscar?idEvento=${idEventoUrgencia}`).then(res => {

        if (res.length === 0) {

            Swal.fire({
                title: 'Solicitud Pabellón',
                text: 'Usted marco como destino "Pabellón", por ende, debe realizar la Solicitud Pabellón. ¿Desea ingresarla?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                allowEscapeKey: false,
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    IrASolicitudPabellon(idPaciente, idEventoUrgencia)
                }
            });

        }

    });

}
function CargarDatosMedicoAlta() {
    CargarDiagnosticosCIE10();
    //CargarComboUbicacion();

    $("#divMedicoEgreso").hide();
}
async function CargarInfoDAU(idAtencionUrgencia) {

    const { CODIGO_PERFIL } = getSession();

    if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia || CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia ||
        CODIGO_PERFIL === perfilAccesoSistema.administradorDeUrgencia) {
        $("#divFechaHoraAdmisionCat").show();

        const { FechaLlegada } = GetJsonIngresoUrgencia(idAtencionUrgencia);
        const categorizacionUrgencia = await getCategorizacion(idAtencionUrgencia);

        $("#txtFechaLlegada").val(moment(FechaLlegada).format("DD-MM-YYYY HH:mm"));
        if (categorizacionUrgencia != null && categorizacionUrgencia.length > 0) {
            $("#txtFechaCategorizacion").val(moment(categorizacionUrgencia[0].FechaHora).format("DD-MM-YYYY HH:mm"));
            if (CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia)
                $("#divFechaHoraAdmisionCat").hide();
        }
        else
            $("#txtFechaCategorizacion").val("");

    } else {
        $("#divFechaHoraAdmisionCat").hide();
    }

}
function cargarInfoPaciente(idPaciente) {

    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/GEN_Paciente/Buscar?idPaciente=${idPaciente}`,
        success: function (paciente) {
            $("#txtDPaciente").val(getNombreCompleto(paciente[0]));
        }, error: function (error) {
            console.error("Ha ocurrido un error al buscar el paciente0")
            console.log(error)
        }
    });

}
function CargarInfoEgresoMedico(idAtencionUrgencia) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Alta/Medica`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $("#rdoTipoEgresoNormal").bootstrapSwitch('state', true);

            if (data.Alta.Destino.Id === 3)
                $("#rdoFallecido").bootstrapSwitch('state', true);
            else
                $("#rdoVivo").bootstrapSwitch('state', true);

            $("#rdoTipoEgresoNormal, #rdoTipoEgresoIrregular, #rdoFallecido, #rdoVivo").bootstrapSwitch('disabled', true);
            $(`#divEgresoNormal select, #divEgresoNormal textarea, #divEgresoNormal input`).attr("disabled", "disabled");
            $("#btnEgresarAlta").hide();

            //Destino Alta: sltDestinoAltaPaciente
            $("#sltDestinoAltaPaciente").val(data.Alta.Destino.Id).change();
            //Pronóstico: sltPronosticoMedicoLegal
            $("#sltPronosticoMedicoLegal").val(data.Alta.Pronostico.Id ?? "0");
            //Indicaciones al alta: txaIndicacionesAlta
            $("#txaIndicacionesAlta").val(data.Alta.Indicaciones)
            $("#txaMedicamentosAlta").val(data.Alta.MedicamentosAlta)

            cargarDiagnosticosCIE10Urgencia(data.Alta.DiagnosticosAlta);

            //Establecimiento: sltEstablecimientoAltaPaciente
            if (data.Alta.Destino.OtroEstablecimiento.Id != null)
                $("#sltEstablecimientoAltaPaciente").val(data.Alta.Destino.OtroEstablecimiento.Id);

            //Ubicación: sltUbicacionDerivado
            if (data.Alta.Destino.Hospitalizacion.Id != null)
                $("#sltUbicacionDerivado").val(data.Alta.Destino.Hospitalizacion.Id);

            //Destino: sltDestinoDerivacion
            if (data.Alta.Destino.Derivacion.Id != null)
                $("#sltDestinoDerivacion").val(data.Alta.Destino.Derivacion.Id);

        }
    });



}
function GetHtmlAtencionClinicasPendientes(response, id) {

    const html = `<div class='alert alert-warning text-center mt-2'>
            <strong><i class="fas fa-exclamation-triangle"></i> ${(response.pedientes == 1)
            ? `Existe ${response.pedientes} indicación`
            : `Existen ${response.pedientes} indicaciones`} sin resolver.</strong><br />
            Cantidad de Indicaciones: <span class="badge badge-light">${(response.cantidad - response.pedientes)}/${response.cantidad}</span><br />
            <a class='btn btn-primary mt-2' onClick="irAtencionClinica(${id})">
                Click aquí para completar Indicaciones pendientes <span class="badge badge-light">${(response.cantidad - response.pedientes)}/${response.cantidad}</span>
            </a>
        </div>`;

    return html;
}
function ChangeDestinoAlta() {

    $("#sltDestinoAltaPaciente").change(function () {

        //2	Hospitalización
        if ($(this).val() == '2') {

            $('#sltEstablecimientoAltaPaciente, #sltDestinoDerivacion, #sltProcesoFallecimiento').parent().hide();
            $('#sltEstablecimientoAltaPaciente, #sltDestinoDerivacion, #sltProcesoFallecimiento').val('0').attr("data-required", false);
            $('#sltUbicacionDerivado').parent().show();
            $('#sltUbicacionDerivado').val('0').attr("data-required", true);

            //3	Traslado
        } else if ($(this).val() == '3') {

            activarEstablecimiento(2);
            $('#sltUbicacionDerivado, #sltDestinoDerivacion, #sltProcesoFallecimiento').parent().hide();
            $('#sltUbicacionDerivado, #sltDestinoDerivacion, #sltProcesoFallecimiento').val('0').attr("data-required", false);

            //4	Derivación
        } else if ($(this).val() == '4') {

            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente, #sltProcesoFallecimiento').parent().hide();
            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente, #sltProcesoFallecimiento').val('0').attr("data-required", false);
            $('#sltDestinoDerivacion').parent().show();
            $('#sltDestinoDerivacion').val('0').attr("data-required", true);



            // Fallecimiento
        } else if ($(this).val() == '5') {
            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente, #sltDestinoDerivacion').parent().hide();
            $('#sltUbicacionDerivado, #sltEstablecimientoAltaPaciente, #sltDestinoDerivacion').val('0').attr("data-required", false);
            $('#sltProcesoFallecimiento').parent().show();
            $('#sltProcesoFallecimiento').val('0').attr("data-required", true);

            //1	Domicilio
            //5	Fallecido
            //6	Otro

        } else {

            $('#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion, #sltProcesoFallecimiento').parent().hide();
            $('#sltEstablecimientoAltaPaciente, #sltUbicacionDerivado, #sltDestinoDerivacion, #sltProcesoFallecimiento').val('0').attr("data-required", false);
        }

        ReiniciarRequired();
    });

}
function activarEstablecimiento(idTipoEstablecimiento) {
    comboEstablecimiento("1", "#sltEstablecimientoAltaPaciente", null, idTipoEstablecimiento);
    $('#sltEstablecimientoAltaPaciente').parent().show();
    $('#sltEstablecimientoAltaPaciente').attr("data-required", true);
    ReiniciarRequired();
}

// Cargar Datos para Perfil de Digitador
function GetJsonMedicos() {

    let medicos = [];
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/Buscar?idProfesion=1&idEstablecimiento=1`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            medicos = data;
        },
        error: function (e) {
            console.log(e);
        }
    });
    return medicos;
}
function setMedicosTypeAhead(data) {

    data = data.map(profesional => {
        return {
            Id: profesional.Id,
            Valor: `${profesional.Persona.Nombre} ${profesional.Persona.ApellidoPaterno} ${profesional.Persona.ApellidoMaterno ?? ""}`
        }
    });

    const typeAhMedicos = {
        input: "#thMedicoEgresa",
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el médico especificado ({{query}})",
        display: ["Valor"],
        template:
            '<span>' +
            '<span class="{{Id}}">{{Valor}}</span>' +
            '</span>',
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                $("#thMedicoEgresa").data('id', item.Id);
            }
        }
    };

    return typeAhMedicos;

}
function CargarMedicosParaEgreso() {

    if (!$._data($("#thMedicoEgresa")[0]).events) {
        const medicos = GetJsonMedicos();
        $("#thMedicoEgresa").unbind();
        $("#thMedicoEgresa").on('input', () => $("#thMedicoEgresa").removeData("id"));
        $("#thMedicoEgresa").blur(() => {
            if ($("#thMedicoEgresa").data("id") == undefined)
                $("#thMedicoEgresa").val("");
        });
        $("#thMedicoEgresa").typeahead(setMedicosTypeAhead(medicos));
        $("#txaIndicacionesAlta").parent().parent().hide();
        $("#txaMedicamentosAlta").parent().parent().hide();
        $("#txaIndicacionesAlta").attr("data-required", false);

        ReiniciarRequired();
    }

    cargarFechasAtencion();
    $("#thMedicoEgresa").data("id", null);
    $("#thMedicoEgresa").val("");
    $("#divMedicoEgreso").show();

}

function cargarFechasAtencion() {
    const fechaActual = GetFechaActual();
    $("#txtFechaAlta").val(fechaActual.format("yyyy-MM-dd"));
    $("#txtHoraAlta").val(fechaActual.format("HH:mm"));
}

async function validarAltaCompleta() {

    const { CODIGO_PERFIL } = getSession();

    $('#datos-administartivos-tab').trigger('click');
    await sleep(250);

    // PACIENTE
    if (!validarCampos("#divDatosAdministrativos", false))
        return false;

    if ((CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia || CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) && !validarFechas())
        return false;

    $('#datos-diagnostico-tab').trigger('click');
    await sleep(250);

    if (!existenDiagnosticosAgregados())
        return false;

    if (CODIGO_PERFIL !== perfilAccesoSistema.digitadorUrgencia) {
        $('#datos-indicaciones-tab').trigger('click');
        await sleep(250);

        if (!validarCampos("#divIndicacionesAlta", false))
            return false;
    }

    return true
}