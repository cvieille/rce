﻿
$(document).ready(function () {

    $("#btnBrazalete").unbind().on('click', function () {

        const idAtencionUrgencia = $("#btnBrazalete").data("iddau");
        ShowModalCargando(true);
        promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Categorizacion`).then(async categorizaciones => {
            if (categorizaciones.length > 0) {
                promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}`).then(async res => {
                    await CargarPacienteBrazalete(res);
                    ShowModalCargando(false);
                }).catch(error => {
                    console.error("Error al entregar GET en impresión de brazaletes.");
                    ShowModalCargando(false);
                });
            } else {
                Swal.fire({ icon: 'error', title: 'Impresión de brazalete', text: 'Para imprimir un brazalete el DAU debe estar categorizado.' })
                ShowModalCargando(false);
            }
        }).catch(error => {
            console.error("Error al entregar GET categorizaciones en impresión de brazaletes.");
            ShowModalCargando(false);
        });

    });

});

function abrirModalImprimir(Id) {
    promesaAjax("GET", `URG_Atenciones_Urgencia/Buscar?idAtencionUrgencia=${Id}`).then(async res => {
        await abrirModalImprimirUc(Id, res[0].Paciente.Nombre + " " + res[0].Paciente.ApellidoPaterno, res[0].Paciente.NombreSocial, res[0].IdEvento, 106);
        ShowModalCargando(false);
    }).catch(error => {
        console.error("Error al entregar GET en impresión de brazaletes.")
        ShowModalCargando(false);
    });
}

function abrirModalImprimirUc(Id, Nombre, NombreSocial, idEvento, IdEstado) {

    // Asignar datos a los botones
    $(`#btnImprimirDau, #btnImprimirExamenesLab ,#btnImprimirExamenesImagen, #btnBrazalete,
       #btImprimirProcedimientos, #btnImprimirDauSimple, #btImprimirMedicamentos, #btImprimirInterconsultor, #btImprimirEvolucion, #btImprimirReceta, #btImprimirLpp`).data("iddau", Id);
    $(`#btnImprimirDau, #btnImprimirExamenesLab ,#btnImprimirExamenesImagen, #btnBrazalete,
       #btImprimirProcedimientos, #btnImprimirDauSimple, #btImprimirMedicamentos, #btImprimirInterconsultor, #btImprimirEvolucion, #btImprimirSolicitudHos`).data("idevento", idEvento);

    // Actualizar el título del modal
    $("#tituloModalImprimir").empty();
    $("#tituloModalImprimir").append(`Imprimir documentos de atención con id: ${Id}`);

    // Actualizar el contenido del modal
    $("#textoModalImprimir").empty();
    let nombrePaciente = Nombre ?? 'Nombre no disponible';
    if (NombreSocial) {
        nombrePaciente = `(${NombreSocial}) ${nombrePaciente}`;
    }
    $("#textoModalImprimir").append(`Paciente: ${nombrePaciente}`);

    if (IdEstado !== 105 && IdEstado !== 106)
        $("#divReceteImprimirUc").addClass("d-none");
    else
        $("#divReceteImprimirUc").removeClass("d-none");

    // Mostrar el modal
    volverBotoneraImpresionesUc();
    $("#mdlBotoneraImprimir").modal("show");

}

async function imprimirFormularioUrgenciaUc(button) {

    let idAtencionUrgencia = $(button).data("iddau")
    let tipoFormulario = $(button).data("tipo")
    let ideventourgencia = $(button).data("idevento")

    let url = GetWebApiUrl()
    switch (tipoFormulario) {
        case "dau":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Imprimir`
            break;
        case "exameneslab":
            cargarSolicitudExamenes(5, ideventourgencia)
            break;
        case "examenesimagen":
            cargarSolicitudExamenes(4, ideventourgencia)
            break;
        case "procedimientos":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Procedimientos/Imprimir`
            break;
        case "medicamentos":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Medicamentos/Imprimir`
            break;
        case "interconsultor":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Interconsultor/Imprimir`
            break;
        case "evolucion":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Evolucion/Imprimir`
            break;
        case "receta":
            url += `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Imprimir/Receta`
            break;
        case "solicitudhos":
            let solicitud = await getLastSolicitudHospitalizacion(ideventourgencia)
            if (!solicitud)
                return
            url += `HOS_Solicitud_Hospitalizacion/${solicitud.Id}/Imprimir`
            break;
        case "lpp":
            let imprimirLpp = await getLastLpp(idAtencionUrgencia);
            if (!imprimirLpp)
                return
            url += `URG_Atenciones_Urgencia/${imprimirLpp.UrgenciaLPP.Id}/LPP/Imprimir`;
    }

    if (tipoFormulario !== "simple" && tipoFormulario !== "exameneslab" && tipoFormulario !== "examenesimagen")
        ImprimirApiExterno(url);
}

async function getLastSolicitudHospitalizacion(idEvento) {

    const solicitudes = await getSolicitudesHospitalizacion(idEvento)

    if (solicitudes.length == 0) {
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: 'El paciente no tiene solicitudes de Hospitalización.',
            showConfirmButton: false,
            timer: 2000
        })
    }

    const solicitud = solicitudes.reduce((max, solicitud) =>
        solicitud.Id > max.Id ? solicitud : max,
        solicitudes[0]
    );

    if (!solicitud)
        return

    return solicitud
}

async function getSolicitudesHospitalizacion(idEvento) {

    if (idEvento === null)
        return

    const url = `${GetWebApiUrl()}HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&idTipoEstadoSistemas=158&idEvento=${idEvento}`

    try {
        const solicitudes = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })

        return solicitudes

    } catch (error) {
        console.error("Error al cargar Solicitud")
        console.log(JSON.stringify(error))
    }

}

async function cargarSolicitudExamenes(idTipoArancel, idEvent) {
    titulo = ``, examenes = []
    switch (idTipoArancel) {
        case 4:
            titulo = `Solicitudes examen de laboratorio`
            examenes = await buscarExamenesImagenPorEvento(idEvent)
            break
        case 5:
            titulo = `Solicitudes examen de imagenologia`
            examenes = await buscarExamenesPorEvento(idEvent)
            break
    }
    let columnas = [
        { title: "Id", data: "Id" },
        { title: "Fecha", data: "Fecha" },
        { title: "Examenes", data: "Solicitud", orderable: false },
        { title: "Imprimir", data: "Id", orderable: false }
    ]
    let configColumn = [
        { targets: 1, render: function (fecha) { return moment(fecha).format("DD/MM/YYYY HH:mm") } }//fecha tiene que ser formateada
        , {
            targets: 2,//Recorriendo los examenes para devolver un listado 
            render: function (solicitud) {
                let stringExamenes = `Sin examenes, revise la solicitud`
                if (solicitud !== null) {
                    if (solicitud.Examen.length > 0) {
                        stringExamenes = `<ul>`
                        let htmlExamenes = ``
                        solicitud.Examen.map(examen => {
                            htmlExamenes += `<li>${examen.Valor}</li>`
                        })
                        stringExamenes += `${htmlExamenes} </ul>`
                    }
                }
                return stringExamenes
            }
        }, {
            targets: -1,//Recorriendo los examenes para devolver un listado 
            render: function (idSolicitud, type, data) {
                return `<button class="btn btn-info btn-sm" onclick="imprimirSolicitudExamenUrgencia(${idSolicitud}, ${idTipoArancel})" type="button"> <i class="fa fa-print"></i> </button>`
            }
        }
    ]
    $("#titleTblSolicitudExamen").append(titulo)
    $("#divTablaExamenesUc").show()
    $("#divBotoneraUrgenciaUc").hide()
    dibujarTablaExamenesIngresados(examenes, "#tblSolicitudExamenes", columnas, configColumn)
}

function imprimirSolicitudExamenUrgencia(idSolicitud, idTipoArancel) {
    if (idTipoArancel == null || idSolicitud == null)
        throw new Error("Error no paso los parametros requeridos idSolicitudExamen o idTipoArancel no puede ser null");

    let url = `${GetWebApiUrl()}`
    if (idTipoArancel == 5)
        url += `RCE_Solicitud_Laboratorio/${idSolicitud}/Imprimir`
    else
        url += `RCE_Solicitud_Imagenologia/${idSolicitud}/Imprimir`
    try {
        ImprimirApiExterno(url, null, "GET")
    } catch (error) {
        console.error("Ocurrio un errro al imprimir la solicitud de examen")
        console.log(error)
    }
}

function volverBotoneraImpresionesUc() {
    $("#titleTblSolicitudExamen").empty()
    $("#divTablaExamenesUc").hide()
    $("#divBotoneraUrgenciaUc").show()
    $(`tblSolicitudExamenes`).empty()
}

// Imprimir LPP 
async function getLastLpp(idAtencionUrgencia) {
    const lpps = await getLpp(idAtencionUrgencia);

    if (lpps.length === 0) {
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: 'El paciente no tiene ningún LPP asociado.',
            showConfirmButton: false,
            timer: 2000
        });
    }

    // último id del array
    const ultimoLpp = lpps[lpps.length - 1];

    if (!ultimoLpp)
        return;

    return ultimoLpp;
}

async function getLpp(idAtencionUrgencia) {
    if (idAtencionUrgencia === null)
        return;

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/LPP`;

    try {
        const lpps = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json'
        });

        return lpps;

    } catch (error) {
        console.error("Error al cargar LPP");
        console.log(JSON.stringify(error));
    }
}