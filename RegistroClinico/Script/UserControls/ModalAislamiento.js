﻿let arrTipoAislamiento = []

const linkVerAislamiento = async (sender) => {

    let tipoItem = $(sender).data("tipo")
    let camaPac = $(sender).data('cama')
    let ubicacionPac = $(sender).data("ubicacion")

    disabledComboTipoMotivo(true)
    comboTipoMotivoTipoAislamiento()
    await comboMotivoTipoAislamiento("#ddlMotivoAislamiento", "#ddlTipoAislamiento")

    idAislamiento = 0;

    idHos = $(sender).data("id");

    const idPaciente = $(sender).data('idpaciente');

    $("#lblNombrePaciente").text($(sender).data('pac'))
    $("#lblIdHospitalizacion").text($(sender).data('id'))
    $("#lblCama").text((camaPac !== "") ? camaPac : "Cama no asignada")
    $("#lblUbicacion").text((ubicacionPac !== "") ? ubicacionPac : "Ubicación no asignada")

    //const idNuevo = $(sender).data('nuevo');

    $('#ddlTipoAislamiento').selectpicker();
    //$('button[data-id="ddlTipoAislamiento"]').html('Seleccione');
    //Es undefined cuando se solicita aislamiento(cuando viene desde la bandeja de hospitalizacion)
    if (tipoItem === "invasivo") {
        //Invasivos 
        $("#textInfoModal").text("Invasivos del paciente")
        $("#vigilancias-tab").removeClass("d-none")
        $("#vigilancias-tab").click()
        $("#aislamientos-tab").addClass("d-none")
        getTablaInvasivos()
    } else {
        //Aislamientos
        $("#textInfoModal").html("<i class='fa fa-eye-slash' aria-hidden='true'></i > Aislamientos del paciente")
        cancelarEdicion()
        if (sSession.CODIGO_PERFIL === perfilAccesoSistema.unidadIaas || sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria) {
            $('#divNuevoAislamiento').show();
            $('#txtFechaInicioAisl').val(moment().format('YYYY-MM-DD'));
            $('#txtFechaFinAisl').val(moment().add(5, 'days').format('YYYY-MM-DD'));
        }
        getTablaAislamiento(idPaciente);
        $("#aislamientos-tab").removeClass("d-none")
        $("#aislamientos-tab").click()
        $("#vigilancias-tab").addClass("d-none")
    }
    $("#mdlAislamiento").modal('show');
}

async function comboMotivoTipoAislamiento(ddlMotivoAilamiento, ddlTipoAislamiento) {

    $(ddlMotivoAilamiento).change(async function () {
        let idtipoMotivoAislamiento = parseInt($(this).val())
        arrTipoAislamiento = []

        if (idtipoMotivoAislamiento === 0)
            disabledComboTipoMotivo(true)
        else
            disabledComboTipoMotivo(false)

        const tipoAislamientos = await cargarTipoAislamiento(idtipoMotivoAislamiento)

        $(ddlTipoAislamiento).empty()
        tipoAislamientos.map(t => {
            $(ddlTipoAislamiento).append(`<option value='${t.Id}'>${t.Valor}</option>`)
            $(ddlTipoAislamiento).selectpicker('refresh')
        })

        $(ddlTipoAislamiento).change(async function () {
            arrTipoAislamiento = $(this).val()
        })
    })
}

async function disabledComboTipoMotivo(deshabilita) {

    if (deshabilita) {
        $("#ddlTipoAislamiento").prop('disabled', true)
        $("#ddlTipoAislamiento").attr('data-required', true)
        $("#ddlTipoAislamiento").selectpicker('refresh')
    }
    else {
        $("#ddlTipoAislamiento").prop("disabled", false)
        $("#ddlTipoAislamiento").attr('data-required', false)
        $("#ddlTipoAislamiento").selectpicker('refresh')
    }
}

function comboTipoMotivoTipoAislamiento() {
    const url = `${GetWebApiUrl()}GEN_Tipo_Motivo_Aislamiento/Combo`;
    setCargarDataEnCombo(url, false, "#ddlMotivoAislamiento");
}

async function cargarTipoAislamiento(idMotivoAislamiento) {

    const url = `${GetWebApiUrl()}GEN_Tipo_Motivo_Aislamiento/${idMotivoAislamiento}/Combo`

    let tipoAislamiento = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (tipoAislamiento) {

            },
        error: function (err) {
            console.error("Error al cargar tipo ailamiento")
            console.error(JSON.stringify(err))
        }
    })

    return tipoAislamiento
}

async function AgregarEditarAislamiento(sender) {

    let tipoAislamientoTabla = []
    let tiposAislamientos = []
    let enviar = false

    let metodo = $(sender).data("method")

    if (!validarCampos("#divAislamientos", false))
        return
    else if (arrTipoAislamiento.length === 0) {
        toastr.error("Seleccione un tipo de aislamiento")
        return
    }

    const idMotivoAislamiento = $("#ddlMotivoAislamiento").val()
    const IdsTiposAislamientos = arrTipoAislamiento.map(x => parseInt(x))

    const AislamientoEncontrado = await buscarAislamiento(idMotivoAislamiento)
    const [TipoAislamiento] = AislamientoEncontrado

    if (TipoAislamiento != null) {
        tiposAislamientos = TipoAislamiento.TipoAislamiento
        if (tiposAislamientos !== undefined)
            tipoAislamientoTabla = tiposAislamientos.map(tipo => tipo.Id)
    }

    const AilamientoFiltrado = IdsTiposAislamientos.filter(valor => {
        if (!tipoAislamientoTabla.includes(valor))
            enviar = true
        return !tipoAislamientoTabla.includes(valor)
    })

    let bValidar = validarCampos('#divNuevoAislamiento', false)
    let fechaFinAislamiento = $('#txtFechaFinAisl').val()

    let method, url, accion

    if (bValidar) {
        let json = {
            IdHospitalizacion: idHos,
            TiposAislamiento: IdsTiposAislamientos,
            IdTipoMotivoAislamiento: parseInt($('#ddlMotivoAislamiento').val()),
            FechaInicio: $('#txtFechaInicioAisl').val(),
            FechaTermino: fechaFinAislamiento ?? null
        };

        url = `${GetWebApiUrl()}HOS_Hospitalizacion/${idHos}/Aislamiento`
        if (metodo === "CREAR") {
            method = "POST"
            json.TiposAislamiento = AilamientoFiltrado
            accion = "ingresó"
        } else {
            //Edicion de un registro de aislamiento
            method = "PUT"
            json.TiposAislamiento = arrTipoAislamiento
            url = `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamiento`
            accion = "editó"
        }
        let mensajeExito = `Se ${accion} correctamente el registro de aislamiento`
        let mensajeError = `No se ${accion} el aislamiento, ocurrió un error`

        if (!enviar && metodo !== "EDITAR") {
            toastr.warning("Tipos de aislamientos ya existen")
            return
        }

        $.ajax({
            type: method,
            url: url,
            data: JSON.stringify(json),
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                $("#txtIdAislamiento").val("")
                idAislamiento = 0;
                getTablaAislamiento();
                $("#ddlTipoAislamiento").selectpicker('refresh')
                //Ocultar los campos de edicion
                cancelarEdicion()
                Swal.fire(
                    'Registro guardado',
                    `${mensajeExito}`,
                    'success'
                )

                $('#ddlMotivoAislamiento').val("0")
                $('#ddlTipoAislamiento').val("0")
                $('#ddlTipoAislamiento').attr("disabled", true)
                disabledComboTipoMotivo(true)
            },
            error: function (err) {
                console.log(mensajeError, err);
                toastr.error(`${mensajeError}`);
            }
        });
    }
}

function getTablaInvasivos(idPaciente = null) {
    let adataset = [];
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Invasivos/Buscar?${idHos == "" ? `idPaciente=${idPaciente}` : `idHospitalizacion=${idHos}`}`

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                adataset.push([
                    r.Id,
                    moment(r.FechaInicio).format('DD-MM-YYYY HH:mm'),
                    r.FechaTermino == null ? 'N/A' : moment(r.FechaTermino).format('DD-MM-YYYY'),
                    r.TipoInvasivo !== null ? r.TipoInvasivo.Valor : "No informado",
                    r.TipoExtremidad !== null ? r.TipoExtremidad.Valor : "No informado",
                    r.TipoPlano !== null ? r.TipoPlano.Valor : "No informado"
                ]);
            });

            $('#tblInvasivosPaciente').addClass("text-wrap").DataTable({
                data: adataset,
                "pageLength": 5,
                order: [],
                columns: [
                    { title: "Id" },
                    { title: "Inicio" },
                    { title: "Término" },
                    { title: "Invasivo" },
                    { title: "Extremidad" },
                    { title: "Plano" }
                ],
                columnDefs: [
                    { targets: [1, 2], sType: "date-ukShort" },
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //if (aData[10] != '') {
                    //    $('td', nRow).css('background-color', '#DFF0D8');
                    //}
                },
                bDestroy: true
            });
        }
    });
}
function getTablaAislamiento(idPaciente = null) {

    let adataset = [];
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamientos/Buscar?${idHos == "" ? `idPaciente=${idPaciente}` : `idHospitalizacion=${idHos}`}`

    if (idHos == "") {
        idHos = parseInt($("#lblIdHospitalizacion").text())
    }

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, r) {
                adataset.push([
                    moment(r.FechaInicio).format('DD-MM-YYYY'),
                    r.FechaTermino == null ? '' : moment(r.FechaTermino).format('DD-MM-YYYY'),
                    r.TipoAislamiento.map(t => t.Valor),
                    r.TipoMotivoAislamiento.Id,
                    r.TipoMotivoAislamiento.Valor,
                    r.TipoEstado.Valor,
                    r.TipoEstado.Id
                ]);
            });

            $('#tblAislamientoPaciente').addClass("nowrap").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        className: 'btn btn-info',
                        text: 'Exportar a Excel'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-info',
                        text: 'Exportar en PDF'
                    }
                ],
                data: adataset,
                "pageLength": 5,
                order: [],
                columns: [
                    { title: "Inicio" },
                    { title: "Término" },
                    { title: "Tipo aislamiento" },
                    { title: "IdMotivoAislamiento" },
                    { title: "Motivo aislamiento" },
                    { title: "Estado" },
                    { title: "Id Estado" },
                    { title: "", className: "text-center" }
                ],
                columnDefs: [
                    {
                        targets: [2],
                        render: function (data, type, full, meta) {
                            return "<div style='white-space:normal;'>" + data + "</div>";
                        }
                    },
                    { targets: [-2, 3], visible: false },
                    { "width": "10%", "targets": [1, 2, 4] },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        visible: sSession.CODIGO_PERFIL === perfilAccesoSistema.unidadIaas,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            data[5] === 138 ? cerrado = true : cerrado = false;
                            var botones =
                                `<button class='btn btn-primary btn-circle' 
                               onclick='toggle("#divAccionAislamiento${fila}"); return false;'>
                               <i class="fa fa-list" style="font-size:15px;"></i>
                                </button>
                                <div id="divAccionAislamiento${fila}" class='btn-container' style="display: none;">
                                     <center>
                                     <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                     <div class="rounded-actions">
                                     <center>
                                `;

                            botones += `<a id='linkEditarAislamiento'
                                        data-id-tipo-motivo=${adataset[fila][3]}
                                        title="Editar"
                                        href="#"
                                        class='btn btn-info btn-circle ${cerrado == true ? "disabled" : ""} m-2'
                                        onclick=linkEditarAislamiento(this)>
                                        <i class='fa fa-edit'>
                                        </i>
                                        </a>
                                        <br/>
                                        <a id='linkTerminarAislamiento'
                                        data-id-motivo-aislamiento=${adataset[fila][3]}
                                        title="Cerrar"
                                        href="#"
                                        class='btn btn-warning btn-circle ${cerrado == true ? "disabled" : ""} m-2'
                                        onclick=linkTerminarAislamiento(${adataset[fila][3]},${idHos})>
                                        <i class='fa fa-sign-out-alt'>
                                        </i>
                                        </a>`

                            botones += `
                             </center >
                             </div >
                             </center >
                            </div >`
                                ;
                            return botones

                        }
                    },
                    {
                        targets: [1, 2], sType: "date-ukShort"
                    }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    //if (aData[10] != '') {
                    //    $('td', nRow).css('background-color', '#DFF0D8');
                    //}
                },
                bDestroy: true
            });
        }
    });
}

const linkEditarAislamiento = async (e) => {

    arrTipoAislamiento = []

    const idTipoMotivoAislamiento = $(e).data('id-tipo-motivo');

    const aislamientos = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamientos/Buscar?idHospitalizacion=${idHos}&idTipoMotivoAislamiento=${idTipoMotivoAislamiento}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

            $("#infoAislamiento").text()
            $("#infoAislamiento").html(`<i class="fa fa-edit"></i>Realizando <b>edición</b> de registro de aislamiento`)
            $("#divEdicionAislamiento").removeClass("d-none")
            $("#divAgregarAislamiento").addClass("d-none")

        }, error: function (err) {
            toastr.error("Ha ocurrido un error al buscar aislamientos en edición")
            console.log("Error al buscar aislamientos", err)
        }
    });

    $("#ddlMotivoAislamiento").val(idTipoMotivoAislamiento)
    await disabledComboTipoMotivo(false)

    const tipoAislamientos = await cargarTipoAislamiento(idTipoMotivoAislamiento)

    const [TipoAislamiento] = aislamientos
    const tiposAislamientosTabla = [...TipoAislamiento.TipoAislamiento]

    const arrTipoAislamientosTabla = tiposAislamientosTabla.map(tipoAislamiento => tipoAislamiento.Id)
    const arrTipoAislamientos = tipoAislamientos.map(tipoAislamiento => tipoAislamiento.Id)
    const repetidos = arrTipoAislamientosTabla.filter(valor => arrTipoAislamientos.includes(valor))

    $(ddlTipoAislamiento).empty()

    tipoAislamientos.map(tipoAislamiento => {
        if (repetidos.includes(tipoAislamiento.Id)) {
            $(ddlTipoAislamiento).append(`<option value='${tipoAislamiento.Id}' selected>${tipoAislamiento.Valor}</option>`)
            $(ddlTipoAislamiento).selectpicker('refresh')
        }
        else {
            $(ddlTipoAislamiento).append(`<option value='${tipoAislamiento.Id}'>${tipoAislamiento.Valor}</option>`)
            $(ddlTipoAislamiento).selectpicker('refresh')
        }
    })

    if (arrTipoAislamiento.length === 0)
        arrTipoAislamiento = repetidos

    $("#ddlTipoAislamiento").change(async function () {
        arrTipoAislamiento = $(this).val()
    })

    return aislamiento
}

async function buscarAislamiento(idTipoMotivoAislamiento) {

    const aislamientos = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamientos/Buscar?idHospitalizacion=${idHos}&idTipoMotivoAislamiento=${idTipoMotivoAislamiento}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

        }, error: function (err) {
            toastr.error("Ha ocurrido un error al buscar aislamientos")
            console.log("Error al buscar aislamientos", err)
        }
    });

    return aislamientos
}

const linkTerminarAislamiento = (IdTipoMotivoAislamiento, IdHospitalizacion) => {

    let json = {
        IdHospitalizacion,
        IdTipoMotivoAislamiento
    }

    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Aislamiento/Cerrar`

    cancelarEdicion()
    ShowModalAlerta(
        'CONFIRMACION',
        'Terminando aislamiento?',
        `Terminar el aislamiento?`,
        function () {
            $("#modalAlerta").modal('hide')
            $.ajax({
                type: 'PATCH',
                url: url,
                data: JSON.stringify(json),
                contentType: "application/json",
                dataType: 'json',
                async: false,
                success: function (data) {
                    getTablaAislamiento()
                    Swal.fire(
                        'Aislamiento cerrado',
                        `Aislamiento terminado.`,
                        'success'
                    )
                }, error: function (err) {
                    toastr.error("Ha ocurrido un error al cerrar aislamiento")
                    console.log("Error al cerrar aislamientos", err)
                }
            });
        },
        function () {
            $("#modalAlerta").modal('hide')
        },
        'Terminar aislamiento',
        'Cancelar'
    )
}

function cancelarEdicion() {
    if ($("#divAgregarAislamiento").hasClass("d-none")) {
        $("#divEdicionAislamiento").addClass("d-none")
        $("#divAgregarAislamiento").removeClass("d-none")
        $("#txtIdAislamiento").val("")
        $("#infoAislamiento").text(`Agregando aislamiento`)
        $('#ddlTipoAislamiento').val(0);
        $('#txtFechaInicioAisl').val(moment().format('YYYY-MM-DD'));
        $('#txtFechaFinAisl').val(moment().add(5, 'days').format('YYYY-MM-DD'));
    }
}