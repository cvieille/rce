﻿$(document).ready(function () {
    $("#sltTipoBox").change(function () {
        comboNombreBox();
    });
});
function comboNombreBox() {
    if ($("#sltTipoBox").val() != '0' && $("#sltTipoBox").val() != null) {
        let url = `${GetWebApiUrl()}URG_Box/Combo/Tipo/${$("#sltTipoBox").val()}/Ubicacion/${$("#sltServicioBox").val()}`;
        setCargarDataEnCombo(url, true, $("#sltNombreBox"));
        $('#sltNombreBox').removeAttr('disabled');
    } else {
        $('#sltNombreBox').val('0').attr('disabled', 'disabled');
    }

}