﻿// Definiciones globales simplificadas
let idDivHosp = "";
let otrasHospitalizaciones = [];

function linkHistorialHosp(event) {
    const id = $(event).data("idpac");
    const nombre = $(event).data("nompac");
    ShowModalHospitalizaciones(id, nombre);
}

function ShowModalHospitalizaciones(idPaciente, nombrePaciente) {
    
    $("#btn-hosp-rce-tab").click();
    let elemento = $("#mdlVerHospitalizaciones .modal-body");
    showModalCargandoComponente(elemento);

    $("#mdlVerHospitalizaciones").modal("show")
    
    let data = [];
    
    let response = hospitalizacionesPreviasPaciente(idPaciente);
    if (response) {
        array = Array.from(response);
    } else {
        array = [];
    }
    $.each(array, function (key, hospitalizacion) {
        
        data.push([
            hospitalizacion.Id,
            hospitalizacion.Fecha != null ? moment(hospitalizacion.Fecha).toDate().format("dd-MM-yyyy") : "x",
            hospitalizacion.Categorizacion != null ? hospitalizacion.Categorizacion.Codigo : "x",
            hospitalizacion.Cama != null ? hospitalizacion.Cama.Actual.Valor : "x",
            hospitalizacion.Estado != null ? hospitalizacion.Estado.Valor : "x"
        ]);
    });
    cargarTablaHistorialHospitalizaciones(data);
    $("#nomPacModalVerHospitalizaciones").html(nombrePaciente);
    $("#txtIdHospModalHosp").val(idPaciente)
    hideModalCargandoComponente(elemento);
}

function buscarOtrasHospitalizaciones() {
    showModalCargandoComponente("#tblHistorialOtrasHospitalizacionesModal")
    let idPaciente = $("#txtIdHospModalHosp").val()
    promesaAjax(`get`, `HOS_Hospitalizacion/HistorialFlorence/Buscar?idPaciente=${idPaciente}`).then(res => {
        
        if ($.fn.DataTable.isDataTable("#tblHistorialOtrasHospitalizacionesModal"))
            $("#tblHistorialOtrasHospitalizacionesModal").DataTable().destroy();

        $("#tblHistorialOtrasHospitalizacionesModal").DataTable({
            language: {
                "processing": `<i class="fa fa-cog fa-spin fa-3x"></i>`
            },
            data:res,
            columns: [
                {title:"Id", data :"id"},
                {title:"Ingreso", data :"fecha_ingreso"},
                {title:"Diagnóstico", data :"diagnostico_ingreso"},
                {title:"Previsión", data :"prevision"},
                {title:"Acciones", data :"id"}
            ],
            destroy: true
        })
        hideModalCargandoComponente($("#tblHistorialOtrasHospitalizacionesModal"))
    }).catch(error => {
        console.error("Error al buscar las hospitalizaciones de florence.")
        console.log(error)
        toastr.error("Ocurrio un error al buscar en la integración, la información no esta disponible en este momento")
    })
    
}

function hospitalizacionesPreviasPaciente(id) {

    let response = false;
    let url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idTipoEstado[0]=87&idTipoEstado[1]=88&idPaciente=${id}`

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {            
            if (data.length > 0 && data[0].Paciente.IdPaciente == id) {
                response = data;
            }
        },
        error: function (jqXHR, status) {
            if (jqXHR.status == 401) {
            } else
                console.log(`Error al cargar JSON: ${JSON.stringify(request)}`);
            console.log(`Error al buscar Hospitalización ${JSON.stringify(jqXHR)}`);
        }
    });
    return response;
}

function cargarTablaHistorialHospitalizaciones(json) {
    let urlActual = window.location.href

    let nuevoIngreso = urlActual.includes("NuevoFormularioAdmision")
    $('#tblHistorialHospitalizacionesModal').addClass("nowrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        data: json,
        columns: [
            { title: "ID", className: "text-center font-weight-bold rce-tray-id" },
            { title: "Fecha"},
            { title: "Categorización" },
            { title: "Cama" },
            { title: "Estado" },
            { title: "" },
        ],
        columnDefs: [
            {
                targets: 5,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones =
                    `
                    <button class='btn btn-primary btn-circle'
                        onclick='toggleModalHosp("#div_accionesMdlHisHOS${fila}"); return false;'>
                        <i class="fa fa-list" style="font-size:15px;"></i>
                    </button>
                    <div id='div_accionesMdlHisHOS${fila}' class='btn-container' style="display: none;">
                        <center>
                            <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                            <div class="rounded-actions">
                                <center>
                    `;
                    botones +=
                    `
                    <a id='linkImprimir' data-id='${json[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/'
                        onclick='linkImprimirHospitalizacion(this);' data-toggle="tooltip" data-placement="left" title="Imprimir">
                        <i class='fa fa-print'></i>
                    </a><br>
                    ${nuevoIngreso ?
                    `<a id='aEdicionHospitalizacion' data-idHos='${json[fila][0]}' class='btn btn-info btn-circle btn-lg' href='#/'
                        onclick='linkEditarHosp(${json[fila][0]});' data-toggle="tooltip" data-placement="left" title="Editar">
                        <i class='fa fa-edit'></i>
                    </a><br>`:""}`
                    
                    botones +=
                    `
                                </center >
                            </div >
                        </center >
                    </div >
                    `;

                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

function linkEditarHosp(idHos) {
    setSession("ID_HOSPITALIZACION", idHos);
    ReiniciarComponentesClinicos();
    CargarHospitalizacion(idHos);
    ocultarModal("mdlVerHospitalizaciones");    
}

function toggleModalHosp(id) {

    if (idDivHosp == "") {

        $(id).fadeIn();
        idDivHosp = id;

    } else if (idDivHosp == id) {

        $(id).fadeOut();
        idDivHosp = "";

    } else {

        $(idDivHosp).fadeOut();
        $(id).fadeIn();
        idDivHosp = id;
    }
}