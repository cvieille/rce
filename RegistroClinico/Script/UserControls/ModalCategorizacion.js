﻿let categorizarRecienNacido = false, idCategorizacionDetalles 

function cambiarFormularioSaludMental(checkbox) {
    let UrlCategorizacion = ``
    let idUbicacion = 0
    //true  = formulario de salud mental (psiquiatria)
    if (checkbox.checked) {
        //Debo cambiarlo para que sea dinamico despues.. tiene que contener el id de psiquiatria
        idUbicacion = 178
        UrlCategorizacion = `${GetWebApiUrl()}HOS_Categorizacion/178/Variables`
        toastr.info("Cargando formulario de categorización salud mental.")
    } else {
        //Tiene que contener el id de la ubicacion de la cama
        idUbicacion = parseInt($("#inputIdUbicacion").val())
        UrlCategorizacion = `${GetWebApiUrl()}HOS_Categorizacion/67/Variables`
        toastr.info("Cargando formulario de categorización normal.")
    }
    peticionCategorizacion(UrlCategorizacion)
}

const linkMostrarModalCategorizacion = async (button) => {
    //Muestra el cuestionario
    if ($("#categorizacion-tab").hasClass("d-none"))
        $("#categorizacion-tab").removeClass("d-none")

    $("#btnUcCategorizarPaciente").removeAttr("disabled")
    $("#btnUcPacienteAusente").removeAttr("disabled")

    let catHoy = false
    catHoy = $(button).data("categorizadohoy")
    //El paciente fue categorizado hoy
    if (catHoy) {
        //bloquear categorizar paciente
        $("#btnUcCategorizarPaciente").attr("disabled", true)
        $("#btnUcPacienteAusente").attr("disabled", true)
        //Ocultar formulario de categorizacion
        $("#historialcategorizacion-tab").click()
        //ocultar tab para categorizar
        $("#categorizacion-tab").addClass("d-none")
        $("#categorizacion").removeClass("show , active")
    } else {
        $('#categorizacion-tab').click()
    }
    LimpiarPaciente()
    $("#txtCategorizacionNombrePaciente").text($(button).data("pac"))
    $("#txtCategorizacionIdHospitalizacion").text($(button).data("id"))
    $("#txtCategorizacionNombreCama").text($(button).data("cama") ?? "No informado")
    $("#txtEstadoCamaActual").val($(button).data("estadocama"))

    $("#inputIdUbicacion").val($(button).data("idubicacion"))
    $("#inputIdCama").val($(button).data("idcama"))
    //$("#inputIdCategorizacion").val($(button).data("idcategorizacion"))
    //$('#categorizacion').tab('show')

    let recienNacido = $(button).data("rn")
    //cuando se categoriza un recien nacido
    if (recienNacido) {
        $("#contenidoTablas").addClass("d-none")
        $("#historialcategorizacion-tab").addClass("d-none")
        $("#informacionPacienteCategorizar").addClass("d-none")
        $("#divUcPaciente").removeClass("d-none")
        $("#cuadroAyudaRecienNacido").removeClass("d-none")
        $("#btnSiguiente").removeClass("d-none")
        $("#sltIdentificacion").val(4)
        $("#sltIdentificacion").attr("disabled", true)
        let numeroDocumento = $(button).data("numerodocumento").split("-")[0]
        let digito = $(button).data("numerodocumento").split("-")[1]
        $("#txtnumerotPac").val(numeroDocumento)
        $("#txtDigitoPac").val(digito)
        $("#txtDigitoPac").trigger('input')

        //toastr.info("Seleccione un recien nacido a categorizar.")
    } else {

        $("#contenidoTablas").removeClass("d-none")
        $("#historialcategorizacion-tab").removeClass("d-none")
        $("#informacionPacienteCategorizar").removeClass("d-none")
        $("#divUcPaciente").addClass("d-none")
        $("#cuadroAyudaRecienNacido").addClass("d-none")
    }
    categorizarRecienNacido = recienNacido
    if (!catHoy) {
        let checkboxsaludmental = document.getElementById("checkFormularioSaludMental");
        // Verifica si está marcado el formulario de salud mental
        if (checkboxsaludmental.checked) {
            // Si está marcado, lo reinicia para un formulario normal.
            checkboxsaludmental.checked = false;
           
        }
        await cambiarFormularioSaludMental(document.getElementById("checkFormularioSaludMental"))
    }
    $("#mdlCategorizacion").modal('show')
    peticionHistorialCategorizacion($(button).data("id"))
}
function categorizarNuevoPaciente() {
    if (Object.keys(GetPaciente()).length == 0) {
        $('#collapseUserControlPaciente').collapse('show');
        if (validarCampos("#divDatosPaciente", true)) {
            $("#contenidoTablas").removeClass("d-none")
            $("#btnSiguiente").addClass("d-none")
            $('#collapseUserControlPaciente').collapse('hide');
        } else {
            toastr.error("Faltan campos obligatorios del paciente.")
        }
    } else {
        $("#contenidoTablas").removeClass("d-none")
        $("#btnSiguiente").addClass("d-none")
        $('#collapseUserControlPaciente').collapse('hide');
    }

}

function peticionHistorialCategorizacion(idhosp) {
    if (idhosp !== undefined) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Categorizacion/buscar?idHospitalizacion=${idhosp}`,
            async: true,
            success: function (data, status, jqXHR) {
                let dataset = []
                if (data.length > 0) {
                    dataset = data.map(a => {
                        return [
                            a.Id,
                            moment(a.Fecha).format("DD-MM-YYYY"),
                            a.Categorizacion ?? "",
                            a.Cama !== null ? a.Cama.Valor : "",
                            a.NivelRiesgo !== null ? a.NivelRiesgo.Valor : "",
                            a.NivelDependencia !== null ? a.NivelDependencia.Valor : "",
                            a.UbicacionReal !== null ? a.UbicacionReal.Valor : "",
                            a.TipoEstado !== null ? a.TipoEstado.Valor : "",
                            a.UbicacionReal !== null ? a.UbicacionReal.Id : "",
                        ]
                    })
                }
                cargarTablaHistorialCategorizacion(dataset)
            }, error: function (err) {
                console.log(err)
                console.error("ha ocurrido un error al buscar el historial de categorizacion")
            }
        })
    }
}
function cargarTablaHistorialCategorizacion(dataset) {

    let table = $("#tblHistorialCategorizacion").addClass("w-100").addClass("dataTable").DataTable({
        data: dataset,
        order: [[0, 'desc']],
        responsive: {
            details: false,
            type: 'column'
        },
        sDom: 'ltip',
        columns: [
            { title: "Id" },
            { title: "Fecha" },
            { title: "Categoría", className: "text-center" },
            { title: "Cama" },
            { title: "Riesgo" },
            { title: "Dependencia" },
            { title: "Ubicacion" },
            { title: "Estado" }
        ],
        columnDefs: [
            {
                targets: 2,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    let clase = ""
                    let letraCategoria = dataset[fila][2].charAt(0)
                    switch (letraCategoria) {
                        case "A":
                            clase = "danger"
                            break
                        case "B":
                            clase = "warning"
                            break
                        case "C":
                            clase = "info"
                            break
                        case "D":
                            clase = "success"
                            break
                        default:
                            clase = "secondary"
                            break
                    }
                    return `<span class="badge badge-${clase}" style="font-size:1em;  cursor:pointer;" onclick="buscarDetallesCategorizacionPorId(this, ${dataset[fila][0]})" >${dataset[fila][2]}</span>`
                }
            }, {
                targets: 0,
                render: function (id, type, row, meta) {
                    var fila = meta.row;
                    if (dataset[fila][8] == 161)
                        return `<button class="btn btn-info dt-control-categorizacion"  type="button">${id}</button>`
                    else
                        return id
                }
            }
        ],
        bDestroy: true
    })


    async function format(data) {
        if (data !== undefined) {
            let contenido = `<table class="table table-bordered table-striped">
        <caption>Categorizaciones de recién nacidos de ${$("#txtCategorizacionNombrePaciente").text()} asociadas a el id categorización(materno): ${data[0]}</caption>
        `
            let categorizaciones = ``
            categorizaciones = await $.ajax({
                method: "GET",
                url: `${GetWebApiUrl()}HOS_Categorizacion/RecienNacido/${data[0]}`,
                async: true,
                success: function (data) {

                }, error: function (err) {
                    console.error("Ha ocurrido un error al intentar buscar la categorizacion del recien nacido")
                    console.log(err)
                }
            })
            if (categorizaciones.length > 0) {
                contenido += `
            <tr>
                <th>ID</th>
                <th>Paciente</th>
                <th>Riesgo</th>
                <th>Dependencia</th>
                <th>Codigo</th>
            </tr>
        `
                categorizaciones.map(cat => {
                    contenido += `
            <tr>
                <td>${cat.Id}</td>
                <td>${cat.Paciente !== null ? `${cat.Paciente.Nombre ?? "Sin informacion"} ${cat.Paciente.ApellidoPaterno ?? ""}` : `Sin informacion`}</td>
                <td>${cat.NivelRiesgo !== null ? `${cat.NivelRiesgo.Valor}` : `Sin informacion`}</td>
                <td>${cat.NivelDependencia !== null ? `${cat.NivelDependencia.Valor}` : `Sin informacion`}</td>
                <td>${cat.Categorizacion ?? `Sin informacion`}</td>
            </tr>
            `
                })
            } else {
                contenido += `<tr><td><i class="fa fa-info-circle fa-lg"></i> No se encontró categorización de recién nacidos asociados a ${$("#txtCategorizacionNombrePaciente").text()} con fecha ${data[1]}</td></tr>`
            }
            contenido += `</table>`
            return (contenido);
        }
    }
    $('button.dt-control-categorizacion').unbind()

    $("button.dt-control-categorizacion").on('click', async function (e) {
        let tr = e.target.closest('tr');
        let row = table.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            row.child(await format(row.data())).show();
        }
    });
}

function buscarDetallesCategorizacionPorId(button, idCategorizacion) {
    if (idCategorizacionDetalles !== idCategorizacion) {
        cerrarPopOvers()
        promesaAjax("GET", `HOS_Categorizacion/Detalles/${idCategorizacion}`).then(res => {
            if (res !== null && res !== undefined) {
                
                let contenidoPopoverDetalle = ``, totalPuntaje = 0

                res.Detalles.map(det => {
                    contenidoPopoverDetalle += `<div class="col-md-12 p-2">   
                <b class="text-primary">${det.Variable}</b>
                ${det.Frecuencia} - Puntaje: ${det.Puntaje}
                </div>`
                    totalPuntaje += det.Puntaje
                })
                let popoverDetallesCategorizacion = `<div class="row p-1">${contenidoPopoverDetalle}
            <div class="col-md-12 p-2"><b>Total puntaje: </b>${totalPuntaje}</div></div>`


                $(button).popover({
                    html: true,
                    placement: 'right',
                    content: popoverDetallesCategorizacion,
                    template: '<div class="popover popover-xxl"  role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
                })
                $(button).popover('show')
                idCategorizacionDetalles = idCategorizacion
            }
        }).catch(error => {
            console.error("ocurrio un error al intentar buscar detalles de categorizacion")
            console.log(error)
        })
    } else {
        cerrarPopOvers()
        idCategorizacionDetalles = undefined
    }
}

function peticionCategorizacion(url) {
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data, status, jqXHR) {
            let ordenado = [...new Set(data.map(a => a.TipoEvaluacion.Valor))].map(b => {
                return {
                    TipoEvaluacion: b,
                    IdTipoEvaluacion: data.find(c => c.TipoEvaluacion.Valor == b).TipoEvaluacion.Id,
                    Items: data.filter(c => c.TipoEvaluacion.Valor == b)
                }
            })
            cargarPreguntasCategorizacion(ordenado)
        },
        error: function (jqXHR, status) {
            console.log("Error al Cargar la categorizacion: " + JSON.stringify(jqXHR));
        }
    });
}
function cargarPreguntasCategorizacion(data) {
    let contenidoPadre = ``, contenidoHijo = ``, contenidoAlternativas = "", tooltipPregunta = ""
    $("#categorizacionPreguntas").empty()
    data.map(x => {
        contenidoHijo = ""
        x.Items.map((y, index) => {

            tooltipPregunta = ""
            contenidoAlternativas = ""
            y.Alternativas.map(z => {
                contenidoAlternativas += `
                                <td onClick="seleccionarRadio(${y.Id},${z.Puntaje})">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="radio" name="inlineRadioOptions-${y.Id}" data-required="true" data-tipoevaluacion="${x.IdTipoEvaluacion}" id="inlineRadio-${y.Id}-${z.Puntaje}" value="${z.Puntaje}">
                                  <label class="form-check-label" for="inlineRadio-${y.Id}-${z.Puntaje}">${z.Puntaje}</label>
                                </td></div>`
                tooltipPregunta += `<p>[${z.Puntaje}]-${z.Descripcion}. </p>`
            })
            contenidoHijo += `<tr><td>
                                    <p><b>${y.Nombre}</b></p>
                                    <p>${y.Descripcion}</p>
                              </td>
                              <td>
                                    <table class="table table-bordered mt-2">
                                    <tr>
                                    ${contenidoAlternativas}
                                    <tr>
                                    </table>
                              </td>
                              <td>


                                <button type="button"
                                class="btn btn-outline-info mt-2"
                                data-toggle="tooltip"
                                data-placement="right"
                                data-html="true"
                                title="<div style='padding:2;'>${tooltipPregunta}</div>">
                                  Detalles
                                </button>
                              </td>
                              </tr>
                              `
        })

        contenidoPadre += `<tr class="table-success">
                               <th>
                                    Parametros de evaluacion de ${x.TipoEvaluacion}
                               </th>
                               <th>Puntaje</th>
                               <th>Ver</th>
                            </tr>
                            ${contenidoHijo}`
    })
    $("#categorizacionPreguntas").append(contenidoPadre)
    //$("#mdlCategorizacion").modal('show')
    $('[data-toggle="tooltip"]').tooltip()
}

function categorizarPacienteAusente() {
    Swal.fire({
        icon: 'info',
        title: 'Categorizando paciente ausente',
        text: `Use está opción cuando el paciente está ausente temporalmente (en procedimiento, en baño, cirugia..etc.) y por eso usted no puede evaluarlo.`,
        showCancelButton: true,
        confirmButtonText: 'Categorizar',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        console.log(result)
        if (result.value == true) {
            categorizarPaciente(130)
        }
    })
}

async function categorizarPaciente(estado = 129) {
    let urlCategorizacion = ``, mensaje = ``
    let nombrePaciente = $("#txtCategorizacionNombrePaciente").text()
    let camaActual = $("#txtCategorizacionNombreCama").text()
    let valido = true;
    if (estado == 129)
        valido = validarCamposInputRadio("#categorizacionPreguntas", true)

    if (valido) {
        let ArregloRespuestas = {}
        let ubicacionReal = $("#inputIdUbicacion").val()
        let idCamaReal = $("#inputIdCama").val()
        idHosp = parseInt($("#txtCategorizacionIdHospitalizacion").text())

        if (categorizarRecienNacido) {
            urlCategorizacion = `${GetWebApiUrl()}HOS_Categorizacion/RecienNacido`
            ArregloRespuestas.IdCategorizacion = parseInt($("#inputIdCategorizacion").val())
            let idPaciente = await GuardarPaciente()
            ArregloRespuestas.IdPaciente = idPaciente

            mensaje = `Recien nacido ha sido categorizado`
        } else {
            urlCategorizacion = `${GetWebApiUrl()}HOS_Categorizacion`
            if (ubicacionReal !== undefined && idCamaReal !== undefined) {
                ArregloRespuestas = {
                    IdHospitalizacion: idHosp,
                    IdCama: idCamaReal,
                    IdUbicacion: ubicacionReal,
                    IdEstado: estado,
                    VersionSaludMental: document.getElementById('checkFormularioSaludMental').checked
                }
                mensaje = `Paciente: ${nombrePaciente}, en la cama: ${camaActual}, ha sido categorizado.`
            } else {
                Swal.fire(
                    'Error',
                    'Categorizacion no realizada, no se pudo obtener la información de la cama y/o ubicación del paciente.',
                    'error'
                )
            }
        }

        let variables = []
        //Respuestas de la categorizacion, solo cuando el paciente no esta ausente
        if (estado == 129) {
            let inputRadio = $("#categorizacionPreguntas").find("input[type='radio']:checked")
            inputRadio.map((index, input) => {
                variables.push({
                    IdVariableCategorizacion: parseInt(idElementoActual = $(input).attr("id").split("-")[1]),
                    IdTipoEvaluacion: parseInt($(input).data("tipoevaluacion")),
                    Puntaje: parseInt(input.value)
                })
            })
            ArregloRespuestas.Variables = variables
        }
        ShowModalCargando(true)
        $.ajax({
            type: 'POST',
            url: urlCategorizacion,
            contentType: "application/json",
            data: JSON.stringify(ArregloRespuestas),
            success: function (data, status, jqXHR) {
                ShowModalCargando(false)
                if (!categorizarRecienNacido) {
                    $("#mdlCategorizacion").modal("hide")
                    Swal.fire(
                        'Operación exitosa!',
                        mensaje,
                        'success'
                    ).then(result => {
                        if (result) {
                            if (categorizandoDesdeMapaCamas) {
                                //CargarMapaCamas(true)
                                promesaAjax("GET", `HOS_categorizacion/Buscar?idHospitalizacion=${idHosp}&idUbicacion=${ubicacionReal}&fecha=${moment(GetFechaActual()).format("YYYY-MM-DD")}`).then(res => {
                                    if (res.length > 0) {
                                        let codigoCompleto = res[0].Categorizacion, color, clase 
                                        code = codigoCompleto.split("")[0];
                                        console.log(code)
                                        switch (code) {
                                            case "A":
                                                //Red
                                                color = "#dc3545";
                                                clase = `danger`
                                                break
                                            case "B":
                                                //Yellow
                                                color = "#DBA607";
                                                clase = `warning`
                                                break
                                            case "C":
                                                //Green 
                                                color = "#17a2b8";
                                                clase = `info`
                                                break
                                            case "D":
                                                //Blue
                                                color = "#28a745";
                                                clase = `success`
                                                break
                                            default:
                                                clase = `dark`
                                                color = "black";
                                                break
                                        }
                                        $(`#iCama_${idCamaReal}`).css("color", color)
                                        $(`#divCategorizacionCama_${idCamaReal}`).append(`<h5><span class="badge badge-${clase} w-100" data-toggle="tooltip" data-placement="bottom" title="${code}" >  ${codigoCompleto}</span></h5>`)
                                    }
                                }).catch(error => {
                                    console.error("Ocurrio un error al intentar buscar la categorizacion")
                                    console.log(error)
                                })
                            }
                        }
                    })
                } else {
                    //Categorizando recien nacido
                    $("#mdlCategorizacion").modal("hide")
                    Swal.fire({
                        icon: 'success',
                        title: 'Recién nacido ha sido categorizado.',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }

            },
            error: function (err) {
                ShowModalCargando(false)
                console.error("Ha ocurrido un error al intentar categorizar un paciente")
                console.log(err)
            }
        })

    } else {
        Swal.fire(
            'Falta información!',
            'Hay preguntas sin respuesta en el cuestionario.',
            'error'
        )
    }

}

function seleccionarRadio(id, puntaje) {
    let inputRadio = document.getElementById(`inlineRadio-${id}-${puntaje}`)
    inputRadio.checked = true
}