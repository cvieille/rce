﻿$('#mdlConsentimiento').on('hidden.bs.modal', function (e) {
    limpiarPersona()
})

const linkMostrarModalConsentimiento = async (sender) => {
    let consentimientosPaciente = []
    
    $("#txtPacienteConsentimiento").val($(sender).data("pac"))
    $("#txtIdPacienteConsentimiento").val($(sender).data("idpaciente"))
    $("#idAtencionConsentimineto").val($(sender).data("id"))
    $("#TipoAtencionConsentimiento").val($(sender).data("tipoatencion"))
    $("#txtFechaConsentimiento").val(moment().format("DD-MM-yyyy"))
    inicializarComponentesConsentimiento()
    ValidarNumeros()
    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#checkAutoRepresenta").bootstrapSwitch()
    $("#sltAceptaConsentimiento").bootstrapSwitch()

    if ($(sender).data("idpaciente") !== undefined) {
        await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Consentimiento/Buscar?idPaciente=${parseInt($(sender).data("idpaciente"))}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                consentimientosPaciente = data.Hospitalizacion.map(a => {
                    return [
                        a.Id,
                        a.TipoConsentimiento.Valor,
                        moment(a.FechaConsentimiento).format("DD-MM-YYYY HH:mm"),
                        a.Persona !== null ? `${a.Persona.Nombre} ${a.Persona.ApellidoPaterno}` : `Se autorepresenta`,
                        a.TipoEstado.Valor,
                        a.TipoEstado.Id
                    ]
                })
            }, error: function (err) {
                console.log("Ha ocurrido un error al intentar obtener consentimientos del paciente.")
                console.error(err)
            }
        })
    }

    $("#tblHistorialConsentimientos").addClass("table table-hover").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        columns: [
            { title:"Id" },
            { title:"Ambito" },
            { title:"Fecha" },
            { title:"Representante" },
            { title:"Estado" },
            { title:"IdEstado" },
            { title:"Acciones" }
        ],
        data: consentimientosPaciente,
        columnDefs: [
            {
                targets: -1,
                render: function (data,fila,datos,meta) {
                    let buttons = `<div>
                                <button type="button" class="btn btn-primary m-1"
                                data-toggle="tooltip" data-placement="left" title="Imprimir"
                                data-original-title="Imprimir"
                                onclick="ImprimirApiExterno('${GetWebApiUrl()}GEN_Consentimiento/${consentimientosPaciente[meta.row][0]}/Imprimir')"
                                ><i class="fa fa-print"></i>
                                </button>`
                    if (consentimientosPaciente[meta.row][5] !== 145 && consentimientosPaciente[meta.row][5] !== 146) {
                        buttons += `<button type="button" class="btn btn-danger m-1"
                                data-toggle="tooltip" data-placement="left" title="Revocar"
                                onclick="revocarConsentimiento(${consentimientosPaciente[meta.row][0]})"
                                data-original-title="Revocar"><i class="fa fa-times" aria-hidden="true"></i>
                                </button>`
                    }
                    buttons += `</div>`
                    return buttons
                }
            },
            {
                targets: -2,
                visible:false
            }
        ],
        destroy:true
    })

    $("#checkAutoRepresenta").on('switchChange.bootstrapSwitch', function (event, state) {
        if (!state) {
            $("#divAcomConsentimiento").slideDown()
        } else {
            $("#divAcomConsentimiento").slideUp()
        }
    });
    
    $("#mdlConsentimiento").modal("show")
}

function inicializarComponentesConsentimiento() {
    comboIdentificacion("#sltTipoIdentificacionConsentimiento", "#labelTipoIdentificacion")
    InicializarComponentesRut("#sltTipoIdentificacionConsentimiento", "#txtnumeroDocAcomCosentimiento", "#txtDigAcomConsentimiento", "#labelTipoIdentificacion")
    InicializarInputRut("#sltTipoIdentificacionConsentimiento", "#txtnumeroDocAcomCosentimiento", "#txtDigAcomConsentimiento")
    ComboTipoRepresentante("#sltTipoAcomConsentimiento")
}
async function generarConsentimiento() {
    let valido = true
    let objetoPersona = {}
    if (!$("#checkAutoRepresenta").bootstrapSwitch('state')) {
        valido = validarCampos("#divAcomConsentimiento", false)
    }
    //los campos validados exitosamente
    if (valido) {
        let jsonApi = {
            IdPaciente: parseInt(document.getElementById("txtIdPacienteConsentimiento").value),
            IdTipoConsentimiento: 1,//Hospitalizacion
            IdHospitalizacion: parseInt(document.getElementById("idAtencionConsentimineto").value),
            PacienteAcepta: $("#sltAceptaConsentimiento").bootstrapSwitch("state")
        }
        //Esto es cuando el paciente no puede representarse el mismo
        if (!$("#checkAutoRepresenta").bootstrapSwitch('state')) {
            objetoPersona = {
                IdIdentificacion: parseInt(document.getElementById("sltTipoIdentificacionConsentimiento").value),
                NumeroDocumento: document.getElementById("txtnumeroDocAcomCosentimiento").value,
                Digito: document.getElementById("txtDigAcomConsentimiento").value,
                Nombre: document.getElementById("txtnombreacompanante").value,
                ApellidoMaterno: document.getElementById("txtapellidopcompanante").value,
                ApellidoPaterno: document.getElementById("txtapellidomacompanante").value
            }
            jsonApi.IdTipoRepresentante = parseInt(document.getElementById("sltTipoAcomConsentimiento").value)
        } else {
            //Aca deberia pasar cuando el paciente se auto representa
            await $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/${jsonApi.IdPaciente}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    //La persona que lo representa va a ser el paciente mismo, busqueda de paciente e ingreso como representante.
                    objetoPersona = {
                        IdIdentificacion: data.Identificacion.Id,
                        NumeroDocumento: data.NumeroDocumento,
                        Digito: data.Digito,
                        Nombre: data.Nombre,
                        ApellidoMaterno: data.ApellidoMaterno,
                        ApellidoPaterno: data.ApellidoPaterno
                    }
                }, error: function (err) {
                    console.error("Ha ocurrido un erro al buscar el paciente")
                    toastr.error("Ha ocurrido un error al buscar el paciente.")
                    console.log(err)
                }
            })
        }
        jsonApi.Persona = objetoPersona

        console.log(jsonApi)
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}GEN_Consentimiento`,
            contentType: 'application/json',
            data: JSON.stringify(jsonApi),
            dataType: 'json',
            success: function (data) {
                $("#mdlConsentimiento").modal("hide")
                limpiarPersona()
                Swal.fire({
                    title: 'Consentimiendo docente creado',
                    text: "¿Quiere generar el documento?",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Imprimir',
                    cancelButtonText: 'Cerrar',
                }).then((result) => {
                    if (result) {
                        ImprimirApiExterno(`${GetWebApiUrl()}GEN_Consentimiento/${data.Id}/Imprimir`)
                    }
                })
            }, error: function (err) {
                console.error("Error ingresando el consentimiento docente")
                console.error(err)
            }
        })
    } else {
        //Fallo la verificacion de los campos requeridos
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Faltan información obligatorios',
            showConfirmButton: false,
            timer: 1500,
            allowOutsideClick: false
        });
    }
}
function revocarConsentimiento(idConsentimiento) {

    Swal.fire({
        title: 'Revocar consentimiento docente',
        text: `¿Revocar consentimiento docente con id ${idConsentimiento}`,
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Revocar',
        cancelButtonText: 'Cerrar',
    }).then((result) => {
        if (result) {
            $.ajax({
                type: 'PATCH',
                url: `${GetWebApiUrl()}GEN_Consentimiento/Revocar/${idConsentimiento}`,
                contentType: 'application/json',
                success: function (data) {
                    $("#mdlConsentimiento").modal("hide")
                    Swal.fire({
                        title: 'Consentimiendo docente revocado',
                        text: "¿Quiere generar el documento?",
                        icon: 'info',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Imprimir',
                        cancelButtonText: 'Cerrar',
                    }).then((result) => {
                        if (result) {
                            ImprimirApiExterno(`${GetWebApiUrl()}GEN_Consentimiento/${idConsentimiento}/Imprimir`)
                        }
                    })
                }, error: function (err) {
                    console.error("Error revocando el consentimiento docente")
                    console.error(err)
                }
            })
        }
    })
}

