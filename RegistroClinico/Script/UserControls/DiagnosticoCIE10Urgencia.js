﻿
let diagnosticosSeleccionados = [];
let divPadre = "";
function InicializarDatosDiagnostico(divPadreSeleccionado, cargarFavoritos = true) {

    const { CODIGO_PERFIL } = getSession();
    divPadre = divPadreSeleccionado; 
    limpiarDiagnosticos();

    CargarDiagnosticosCIE10();

    $(`#${divPadre}_btnAgregarPorCodigoDiag`).unbind().on('click', (evt) => {
        agregarPorCodigoDiagnosticoCIE10();
    });

    $(`#${divPadre}_btnAgregarDiagnosticoAlta`).unbind().on('click', (evt) => {
        evt.preventDefault();
        if (validarDiagnostico()) {
            clickAgregarDiagnostico();
        }
    });

    //Dibujando los diagnostivos mas frecuentes (diagnosticos favoritos) pendiente obtener el tipo de atencion
    $(`#${divPadre}_divDiagnosticosFrecuentesUc`).parent().hide();
    if (cargarFavoritos) {
        promesaAjax('GET', `GEN_Diagnostico_CIE10/1/Favoritos`).then(res => {
            $(`#${divPadre}_divDiagnosticosFrecuentesUc`).empty()
            res.forEach(item => {
                $(`#${divPadre}_divDiagnosticosFrecuentesUc`).parent().show();
                $(`#${divPadre}_divDiagnosticosFrecuentesUc`).append(`<div class="col-md-3 col-xs-12"> <button class="btn btn-outline-dark btn-sm m-1 w-100" onclick='agregarDiagnosticoAccesoRapido(${JSON.stringify(item)})'  type="button" style="overflow: hidden; ">${item.Alias.Valor}</button></div>`)
            })
        }).catch(error => {
            console.error("Error al obtener diagnosticos mas frecuentes")
            console.log(error)
        })
    }

    if (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia) {
        $(`#${divPadre}_divClasificacionDiagnostico`).hide();
        $(`#${divPadre}_sltClasificacionDiagnostico`).removeAttr("data-required");
    } else {
        if ($(`#${divPadre}_sltClasificacionDiagnostico`).is(':empty'))
            setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Diagnostico_CIE10/Clasificacion/Combo`, false, `#${divPadre}_sltClasificacionDiagnostico`);
    }
}

function agregarDiagnostico(json) {
    /*{ estructura del json recibido
        Alias:ListaGenericaIdValor,
        AliasCompleto: "Alias + Codigo"
        Diagnostico: ListaGenericaIdValor
        DiagnosticoPadre: ListaGenericaIdValor
    }*/
    $(`#${divPadre}_txtDiagnosticoCIE10, #${divPadre}_txtDiagnosticoCIEPorCodigo, #${divPadre}_btnAgregarPorCodigoDiag`).attr("disabled", "disabled");
    $(`#${divPadre}_txtDiagnosticoCIEPorCodigo`).removeClass("invalid-input");
    $(`#${divPadre}_txtDiagnosticoCIEPorCodigo`).val("");
    $(`#${divPadre}_txtDiagnosticoCIE10`).data('diagnostico', json);
    mostrarDiagnosticoSeleccionado(json);
}

function mostrarDiagnosticoSeleccionado(json) {
    $(`#${divPadre}_divDiagnosticoSeleccionado`).html(`
        <a id='${divPadre}_aQuitarDiagnosticoSeleccionado' style="position:absolute;right:10px;top:5px;font-weight:bold;cursor:pointer;">X</a>
            <span style='font-size:15px !important;'>${(json.DiagnosticoPadre != null) ? `${json.DiagnosticoPadre.Valor??""}<br />` : ""}</span>
            <span style='font-size:${(json.DiagnosticoPadre != null) ? "12px" : "15px"} !important;'>${json.Diagnostico.Valor}</span><br />
            <span style='font-size:13px !important;font-weight:bold;'>${json.AliasCompleto ?? ""}</span>`);
    $(`#${divPadre}_divDiagnosticoSeleccionado`).show();
    $(`#${divPadre}_aQuitarDiagnosticoSeleccionado`).click(() => quitarDiagnosticoSeleccionado());
}

function quitarDiagnosticoSeleccionado() {
    $(`#${divPadre}_divDiagnosticoSeleccionado`).empty();
    $(`#${divPadre}_divDiagnosticoSeleccionado`).hide();
    $(`#${divPadre}_txtDiagnosticoCIE10, #${divPadre}_txtDiagnosticoCIEPorCodigo, #${divPadre}_btnAgregarPorCodigoDiag`).removeAttr("disabled");
    $(`#${divPadre}_txtDiagnosticoCIEPorCodigo`).removeClass("invalid-input");
    $(`#${divPadre}_txtDiagnosticoCIE10, #${divPadre}_txtDiagnosticoCIEPorCodigo`).val("");
    $(`#${divPadre}_txtDiagnosticoCIE10`).data('diagnostico', null);
}

function diagnosticExists(idDiagnostico, idAlias = null) {
    var exist = false;
    $(`#${divPadre}_tblDiagnosticosAlta tbody tr`).each(function (index) {
        if ($(this).attr("data-idDiagnostico") == idDiagnostico || (idAlias != null && $(this).attr("data-id-alias") == idAlias))
            exist = true;
    });
    return exist;
}

function clickAgregarDiagnostico() {

    const item = $(`#${divPadre}_txtDiagnosticoCIE10`).data("diagnostico");
    if (!diagnosticExists(item.Diagnostico.Id, item.Alias.Id)) {

        const { CODIGO_PERFIL } = getSession();
        const json = {
            Clasificacion: {
                Id: (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia) ? 2 : valCampo(parseInt($(`#${divPadre}_sltClasificacionDiagnostico`).val())),
                Valor: (CODIGO_PERFIL === perfilAccesoSistema.digitadorUrgencia) ? "Confirmación" : $(`#${divPadre}_sltClasificacionDiagnostico option:selected`).text()
            },
            DetalleDiagnostico: $.trim($(`#${divPadre}_txtDiagnosticoDetalle`).val()),
            DiagnosticoPrincipal: $(`#${divPadre}_tblDiagnosticosAlta tbody tr`).length === 0
        };
        agregarFilaDiagnostico({ ...item, ...json }, () => eliminarDiagnostico(item.Alias.Id));
    } else {
        Swal.fire({
            title: 'Diagnóstico',
            text: 'Este diagnóstico ya está ingresado.',
            icon: 'warning'
        });
        throw new Error("El diagnostico ya ha sido agregado")
    }
    var time = setTimeout(function () {
        $(`#${divPadre}_txtDiagnosticoCIE10`).val("");
        clearTimeout(time);
    }, 10);

    quitarDiagnosticoSeleccionado();
    $(`#${divPadre}_sltClasificacionDiagnostico`).val("0");
    $(`#${divPadre}_txtDiagnosticoDetalle`).val("");

}

function agregarPorCodigoDiagnosticoCIE10() {

    const codigo = $.trim($(`#${divPadre}_txtDiagnosticoCIEPorCodigo`).val());
    if (codigo != "") {
        ShowModalCargando(true);
        $.ajax({
            type: "GET",
            url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/PorCodigo?codigo=${codigo}`,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                
                if (!diagnosticExists(data.Diagnostico.Id)) {
                    if (data.Alias.length > 1) {

                        toastr.info("El código ingresado arrojo varios resultados, debe ingresar uno.")
                        $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).empty()
                        $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).data("data", data)

                        data.Alias.forEach(x => {
                            $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).append(`<option value=${x.Id}>${x.Valor}</option>`)
                        })

                        $(`#${divPadre}_divBusquedaDiagnosticoUc`).hide()
                        $(`#${divPadre}_divSeleccionAliasDiagnosticoUc`).show()
                    } else {
                        agregarDiagnostico(data);
                    }
                } else {
                    toastr.error("El diagnóstico buscado ya ha sido agregado a esta solicitud.")
                }
                ShowModalCargando(false);
            },
            error: function (jqXHR, status) {
                console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
                if (jqXHR.status === 404)
                    $(`#${divPadre}_txtDiagnosticoCIEPorCodigo`).removeClass("invalid-input").addClass("invalid-input");
                ShowModalCargando(false);
            }
        });
    }

}

function setTypeAheadDiagnostico(data) {
    const typeAhObject2 = {
        input: `#${divPadre}_txtDiagnosticoCIE10`,
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnostico especificado ({{query}})",
        display: ["AliasCompleto"],
        template:
            '<span>' +
                '<span>{{AliasCompleto}}</span> ' +
            '</span>',
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                if (!diagnosticExists(item.Diagnostico.Id, item.Alias.Id))
                    agregarDiagnostico(item);
            }
        }
    };

    $(`#${divPadre}_txtDiagnosticoCIE10`).on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    });

    return typeAhObject2;
}

async function cargarHipotesisDiagnostica(idDau) {
    const diagnosticos = await GetHipotesisDiagnosticos(idDau);
    await cargarDiagnostico(diagnosticos);
}

async function cargarDiagnostico(diagnosticos) {

    $(`#${divPadre}_tblDiagnosticosAlta`).hide();
    $(`#${divPadre}_tblDiagnosticosAlta tbody`).empty();

    if (diagnosticos.length > 0) {
        // Limpiar la tabla antes de añadir nuevos diagnósticos
        $(`#${divPadre}_tblDiagnosticosAlta tbody`).empty();
        diagnosticos.forEach(json => {
            agregarFilaDiagnostico(json, () => eliminarDiagnostico(json.Alias.Id));
        });
        ShowModalCargando(false);
    }
}

async function GetHipotesisDiagnosticos(idDau) {

    let diagnosticos = [];

    try {

        await $.ajax({
            method: "GET",
            url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Diagnosticos`,
            success: function (response) {
                diagnosticos = response;
            },
            error: function (jqXHR) {
                if (jqXHR.status === 404) { }
            }
        });

    } catch (ex) {

    }

    return diagnosticos;
}

function agregarFilaDiagnostico(json, functionQuitarDiagnostico) {
    const countFilas = $(`#${divPadre}_tblDiagnosticosAlta tbody tr`).length;
    var idTr = `${divPadre}_trDiagnosticos_` + countFilas;
    // 2 = hay que cambiarlo cuando se publique la parte del médico
    var tr =
        `<tr id='${idTr}' data-idDiagnostico = '${json.Diagnostico.Id}' data-id-alias="${json.Alias.Id}"  class='text-center'>
            <td class='pt-1 pb-1'colspan=1>
                <span style='font-size:15px !important;'>${(json.DiagnosticoPadre != null) ? `${json.DiagnosticoPadre.Valor}<br />` : ""}</span>
                <span style='font-size:${(json.DiagnosticoPadre != null) ? "12px" : "15px"} !important;'>${json.Diagnostico.Valor}</span><br />
                <span style='font-size:13px !important;font-weight:bold;'>${json.Alias?.Valor ?? ""}</span>
            </td>
            <td>
                <select id="${divPadre}_sltClasificacionDiagnostico_${countFilas}" data-idAlias="${json.Alias.Id}" class="form-control" data-required='true'>
                    ${ $(`#${divPadre}_sltClasificacionDiagnostico`).html() }
                </select>
            </td>
            <td>${ json.DetalleDiagnostico ?? ""}</td>
            <td>
                <input id="${divPadre}_rdoDiagnotico_${countFilas}" 
                       type="radio"
                       data-idAlias="${json.Alias.Id}" 
                       name="${divPadre}_diagnosticoPrincipal" 
                       ${json.DiagnosticoPrincipal ? "checked" : ""} />
            </td>
            <td class='text-center pt-1 pb-1'>
                <a class='btn btn-circle-pretty-small btn-danger' data-idDiagnostico='${json.Diagnostico.Id}' style='color: #ffffff;'>
                    <i class='fa fa-times'></i>
                </a>
            </td>
        </tr>`;

    $(`#${divPadre}_tblDiagnosticosAlta tbody`).append(tr);
    $(`#${divPadre}_sltClasificacionDiagnostico_${(countFilas)}`).val(json.Clasificacion?.Id ?? "0");
    $(`#${divPadre}_sltClasificacionDiagnostico_${(countFilas)} option[value='0']`).remove();
    $(`#${divPadre}_rdoDiagnotico_${countFilas}`).unbind().change(function () {
        if ($(this).is(':checked')) {
            const idAlias = parseInt($(this).attr("data-idAlias"));
            const index = diagnosticosSeleccionados.findIndex(({ IdAlias }) => IdAlias === idAlias);
            diagnosticosSeleccionados.forEach(function (d) { d.DiagnosticoPrincipal = false; });
            diagnosticosSeleccionados[index].DiagnosticoPrincipal = true;
        }
    });
    $(`#${divPadre}_sltClasificacionDiagnostico_${(countFilas)}`).unbind().on('change', function (evt) {
        const idAlias = parseInt($(this).attr("data-idAlias"));
        const index = diagnosticosSeleccionados.findIndex(({ IdAlias }) => IdAlias === idAlias);
        diagnosticosSeleccionados[index].IdClasificacion = valCampo(parseInt($(this).val()));
    });

    $(`#${divPadre}_tblDiagnosticosAlta`).show();
    $(`#${divPadre}_txtDiagnosticoCIE10`).removeClass("invalid-input");

    diagnosticosSeleccionados.push({
        IdAlias: json.Alias.length > 0 ? json.Alias[0].Id :  json.Alias.Id,
        IdClasificacion: json.Clasificacion.Id,
        DiagnosticoPrincipal: json.DiagnosticoPrincipal,
        DetalleDiagnostico: valCampo(json.DetalleDiagnostico)
    });

    $("#" + idTr + " td a").click(function () {
        const sender = this;
        Swal.fire({
            title: "Eliminando diagnóstico",
            text: "Por favor, confirme la acción",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Eliminar diagnóstico."
        }).then((result) => {
            if (result.value) {
                $(sender).parent().parent().remove();
                functionQuitarDiagnostico();
                $(sender).unbind();
                if ($(`#${divPadre}_tblDiagnosticosAlta tbody tr`).length === 1)
                    $(`#${divPadre}_tblDiagnosticosAlta tbody input[type='radio']`).attr("checked", "checked");
            }
        });
    });

}

function eliminarDiagnostico(idAlias) {

    for (var i = 0; i < diagnosticosSeleccionados.length; i++) {
        if (diagnosticosSeleccionados[i].IdAlias == idAlias) {
            diagnosticosSeleccionados.splice(i, 1);
            break;
        }
    }

    if ($.trim($(`#${divPadre}_tblDiagnosticosAlta tbody`).html()) === "") {
        $(`#${divPadre}_tblDiagnosticosAlta`).hide();
    }

}

function validarDiagnosticos() {
    if (diagnosticosSeleccionados.length === 0) {
        $(`#${divPadre}_txtDiagnosticoCIE10`).addClass("invalid-input");
        return false;
    } else {
        $(`#${divPadre}_txtDiagnosticoCIE10`).removeClass("invalid-input");
        return true;
    }
}

function getDiagnosticosOrdenados(data) {

    const diagnosticos = data.map((json) => {
        json.AliasCompleto = `${json.Alias !== null ? `${json.Alias.Valor}` : ""} - ${json.Codigo??""}`;
        return json;
    });
    diagnosticos.sort(function (a, b) {
        return a.AliasCompleto.length - b.AliasCompleto.length;
    });

    return diagnosticos;

}

function validarDiagnostico() {

    let esValido = true;
    $(`#${divPadre}_txtDiagnosticoCIE10`).removeClass("invalid-input");
    if ($(`#${divPadre}_txtDiagnosticoCIE10`).data("diagnostico") == undefined) {
        $(`#${divPadre}_txtDiagnosticoCIE10`).addClass("invalid-input");
        esValido = false;
    }

    if (!validarCampos(`#${divPadre}_divInfoDiagnostico`, false))
        esValido = false;

    return esValido;
}

function existenDiagnosticosAgregados() {
    if ($(`#${divPadre}_tblDiagnosticosAlta tbody tr`).length === 0)
        Swal.fire({
            title: 'Diagnóstico',
            text: 'Debe Agregar por al menos un diagnóstico',
            icon: 'warning'
        });
    return ($(`#${divPadre}_tblDiagnosticosAlta tbody tr`).length > 0)
}

function limpiarDiagnosticos() {
    $(`#${divPadre}_txtDiagnosticoCIE10, #${divPadre}_txtDiagnosticoCIEPorCodigo, #${divPadre}_btnAgregarPorCodigoDiag`).removeAttr("disabled");
    $(`#${divPadre}_txtDiagnosticoCIEPorCodigo, #${divPadre}_txtDiagnosticoCIE10`).val("");
    $(`#${divPadre}_tblDiagnosticosAlta, #${divPadre}_divDiagnosticoSeleccionado`).hide();
    $(`#${divPadre}_tblDiagnosticosAlta tbody`).empty();
    $(`#${divPadre}_txaIndicacionesAlta, #${divPadre}_txtDiagnosticoDetalle`).val("");
    diagnosticosSeleccionados = [];
}

function CargarDiagnosticosCIE10() {
    if (!$._data($(`#${divPadre}_txtDiagnosticoCIE10`)[0]).events) {

        $(`#${divPadre}_txtDiagnosticoCIE10`).on('input', () => $(`#${divPadre}_txtDiagnosticoCIE10`).removeData("diagnostico"));
        $(`#${divPadre}_txtDiagnosticoCIE10`).blur(() => {
            if ($(`#${divPadre}_txtDiagnosticoCIE10`).data("diagnostico") == undefined)
                $(`#${divPadre}_txtDiagnosticoCIE10`).val("");
        });

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/Alias`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                const json = getDiagnosticosOrdenados(data);
                $(`#${divPadre}_txtDiagnosticoCIE10`).typeahead(setTypeAheadDiagnostico(json));
            }
        });

    }
}
function cargarDiagnosticosCIE10Urgencia(diagnosticos) {

    //Diagnóstico: tblDiagnosticos
    if (diagnosticos.length > 0) {
        $(`#${divPadre}_tblDiagnosticosAlta`).show();
        diagnosticos.forEach((item, index) => {
            $(`#${divPadre}_tblDiagnosticosAlta tbody`).append(
                `<tr class='text-center'>
                    <td class='pt-1 pb-1' style='font-size:20px !important;'>${item.Valor}</td>
                    <td class='text-center pt-1 pb-1'><br /></td>
                </tr>`);
        });
    }

}

function getDiagnosticosRealizados() {
    return diagnosticosSeleccionados;
}
function agregarAliasSeleccionado() {
    let diagnosticoSeleccionadoPorAlias = $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).data("data")

    diagnosticoSeleccionadoPorAlias.Alias = {
        Id: $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).val(),
        Valor: $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc option:selected`).text()
    }
    diagnosticoSeleccionadoPorAlias.AliasCompleto = diagnosticoSeleccionadoPorAlias.Codigo + " - " + $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc option:selected`).text()
    $(`#${divPadre}_txtDiagnosticoCIE10`).data("diagnostico", diagnosticoSeleccionadoPorAlias)

    agregarDiagnostico(diagnosticoSeleccionadoPorAlias);
    cancelarSeleccionDiagnosticoPorAlias()
}

function agregarDiagnosticoAccesoRapido(diagnosticoFavorito) {
    agregarDiagnostico(diagnosticoFavorito);
}

function cancelarSeleccionDiagnosticoPorAlias() {
    $(`#${divPadre}_sltSeleccionAliasDiagnosticoUc`).empty()

    $(`#${divPadre}_divBusquedaDiagnosticoUc`).show()
    $(`#${divPadre}_divSeleccionAliasDiagnosticoUc`).hide()
}
function setDataTipoClasificacion(idTipoClasificacion=0) {
    if (idTipoClasificacion !== 0)
        $(`#${divPadre}_sltClasificacionDiagnostico`).val(idTipoClasificacion)
}