﻿
nSession = getSession()
$(document).ready(async function () {

    if (nSession.CODIGO_PERFIL != perfilAccesoSistema.medico && nSession.CODIGO_PERFIL != perfilAccesoSistema.matroneriaUrgencia && nSession.CODIGO_PERFIL != perfilAccesoSistema.enfermeraUrgencia && nSession.CODIGO_PERFIL != perfilAccesoSistema.tensUrgencia) {
        $("#aCerrarAtencion").hide();
        return;
    }
    
    $("#aCerrarAtencion").show();

    $('#mdlCerrarAtencion').on('hidden.bs.modal', function () {
        limpiarCamposCerrarAtencion();
    })
})

async function limpiarCamposCerrarAtencion() {
    $('#observacionesCierreAtencion').val('');
}

async function cargarDatosModalCierre(button = null) {

    ShowModalCargando(true);
    reiniciarInfoHospitalizacion();
    $("#rdoSiHospitalizacion").on('change', function () {
        $("#rdoSiHospitalizacion").parent().parent().parent().removeAttr('style');
        if ($(this).prop("checked"))
            $("#divMotivosRechazoHospitalizacion").hide();
    });
    
    $("#rdoNoHospitalizacion").on('change', function() {
        $("#rdoSiHospitalizacion").parent().parent().parent().removeAttr('style');
        if ($(this).prop("checked"))
            $("#divMotivosRechazoHospitalizacion").show();
    });

    let idAtencionUrgenciaCerrarUc
    if (button !== null)///cuando se hace desde la bandeja
        idAtencionUrgenciaCerrarUc = $(button).data("id")
    else //Cuando se hace desde la atencion clinica
        idAtencionUrgenciaCerrarUc = nSession.ID_INGRESO_URGENCIA

    if (idAtencionUrgenciaCerrarUc == null || idAtencionUrgenciaCerrarUc == undefined)
        throw new Error("No se paso id DAU")

    comboMotivoRechazoHospitalizacion();
    $("#btnCerrarCaso").data("id", idAtencionUrgenciaCerrarUc)
    let categorizacionUrgenciaUc = await getCategorizacion(idAtencionUrgenciaCerrarUc)
    let json = GetJsonIngresoUrgencia(idAtencionUrgenciaCerrarUc);

    if (!json.ValidadoPorMedico) {//No ha sido validad por medico
        Swal.fire({
            icon: 'error',
            title: 'No es posible dar cierre al caso.',
            text: 'Está atención no ha sido validada por un médico, debe estar valida para poder dar el cierre al caso'
        });
        ShowModalCargando(false)
        throw new Error("Está atención no ha sido validada por un médico, debe estar valida para poder dar el cierre al caso")
    }

    let objAtencionUrgencia = {
        idAtencion: json.Id,
        idPaciente: json.IdPaciente,
        motivoConsulta: json.MotivoConsulta,
        categorizacion: categorizacionUrgenciaUc,
        tipoAtencion: json.TipoAtencion
    }
    
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPancienteCierreAtencion");

    mostrarUsuarioLogueadoEnModales("#mdlCerrarAtencion")

    ShowModalCargando(false)
    $("#mdlCerrarAtencion").modal("show")

}

function reiniciarInfoHospitalizacion() {

    $("#divHospitalizacion").hide();
    $("#divMotivosRechazoHospitalizacion").hide();
    $("#txtObservacionesRechazoHospitalizacion").val("");
    $("#sltTipoMotivoRechazo").val("0");
    $("#rdoSiHospitalizacion, #rdoNoHospitalizacion").prop("checked", false);

}

function comboMotivoRechazoHospitalizacion() {
    if ($('#sltTipoMotivoRechazo option').length === 0) {
        let url = `${GetWebApiUrl()}HOS_Tipo_Rechazo_Hospitalizacion/Combo`;
        setCargarDataEnCombo(url, true, $("#sltTipoMotivoRechazo"));
    }
}
async function CerrarAtencionUrgencia(button) {

    let json = {}
    json.Observaciones = $("#observacionesCierreAtencion").val();
    json.RechazoHospitalizacion = null;

    if ($("#divHospitalizacion").is(":visible") && $("#rdoNoHospitalizacion").prop("checked")) { 
        json.RechazoHospitalizacion = {
            IdTipoRechazoHospitalizacion: valCampo($("#sltTipoMotivoRechazo").val()),
            Observaciones: valCampo($("#txtObservacionesRechazoHospitalizacion").val())
        }
    }

    let idDAUuC = $(button).data("id")
    if (idDAUuC == null || idDAUuC == undefined)
        throw new Error("No se paso id DAU")

    ShowModalCargando(true);
    let tieneIngresoMedico = await dauTieneIngresoMedico(idDAUuC);
    let tieneDiagnostico = await dauTieneDiagnosticos(idDAUuC);
    let tieneIndicacionesPendientes = await dauTieneIndicacionesPendientes(idDAUuC);
    let tieneAltaMedica = await dauTieneAltaMedica(idDAUuC);
    json.Id = idDAUuC

    if (tieneIngresoMedico && tieneDiagnostico && tieneIndicacionesPendientes && tieneAltaMedica) {
        await patchCerrarAtencionUrgencia(json);
    } else {
        // Si alguna validación falla, habilitar el botón nuevamente
        $("#btnCerrarCaso").prop('disabled', false);
    }
}

async function patchCerrarAtencionUrgencia(json) {

    if (json === undefined)
        return

    ShowModalCargando(false);
    let nombreUsuario = obtenerNombreUsuario();
    Swal.fire({
        title: 'Usted está dando por cerrado el caso.',
        text: 'Si confirma cerrar el caso no volverá a aparecer en la bandeja de urgencia ni en el mapa de camas.'
            + 'El usuario que realiza el cierre es ' + nombreUsuario + '. '
            + 'Puede confirmar o ignorar la acción',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then(async (result) => {
        if (result.value) {
            const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.Id}/Cierre`
            await $.ajax({
                type: "PATCH",
                url: url,
                data: JSON.stringify(json),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: async function (data, status, jqXHR) {
                    //await ImprimirApiExternoAsync(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.Id}/Imprimir/Receta`)
                    //await ImprimirApiExternoAsync(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.Id}/Imprimir`)
                    if (jqXHR.status === 204) {
                        setSession('ID_ATENCION_URGENCIA', json.Id);
                        window.location.href = `${ObtenerHost()}/Vista/ModuloUrgencia/VistaMapaCamasUrgencia.aspx`;
                        toastr.success(`Solicitud realizada`);
                    }
                    else {
                        console.log("Código de estado: " + jqXHR.status);
                    }
                },
                error: async function (jqXHR, status) {
                    console.log("Error al actualizar interconsultor urgencia: " + JSON.stringify(jqXHR));
                    // En caso de error, habilitar el botón nuevamente
                    $("#btnCerrarCaso").prop('disabled', false);
                }
            });
        }
    })
}

async function dauTieneIngresoMedico(idDau) {

    let esValido = true

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Medico`,
        success: function (data) {

        }, error: function (jqXHR) {

            if (jqXHR.status === 404) {

                ShowModalCargando(false);
                Swal.fire({
                    title: 'Error',
                    text: "Se debe agregar Anamnesis y Examen físico.",
                    icon: 'warning',
                    position: 'center',
                    showConfirmButton: true,
                })               
                esValido = false;
                // En caso de error, habilitar el botón nuevamente
                $("#btnCerrarCaso").prop('disabled', false);
            }
        }
    })

    return esValido
}

async function dauTieneDiagnosticos(idDau) {

    let esValido = true

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Diagnosticos`,
        success: function (data) {

        }, error: function (jqXHR) {

            if (jqXHR.status === 404) {

                ShowModalCargando(false);
                Swal.fire({
                    title: 'Error',
                    text: "Se debe agregar al menos un diagnóstico.",
                    icon: 'warning',
                    position: 'center',
                    showConfirmButton: true,
                })
                esValido = false
                // En caso de error, habilitar el botón nuevamente
                $("#btnCerrarCaso").prop('disabled', false);
            }
        }
    })

    return esValido;
}

async function dauTieneIndicacionesPendientes(idDau) {

    let esValido = true

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/IndicacionesPendientes`,
        success: function (response) {

            const { Procedimientos, Medicamentos, ExamenesLaboratorio, ExamenesImagenologia, Interconsultor } = response

            if (Procedimientos > 0 || Medicamentos > 0 || ExamenesLaboratorio > 0 || ExamenesImagenologia > 0 || Interconsultor > 0) {

                let texto = (Procedimientos > 0) ? `Procedimientos Pendientes: <strong>${ Procedimientos }</strong><br />` : ``;
                texto += (Medicamentos > 0) ? `Medicamentos Pendientes: <strong>${ Medicamentos }</strong><br />` : ``;
                texto += (ExamenesLaboratorio > 0) ? `Exámenes Laboratorio Pendientes: <strong>${ ExamenesLaboratorio }</strong><br />` : ``;
                texto += (ExamenesImagenologia > 0) ? `Exámenes Imagenología Pendientes: <strong>${ ExamenesImagenologia }</strong><br />` : ``;
                texto += (Interconsultor > 0) ? `Interconsultores Pendientes: <strong>${ Interconsultor }</strong><br />` : ``;

                ShowModalCargando(false);
                Swal.fire({ title: 'Indicaciones pendientes', html: `${texto}`, icon: 'warning' }).then((result) => {})               
                esValido = false;
                // En caso de error, habilitar el botón nuevamente
                $("#btnCerrarCaso").prop('disabled', false);
            }
        }, error: function (jqXHR) {

            if (jqXHR.status === 404) { }
            ShowModalCargando(false);
            console.log("Error: " + JSON.stringify(jqXHR))
        }
    })

    return esValido
}

async function dauTieneAltaMedica(idDau) {

    let esValido = true

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/Alta/Medica`,
        success: async function (altas) {

            if (altas[altas.length - 1].TipoDestinoUrgencia.Id === 2) {

                ShowModalCargando(false);

                if ($("#divHospitalizacion").is(":visible")) {

                    // Verificar si el paciente tiene un Formulario LPP
                    let tieneFormularioLPP = await validarFormularioLPP(idDau);

                    if (!tieneFormularioLPP) {
                        Swal.fire({
                            title: 'Debe agregar al menos un LPP',
                            text: "El destino es Hospitalización, pero no se ha agregado un Formulario LPP. Por favor, agrégalo antes de cerrar la atención.",
                            icon: 'warning',
                            showConfirmButton: true,
                            timer: 8000
                        });
                        esValido = false;
                        // En caso de error, habilitar el botón nuevamente
                        $("#btnCerrarCaso").prop('disabled', false);
                        return;
                    }

                    if (!$("#rdoSiHospitalizacion").prop("checked") && !$("#rdoNoHospitalizacion").prop("checked")) {
                        $("#rdoSiHospitalizacion").parent().parent().parent().css('border', '1px solid red');
                        esValido = false;
                        return;
                    }

                    if ($("#rdoSiHospitalizacion").prop("checked")) {
                        esValido = true;
                        return;
                    }

                    esValido = validarCampos("#divHospitalizacion", false);
                    return;
                }
                
                reiniciarInfoHospitalizacion();
                $("#divHospitalizacion").show();
                esValido = false;

            }

        }, error: function (jqXHR) {

            if (jqXHR.status === 404) {
                ShowModalCargando(false);
                Swal.fire({
                    title: 'Error',
                    text: "Se debe agregar al menos un alta médica.",
                    icon: 'warning',
                    position: 'center',
                    showConfirmButton: true,
                })
                esValido = false;
                // En caso de error, habilitar el botón nuevamente
                $("#btnCerrarCaso").prop('disabled', false);
            }
        }
    })

    return esValido;
}

async function validarFormularioLPP(idDau) {
    let esValido = true;

    await $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}/URG_Atenciones_Urgencia/${idDau}/LPP`,
        success: function (data) {
            if (data.length === 0) {
                esValido = false;
            }
        },
        error: function (jqXHR) {
            esValido = false;
        }
    });

    return esValido;
}
