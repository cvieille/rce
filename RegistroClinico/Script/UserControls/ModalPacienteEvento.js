﻿
function ShowPacienteEventos(idPaciente) {
    console.log(idPaciente)
    GetEvento().GEN_idPaciente = idPaciente;

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}RCE_Eventos/GEN_idPaciente/${idPaciente}`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            var adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.RCE_idEventos,
                    val.RCE_descripcionTipo_Evento,
                    val.RCE_anamnesisEventos,
                    val.RCE_diagnosticoEventos,
                    (val.RCE_alta_pacienteEventos !== null) ? moment(val.RCE_alta_pacienteEventos).format('DD-MM-YYYY') : '',
                    (val.RCE_fecha_inicioEventos !== null) ? moment(val.RCE_fecha_inicioEventos).format('DD-MM-YYYY') : '',
                    (val.RCE_fecha_terminoEventos !== null) ? moment(val.RCE_fecha_terminoEventos).format('DD-MM-YYYY') : ''
                ]);
            });
            LlenaGrillaPacienteEventos(adataset, '#tblEventos');
            $("#mdlEventoAsignar").modal('show');
        }
    });
}

function LlenaGrillaPacienteEventos(datos, grilla) {

    $('#tblEventos').DataTable({
        data: datos,
        lengthMenu: [[5, 10, 50], [5, 10, 50]],
        columns: [
            { title: "RCE_idEventos" },
            { title: "Tipo Evento" },
            { title: "Anamnesis Evento" },
            { title: "Diagnóstico Evento" },
            { title: "Fecha Alta Paciente" },
            { title: "Fecha Inicio Evento" },
            { title: "Fecha Término Evento" },
            { title: "" }
        ],
        "columnDefs": [
            {
                "targets": -1,
                "data": null,
                "orderable": false,
                "render": function (data, type, row, meta) {

                    var fila = meta.row;
                    var botones =
                        "<a id='linkAsignarEvento' data-id='" + datos[fila][0] + "' " +
                            "data-fecha='" + datos[fila][6] + "' class='btn btn-info' " +
                            "href='#/' onclick='LinkAsignarEvento(this)'>" +
                                "<i class='fa fa-check'></i> Asignar Evento" +
                        "</a>";

                    return botones;
                }
            },
            {
                "targets": [0],
                "visible": false
            }
        ],
        "bDestroy": true
    });
}

function LinkAsignarEvento(a) {
    var fechaE = $(a).data("fecha");

    GetEvento().RCE_idEventos = parseInt($(a).data("id"));
    $("#mdlEventoAsignar").modal('hide');
    toastr.success('Evento seleccionado Exitosamente.');

    //nuevoIPD
    var dataTable = $('#tblEventos')
        .DataTable()
        .row(function (idx, data, node) {
            return data[0] == $(a).data("id");
        }).data();
    if ($('#txtDiagnostico')) 
        $('#txtDiagnostico').val(dataTable[3]);
    if ($('#lbltxtDiagnostico')) 
        $('#lbltxtDiagnostico').addClass('active');

    if ($('#fechaEvento').length) {
        $('#fechaEvento').val(fechaE);
    }
}

function LinkNuevoEvento() {
    $("#mdlEventoAsignar").modal('hide');
    $("#mdlEventoNuevo").modal('show');

    iniciarCamposEvento();
}
