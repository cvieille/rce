﻿let selectedDiagnostics = []
let limiteDiagnosticos = 0


function getDiagnosticosOrdenadosUc(data) {
    const diagnosticos = data.map((json) => {
        json.AliasCompleto = `${json.Alias !== null ? `${json.Alias.Valor}` : ""} - ${json.Diagnostico !== null ? json.Diagnostico.Valor:''}`;
        return json;
    });

    diagnosticos.sort(function (a, b) {
        return a.AliasCompleto.length - b.AliasCompleto.length;
    });

    return diagnosticos;

}


function CargarDiagnosticosCIE10Uc(cantidadDiagnosticos = 0) {
    limiteDiagnosticos = cantidadDiagnosticos
    if (!$._data($("#txtDiagnosticoCIE10Uc")[0]).events) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/Alias`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                const json = getDiagnosticosOrdenadosUc(data);
                console.log(json)
                $("#txtDiagnosticoCIE10Uc").typeahead(setTypeAheadUc(json));
            }
        });
    }
}
//Obtener lista de diagnosticos seleccionados
function obtenerDiagnosticosSeleccionados() {
    return selectedDiagnostics
}
//convertir el #txtDiagnosticoCIE10 en typeahead
function setTypeAheadUc(data, cantidadFilas = 0) {
    var typeAhObject = {
        input: '#txtDiagnosticoCIE10Uc',
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el diagnóstico especificado ({{query}})",
        display: ["AliasCompleto"],
        template:
            '<span>' +
            '<span>{{AliasCompleto}}</span> ' +
            '</span>',
        correlativeTemplate: true,
        source: {
              data 
        },
        callback: {
            onClick: function (node, a, item, event) {
                clickAgregarDiagnosticoUc(item)
            }
        }
    };

    $("#txtDiagnosticoCIE10Uc").on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    });

    return typeAhObject;
}
//Agrega una fila a la tabla y a la lista de diagnosticos
function addRowUc(json, cantidadDiagnosticos = 0) {

    var idTr = "trDiagnosticos_" + $("#tblDiagnosticos tbody").length;
    var colspan = 1;

    var tr =
        `
        <tr id='${idTr}' data-idDiagnostico = '${json.Diagnostico.Id}' class='text-center'>
            <td class='pt-1 pb-1'colspan= ${colspan}>
                <span style='font-size:15px !important;'>${(json.DiagnosticoPadre != null) ? `${json.DiagnosticoPadre.Valor}<br />` : ""}</span>
                <span style='font-size:${(json.DiagnosticoPadre != null) ? "12px" : "15px"} !important;'>${json.Diagnostico.Valor}</span><br />
                <span style='font-size:13px !important;font-weight:bold;'>${json.Alias ?? ""}</span>
            </td>
            <td class='text-center pt-1 pb-1'>
                <a class='btn btn-circle-pretty-small btn-danger' data-idDiagnostico = '${json.Diagnostico.Id}' style='color: #ffffff;'>
                    <i class='fa fa-times'></i>
                </a>
            </td>
        </tr>`;
    if ($("#tblDiagnosticos tbody tr").length < cantidadDiagnosticos && cantidadDiagnosticos !== 0)
        $("#tblDiagnosticos tbody").append(tr);
    else if (cantidadDiagnosticos === 0)
        $("#tblDiagnosticos tbody").append(tr);

    $("#tblDiagnosticos").prop("style", "display: default;");
    $("#txtDiagnosticoCIE10").removeClass("invalid-input");

    selectedDiagnostics.push({ Id: json.Diagnostico.Id, IdClasificacion: 2 });

    $("#" + idTr + " td a").click(function () {

        $(this).parent().parent().remove();
        for (var i = 0; i < selectedDiagnostics.length; i++) {
            if (selectedDiagnostics[i] == $(this).attr("data-idDiagnostico")) {
                selectedDiagnostics.splice(i, 1);
                break;
            }
        }

        if ($.trim($("#tblDiagnosticos tbody").html()) === "")
            $("#tblDiagnosticos").hide();

        $(this).unbind();

    });
}
//Limpia la tabla y reset del arreglo
function limpiarDiagnosticos() {
    $('#tblDiagnosticos').hide();
    $('#tblDiagnosticos tbody').empty();
    $('#txaIndicacionesAlta').val("");
    selectedDiagnostics = [];
}
function validarDiagnosticos() {
    if (selectedDiagnostics.length === 0) {
        $("#txtDiagnosticoCIE10").addClass("invalid-input");
        return false;
    } else {
        $("#txtDiagnosticoCIE10").removeClass("invalid-input");
        return true;
    }
}

function diagnosticExistsUc(id) {
    
    
    var exist = false;
    $("#tblDiagnosticos tbody tr").each(function (index) {
        if ($(this).attr("data-idDiagnostico") == id) {
            toastr.info("Este diagnostico ya ha sido agregado")
            return true;
        }
    });
    //Comprobar la cantidad limite de diagnosticos a agregar
    if (limiteDiagnosticos !== 0) {
        if (limiteDiagnosticos == selectedDiagnostics.length) {
            toastr.warning(`No es posible agregar mas de ${limiteDiagnosticos} diagnosticos`)
            return true
        }
    }

    return exist;
}
function clickAgregarDiagnosticoUc(item) {
    if (!diagnosticExistsUc(item.Diagnostico.Id))
        addRowUc(item);
    var time = setTimeout(function () {
        $("#txtDiagnosticoCIE10Uc").val("");
        clearTimeout(time);
    }, 10);
}

function agregarPorCodigoDiagnosticoCIE10Uc() {
    
    const codigo = $.trim($("#txtDiagnosticoCIEPorCodigoUc").val());
    if (codigo != "") {
        ShowModalCargando(true);
        $.ajax({
            type: "GET",
            url: `${GetWebApiUrl()}GEN_Diagnostico_CIE10/PorCodigo?codigo=${codigo}`,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                $("#txtDiagnosticoCIEPorCodigoUc").removeClass("invalid-input");
                $("#txtDiagnosticoCIEPorCodigoUc").val("");
                clickAgregarDiagnosticoUc(data);
                ShowModalCargando(false);
            },
            error: function (jqXHR, status) {
                toastr.error("No se encontro diagnótico CIE10 con ese código.")
                console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
                if (jqXHR.status === 404)
                    $("#txtDiagnosticoCIEPorCodigoUc").removeClass("invalid-input").addClass("invalid-input");
                ShowModalCargando(false);
            }
        });
    } else {
        toastr.error("Debe ingresar un codigo CI10")
    }
}