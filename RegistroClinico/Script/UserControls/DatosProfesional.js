﻿let profesionalUserControl = null;


async function cargarDatosProfesional(profesional = null) {
    if (profesional == null) {
      await $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
            async: true,
            success: function (data) {
                profesionalUserControl = data[0].Persona;
            }, error(err) {
                console.error("Ha ocurrido un error al buscar el profesional")
            }
        })
    } else {    
        profesionalUserControl = profesional
    }
    try {
        document.getElementById("txtNumeroDocumentoProfesional").value = profesionalUserControl.NumeroDocumento
        document.getElementById("txtNombreProfesional").value = profesionalUserControl.Nombre
        document.getElementById("txtApePatProfesional").value = profesionalUserControl.ApellidoPaterno
        document.getElementById("txtApeMatProfesional").value = profesionalUserControl.ApellidoMaterno
    } catch (err) {
        console.error("Ha ocurrido un error al intentar cargar profesional, compruebe las llaves del objeto pasado como parametro UC=DatosProfesional")
        console.log(err)
    }
}