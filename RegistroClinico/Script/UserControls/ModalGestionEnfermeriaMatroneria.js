﻿//Esto es para no perder el scroll del modal
const linkIrUserControlModal = async (button, modal = '#mdlGestionPacienteEnfermeria') => {
    //Tipo moodal establece que modal se va a abrir
    //let modal ='#mdlGestionPacienteEnfermeria'
    if (categorizandoDesdeMapaCamas == true)
        modal = '#mdlInfoPaciente'

    let tipoModal = $(button).data("tipomodal")

    $(modal).modal("hide")
    $(modal).on("hidden.bs.modal", function (e) {
        switch (tipoModal) {
            case "categorizacion":
                linkMostrarModalCategorizacion($(button))
                break
            case "escalasclinicas":
                linkVerEscalasClinicas($(button))
                break
            case "aislamiento":
                linkVerAislamiento($(button))
                break
            case "movimiento":
                linkMostrarModalMovimientosHospitalizacion($(button))
                break
            case "historial":
                linkHistorialHosp($(button))
                break
            case "ingresomedico":
                linkBandejaIngresoMedico($(button))
                break
            case "consentimiento":
                linkMostrarModalConsentimiento($(button))
                break
            case "brazalete":
                linkMostrarModalBrazalete($(button))
                break
            case "alergias":
                linkMostrarModalAlergias($(button), 0, 0)
                break
            case "neonato":
                linkMostrarModalNeoNato($(button))
                break
            case "ingresoenfermeria":
                linkMostrarModalIngresoEnfermeria($(button)) //
                break
            case "pacientetransito":
                linkMostrarModalPacienteEnTransito($(button))
                break
            case "cambiocama":
                linkMostrarModalCambioCama($(button))
                break
            default:
                toastr.error("No se ha pasado un tipo modal a traves del boton")
                break
        }
        $(modal).unbind()
    })
}

const linkMostrarModalGestionPaciente = (sender, mostrarModal=true) => {

    let idEstado = $(sender).data("idestado")
    
    let idCamaActual = $(sender).data("idcama") === '' ? null : $(sender).data("idcama"), idHosp = $(sender).data(`id`), idUbicacion = $(sender).data("idubicacion")
    if (idCamaActual == undefined || idEstado == 156) {
        dibujarBotoneraSegunEstado(idUbicacion, idEstado)
    } else {
        if (idEstado == 88 || categorizandoDesdeMapaCamas) {
            promesaAjax("GET", `HOS_categorizacion/Buscar?idHospitalizacion=${idHosp}&idUbicacion=${idUbicacion}&fecha=${moment(GetFechaActual()).format("YYYY-MM-DD")}`).then(res => {

                let fueCategorizadoHoy = false
                if (res.length > 0) {
                    fueCategorizadoHoy = true
                    document.getElementById("inputIdCategorizacion").value = res[0].Id
                    $(`#btnVerModalCategorizar, #btnVerModalCategorizarRN`).data("idcategorizacion", res[0].Id)
                }
                $(`#btnVerModalCategorizar`).data("categorizadohoy", fueCategorizadoHoy)
                dibujarBotoneraSegunEstado(idUbicacion, idEstado, idCamaActual, fueCategorizadoHoy)
            }).catch(error => {
                console.error("Ocurrio un error al intentar buscar la categorizacion")
                console.log(error)
            })
        }
    }


    //Esto es el nombre del paciente
    $(`#btnVerModalHojaEnfermeria,
       #btnVerModalTraslados,
       #btnVerModalCategorizar,
       #btnVerModalEscalasClinicas,
       #btnVerModalAislamientos,
       #btnVerModalConsentimiento,
       #btnVerModalEgreso,
       #btnVerModalBrazalete,
       #btnVerFichaNeoNato,
       #btnVerModalAlergias,
       #btnVerModalPacienteEnTransito,
       #btnIrAtencionClinicaHos,
       #btnVerModalCategorizarRN,
       #btnAsignarCama`).data("pac", $(sender).data("pac"))
    //id de la hospitalizacion
    $(`#btnVerModalHojaEnfermeria,
       #btnVerModalTraslados,
       #btnVerModalCategorizar,
       #btnVerModalEscalasClinicas,
       #btnVerModalAislamientos,
       #btnVerModalConsentimiento,
       #btnVerModalEgreso,
       #btnVerModalBrazalete,
       #btnVerFichaNeoNato,
       #btnVerModalAlergias,
       #btnVerModalPacienteEnTransito,
       #btnIrAtencionClinicaHos,
       #btnVerModalCategorizarRN,
       #btnAsignarCama`).data("id", $(sender).data("id"))
    //String de la ubicacion del paciente
    $(`#btnVerModalHojaEnfermeria,
       #btnVerModalTraslados,
       #btnVerModalCategorizar,
       #btnVerModalEscalasClinicas,
       #btnVerModalAislamientos,
       #btnVerModalConsentimiento,
       #btnVerModalEgreso,
       #btnVerModalBrazalete,
       #btnVerFichaNeoNato,
       #btnVerModalAlergias,
       #btnVerModalPacienteEnTransito,
       #btnIrAtencionClinicaHos,
       #btnVerModalCategorizarRN,
       #btnAsignarCama`).data("ubicacion", $(sender).data("ubicacion"))
    //Id de la cama actual
    $(`#btnVerModalHojaEnfermeria,
       #btnVerModalTraslados,
       #btnVerModalEscalasClinicas,
       #btnVerModalAislamientos,
       #btnVerModalEgreso,
       #btnVerModalBrazalete,
       #btnVerFichaNeoNato,
       #btnVerModalPacienteEnTransito,
       #btnIrAtencionClinicaHos,
       #btnVerModalCategorizarRN,
       #btnVerModalAlergias`).data("cama", $(sender).data("cama"))
    //id del paciente


    $("#btnVerModalAlergias,  #btnVerFichaNeoNato, #btnAsignarCama, #btnVerModalCategorizarRN").data("idpaciente", $(sender).data("idpaciente"))

    $(`#btnVerModalCategorizar,
       #btnVerModalTraslados, #btnVerModalCategorizarRN`).data("idubicacion", $(sender).data("idubicacion"))
    $(`#btnVerModalTraslados, #btnVerModalCategorizar, #btnVerModalCategorizarRN`).data("idcama", $(sender).data("idcama"))
    $(`#btnVerModalTraslados, #btnVerModalCategorizarRN`).data("idhosp", $(sender).data("id"))
    $(`#btnVerModalTraslados, #btnVerModalCategorizarRN`).data("idpaciente", $(sender).data("idpaciente"))

    //Data modal egreso
    $(`#btnVerModalEgreso,
       #btnVerModalTraslados`).data("ubicacion", $(sender).data("ubicacion"))
    $(`#btnVerModalEgreso`).data("fechahorahosp", `${$(sender).data("fechahosp")} ${$(sender).data("horahosp")}`)
    $(`#btnVerModalEgreso, #btnVerModalCategorizarRN`).data("numerodocumento", $(sender).data("numerodocumento"))
    $(`#btnVerModalEgreso`).data("idpaciente", $(sender).data("idpaciente"))

    //Informacion del modal actual (modal gestion paciente)
    $("#txtModalGestionPacNombrePaciente").val($(sender).data("pac"))
    $("#txtModalGestionPacIdHospitalizacion").val($(sender).data("id"))
    $("#txtModalGestionPacNombreCama").val($(sender).data("cama"))

    //data modal consentimiento informado
    $("#btnVerModalConsentimiento").data("tipoatencion", "hospitalizacion")
    $("#btnVerModalConsentimiento").data("idpaciente", $(sender).data("idpaciente"))

    if (mostrarModal)
        $("#mdlGestionPacienteEnfermeria").modal("show")
}



const dibujarBotoneraSegunEstado = (idUbicacion, idEstado, idCamaActual = null, fueCategorizadoHoy = false) => {
    let botonera = [...$(".ingresado")]
    if (idEstado == 87 || (idEstado == 156 && idCamaActual == null)) {
        botonera.forEach(item => {
            $(item).addClass("d-none")
        })
    } else {
        botonera.forEach(item => {
            if ($(item).hasClass("d-none"))
                $(item).removeClass("d-none")
        })
        $("#btnAsignarCama").data("idtipoestado", idEstado)
        //En obstetricia y cuando el paciente esta categorizado mostrar boton de categorizar RN
        if (idUbicacion == 161 && fueCategorizadoHoy)
            $("#divVerModalCategorizarRN").css("display", "block")
        else
            $("#divVerModalCategorizarRN").css("display", "none")
    }
}
function CargarCama(GEN_idUbicacion, sltCama) {
    let url = `${GetWebApiUrl()}HOS_Cama/Hospitalizacion/GEN_Ubicacion/${GEN_idUbicacion}/Libres`;
    setCargarDataEnCombo(url, false, sltCama);
}



function pacTieneHospitalizacionesIngresadas(hospitalizaciones, idHospitalizacion) {
    return hospitalizaciones.find(h => h.Id !== idHospitalizacion)
}

async function getHospitalizacionesIngresadasOTransitoPaciente(idPaciente) {
    if (idPaciente === undefined)
        return
    const url = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${idPaciente}&idTipoEstado=156`
    try {
        const transitos = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
        })
        return transitos
    } catch (error) {
        console.error("Error al cargar Transitos del paciente")
        console.log(JSON.stringify(error))
    }
}

const ingresarPacienteEnfermeria = (sender) => {

    setSession('ID_HOSPITALIZACION', $(sender).data("id"));
    setSession('ID_PACIENTE', $(sender).data("idpaciente"));

    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/NuevoIngresoEnfermeria.aspx";
}

function alertaPacienteYatieneHospitalizacion(servicio = "", cama = "") {

    Swal.fire({
        icon: 'warning',
        title: 'No puede efectuar el ingreso',
        text: `El paciente tiene episodios de hospitalización abiertos en: ${servicio}, cama: ${cama}`,
    })
}

//function irAtencionClinicaHos(idHosp) {
//    localStorage.setItem("ES_ATENCION_CLINICA_HOSP", true)
//    setSession('ID_HOSPITALIZACION', idHosp)
//    //window.location.href = ObtenerHost() + "/Vista/ModuloHOS/AtencionClinicaHos.aspx";
//}

const irAtencionClinicaHos = (sender) => {
    setSession('ID_HOSPITALIZACION', $(sender).data("id"))
    setSession('ID_PACIENTE', $(sender).data("idpaciente"))

    window.location.href = ObtenerHost() + "/Vista/ModuloHOS/AtencionClinicaHos.aspx"
}