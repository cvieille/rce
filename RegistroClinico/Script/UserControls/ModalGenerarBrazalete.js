﻿$(document).ready(function () {

    $("input[name='TipoBrazalete']").bootstrapSwitch();
    $("#txtNombreBrazalete, #txtPrimerApellidoBrazalete, #txtSegundoApellidoBrazalete, #txtNombreSocialBrazalete").on('input', function () {
        $(this).val($(this).val().toUpperCase());
        generarBrazaleteSegunSeleccion();
    });
    $("#txtFechaNacimientoBrazalete").on('blur', function () {
        const edad = CalcularEdad($(this).val());
        $("#txtEdadBrazalete").val((edad?.años < 18) ? edad?.edad : `${edad?.años} años`);
        generarBrazaleteSegunSeleccion();
    })
    $("input[name='TipoBrazalete']").on('switchChange.bootstrapSwitch', function (event, state) {
        generarBrazaleteSegunSeleccion();
    });

    $("#btnImprimirBrazalete").on('click', async function (evt) {

        ShowModalCargando(true);
        evt.preventDefault();
        if (validarCampos("#divPacienteBrazalete", false)) {

            const idPaciente = $("#btnImprimirBrazalete").data("idPaciente");
            await guadarPacienteBrazalete(idPaciente);

            const sSession = getSession();
            const idAtencionUrgencia = $("#txtNumeroDAUBrazalete").val();
            const tipoAtencion = $("#rdoTipoBrazaletePediatrico").bootstrapSwitch("state") ? "Pediatrico" : "Adulto";
            const tipoBrazalete = getTipoBrazalete();

            if (sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia)
                await imprimirBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete, TipoImpresora.GinecoObtetrico_Adulto);
            else if ($("#rdoTipoBrazaleteAdulto").bootstrapSwitch("state"))
                await imprimirBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete, TipoImpresora.General_Adulto);
            else if ($("#rdoTipoBrazaletePediatrico").bootstrapSwitch("state")) 
                await imprimirBrazalete(idAtencionUrgencia, tipoAtencion, tipoBrazalete, TipoImpresora.General_Pediatrico);

        }
        ShowModalCargando(false);

    });
    limpiarComponentes();
    
});
function getTipoBrazalete() {
    const idIdentificacion = $("#txtIdentificacionBrazalete").data("id-identificacion");
    const nombreSocial = $("#txtNombreSocialBrazalete").val();
    if (idIdentificacion === 5)
        return "NN";
    else if (idIdentificacion === 4)
        return "RN";
    else if (nombreSocial !== "")
        return "NombreSocial";
    return "Normal";
}
function generarBrazaleteSegunSeleccion() {
    if ($("#rdoTipoBrazaletePediatrico").bootstrapSwitch("state"))
        GenerarBrazalete("pediatrico");
    else if ($("#rdoTipoBrazaleteAdulto").bootstrapSwitch("state"))
        GenerarBrazalete("adulto");
}
function limpiarComponentes() {

    $("#divImprimir").hide();
    $("#divVistaPreviaBrazalete").html(
        `<div class="alert alert-info text-center">
            <i class="fa fa-info-circle"></i> Seleccione un tipo de Brazalete
        </div>`);

    $("#rdoTipoBrazaleteAdulto, #rdoTipoBrazaletePediatrico").bootstrapSwitch('disabled', false);
    $("input[name='TipoBrazalete']").bootstrapSwitch('radioAllOff', true);
    $("input[name='TipoBrazalete']").bootstrapSwitch('state', false);
    $("input[name='TipoBrazalete']").bootstrapSwitch('radioAllOff', false);
    $("#txtNumeroDocBrazalete, #txtNombreBrazalete, #txtPrimerApellidoBrazalete, #txtSegundoApellidoBrazalete").val("");
    $("#txtNombreSocialBrazalete, #txtFechaNacimientoBrazalete, #txtEdadBrazalete").val("");
    $("#sltSexoBrazalete").val("0");
    ReiniciarRequired();
}

async function CargarPacienteBrazalete(urgencia) {

    ShowModalCargando(true);
    ConectarImpresoras();
    limpiarComponentes();
    const { NumeroDocumento, Digito, Nombre, ApellidoPaterno, ApellidoMaterno, NombreSocial, Sexo, Identificacion, FechaNacimiento, Edad } = await getPacienteBrazalete(urgencia.IdPaciente);
    const numDoc = (Identificacion.Id === 1 || Identificacion.Id === 4) ? GetRUTConFormato(NumeroDocumento, Digito) : NumeroDocumento

    $("#txtNombreBrazalete, #txtPrimerApellidoBrazalete, #txtFechaNacimientoBrazalete").attr("data-required", (Identificacion.Id !== 5));
    ReiniciarRequired();

    $("#btnImprimirBrazalete").data("idPaciente", urgencia.IdPaciente);
    $("#txtIdentificacionBrazalete").val(Identificacion.Valor);
    $("#txtIdentificacionBrazalete").data("id-identificacion", Identificacion.Id);
    $("#txtNumeroDocBrazalete").val(numDoc);
    $("#txtNombreBrazalete").val(Nombre);
    $("#txtPrimerApellidoBrazalete").val(ApellidoPaterno);
    $("#txtSegundoApellidoBrazalete").val(ApellidoMaterno);
    $("#txtNombreSocialBrazalete").val(NombreSocial);
    $("#txtFechaNacimientoBrazalete").val(moment(FechaNacimiento).format('YYYY-MM-DD'));
    $("#txtEdadBrazalete").val((Edad?.años < 18) ? Edad?.edad : `${Edad?.años} años`);

    if (Edad === null)
        $("#txtEdadBrazalete").val("");

    $("#txtNumeroDAUBrazalete").val(urgencia.Id);
    $("#txtFechaAdmision").val(moment(urgencia.FechaLlegada).format("DD/MM/YYYY HH:mm:ss"));
    $("#txtTipoAtencion").val(urgencia.TipoAtencion.Valor);
    $("#txtTipoAtencion").data("id-tipo-atencion", urgencia.TipoAtencion.Id);
    CargarComboSexoPaciente();

    if (Sexo !== null)
        $("#sltSexoBrazalete").val(Sexo.Id);

    $("#mdlBrazalete").modal("show");

    const sSession = getSession();
    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) {
        $("#rdoTipoBrazaleteAdulto").bootstrapSwitch("state", true, false);
        $("#rdoTipoBrazaleteAdulto, #rdoTipoBrazaletePediatrico").bootstrapSwitch('disabled', true);
    }

    ShowModalCargando(false);
    
}

function CargarComboSexoPaciente() {
    if ($("#sltSexoBrazalete option").length === 0) {
        let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
        setCargarDataEnCombo(url, false, "#sltSexoBrazalete");
    }
}

function GenerarBrazalete(tipoBrazalete) {

    const nombreSocial = $("#txtNombreSocialBrazalete").val();
    const idIdentificacion = parseInt($("#txtIdentificacionBrazalete").data("id-identificacion"));
    let nombreCompleto = $.trim(`${$("#txtNombreBrazalete").val()} ${$("#txtPrimerApellidoBrazalete").val()} ${$("#txtSegundoApellidoBrazalete").val()}`);

    if (idIdentificacion === 5)
        nombreCompleto = (nombreCompleto === "") ? "PACIENTE NN" : `PACIENTE NN: ${nombreCompleto}`;

    $("#divVistaPreviaBrazalete").html(
        `<h4 class="text-center">Vista Previa del Brazalete</h4>
        <div class="d-flex justify-content-center mt-3">
            <div class="d-flex align-items-center" style="position:relative">
                <div class="d-flex align-items-center brazalete-div-text">
                    <div class="flex-fill text-center">
                        ${ nombreSocial !== "" ? `<p class="brazalete-text brazalete-text-${tipoBrazalete} m-0">${nombreSocial}</p>` : "" }
                        <p class="brazalete-text brazalete-text-${ tipoBrazalete} m-0">${ nombreCompleto }</p>
                        <div class="d-flex mt-1 mb-0 ml-0 mr-0">
                            <p class="brazalete-text m-0 flex-fill brazalete-text-${ tipoBrazalete }-subtitulo">${GetInformacionPaciente()}</p>
                        </div>
                        <div class="d-flex mt-1 mb-0 ml-0 mr-0">
                            <p class="brazalete-text m-0 flex-fill brazalete-text-${ tipoBrazalete}-subtitulo">HOSPITAL CLINICO DE MAGALLANES - ${ $("#txtFechaAdmision").val() }</p>
                        </div>
                    </div>
                </div>
                <div class="brazalete brazalete-left">
                    <br />
                </div>
                <div class="brazalete brazalete-right brazalete-right-${ tipoBrazalete }">
                    <br />
                </div>
            </div>
        </div>
        <div class="alert alert-info mt-3 text-center">
            <strong><i class='fa fa-info-circle'></i> Esto es solo una vista previa aproximada.</strong>
        </div>`);

    $("#divImprimir").show();
}

function GetInformacionPaciente() {

    const idIdentificacion = parseInt($("#txtIdentificacionBrazalete").data("id-identificacion"));
    const edad = $("#txtEdadBrazalete").val() !== "" ? $("#txtEdadBrazalete").val() : "DESCONOCIDA";
    const sexo = ($("#sltGeneroBrazalete").val() == 0) ? "" : `SEXO: ${$(`#sltGeneroBrazalete option[value='${$("#sltGeneroBrazalete").val()}']`).text()}`;

    if (idIdentificacion === 5) 
        return `EDAD APROX: ${edad}${GetNumeroDocFormato()}`;
    
    if (idIdentificacion === 4) 
        return `EDAD: ${edad}${GetNumeroDocFormato()}   SEXO: ${sexo}`;
    
    return `EDAD: ${edad}${GetNumeroDocFormato()}`;
    
}

function GetNumeroDocFormato() {

    const idDAU = $("#txtNumeroDAUBrazalete").val();
    const idIdentificacion = parseInt($("#txtIdentificacionBrazalete").data("id-identificacion"));
    let numDoc = $("#txtNumeroDocBrazalete").val() !== "" ? $("#txtNumeroDocBrazalete").val() : "";

    if (idIdentificacion === 1)
        numDoc = `   RUT: ${numDoc}`;
    else if (idIdentificacion === 2 || idIdentificacion === 3)
        numDoc = `   N° DOC: ${numDoc}`;
    else if (idIdentificacion === 4)
        numDoc = `   RUT MAT: ${numDoc}`;
    else
        return `   N° DAU: ${idDAU}`;

    return `${numDoc}   N° DAU: ${idDAU}`;

}

async function getPacienteBrazalete(idPaciente) {
    const paciente = await promesaAjax("GET", `GEN_Paciente/${idPaciente}`);
    return paciente;
}

async function guadarPacienteBrazalete(idPaciente) {

    const paciente = await getPacienteBrazalete(idPaciente);
    const pacienteJson = {
        "Id": idPaciente, 
        "IdIdentificacion": valCampo(paciente.Identificacion?.Id),
        "NumeroDocumento": paciente.NumeroDocumento,
        "Digito": paciente.Digito,
        "Nombre": valCampo($("#txtNombreBrazalete").val()),
        "ApellidoPaterno": valCampo($("#txtPrimerApellidoBrazalete").val()),
        "ApellidoMaterno": valCampo($("#txtSegundoApellidoBrazalete").val()),
        "DireccionCalle": paciente.DireccionCalle,
        "DireccionNumero": paciente.DireccionNumero,
        "DireccionRural": paciente.DireccionRural,
        "IdPais": valCampo(paciente.Pais?.Id),
        "IdRegion": valCampo(paciente.Region?.Id),
        "IdProvincia": valCampo(paciente.Provincia?.Id),
        "IdComuna": valCampo(paciente.Comuna?.Id),
        "Telefono": paciente.Telefono,
        "IdSexo": valCampo(paciente.Sexo?.Id),
        "FechaNacimiento": $("#txtFechaNacimientoBrazalete").val(),
        "OtrosTelefonos": paciente.OtrosFonos,
        "Email": paciente.Email,
        "IdPrevision": valCampo(paciente.Prevision?.Id),
        "IdPrevisionTramo": valCampo(paciente.PrevisionTramo?.Id),
        "Nui": paciente.Nui,
        "IdEstadoConyugal": valCampo(paciente.EstadoConyugal?.Id),
        "IdPuebloOriginario": valCampo(paciente.PuebloOriginario?.Id),
        "IdNacionalidad": valCampo(paciente.Nacionalidad?.Id),
        "IdTipoEscolaridad": valCampo(paciente.NivelEducacional?.Id),
        "FechaFallecimiento": paciente.FechaFallecimiento,
        "IdPersonas": paciente.IdPersona,
        "NombreSocial": valCampo($("#txtNombreSocialBrazalete").val()),
        "Prais": paciente.Prais,
        "IdTiposDomicilio": valCampo(paciente.TipoDomicilio?.Id),
        "IdTipoOcupaciones": valCampo(paciente.Ocupaciones?.Id),
        "IdTipoGenero": valCampo(paciente.Genero?.Id),
        "IdTipoCategoriaOcupacion": valCampo(paciente.TipoCategoriaOcupacion?.Id)
    }
    console.log(JSON.stringify(pacienteJson));
    await promesaAjax("PUT", `GEN_Paciente/${idPaciente}`, pacienteJson);

}