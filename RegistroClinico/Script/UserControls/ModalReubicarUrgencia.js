﻿let evitarToastr = false;

function toggleCardReubicarSacarPac(e) {

    let cadena = $("#cardTrasladarPaciente").attr("data-visible")
    let isVisibleCard = JSON.parse(cadena) // to boolean
    let isVisible = !isVisibleCard

    if (isVisible) {
        $("#cardTrasladarPaciente").attr("data-visible", isVisible)
        $("#cardTrasladarPaciente").show(150)
        $("#cardSacarPaciente").hide(150)
        $("#btnCambiarCardReubicarSacarPac").text("Sacar Paciente")
    }
    else {
        $("#cardTrasladarPaciente").attr("data-visible", isVisible)
        $("#cardSacarPaciente").show(150)
        $("#cardTrasladarPaciente").hide(150)
        $("#btnCambiarCardReubicarSacarPac").html(`<span><i class="fa fa-angle-left"></i></h4></span>`)
    }

}


// Función para reubicar al paciente
async function reubicarPacienteEnBox(id, IdCamaTipoBox) {

    const data = GetJsonIngresoUrgencia(id);
    const { IdPaciente, MotivoConsulta } = data;
    const paciente = await getPacientePorId(IdPaciente)
    const { NombreSocial, Nombre, ApellidoPaterno, ApellidoMaterno, NumeroDocumento, Digito } = paciente

    categorizacionUrgencia = await getCategorizacion(id)

    let nombrePaciente = (NombreSocial != null) ? `(${NombreSocial}) ` : "";
    nombrePaciente = $.trim(`${Nombre ?? ``} ${ApellidoPaterno ?? ``} ${ApellidoMaterno ?? ""}`);
    nombrePaciente = (nombrePaciente != "") ? nombrePaciente : "Sin Información";

    const numeroDoc = (NumeroDocumento ?? "") + ((Digito != null) ? `-${Digito}` : "");

    $("#cardTrasladarPaciente").attr("data-visible", true)
    $("#cardTrasladarPaciente").show()

    const idTipoAtencion = $("#selectTipoAtencion option:selected").text()


    //DATOS PACIENTE

    let objAtencionUrgencia = {
        idAtencion: id,
        idPaciente: IdPaciente,
        motivoConsulta: MotivoConsulta,
        categorizacion: categorizacionUrgencia,
        tipoAtencion: idTipoAtencion
    }

    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteReubicacion")

    // FIN 


    $("#btnReubicarPaciente").data("idAtencionUrgencia", id);
    $("#btnReubicarPaciente").data("idCamaTipoBox", IdCamaTipoBox);

    if ($("#sltTipoAtencionReubicarPaciente option").length === 0)
        setCargarDataEnCombo(`${GetWebApiUrl()}URG_Tipo_Atencion/Combo`, false, "#sltTipoAtencionReubicarPaciente");

    $("#sltTipoAtencionReubicarPaciente").val("0");
    $("#sltTipoBoxReubicarPaciente, #sltBoxReubicarPaciente").empty().attr("disabled", "disabled");

    $("#sltTipoAtencionReubicarPaciente").unbind().on("change", function () {
        $("#sltTipoBoxReubicarPaciente, #sltBoxReubicarPaciente").empty().attr("disabled", "disabled");
        if ($("#sltTipoAtencionReubicarPaciente").val() !== "0") {
            $("#sltTipoBoxReubicarPaciente").removeAttr("disabled");
            setCargarDataEnCombo(`${GetWebApiUrl()}URG_Tipo_Box/Combo?idTipoAtencion=${$(this).val()}`,
                false,
                "#sltTipoBoxReubicarPaciente");
        }
    });

    $("#sltTipoBoxReubicarPaciente").unbind().on("change", function () {
        $("#sltBoxReubicarPaciente").empty().attr("disabled", "disabled");
        if ($("#sltTipoBoxReubicarPaciente").val() !== "0") {
            $("#sltBoxReubicarPaciente").removeAttr("disabled");
            setCargarDataEnCombo(`${GetWebApiUrl()}URG_Cama/Combo?idTipoAtencion=${$("#sltTipoAtencionReubicarPaciente").val()}&idTipoBox=${$(this).val()}`,
                false,
                "#sltBoxReubicarPaciente");
        }
    });

    const idAtencion = $("#selectTipoAtencion").val()
    await sleep(200)
    $("#sltTipoAtencionReubicarPaciente").val(idAtencion).trigger("change")

    $("#mdlReubicarSacarPaciente").modal("show");
    $("#cardSacarPaciente").hide()
    $("#txtnumeroDocReubicarPaciente").html(numeroDoc);
    $("#txtnombreReubicarPaciente").html(nombrePaciente);
    $("#txtMotivoReubicarPaciente").html(MotivoConsulta ?? "");
    $("#btnReubicarPaciente").unbind().on('click', function (evt) {
        evt.preventDefault();
        const idCamaTipoBox = valCampo(parseInt($("#sltBoxReubicarPaciente").val()));
        const idAtencionUrgencia = parseInt($("#btnReubicarPaciente").data("idAtencionUrgencia"));
        if (validarCampos("#divReubicarPaciente", false))
            guardarReubicarPaciente(idAtencionUrgencia, idCamaTipoBox);

    });
    $("#divReubicarPaciente span.invalid-input").remove();
    $("#divReubicarPaciente select").removeClass("is-invalid");
    ReiniciarRequired();
}
function sacarPacienteDeBox() {

    ShowModalCargando(true);

    const { idAtencionUrgencia, idCamaTipoBox } = $("#btnReubicarPaciente").data()
    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/box/${idCamaTipoBox}/Reversar`
    const title = 'El paciente ha sido retirado del box'
    const msgError = 'El Box ya está ocupado'

    patchReubicarSacarPacienteBox({ url, title, msgError })

}

function patchReubicarSacarPacienteBox({ url, title }) {
    try {
        return $.ajax({
            method: 'PATCH',
            url: url,
            success: function (data) {
                // En urgencia carga la tabla urgencia
                cargarCamasUrgencia(parseInt($("#selectTipoAtencion").val()), false);
                $('#mdlReubicarSacarPaciente').modal('hide');
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: title,
                    showConfirmButton: false,
                    timer: 3000
                });
                ShowModalCargando(false);
            },
            error: function (jqXHR, status) {
                ShowModalCargando(false);
                if (jqXHR.status === 404) {
                    Swal.fire({ title: 'Error en el Traslado', text: jqXHR.statusText, icon: 'warning' });
                    cargarCamasUrgencia(parseInt($("#selectTipoAtencion").val()), false);
                } else {
                    console.error("Error en la solicitud: ", jqXHR);
                }
            }
        });
    } catch (ex) {
        ShowModalCargando(false);
        console.error("Error al realizar la operación: ", ex);
        Swal.fire({ title: 'Error', text: 'Ocurrió un error inesperado.', icon: 'error' });
    }
}


function guardarReubicarPaciente(idAtencionUrgencia, idCamaTipoBox) {
    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Traslado/${idCamaTipoBox}`;
    const title = 'El paciente ha sido Reubicado.';
    
    evitarToastr = true; // Evitar toastr

    patchReubicarSacarPacienteBox({ url, title })
        .then(() => {
            // Aquí podria redirigir a vista mapa camas (?)
            evitarToastr = false;
        })
        .catch(error => {
            console.error("Error durante la reubicación: ", error);
            evitarToastr = false;
        });
}

