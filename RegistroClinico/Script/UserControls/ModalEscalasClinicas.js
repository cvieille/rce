﻿//Funcion para mostrar el modal
let idDiv = ""
async function linkVerEscalasClinicas(button) {

    let id = $(button).data("id")
    let nombre = $(button).data("pac")
    let cama = $(button).data("cama")
    await ShowModalEscalasClinicas(id, nombre, cama)

    if (button.dataset !== undefined) {
        const dataIdEscala = button.dataset.idEscalaClinica
        await ocultarMostrarEscalasClinicas(dataIdEscala)
    }
}

async function ocultarMostrarEscalasClinicas(id) {

    let objEscala = {

        "HERIDAS": async () => {

            $("#ulPillsEscalas a").hide()
            $("#ulPillsEscalas a:gt(-4)").show() // muestra las ultimas 3 escalas
            await sleep(200)
            $("#Heridas-tab").trigger("click")
        },
        "RIESGOS": async () => {

            $(`#ulPillsEscalas a[id="Glasgow-tab"]`).hide() // oculta escala glasgow
            $("#ulPillsEscalas a:gt(-4)").hide() // oculta las ultimas 3 escalas
        },
        "VALORACIONES": async () => {

            $("#ulPillsEscalas a:not(#Glasgow-tab)").hide() // oculta todas las escalas excepto glasgow
            await sleep(200)
            $("#Glasgow-tab").trigger("click")
        }
    }

    objEscala[id]()
}

//Carga todas las escalas para dibujar en el modal
async function cargarEscalasClinicas() {
    let datos = [];
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Tipo_Escala_Clinica/Combo`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            datos = data.map(a => a)
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
    return datos
}
//LLena modal, la botonera y el contenido
async function ShowModalEscalasClinicas(idHospitalizacion, nombrePaciente, numeroCama) {
    let pillsBotones = ``, pillsContenido = ``, nombreEscala
    let escalasClinicas = await cargarEscalasClinicas()
    //Creacion de botonera
    $("#ulPillsEscalas").empty()
    $("#v-pills-tabContent").empty()
    escalasClinicas.map((a, index) => {
        let nombreEscalaSplitted = a.Valor.split(" ")
        nombreEscala = nombreEscalaSplitted[nombreEscalaSplitted.length - 1]
        //Cargar la botonera

        pillsBotones += `
                        <li class="nav-item">
                           <a class="nav-link ${index == 0 ? "active" : ""}"
                            id="${nombreEscala}-tab" data-toggle="tab"
                            href="#${nombreEscala}-pills-1" role="tab"
                            aria-controls="v-pills-1"
                            aria-selected="${index == 0 ? "true" : "false"}">
                            ${a.Valor}
                            </a>
                        </li>
                        `
        //Cargar el contenido
        pillsContenido += `
            <div class="tab-pane fade ${index == 0 ? "show active" : ""} col-lg-12" id="${nombreEscala}-pills-1" role="tabpanel" aria-labelledby="v-${nombreEscala}-1-tab">
                <div class="row" id="container-barthel">
                    <div class="col-lg-12">
                        <div class="table-responsive-xl">
                            <table class="table table-bordered table-hover" style="width: 100%;" id="tbl${nombreEscala}">
                            </table>
                        </div>                        
                    </div>
                </div>
                <br />
            </div>`
        cargarEscala(idHospitalizacion, a.Id, nombrePaciente, numeroCama, a.Valor)
    })
    $("#ulPillsEscalas").append(pillsBotones)
    $("#v-pills-tabContent").append(pillsContenido)

    let elemento = $("#mdlVerEscalas .modal-body");
    $("#mdlVerEscalas").modal("show");
    showModalCargandoComponente(elemento);


    //cargarTablaEscalasClinicas(data, idHospitalizacion, nombrePaciente, numeroCama);
    $("#numHospitalizacionModalEscalas").html(idHospitalizacion);
    $("#nomPacModalEscalas").html(nombrePaciente);
    $("#numCamaModalEscalas").html(numeroCama);
    hideModalCargandoComponente(elemento);
}
//Buscador de escalas clinicas
async function escalasClinicasHospitalizacion(idHosp, idEscala) {

    let datos = [];
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Escala_Clinica/Bandeja?idTipoEscala=${idEscala}&idHospitalizacion=${idHosp}&idUrgencia=`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            datos = data.map(a => a)
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
    return datos

}
//Llena de data el modal para agregar un nuevo indice
function mostrarModalValoracion(element) {
    //oculta el modal que muestra el historial de escalas

    $("#mdlVerEscalas").modal("hide")
    $("#mdlVerEscalas").on("hidden.bs.modal", async function (e) {
        $("#nomPacModalIndicesEscala").html($(element).data("nombrepaciente"))
        $("#numCamaModalIndicesEscala").html($(element).data("numerocama"))
        $("#numHospitalizacionModalIndicesEscala").html($(element).data("idhosp"))

        $("#modalBodyValoraciones").empty()
        let idEscala = $(element).data("id")
        let accion = $(element).data("method")
        let tipoEscala = $(element).data("escala")


        switch (tipoEscala) {
            case "Barthel":
                await cargarEvaluacion(1, accion)
                break
            case "LPP":
                await cargarEvaluacion(2, accion)
                break
            case "Caida":
                await cargarEvaluacion(3, accion)
                break
            case "Contención":
                await cargarEvaluacion(4, accion)
                break
            case "Glasgow":
                await cargarEvaluacion(5, accion)
                break
            case "Pediátrica":
                await cargarEvaluacion(6, accion)
                break
            case "Heridas":
                await cargarEvaluacion(7, accion)
                break
            case "Diabético":
                await cargarEvaluacion(8, accion)
                break
            case "Venosas":
                await cargarEvaluacion(9, accion)
                break
            default:
                let contenido = `<h1>
                            Ups!,
                            <i class="far fa-frown"></i>
                            no hay nada aquí por ahora..
                            </h1>
                            <button
                            type="button"
                            onclick="cancelarIngresoEdicion()"
                            class="btn btn-outline-success">Volver atrás
                            </button>`
                $("#modalBodyValoraciones").append(contenido)
                //toastr.error("Ha ocurrido un error")
                break
        }
        //editando, deberia cargar las respuestas anteriores
        if (accion == "EDIT") {
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Escala_Clinica/${idEscala}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    let selects = $("#modalBodyValoraciones").find('select')
                    selects.map((index, a) => {
                        $(a).val(data.ItemsEscalaClinicaPadre.ItemEscalaClinica[index].Alternativas[0].IdAlternativaEscalaClinica)
                    })
                    $("#txtObservacionEvaluacion").val(data.Observaciones)
                },
                error: function (err) {
                    console.error(JSON.stringify(err))
                }

            })
            //Setea el id de la escala para saber cual se esta editando
            $(`#btnGuardar${tipoEscala}`).data("id", idEscala)
        }

        //Este modal es para el ingreo o edicion de  una escala
        $("#mdlValoraciones").modal("show")
        $("#mdlVerEscalas").unbind()
    })
    //Fin evento o hide modal para no perder scroll
}
async function cargarEvaluacion(idEvaluacion, accion) {

    let respuestas
    respuestas = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Tipo_Escala_Clinica/${idEvaluacion}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
    dibujarEvaluacion(respuestas, accion)
}

function dibujarEvaluacion(respuestas, accion) {
    let nombreSplitted = respuestas.DescripcionItemEscalaClinica.split(" ")
    let nombre = nombreSplitted[nombreSplitted.length - 1]
    $("#modalBodyValoraciones").empty()
    let content = ""
    content = `
            <div class="row m-1">
                <div class="col-md-12 text-center">
                <h4>${respuestas.DescripcionItemEscalaClinica}</h4>
                </div>
            </div>
            <div class="row p-3">`

    respuestas.ItemEscalaClinica.map((a, index) => {
        let tooltipContenido = ""
        let options = `<option value=0>seleccione</option>`
        a.Alternativas.map(b => {
            options += `<option 
                        data-puntaje="${b.Puntaje}" 
                        id="opt-${b.IdAlternativaEscalaClinica}-${b.Puntaje}"
                        value="${b.IdAlternativaEscalaClinica}">${b.DescripcionAlternativaEscalaClinica}  ${b.Puntaje != null ? `-${b.Puntaje}` : ""}
                        </option>`
        })
        if (a.InformacionAdicional !== null) {
            tooltipContenido =
                `<i class="fa fa-info-circle fa-lg latidos text-info" data-toggle="tooltip"
            data-placement="right"
            data-html="false"
            title="${a.InformacionAdicional} hola"></i>`
        }
        content += `
            <div class="col-sm-1 col-md-4 card p-2">
                <label>${a.DescripcionItemEscalaClinica} ${a.InformacionAdicional == null ? "" : tooltipContenido} </label>
                <select class="form-control"  name="${nombre}-${index}" id="${nombre}-${index}" data-required="true">
                   ${options}
                </select>
            </div>`
    })
    content += `
            <div class="col-md-12 m-1">
                <label for="txtObservacionEvaluacion">Observaciones:</label>
                <textarea class="form-control"
                          id="txtObservacionEvaluacion"
                          maxlength=500
                          style="resize:none;"></textarea>
            </div>
            <div class="col-md-12 mt-1">
                <div class="text-right mt-3">
                    
                    <button type="button" class="btn btn-primary"
                            id="btnGuardar${nombre}" 
                            data-nombreescala="${respuestas.DescripcionItemEscalaClinica}"
                            data-method="${accion}" 
                            data-escala="${nombre}" 
                            data-idescala="${respuestas.IdItemEscalaClinica}"
                            onclick=guardarIndice(this) >
                    <i class="fa fa-save"></i> Guardar ${respuestas.DescripcionItemEscalaClinica}</button>
                    
                    <button type="button" class="btn btn-outline-secondary" onclick="cancelarIngresoEdicion()" ></i> Cancelar</button>
                </div>
             </div>
          </div>`
    $("#modalBodyValoraciones").append(content)

    $('[data-toggle="tooltip"]').tooltip()
    //setTimeout(function () {
    //    $("#modalBodyValoraciones").tooltip()
    //},5000)
}

function guardarIndice(sender) {
    let editando = false, method, url, escalaApi = {}, idhosp, IdItemEscala
    let metodo = $(sender).data("method") ?? false
    let nombreEscala = $(sender).data("nombreescala")
    let idEscala = $(sender).data("id")
    let indice = $(sender).data("escala")

    metodo == "EDIT" ? editando = true : editando = false
    //Validacion de inputs
    if (validarCampos("#modalBodyValoraciones", false)) {
        let respuestas = [], url
        //Obtenemos los inputs del cuestionario
        let selects = $("#modalBodyValoraciones").find('select')
        //Iterar las opciones
        selects.map((index, a) => {
            //Guardando el id de la respuesta
            respuestas.push(parseInt(a.value))
        })
        idhosp = parseInt($("#numHospitalizacionModalIndicesEscala").text())
        url = `${GetWebApiUrl()}GEN_Escala_Clinica`
        if (editando == true)
            url += `/${idEscala}`
        switch (indice) {
            case "Barthel":
                IdItemEscala = 1
                break
            case "LPP":
                IdItemEscala = 2
                break
            case "Caida":
                IdItemEscala = 3
                break
            case "Contención":
                IdItemEscala = 4
                break
            case "Glasgow":
                IdItemEscala = 5
                break
            case "Pediátrica":
                IdItemEscala = 6
                break
            case "Heridas":
                IdItemEscala = 7
                break
            case "Diabético":
                IdItemEscala = 8
                break
            case "Venosas":
                IdItemEscala = 9
                break
        }
        escalaApi = {
            Observacion: $("#txtObservacionEvaluacion").val(),
            IdTipoEscala: IdItemEscala,
            IdHospitalizacion: idhosp,
            IdAtencionUrgencia: null,
            Respuestas: respuestas
        }

        editando == true ? method = 'PUT' : method = 'POST'

        $.ajax({
            type: method,
            data: JSON.stringify(escalaApi),
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                Swal.fire(
                    `${nombreEscala} guardada`,
                    "Información almacenada exitosamente",
                    'success'
                )

                if (window.location.href.includes("Vista/ModuloHOS/NuevaHojaEnfermeria.aspx")) {

                    let json = {}
                    let id = `frameEscalasPdf${IdItemEscala}`
                    const urlEscala = `${GetWebApiUrl()}GEN_Escala_Clinica/${data.Id}/Imprimir`

                    json.idTipoEscala = data.TipoEscala.Id
                    json.resultado = data.TipoClasificacion.Valor
                    json.totalPuntaje = data.TotalPuntaje

                    $(`#accordionPdfItemRiesgo-${json.idTipoEscala}`).remove()
                    showEscalasClinicasPDF(json)
                    ImprimirApiExterno(urlEscala, id)
                }

                $("#mdlValoraciones").modal("hide")
            },
            error: function (err) {
                console.log(JSON.stringify(err))
            }
        })
    }
}
async function cargarEscala(idHospitalizacion, idEscala, nombrePaciente, numeroCama, nombreEscala) {
    let nombreCompleto = nombreEscala.split(" ")
    //para obtener el nombre corto, ejemplo Barthel,Lpp,Caidas ..etc
    let nombre = nombreCompleto[nombreCompleto.length - 1]
    let response = await escalasClinicasHospitalizacion(idHospitalizacion, idEscala);

    let escala = {
        idEscala,
        nombreEscala,
        nombre
    }

    let idTabla = `#tbl${nombre}`, data = []
    if (response) {
        array = Array.from(response);
    } else {
        array = [];
    }

    $.each(array, function (key, escalaClinica) {
        data.push([
            /*0*/escalaClinica.Id,
            /*1*/escalaClinica.FechaHora != null ? moment(escalaClinica.FechaHora).format("DD-MM-YYYY HH:mm") : "x",
            /*2*/escalaClinica.Profesional != null ? `${escalaClinica.Profesional.Nombre} ${escalaClinica.Profesional.ApellidoPaterno}` : "x",
            /*3*/escalaClinica.TotalPuntaje != null ? escalaClinica.TotalPuntaje : "x",
            /*4*/escalaClinica.Clasificacion !== null ? escalaClinica.Clasificacion.Valor : "No encontrado",
            /*5*/nombreEscala,//Nombre completo
            /*6*/nombre,//Nombre resumido para el switch
            /*7*/escalaClinica.InformacionAdicional//Nombre resumido para el switch
        ]);
    });

    cargarTablaEscalasClinicas(data, idTabla, idHospitalizacion, nombrePaciente, numeroCama, escala) // escala es un objeto
}
function cargarTablaEscalasClinicas(data, idTabla, idHospitalizacion, nombrePaciente, numeroCama, escala) {

    $(idTabla).addClass("nowrap").addClass("tblBandeja").addClass("dataTable").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            },
            {
                text: `Nueva ${escala.nombreEscala}`,
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).attr('id', `btnNueva${escala.nombre}`);
                    $(node).attr('onclick', 'mostrarModalValoracion(this);');
                },
                attr: {
                    'data-escala': escala.nombre,
                    'data-idescala': escala.idEscala,
                    'data-idhosp': idHospitalizacion,
                    'data-numerocama': numeroCama,
                    'data-nombrepaciente': nombrePaciente,
                    'data-nombreescala': escala.nombreEscala,
                    'data-method': 'CREATE'
                }
            }
        ],
        "pageLength": 5,
        "order": [[0, "desc"]],
        data: data,
        responsive: {
            details: false,
            type: 'column'
        },
        columns: [{ title: "Id" },
        { title: "Fecha" },
        { title: "Profesional" },
        { title: "Puntaje" },
        { title: "Resultado" },
        { title: "nombreEscala" },
        { title: "nombre" },
        { title: "informacionAdicional" },
        { title: "Acciones", class: "text-center" }],
        columnDefs: [
            { targets: [-3, 5, 7], visible: false },
            {
                targets: [4],
                render: function (data, type, row) {
                    let InformacionAdicional = row[7]
                    return `<span data-toggle="tooltip" data-placement="top" title="${InformacionAdicional ?? ""}"> ${data}</span>`
                }
            },
            {
                targets: -1,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    //La variable data es la fila actual
                    let fila = meta.row;
                    let botones = `
                            <button class='btn btn-primary btn-circle' type="button"
                                onclick='toggle("#div_accionesMdlEscCli${data[6]}${fila}"); return false;'>
                                <i class="fa fa-list" style="font-size:15px;"></i>
                            </button>
                            <div id='div_accionesMdlEscCli${data[6]}${fila}' class='btn-container' style="display: none;">
                                <center>
                                    <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                    <div class="rounded-actions">
                                        <center>
                            `;

                    botones += `
                                <a id='linkEditar' class='btn btn-info btn-circle btn-lg' href='#/'
                                    onclick='mostrarModalValoracion(this)'
                                    data-id='${data[0]}' 
                                    data-idhosp="${idHospitalizacion}" 
                                    data-nombrepaciente="${nombrePaciente}"
                                    data-numerocama="${numeroCama}"
                                    data-method='EDIT'
                                    data-escala='${data[6]}'
                                    data-nombreescala='${data[5]}'
                                    data-toggle="tooltip"
                                    data-placement="left" title="Editar Escala">
                                    <i class='fa fa-edit'></i>
                                </a><br>
                                `;

                    botones += `
                                <a id='linkImprimir' class='btn btn-info btn-circle btn-lg' href='#/' data-id='${data[0]}'
                                    onclick='linkImprimirEscalaClinica(this);' data-toggle="tooltip" data-placement="left" title="Imprimir Escala">
                                    <i class='fa fa-print'></i>
                                </a><br>
                                <a id='linkEliminar' class='btn btn-danger btn-circle btn-lg' href='#/'
                                    onclick='EliminarEscalaClinica(${data[0]});' data-toggle="tooltip" data-placement="left" title="Eliminar Escala">
                                    <i class='fa fa-trash'></i>
                                </a><br>
                                `;

                    botones +=
                        `
                        </center >
                    </div >
                </center >
            </div >
            `;

                    return botones;
                }
            }
        ],
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip()
}

function nombreFuncion() {
    console.log(1)
}

function EliminarEscalaClinica(idEscala) {
    ShowModalAlerta('CONFIRMACION',
        'Quitar Caso',
        '¿Está seguro que quiere eliminar índice clínico?',
        function () {
            $.ajax({
                type: 'DELETE',
                url: `${GetWebApiUrl()}GEN_Escala_Clinica/${parseInt(idEscala)}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    $('#modalAlerta').modal('hide');
                    Swal.fire(
                        'Realizado',
                        `Escala clínica con id: ${idEscala} elminada exitosamente`,
                        'info'
                    )
                },
                error: function (err) {
                    console.log(JSON.stringify(err))
                }
            })
            $("#mdlVerEscalas").modal("hide")
        });
}

function linkImprimirEscalaClinica(sender) {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}GEN_Escala_Clinica/${id}/Imprimir`;
    ImprimirApiExterno(url);
}
function cancelarIngresoEdicion() {

    $("#mdlValoraciones").modal("hide")
    $("#mdlVerEscalas").modal("show")
}
