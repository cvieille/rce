﻿let ArsenalMedicamentosUserControl = []
let medicamentosSeleccionadosUc = []
let tablaMedicamentosUc

$(document).ready(async function () {
    comboViaAdministracion()
    ArsenalMedicamentosUserControl = await getJsonMedicamentosArsenal();
    inicializarTypeAhead("#thMedicamentosUc", ArsenalMedicamentosUserControl)
    $("#btnAgregarMedicamentoUc").on('click', function () {
         seleccionarMedicamento()
    })
    tablaMedicamentosUc = $("#tblMedicamentosUserControl").DataTable({
        destroy: true,
        data: medicamentosSeleccionadosUc,
        columns: [
            {title:"Medicamento", data:"Medicamento", orderable: false },
            { title: "Dosis", data: "Dosis", orderable: false },
            { title: "Frecuencia", data: "Frecuencia", orderable: false },
            { title: "Via", data: "ViaAdministracion", orderable: false },
            { title: "Acciones", data: "Id", orderable: false }
        ],
        fnCreatedRow: function (rowEl, medicamentoSeleccionado) {
            $(rowEl).attr('id', `row-${medicamentoSeleccionado.Id}`);
        },
        columnDefs: [
            {
                targets: -1,
                render: function (id) {
                    return `<button class="btn btn-danger btn-sm btn-circle" onclick="eliminarMedicamentoUc(${id})" type="button"><i class="fa fa-trash"></i></button>`
                }
            }
        ]
    })
})

function seleccionarMedicamento() {
    console.log(validarCampos("#divMedicamentosUc"))
    if (validarCampos("#divMedicamentosUc")) {

        let objetoAgregar = {
            Id: $("#thMedicamentosUc").data("id"),
            Medicamento: $("#thMedicamentosUc").val(),
            Dosis: $("#txtDosisMedicamento").val(),
            Frecuencia: $("#txtFrecuenciaMedicamento").val(),
            IdViaAdministracion: $("#sltViaAdministracion").val(),
            ViaAdministracion: $("#sltViaAdministracion option:selected").text()
        }
        medicamentosSeleccionadosUc.push(objetoAgregar)
        tablaMedicamentosUc.row.add(objetoAgregar).draw()
    }
}
function eliminarMedicamentoUc(id) {
    medicamentosSeleccionadosUc = medicamentosSeleccionadosUc.filter(f => f.Id !== id)
    tablaMedicamentosUc.row("#row-" + id).remove().draw(false);
}
function inicializarTypeAhead(idSelector, arsenalMedicamentos) {
    $(idSelector).typeahead(setMedicamentosTypeAhead(arsenalMedicamentos));
    $(idSelector).on('input', () => $(idSelector).removeData("id"));
    $(idSelector).blur(() => {
        if ($(idSelector).data("id") == undefined)
            $(idSelector).val("");
    });
}

function obtenerMedicamentosSeleccionados() {
    return medicamentosSeleccionadosUc
}

function comboViaAdministracion() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Via_Administracion/Combo`;
    setCargarDataEnCombo(url, true, $("#sltViaAdministracion"));
}

// MEDICAMENTOS
function getJsonMedicamentosArsenal() {

    let arsenalMedicamentos = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Medicamento_Arsenal/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            arsenalMedicamentos = data;
        },
        error: function (e) {
            console.log(e);
        }
    });

    return arsenalMedicamentos;

}
function setMedicamentosTypeAhead(data) {

    var typeAhObject = {
        input: "#thMedicamentos",
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el medicamento especificado ({{query}})",
        template:
            `<span>
                <span class="{{Valor}}">{{Valor}} {{Forma}} {{Presentacion}}</span>
            </span>`,
        correlativeTemplate: true,
        source: {
            GEN_Medicamento: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                //$("#thMedicamentos").attr('data-id', item.Id);
                $("#thMedicamentosUc").data('id', item.Id);
            }
        }
    };

    return typeAhObject;

}