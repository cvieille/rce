﻿
const linkMostrarModalIngresoEnfermeria = async (sender) => {

    let nombrePaciente = $(sender).data("pac")
    let id = $(sender).data("id")
    let ubicacion = $(sender).data("ubicacion")

    $('#numHospitalizacionCamaOrigen').val(id);
    $('#nomPacienteCamaOrigen').val(nombrePaciente);
    $('#ubicacionActualCamaOrigen').val(ubicacion);


    $("#btnOrigen").data("id", id)
    $("#btnOrigen").data("idpaciente", $(sender).data("idpaciente"))

    await linkIrIngresoEnfermeria(sender)

    mostrarModal("mdlCamaOrigen");
}

async function alertaPacienteYatieneHospitalizaciones() {
    const paciente = await buscarPacientePorDocumentoEIdentificacion($("#txtnumerotPac").val(), $("#sltIdentificacion").val());
    if (paciente !== undefined) {
        const { IdPaciente } = paciente[0];
        const hospitalizaciones = await getHospitalizacionesPaciente(IdPaciente);
        if (hospitalizaciones.length > 0) {
            const idHospitalizacion = hospitalizaciones[0].Id; // capturando el Id del objeto   
            let result = await Swal.fire({
                icon: 'warning',
                title: `El paciente tiene hospitalizaciones activas.
                HOSPITALIZACIÓN N°${idHospitalizacion}`,
                confirmButtonText: "Volver a la bandeja",
                confirmButtonColor: "#0069d9",
                allowOutsideClick: false
            });
            if (result.value) {
                ShowModalCargando(true);
                window.location.href = `${ObtenerHost()}/Vista/ModuloHOS/BandejaHOS.aspx`;
                ShowModalCargando(false);
            }
        }
    }
}


const linkIrIngresoEnfermeria = async (sender) => {

    const idpaciente = $(sender).data("idpaciente")
    const idhospitalizacion = $(sender).data("id")
    const hospitalizacionesIngresadas = await getHospitalizacionesIngresadasOTransitoPaciente(idpaciente)

    const hospitalizacionesEncontradas = pacTieneHospitalizacionesIngresadas(hospitalizacionesIngresadas, idhospitalizacion)
    
    if (hospitalizacionesEncontradas !== undefined) {
        const { Cama } = hospitalizacionesEncontradas
        const servicio = (Cama !== undefined) ? Cama.Actual.Ubicacion.Valor : "No disponible"
        const cama = (Cama !== undefined) ? Cama.Actual.Valor : "No disponible"
        alertaPacienteYatieneHospitalizacion(servicio, cama)
        return
    }

    try {

        let idHosp = idhospitalizacion

        $.ajax({
            method: "GET",
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Enfermeria/Ingreso/Buscar?idHospitalizacion=${idHosp}`,
            success: function (data) {

                if (data.length == 0) {
                    ingresarPacienteEnfermeria(sender)
                } else {

                    // Función para formatear la fecha
                    function formatDate(dateString) {
                        const options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' };
                        return new Date(dateString).toLocaleDateString('es-ES', options);
                    }

                    // Recorrer y modificar los registros
                    data.forEach(record => {
                        record.Fecha = formatDate(record.Fecha);
                    });

                    $("#tblIngresosEnfermeria").DataTable({
                        "order": [],
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend: 'excel',
                                className: 'btn btn-info',
                                text: 'Exportar a Excel'
                            },
                            {
                                extend: 'pdf',
                                className: 'btn btn-info',
                                text: 'Exportar en PDF'
                            },
                            {
                                text: 'Nuevo Ingreso',
                                className: 'btn btn-success',
                                action: function () {
                                    ingresarPacienteEnfermeria(sender)
                                }
                            }
                        ],
                        destroy: true,
                        data: data,
                        columns: [
                            { title: "Id", data: "Id" },
                            { title: "Fecha ingreso", data: "Fecha" },
                            { title: "Cama", data: "Cama" },
                            { title: "Ubicacion", data: "Ubicacion" },
                            { title: "Motivo", data: "Motivo" },
                            { title: "", data: "Id" },
                        ], columnDefs: [
                            {
                                orderable: false,
                                targets: -1,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    //Renderizar botones de impresion
                                    return `<a 
                                        class="btn btn-outline-info btn-xl" 
                                        data-id="${data}"
                                        onclick="imprimirIngresoEnfermeria(this)"
                                        >
                                            <i class="fa fa-print">1</i>
                                       </a>`
                                }
                            },
                            {
                                targets: 2,//cama
                                render: function (Cama) {
                                    //Control de nulos
                                    if (Cama == null)
                                        return `Sin cama`
                                    else
                                        return Cama.Valor ?? `Sin cama`
                                }
                            },
                            {
                                targets: 3,//ubicacion
                                render: function (Ubicacion) {
                                    //control de nullos
                                    if (Ubicacion == null)
                                        return `Sin ubicación`
                                    else
                                        return Ubicacion.Valor ?? `Sin ubicación`

                                }
                            }
                        ]
                    })
                    //linkIrUserControlModal(sender, '#mdlGestionPacienteEnfermeria')

                }
            }, error: function (error) {
                console.error(" ha ocurrido un error al intentar obtener los ingresos de enferrmeria")
                console.log(error)
            }
        })
    } catch (error) {
        console.log(error)
    }
}


//let examenesLaboratorioEgreso = []
//let idHospitalizacion = 0
//$(document).ready(async function () {

//    colapsarElementoDOM("#divInfoEntregaDocumentosEgreso", false)
//    colapsarElementoDOM("#divEntregaExamenesEgreso", false)
//    colapsarElementoDOM("#divCuidadosEnfermeriaEgresoEnf", false)


//    $("#sltTipoRetiro").on('change', async function () {
//        if ($('select[name="sltTipoRegistro"] option:selected').text() == 'Familiares') {
//            toastr.info("Por favor, ingrese la información del acompañante")
//            await ShowAcompañante(true)
//        }
//        else {
//            await ShowAcompañante(false)
//        }
//    })

//    await resetLoadAcompaniante("#sltIdentificacionAcompañante", "#txtnumeroDocAcompañante", "#txtDigAcompañante", "divAcompañante")
//})

//const linkMostrarModalEgreso = (sender) => {
//    //ocultarMostrarAcompanante("#divAcompananteEgreso", true)
//    buscarPacientePorId($(sender).data("idpaciente"))
//    getDatosHospitalizacionPorId($(sender).data("id"), false)
//    idHospitalizacion = $(sender).data("id")
//    BloquearPaciente()
//    //if (($('#divAcompañante').css('display') == 'none')==false ) {
//    //    ShowAcompañante(false)
//    //}


//    if ($('#mdlGestionPacienteEnfermeria').is(':visible')) {
//        //Cuando viene desde la bandeja de hospitalizacion, para no perder el scroll de los modales
//        //Hay que controlar el hidden de un modal para despues abrir el otro.
//        $("#mdlGestionPacienteEnfermeria").modal("hide")
//        $("#mdlGestionPacienteEnfermeria").on("hidden.bs.modal", function (e) {
//            inicializarModalAltaEnfemeria(sender)
//            $("#mdlGestionPacienteEnfermeria").unbind()
//        })
//    } else {
//        $("#mdlInfoPaciente").modal("hide")
//        $("#mdlInfoPaciente").on("hidden.bs.modal", function (e) {
//            inicializarModalAltaEnfemeria(sender)
//            $("#mdlInfoPaciente").unbind()
//        })
//    }
//}

//function inicializarModalAltaEnfemeria(sender) {

//    //Inicializar busqueda de examenes
//    arrayCarteraArancel = GetJsonCartera()
//    let element = "#thExamenesLaboratorio", emptytempl = "No existe el examen especificado ({{query}})"
//    let templ = `<span><span class="{{IdCarteraServicio}}">{{ServicioCartera.Valor}}</span></span>`, source = "GEN_cartera";

//    $("#thExamenesLaboratorio").typeahead(setTypeAhead(arrayCarteraArancel, element, emptytempl, templ, source));
//    $("#thExamenesLaboratorio").change(() => $("#thExamenesLaboratorio").removeData("id"));
//    $("#thExamenesLaboratorio").blur(() => {
//        if ($("#thExamenesLaboratorio").data("id") == undefined)
//            $("#thExamenesLaboratorio").val("");
//    });
//    $("#addElementTableExam").on('click', function () {


//        if (validarCampos("#divExamLabEgreso", false)) {
//            $("#thExamenesLaboratorio").data("text-servicio", $("#thExamenesLaboratorio").val())
//            let idcartera = $("#thExamenesLaboratorio").data("id");
//            let examenEncontrado = arrayCarteraArancel.find(x => x.IdCarteraServicio == idcartera).Aranceles
//            let idTipoArancel = examenEncontrado[0].TipoArancel.Id


//            $("#thExamenesLaboratorio").data("id-tipo-cartera", idTipoArancel)
//            $("#thExamenesLaboratorio").data("id-cartera", idcartera)
//            //$("#thExamenesLaboratorio").data("id-tipo-arancel", idTipoArancel)

//            agregarExamenLaboratorioEgreso($("#thExamenesLaboratorio"))
//            $("#thExamenesLaboratorio").val("")
//        }
//    })
//    $("#thExamenesLaboratorio").on('keydown', function (e) {
//        //13===Enter
//        if (e.keyCode == 13) {
//            e.preventDefault()
//            $("#addElementTableExam").click()
//        }
//    });
//    $.ajax({
//        type: 'GET',
//        url: `${GetWebApiUrl()}HOS_Entrega_Indicaciones/Combo`,
//        contentType: 'application/json',
//        dataType: 'json',
//        success: function (data) {
//            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "ENTREGA DOCUMENTOS"), "#divEntregaDocumentosEgreso")
//            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "CUIDADOS DE ENFERMERÍA"), "#divCuidadosEnfermeriaEgreso")
//            dibujarInformacionEgresoEnfermeria(data.filter(x => x.Valor == "ENTREGA EXÁMENES"), "#divExamenEgreso")
//        }, error: function (err) {
//            console.error("Ha ocurrido un error al traer la informacion del egreso")
//            console.log(err)
//        }
//    })

//    setCargarDataEnCombo(`${GetWebApiUrl()}HOS_Tipo_Destino_Alta_Enfermeria/Combo`, true, "#sltTipoDestino")
//    setCargarDataEnCombo(`${GetWebApiUrl()}HOS_Tipo_Retiro_Alta/Combo`, true, "#sltTipoRetiro")

//    ocultarMostrarAcompanante("#divAcompananteEgreso", false)
//    dibujarTablaExamenEgreso()
//    $("#mdlEgresoEnfermeria").modal("show")
//}

//function dibujarInformacionEgresoEnfermeria(info, div) {
//    $(div).empty()
//    let contenido = ``
//    info.map(a => {
//        a.Indicaciones.map(b => {
//            //let columna = 3
//            //b.Valor.length > 35 ? columna = 6 : columna = 3
//            contenido += `
//            <div class="col-md-6">
//                <div class="form-check">
//                  <input class="form-check-input" type="checkbox" onchange="habilitarDeshabilitarInput(this)" value="${b.Id}" id="check-${b.Id}">
//                  <label class="form-check-label" for="check-${b.Id}">
//                    ${b.Valor}
//                  </label>
//                </div>
//                <input class="form-control" id="txtObservacion-${b.Id}" disabled type="text" placeholder="Observación para:${b.Valor}" />
//            </div>`
//        })
//        $(div).append(contenido)
//    })
//}
//function habilitarDeshabilitarInput(check) {
//    if (check.checked) {
//        $(`#txtObservacion-${check.value}`).attr("disabled", false)
//    } else {
//        $(`#txtObservacion-${check.value}`).val("")
//        $(`#txtObservacion-${check.value}`).attr("disabled", true)
//    }
//}
//function agregarExamenLaboratorioEgreso(element) {
//    let idTipoCartera = $(element).data("id-tipo-cartera")
//    let idCartera = $(element).data("id-cartera")
//    let nombreExamen = $(element).val();
//    if (idCartera !== undefined) {
//        let coincidencia = examenesLaboratorioEgreso.find(x => x[1] == idCartera)
//        if (coincidencia == undefined) {
//            examenesLaboratorioEgreso.push([
//                nombreExamen,
//                idCartera,
//                idTipoCartera
//            ])
//            var tableExamenesEgreso = $("#tblExamenesEgreso").DataTable();
//            tableExamenesEgreso.row.add([
//                nombreExamen,
//                idCartera,
//                idTipoCartera
//            ])
//            tableExamenesEgreso.draw();
//        } else {
//            toastr.info("Examen selecionado ya se encuentra en el listado")
//        }
//    } else {
//        toastr.error("No se ha encontrado elemento, porfavor intente de nuevo")
//        console.error("ha ocurrido un errror, no se obtuvo id examen")
//    }
//}
////Esta es la seccion de examenes para el egreso de enfermeria
//function dibujarTablaExamenEgreso() {
//    $("#tblExamenesEgreso").addClass("datatable").addClass("w-100").DataTable({
//        data: examenesLaboratorioEgreso,
//        pageLength: 5,
//        columns: [
//            { title: "Nombre examen" },
//            { title: "id cartera" },
//            { title: "Tipo examen" },
//            { title: "Acciones", className: "text-center" }
//        ],
//        columnDefs: [
//            { targets: [1], visible: false },
//            {
//                targets: -1,
//                render: function (data, type, row, meta) {
//                    return `<button type="button" class="btn btn-danger btn-circle" onclick="eliminarExamenEgreso(${examenesLaboratorioEgreso[meta.row][1]})"><i class="fa fa-trash"></i></button>`
//                }
//            },
//            {
//                targets: -2,
//                render: function (data, type, row, meta) {
//                    if (examenesLaboratorioEgreso[meta.row][2] == 5) {
//                        return `<span>Exámen laboratorio</span>`
//                    } else {
//                        return `<span>Exámen imagenología</span>`
//                    }
//                }
//            }
//        ],
//        destroy: true
//    })
//}
