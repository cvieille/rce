﻿function cargarSignosVitales(button) {
    let idFiltroSignosVitales = $(button).data("id")
    let tipoSignosVitales = $(button).data("tipo")
    if (idFiltroSignosVitales!==undefined) {
        switch (tipoSignosVitales) {
            case "hojaenfermeria":
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}HOS_Hoja_Enfermeria/${idFiltroSignosVitales}/SignosVitales`,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        dibujarSignosVitalesUserControl(data)
                    }, error: function (err) {
                        console.log("Error al buscar los signso vitales desde hoja de enfermeria" + err)
                    }
                })
                break
        }
    }
    
}
function dibujarSignosVitalesUserControl(data) {
    let contenidoHtml = "", contenidoItems=""
    let orden = [...new Set(data.map(i => i.Fecha))].map(b => {
        return {
            Fecha: b,
            Items: data.filter(a => a.Fecha === b)
        }
    })


    $("#historialItemsModal").empty()
    orden.map((a, index) => {
        contenidoItems= ""
        a.Items.map(b => {
            contenidoItems +=  `<div class="col-md-2 col-sm-6">
                                    <label>${b.Descripcion}</label>
                                    <input class="form-control" value=${b.Valor} disabled="disabled"/>
                                </div>`
        })

        contenidoHtml += `
            <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse${index}" aria-expanded="true" aria-controls="collapseOne">
                      Registro de signos vitales con fecha: ${moment(a.Fecha).format("DD-MM-YYYY HH:mm")}
                    </button>
                  </h5>
                </div>

                <div id="collapse${index}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body row">
                    ${contenidoItems}
                  </div>
                </div>
              </div>
        `
    })
    $("#historialItemsModal").append(contenidoHtml)
}