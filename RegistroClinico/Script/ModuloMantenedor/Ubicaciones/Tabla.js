﻿let listadoUbicaciones = []

const columnas = [
    { title: 'ID' },
    { title: 'Ubicacion' },
    { title: 'Ambito' },
    { title: 'Estado' },
    { title: "Acciones", className: "text-center" }
]

async function getTablaMantenedorUbicaciones() { // main

    await actualizarDatosTablaUbicaciones()
}

async function actualizarDatosTablaUbicaciones() {
    //pequeno delay esperar que se cargue el combo
    setTimeout(async function () {
        let ubicaciones = await getUbicaciones($("#sltMantenedorAmbito").val())
        listadoUbicaciones = ubicaciones
        const ubicacionesPreparadas = await preparaDatosTablaUbicaciones(ubicaciones)
        await dibujarTablaMantenedorUbicaciones("#tblMantenedorUbicacion", ubicacionesPreparadas)
    }, 500)
}

async function getUbicaciones(idAmbito = null) {
    console.log(idAmbito)
    if (idAmbito === 0)
        idAmbito = null

    const url = `${GetWebApiUrl()}GEN_Ubicacion/Buscar?idAmbito=${idAmbito}`

    let ubicaciones = await $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success:
            function (data) {

            },
        error: function (err) {
            console.error("Error al cargar ubicaciones")
            console.error(JSON.stringify(err))
        }
    })

    return ubicaciones
}

async function preparaDatosTablaUbicaciones(ubicaciones) {
    if (ubicaciones === undefined || ubicaciones === null)
        return

    let adataset = []

    $.each(ubicaciones, function () {
        adataset.push(
            [
                this.Id,
                this.Ubicacion.Valor,
                this.Ambito.Valor,
                this.Estado,
                "",
                this.Ubicacion.Id,
            ]
        );
    });

    return adataset
}

async function dibujarTablaMantenedorUbicaciones(tabla, adataset) {
    $(tabla).addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: columnas,
        columnDefs: [
            {
                targets: 2,
                sType: "date-ukLong"
            },
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let idAmbitoUbicacion = adataset[fila][0];
                    let idUbicacion = adataset[fila][5];
                    let botones = `
                    <button type='button' class='btn btn-primary btn-circle btnConfig' 
                        data-toggle="tooltip"
                        data-placement="left"
                        ${adataset[fila][3].toLowerCase()=="activo"?"":"disabled"}
                        onclick="configurarUbicacion(${idUbicacion})"
                        title="Configurar ubicación"> <i class="fa fa-cog fa-lg"></i> </button>
                    `
                    //    `
                    //    <button type='button' class='btn btn-primary btn-circle'
                    //        onclick='toggle("#div_accionesIM${fila}"); return false;'>
                    //        <i class="fa fa-list" style="font-size:15px;"></i>
                    //    </button>
                    //        <div id='div_accionesIM${fila}' class='btn-container' style="display: none;">
                    //           <center>
                    //                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                    //                <div class="rounded-actions">
                    //                    <center>
                    //            `;
                    //botones += dibujarBotonPermisosAmbitoUbicacion(idAmbitoUbicacion);

                    return botones;

                }
            },
            {
                targets: 3,
                render: function (data, type, row, meta) {

                    let fila = meta.row;
                    let idAmbitoUbicacion = adataset[fila][0];
                    let nombreUbicacion = adataset[fila][1];
                    let estado = adataset[fila][3];
                    let botones = ""
                    botones += dibujarBotonCambiarEstadoAmbitoUbicacion(idAmbitoUbicacion, nombreUbicacion, estado);

                    `</center>
                            </div>
                        </center>
                       </div>`;

                    return botones;

                }
            }
        ],
        "bDestroy": true
    });

    //$('[data-toggle="tooltip"]').tooltip();
}

function dibujarBotonPermisosAmbitoUbicacion(id) {
    return ` <a id="permisosAmbitoUbicacion"
                data-id="${id}"
                class="btn btn-default btn-circle btn-lg"
                href="#/"
                onclick="showPermisoAmbitoUbicacion(this);">
                    <i class="fa fa-edit"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Cambiar permisos"></i>
            </a>`
}

function dibujarBotonCambiarEstadoAmbitoUbicacion(id, nombreUbicacion, estado) {

    let pintarEstado = {
        "Activo": "btn btn-info btn-sm",
        "Inactivo": "btn btn-danger btn-sm"
    }

    return `
            <div class="text-center">
			 <button
			    type="button"
			    class="${pintarEstado[estado]}"
			    data-id="${id}"
                data-nombre-ubicacion="${nombreUbicacion}"
			    id="estadoAmbitoUbicacion${id}"
                data-estado="${estado}"
			    data-toggle="tooltip"
			    data-placement="bottom"
			    title="Cambiar estado"
			    style="display:inline;"
			    onclick="CambiarEstadoAmbitoUbicacion(this)"
			>
            ${estado}
			<i class="fas fa-exchange-alt fa-sm"></i>
			</button>
			</div>`;
}

//async function CambiarEstadoAmbitoUbicacion(e) {

//    try {

//        let id = parseInt($(e).data("id"))
//        let nombreUbicacion = $(e).data("nombre-ubicacion")
//        let estado = $(e).data("estado")

//        let imprimirEstado = estado === "Activo"
//            ? `¿Seguro quiere desactivar ${nombreUbicacion}?`
//            : `¿Seguro quiere activar ${nombreUbicacion}?`

//        if (id === undefined || id <= 0)
//            throw new Error("Error: id no es válido")

//        await $.ajax({
//            type: 'PATCH',
//            url: `${GetWebApiUrl()}GEN_Ubicacion/${id}/CambiarEstado`,
//            contentType: "application/json",
//            success: async function (data, status, jqXHR) {

//                const result = await Swal.fire({
//                    title: `Cambiando estado`,
//                    text: `${imprimirEstado}`,
//                    icon: 'warning',
//                    showCancelButton: true,
//                    confirmButtonText: 'Si',
//                    cancelButtonText: 'No',
//                })

//                if (result.value) {

//                    await actualizarDatosTablaUbicaciones()
//                    toastr.success('Estado cambiado correctamente');
//                }
//            },
//            error: function (jqXHR, status) {
//                console.log(jqXHR);
//            }
//        });
//    } catch (error) {
//        console.error("Error cambiar estado ámbito ubicación")
//        console.error(error.message)
//    }
//}

async function CambiarEstadoAmbitoUbicacion(e) {

    let id = parseInt($(e).data("id"))
    let nombreUbicacion = $(e).data("nombre-ubicacion")
    let estado = $(e).data("estado")

    let imprimirEstado = estado === "Activo"
        ? `¿Seguro quiere desactivar ${nombreUbicacion}?`
        : `¿Seguro quiere activar ${nombreUbicacion}?`

    if (id === undefined || id <= 0)
        throw new Error("Error: id no es válido")

    const result = await Swal.fire({
        title: `Cambiando estado`,
        text: `${imprimirEstado}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {

        await $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}GEN_Ubicacion/${id}/CambiarEstado`,
            contentType: "application/json",
            success: async function (data, status, jqXHR) {

                await actualizarDatosTablaUbicaciones()
                toastr.success('Estado cambiado correctamente');
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
            }
        })
    }
}

function showModalCambiarEstadoAmbitoUbicacion(e) {

    let id = $(e).data("id")
    let nombreUbicacion = $(e).data("nombre-ubicacion")

    $("#tituloModalCambioEstadoAmbitoUbicacion").text(nombreUbicacion)
    $("#mdlCambioEstadoAmbitoUbicacion").attr("data-id", id)
    $("#guardarEstadoAmbitoUbicacion").data("id", id)
    $(`#mdlCambioEstadoAmbitoUbicacion[data-id="${id}"]`).modal("show")
}

function toggle(id) {

    if (idDiv == "") {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = "";
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}
