﻿
let idDiv;
let cargaInicial = true
let ubicacionSeleccionada = 0, alaSeleccionada 
let ubicacionCompleta, edicionAvanzada = false


let handlerNulos = {
    set: function (obj, prop, value, valor) {
        if (value == "" || value == null) {
            throw new Error(`El campo ${prop} no puede estar vacio`)
            return
        }
        obj[prop] = value;
        return true;
    }
}

$(document).ready(async function () {
    idDiv = "";
    ShowModalCargando(false);
    await getTablaMantenedorUbicaciones()
    inicializarCombos()

    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'Activo';
    $.fn.bootstrapSwitch.defaults.offText = 'Inactivo';
    $("#estado").bootstrapSwitch('state', true);
    $("#estado").attr('disabled', true);
    $("#mdlGestionCama").on('hidden.bs.modal', function () {
        habilitarEdicion(false)
    })
});

function inicializarCombos() {
    cargarComboAmbito()
    cargarComboAlas() 
    cargarComboTipoCama("#sltTipoCamaGestion")
    cargarComboTipoClasificacionCama("#sltTipoClasificacionCamaGestion")
}

function cargarComboAlas() {
    const url = `${GetWebApiUrl()}GEN_Ala/Combo`;
    setCargarDataEnCombo(url, false, "#sltAlas");

}

function cargarComboAmbito() {
    const url = `${GetWebApiUrl()}GEN_Ambito/Combo`;
    setCargarDataEnCombo(url, false, "#sltMantenedorAmbito");
    //Por ahora se carga por defecto hospitalizacion revisar despues
    $("#sltMantenedorAmbito").val(1)
}

function cargarComboTipoCama(selector = "#sltTipoCama") {
    const url = `${GetWebApiUrl()}HOS_cama/TipoCama`;
    setCargarDataEnCombo(url, false, selector);
}

function cargarComboTipoClasificacionCama(selector = "#sltTipoClasificacionCama") {
    const url = `${GetWebApiUrl()}HOS_cama/TipoClasificacionCama`;
    setCargarDataEnCombo(url, false, selector);
}

async function filtrarUbicaciones() {

    const idAmbito = parseInt($("#sltMantenedorAmbito").val())

    const ubicacionesPorAmbito = await getUbicaciones(idAmbito)
    const ubicaciones = await preparaDatosTablaUbicaciones(ubicacionesPorAmbito)
    dibujarTablaMantenedorUbicaciones("#tblMantenedorUbicacion", ubicaciones)

}

function editarUbicacion(idUbicacion) {

}

function configurarUbicacion(idUbicacion) {
    ShowModalCargando(true)
    ubicacionSeleccionada = idUbicacion
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/Mapa/General/${idUbicacion}`,
        success: function (data) {
            
            $("#divTblUbicaciones").addClass("d-none")
            $("#divUbicacion").empty()
            let contenido = ``
            data.map(ubicacion => {
                contenido += `
                  <div class="card">
                    <div class="card-header bg-dark" id="heading-${ubicacion.IdUbicacion}">
                        <div class="row" id="divTituloUbicacion-${ubicacion.IdUbicacion}">
                            <div class="col-md-10 col-sm-11">
                              <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-${ubicacion.IdUbicacion}" aria-expanded="true" aria-controls="collapse-${ubicacion.IdUbicacion}">
                                    <h4 class="text-white" id="tituloUbicacion">${ubicacion.NombreUbicacion}  </h4> 
                                </button>
                                
                              </h2>
                            </div>
                            <div class="col-md-1 col-sm-1">
                            <span class="badge badge-${ubicacion.Estado.toUpperCase() == "ACTIVO" ? "success" : "danger"}">${ubicacion.Estado}</span>
                            </div>
                            <div class="col-md-1 col-sm-1 text-center">
                                <button class="btn btn-dark btnMenu" id="btnHabilitar" onclick="toggle('#div_accionesDivUbi-${ubicacion.IdUbicacion}')" type="button">
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <div id="div_accionesDivUbi-${ubicacion.IdUbicacion}" class="btn-container" style="display:none;">
                                     <center>
                                        <i class="fas fa-caret-down" style="color: #white; font-size: 16px;"></i>
                                        <ul class="list-group  ">
                                           <li class="list-group-item list-group-item-secondary border-dark">
                                               <a  class="btn btn-primary btn-circle btn-lg"  data-toggle="tooltip" onclick="habilitarUbicacion('ubicacion',${ubicacion.IdUbicacion})" data-placement="left" title="Editar nombre ubicación">
                                                <i class="fas fa-edit fa-lg"></i>
                                                </a>
                                           </li>
                                            <li class="list-group-item list-group-item-secondary border-dark">
                                               <a  class="btn btn-success btn-circle btn-lg" onclick='mdlAsociarAla(${JSON.stringify(ubicacion)})'  data-toggle="tooltip" data-placement="left" title="Agregar ala">
                                                <i class="fas fa-plus"></i>
                                                </a>
                                           </li>
                                       
                                        </ul>
                                     </center>
                                </div>
                            </div>
                        </div>
                        <div class="row d-none" id="divEditarUbicacion-${ubicacion.IdUbicacion}">
                            <div class="col-md-6">
                                <input type="text" class="form-control" data-required="true" disabled=true id="inputUbicacion-${ubicacion.IdUbicacion}" value="${ubicacion.NombreUbicacion}" />
                            </div>     
                            <div class="col-md-3">
                                <button class="btn btn-success" id="btnGuardar" type="button" data-toggle="tooltip" data-placement="left"  title="Guardar cambios" onclick="renombrarElemento('ubicacion',${ubicacion.IdUbicacion})"><i class="fa fa-save"></i></button>
                                <button class="btn btn-danger " id="btnCancelar" type="button"  title="Cancelar edición" onclick="habilitarUbicacion('ubicacion',${ubicacion.IdUbicacion})"><i class="fa fa-window-close"></i></button>
                            </div>    
                        </div>
                    </div>

                    <div id="collapse-${ubicacion.IdUbicacion}" class="collapse show" aria-labelledby="heading-${ubicacion.IdUbicacion}" data-parent="#divUbicacion">
                      <div class="card-body" style="min-height:150px;">
                        <div id="accordionAlas" class="accordion">
                            ${dibujarContenidoAlas(ubicacion.Ala, { IdUbicacion: ubicacion.IdUbicacion, NombreUbicacion: ubicacion.NombreUbicacion })}
                        </div>
                      </div>
                    </div>
                  </div>
                `
            })
            $("#divUbicacion").append(contenido)
            $("#divConfigurarUbicacion").removeClass("d-none")
            
            $('[data-toggle="tooltip"]').tooltip()

            ShowModalCargando(false)
            cargaInicial = false;
        }, error: function (error) {
            console.error("Ha ocurrido un error al buscar la ubicacion")
            console.log(error)
            console.log(JSON.stringify(error))
            ShowModalCargando(false)
        }
    })
}

function habilitarUbicacion(tipo,id) {
    
    let input, divEditar, divTitulo
    switch (tipo) {
        case "ubicacion":
            input = `#inputUbicacion-${id}`
            divEditar = `#divEditarUbicacion-${id}`
            divTitulo = `#divTituloUbicacion-${id}`
            break;
        case "ala":
            input = `#inputAla-${id}`
            divEditar = `#divEditarAla-${id}`
            divTitulo = `#divTituloAla-${id}`
            break;
        case "habitacion":
            input = `#inputHabitacion-${id}`
            divEditar = `#divEditarHabitacion-${id}`
            divTitulo = `#divTituloHabitacion-${id}`
            break;
        case"cama":
            break;
    }
    if ($(input).prop('disabled')) {
        $(input).removeAttr('disabled');
        $(divEditar).removeClass("d-none")
        $(divTitulo).addClass("d-none")
    } else {
        $(input).prop('disabled', true)
        $(divEditar).addClass("d-none")
        $(divTitulo).removeClass("d-none")
    }
}

function dibujarContenidoAlas(Ala, Ubicacion) {
    let contenidoAlas = ``, arregloCamas = [], camasOcupadas = []
    if (Ala.length == 0)
        return `<h2 class="text-center"> <i class="fa fa-info-circle text-success"></i> No se encontraron alas asociadas a esta ubicación.  </h2>`

    Ala.map((ala, index) => {

        arregloCamas = ala.Habitacion.map(hab => hab.Cama)
        camasOcupadas = arregloCamas.flat().filter(c => c.IdTipoEstado == 85)

        contenidoAlas += `
            <div class="card card-info">
                <div class="card-header" id="headingOne">
                    <div class="row" id="divTituloAla-${ala.IdAla}">
                        <div class="col-md-10 col-sm-10">
                          <h5 class="mb-0">
                            <button class="btn btn-link btn-block" type="button" data-toggle="collapse" onclick="setAlaSeleccionada('collapseAla-${ala.IdAla}')" data-target="#collapseAla-${ala.IdAla}" aria-expanded="true" aria-controls="collapseAla-${ala.IdAla}">
                              <h5 class="text-white">${ala.NombreAla}</h5>
                            </button>
                          </h5>
                         </div>
                         <div class="col-md-1 col-sm-1">
                            <span class="badge badge-${ala.Estado.toUpperCase() == "ACTIVO" ? "success" : "danger"}">${ala.Estado}</span>
                         </div>
                         <div class="col-md-1 col-sm-1 text-center">
                                <button class="btn btn-info btnMenu mx-auto" id="btnHabilitar" onclick="toggle('#div_accionesDivAla-${ala.IdAla}')" type="button" >
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <div id="div_accionesDivAla-${ala.IdAla}" class="btn-container" style="display:none;">
                                    <center>
                                    <i class="fas fa-caret-down" style="color: #white; font-size: 16px;"></i>
                                    <ul class="list-group  ">
                                            <li class="list-group-item list-group-item-secondary border-dark">
                                            <a  class="btn btn-success btn-circle btn-lg" onclick='crearNuevoElemento("habitacion", ${JSON.stringify(Ubicacion)}, ${JSON.stringify(ala)})'  data-toggle="tooltip" data-placement="left" title="Agregar Habitacion">
                                            <i class="fas fa-plus"></i>
                                            </a>
                                            </li>
                                             <li class="list-group-item list-group-item-secondary border-dark">
                                            <a  class="btn btn-danger btn-circle btn-lg
                                            ${camasOcupadas.length > 0 ? "disabled" : ""}" 
                                            ${camasOcupadas.length > 0 ? "disabled='disabled'" : ""}
                                            ${camasOcupadas.length > 0 ? "" : `onclick='cambiarEstadoAla(${Ubicacion.IdUbicacion},${ala.IdAla}, 0)'`} data-toggle="tooltip" data-placement="left" title="Desactivar ala">
                                            <i class="fas fa-ban"></i>
                                            </a>
                                            </li>
                                       
                                    </ul>
                                     </center>
                                </div>
                         </div>
                    </div>
                    <div class="row d-none" id="divEditarAla-${ala.IdAla}">
                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled id="inputAla-${ala.IdAla}" value="${ala.NombreAla}" />
                            </div>     
                            <div class="col-md-3">
                                <button class="btn btn-success" id="btnGuardar" type="button" data-toggle="tooltip" data-placement="left"  title="Guardar cambios" onclick="renombrarElemento('ala',${ala.IdAla})"><i class="fa fa-save"></i></button>
                                <button class="btn btn-danger " id="btnCancelar" type="button"  title="Cancelar edición" onclick="habilitarUbicacion('ala',${ala.IdAla})"><i class="fa fa-window-close"></i></button>
                            </div>    
                     </div>
                </div>

                <div id="collapseAla-${ala.IdAla}" class="collapse ${alaSeleccionada == `collapseAla-${ala.IdAla}` ? "show" : (index == 0 && cargaInicial) ? "show" : "hide"  } " aria-labelledby="headingOne" data-parent="#accordionAlas">
                  <div class="card-body">
                    <div class="row">
                        ${dibujarContenidoHabitaciones(ala.Habitacion, Ubicacion, ala)}
                    </div>
                  </div>
                </div>
             </div>  
            `
    })
    return contenidoAlas
}


function setAlaSeleccionada(ala) {
    alaSeleccionada = ala
}

function dibujarContenidoHabitaciones(Habitacion, Ubicacion, Ala) {
    let contenidoHabitaciones = ``
    if (Habitacion.length == 0)
        contenidoHabitaciones = `<div class="col-md-12"><h5><i class="fa fa-info-circle"></i>No se encontraron habitaciones</h5> <button type="button" onclick='crearNuevoElemento("habitacion", ${JSON.stringify(Ubicacion)}, ${JSON.stringify(Ala)})' class="btn btn-primary"> <i class="fa fa-plus"></i>Agregar habitación</button></div>`
    else {
        Habitacion.map(habitacion => {
            let contenidoCamas = ``
            contenidoHabitaciones += `
                    <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="card card-dark">
                                    <div class="card-header">
                                        <div class="row" id="divTituloHabitacion-${habitacion.IdHabitacion}">
                                            <div class="col-md-6 col-sm-12 col-xs-12"><h5 class="text-white">${habitacion.NombreHabitacion}</h5></div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <span class="badge badge-${habitacion.Estado.toUpperCase() == "ACTIVO" ? "success" : "danger"}">${habitacion.Estado} <i class="fas fa-exchange-alt fa-sm"></i></span>
                                             </div>
                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <button class="btn btn-dark btnMenu" id="btnHabilitar" onclick="toggle('#div_accionesDivHabitacion-${habitacion.IdHabitacion}')" type="button" >
                                                    <i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i>
                                                </button>
                                                <div id="div_accionesDivHabitacion-${habitacion.IdHabitacion}" class="btn-container"  style="display:none;">
                                                    <center>
                                                    <i class="fas fa-caret-down fa-xl text-info" ></i>
                                                     <ul class="list-group  ">
                                                      <li class="list-group-item list-group-item-secondary border-dark">
                                                        <a  class="btn btn-primary  btn-sm"  data-toggle="tooltip" onclick="habilitarUbicacion('habitacion',${habitacion.IdHabitacion})" data-placement="left" title="Editar nombre habitación">
                                                            <i class="fas fa-edit fa-sm"></i>
                                                        </a>
                                                      </li>
                                                      <li class="list-group-item list-group-item-secondary  border-dark">
                                                        <a  class="btn btn-success  btn-sm" onclick='crearNuevoElemento("cama", ${JSON.stringify(Ubicacion)}, ${JSON.stringify(Ala)},  ${JSON.stringify(habitacion)})' data-toggle="tooltip" data-placement="left" title="Agregar cama">
                                                            <i class="fas fa-plus fa-sm"></i>
                                                        </a>
                                                      </li>
                                                      <li class="list-group-item list-group-item-secondary  border-dark">
                                                        <a  class="btn btn-success  btn-sm" onclick='modalReubicacionHabitacion(${JSON.stringify(Ubicacion)}, ${JSON.stringify(Ala)},  ${JSON.stringify(habitacion)})' data-toggle="tooltip" data-placement="left" title="Mover de ubicacion">
                                                            <i class="fas fa-arrow-right fa-sm"></i>
                                                        </a>
                                                      </li>
                                                    </ul>
                                                     </center>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row d-none" id="divEditarHabitacion-${habitacion.IdHabitacion}">
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" data-required=true disabled id="inputHabitacion-${habitacion.IdHabitacion}" value="${habitacion.NombreHabitacion}" />
                                                    <input type="hidden" id="txtIdHabitacion-${habitacion.IdHabitacion}" value="${habitacion.IdHabitacion}" />
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control" id="sltEstado-${habitacion.IdHabitacion}">
                                                        <option value="Activo" ><i class="fa fa-check text-success"></i> Activo</option>
                                                        <option value="Inactivo" ${habitacion.Estado.toUpperCase() == "ACTIVO" ? "" : "Selected"}> <i class="fa fa-trash text-danger"></i>Inactivo</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn btn-success" id="btnGuardar" type="button" data-toggle="tooltip" data-placement="top"  title="Guardar cambios" onclick="editarHabitacion(${habitacion.IdHabitacion})"><i class="fa fa-save"></i></button>
                                                    <button class="btn btn-danger " id="btnCancelar" type="button"  data-toggle="tooltip" data-placement="top" title="Cancelar edición" onclick="habilitarUbicacion('habitacion',${habitacion.IdHabitacion})"><i class="fa fa-window-close"></i></button>
                                                </div>    
                                         </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            ${dibujarContenidoCama(habitacion.Cama,Ubicacion,Ala, habitacion)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
        })
    }
    return contenidoHabitaciones
}

function dibujarContenidoCama(Cama, Ubicacion, Ala, habitacion) {


    let contenidoCamas = ``
    if (Cama.length == 0)
        contenidoCamas = `<div class="col-md-12"><h5><i class="fa fa-info-circle"></i>No se encontraron camas</h5>
        <button class="btn btn-primary" type="button" onclick='crearNuevoElemento("cama", ${JSON.stringify(Ubicacion)}, ${JSON.stringify(Ala)},  ${JSON.stringify(habitacion)})' > <i class="fa fa-plus"></i>Agregar Cama</button></div>`
    else {
        Cama.map(cama => {
            
            let clase, icono, color, claseBs = ``

            let botonAccion = `
            <button class="btn btn-primary btnConfig btn-xs w-100" id="btnHabilitar"  type="button" data-toggle="tooltip" data-placement="top" title="Gestionar cama" >
               <i class="fa fa-cog  " aria-hidden="true"></i>
            </button> 
            `
                switch (cama.IdTipoEstado) {
                    //Estoy usando los mismos colores del mapa de camas para seguir con el estilo
                    case 85://cama ocupada
                        clase = `cama ocupada`
                        icono = `fa fa-user-circle`
                        
                        break;
                    case 86://Cama bloqueda
                        clase = `cama bloqueada`
                        icono = `fa fa-lock`
                        break
                    case 84://Cama disponivle
                        clase = `cama disponible`
                        icono = `fa fa-check-circle`
                        break
                    case 99://Cama dsshabilitada
                        clase = `cama deshabilitada`
                        icono = `fa fa-ban`
                        break;
                }
            


            contenidoCamas += `
                    <div id="divCama" class="col-xl-4  col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-warning">
                            <div class="card=body">
                                <div class="row p-1">
                                    
                                    <div class="col-md-12 text-center">
                                        <span><b>${cama.Nombre}</b></span>
                                    </div>
                                    
                                    <div class="col-md-12 " >
                                        <div class='${clase} mx-auto'
                                        data-color="${claseBs}"
                                        onclick='gestionarCama(${cama.Id})' 
                                        >
                                            <img src='../../../Style/img/bed.PNG'  width='40' draggable='false' />
                                            <i id='iCama_' class='${icono}' style='z-index:0; color:black;'></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
        })
    }

    return contenidoCamas
}
function volverAtras() {
    ubicacionSeleccionada = 0
    cargaInicial = true
    alaSeleccionada = undefined
    $("#divConfigurarUbicacion").addClass("d-none")
    $("#divTblUbicaciones").removeClass("d-none")
    $("#divUbicacion").empty()
    filtrarUbicaciones()
}

function crearNuevoElemento(elementoCrear, Ubicacion, Ala, Habitacion, Cama) {
    $("#tituloModalCrearElemento").text("")
    $("#divCrearElemento").empty()
    
    let titulo
    let contenido = `
    <input type="hidden" id="txtIdUbicacion" value="${Ubicacion.IdUbicacion}"/>
    <input type="hidden" id="txtIdAla" value="${Ala.IdAla}"/>
    `
    switch (elementoCrear) {
        case 'ala':
            break;
        case 'habitacion':
            titulo = "Creando una habitación."
            contenido += `
            <div class="col-md-12"><h5> <i class="fa fa-info-circle"></i> Usted esta creando una habitacion en <b>${Ala.NombreAla}</b> que está en <b>${Ubicacion.NombreUbicacion}</b> </h5></div>
            <div class="col-md-6 col-xs-12">
                <label>Ingrese el nombre de la habitación:</label>
                <input type="text" class="form-control" id="txtNombreHabitacion" max="15" placeholder="Ej:HAB101" data-required=true maxlength="30" />
                 <input type="hidden" id="txtIdUbicacion" value="${Ala.IdAla}" />
                  <input type="hidden" id="txtIdAla" value="${Ubicacion.IdUbicacion}" />
            </div>
            <div class="col-md-3 col-xs-6">
                <button class="btn btn-primary divButtonLabel" onclick="guardarElemento('habitacion')" type="button"><i class="fa fa-save"></i> Guardar</button>
            </div>
            `

            break;
        case 'cama':
            titulo = "Creando una cama."
            contenido += `
            <div class="col-md-12"><h5> <i class="fa fa-info-circle"></i> Usted esta creando una cama adentro de la habitacion <b>${Habitacion.NombreHabitacion}</b> en <b>${Ala.NombreAla}</b> que está en <b>${Ubicacion.NombreUbicacion}</b> </h5></div>
            <div class="col-md-3 col-xs-12">
                <label>Nombre</label>
                <input type="text" class="form-control" placeholder="Nombre de la cama" id="txtNombreCama" data-required=true maxlength="30" />
                <input type="hidden" id="txtIdHabitacion" value="${Habitacion.IdHabitacion}" />
            </div>
            <div class="col-md-3 col-xs-6">
                <label>Tipo cama</label>
                <select class="form-control" data-required=true id="sltTipoCama"></select>
            </div>
            <div class="col-md-3 col-xs-6">
                <label>Clasificacion cama</label>
                 <select class="form-control" data-required=true id="sltTipoClasificacionCama"></select>
            </div>
            <div class="col-md-3 col-xs-6">
                <button class="btn btn-primary divButtonLabel" onclick="guardarElemento('cama')" type="button"><i class="fa fa-save"></i> Guardar</button>
            </div>
            `
            break;
    }
    
    $("#tituloModalCrearElemento").text(titulo)
    $("#divCrearElemento").append(contenido)
    
    ReiniciarRequired()
    cargarComboTipoCama()
    cargarComboTipoClasificacionCama()
    $("#mdlCrearElemento").modal('show')
}

function guardarElemento(elemento) {
    console.log(elemento)
    if (validarCampos("#divCrearElemento")) {

        let urlPeticion = `${GetWebApiUrl()}`
        

        const objeto = new Proxy({}, handlerNulos);


        switch (elemento) {
            case 'habitacion':
                objeto.Nombre = $("#txtNombreHabitacion").val()
                objeto.IdUbicacion = $("#txtIdUbicacion").val()
                objeto.IdAla = $("#txtIdAla").val()
                urlPeticion += `HOS_Habitacion`
                break
            case 'cama':
                objeto.Nombre = $("#txtNombreCama").val()
                objeto.IdHabitacion = parseInt($("#txtIdHabitacion").val())
                objeto.IdTipoCama = $("#sltTipoCama").val();
                objeto.IdTipoClasificacionCama = $("#sltTipoClasificacionCama").val();
                urlPeticion += `HOS_Cama`
                break;
        }
        $.ajax({
            method: "POST",
            url: urlPeticion,
            data: objeto,
            success: function (data) {
                Swal.fire({
                    title: `La ${elemento} ha sido creada`,
                    icon: 'success',
                    timer: 2000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(result => {
                    if (ubicacionSeleccionada !== 0)
                        configurarUbicacion(ubicacionSeleccionada) 
                })
                
                $("#mdlCrearElemento").modal("hide")
            }, error: function (error) {
                console.error("Ha ocurrido un error")
                console.log(error)
            }
        })
    }

}

async function editarHabitacion(idCama) {
    let textoAdvertencia = `Usted está editando una habitación tenga presente que esto tendrá un impacto en todo el sistema RCE. <br/>
                           Recuerde que si <b>desactiva una habitación dejará de estar disponible</b> para uso clínico y todas las camas asociadas a ésta.                             
    <b>Confirme la acción.</b>`



    const result = await Swal.fire({
        title: `Editando habitación.`,
        html: `${textoAdvertencia}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Editar',
        cancelButtonText: 'Cancelar',
    })
    if (result.value) {
        const objetoEnviar = new Proxy({}, handlerNulos)
        objetoEnviar.Id = $(`#txtIdHabitacion-${idCama}`).val()
        objetoEnviar.Nombre = $(`#inputHabitacion-${idCama}`).val()
        objetoEnviar.Estado = $(`#sltEstado-${idCama}`).val()
        promesaAjax(`PUT`, `HOS_Habitacion`, objetoEnviar).then(res => {
            Swal.fire({
                title: 'La habitación ha sido editada',
                text: 'Cambios realizados con éxito, actualizando información',
                icon: 'success',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then(result => {
                if (ubicacionSeleccionada !== 0)
                    configurarUbicacion(ubicacionSeleccionada)
            })
        }).catch(error => {
            console.log(error)
            console.error("Ha ocurrido un erro al intentar editar la habitacion")
        })
    }
}

async function editarCama() {

    if (validarCampos("#divGestionCama", true) && validarCampos("#divEdicionAvanzada", true)) {

        let textoAdvertencia = `Usted está editando una cama tenga presente que esto tendrá un impacto en todo el sistema RCE. <br/>
                           Recuerde que si <b>desactiva una cama dejará de estar disponible</b> para uso clínico.                             
                            <b>Confirme la acción.</b>`

        const result = await Swal.fire({
            title: `Editando cama.`,
            html: `${textoAdvertencia}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Editar',
            cancelButtonText: 'Cancelar',
        })
        if (result.value) {

            const objetoEnviar = new Proxy({}, handlerNulos);
            let objetoCambioUbicacion = new Proxy({}, handlerNulos);

            objetoEnviar.Id = parseInt($("#txtIdCama").val())
            objetoEnviar.Nombre = $("#txtNombreCama").val()
            objetoEnviar.IdTipoCama = parseInt($("#sltTipoCamaGestion").val())
            objetoEnviar.IdTipoClasificacionCama = parseInt($("#sltTipoClasificacionCamaGestion").val())
            objetoEnviar.IdHabitacion = parseInt($("#txtIdHabitacion").val())
            objetoEnviar.Estado = $("#estado").bootstrapSwitch('state') == true ? "Activo" : "Inactivo"


            promesaAjax("PUT", "HOS_cama", objetoEnviar).then(res => {
                let timerInterval
                Swal.fire({
                    title: 'El cama ha sido editada',
                    text: 'Cambios realizados con éxito, actualizando información',
                    icon: 'success',
                    timer: 2000,
                    timerProgressBar: true,
                }).then(result => {
                    //aca deberia editar la ubicacion de la cama
                    if (edicionAvanzada == true) {
                        objetoCambioUbicacion.IdCama = $("#txtIdCama").val()
                        objetoCambioUbicacion.IdHabitacion = $("#sltHabitacion").val()
                        $.ajax({
                            method: "PUT",
                            url: `${GetWebApiUrl()}HOS_Cama/CambiarUbicacion`,
                            data: objetoCambioUbicacion,
                            successs: function (data) {
                                console.log("Se camibio la cama")
                            }, error: function (error) {
                                console.error("Errro al cambiar la cama de habitacion")
                                console.log(error)
                            }
                        })
                    }

                    if (ubicacionSeleccionada !== 0)
                        configurarUbicacion(ubicacionSeleccionada)
                    $("#mdlGestionCama").modal("hide")
                })

            }).catch(error => {
                console.error("Ocurrio un error al intentar editar la cama" + objetoEnviar.Nombre)
                console.log(error)
            })
        }
    }
    
}
async function renombrarElemento(elemento, id) {

    
        let url = `${GetWebApiUrl()}`, textoAdvertencia = ``, titulo = ``, divValidar =``
        let objetoEnviar = {}
        switch (elemento) {
            case 'ubicacion':
                url += `GEN_Ubicacion`
                objetoEnviar.Id = id
                objetoEnviar.Valor = $(`#inputUbicacion-${id}`).val()
                titulo = `Renombrando ubicación.`
                textoAdvertencia = `Usted esta a punto de renombrar una ubicación, este nuevo nombre aparecerá en todo el sistema. </br> <b>Confirme que desea realizar la acción.</b>`
                divValidar = `#divEditarUbicacion-${id}`
                break
            case 'ala':
                break
            case 'habitacion':
                break
            case 'cama':
                break
    }
    if (validarCampos(divValidar, true)) {
        const result = await Swal.fire({
            title: `Renombrar elemento`,
            html: `${textoAdvertencia}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Renombrar',
            cancelButtonText: 'Cancelar',
        })

        if (result.value) {
            $.ajax({
                method: "PATCH",
                url: url,
                data: objetoEnviar,
                success: function () {
                    let timerInterval
                    Swal.fire({
                        title: 'El nombre ha sido cambiado',
                        text: 'Cambios realizados con éxito, actualizando información',
                        icon: 'success',
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    })
                    if (ubicacionSeleccionada !== 0)
                        configurarUbicacion(ubicacionSeleccionada) 
                }, error: function (error) {
                    console.log(error)
                    console.error("Ha  ocurrido un error al intentar cambiar el nombre de la ubicacion")
                }
            })
        }
    }
    
}
async function gestionarCama(IdCama) {
    promesaAjax("GET", `HOS_Cama/${IdCama}`).then(res => {
        $("#divGestionInfo").empty()
        $("#tituloModalGestionCama").text(`Gestionando cama ${res.Valor}`)
        $("#txtNombreCama").val(res.Valor)
        $("#txtIdCama").val(res.Id)
        $("#txtIdHabitacion").val(res.Habitacion.Id)
        $("#txtIdAla").val(res.Ala.Id)
        $("#txtIdUbicacion").val(res.Ubicacion.Id)

        if (res.TipoCama !== null) 
            $("#sltTipoCamaGestion").val(res.TipoCama.Id)
        else 
            $("#sltTipoCamaGestion").val(0)

        if (res.TipoClasificacion !== null)
            $("#sltTipoClasificacionCamaGestion").val(res.TipoClasificacion.Id)
        else 
            $("#sltTipoClasificacionCamaGestion").val(0)

        let color, icono

        if (res.Activo.toLowerCase() == "activo")
            $("#estado").bootstrapSwitch('state', true);
        else
            $("#estado").bootstrapSwitch('state', false);


        $("#estado").bootstrapSwitch('disabled', true);

        if (res.Estado !== null) {

            let botones = `<button type="button" id="btnEditar" onclick="habilitarEdicion(true)" class="btn btn-primary" ${res.Estado.Id == 85 ? "disabled" : ""}>Habilitar edición <i class="fas fa-edit"></i></button>
              <button type="button" id="btnGuardarEdicion" onclick="editarCama()"  class="btn btn-info d-none" ${res.Estado.Id == 85 ? "disabled" : ""}>Guardar edición <i class="fas fa-save"></i></button>
            <button type="button" id="btnCancelarEdicion" onclick="habilitarEdicion(false)" class="btn btn-danger d-none" ${res.Estado.Id == 85 ? "disabled" : ""}>Cancelar edición <i class="fa fa-times-circle"></i></button>
          `

            switch (res.Estado.Id) {
                //Estoy usando los mismos colores del mapa de camas para seguir con el estilo
                case 85://cama ocupada
                    $("#divGestionInfo").append(`
                    <div class="alert alert-danger" role="alert">
                       <h6><i class="fa fa-info-circle"></i> Información: En camas ocupadas no se puede realizar gestión, primero debe liberarla.</h6>
                    </div>
                    `)
                    color = `warning`
                    icono = `fa fa-user-circle`
                    break;
                case 86://Cama bloqueda
                    color = `secondary`
                    icono = `fa fa-lock`
                    botones += `
                    <button type="button" data-idcama="${res.Id}" data-idestado="84" onclick=" cambiarEstadoCama(this)" class="btn btn-success">Desbloquear <i class="fa fa-unlock"></i></button>
                    <button type="button" data-idcama="${res.Id}" data-idestado="99" onclick=" cambiarEstadoCama(this)" class="btn btn-secondary">Deshabilitar <i class="fa fa-ban"></i></button>
                    `
                    break
                case 84://Cama disponivle
                    color = `success`
                    icono = `fa fa-check-circle`
                    botones += `
                    <button type="button" data-idcama="${res.Id}" data-idestado="86" onclick=" cambiarEstadoCama(this)" class="btn btn-dark">Bloquear <i class="fa fa-lock"></i></button>
                    <button type="button" data-idcama="${res.Id}" data-idestado="99" onclick=" cambiarEstadoCama(this)" class="btn btn-secondary">Deshabilitar <i class="fa fa-ban"></i></button>
                    `
                    break
                case 99://Cama dsshabilitada
                    color = `ligth`
                    icono = `fa fa-ban`
                    botones += `
                    <button type="button" data-idcama="${res.Id}" data-idestado="86" onclick=" cambiarEstadoCama(this)" class="btn btn-dark">Bloquear <i class="fa fa-lock"></i></button>
                    <button type="button" data-idcama="${res.Id}" data-idestado="84" onclick=" cambiarEstadoCama(this)" class="btn btn-success">Habilitar <i class="fa fa-check"></i></button>
                    `
                    break;
            }

            botones += `<button type="button" onclick="buscarMovimientosCama(${res.Id}, '${res.Valor}')" class="btn btn-info">Movimientos <i class="fa fa-eye"></i></button>`

            $("#divGestionEstado").empty()
            $("#divGestionEstado").append(`
            <label>Estado actual:</label></br>
            <h5><span class="badge badge-${color}">${res.Estado.Valor}  <i class="${icono}"></i></span></h5>
            `)

            $("#divGestionBotones").empty()
            $("#divGestionBotones").append(botones)

            //cargar ubicacion ala y habitacion
            cargarSltUbicaciones("#sltUbicacion")
            $("#sltUbicacion").val(res.Ubicacion.Id).trigger('change');
            $("#sltAla").val(res.Ala.Id).trigger('change');
            $("#sltHabitacion").val(res.Habitacion.Id);
        }

        $("#mdlGestionCama").modal("show")
    }).catch(erro => {
        console.error("Ocurri un erro al intentar obtener la cama")
        console.log(erro)
    })
}

function cargarSltUbicaciones(idSelector) {
    //listadoUbicaciones es variable global en el archivo Tabla.js
    $(idSelector).empty()
    listadoUbicaciones.forEach(item => {
        $(idSelector).append(`<option value=${item.Ubicacion.Id}>${item.Ubicacion.Valor}</option>`)
    })
    
}
function cargarAlas(input, idSelector) {
    if (input.value == 0)
        throw new Error("Se paso id = 0 al intentar cargar alas")

    const url = `${GetWebApiUrl()}GEN_Ala/GEN_Ubicacion/${input.value}`;
    setCargarDataEnCombo(url, false, idSelector);
}
function cargarHabitaciones(input) {
    let idUbicacion = $("#sltUbicacion").val()
    if (input.value == 0 || idUbicacion==0)
        throw new Error("Se paso id = 0 al intentar cargar habitaciones")

    const url = `${GetWebApiUrl()}HOS_habitacion/Ala/${input.value}/Ubicacion/${idUbicacion}`;
    setCargarDataEnCombo(url, false, "#sltHabitacion");
}
async function cambiarEstadoCama(button) {
    let idCama= parseInt($(button).data("idcama"))
    let idEstado = parseInt($(button).data("idestado"))
    if (idCama !== null && idEstado !== null) {
        
        let  titulo = ``, texto = `La cama <b>no estará disponible</b> para uso clínico, porfavor confirme la acción`
        switch (idEstado) {
            case 84://Hacer la cama disponible
                titulo = `Habilitando cama`
                texto = `La cama <b>estará disponible</b> para uso clínico , porfavor confirme la acción`
                break
            case 86://Bloquearla
                titulo = `Bloqueando cama`
                break;
            case 99://Deshabilitarla
                titulo = `Deshabilitando cama`
                break;

        }
        const result = await Swal.fire({
            title: titulo,
            html: texto,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
        })

        if (result.value) {
            promesaAjax("PATCH", `HOS_Cama/CambiarEstado/${idCama}/${idEstado}`).then(res => {
                if (ubicacionSeleccionada !== 0)
                    configurarUbicacion(ubicacionSeleccionada) 
                let timerInterval
                Swal.fire({
                    title: 'El estado ha sido cambiado.',
                    text: 'Cambios realizados con éxito, actualizando información',
                    icon: 'success',
                    timer: 2000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(result => {
                    $("#mdlGestionCama").modal("hide")
                    console.log(result)
                })
            }).catch(error=>{
                console.error("Ha ocurrido un erro al intentar cambiar el estado de la cama")
                console.log(error)
            })
        }
    }
}

function buscarMovimientosCama(idCama, nombreCama) {
    promesaAjax("get", `HOS_Cama/${idCama}/Movimientos`).then(res => {
        $("#mdlGestionCama").modal("hide")
        $("#tituloMdlMovimientosCama").empty()
        $("#tituloMdlMovimientosCama").append(`<h4>Movimientos de la cama: ${nombreCama}</h4>`)

        $("#tblMovimientosCama").DataTable({
            data: res,
            columns: [
                {title:"id", data:"Id"},
                { title: "Movimiento", data: "Movimiento" },
                { title: "Fecha", data: "Fecha" },
                { title: "Usuario", data: "Usuario", render: function (usuario) { if (usuario !== null) { return `${usuario.Nombre} ${usuario.ApellidoPaterno}`} } }
            ],
            columnDefs: [
                { targets: -2, render: function (fecha) {return moment(fecha).format("DD-MM-YYYY HH:mm") } }
            ],
            destroy:true
        })
        $("#mdlMovimientosCama").modal("show")

    }).catch(error => {
        toastr.error("Ha ocurrido un error al buscar los movimientos")
        console.error("Ha ocurrido un error al buscar los movimientos")
        console.log(error)
    })
}

async function cambiarEstadoAla(idUbicacion, idAla, estado, button) {
    let result = false
    let object = {
        Estado: "Inactivo"
    }

    if (estado == 0) {
        result = await Swal.fire({
            title: "Desactivando Ala",
            html: "<p>Usted esá a punto de desactivar el ala, todas las habitaciones y camas asociadas a ésta no estarán disponibles para uso clínico.</p>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Desactivar',
            cancelButtonText: 'Cancelar',
        })
        object.IdUbicacion = idUbicacion
        object.IdAla = idAla
    } else {
        object.Estado = "Activo"
        result = { value : validarCampos("#divAsociarAla") }
        if (result.value) {
            object.IdAla = parseInt($("#sltAlas").val()),
            object.IdUbicacion = parseInt($(button).data('idubicacion'))
        }
    }
    if (result.value) {
        promesaAjax("PATCH", `GEN_Ubicacion/CambiarEstado`, object).then(res => {
            console.log(res)
            Swal.fire({
                title: 'El estado ha sido cambiado.',
                text: 'Cambios realizados con éxito, actualizando información',
                icon: 'success',
                timer: 2000,
                timerProgressBar: true
            }).then(result => {
                if (ubicacionSeleccionada !== 0)
                    configurarUbicacion(ubicacionSeleccionada) 
                if (estado !== 0)
                    $("#mdlAsociarAla").modal("hide")
            })
        }).catch(error=> {
            console.error("ocurrio un error al intentar cambiar el estado de la ubicacion ala")
            console.log(error)
        })
    }

}
function habilitarEdicion(habilitar) {
    if (habilitar) {
        $("#txtNombreCama, #sltTipoCamaGestion, #sltTipoClasificacionCamaGestion").removeAttr('disabled')
        $("#estado").bootstrapSwitch('disabled', false);
        $("#btnEditar").addClass("d-none")
        $("#btnGuardarEdicion, #btnCancelarEdicion, #divEdicionAvanzada").removeClass("d-none")
        ReiniciarRequired()
    } else {
        $("#txtNombreCama").attr('disabled',true)
        $("#sltTipoCamaGestion").attr('disabled',true)
        $("#sltTipoClasificacionCamaGestion").attr('disabled', true)
        $("#estado").bootstrapSwitch('state', true);
        $("#estado").bootstrapSwitch('disabled', true);
        $("#btnEditar").removeClass("d-none")
        $("#btnGuardarEdicion , #divEdicionAvanzada").addClass("d-none")
        $("#btnCancelarEdicion").addClass("d-none")
        $(".inputGestionCama").addClass("d-none")
        edicionAvanzada = false
    }
}

function mdlAsociarAla(ubicacion) {
    $("#tituloAsociarAla").text(`Asociando una ala a ${ubicacion.NombreUbicacion}`)
    let listadoOpciones = [...$("#sltAlas > option")]
    listadoOpciones.forEach(option => $(option).removeAttr("disabled"))


    ubicacion.Ala.forEach(ala => {
        listadoOpciones.forEach(option => {
            if (parseInt(ala.IdAla)  == parseInt(option.value)) {
                $(option).attr("disabled", true)
                return;
            }
        })
    })
    $("#btnAsociarAla").data('idubicacion', ubicacion.IdUbicacion)
    $("#mdlAsociarAla").modal('show')
}

function cargarEdicionEvanzada(mostrar) {

    edicionAvanzada = true
    if (mostrar) {
        $(".inputGestionCama").removeClass("d-none")
    }
    
}

function modalReubicacionHabitacion(ubicacion, ala, habitacion) {
    $("#txtInfoReubicacionHabitacion").empty()
    $("#txtInfoReubicacionHabitacion").append(`Seleccione la nueva ubicación para la habitacion ${habitacion.NombreHabitacion}`)

    $("#txtNombreHabitacion").val(habitacion.NombreHabitacion)
    $("#txtNombreHabitacion").data('id',habitacion.IdHabitacion)
    $("#txtNombreHabitacion").attr("disabled", true)

    cargarSltUbicaciones("#sltUbicacionHabitacion")
    $("#sltUbicacionHabitacion").val(ubicacion.IdUbicacion).trigger('change')
    $("#sltAlaHabitacion").val(ala.IdAla)
    
    $("#mdlReubicacionHabitacion").modal("show")
}
function reubicarHabitacion() {
    let nombreHabitacion = document.getElementById("txtNombreHabitacion").value
    let nuevaUbicacion = $('#sltUbicacionHabitacion option:selected').text();
    let nuevaAla = $('#sltAlaHabitacion option:selected').text();
    Swal.fire({
        title: `Reubicando la habitación y sus camas.`,
        html: `Usted está reubicando la habitación ${nombreHabitacion} y sus camas, recuerde que esto tiene un impacto en los sistemas clínicos.<br/>
        Revise y confirme la acción: <br/>
        Reubicando la habitación <b> ${nombreHabitacion} </b>, ahora estará en: <b> ${nuevaUbicacion}-${nuevaAla}</b>
        `,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Reubicar',
        cancelButtonText: 'Cancelar',
    }).then(result=>{
        let objetoEnviar = {
            IdHabitacion: $("#txtNombreHabitacion").data("id"),
            IdUbicacionNueva: $("#sltUbicacionHabitacion").val(),
            IdUbicacionAla: $("#sltAlaHabitacion").val()
        }
        $.ajax({
            method: "PUT",
            url: `${GetWebApiUrl()}HOS_Habitacion/Reubicar`,
            data: objetoEnviar,
            success: function (data) {
                Swal.fire({
                    title: `La cama ha sido reubicada`,
                    icon: 'success',
                    timer: 2000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                            b.textContent = Swal.getTimerLeft()
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(result => {
                    if (ubicacionSeleccionada !== 0)
                        configurarUbicacion(ubicacionSeleccionada)
                })
            }, error: function (error) {
                console.error("Error al reubicar la habitacion")
                console.log(error)
            }
        })
    })
}