$(document).ready(function () {
    ShowModalCargando(false);
    cargarIps();
});

// Funci�n para inicializar DataTable con las IPs
async function dibujarTablaIps(tabla, adataset) {
    $(tabla).DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "IP", data: 0 },
            { title: "Acciones", data: 1 }
        ],
        columnDefs: [
            {
                targets: 1,
                render: function (data, type, row, meta) {
                    const ip = adataset[meta.row][0];
                    return generarBotones(ip);
                },
                width: "100px"
            }
        ],
        bDestroy: true,
    });
}

// Genera los botones para cada fila
function generarBotones(ip) {
    return `
        <center><button type="button" class="btn btn-danger btn-md" onclick="eliminarFila('${ip}')">
            <i class="fa fa-trash-alt fa-sm"></i>
        </button></center>
    `;
}

// Funci�n para cargar las IPs desde el servidor
async function cargarIps() {
    try {
        const response = await $.ajax({
            url: '/Vista/ModuloUrgencia/PantallaEsperaUrgencia.aspx/ObtenerIpsAutorizadas',
            method: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        });

        console.log("Respuesta del servidor:", response);

        if (response.d && Array.isArray(response.d)) {
            const ipsFormateadas = response.d.map(ip => [ip, ""]);
            dibujarTablaIps('#tablaIps', ipsFormateadas);
        } else {
            console.error("La respuesta no es un arreglo de IPs.");
            alert("Error: formato de respuesta incorrecto.");
        }
    } catch (error) {
        alert("Error al cargar las IPs autorizadas.");
        console.error(error);
    }
}


// Funci�n para eliminar una fila
async function eliminarFila(ip) {
    try {
        console.log("Eliminar IP:", ip);
    } catch (error) {
        alert("Error al eliminar la IP.");
        console.error(error);
    }
}
