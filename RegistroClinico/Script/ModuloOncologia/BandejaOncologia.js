﻿$(document).ready(function () {
    buscarSolicitudesDeOncologia()
})

function buscarSolicitudesDeOncologia() {
    InicializarComponentesRut("#sltTipoIdentificacion", "#txtOncoFiltroRut", "#txtOncoFiltroDV", "#txtOncoFiltroRut")
    comboIdentificacion("#sltTipoIdentificacion", "#lblTipoIdentificacion")
    poblarBandejaOncologia([])
    
    ShowModalCargando(false)
}

function poblarBandejaOncologia(data){
    $("#tblOncologia").DataTable({
        data: data,
        columns: [
            {"title":"ID"},
            {"title":"Paciente"},
            {"title":"Fecha"},
            {"title":"Origen"},
            {"title":"Ingresado por:"},
            {"title":"Acciones"},
        ]
    })
}