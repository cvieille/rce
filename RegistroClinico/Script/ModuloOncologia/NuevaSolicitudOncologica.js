﻿let semanaAtras = moment().subtract(7, 'days').format('YYYY-MM-DD')
let semanaAdelante = moment().add(7, 'days').format('YYYY-MM-DD')

$(document).ready(function () {
    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#checkGes").bootstrapSwitch('state', false);
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Establecimiento/Combo`, false, "#sltProcedencia");
    $('#dpFechaIngreso').val(moment().format('YYYY-MM-DD'));
    $('#dpFechaIngreso').prop("min", semanaAtras)
    $('#dpFechaIngreso').prop("max", semanaAdelante)
    $('#dpFechaIngreso').prop("min", semanaAtras)
    $('#dpFechaIngreso').prop("max", semanaAdelante)

    CargarDiagnosticosCIE10()

    $("#dpFechaIngreso").on('blur', function () {
        if (moment($(this).val()).isAfter(moment(semanaAdelante)) || moment($(this).val()).isBefore(moment(semanaAtras))) {
            toastr.error("La fecha no puede exceder una semana")
        $('#dpFechaIngreso').val(moment().format('YYYY-MM-DD'));
    }

})

    ShowModalCargando(false);
    cargarDatosProfesional()
})

function guardarRegistroOncologico() {

    let objetoOncologico = {
        idPaciente: 1,
        Fecha: "123",
        idEstablecimiento: 2,
        ResumenClinic: "dsa",
        Ges: true,
        IdDiagnostico: 1,
        IdTipoComite: 3,
        IdEscalaEcog: 2,
        TratamientoPropuesto: "!@#!",
        IdProfesional: 2,
        IdTipoEstado
    }

}