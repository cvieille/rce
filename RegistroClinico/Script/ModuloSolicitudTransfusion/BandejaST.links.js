﻿
const LinkVerEditarST = (sender) =>{
    let id = $(sender).data("id");
    let idPaciente = $(sender).data("idpaciente");
    setSession('ID_SOLICITUD_TRANSFUSION', id);
    setSession('ID_PACIENTE', idPaciente);
    window.location.replace(ObtenerHost() + "/Vista/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.aspx");
}
const LinkImprimirST = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

const linkRecibirST = (sender)=>{ //Primer Modal Recibir
    let tituloModal = ""
    if ($(sender).data('accion') == "recibir") {
        tituloModal = "Recepcionando solicitud de transfusion"
    }
    else if ($(sender).data('accion') == "finalizar") {
        tituloModal = "Finalizando solicitud de transfusion"
    }
    else if ($(sender).data('accion') == "anular") {        
        $("#divMotivoAnulacion").show()
        $("#motivoAnulacion").val("")
        tituloModal = "Anulando solicitud de transfusion"
    }

    else {
        console.log("Accion incorrecta")
    }
    let id = $(sender).data('id')
    let tipoAccion = $(sender).data('accion');
    $('#idSolicitudTransfusion').val(id);
    $('#tipoAccion').val(tipoAccion);

    $("#txtNombrePaciente").text($(sender).data('nombrepaciente'))
    $("#txtRutPaciente").text($(sender).data('rut'))
    $("#txtFechaSolicitud").text($(sender).data('fechasolicitud'))
    $("#txtProcedimiento").text($(sender).data('procedimiento'))
    $("#txtMotivoTransfusion").text($(sender).data('motivo'))
    $("#txtAmbito").text($(sender).data('ambito'))
    $("#txtProfSolicita").text($(sender).data('profsolicita'))
    $("#tituloModalRecibirFinalizar").text(tituloModal)
    $("#txtPrioridad").text($(sender).data('prioridad'))
    //RecibirST(tituloModal)


    $('#modalRecibirST').modal('show');
}

//Cerrar
const linkCerrarST = (sender) =>{
    let id = $(sender).data("id");
    let nombrePaciente = $(sender).data("nombrepaciente");
    let hemocomponente = $(sender).data("hemocomponente");
    $('#idSolicitudCierre').val(id);
    $('#cierrePaciente').val(nombrePaciente);
    $('#cierreHemocomponente').val(hemocomponente);
    $("#idSolicitudCierre").prop('disabled', true);
    $("#cierrePaciente").prop('disabled', true);
    $("#cierreHemocomponente").prop('disabled', true);

    let url = `${GetWebApiUrl()}RCE_Tipo_Cierre_Transfusion/combo`;
    setCargarDataEnCombo(url, false, "#cerrarMotivoCierre")
    $('#modalCerrarST').modal('show');
}
const LinkVerMovimientosST = (sender) =>{
    let id = $(sender).data("id")
    let titulo = `Solicitud de Transfusión #${id}`
    let _url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Movimientos`

    getMovimientosSistemaGenerico(_url, id)
}
//Procesar Solicitud Transfusion
const linkProcesarSolicitudTransfusion =  (sender) => {
    $.fn.bootstrapSwitch.defaults.onText = "Si"
    $.fn.bootstrapSwitch.defaults.offText = "No"
    $("#checkReaccionAdversa").bootstrapSwitch();
    $("#procesarObservacion").val("")
    let id = $(sender).data("id");
    let contenido = ``, contenidoH = ``
    
    $('#idProcesarST').val(id);
    let url = `${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`;
    setCargarDataEnCombo(url, false, "#sltGrupoSanguineoProcesar")
    let urlBuscarSolicitud = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}`
    ShowModalCargando(true)
    $.ajax({
        type: 'GET',
        url: urlBuscarSolicitud,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $("#txtIdSolicitud").text(data.IdSolicitud)
            let nombrePaciente = `${data.Paciente.Nombre ?? ""} ${data.Paciente.ApellidoPaterno ?? ""} ${data.Paciente.ApellidoMaterno ?? ""}`
            $("#txtNombrePacienteSolicitud").text(nombrePaciente)
            $("#txtRutPacienteSolicitud").text(data.Paciente.NumeroDocumento ?? "")
            if (data.IdHospitalizacion != undefined) {
                $("#txtIdAtencion").text(`Hospitalización N° ${data.IdHospitalizacion}`)
            } else {
                $("#txtIdAtencion").text(`Atención urgencia N° ${data.IdHospitalizacion}`)
            }
            let nombreProfesional = `${data.Profesional.Nombre ?? ""} ${data.Profesional.ApellidoPaterno ?? ""} ${data.Profesional.ApellidoMaterno ?? ""}`
            $("#txtProfesionalSolicitaTransfusion").text(nombreProfesional)
            $("#txtFechaSolicitudTransfusion").text(moment(data.FechaSolicitud).format("DD-MM-YYYY HH:mm"))
            $("#txtFechaRecepcionTransfusion").text(moment(data.FechaRecepcion).format("DD-MM-YYYY HH:mm"))

            $("#txtAmbitoTransfusion").text(data.TipoAmbito.Valor)
            $("#txtProcedimientoSolicitud").text(data.TipoSolicitud.Valor)
            $("#txtMotivoSolicitud").text(data.TipoMotivoTransfusion.Valor)
            if (data.TipoPrioridad!=null)
                $("#txtPrioridadSolicitud").text(data.TipoPrioridad.Valor)
            else
                $("#txtPrioridadSolicitud").text("N/A")

            $("#txtDiagnosticoSolicitud").text(data.Diagnostico??"No informado")
            $("#txtObservacionesSolicitud").text(data.Observacion ?? "No informado")
            //Es transfusion
            if (data.TipoSolicitud.Id == 1) {
                $("#ListaExamenes").empty()
                contenido += `<div class="col-md-12"><h5>Listado de examenes solicitado</h5></div>`
                //if (data.Examenes.length > 0) {
                //    contenido +=`<div class="col-md-12"><h5>Listado de examenes solicitado</h5></div>`
                //    data.Examenes.map(Examen => {
                //        contenido += `
                //            <div class="col-md-3 colsm-6">
                //            <label>${Examen.Descripcion.split("(")[0]}</label>
                //            <input id="" class="form-control" value="${Examen.Valor}" disabled/>
                //            </div>
                //        `
                //    })
                //    $("#ListaExamenes").append(contenido)
                //}
                if (data.Hemocomponentes.length > 0) {
                    $("#ListaHemocomponentes").empty()
                    contenidoH += ` <div class="col-md-12"><h5>Listado de hemocomponentes solicitados</h5></div>
                                    <div class="col-md-4">Hemocomponentes solicitados</div>
                                    <div class="col-md-4">Cantidad Solicitada</div>
                                    <div class="col-md-4">Cantidad transfundida</div>
                                   `
                    data.Hemocomponentes.map(Hemo => {
                        console.log(Hemo)
                        contenidoH += `
                            <div class="col-md-4 col-sm-12 mt-1">
                                <span><b>${Hemo.TipoHemocomponente.Valor}</b></span>
                            </div>
                            <div class="col-md-4 col-sm-12 mt-1">
                                <input value="${Hemo.Cantidad}" class="form-control w-50" disabled />
                            </div>
                            <div class="col-md-3 col-sm-12 mt-1">
                                <input id="Hemo-${Hemo.Id}"
                                type="text" class="form-control number cantidad-transfundida"
                                onkeydown="validarLongitud(this,4,event)"
                                placeholder="Cantidad de ${Hemo.TipoHemocomponente.Valor.split("(")[0]}"
                                data-required="true"/>
                            </div>
                        `
                        
                        buscarExamenesPorId(Hemo.TipoHemocomponente.Id).then(res => {
                            contenido = ""
                            res.map(Examen => {
                                let coincidencia = data.Examenes.find(x => x.IdCarteraServicio == Examen.Id)
                                contenido += `
                                    <div class="col-md-3 colsm-6">
                                    <label>${Examen.Valor.split("(")[0]}</label>
                                    <input id="inputExamen-${Examen.Id}" type="text"
                                    class="form-control ${Examen.Formato == 'int' ? "number" : ""}   ${coincidencia == undefined? "examen-transfusion":""}"
                                    placeholder="Ingrese cantidad ${Examen.Etiqueta !== null ? `en ${Examen.Etiqueta}` : `de ${Examen.Valor}`}"
                                    ${Examen.Formato == 'flt' ?`onkeydown="validarDecimales(event)"`:""}
                                    maxlength="${Examen.Caracteres}"
                                    ${coincidencia !== undefined ? `value = "${coincidencia.Valor}" disabled` :`data-required="true"`}
                                    />
                                    </div>
                                `
                                
                            })
                            $("#ListaExamenes").append(contenido)
                            ValidarNumeros()
                        }).catch(err => {
                            console.error("Ha ocurrido un error al recuperar los examenes")
                        })
                    })
                    
                    $("#ListaHemocomponentes").append(contenidoH)
                }
                
            } else {
                $("#ListaHemocomponentes").empty()
                $("#ListaExamenes").empty()
                $("#divGrupoSanguineo").addClass("d-none")
                $("#divCheckReaccion").addClass("d-none")
                //Es sangria
                contenidoSangria = `
                        <div class="col-md-3 sol-sm-12">
                            <label>Cantidad sesiones</label>
                            <input type="text" class="form-control" value="${data.CantidadSesion}" disabled/>
                        </div>

                        <div class="col-md-3 sol-sm-12">
                            <label>Cantidad sesiones realizadas</label>
                            <input type="text" id="txtCantidadRealizadas" placeholder="Ingrese sesiones realizadas" onkeydown="validarLongitud(this,3,event)" class="form-control number"  data-required="true" />
                        </div>

                        <div class="col-md-3 sol-sm-12">
                            <label>Cantidad por sesión</label>
                            <input type="text" class="form-control" value="${data.NumeroSesion}" disabled/>
                        </div>

                        <div class="col-md-3 sol-sm-12">
                            <label>Cantidad sesiones realizadas</label>
                            <input type="text" id="txtCantidadRealizadasSesion" placeholder="Ingrese cantidad por sesión" onkeydown="validarLongitud(this,3,event)" class="form-control number"  data-required="true" />
                        </div>
                    `
                $("#ListaHemocomponentes").append(contenidoSangria)
            }
            ReiniciarRequired()
            ValidarNumeros()
        }, error: function (err) {
            toastr.error("Ha ocurrido un error al buscar la informacion de la solicitud con id:"+id)
            console.error("Ha ocurrido un error al buscar la informacion de la solicitud con id:" + id)
            console.log(err.responseJSON)
            console.log(err.status)
        }
    })
    ShowModalCargando(false)
    $('#modalProcesarST').modal('show');
}
const buscarExamenesPorId = async (id) => {
    let examenes = []
    let url = `${GetWebApiUrl()}RCE_Examen_Hemocomponente_Transfusion/${id}/Combo`;
    await $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {
            examenes = [...data]
        }, error: function (err) {
            console.error("Ha ocurrido un error al buscar los hemocomponentes")
            console.error("Mensaje de error:")
            console.log(err)
        }
    });
    return examenes
}