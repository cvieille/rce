﻿

let HemocomponentesNuevos = [], HemocomponentesAlmacenados = [], ExamenesAlmacenados = [], HemocomponentesApi = [], ExamenesApi = []
let ExamenesTransfusionNuevos = [];
let idSolicitudTransfusion = 0, editando = false
let idHOS = idURG = idPAC = null;
let IdHospitalizacionST = null
let IdAtencionesUrgenciaST = null

let ss = getSession();

$(document).ready(function () {

    inicializarComponentesVista();
    ValidarNumeros()

    if (ss.ID_SOLICITUD_TRANSFUSION != undefined && ss.ID_SOLICITUD_TRANSFUSION != null) {
        editando = true
        obtenerSolicitud(ss.ID_SOLICITUD_TRANSFUSION);
        getDataFormST()
        deleteSession('ID_SOLICITUD_TRANSFUSION');

    } else {
        cargarProfesional(getSession().ID_PROFESIONAL);
        BloquearPaciente()
        //showModalCargandoComponente(false);
    }

    if (idPAC != undefined) {

        CargarPacienteActualizado(idPAC)
        let idIdentificacion = $("#sltIdentificacion").val()
        let numeroDocumento = $("#txtnumerotPac").val()
        buscarPacientePorDocumento(idIdentificacion, numeroDocumento).then(paciente => {
            if (paciente.Prevision !== null)
                comboPrevisionTramo(paciente.Prevision.Id)
            if (paciente.Prevision != null && paciente.PrevisionTramo != null)
                getPrevisionPrevisionTramo(paciente.Prevision.Id, paciente.PrevisionTramo.Id)
        })
        
        BloquearPaciente();
        let idPacienteActual = GetPaciente().GEN_idPaciente
        if (idPacienteActual != undefined) {
            cargarTransfusionesPrevias(idPacienteActual)
        }
    }
    $('#sltPrioridadST').change(function () {
        if ($('#sltPrioridadST').val() == 4) {
            if ($('#divObservacionesPrioridad').hasClass("d-none")) {
                $('#divObservacionesPrioridad').removeClass("d-none")
            }
        } else {
            if (!$('#divObservacionesPrioridad').hasClass("d-none")) {
                $('#divObservacionesPrioridad').addClass("d-none")
            }
        }
    })


    $('#sltTipoST').change(function () {

        if ($('#sltTipoST').val() === "1")//Cuando es transfusion
            cambiarVistaporTipoST(true)
        else if ($('#sltTipoST').val() === "2")//Cuando es sangria
            cambiarVistaporTipoST(false)
        else
            cambiarVistaporTipoST(null)
    });
});

function cargarTransfusionesPrevias(idPaciente) {

    let arrayTransfusionesPrevias = []
    let urlTransfusionesPrevias = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/Buscar?idPaciente=${idPaciente}`

    $.ajax({
        type: 'GET',
        url: urlTransfusionesPrevias,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {
            if (data.length > 0) {
                data.map(item => {
                    arrayTransfusionesPrevias.push(
                        [
                            item.Id,
                            item.FechaSolicitud,
                            item.TipoEstadoSistema != null ? item.TipoEstadoSistema.Valor : "No informado",
                            item.TipoSolicitud != null ? item.TipoSolicitud.Valor : "No informado",
                            item.TipoMotivoTransfusion != null ? item.TipoMotivoTransfusion.Valor : "No informado",
                            item.TipoAmbito != null ? item.TipoAmbito.Valor : "No informado",
                            item.Profesional != null ? `${item.Profesional.Nombre} ${item.Profesional.ApellidoPaterno}` : "No informado",
                            item.ReaccionAdversa
                        ]
                    )
                })
                cargarTablaTransfusionesPrevias(arrayTransfusionesPrevias)
            } else {
                cargarTablaTransfusionesPrevias([])
            }

        }, error: function (err) {
            console.error("No se han encontrado transfusiones previas")
            cargarTablaTransfusionesPrevias([])
            //toastr.error("Ha ocurrido un error al intentar buscar las transfusiones del paciente")
        }
    })

}

function cargarTablaTransfusionesPrevias(arrayTransfusionesPrevias) {
    $("#tblTransfusionesPrevias").addClass("text-wrap").addClass('dataTable').DataTable({
        data: arrayTransfusionesPrevias,
        columns: [
            { title: 'ID', className: "text-center font-weight-bold rce-tray-id" },
            { title: "Fecha" },
            { title: "Estado" },
            { title: "Solicitud" },
            { title: "Motivo" },
            { title: "Ambito" },
            { title: "Profesional" },
            { title: "Reaccion", className: "text-center" }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let infoReaccion = ``
                    switch (arrayTransfusionesPrevias[fila][7]) {
                        case true:
                            infoReaccion = `<span><i class="fa fa-exclamation-triangle" style="color:red;" aria-hidden="true"></i> <br> Adversa</span>`
                            break
                        case false:
                            infoReaccion = `<span><i class="fa fa-check" style="color:green;" aria-hidden="true"></i> <br> Normal</span>`
                            break
                        default:
                            infoReaccion = `<span><i class="fa fa-file-o" style="color:blue;" aria-hidden="true"></i> <br>Sin información</span>`
                            break
                    }
                    return infoReaccion
                }
            }
        ],
        bDestroy: true
    })
}

function dibujarHemocomponentes() {
    const url = `${GetWebApiUrl()}RCE_Tipo_Hemocomponente_Transfusion/Combo`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {
            let contenido = "", contenido2 = ""
            data.map(item => {
                contenido += `
                <div class="row p-2" id="divCantidadHemocomponente-${item.Id}">
                    <div class="col-md-3 col-sm-12">
                    <input class="form-check-input" type="checkbox" value="${item.Id}" onchange="mostrarExamenes(this,event)" data-componente="${item.Valor}" id="checkbox-${item.Id}"/>
                    <label class="form-check-label" for="${item.Id}-${item.Valor}">
                        ${item.Valor}
                    </label>
                    </div>
                    <div class="col-md-3 col-sm-12" >
                        <label for="inputCantidad-${item.Id}"> Ingrese cantidad:${item.Valor.split("(")[0]}</label>
                        <input type="text"
                        class="form-control number" id="inputCantidad-${item.Id}" 
                        onkeydown="validarLongitud(this, 2,event)"
                        placeholder="${item.Etiqueta !== null ? item.Etiqueta : `Cantidad de ${item.Valor.split("(")[0]}`}"
                        data-required="true" disabled/>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row  d-none" id="examenesHemocomponente-${item.Id}"></div>
                    </div>
                </div>
                `
            })
            $("#listadoHemocomponentes").append(contenido)
        }, error: function (err) {
            console.error("Ha ocurrido un error al buscar los hemocomponentes")
            console.error("Mensaje de error:")
            console.log(err)
        }
    });
}
function mostrarExamenes(chechbox, event) {
    event.preventDefault()
    let id = chechbox.value
    if (chechbox.checked) {
        $(`#inputCantidad-${id}`).removeAttr("disabled")
        $(`#examenesHemocomponente-${id}`).removeClass("d-none")
        $(`#inputCantidad-${id}`).attr("data-required", true)
        buscarExamenesPorId(id)
    } else {
        //En la edicion se debe controlar que borren los examenes anteriores.
        if (editando) {
            //Gestionar los hemocomponetes guardados anteriormente
            if (HemocomponentesAlmacenados.length > 0) {
                console.log(HemocomponentesAlmacenados)
                let coincidencia = HemocomponentesAlmacenados.find(item => item.TipoHemocomponente.Id == id).Id
                if (coincidencia) {
                    HemocomponentesAlmacenados = HemocomponentesAlmacenados.filter(item => item.Id !== coincidencia)
                }
            }
        }
        //Cuando no se esta editando
        $(`#inputCantidad-${id}`).attr("disabled", true)
        $(`#inputCantidad-${id}`).val("")
        $(`#examenesHemocomponente-${id}`).addClass("d-none")
        $(`#examenesHemocomponente-${id}`).removeAttr("data-required")

    }
}
function buscarExamenesPorId(id) {

    $(`#examenesHemocomponente-${id}`).empty()
    let url = `${GetWebApiUrl()}RCE_Examen_Hemocomponente_Transfusion/${id}/Combo`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {

            let contenido = ``
            data.map(item => {

                contenido += `
                        <div class="col-md-6" id="divInputCantidadExamen-${id}">
                        <label for="inputCantidadExamen-${item.Id}">${item.Valor}</label>
                        <input type="text"
                            class="form-control ${item.Formato == "int" ? "number" : ""}"
                            id="inputCantidadExamen-${item.Id}"
                            ${item.Formato == "flt" ? "onkeydown='validarDecimales(event)'" : ""}
                            maxlength="300"
                            onkeypress="validarLongitud(this, ${item.Caracteres},event)"
                            placeholder="${item.Etiqueta == null ? `Cantidad de ${item.Valor}` : `Ingrese cantidad en: ${item.Etiqueta}`}"
                            ${id != 4 ? `data-required="true"` : ""}/>
                        </div>
                `
            })
            $(`#examenesHemocomponente-${id}`).append(contenido)
            ValidarNumeros()
        }, error: function (err) {
            console.error("Ha ocurrido un error al buscar los hemocomponentes")
            console.error("Mensaje de error:")
            console.log(err)
        }
    });
}
function buscarHemocomponentes() {

    let nombreErrorHemocomponente = "", valido = true
    let HemocomponentesModificados = [], ExamenesModificados = []
    Hemocomponentes = []
    //Busqueda de hemocomponentes seleccionados (checkbox)
    let inputChecked = $("#listadoHemocomponentes").find("input[type='checkbox']:checked")
    if (inputChecked.length > 0) {
        //Recorrer los checkbox para almacenar los hemocomponentes o modificar los que vienen de la bdd
        inputChecked.map((index, item) => {
            let idHemoComponente = parseInt(item.value)
            let validado = false;
            //Cuando es transfusion inmediata
            if ($("#sltPrioridadST").val() == '1') {
                //Cuando solicita globulos rojo solo valida el campo Hemoglobina.
                if (document.getElementById("checkbox-1").checked) {
                    if ($("#inputCantidadExamen-450").val() == "") {
                        $("#inputCantidadExamen-450").addClass("invalid-input")
                        nombreErrorHemocomponente = "Hemoglobina"
                        valido =  false
                    }
                }
                //Cuando es plasma fresco tiene que tener contenido oel TTPA o el TP
                if (document.getElementById("checkbox-2").checked) {
                    if ($("#inputCantidadExamen-453").val() == "" && $("#inputCantidadExamen-454").val() == "") {
                        $("#inputCantidadExamen-453").addClass("invalid-input")
                        $("#inputCantidadExamen-454").addClass("invalid-input")
                        nombreErrorHemocomponente = "Plasma fresco"
                        valido = false
                    }
                }

                return valido
            } else {
                valido = validarCampos(`#divCantidadHemocomponente-${item.value}`, true)
            }
            //Validar el input cantidad de cada Hemocomponente
            if (valido) {
                //Cuando se esta editando la solicitud de transfusion
                if (editando) {
                    //Comprobar que hayan  hemocomponentes almacenados en la bdd
                    if (HemocomponentesAlmacenados.length > 0) {
                        //Obtener el id del cada Hemocomponente seleccionado (usando los checkbox)

                        //revisar si existe en la bdd, para solo modificar la cantidad
                        let HemocomponenteBuscado = HemocomponentesAlmacenados.find(item => item.TipoHemocomponente.Id == idHemoComponente)
                        //Se encontro un Hemocomponente con registros anteriores
                        if (HemocomponenteBuscado) {
                            //Modificar cantidad
                            HemocomponenteBuscado.Cantidad = parseInt($(`#inputCantidad-${item.value}`).val())
                            //guardar el elemento modificado en un nuevo arreglo
                            HemocomponentesModificados.push({
                                Id: HemocomponenteBuscado.Id,
                                IdTipoHemocomponenteTransfusion: HemocomponenteBuscado.TipoHemocomponente.Id,
                                Cantidad: HemocomponenteBuscado.Cantidad
                            })
                        } else {
                            //En caso de que no haya sido almacenado con anterioridad, debe seer un nuevo registro
                            HemocomponentesNuevos.push({ IdTipoHemocomponenteTransfusion: parseInt(item.value), Cantidad: parseInt($(`#inputCantidad-${item.value}`).val()) })
                        }
                    }
                    //buscar xamenes almacenados para modificar o crear nuevos
                    let inputExamenesHemocomponente = $(`#examenesHemocomponente-${idHemoComponente}`).find("input[type='text']")
                    inputExamenesHemocomponente.map((index, input) => {
                        let nuevoValor = input.value
                        let idCartera = parseInt($(input).attr("id").split("-")[1])
                        ExamenBuscado = ExamenesAlmacenados.find(Examen => Examen.IdCarteraServicio == idCartera)
                        if (ExamenBuscado) {
                            ExamenesModificados.push({
                                Id: ExamenBuscado.Id,
                                IdCarteraArancel: ExamenBuscado.IdCarteraServicio,
                                Valor: parseInt(nuevoValor)
                            })
                        } else {
                            //Cuando el examen no tiene valor, no lo agrega al arreglo (solo en transfusion inmedianta)
                            if (input.value !== "") {
                                ExamenesTransfusionNuevos.push({
                                    IdCarteraArancel: parseInt($(input).attr('id').split("-")[1]),
                                    Valor: parseFloat($(input).val())
                                })
                            }

                        }
                    })
                } else {
                    //Cuando se esta creando una nueva solicitud de transfusion
                    HemocomponentesNuevos.push({ IdTipoHemocomponenteTransfusion: parseInt(item.value), Cantidad: parseInt($(`#inputCantidad-${item.value}`).val()) })
                    let inputExamenesHemocomponente = $(`#examenesHemocomponente-${item.value}`).find("input[type='text']")
                    //Recorrer la cantidad de examenes de cada hemocomponete
                    inputExamenesHemocomponente.map((index, item) => {
                        if (item.value !== "") {
                            ExamenesTransfusionNuevos.push({
                                IdCarteraArancel: parseInt($(item).attr('id').split("-")[1]),
                                Valor: parseFloat($(item).val())
                            })
                        }
                    })

                }
            } else {
                nombreErrorHemocomponente += $(item).data("componente").split("(")[0]
                valido = false
            }
        })
        //cuando hacen falta campos obligatorios en las cantidades
        if (!valido) {
            toastr.error("Faltan las cantidades de:" + nombreErrorHemocomponente)
            return false
        }
    } else {
        //Cuando intentan enviar sin haber seleccionado al menos un hemocomponente
        toastr.error("Debe seleccion al menos un Hemocomponente")
        return false
    }
    ExamenesApi = ExamenesModificados.concat(ExamenesTransfusionNuevos)
    HemocomponentesApi = HemocomponentesModificados.concat(HemocomponentesNuevos)

    return true
}
function getDataFormST() {

    let id = ss.ID_SOLICITUD_TRANSFUSION

    if (id !== undefined) {
        let url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}`

        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: function (transfusion) {

                const { IdSolicitud, Hospitalizacion = null, AtencionesUrgencia = null, Prevision, PrevisionTramo } = transfusion

                comboPrevisionTramo(Prevision.Id)
                getPrevisionPrevisionTramo(Prevision.Id, PrevisionTramo.Id)

                if (Hospitalizacion !== null) {
                    IdHospitalizacionST = Hospitalizacion.IdHospitalizacion
                }
                else if (AtencionesUrgencia !== null) {
                    IdAtencionesUrgenciaST = AtencionesUrgencia.IdAtencionesUrgencia
                }
            }, error: function (err) {
                console.error(err)
            }
        });
    }  
}

function inicializarComponentesVista() {
    //    idHOS = getSession().ID_HOSPITALIZACION;
    idURG = getSession().ID_ATENCION_URGENCIA;
    idPAC = getSession().ID_PACIENTE;
    iniciarFechasHoras();
    cargarCombosNuevaSolicitud()
    /*crearTablaExamenesST()*/
    dibujarHemocomponentes()
    //muestra Hospitalización y Urgencia
    $("#sltAmbito > option").each(function () {
        if (this.value != 1 && this.value != 3)
            $(this).hide()
    });

    if (ss.ID_HOSPITALIZACION !== undefined) {
        IdHospitalizacionST = ss.ID_HOSPITALIZACION
        $("#txtIDHOsOrUrg").val(IdHospitalizacionST)
        establecerAmbitoTransfusion(1);        
        $("#txtIDHOsOrUrg").show()
        //Aca debo buscar la hospitalizacion
        buscarHospitalizacion(IdHospitalizacionST)
    } else {
        IdAtencionesUrgenciaST = ss.ID_ATENCION_URGENCIA
        $("#txtIDHOsOrUrg").val(IdAtencionesUrgenciaST)
        establecerAmbitoTransfusion(3)        
        $("#txtIDHOsOrUrg").show()
    }

    $("#sltHemocomponenteST").change(function (e) {
        let idHemocomponente = parseInt(e.target.value)
        if (idHemocomponente == 0) {
            if (!$("#divExamenesST").hasClass("d-none"))
                $("#divExamenesST").addClass("d-none")
        } else {
            let url = `${GetWebApiUrl()}RCE_Examen_Hemocomponente_Transfusion/${idHemocomponente}/Combo`;
            setCargarDataEnCombo(url, true, "#sltExamenesLaboratorio")
            $("#divExamenesST").removeClass("d-none")
        }

    })
}
function establecerAmbitoTransfusion(id) {
    $("#sltAmbito").val(id)
    $("#sltAmbito").attr("disabled", true)
}
function buscarHospitalizacion(idhos) {
    if (idhos) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idTipoEstado[0]=87&idTipoEstado[1]=88&idHospitalizacion=${idhos}`,
            async: false,
            success: function (data) {
                if (data[0].Diagnostico != null)
                    if (data[0].Diagnostico.Actual != null)
                        $("#txaDiagnosticoST").val(data[0].Diagnostico.Actual)
            }, error: function (err) {
                toastr.error("Error al buscar la hospitalizacion")
                console.log("Error al buscar la hospitalizacion Error:", err)
            }
        });
    }
}
function iniciarFechasHoras() {
    let fechaHoy = moment(Date.now()).format("YYYY-MM-DD");
    let horaActual = moment(Date.now()).format("HH:mm");
    $("#dtFechaST").val(fechaHoy);
    $("#tmHoraST").val(horaActual);
    $("#dtFechaRecepcionBS").val(fechaHoy);
    $("#tmHoraRecepcionBS").val(horaActual);
}

function cargarProfesional(id) {
    let url = "/GEN_Profesional/" + id;
    promesaAjax("GET", url).then(
        jsonRespuesta => {

            console.log("viene el profesional?", jsonRespuesta)
            $("#txtNumeroDocumentoProfesional").val(`${jsonRespuesta.NumeroDocumento}-${jsonRespuesta.Digito}`)
            $("#txtNombreProfesional").val(`${jsonRespuesta.Nombre}`)
            $("#txtApePatProfesional").val(`${jsonRespuesta.ApellidoPaterno}`)
            $("#txtApeMatProfesional").val(`${jsonRespuesta.ApellidoMaterno}`)
        }
    ).catch(
        err => console.error(err)
    )
}

function clickBtnGuardarST() {
    if (validarCampos(".cuerpoST", true)) {
        let success = true;
        let id = ss.ID_SOLICITUD_TRANSFUSION;
        let json = {};
        let url;
        let metodo;

        if (id > 0) {
            metodo = "PUT";
            url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}`;
        } else {
            metodo = "POST";
            url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion`;
        }
        json = crearObjetoParaApi(metodo);

        if (json != undefined) {
            ShowModalCargando(true);

            $.ajax({
                type: metodo,
                url: url,
                contentType: 'application/json',
                async: true,
                data: JSON.stringify(json),
                success: function (data) {
                    ShowModalCargando(false);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: `Solicitud de transfusión ingresada`,
                        text: `La solicitud de transfusión fue ingresada correctamente`,
                        showConfirmButton: false,
                        timer: 1500,
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            window.location.replace(ObtenerHost() + "/Vista/ModuloSolicitudTransfusion/BandejaST.aspx");
                        }
                    });
                }, error: function (err) {
                    console.error("Ha ocurrido un error al guardar/editar la solicitud de transfusion")
                    console.error("Mensaje de error:")
                    console.log(err)
                }
            });
        }
    }
}


function crearObjetoParaApi(metodo) {
    let idTipoSolicitud = $('#sltTipoST').val()
    let validarElementos = true
    //Cuando es transfusion
    if (idTipoSolicitud == "1") {
        validarElementos = buscarHemocomponentes()
    } else {//cuando es sangria
        validarElementos = validarCampos("#divSangriaST", true)
    }

    console.info(validarElementos)

    if (validarElementos) {

        const json = {
            "DiagnosticoSolicitudTransfusion": $("#txaDiagnosticoST").val(),
            "IdTipoSolicitud": valCampo(parseInt($("#sltTipoST").val())),
            "IdTipoMotivoTransfusion": valCampo(parseInt($("#sltTipoMotivoST").val())),
            "IdTipoAmbito": valCampo(parseInt($("#sltAmbito").val())),
            "IdTipoPrioridadTransfusion": $("#sltPrioridadST").val() != "0" ? parseInt($("#sltPrioridadST").val()) : null,
            "ObservacionPrioridadSolicitudTransfusion": $("#txtObservacionesReserva").val() == "" ? null : $("#txtObservacionesReserva").val(),
            "IdPaciente": GetPaciente().GEN_idPaciente,
            "IdHospitalizacion": IdHospitalizacionST ?? null,
            "IdAtencionesUrgencia": IdAtencionesUrgenciaST ?? null,
            "NumeroSesion": $("#txtNumeroSesiones").val() != "" ? parseInt($("#txtNumeroSesiones").val()) : null,
            "CantidadSesion": $("#textCantidadSesiones").val() != "" ? parseInt($("#textCantidadSesiones").val()) : null,
            "Hemocomponentes": HemocomponentesApi,
            "Examenes": ExamenesApi,
            "Observacion": $("#txtObservacionSolicitud").val()
        };

        return json;
    }
}

function obtenerSolicitud(id) {

    let url = "RCE_Solicitud_Transfusion/" + id;
    let success = true;

    promesaAjax("GET", url)
        .then(jsonRespuesta => {
            if (jsonRespuesta.IdHospitalizacion != undefined || jsonRespuesta.IdHospitalizacion != null) {

                $("#sltAmbito").val(1)
                $("#sltAmbito").attr("disabled", true)
                $("#txtIDHOsOrUrg").val(jsonRespuesta.IdHospitalizacion)
                $("#txtIDHOsOrUrg").show()
            }
            else {
                $("#sltAmbito").val(3)
                $("#sltAmbito").attr("disabled", true)
                $("#txtIDHOsOrUrg").val(jsonRespuesta.IdAtencionesUrgencia)
                $("#txtIDHOsOrUrg").show()
            }

            return cargarSolicitud(jsonRespuesta)

        })
        .catch(error => {
            success = false
            console.log(error)
        })
        .finally(() => {
            ShowModalCargando(false);
            if (!success)
                toastr.error("Error al cargar <u><b>SOLICITUD DE TRANSFUSIÓN #" + id + "</b></u>");
        });
}

async function cargarSolicitud(json) {

    CargarPacienteActualizado(json.Paciente.Id)
    BloquearPaciente()

    idHOS = json.IdHospitalizacion;
    idURG = json.IdAtencionesUrgencia;
    $("#txaDiagnosticoST").val(json.Diagnostico);
    let fechaHora = moment(json.FechaSolicitud);
    $("#dtFechaST").val(fechaHora.format('YYYY-MM-DD'));
    $("#tmHoraST").val(fechaHora.format('HH:MM'));
    $("#sltTipoMotivoST").val(json.TipoMotivoTransfusion.Id);

    $("#nmbCantidadHemocomponenteST").val(json.CantidadTipoHemocomponente);
    if (json.TipoPrioridad != null)
        $("#sltPrioridadST").val(json.TipoPrioridad.Id).change();
    $("#txtObservacionesReserva").val(json.ObservacionPrioridad ?? null);
    $("#txtObservacionSolicitud").val(json.Observacion ?? "");
    //$("#txtSangriaTerapeutica").val(json.SangriaTerapeutica); depreacted
    $("#swTransfusionesPrevias").prop("checked", json.TransfusionesPrevias);
    $("#swReaccionAdversa").prop("checked", json.ReaccionAdversa);

    //$("#txtNumeroDocumentoProfesional").val(json.Profesional.Rut);
    //$("#txtNombreProfesional").val(json.Profesional.Nombre);
    //$("#txtApePatProfesional").val(json.Profesional.ApellidoPaterno);
    //$("#txtApeMatProfesional").val(json.Profesional.ApellidoMaterno);

    const profesionalST = await getProfesionalPorIdST(json.Profesional.Id)
    await cargarProfesionalST(profesionalST)

    $("#sltTipoST").val(json.TipoSolicitud.Id).change()
    if (json.TipoSolicitud.Id == 1) {
        //es transfusion


        const hemocomponentesST = await getHemocomponentesST(json.IdSolicitud)
        const examenesST = await getExamenesST(json.IdSolicitud)


        let cargoComponentes = cargarHemocomponentes(hemocomponentesST);
        //Cargo componentes de manera exitosa
        if (cargoComponentes) {
            cargarExamenes(examenesST)
        }
    } else {
        $("#txtNumeroSesiones").val(json.NumeroSesion)
        $("#textCantidadSesiones").val(json.CantidadSesion)
    }
}

async function getExamenesST(id) {

    try {
        const examen = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Examen`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return examen

    } catch (error) {
        console.error("Error al cargar hemocomponentes")
        console.log(JSON.stringify(error))
    }
}

async function getHemocomponentesST(id) {

    try {
        const hemocomponente = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Hemocomponente`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return hemocomponente

    } catch (error) {
        console.error("Error al cargar hemocomponentes")
        console.log(JSON.stringify(error))
    }
    
}

async function getProfesionalPorIdST(id) {

    try {
        const profesional = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return profesional

    } catch (error) {
        console.error("Error al cargar profesional")
        console.log(JSON.stringify(error))
    }
}

async function cargarProfesionalST(profesional) {

    if (profesional !== null) {
        const rutProfesionalST = `${profesional.GEN_rutProfesional}-${profesional.GEN_digitoProfesional}`

        $("#txtNumeroDocumentoProfesional").val(rutProfesionalST);
        $("#txtNombreProfesional").val(profesional.GEN_nombreProfesional);
        $("#txtApePatProfesional").val(profesional.GEN_apellidoProfesional);
        $("#txtApeMatProfesional").val(profesional.GEN_sapellidoProfesional);
    }
}

//Carga de hemocomponentes en el PATCH
function cargarHemocomponentes(Hemocomponentes) {
    if (Hemocomponentes.length > 0) {
        Hemocomponentes.map(item => {
            $(`#checkbox-${item.TipoHemocomponente.Id}`).prop('checked', true).change();
            $(`#inputCantidad-${item.TipoHemocomponente.Id}`).val(item.Cantidad);
        })
        //Guardar los hemocomponentes
        HemocomponentesAlmacenados = Hemocomponentes
        return true
    } else {
        console.error("Ha ocurrido un error, no hay hemocomponetes ingresados")
        toastr.error("Error, no se encontraron hemocomponentes")
        return false
    }
}

async function cargarExamenes(Examenes) {
    await sleep(100)
    Examenes.map(Examen => {
        $(`#inputCantidadExamen-${Examen.IdCarteraServicio}`).val(Examen.Valor)
    })
    ExamenesAlmacenados = Examenes
}

function cambiarVistaporTipoST(esTransfusion) {
    if (esTransfusion === true) {
        //$("#divExamenesST").show();

        $("#divSangriaST").hide();
        $("#divEspecificacionesST").show();
        $("#dvPrioridadST").show();

    }
    else if (esTransfusion === false) {
        //$("#divExamenesST").hide();
        $("#dvPrioridadST").hide();
        $("#divSangriaST").show();
        $("#divEspecificacionesST").hide();
        $("#sltPrioridadST").val(0).change()
    }
    else {
        $("#divExamenesST").hide();
        $("#divSangriaST").hide();
    }
}
