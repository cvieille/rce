﻿let idDiv = "";
var sSession;


$(document).ready(() => {
    inicializarBandejaST();

    sSession = getSession();

    if (sSession.CODIGO_PERFIL == perfilAccesoSistema.bancoSangre) {
        $('#lnbNuevoST').hide();
        $('#divMotivoAnulacion').hide();
    }

    
    //Agregar inicializacion de filtros, llamada respectiva y eventos botones...
    cargarCombosBandeja()

    $('#txtSTFiltroRut').on('keypress', function (e) {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4) {
            var charCode = (e.which) ? e.which : e.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    });

    $('#txtSTFiltroRut').on('blur', async function () {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4)
            $('#txtSTFiltroDV').val(await ObtenerVerificador($(this).val()));
    });

    //sltTipoIdentificacion txtSTFiltroRut
    ocultarMostrarFichaPac($("#sltTipoIdentificacion"), $("#txtSTFiltroRut"))

    $('#btnFiltro').on('click', function (e) {
        cargarBandejaST(sSession)
        e.preventDefault();
    });

    $('#btnLimpiarFiltro').on('click', function (e) {
        limpiarFiltros();
        e.preventDefault();
    });
    $('#btnRecibirST').click(function () { //Segundo Modal Recibir
        let validacion = true;
        $("#motivoAnulacion").removeClass("is-invalid")
        if ($('#tipoAccion').val() == 'anular') {
            $("#motivoAnulacion").val() == "" ? validacion = false : validacion = true
        }
        if (validacion) {
            ShowModalAlerta('CONFIRMACION',
                'Solicitud de Transfusión',
                'Una vez realizado, no se podrá revertir los cambios',
                RecibirST, null, 'Aceptar', 'Cancelar');
        } else {
            toastr.error("Se requiere motivo de anulacion")
            $("#motivoAnulacion").addClass("is-invalid")
        }
    });
});

function inicializarBandejaST() {
    ShowModalCargando(true);
    cargarBandejaST();
    ShowModalCargando(false);
}

function toggle(id) {
    if (idDiv == "") {
        $(id).fadeIn();
        idDiv = id;
    }
    else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = "";
    }
    else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}

function cargarBandejaST() {
    sSession = getSession();
    //Cargar Estados Transfusiones
    let adataset = [];

    let url_ = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/Bandeja?` +
        `id=${$("#txtSTFiltroId").val() !== "" ? $("#txtSTFiltroId").val() : 0}` +
        `&idEstado=${$("#sltSTEstado").val()}` +
        `&idAmbito=${$("#sltAmbito").val()}` +
        `&nombre=${$.trim($("#txtSTFiltroNombre").val())}` +
        `&apellidoPaterno=${$.trim($("#txtSTFiltroApeParteno").val())}` +
        `&apellidoMaterno=${$.trim($("#txtSTFiltroSApeMaterno").val())}` +
        `&numeroDocumento=${$.trim($("#txtSTFiltroRut").val())}`

    $.ajax({
        type: 'GET',
        url: url_,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    /*0*/val.Id,
                    /*1*/(val.Paciente.NumeroDocumento ?? '') + '-' + (val.Paciente.Digito ?? ''),
                    /*2*/(val.Paciente.Nombre ?? '') + ' ' + (val.Paciente.ApellidoPaterno ?? '') + ' ' + (val.Paciente.ApellidoMaterno ?? ''),
                    /*3*/moment(moment(val.FechaSolicitud).toDate()).format('DD-MM-YYYY'),
                    /*4*/val.TipoAmbito.Valor,
                    /*5*/val.TipoSolicitud.Valor,
                    /*6*/val.TipoMotivoTransfusion.Valor,
                    /*7*/(val.Profesional.Nombre ?? '') + ' ' + (val.Profesional.ApellidoPaterno ?? '') + ' ' + (val.Profesional.ApellidoMaterno ?? ''),
                    /*8*/val.TipoEstadoSistema.Id,
                    /*9*/val.TipoPrioridad!=undefined?val.TipoPrioridad.Valor:"",
                    /*10*/val.Acciones.Editar,
                    /*11*/val.Acciones.Imprimir,
                    /*12*/val.Acciones.VerMovimiento,
                    /*13*/val.Acciones.Recibir,
                    /*14*/val.Acciones.Cerrar,
                    /*15*/val.Acciones.Anular,
                    /*16*/val.Acciones.Procesar,
                    /*17*/val.TipoPrioridad!=undefined? val.TipoPrioridad.Id:"",
                    /*18*/val.Paciente.IdPaciente,
                    '',
                ]);
            });
            LlenaGrillaST(adataset, sSession);


            $('[data-toggle="tooltip"]').tooltip();

        }

    })
}



function LlenaGrillaST(adataset, sSession) {

    //Tabla
    $("#tblST").addClass("text-wrap").addClass('dataTable').DataTable({
        "order": [[0, "desc"]],
        responsive:true,
        data: adataset,
        columns: [
            { title: 'ID', className: "text-center font-weight-bold rce-tray-id" },
            { title: "Nº Dcto." },
            { title: "Nombre Paciente" },
            { title: "Fecha" },
            { title: "Ámbito" },
            { title: "Tipo" },
            { title: "Motivo" },
            { title: "Profesional" },
            { title: "Estado", className: "text-center" },
            { title: "Prioridad", className: "text-center" },
            { title: "", class: "text-center" }
        ],
        columnDefs: [
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {

                    let fila = meta.row;
                    let botones = `
                    <button class='btn btn-primary btn-circle' 
                        onclick='toggle("#div_accionesST${fila}"); return false;'>
                        <i class="fa fa-list" style="font-size:15px;"></i>
                    </button>
                        <div id='div_accionesST${fila}' class='btn-container' style="display: none;">
                            <center>
                                <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                                <div class="rounded-actions">
                                    <center>
                    `;
                    //RECIBIR SOLICITUD TRANSFUSION
                    if (adataset[fila][13]) {
                        botones += `<a id='recibirST'
                                    data-id=${adataset[fila][0]}
                                    data-rut="${adataset[fila][1]}"
                                    data-nombrePaciente="${adataset[fila][2]}"
                                    data-fechasolicitud="${adataset[fila][3]}"
                                    data-ambito="${adataset[fila][4]}"
                                    data-procedimiento="${adataset[fila][5]}"
                                    data-motivo="${adataset[fila][6]}"
                                    data-profsolicita="${adataset[fila][7]}"
                                    data-prioridad="${adataset[fila][9]}"
                                    data-accion='recibir'
                                    class='btn btn-warning btn-circle btn-lg'
                                    href='#/'
                                    onclick='linkRecibirST(this)'>
                                        <i class='fas fa-file-import'
                                            data-toggle="tooltip"
                                            data-placement="left"
                                            title="Recepcionar">
                                        </i>
                                    </a>
                                    <br>`;
                    }
                    //PROCESAR SOLICITUD TRANSFUSION
                    if (adataset[fila][16]) {
                        botones += `
                    <a  
                        data-id='${adataset[fila][0]}'
                        data-nombrePaciente="${adataset[fila][2]}"
                        data-accion='Procesar'
                        class='btn btn-danger btn-circle btn-lg load-click'
                        onclick='linkProcesarSolicitudTransfusion(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Procesar Solicitud de Transfusión">
                        <i class="fas fa-forward"></i>
                    </a>
                    <br>
                    `;
                    }
                    //CERRAR SOLICITUD TRANSFUSION
                    if (adataset[fila][14]) {
                        botones += `
                            <a
                                data-id='${adataset[fila][0]}'
                                data-nombrePaciente="${adataset[fila][2]}"
                                data-hemocomponente="${adataset[fila][5]}"
                                data-hemocompCant="${adataset[fila][6]}"
                                data-accion='finalizar'
                                class='btn btn-success btn-circle btn-lg load-click'
                                onclick='linkCerrarST(this);'
                                data-toggle="tooltip"
                                data-placement="left"
                                title="Cerrar Solicitud de Transfusión">
                                <i class="fas fa-check-double"></i>  
                            </a>
                            <br>
                            `;
                    }
                    //EDITAR
                    if (adataset[fila][10]) {
                        botones += `
                        <a
                            data-id='${adataset[fila][0]}'
                            data-idpaciente='${adataset[fila][18]}'
                            class='btn btn-info btn-circle btn-lg load-click'
                            onclick='LinkVerEditarST(this);'
                            data-toggle="tooltip"
                            data-placement="left"
                            title="Editar Solicitud de Transfusión">
                            <i class="fas fa-edit"></i>   
                        </a>
                        <br>
                        `;
                    }
                    //IMPRIMIR
                    if (adataset[fila][11]) {
                        botones += `
                        <a
                            data-id='${adataset[fila][0]}'
                            class='btn btn-info btn-circle btn-lg load-click'
                            onclick='LinkImprimirST(this);'
                            data-toggle="tooltip"
                            data-placement="left"
                            title="Imprimir Solicitud de Transfusión">
                            <i class="fas fa-print"></i>
                        </a>
                        <br>
                        `;
                    }
                    //VER MOVIMIENTOS
                    if (adataset[fila][12]) {
                        botones += `
                        <a
                            data-id='${adataset[fila][0]}'
                            class='btn btn-info btn-circle btn-lg load-click'
                            onclick='LinkVerMovimientosST(this);'
                            data-toggle="tooltip"
                            data-placement="left"
                            title="Ver Movimientos de ST">
                            <i class="fas fa-list"></i>
                        </a>
                        <br>
                        `;
                    }
                    //ANULAR SOLICITUD TRANSFUSION
                    if (adataset[fila][15]) {
                        botones += `
                    <a  
                        data-id=${adataset[fila][0]}
                        data-rut="${adataset[fila][1]}"
                        data-nombrePaciente="${adataset[fila][2]}"
                        data-fechasolicitud="${adataset[fila][3]}"
                        data-ambito="${adataset[fila][4]}"
                        data-hemocomponente="${adataset[fila][5]}"
                        data-hemocompCant="${adataset[fila][6]}"
                        data-profsolicita="${adataset[fila][7]}"
                        data-tipoestadosistema="${adataset[fila][8]}"
                        data-accion='anular'
                        class='btn btn-danger btn-circle btn-lg load-click'
                        onclick='linkRecibirST(this);'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Anular Solicitud de Transfusión">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    <br>
                    `;
                    }

                    botones += `
                            </center >
                            </div >
                        </center >
                    </div >
                    `;
                    return botones;
                }
            },
            {
                "targets": "7,8,9,10",
                "visible": false
            }, {
                //Render de estados de la transfusion
                "targets": -3,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones = ``, color = "", titulo = "", letra = ""
                    switch (adataset[fila][8]) {
                        case 123:
                            color = `primary`
                            titulo = `solicitada`
                            letra = "S"
                            break
                        case 124:
                            color = `success`
                            titulo = `recepcionada`
                            letra = "R"
                            break
                        case 125:
                            color = `secondary`
                            titulo = `procesada`
                            letra = "P"
                            break
                        case 126:
                            color = `dark`
                            titulo = `finalizada`
                            letra = "F"
                            break
                        case 127:
                            color = `danger`
                            titulo = `anulada`
                            letra = "A"
                            break
                    }
                    botones = `<button class="btn 
                                        btn-outline-${color} btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Transfusión ${titulo}">
                                        <b>${letra}</b>
                                        </button>`
                    return botones;
                }
            }, {
                //render de las prioridades de la transfusion
                targets: -2,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let prioridad = ``, icono = ``
                    switch (adataset[fila][17]) {
                        case 1://Inmediata
                            color = `danger`
                            icono = `<i class="fa fa-exclamation-triangle latidos" style="color:red;" aria-hidden="true"></i>`
                            break
                        case 2://Urgente
                            color = `warning`
                            icono = `<i class="fa fa-info-circle" style="color:orange;" aria-hidden="true"></i>`
                            break
                        case 3://No urgente
                            color = `info`
                            icono = `<i class="fa fa-clock" style="color:green;" aria-hidden="true"></i>`
                            break
                        case 4://reserva
                            icono = `<i class="fa fa-book" style="color:blue;" aria-hidden="true"></i>`
                            break
                        default:
                            return `N/A`
                            break
                    }
                    prioridad = `
                        <span>${icono}</span><br/>
                        <span>${adataset[fila][9]}</span
                    `
                    return prioridad
                }
            },
            //ocultar columnas en responsivo.
            { responsivePriority: 1, targets: 2 },
            { responsivePriority: 2, targets: 10 },
            { responsivePriority: 3, targets: 1 },
            { responsivePriority: 4, targets: 9 },
            { responsivePriority: 5, targets: 3 },
            { responsivePriority: 6, targets: 6 },
            { responsivePriority: 7, targets: 8 },
            { responsivePriority: 8, targets: 5 },
            { responsivePriority: 9, targets: 4 },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //if (aData[8] == 125) {
            //    $('td', nRow).css('background-color', '#dff0d8');//verde
            //} else if (aData[8] == 124) {
            //    $('td', nRow).css('background-color', '#F4EB5D');//amarillo
            //}
        },
        bDestroy: true
    });

}

function procesarSolicitudTransfusion() {
    let tipoProcedimiento = $("#txtProcedimientoSolicitud").text()
    let id = $('#idProcesarST').val(), json = {}
    let urlProcesar =`${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Procesar/`
    
   
    if (validarCampos("#divInputProcesar", false)) {
        if (tipoProcedimiento == "Transfusión") {
            if (validarCampos("#ListaExamenes", true)) {
                //cuando es transfusion
                urlProcesar += `Transfusion`
                let arrayCantidadTransfundida = []
                let arrayExamenesTransfusion = []
                //Busca los input donde se ingresan las cantidades transfundidas
                let inputCantidadTransfundida = document.getElementsByClassName("cantidad-transfundida")
                //transformar de htmlcolection to array
                arrayCantidadTransfundida = [...inputCantidadTransfundida]
                //Trabajando aca

                let examenesTransfusion = document.getElementsByClassName(" examen-transfusion")
                arrayExamenesTransfusion = [...examenesTransfusion]

                arrayExamenesApi = arrayExamenesTransfusion.map(input => {
                    return {
                        IdCarteraArancel: parseInt($(input).attr("id").split("-")[1]),
                        Valor: parseInt($(input).val())
                    }
                })

                console.info(arrayExamenesApi)

                let arregloApi = arrayCantidadTransfundida.map(input => {
                    return {
                        Id: parseInt($(input).attr("id").split("-")[1]),
                        CantidadTransfundida: parseInt(input.value)
                    }
                    input.value = "" ? valido = false : valido = true
                })
                let reaccionAdversa = $("#checkReaccionAdversa").bootstrapSwitch("state")

                json = {
                    IdGrupoSanguineo: parseInt($('#sltGrupoSanguineoProcesar').val()),
                    Observacion: $('#procesarObservacion').val(),
                    Hemocomponentes: arregloApi,
                    Reaccion: reaccionAdversa,
                    Examenes:arrayExamenesApi
                  }
            }else {
               toastr.error("Faltan campos obligatorios")
            }
            
        } else {
            //cuando es sangria 
            urlProcesar += `Sangria`
            json = {
                Observacion: $('#procesarObservacion').val(),
                SesionesRealizadas: parseInt($('#txtCantidadRealizadas').val()),
                CantidadSesionesRealizadas: parseInt($('#txtCantidadRealizadasSesion').val()),
            }
        }

        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(json),
            contentType: "application/json",
            url: urlProcesar,
            success: function (data, status, jqXHR) {
                
                cargarBandejaST(sSession);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: `Solicitud procesada`,
                    text: `La solicitud de transfusión con ID ${id} ha sido procesada`,
                    showConfirmButton: false,
                    timer: 2500,
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        $("#modalProcesarST").modal('hide')
                    }
                });
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
            }
        });
    } else {
        toastr.error("Se necesita llenar campos obligatorios.")
    }

}


function cerrarSolicitudTransfusion() {
    let id = $('#idSolicitudCierre').val();
    if (validarCampos("#divMotivoCierre", false)) {
        json = {
            Id: id,
            IdTipoCierre: $('#cerrarMotivoCierre').val(),
            Observacion: $('#txtObservacionCierre').val() == "" ? null : $('#txtObservacionCierre').val()
        }

        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(json),
            url: `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}/Cerrar`,
            contentType: "application/json",
            success: function (data, status, jqXHR) {
                cargarBandejaST(sSession);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: `Solicitud cerrada`,
                    text: `La solicitud de transfusión con ID ${id} ha sido cerrada`,
                    showConfirmButton: false,
                    timer: 2500,
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        $("#modalCerrarST").modal('hide')
                    }
                });
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
            }
        });
    } else {
        toastr.error("Se necesita motivo de cierre")
    }

}

function RecibirST() { //Endpoint Cambia estado a Recibido
    console.log($('#tipoAccion').val())
    let tipoAccion = $('#tipoAccion').val()
    let titulo = `La solicitud de transfusión ha sido `
    let id = $('#idSolicitudTransfusion').val()
    let texto = `Solicitud con ID: ${id} ha sido `
    let url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${id}`;
    let method = ""
    let json = {}

    switch (tipoAccion) {
        case "recibir":
            titulo += `recibida`
            texto += `recibida`
            url += "/Recibir"
            method = `PATCH`
            break
        case "finalizar":
            titulo += `finalizada`
            texto += `finalizada`
            url += "/Cerrar"
            method = `PATCH`
            break
        case "anular":
            json.Id = parseInt(id)
            json.MotivoAnulacion = $("#motivoAnulacion").val()
            titulo += `anulada`
            texto += ` anulada`
            url += "/Anular"
            method = `DELETE`
            break
    }
    $.ajax({
        type: method,
        url: url,
        contentType: "application/json",
        data: JSON.stringify(json),
        success: function (data, status, jqXHR) {
            $('#idSolicitudTransfusion').val('0');
            cargarBandejaST(sSession);
            $('#modalRecibirST').modal('hide');
            $('#modalAlerta').modal('hide');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: titulo,
                text: texto,
                showConfirmButton: false,
                timer: 3000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    $("#mdlAsignarEspecialista").modal('hide')
                }
            });
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });

}


function limpiarFiltros() {
    $('#txtSTFiltroId').val('')
    $("#sltTipoIdentificacion").val(1)
    $('#txtSTFiltroRut').val('')
    $('#txtSTFiltroDV').val('')
    $('#txtSTFiltroNombre').val('')
    $('#txtSTFiltroApeParteno').val('')
    $('#txtSTFiltroSApeMaterno').val('')
    $('#sltSTEstado').val(0)
    $('#sltAmbito').val(0)
    $('#txtSTFiltroDV').show()
    $('#separadorDV').show()
    cargarBandejaST();
}