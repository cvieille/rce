﻿function cargarCombosNuevaSolicitud() {
    cargarCombosTipoST();
    cargarCombosMotivo();
    cargarComboPrioridad();
    cargarComboGrupoSanguineo();
    cargarComboAmbito();
}

function cargarCombosBandeja() {
    comboIdentificacion("#sltTipoIdentificacion")
    CargarComboTipoEstadoSistema()
    cargarComboAmbito()
}
const cargarComboAmbito = () => {
    const url = `${GetWebApiUrl()}GEN_Ambito/Combo`;
    setCargarDataEnCombo(url, false, $('#sltAmbito'))
}

function cargarCombosMotivo() {
    const url = `${GetWebApiUrl()}/RCE_Tipo_Motivo_Transfusion/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoMotivoST'))
}
function cargarCombosTipoST() {
    
    const url = `${GetWebApiUrl()}RCE_Tipo_Solicitud_Transfusion/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoST'))
}

function cargarComboPrioridad() {
    const url = `${GetWebApiUrl()}RCE_Tipo_Prioridad_Transfusion/Combo`;
    setCargarDataEnCombo(url, false, "#sltPrioridadST");
}
function cargarComboGrupoSanguineo() {
    const url = `${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`;
    setCargarDataEnCombo(url, false, "#sltGrupoSanguineoBS");
}

function CargarComboTipoEstadoSistema() {
    const url = `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/Transfusion`;
    setCargarDataEnCombo(url, false, $("#sltSTEstado"));
}

function comboIdentificacion(selector) {

    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, r) {
                $(selector).append('<option value="' + r.Id + '">' + r.Valor + '</option>');
            });
            $("#lblTipoIdentificacion").text($(selector).children("option:selected").text());
            $(selector).prepend("<option value='0'>Seleccione</option>");
            $(selector).val('1');
        }
    })
}