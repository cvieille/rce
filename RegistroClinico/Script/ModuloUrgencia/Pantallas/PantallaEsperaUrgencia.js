﻿// Variables globales para almecenar tiempos de espera
let tiempoEsperaAnterior = {
    adultos: {},
    pediatricos: {}
};

$(document).ready(function () {
    iniciarActualizaciones();
});
function iniciarActualizaciones() {
    // Actualiza el reloj
    actualizarReloj();
    setInterval(actualizarReloj, 1000); // Actualiza el reloj cada segundo

    // Actualiza los datos del endpoint periódicamente
    actualizarDatosPantalla(); // Primera llamada inmediata
}
async function actualizarDatosPantalla() {
    await getUrgenciaPantalla();
    setTimeout(actualizarDatosPantalla, 120000); // Reintenta después de 2 minutos
}
async function getUrgenciaPantalla() {
    try {
        const response = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/AtencionesUrgenciaPantalla`,
            contentType: 'application/json',
            dataType: 'json'
        });

        //if (response.data) {
        // Procesar los datos
        obtenerPacientesEspera(response.data);
        obtenerPacientesAtendidos(response.data);
        calcularTiempoEsperaAdultosCadena(response);
        calcularTiempoEsperaPediatricosCadena(response);
        //} else {
        //    console.warn("La respuesta no contiene datos válidos.");
        //    toastr.warning("No se recibieron datos de urgencias. Intenta nuevamente.");
        //}

    } catch (error) {
        console.log(error)
        console.log(JSON.stringify(error));
        console.error("Error al cargar pantalla espera urgencia");
    }
}

function obtenerPacientesEspera(data) {
    if (Array.isArray(data)) {
        const totalAtenciones = data.reduce((acc, atencion) => {
            const estado = atencion.Atencion.Estado.Id;
            const codigoCategorizacion = atencion.Categorizacion ? atencion.Categorizacion.Codigo : null;
            if (estado === 100 || estado === 102) { // Sumar para ambos estados
                if (atencion.Atencion.TipoAtencion.Id === 1) { // Adultos
                    acc.atencionAdulto++;
                } else if (atencion.Atencion.TipoAtencion.Id === 3) { // Pediatricos
                    acc.atencionPediatrica++;
                }

            }

            // CONTAR LOS C
            if (estado === 100 || estado === 102) {
                if (atencion.Atencion.TipoAtencion.Id === 1) { // Adultos
                    if (codigoCategorizacion === "C2") {
                        acc.totalC2++;
                    } else if (codigoCategorizacion === "C3") {
                        acc.totalC3++;
                    } else if (codigoCategorizacion === "C4") {
                        acc.totalC4++;
                    } else if (codigoCategorizacion === "C5") {
                        acc.totalC5++;
                    }
                } else if (atencion.Atencion.TipoAtencion.Id === 3) { // Pediatricos
                    if (codigoCategorizacion === "C2") {
                        acc.totalPedC2++;
                    } else if (codigoCategorizacion === "C3") {
                        acc.totalPedC3++;
                    } else if (codigoCategorizacion === "C4") {
                        acc.totalPedC4++;
                    } else if (codigoCategorizacion === "C5") {
                        acc.totalPedC5++;
                    }
                }
            }

            return acc;
        },
            {
                atencionAdulto: 0,
                atencionPediatrica: 0,

                // TOTALES ADULTOS
                totalC2: 0,
                totalC3: 0,
                totalC4: 0,
                totalC5: 0,
                // TOTALES PED
                totalPedC2: 0,
                totalPedC3: 0,
                totalPedC4: 0,
                totalPedC5: 0
            });

        // Actualizar tarjetas con los totales
        actualizarTarjetasEspera(totalAtenciones.atencionAdulto, totalAtenciones.atencionPediatrica);

        // Adultos
        $('#atendidos-pacientes-c2').text(totalAtenciones.totalC2);
        $('#atendidos-pacientes-c3').text(totalAtenciones.totalC3);
        $('#atendidos-pacientes-c4').text(totalAtenciones.totalC4);
        $('#atendidos-pacientes-c5').text(totalAtenciones.totalC5);
        //Pediatricos
        $('#atendidos-pacientes-ped-c2').text(totalAtenciones.totalPedC2);
        $('#atendidos-pacientes-ped-c3').text(totalAtenciones.totalPedC3);
        $('#atendidos-pacientes-ped-c4').text(totalAtenciones.totalPedC4);
        $('#atendidos-pacientes-ped-c5').text(totalAtenciones.totalPedC5);

    } else {
        console.error("La respuesta no es un array", data);
        // Actualizar tarjetas con los totales
        actualizarTarjetasEspera(0, 0);
    }
}

// Función para actualizar las tarjetas de espera
function actualizarTarjetasEspera(adultos, pediatricos) {
    $('#espera-pacientes').text(adultos);
    $('#espera-pacientes-pediatrica').text(pediatricos);
}

function obtenerPacientesAtendidos(data) {
    if (Array.isArray(data)) {
        const totalAtenciones = data.reduce((acc, atencion) => {
            const estado = atencion.Atencion.Estado.Id;
            if (estado === 103 || estado === 104 || estado === 105) { // suma de Paciente en Box + Paciente en Atencion + Paciente dado de alta
                if (atencion.Atencion.TipoAtencion.Id === 1) {
                    acc.atencionAdulto++;
                    //console.log("ADULTO - Estado:", estado, "Atención:", atencion);
                } else if (atencion.Atencion.TipoAtencion.Id === 3) {
                    acc.atencionPediatrica++;
                    //console.log("PEDIÁTRICO - Estado:", estado, "Atención:", atencion);
                }
            }
            return acc;
        },
            {
                atencionAdulto: 0,
                atencionPediatrica: 0,
            });

        $('#atendidos-pacientes').text(totalAtenciones.atencionAdulto);
        $('#atendidos-pacientes-pediatrica').text(totalAtenciones.atencionPediatrica);
    } else {
        console.error("La respuesta no es un array", data);
        $('#atendidos-pacientes').text(0);
        $('#atendidos-pacientes-pediatrica').text(0);
        $('#atendidos-pacientes-c4').text(0);
    }
}

// ADULTOS
const calcularTiempoEsperaAdultos = (jsonData, categoriaCodigo) => {
    // estados válidos para cada categoría
    const estadosValidosC1 = [100, 102, 103, 104];
    const estadosValidosOtrasCategorias = [102];

    let pacientesCategoria = jsonData.data.filter(paciente =>
        paciente.Categorizacion && paciente.Categorizacion.Codigo === categoriaCodigo &&
        paciente.Atencion.TipoAtencion && paciente.Atencion.TipoAtencion.Id === 1 &&
        (
            (categoriaCodigo === 'C1' && estadosValidosC1.includes(paciente.Atencion.Estado.Id)) ||
            (categoriaCodigo !== 'C1' && estadosValidosOtrasCategorias.includes(paciente.Atencion.Estado.Id))
        )
    );

    // Inicializar el mayor tiempo de espera y el paciente con mayor espera
    let mayorTiempoEsperaMinutos = 0;
    let pacienteMayorEspera = null;

    pacientesCategoria.forEach(paciente => {
        const fechaActual = moment();
        const fechaLlegada = moment(paciente.Atencion.FechaLLegada);

        // Calcular el tiempo de espera en minutos
        const tiempoEsperaMinutos = fechaActual.diff(fechaLlegada, 'minutes');

        // Encontrar el paciente con el mayor tiempo de espera
        if (tiempoEsperaMinutos > mayorTiempoEsperaMinutos) {
            mayorTiempoEsperaMinutos = tiempoEsperaMinutos;
            pacienteMayorEspera = paciente; // Guardar el paciente actual como el de mayor espera
        }
    });

    // Si no hay pacientes en espera, ocultar la categoría correspondiente
    if (!pacienteMayorEspera) {
        $(`.categoria-card.categoria-${categoriaCodigo}`).hide();
        return 0;  // Si no hay pacientes, retornar 0
    }

    // Agregar clase "latido" solo si es categoría C1
    if (categoriaCodigo === 'C1') {
        $(`.categoria-card.categoria-${categoriaCodigo}`).addClass('latido');
    } else {
        $(`.categoria-card.categoria-${categoriaCodigo}`).removeClass('latido');
    }

    // Mostrar la tarjeta de la categoría actual
    $(`.categoria-card.categoria-${categoriaCodigo}`).show();

    // Retornar el tiempo de espera para usar en las categorías siguientes
    return mayorTiempoEsperaMinutos;
};

const calcularTiempoEsperaAdultosCadena = (jsonData) => {
    let tiempoC1 = calcularTiempoEsperaAdultos(jsonData, 'C1');
    let tiempoC2 = calcularTiempoEsperaAdultos(jsonData, 'C2');
    let tiempoC3 = calcularTiempoEsperaAdultos(jsonData, 'C3');
    let tiempoC4 = calcularTiempoEsperaAdultos(jsonData, 'C4');
    let tiempoC5 = calcularTiempoEsperaAdultos(jsonData, 'C5');
    let tiempoSC = calcularTiempoEsperaAdultos(jsonData, 'SC');

    if (tiempoC1 > 0 && tiempoC2 > 0) {
        tiempoC2 += Math.floor(tiempoC1 * 0.30);
    }
    if (tiempoC2 > 0 && tiempoC3 > 0) {
        tiempoC3 += Math.floor(tiempoC2 * 0.30);
    }
    if (tiempoC3 > 0 && tiempoC4 > 0) {
        tiempoC4 += Math.floor(tiempoC3 * 0.30);
    }
    // Sumar el 100% de C4 a C5 si C5 es menor o igual que C4
    if (tiempoC4 > 0 && tiempoC5 > 0 && tiempoC5 <= tiempoC4) {
        tiempoC5 += tiempoC4; // Suma el valor completo de C4 al valor actual de C5
    }

    const updateCategoriaTiempo = (categoria, tiempo) => {
        if (categoria === 'C1' && tiempo > 0) {
            const mensajeCritico = "Paciente crítico en atención";
            $(`.categoria-card.categoria-${categoria} p`).html(mensajeCritico);
            $(`.categoria-card.categoria-${categoria}`).show();
        } else if (tiempo > 0) {
            const horas = Math.floor(tiempo / 60);
            const minutos = tiempo % 60;
            const textoTiempo = `Espera estimada <br> <b>${horas}</b> horas <b>${minutos}</b> minutos <hr>`;

            $(`.categoria-card.categoria-${categoria} p`).html(textoTiempo);
            $(`.categoria-card.categoria-${categoria}`).show();
        } else {
            $(`.categoria-card.categoria-${categoria}`).hide();
        }
    };

    // Actualizar cada categoría
    updateCategoriaTiempo('C1', tiempoC1);
    updateCategoriaTiempo('C2', tiempoC2);
    updateCategoriaTiempo('C3', tiempoC3);
    updateCategoriaTiempo('C4', tiempoC4);
    updateCategoriaTiempo('C5', tiempoC5);
    updateCategoriaTiempo('SC', tiempoSC);
};


// PEDIATRICOS
const calcularTiempoEsperaPediatricos = (jsonData, categoriaCodigo) => {
    // Determinar los estados válidos para cada categoría
    const estadosValidosC1 = [100, 102, 103, 104];
    const estadosValidosOtrasCategorias = [102];

    let pacientesCategoria = jsonData.data.filter(paciente =>
        paciente.Categorizacion && paciente.Categorizacion.Codigo === categoriaCodigo &&
        paciente.Atencion.TipoAtencion && paciente.Atencion.TipoAtencion.Id === 3 &&
        (
            (categoriaCodigo === 'C1' && estadosValidosC1.includes(paciente.Atencion.Estado.Id)) ||
            (categoriaCodigo !== 'C1' && estadosValidosOtrasCategorias.includes(paciente.Atencion.Estado.Id))
        )
    );

    // Inicializar el mayor tiempo de espera y el paciente con mayor espera
    let mayorTiempoEsperaMinutos = 0;
    let pacienteMayorEspera = null;

    pacientesCategoria.forEach(paciente => {
        const fechaActual = moment();
        const fechaLlegada = moment(paciente.Atencion.FechaLLegada);

        // Calcular el tiempo de espera en minutos
        const tiempoEsperaMinutos = fechaActual.diff(fechaLlegada, 'minutes');

        // Encontrar el paciente con el mayor tiempo de espera
        if (tiempoEsperaMinutos > mayorTiempoEsperaMinutos) {
            mayorTiempoEsperaMinutos = tiempoEsperaMinutos;
            pacienteMayorEspera = paciente; // Guardar el paciente actual como el de mayor espera
        }
    });

    // Si no hay pacientes en espera, ocultar la categoría correspondiente
    if (!pacienteMayorEspera) {
        $(`.categoria-card-pediatrica.categoria-${categoriaCodigo}`).hide();
        return 0;  // Si no hay pacientes, retornar 0
    }

    // Agregar clase "latido" solo si es categoría C1
    if (categoriaCodigo === 'C1') {
        $(`.categoria-card-pediatrica.categoria-${categoriaCodigo}`).addClass('latido');
    } else {
        $(`.categoria-card-pediatrica.categoria-${categoriaCodigo}`).removeClass('latido');
    }

    // Mostrar la tarjeta de la categoría actual
    $(`.categoria-card-pediatrica.categoria-${categoriaCodigo}`).show();

    // Retornar el tiempo de espera para usar en las categorías siguientes
    return mayorTiempoEsperaMinutos;
};

const calcularTiempoEsperaPediatricosCadena = (jsonData) => {
    let tiempoC1 = calcularTiempoEsperaPediatricos(jsonData, 'C1');
    let tiempoC2 = calcularTiempoEsperaPediatricos(jsonData, 'C2');
    let tiempoC3 = calcularTiempoEsperaPediatricos(jsonData, 'C3');
    let tiempoC4 = calcularTiempoEsperaPediatricos(jsonData, 'C4');
    let tiempoC5 = calcularTiempoEsperaPediatricos(jsonData, 'C5');
    let tiempoSC = calcularTiempoEsperaPediatricos(jsonData, 'SC');

    // Incrementar el tiempo de espera de cada categoría solo si el tiempo de la categoría anterior es mayor a 0
    if (tiempoC1 > 0 && tiempoC2 > 0) {
        tiempoC2 += Math.floor(tiempoC1 * 0.30);
    }
    if (tiempoC2 > 0 && tiempoC3 > 0) {
        tiempoC3 += Math.floor(tiempoC2 * 0.30);
    }
    if (tiempoC3 > 0 && tiempoC4 > 0) {
        tiempoC4 += Math.floor(tiempoC3 * 0.30);
    }
    // Sumar el 100% de C4 a C5 si C5 es menor o igual que C4
    if (tiempoC4 > 0 && tiempoC5 > 0 && tiempoC5 <= tiempoC4) {
        tiempoC5 += tiempoC4;
    }

    const updateCategoriaTiempo = (categoria, tiempo) => {
        if (categoria === 'C1' && tiempo > 0) {
            const mensajeCritico = "Paciente crítico en atención";
            $(`.categoria-card-pediatrica.categoria-${categoria} p`).html(mensajeCritico);
            $(`.categoria-card-pediatrica.categoria-${categoria}`).show();
        } else if (tiempo > 0) {
            const horas = Math.floor(tiempo / 60);
            const minutos = tiempo % 60;
            const textoTiempo = `Espera estimada <br> <b>${horas}</b> horas <b>${minutos}</b> minutos <hr>`;

            $(`.categoria-card-pediatrica.categoria-${categoria} p`).html(textoTiempo);
            $(`.categoria-card-pediatrica.categoria-${categoria}`).show();
        } else {
            $(`.categoria-card-pediatrica.categoria-${categoria}`).hide();
        }
    };

    // Llamar a la función de actualización para cada categoría pediátrica
    updateCategoriaTiempo('C1', tiempoC1);
    updateCategoriaTiempo('C2', tiempoC2);
    updateCategoriaTiempo('C3', tiempoC3);
    updateCategoriaTiempo('C4', tiempoC4);
    updateCategoriaTiempo('C5', tiempoC5);
    updateCategoriaTiempo('SC', tiempoSC);
};

function actualizarReloj() {
    const reloj = document.getElementById('reloj');

    const horaActual = moment(GetFechaActual()).format("HH:mm:ss");
    const fechaActual = moment(GetFechaActual()).format("DD-MM-YY");

    // Actualizar el reloj con la fecha y hora DESDE SERVIDOR
    reloj.textContent = `${horaActual} (${fechaActual})`;
}
