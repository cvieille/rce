﻿
let sSession, vCodigoPerfil, vIdAtencion, vCategorizacion, vRecategorizar, URG_idCategorizaciones_Atencion, idPaciente;
let bDatosVitales, vEstado, vDiasPaciente, vOpcion, vEpidemiologico, idDiv;
let diagnosticosCIE10;
let datosExportarURG = [];

$(document).ready(function () {

    InicializarComponentes();
    InicializarFiltros();
    InicializarCategorizacion();
    InicializarAlta();
    InicializarDetallesAlergias();

    // Cerrar el modal mdlIndicacionesPendientes cuando se hace clic fuera de él
    $(document).on('click', function (event) {
        if (!$(event.target).closest('.modal-content').length) {
            $('#mdlIndicacionesPendientes').modal('hide');
        }
    });

    // Función para recargar la tabla de urgencias
    function recargarBandejaUrgencia() {
        CargarTablaUrgencia();
    }

    // Variables para controlar la inactividad
    let temporizadorInactividad;
    let tiempoInactividad = 3 * 60 * 1000;

    // Función para verificar inactividad y recargar la bandeja
    function verificarInactividad() {
        recargarBandejaUrgencia();
        iniciarTemporizadorInactividad();
    }

    // Iniciar o reiniciar el temporizador de inactividad
    function iniciarTemporizadorInactividad() {
        clearTimeout(temporizadorInactividad);
        temporizadorInactividad = setTimeout(verificarInactividad, tiempoInactividad);
    }

    // Reiniciar el temporizador en cada movimiento o clic del mouse
    $(document).on('mousemove click', function () {
        iniciarTemporizadorInactividad();
    });

    // Iniciar el temporizador al cargar la página
    iniciarTemporizadorInactividad();
});


function mostrarCategorizacionesPendientes() {

    $("button[data-id-categorizacion]").unbind().on('click', function () {
        limpiarFiltros();
        const idCat = parseInt($(this).attr("data-id-categorizacion"));
        $("#sltEstado").val((idCat === 6) ? 100 : 102);
        $("#sltCategorizacionFiltro").selectpicker("val", idCat);
        CargarTablaUrgencia();
    });

}

function cargarAtencionesPendientes() {

    const idsAtenciones = ($("#sltTipoUrg").selectpicker("val") === [] || $("#sltTipoUrg").selectpicker("val").length === 0) ? [1, 2, 3] : $("#sltTipoUrg").selectpicker("val");

    // Validar si idsAtenciones es null, vacío o contiene un array vacío
    if (!idsAtenciones || idsAtenciones.length === 0) {
        return;  // Si está vacío, no realizar la solicitud
    }

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/PendienteAtencion?idsTipoAtencion=${idsAtenciones}`,
        success: function ({ C1, C2, C3, C4, C5, SC }) {
            $("button[data-id-categorizacion='1']").text(`C1 = ${C1}`);
            $("button[data-id-categorizacion='2']").text(`C2 = ${C2}`);
            $("button[data-id-categorizacion='3']").text(`C3 = ${C3}`);
            $("button[data-id-categorizacion='4']").text(`C4 = ${C4}`);
            $("button[data-id-categorizacion='5']").text(`C5 = ${C5}`);
            $("button[data-id-categorizacion='6']").text(`SC = ${SC}`);
        },
        error: function (jqXHR, status) {
            console.log(`Error al mostrar pendientes: ${JSON.stringify(jqXHR)}`);
        }
    });
}


function validaSiguiente() {

    if (!validarCampos("#divAntecedente_3", false))
        return;

    $("#datos-categorizacion-tab").removeAttr("disabled");
    $("#datos-categorizacion-tab").trigger("click");
    $("#datos-categorizacion-tab").attr("disabled", "disabled");
}

// INICIALIZAR COMPONENTES
function InicializarComponentes() {
    sSession = getSession();
    //RevisarAcceso(false, sSession);
    vCodigoPerfil = sSession.CODIGO_PERFIL;
    $("textarea").val("");
    $("#lnbExportar").hide();
    idDiv = '';

    if (vCodigoPerfil !== perfilAccesoSistema.admisionUrgencia && vCodigoPerfil !== perfilAccesoSistema.digitadorUrgencia)
        $("#btnIndicacionesPendientes").show()

    if (vCodigoPerfil === perfilAccesoSistema.admisionUrgencia ||
        vCodigoPerfil === perfilAccesoSistema.matroneriaUrgencia)
        $('#lblNuevoIngreso').show();

    comboIdentificacion("#sltIdentificacionPaciente", "0");

    inicioTipoIdentificacionEnZeroUrgencia()

    deleteSession("FiltrosBandejaUrgencia")
    $("#sltTipoUrg, #sltCategorizacionFiltro, #sltPrioridad").selectpicker("val", []);
    $("#sltEstado").val("0");
    $("#txtIdtencionesUrgencia, #txtnumeroDocPaciente, #txtDigPaciente, #txtNombrePaciente, #txtApePatPaciente, #txtApeMatPaciente").val("");

    comboPrioridad();
    CargarFiltroAdmision().then(() => {

        MostrarSeleccionTipoAtencion().then(() => {
            CargarTablaUrgencia();
            mostrarCategorizacionesPendientes();
        });
    });
    //67716
    $('#btnAnular').click(function (e) {

        if (validarCampos("#mdlAnularAtencion", false)) {

            let jsonAnular = {
                Observacion: $('#txtMotivoAnulacion').val()
            };

            $.ajax({
                type: 'DELETE',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${vIdAtencion}`,
                contentType: 'application/json',
                data: JSON.stringify(jsonAnular),
                success: function (data) {
                    CargarTablaUrgencia();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Se ha anulado la atención",
                        showConfirmButton: false,
                        timer: 3000
                    });
                    $('#mdlAnularAtencion').modal('hide');
                },
                error: function (jqXHR, status) {
                    console.log(`Error al anular atencion urgencia: ${JSON.stringify(jqXHR)}`);
                }
            });
        }

        e.preventDefault();
    });
    $("#btnUrgFiltro").click(function () {

        deleteSession("FiltrosBandejaUrgencia")
        const _CargarTablaUrgencia = async () => {
            ShowModalCargando(true);
            await sleep(1);
            CargarTablaUrgencia();
        }

        _CargarTablaUrgencia().then(() => ShowModalCargando(false));

    });

    //$("#sltIdentificacionPaciente") $("#txtnumeroDocPaciente")
    ocultarMostrarFichaPac($("#sltIdentificacionPaciente"), $("#txtnumeroDocPaciente"))

    $("#btnUrgLimpiarFiltro").click(function () {
        limpiarFiltros();
        CargarTablaUrgencia();
    });

    $("#aGuardarSignoVital").click(function (event) {
        event.preventDefault();
        GuardarSignosVitales($(this).data("id"));
    });

    $("#sltTipoAtencionSeleccion").on('change', function () {
        deleteSession("FiltrosBandejaUrgencia");
        localStorage.setItem("TIPO_ATENCION", $(this).val());
        $("#sltTipoUrg").selectpicker("val", (localStorage.getItem("TIPO_ATENCION") === "1") ? ['1', '2', '3'] : '2').selectpicker('refresh');
        CargarTablaUrgencia();
    });

}

function limpiarFiltros() {
    deleteSession("FiltrosBandejaUrgencia")
    $("#sltCategorizacionFiltro, #sltPrioridad").selectpicker("val", []);
    $("#sltEstado").val("0");
    $("#txtIdtencionesUrgencia, #txtnumeroDocPaciente, #txtDigPaciente, #txtNombrePaciente, #txtApePatPaciente, #txtApeMatPaciente").val("");
}

function inicioTipoIdentificacionEnZeroUrgencia() {

    const filtroBandeja = $("#btn-FiltroBandejaUrgencia")
    filtroBandeja.click(function () {
        toogleFiltroBandeja(
            {
                filtro: "btn-FiltroBandejaUrgencia",
                identificacion: "sltIdentificacionPaciente",
                rut: "txtnumeroDocPaciente",
                dv: "txtDigPaciente",
                guionDv: "guionDigitoUrg"
            })
    })
}
function InicializarFiltros() {

    $("#txtnumeroDocPaciente").blur(async function () {

        // SI ES RUT O RUT MATERNO
        if ($("#sltIdentificacionPaciente").val() === '1' || $("#sltIdentificacionPaciente").val() === '4') {
            let dig = await ObtenerVerificador($(this).val());
            (dig !== null) ? $(`#txtDigPaciente`).val(dig) : $(`#txtDigPaciente, #txtnumeroDocPaciente`).val("");
        }
    });

    $("#selTipoIdentificacion").change(function () {

        $(`label[for='${$(this).attr("id")}']`).text($(this).children("option:selected").text());
        var parent = $(this).parent().parent().parent();
        ($(this).val() == '1' || $(this).val() == '4') ? $(`#${parent.attr("id")} .digito`).show() : $(`#${parent.attr("id")} .digito`).hide();

        $("#txtnumeroDocPaciente, #txtDigPaciente").val("");

    });
    comboTipoEstado();
    comboTipoClasificacion(null, `sltClasificacionFiltro`);

}

function InicializarCategorizacion() {
    $('#btnRecategorizarC2').click(function (e) {
        vRecategorizar = true;
        categorizar(2);
        e.preventDefault();
    });
    $('#btnRecategorizarC3').click(function (e) {
        vRecategorizar = true;
        categorizar(3);
        e.preventDefault();
    });
    $('#btnRecategorizarC4').click(function (e) {
        vRecategorizar = true;
        categorizar(4);
        e.preventDefault();
    });
    $('#btnRecategorizarC5').click(function (e) {
        vRecategorizar = true;
        categorizar(5);
        e.preventDefault();
    });
    $("#btnRefrescar").click(function (e) {
        CargarTablaUrgencia();
    });
    $('#btnSiAmenaza').click(function (e) {
        $('#divOverlayCat1').show();
        $('#tCat').html('Categorización: C1');
        vCategorizacion = 1;
        $('#tEstimado').html('La atención debe ser inmediata');
        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            // A modo de prueba
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });
    });
    $('#btnNoAmenaza').click(function (e) {
        $('#divOverlayCat1').show();
        $('#item01').slideDown('fast');
        $('#item02').slideDown('fast');
        $('#item03').slideDown('fast');
        $('#divOverlayCat2').hide();
    });
    $('#btnNinguno').click(function (e) {
        $('#divOverlayCat3').show();
        $('#tCat').html('Categorización: C5');
        vCategorizacion = 5;
        $('#tEstimado').html('Paciente categorizado');
        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });
    });
    $('#btnUno').click(function (e) {
        $('#divOverlayCat3').show();
        $('#tCat').html('Categorización: C4');
        vCategorizacion = 4;
        $('#tEstimado').html('Se debe atender dentro de 180 minutos');
        // Reemplazar esto con una llamada a la api
        $('#categorizarPaciente').modal('hide');
        $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
            $('#pacienteCategorizado').modal('show');
            $(e.currentTarget).unbind();
        });
    });
    $('#btnMasdeuno').click(function (e) {
        $('#divOverlayCat3').show();
        $('#divOverlayCat4').hide();
        $('#divOverlayCat6').hide();
    });
    $('#btnAceptarAnte').click(function (e) {
        if (vOpcion == 1) {
            $('#divOverlayCat5').show();
            $('#tCat').html('Categorización: C3');
            vCategorizacion = 3;
            $('#tEstimado').html('Se debe atender dentro de 90 minutos');
            // Reemplazar esto con una llamada a la api
            $('#categorizarPaciente').modal('hide');
            $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
                $('#pacienteCategorizado').modal('show');
                $(e.currentTarget).unbind();
            });
            $('#divCatEnfermero').show();
            $('#divRecategorizarC4').show();
            $('#divRecategorizarC5').show();
        } else if (vOpcion == 2) {
            if ($('#switchEpidemiologicos').prop('checked')) {
                $('#divOverlayCat5').show();
                $('#tCat').html('Categorización: C3');
                vCategorizacion = 3;
                $('#tEstimado').html('Se debe atender dentro de 90 minutos');
                // Reemplazar esto con una llamada a la api
                $('#categorizarPaciente').modal('hide');
                $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
                    $('#pacienteCategorizado').modal('show');
                    $(e.currentTarget).unbind();
                });
                $('#divCatEnfermero').show();
                $('#divRecategorizarC4').show();
                $('#divRecategorizarC5').show();
            } else {
                if ($('#checkRequiereRec').prop('checked')) {
                    $('#tCat').html('Categorización: C4');
                    vCategorizacion = 4;
                    $('#tEstimado').html('Se debe atender dentro de 180 minutos');
                    // Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();
                    });
                } else {
                    $('#tCat').html('Categorización: C5');
                    vCategorizacion = 5;
                    $('#tEstimado').html('Paciente categorizado');
                    // Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();
                    });
                }
            }
        }
        e.preventDefault();
    });
    $('#switchEpidemiologicos').on('switchChange.bootstrapSwitch', function () {
        if (vOpcion == 2) {
            if ($('#switchEpidemiologicos').prop('checked'))
                $('#divOverlayEpidemio').show();
            else
                $('#divOverlayEpidemio').hide();
        }
    });
    $('#btnAceptarDatos').click(function (e) {
        ValidarCategorizacion();
        e.preventDefault();
    });
    $('.clickCat2').mouseup(function (e) {
        setTimeout(function () {
            if ($('#toggle01').children('.active').length > 0 && $('#toggle02').children('.active').length > 0 && $('#toggle03').children('.active').length > 0) {
                $('#divOverlayCat2').show();
                var v1 = $('#toggle01').children('.active').children().attr('cat');
                var v2 = $('#toggle02').children('.active').children().attr('cat');
                var v3 = $('#toggle03').children('.active').children().attr('cat');
                if (v1 == '1' && v2 == '1' && v3 == '2') {
                    if (vDiasPaciente > 1095) {
                        $('#divOverlayCat3').hide();
                    } else {
                        $('#divOverlayCat3').show();
                        $('#divOverlayCat4').hide();
                        $('#divOverlayCat6').hide();
                    }
                } else {
                    $('#tCat').html('Categorización: C2');
                    vCategorizacion = 2;
                    $('#tEstimado').html('Se debe atender dentro de 30 minutos');
                    //Reemplazar esto con una llamada a la api
                    $('#categorizarPaciente').modal('hide');
                    $("#categorizarPaciente").on('hidden.bs.modal', function (e) {
                        // A modo de prueba
                        $('#pacienteCategorizado').modal('show');
                        $(e.currentTarget).unbind();
                    });
                }
            }
        }, 100);
    });
    $('#btnNuevo').click(function (e) {
        $(`#txtTemp`).attr("data-required", false);
        $("#item01, #item02, #item03, #divOverlayCat1, #divEpidemio").hide();
        $("#divOverlayCat2, #divOverlayCat3, #divOverlayCat4, #divOverlayCat5, #divOverlayEpidemio").show();
        $("#toggle00, #toggle01, #toggle02, #toggle03, #toggle04").children().removeClass('active');
        $("#txtSat, #txtFR, #txtFC, #txtTemp").removeClass('is-invalid');
        $("#txtSat, #txtFR, #txtFC, #txtTemp").val('');
        $("#checkRequiereRec").prop('checked', true);
        $("#checkNoRequiereRec").prop('checked', false);
        $("#switchEpidemiologicos").bootstrapSwitch('state', true);
        vEpidemiologico = false;
        $("#categorizarPaciente").animate({
            scrollTop: $('#catC1').offset().top
        }, 250);
        e.preventDefault();
    });
    $('#btnCancelarCategorizacion').click(function (e) {
        $("#pacienteCategorizado").modal('hide');
        $("#categorizarPaciente").modal('show');
        $('#btnNuevo').trigger("click");
        $('#divCatEnfermero').hide();
        e.preventDefault();
    });
    $('#btnAceptarCategorizacion').click(function (e) {
        categorizar(vCategorizacion);
        e.preventDefault();
    });
}


function InicializarDetallesAlergias() {
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").on('switchChange.bootstrapSwitch', function (event, state) {
        const id = $(this).attr("id");

        if (id === "rdoAlergiaSi" && $(this).bootstrapSwitch('state')) {
            $("#divAntecedente_3").show();
            $("#txtAntecedente_3").attr("data-required", true);
        } else if ((id === "rdoAlergiaNo" && $(this).bootstrapSwitch('state'))
            || (id === "rdoAlergiaDesconocido" && $(this).bootstrapSwitch('state'))) {
            $("#divAntecedente_3").hide();
            $("#txtAntecedente_3").attr("data-required", false);
        }

        ReiniciarRequired();
    });

    $("#divDetalleAlergias").hide();
}

async function MostrarSeleccionTipoAtencion() {

    // Eliminar el valor almacenado en localStorage para poder probar
    //localStorage.removeItem("TIPO_ATENCION");

    switch (sSession.CODIGO_PERFIL) {
        case 25:
            $("#sltTipoUrg").selectpicker("val", '2');
            break;
        case 1: case 17: case 21: case 22: case 23:
            if (localStorage.getItem("TIPO_ATENCION") == null || localStorage.getItem("TIPO_ATENCION") == undefined) {

                const { value: tipoAtencion } = await Swal.fire({
                    title: 'Seleccione Tipo de atención',
                    input: 'radio',
                    allowOutsideClick: false,
                    inputOptions: {
                        '1': 'Urgencia General',
                        '2': 'Urgencia Gineco-obstétrico'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'Debe seleccionar una opción';
                        }
                    }
                });

                localStorage.setItem("TIPO_ATENCION", tipoAtencion);
            }

            $("#sltTipoUrg").selectpicker("val", (localStorage.getItem("TIPO_ATENCION") === "1") ? ['1', '2', '3'] : '2');
            $("#sltTipoAtencionSeleccion").val(localStorage.getItem("TIPO_ATENCION"));
            $("#divTipoAtencionSeleccion").show();

            break;
    }
    $("#divTipoAtencionSeleccion").hide();
}

function setSelectedTipoUrgencia(element) {
    document.querySelectorAll('input.swal-urgencia-opcion').forEach(btn => {
        btn.classList.remove('selected', 'btn-danger');
    });

    element.classList.add('selected', 'btn-danger');
}

// COMBOS
async function CargarFiltroAdmision() {
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Tipo_Atencion/Combo`,
        success: function (data, status, jqXHR) {
            comboTipoUrgencia(data);
            comboCategorizacionFiltro()
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar campos de filtros: ${JSON.stringify(jqXHR)}`);
        }
    });
}
function comboCategorizacionFiltro() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Categorizacion/Combo`,
        success: function (data, status, jqXHR) {
            $.each(data, function (i, r) {

                var option = "";

                switch (r.Valor) {
                    case "C1":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-danger badge-count'>C1</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                        break;
                    case "C2":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-orange badge-count'>C2</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                        break;
                    case "C3":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-warning badge-count'>C3</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                        break;
                    case "C4":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-success badge-count'>C4</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                        break;
                    case "C5":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-info badge-count'>C5</span>" 
                        value='${r.Id}'>
                        ${r.Valor}
                    </option>`;
                        break;
                    case "S/C":
                        option =
                            `<option data-content="<span class='badge badge-pill badge-dark badge-count'>S/C</span>" 
                            value='${r.Id}'>
                            ${r.Valor}
                        </option>`;
                        break;
                }

                $("#sltCategorizacionFiltro").append(option);
            });

            $('#sltCategorizacionFiltro').selectpicker('refresh');
            $('#sltCategorizacionFiltro').on('change', () => {

                $('#sltCategorizacionFiltro option').each((index, item) => $(item).removeAttr("disabled"));
                const arrayCat = $('#sltCategorizacionFiltro').selectpicker('val');

                if (arrayCat.includes("6")) {

                    $('#sltCategorizacionFiltro option').each((index, item) => {
                        if ($(item).attr("value") != "6")
                            $(item).attr("disabled", "disabled");
                    });

                } else if (arrayCat.length > 0)
                    $('#sltCategorizacionFiltro option[value="6"]').attr("disabled", "disabled");

                $('#sltCategorizacionFiltro').selectpicker('refresh');

            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar campos de filtros: ${JSON.stringify(jqXHR)}`);
        }
    });
}

// Cargar Tabla Urgencia
function iniciarFiltros() {

    //los filtros de busqueda de la bandeja de urgencia se guardan en la sesion segun requerimientos
    $("#avisoFiltros").empty()
    //Obtener filtros que se guardan en la sesion
    let niSession = getSession();
    let posiblesFiltros = niSession.FiltrosBandejaUrgencia
    let textFiltros = "", filtros
    //Existen filtros almacenados
    if (posiblesFiltros && posiblesFiltros.length > 0) {

        let separador = ""
        textFiltros += "<span class='alert alert-warning'> <b><i class='fa fa-info-circle fa-lg'> </i>&nbsp;Filtrando por"
        $("#divFiltroUrg").addClass('show')
        posiblesFiltros.map((a, index) => {
            //Separador de palabras
            index == posiblesFiltros.length - 1 ? separador = "." : separador = ","
            let nombre = " " + a.Nombre
            textFiltros += nombre + separador
            //Setear valores almacenados en la sesion  en los inputs 
            if (a.Clase == 'selectpicker') {
                $("#" + a.Id).selectpicker('val', a.Valor)
            } else {
                $("#" + a.Id).val(a.Valor)
            }
        })
        filtros = true
        textFiltros += "</b></span>"
        $("#avisoFiltros").append(textFiltros)

    } else {
        //No hay filtros de busqueda almacenados en la sesion
        filtros = false
        $("#avisoFiltros").empty()
    }

    pintarFiltros(filtros);

}
function pintarFiltros(filtros) {

    //Obtener inputs de filtro de busqueda bandeja urgencia
    let input = $("#divFiltroUrg").find('input')
    let selects = $("#divFiltroUrg").find('select')
    valoresInputs = input.map((index, e) => {
        if (e.value != "" && e.value != 0 && e.value != '<empty string>' && e.value != undefined) {
            return { Id: e.id, Nombre: e.name, Valor: e.value, Clase: $(e).attr('class') }
        }
    })
    valoresSelect = selects.map((index, e) => {
        //Omitir el tipo identificacion, porque viene con carga, no tiene el valor <seleccione>
        if ($(e).attr("name") != "Identificacion") {
            if (e.value != "" && e.value != 0 && e.value != '<empty string>' && e.value != undefined) {
                return { Id: e.id, Nombre: $(e).attr("name"), Valor: $(e).val(), Clase: $(e).attr('class') }
            }
        }
    })
    //Concatenar filtros de input con filtros de select
    let valoresFinales = [...valoresInputs].concat([...valoresSelect])
    //Si no hay filtros aun en el local storage, pero si se han ingresado filtros,//
    //se usan los filtros del arreglo  que se envia a la local storage.-----------//
    //Esto soluciona que no se puede acceder a los filtros almacenados en---------//
    //la local storage a menos que la pagina se recargue--------------------------//
    if (filtros == false) {
        $("#avisoFiltros").empty()
        textFiltros = ""
        let nombre = ""
        //accediendo al arreglo que se envió a la sesion
        valoresFinales.map((a, index) => {
            let separador = ""
            textFiltros = "<span class='alert alert-warning'> <b><i class='fa fa-info-circle fa-lg'> </i>&nbsp;Filtrando por"

            index == valoresFinales.length - 1 ? separador = "." : separador = ","
            nombre += " " + a.Nombre + separador
            textFiltros += nombre

        })
        textFiltros += "</b></span>"
        $("#avisoFiltros").append(textFiltros)
    }

    setSession('FiltrosBandejaUrgencia', valoresFinales);

}
function getColumnsTablaUrgencia() {
    return [
        /*0*/ { "data": "Id", "title": "ID", "name": "id" },
        /*1*/ {
            "data": "Paciente",
            "title": "N° Documento",
            "orderable": false,
            "render": (paciente) => {
                const identificacionPaciente = (paciente.Identificacion.Id == 5) ? "NN" : paciente.NumeroDocumento;
                return identificacionPaciente + ((paciente.Digito != null) ? "-" + paciente.Digito : "");
            }
        },
        /*2*/ { "data": "Paciente", "title": "Nombre", "orderable": true, "name": "nombre", "render": (pac) => getNombreCompleto(pac) },
        /*3*/ { "data": "Paciente.FechaNacimiento", "title": "Edad", "name": "edad", "orderable": true, "render": (fechaNac) => fechaNac ? edad(fechaNac) : "" },
        /*4*/ { "data": "FechaAdmision", "title": "Fecha Admisión", "orderable": true, "name": "fechaAdmision" },
        /*5*/ { "title": "Alertas", "className": "text-center", "orderable": false },
        /*6*/ { "data": "MotivoConsulta", "title": "Motivo", "name": "dias", "orderable": false },
        /*7*/ { "data": "Estado", "title": "Estado", "orderable": true, "orderable": false },
        /*8*/ { "data": "Acciones", "title": "Acciones", "className": "text-center", "orderable": false }
    ];
}

function getTiempoTranscurrido(FechaAdmision, TiempoTranscurrido) {

    if (FechaAdmision === undefined || TiempoTranscurrido === undefined)
        return ""

    const [horas, minutos, segundos] = TiempoTranscurrido.split(':')
    const FechaAdmisionFormateada = moment(FechaAdmision).format('DD-MM-YYYY HH:mm')
    return `<div class="card-footer mt-1 border rounded-lg d-flex flex-wrap flex-row justify-content-center">
                <div>
                   ${FechaAdmisionFormateada}
                </div>
                <div class="d-flex flex-wrap flex-row justify-content-center">
                    <i class="fa fa-user-clock mt-2"></i>
                    <b class="mt-2 ml-1" style="font-size:12px;">Tiempo transcurrido:</b>
                    <span class="h6 text-muted mt-2" style="font-size:14px;">
                        ${horas} horas, ${minutos} minutos
                    </span>
                </div>
            </div>
    `
}
function getColumnsRenderTablaUrgencia() {

    let configCategorizacion = {
        heading: "h5",
        badge: "badge-pill"
    }

    return [
        {
            targets: 4,
            render: function (data, type, row, meta) {
                const { FechaAdmision, TiempoTranscurrido } = row;
                return getTiempoTranscurrido(FechaAdmision, TiempoTranscurrido)
            }
        },
        {
            targets: 5,
            render: function (data, type, row, meta) {

                const { Acompañante, Categorizacion, Box, TipoPrioridadPaciente, HayAltaMedica,
                    HayAltaEnfermeria, HayAltaMatroneria, IdTipoAtencion } = row;

                let botones = "";

                // Dibuja Icono de Tipo Atencion
                botones = dibujarIconoTipoAtencion(IdTipoAtencion);

                if (Acompañante == "SI")
                    botones += `<i class="fa fa-user" aria-hidden="true"></i>`;

                if (Categorizacion != null)
                    botones += dibujarIconoCategorizacion(Categorizacion, configCategorizacion);

                if (TipoPrioridadPaciente != null)
                    botones += `<span class="badge btn-warning mb-2" 
                                      style="font-size:15px"
                                      data-toggle='tooltip'
                                      data-placement='left'
                                      title='${TipoPrioridadPaciente.TipoPrioridadPaciente.Valor}'>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Atención Preferencial
                                </span>`;

                if (Box != null)
                    botones += `<h6 class="text-primary">${Box.NombreTipoBox} | ${Box.NombreCama}</h6>`;

                if (HayAltaMedica)
                    botones += `<div class='mb-2'><span class="badge badge-pill btn-success" style='font-size:15px'><i class="fas fa-check"></i> Alta Médica</span></div>`;

                if (HayAltaEnfermeria)
                    botones += `<span class="badge badge-pill btn-success" style='font-size:15px'><i class="fas fa-check"></i> Alta No Médica</span>`;

                if (HayAltaMatroneria)
                    botones += `<span class="badge badge-pill btn-success" style='font-size:15px'><i class="fas fa-check"></i> Alta Matronería</span>`;

                return botones;
            }
        },
        {
            targets: 6,
            data: null,
            orderable: false,
            render: function (data, type, row, meta) {
                const { MotivoConsulta, Categorizacion } = row;
                return `${MotivoConsulta}` + ((Categorizacion != null && Categorizacion.Observaciones != null) ? ` (${Categorizacion.Observaciones})` : "");
            }
        },
        {
            targets: 8,
            data: null,
            orderable: false,
            render: function (data, type, row, meta) {

                const fila = meta.row;
                const { Id, IdTipoAtencion, Paciente, Categorizacion, Box, IdEvento, TipoEstadoSistema, FechaAdmision } = row;
                const { Editar, TomarSignosVitales, Categorizar, EnviarBox, AtencionClinica, DarAlta, Anular, LlamarPaciente, CerrarAtencion } = row.Acciones;
                const nombrePaciente = getNombreCompleto(Paciente);

                const { Id: IdTipoEstado } = TipoEstadoSistema

                let botones =
                    `<button class='btn btn-primary btn-circle' 
                        onclick='toggle("#div_accionesURG${fila}"); return false;'>
                        <i class="fa fa-list" style="font-size:15px;"></i>
                    </button>
                    <div id='div_accionesURG${fila}' class='btn-container' style="display: none;">
                        <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                        <div class="rounded-actions">`;

                // Editable?
                if (Editar) {
                    botones +=
                        `<a id='linkEditar_${fila}' data-id='${Id}' data-count-box='${Box == null ? 0 : 1}'
                            class='btn btn-success btn-circle btn-lg' href='#/' onclick='linkEditar(this)' data-toggle='tooltip'
                            data-placement='left' title='Editar'><i class='fa fa-edit'></i>
                        </a><br />`;
                }

                // Signos vitales
                if (TomarSignosVitales) {
                    botones +=
                        `<a id='linkSignosVitales_${fila}' data-id='${Id}' class='btn btn-primary btn-circle btn-lg'
                            href='#/' onclick='linkSignosVitales(this)' data-toggle="tooltip" data-placement="left"
                            title="Signos vitales" data-pac='${nombrePaciente}' data-categorizacion='${Categorizacion}'>
                            <i class='fa fa-heartbeat'></i>
                        </a><br />`;
                }

                // Categorizable?
                if (Categorizar) {
                    botones +=
                        `<a id='linkCategorizar_${fila}' data-id='${Id}' data-pac='${nombrePaciente}'
                            data-dias='${edadDias(Paciente.FechaNacimiento)}' class='btn btn-info btn-circle btn-lg' href='#/'
                            onclick='linkCategorizar(this)' data-toggle='tooltip' data-placement='left' title='Categorizar'>
                            <i class='fas fa-plus'></i>
                        </a><br />`;
                }

                // Ingresar a box?
                if (EnviarBox) {
                    botones +=
                        `<a id='linkAsignarBox_${fila}' data-id='${Id}' data-idtipoatencion='${IdTipoAtencion}' data-id-estado='${IdTipoEstado}' data-idpaciente=${Paciente.IdPaciente}
                            data-id-categorizacion='${Categorizacion}' data-nombrepaciente='${nombrePaciente}' class='btn btn-warning btn-circle btn-lg' href='#/'
                            onclick='linkAsignarBox(this)' data-toggle='tooltip' data-placement='left' title='Enviar a Box'>
                            <i class="fas fa-procedures"></i>
                        </a><br />`;
                }

                // Atención clínica
                if (AtencionClinica) {
                    botones +=
                        `<a id='linkAtencionClinica_${fila}' data-id='${Id}' data-id-estado='${IdTipoEstado}' class='btn btn-success btn-circle btn-lg'
                            href='#/' onclick='linkAtencionClinica(this)' data-toggle='tooltip'
                            data-placement='left' title='Atención clínica'><i class="far fa-file-alt"></i>
                        </a><br />`;
                }

                // Egreso / Alta
                if (DarAlta) {
                    if (vCodigoPerfil !== perfilAccesoSistema.tensUrgencia) {
                        botones +=
                            `<a id='linkAltaMedica_${fila}' data-id='${Id}' class='btn btn-info btn-circle btn-lg'
                                data-toggle='tooltip' data-placement='left'
                                href='#/' onclick='linkEgresoIngresoUrgenciaDesdeBandeja(${Id})' title='Egresar / Dar alta'>
                                <i class="fas fa-angle-double-right"></i>
                            </a><br />`;
                    }
                }

                // Anulable?
                if (Anular) {
                    botones +=
                        `<a id='linkAnular_${fila}' data-id='${Id}' class='btn btn-danger btn-circle btn-lg'
                            href='#/' onclick='linkAnular(this)' data-toggle='tooltip' data-placement='left' title='Anular'>
                            <i class='fa fa-ban'></i>
                        </a><br />`;
                }

                // VER HISTORIAL HOSPITALIZACIONES DEL PACIENTE
                if (vCodigoPerfil !== perfilAccesoSistema.admisionUrgencia) {
                    botones +=
                        `<a id='linkHistorialHosp' data-idpac='${Paciente.IdPaciente}' data-nompac='${nombrePaciente}'
                        class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkHistorialHosp(this);'
                        data-toggle="tooltip" data-placement="left" title="Historial de Hospitalizaciones">
                        <i class="fas fa-history"></i>
                    </a><br />`;
                }


                botones +=
                    `<button id='linkImprimirRce_${Id}' data-id='${Id}' data-idevento=${IdEvento} class='btn btn-info btn-circle btn-lg' type="button"
                        data-print='true' onclick="abrirModalImprimirUc(${Id},'${nombrePaciente}', ${Paciente.NombreSocial !== null ? `'${Paciente.NombreSocial}'` : 'null'}, ${IdEvento}, ${IdTipoEstado})" data-toggle="tooltip" data-placement="left"
                        title="Imprimir DAU">
                        <i class='fas fa-print'></i>
                    </button><br />`;

                //btn para llamar paciente
                if (LlamarPaciente) {
                    botones +=
                        `<a id='linkLlamarPac_${fila}' data-id='${Id}'
                            class='btn btn-warning btn-circle btn-lg' href='#/' onclick="abrirModalLlamarPaciente(${Id}, ${TipoEstadoSistema.Id}, '${Paciente.Nombre} ${Paciente.ApellidoPaterno}', ${Paciente.NombreSocial !== null ? `'${Paciente.NombreSocial}'` : 'null'})"
                            data-toggle="tooltip" data-placement="left" title="Llamar paciente">
                            <i class="fas fa-bullhorn"></i>
                        </a><br />`;
                }

                botones +=
                    `<a id='linkMovimientosIngresoUrg_${fila}' data-id='${Id}'
                        class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkMovimientosIngresoUrgencia(this)'
                        data-toggle="tooltip" data-placement="left" title="Movimientos">
                        <i class="fas fa-list"></i>
                    </a><br />`;

                if (CerrarAtencion) {
                    botones +=
                        `<a id='aCerrarAtencion' data-id='${Id}' data-idpaciente='${Paciente.Id}'
                            data-nuevo='0' class='btn btn-danger btn-circle btn-lg' onclick="cargarDatosModalCierre(this)" href='#/' 
                            data-toggle="tooltip" data-placement="left" title="Cerrar Atención">
                            <i class='fa fa-check'></i>
                        </a><br />`;
                }

                botones +=
                    `<a id='btnComentariosUrgencia_${fila}' data-id='${Id}' data-nombrepaciente='${Paciente.Nombre} ${Paciente.ApellidoPaterno}'
                        class='btn btn-info btn-circle btn-lg' href='#/' onclick='mostrarModalComentarios(this)'
                        data-toggle="tooltip" data-placement="left" title="Comentarios">
                        <i class="fa fa-comments" aria-hidden="true"></i>
                    </a><br />`;

                return botones + "</div></div>";

            }
        },
        { responsivePriority: 1, targets: 0 }, // Id urgencia
        { responsivePriority: 2, targets: 1 }, // rut  paciente
        { responsivePriority: 3, targets: 2 }, // nombre paciente
        { responsivePriority: 4, targets: 8 }, // botonera
        { responsivePriority: 5, targets: 5 }, // alertas
        { responsivePriority: 6, targets: 7 }, // Estado
        { responsivePriority: 7, targets: 6 }, // Motivo consulta
        { responsivePriority: 8, targets: 4 }, // fecha
        { responsivePriority: 9, targets: 3 }, // edad
        { responsivePriority: 10, targets: 8 }, // Hora

    ];

}
function CargarTablaUrgencia() {


    //iniciarFiltros();

    let url =
        `${GetWebApiUrl()}URG_Atenciones_Urgencia/Bandeja?` +
        `idAtencionesUrgencia=${($("#txtIdtencionesUrgencia").val() !== "") ? $("#txtIdtencionesUrgencia").val() : 0}` +
        `&idcategorizacion=${($("#sltCategorizacionFiltro").selectpicker("val") !== null) ? $("#sltCategorizacionFiltro").selectpicker("val") : ""}` +
        `&idtipo=${($("#sltTipoUrg").selectpicker("val") !== null) ? $("#sltTipoUrg").selectpicker("val") : ""}` +
        `&numerodocumento=${($.trim($("#txtnumeroDocPaciente").val()) !== "") ? $.trim($("#txtnumeroDocPaciente").val()) : ""}` +
        `&nombre=${($.trim($("#txtNombrePaciente").val()) !== "") ? $.trim($("#txtNombrePaciente").val()) : ""}` +
        `&apellidopaterno=${($.trim($("#txtApePatPaciente").val()) !== "") ? $.trim($("#txtApePatPaciente").val()) : ""}` +
        `&apellidomaterno=${($.trim($("#txtApeMatPaciente").val()) !== "") ? $.trim($("#txtApeMatPaciente").val()) : ""}` +
        `&idEstado=${($("#sltEstado").val() !== "0") ? $("#sltEstado").val() : null}` +
        `&idTipoClasificacion=${$("#sltClasificacionFiltro").val()}` +
        `&idTipoPrioridad=${($('#sltPrioridad').selectpicker("val") !== null) ? $('#sltPrioridad').selectpicker("val") : ""}`;

    url += `${($("#sltIdentificacionPaciente").val() !== "0") ? `&idIdentificacion=${$("#sltIdentificacionPaciente").val()}` : ""}`

    const columns = getColumnsTablaUrgencia();
    const columnDefs = getColumnsRenderTablaUrgencia();

    cargarDatatableAjax('#tblUrgencia', url, columns, columnDefs);
    $('[data-toggle="tooltip"]').tooltip();

    if (vCodigoPerfil !== perfilAccesoSistema.admisionUrgencia)
        cargarAtencionesPendientes();
}
function dibujarIconoTipoAtencion(idTipoAtencion) {
    let classIcono;
    let letraIcono;

    if (idTipoAtencion == 1) {
        classIcono = "badge badge-pill btn-success badge-count";
        letraIcono = "A";
    }
    else if (idTipoAtencion == 2) {
        classIcono = "badge badge-pill btn-warning badge-count";
        letraIcono = "G";
    }
    else if (idTipoAtencion == 3) {
        classIcono = "badge badge-pill btn-info badge-count";
        letraIcono = "P";
    }

    return `<h5><span id="bgAtencion" 
                class="${classIcono}">${letraIcono}</span></h5>`;
}
function toggle(id) {
    if (idDiv == '') {
        $(id).fadeIn();
        idDiv = id;
    } else if (idDiv == id) {
        $(id).fadeOut();
        idDiv = '';
    } else {
        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }
}
function limpiarCampos() {

    $(`#txtTemp`).attr("data-required", false);
    $("#alertSignosVitales, #divOverlayCat1, #divOverlayCat5, #divOverlayCat6, #item01, #item02, #item03, #divEpidemio").hide();
    $("#catC1, #catC2, #catC3, #catC4, #divOverlayCat2, #divOverlayCat3, #divOverlayCat4, #divOverlayEpidemio").show();
    $("#toggle00, #toggle01, #toggle02, #toggle03, #toggle04").children().removeClass('active');
    $("#checkRequiereRec").prop('checked', true);
    $("#checkNoRequiereRec").prop('checked', false);
    $("#switchEpidemiologicos").bootstrapSwitch('state', true);
    vEpidemiologico = false;
    $("#txtSat, #txtFR, #txtFC, #txtTemp").removeClass('is-invalid');
    $("#txtSat, #txtFR, #txtFC, #txtTemp").val('');

}
function ActualizarAtencionUrgencia(json) {

    var esValido = false;

    $.ajax({
        type: "PUT",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.URG_idAtenciones_Urgencia}`,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            esValido = true;
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
        }
    });

    return esValido;

}

// CATEGORIZAR
function ValidarCategorizacion() {

    let bValido = true;

    //if ($('#txtFR').val() == null || $('#txtFR').val() == undefined || $('#txtFR').val() == '' || $('#txtFR').val() < 5 || $('#txtFR').val() > 100) {
    //    $('#txtFR').addClass('is-invalid', true);
    //    bValido = false;
    //}

    //if ($('#txtFC').val() == null || $('#txtFC').val() == undefined || $('#txtFC').val() == '' || $('#txtFC').val() < 0 || $('#txtFC').val() > 200) {
    //    $('#txtFC').addClass('is-invalid', true);
    //    bValido = false;
    //}

    //if ($('#txtSat').val() == null || $('#txtSat').val() == undefined || $('#txtSat').val() == '' || $('#txtSat').val() < 50 || $('#txtSat').val() > 100) {
    //    $('#txtSat').addClass('is-invalid', true);
    //    bValido = false;
    //}

    if (!validarCampos("#divSignosVitalesCatUrg", false)) {
        toastr.error("Debe ingresar valores válidos");
        return
    }

    if (validarCamposRegex("#divSignosVitalesCatUrg", false)) {
        toastr.error("Debe ingresar valores válidos");
        return
    }

    if (bValido) {
        bDatosVitales = true;
        if (vDiasPaciente <= 91) {//hasta 3 meses

            //if ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '' || $('#txtTemp').val() < 20 || $('#txtTemp').val() > 60) {
            //    $('#txtTemp').addClass('is-invalid', true);
            //    bValido = false;
            //}

            if (bValido) {
                if ($('#txtFR').val() >= 50 || $('#txtFC').val() >= 180 || $('#txtSat').val() < 92 || $('#txtTemp').val() >= 38) {
                    DarResultadoCategorizacion({
                        IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                    });
                } else {
                    DarResultadoCategorizacion({
                        IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                    });
                }
            }

        } else if (vDiasPaciente >= 92 && vDiasPaciente <= 1095) {//3 meses - 3 años 

            // Paciente entre 3 meses a 3 años
            // C2
            if ($('#txtFR').val() >= 40 || $('#txtFC').val() >= 160 || $('#txtSat').val() < 92) {
                $('#divOverlayCat6').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });
            }
            // C3
            if ($('#txtFR').val() < 40 && $('#txtFC').val() < 160 && $('#txtSat').val() >= 92) {
                if ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '' || $('#txtTemp').val() < 20 || $('#txtTemp').val() > 60) {
                    $('#txtTemp').addClass('is-invalid', true);
                } else {
                    if ($('#txtTemp').val() >= 39) {
                        vEpidemiologico = true;
                        $('#divEpidemio').slideDown('fast');
                        $('#divOverlayCat6').show();
                        $('#divOverlayCat5').hide();
                        vOpcion = 1;
                    }
                    else {
                        vEpidemiologico = true;
                        $('#divEpidemio').slideDown('fast');
                        $('#divOverlayCat6').show();
                        $('#divOverlayCat5').hide();
                        vOpcion = 2;
                    }
                }
            }
            // 3 años 8 años
        } else if (vDiasPaciente >= 1096 && vDiasPaciente <= 2920) {

            if ($('#txtFR').val() >= 30 || $('#txtFC').val() >= 140 || $('#txtSat').val() < 92) {
                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });
            } else {
                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });
            }

        } else if (vDiasPaciente >= 2921) {

            // Mayor a 8 años
            if ($('#txtFR').val() < 20 && $('#txtFC').val() < 100 && $('#txtSat').val() >= 92) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });

            } else if (($('#txtFR').val() >= 21 && $('#txtFR').val() <= 24) && ($('#txtFC').val() >= 101 && $('#txtFC').val() <= 119) && ($('#txtSat').val() >= 88 && $('#txtSat').val() <= 91)) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });

            } else if ($('#txtFR').val() >= 25 || $('#txtFC').val() >= 120 || $('#txtSat').val() < 88) {

                $('#divOverlayCat4').show();
                DarResultadoCategorizacion({
                    IdTipoCategorización: 2, TipoCategorización: "C2", TiempoEstimado: 'Se debe atender dentro de 30 minutos'
                });

            } else {

                DarResultadoCategorizacion({
                    IdTipoCategorización: 3, TipoCategorización: "C3", TiempoEstimado: 'Se debe atender dentro de 90 minutos'
                });
                $('#divCatEnfermero').show();
                $('#divRecategorizarC2').show();

            }
        }
    }
}

// TRAYENDO GET DE DCLINICO EN CATEGORIZACION
function cargarInfoCategorizacionConDAU(idPaciente) {
    // Obtener la información del paciente del nuevo endpoint
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/DClinico`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            // Pintar el valor de Alergias en el textarea correspondiente
            if (data.Alergias) {
                $("#txtAntecedente_3").val(data.Alergias);
                // Si hay alergias, activar el switch
                $("#rdoAlergiaSi").bootstrapSwitch('state', true);
            }

            // Pintar el valor de AntecedentesMorbidos en el campo txtAntecedente_1
            if (data.AntecedentesMorbidos) {
                $("#txtAntecedente_1").val(data.AntecedentesMorbidos);
            } else {
                $("#txtAntecedente_1").val(''); // Limpiar
            }

            // Pintar el valor de AntecedentesQuirurgicos en el campo txtAntecedente_4
            if (data.AntecendetesQxDetalle) {
                $("#txtAntecedente_4").val(data.AntecendetesQxDetalle);
            } else {
                $("#txtAntecedente_4").val(''); // Limpiar
            }

            // Pintar el valor de AntecedentesGinecobstetricos en el campo txtAntecedente_5
            if (data.AntecedentesGinecobstetricos) {
                $("#txtAntecedente_5").val(data.AntecedentesGinecobstetricos);
            } else {
                $("#txtAntecedente_5").val(''); // Limpiar 
            }
        },
        error: function (xhr, status, error) {
            console.error("Error al cargar la información:", error);
        }
    });
}


// ENVIANDO PUT
function actualizarDatosClinicos(idPaciente) {
    let datosClinicos = {
        // Capturar solo los campos necesarios
        AlergiasDetalle: valCampo($("#txtAntecedente_3").val()),
        AntecedentesMorbidos: $("#txtAntecedente_1").val(),
        AntecedentesQuirurgicosDetalle: $("#txtAntecedente_4").val(),
        AntecedentesGinecobstetricos: $("#txtAntecedente_5").val()
    };

    // Realizar el PUT para actualizar los datos clínicos
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/DClinico`,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(datosClinicos),
        success: function (data) {
            console.log("Datos clínicos actualizados exitosamente");
        },
        error: function (e) {
            console.log("Error al actualizar los datos clínicos: " + JSON.stringify(e));
        }
    });
}

function categorizar(categorizacion) {
    if (!validarCampos("#divCategorizacion", false))
        return;

    ShowModalCargando(true);
    $('#btnAceptarCategorizacion').attr("disabled", "disabled");

    let json = {};
    json.IdCategorizacion = categorizacion;
    json.MotivoConsulta = valCampo($("#txtMotivoConsultaCat").val());
    json.Observaciones = valCampo($("#txtObservacionesCategorizacion").val());
    json.IdTipoClasificacion = valCampo(parseInt($("#sltTipoClasificacion").val()));
    json.IdTipoPrioridadpaciente = valCampo(parseInt($("#sltTipoPrioridadPaciente").val()));

    if ($("#rdoAlergiaSi").bootstrapSwitch("state")) {
        json.IdTipoAlternativaAntecedentes = 1;
    } else if ($("#rdoAlergiaNo").bootstrapSwitch("state")) {
        json.IdTipoAlternativaAntecedentes = 2;
    } else if ($("#rdoAlergiaDesconocido").bootstrapSwitch("state")) {
        json.IdTipoAlternativaAntecedentes = 3;
    }

    const arrayAntecedentes = [];
    agregarAntecedentesCategorizacionUrg("#divUrgenciaAntecedentes", arrayAntecedentes);
    agregarAntecedentesCategorizacionUrg("#divUrgenciaCategorizacionAlergia", arrayAntecedentes);

    json.Antecedentes = arrayAntecedentes;

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${vIdAtencion}/Categorizar`;

    if (sSession.CODIGO_PERFIL == perfilAccesoSistema.matroneriaUrgencia) {
        json.IdCategorizacionObstetrico = valCampo(parseInt($("#sltCategorizacionObstetrico").val()));
        url += "/Obstetrico";
    } else {
        json.FrecuenciaCardiaca = null;
        json.FrecuenciaRespiratoria = null;
        json.Saturacion = null;
        json.Temperatura = null;
        json.AntecedentesEpidemiologicos = null;
        json.Recursos = null;

        if (bDatosVitales) {

            json.FrecuenciaCardiaca = $('#txtFC').val();
            json.FrecuenciaRespiratoria = $('#txtFR').val();
            json.Saturacion = $('#txtSat').val();
            json.Temperatura = ($('#txtTemp').val() == null || $('#txtTemp').val() == undefined || $('#txtTemp').val() == '') ? null : $('#txtTemp').val();

            if (vEpidemiologico) {
                json.AntecedentesEpidemiologicos = $('#switchEpidemiologicos').prop('checked') ? 'SI' : 'NO';
                json.Recursos = $('#checkRequiereRec').prop('checked') ? 1 : 0;
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: url,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            $('#pacienteCategorizado').modal('hide');
            $(`#linkImprimirRce_${vIdAtencion}`).trigger("click");
            CargarTablaUrgencia();
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: `El paciente ha sido categorizado`,
                showConfirmButton: false,
                timer: 2000
            });

            // llamamos a la función para hacer el PUT de los antecedentes clínicos
            if (idPaciente) {
                //actualizarDatosClinicos(idPaciente);
            } else {
                console.log("idPaciente no está definido");
            }

            $('#btnAceptarCategorizacion').removeAttr("disabled");
            ShowModalCargando(false);
        },
        error: function (e) {
            console.log("Error al guardar categorizacion: " + JSON.stringify(e));
            $('#btnAceptarCategorizacion').removeAttr("disabled");
            ShowModalCargando(false);
        }
    });
}


function agregarAntecedentesCategorizacionUrg(selector, arrayAntecedentes) {
    $(selector + " textarea").each(function () {
        if ($.trim($(this).val()) !== "") {
            arrayAntecedentes.push({
                IdAntecedente: $(this).attr("data-id-antecedente"),
                DescripcionUrgenciaAntecedente: valCampo($(this).val())
            });
        }
    });
}
function linkCategorizar(e) {

    limpiarErorresCampos("divCategorizacion")
    const id = $(e).data('id');
    CargarInfoCategorizacion(id);

    bDatosVitales = false;
    vIdAtencion = id;

    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) {

        $("#divCategorizacionObstetrico").show();

        $("#datos-categorizacion-tab").removeAttr("disabled");
        $("#datos-categorizacion-tab").trigger("click");
        $("#datos-categorizacion-tab").attr("disabled", "disabled");

        $("#divCategorizacionNormal, #datos-antecedentes-tab, #aSiguienteAntecedentes, #aAtrasCategorizacion").hide();
        comboCategorizacionObstetrico();
        $("#btnCategorizacionObstetrico").unbind().on('click', (e) => {

            if (validarCampos("#divCategorizacionObstetrico", false)) {

                const optSel = $("#sltCategorizacionObstetrico").find(":selected");
                // Estos son los ids que tienen como descripción "Otros C1, C2..."
                switch (parseInt($("#sltCategorizacionObstetrico").val())) {
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                        $("#txtObservacionesCategorizacion").attr("data-required", "true");
                        break;
                    default:
                        $("#txtObservacionesCategorizacion").attr("data-required", "false");
                }

                DarResultadoCategorizacion({
                    IdTipoCategorización: optSel.data("id-Categorizacion"),
                    TipoCategorización: optSel.data("descripcion-categorizacion"),
                    TiempoEstimado: optSel.data("tiempo-estimado")
                });

                ReiniciarRequired();
            }

            e.preventDefault();
        });

    } else {

        $("#divCategorizacionNormal").show();
        $("#divCatEnfermero, #divCategorizacionObstetrico").hide();
        $("#btnNuevo").removeAttr("disabled");
        $("#datos-antecedentes-tab").trigger("click");

        vDiasPaciente = $(e).data('dias');
        if (vDiasPaciente <= 91)//hasta 3 meses
            $('#txtTemp').removeAttr('disabled');
        else if (vDiasPaciente >= 92 && vDiasPaciente <= 1095)//3 meses - 3 años
            $('#txtTemp').removeAttr('disabled');
        else if (vDiasPaciente >= 1096 && vDiasPaciente <= 2920)//3 años 8 años
            $('#txtTemp').attr('disabled', true);
        else if (vDiasPaciente >= 2921)
            $('#txtTemp').attr('disabled', true);

        limpiarCampos();

    }

    establecerRegexSignosVitalesCat(vDiasPaciente)

    // Nombre de Paciente
    $('#lblPacienteCat').html($(e).data('pac'));
    $('#categorizarPaciente').modal('show');
    $("#txtObservacionesCategorizacion").val("");


}

function establecerRegexSignosVitalesCat(vDiasPaciente) {

    const regexFR = /^(?:[5-9]|[1-5][0-9]|60)$/
    const regexFC = /^((0)|1[0-9][0-9]|[1-9][0-9]|[1-9]|(200))$/
    const regexSat = /^(0|[1-9]\d?|100)$/
    const regexTemp = /^(3[0-9]|40|41|42)(\.[0-9]|[0-9]{2})?$/

    //Asignando regex a los campos
    const signosVitales = $("#divSignosVitalesCatUrg input").toArray()
    signosVitales.map(signos => {
        //F. respiratoria
        if (signos.id === 'txtFR') {
            $(`#${signos.id}`).attr("data-regex", regexFR)
            $(`#${signos.id}`).data("regex", regexFR)
            $(`#${signos.id}`).attr("oninput", `validarRegex(this,${regexFR})`)
        }
        //F. Cardíaca
        else if (signos.id === 'txtFC') {
            $(`#${signos.id}`).attr("data-regex", regexFC)
            $(`#${signos.id}`).data("regex", regexFC)
            $(`#${signos.id}`).attr("oninput", `validarRegex(this,${regexFC})`)
        }
        //Saturación
        else if (signos.id === 'txtSat') {
            $(`#${signos.id}`).attr("data-regex", regexSat)
            $(`#${signos.id}`).data("regex", regexSat)
            $(`#${signos.id}`).attr("oninput", `validarRegex(this,${regexSat})`)
        }
        //Temperatura
        else if (signos.id === 'txtTemp' && vDiasPaciente <= 91) {
            $(`#${signos.id}`).attr("data-regex", regexTemp)
            $(`#${signos.id}`).data("regex", regexTemp)
            $(`#${signos.id}`).attr("oninput", `validarRegex(this,${regexTemp})`)
            $(`#${signos.id}`).attr("data-required", true)
            $(`#${signos.id}`).data("required", true)
        }

        if (signos.id !== 'txtTemp') {
            $(`#${signos.id}`).attr("data-required", true)
            $(`#${signos.id}`).data("required", true)
        }
    })
}
function limpiarCategorizacion(IdTipoAtencion) {
    comboTipoClasificacion(IdTipoAtencion, `sltTipoClasificacion`);
    comboTipoPrioridadPaciente();
    $("#divDetalleAlergias").hide();
    $(`#txtMotivoConsultaCat, #txtDetalleAlergias, #txtAntecedentesMorbidos, #txtAntecedentesQx, #txtObservacionesCategorizacion`).val("");
    $(`#sltTipoClasificacion, #sltTipoPrioridadPaciente`).val("0");
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', true);
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('state', false, true);
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('radioAllOff', false);

}


function CargarInfoCategorizacion(idAtencionUrgencia) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            const json = data;
            console.log("JSON", json);

            idPaciente = json.IdPaciente;

            limpiarCategorizacion(json.TipoAtencion.Id);
            $("#txtDetalleAlergias").val("");

            if (json.TipoClasificacion !== null)
                $(`#sltTipoClasificacion`).val(json.TipoClasificacion.Id);

            $('#txtMotivoConsultaCat').val(json.MotivoConsulta);
            $("#txtMotivoConsultaCat").attr("disabled", "disabled");

            mostrarUrgenciaAntecedentes(json.TipoAtencion.Id);
            cargarInfoCategorizacionConDAU(json.IdPaciente);
            cargarInfoUrgenciaTipoPrioridadPaciente(idAtencionUrgencia);
        },
        error: function (err) {
            console.log(`Error al cargar DAU desde carga de categorización: ` + JSON.stringify(err));
        }
    });
}


//function cargarInfoCategorizacionConDAU(idAtencionUrgencia) {

//    $.ajax({
//        type: 'GET',
//        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Antecedentes`,
//        contentType: 'application/json',
//        dataType: 'json',
//        success: function (antecedentes) {

//            antecedentes.forEach((antecedente) => {
//                $(`textarea[data-id-antecedente='${antecedente.ClasificacionAntecedente.Id}']`).val(antecedente.UrgenciaClasificacionAntecedente.Valor);
//            });

//            if ($("#txtAntecedente_3").val() !== "")
//                $("#rdoAlergiaSi").bootstrapSwitch('state', true);

//        }
//    });

//}

function cargarInfoUrgenciaTipoPrioridadPaciente(idAtencionUrgencia) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/TipoPrioridadPaciente`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (urgenciaTipoPrioridadPaciente) {
            $("#sltTipoPrioridadPaciente").val(urgenciaTipoPrioridadPaciente[0].TipoPrioridadPaciente.Id);
        },
        error: function (err) {
            console.log(JSON.stringify(err));
        }
    });

}

function DarResultadoCategorizacion(jsonCategorizacion) {

    $('#tCat').html(`Categorización: ${jsonCategorizacion.TipoCategorización}`);

    vCategorizacion = jsonCategorizacion.IdTipoCategorización;
    $('#tEstimado').html(jsonCategorizacion.TiempoEstimado);
    // 'Se debe atender dentro de 30 minutos'

    // Reemplazar esto con una llamada a la api
    $('#categorizarPaciente').modal('hide');
    $('#categorizarPaciente').on('hidden.bs.modal', function (e) {
        $('#pacienteCategorizado').modal('show');
        $(e.currentTarget).unbind();
    });

}
function mostrarUrgenciaAntecedentes(idTipoAtencion) {

    $("#divUrgenciaAntecedentes").empty();
    $("#divUrgenciaCategorizacionAlergia").empty()

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idTipoAtencion}/Antecedentes/Combo`,
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            sSession = getSession();
            data.forEach((antecedente) => {

                if (antecedente.Id !== 1 && antecedente.Id !== 3) { // 1: Antecedente mórbido, 3: Alergias
                    $("#divUrgenciaAntecedentes").append(
                        `<div class="form-row mt-2">
                            <div id="divAntecedente_${antecedente.Id}" class="col-md-12 mt-3">
                                <strong>${antecedente.Valor}</strong>
                                <textarea id="txtAntecedente_${antecedente.Id}" 
                                          data-id-antecedente='${antecedente.Id}' 
                                          class="form-control" 
                                          placeholder="Escriba detalles" 
                                          rows="3" maxlength="1000"></textarea>
                            </div>
                        </div>`);
                } else {
                    $("#divUrgenciaCategorizacionAlergia").append(`<div class="row mt-1">
                        <div id="divAntecedente_${antecedente.Id}" class="col-lg-12 mt-2">
                            <strong>${antecedente.Valor}</strong>
                            <textarea id="txtAntecedente_${antecedente.Id}" 
                                      data-id-antecedente='${antecedente.Id}' 
                                      class="form-control" 
                                      placeholder="Escriba detalles" 
                                      rows="3" maxlength="1000"></textarea>
                        </div>`);
                }
            });

            // txt de Alergias
            $("#divAntecedente_3").hide();

            if (sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) {
                $('#divAntecedentesAdicionales').contents().appendTo('#divAntecedentesAdicionalesCat');
                //$("#divAntecedentesAdicionalesCat").html($("#divAntecedentesAdicionales").html());
                //$("#divAntecedentesAdicionales").remove();
            }

        },
        error: function (err) {
            console.log(JSON.stringify(err));
        }
    });

}

// EDAD  
function edad(b) {

    var a = moment();
    b = moment(b);
    var years = a.diff(b, 'year');
    b.add(years, 'years');
    var months = a.diff(b, 'months');
    b.add(months, 'months');
    var days = a.diff(b, 'days');
    let response = "";

    if (years > 0) {
        response += `${years} ${(years == 1 ? ' año ' : ' años ')}`;
    } else if (months > 0) {
        response += `${months} ${(months == 1 ? ' mes ' : ' meses ')}`;
    } else if (days > 0) {
        response += `${days} ${(days == 1 ? ' día ' : ' días ')}`;
    } else {
        response += `Recién Nacido`;
    }

    return response;

}
function edadDias(b) {
    var a = moment();
    var days = a.diff(b, 'days');
    return days;
}

// ACCIONES DE TABLA GENERAL
function linkImprimir(id) {
    ImprimirApiExterno(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Imprimir/Simple`)
}

function linkImprimirExamenes(id) {
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}?extens=INDICACIONESCLINICAS`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            showSeleccionTipoExamen(data, id);
        }, error: function (e, r) {
            console.log(`Error al cargar Json Imprimir Urgencia: ${JSON.stringify(e)}`);
        }
    });
}
function showSeleccionTipoExamen(data, id) {

    Swal.fire({
        title: 'Seleccione Tipo de Examenes',
        input: 'radio',
        showCancelButton: true,
        confirmButtonText: 'Imprimir',
        cancelButtonText: "Cancelar",
        inputOptions: {
            'laboratorio': 'Laboratorio',
            'imagenologia': 'Imagenología'
        },
        inputValidator: (value) => {
            if (!value) {
                return 'Debe seleccionar una opción'
            }
        }
    }).then(async (result) => {

        if (result.value) {

            if ((result.value == 'laboratorio' && data.IndicacionesClinicas.ExamenesLaboratorio.length == 0) ||
                (result.value == 'imagenologia' && data.IndicacionesClinicas.ExamenesImagenologia.length == 0)) {

                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'El paciente no posee exámenes registrados.',
                    showConfirmButton: false,
                    timer: 2000
                });

                await sleep(2000);
                showSeleccionTipoExamen(data, id);

            } else {

                const idTipoArancel = (result.value == 'laboratorio') ? 5 : 4;
                ImprimirApiExterno(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/${idTipoArancel}/Imprimir`);

            }
        }
    });
}
function linkAnular(e) {

    var id = $(e).data('id');
    vIdAtencion = id;
    $('#lblAtencionAnular').html(id);
    $('#txtMotivoAnulacion').val("");
    $('#mdlAnularAtencion').modal('show');

}
function linkMovimientosIngresoUrgencia(e) {
    let id = $(e).data("id")
    let titulo = `Urgencia #${id}`
    let _url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Movimientos`

    getMovimientosSistemaGenerico(_url, titulo)
}

function linkEditar(e) {

    const countBox = parseInt($(e).data("count-box"));
    if ((sSession.CODIGO_PERFIL != perfilAccesoSistema.medico) || ((sSession.CODIGO_PERFIL === perfilAccesoSistema.medico || sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) && countBox > 0)) {
        const id = $(e).data('id');
        setSession('ID_INGRESO_URGENCIA', id);
        window.location.replace(`${ObtenerHost()}/Vista/ModuloUrgencia/NuevoIngreso.aspx`);
    } else {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Debe asignar un box para este paciente.',
            showConfirmButton: false,
            timer: 2000
        })
    }
}
function linkAtencionClinica(sender) {

    const idEstado = $(sender).data("id-estado")

    if (idEstado !== 103 && idEstado !== 104 && idEstado !== 105) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Debe asignar un box para este paciente.',
            showConfirmButton: false,
            timer: 2000
        })
    }
    else {
        irAtencionClinica($(sender).data("id"));
    }
}
function irAtencionClinica(id) {

    localStorage.setItem("ES_ATENCION_CLINICA", true);
    setSession('ID_INGRESO_URGENCIA', id);
    window.location.replace(`${ObtenerHost()}/Vista/ModuloUrgencia/AtencionClinica.aspx`);
}
async function linkEgresoIngresoUrgenciaDesdeBandeja(id) {

    const urgencia = GetJsonIngresoUrgencia(id);
    const categorizacionUrgencia = await getCategorizacion(id)
    let objAtencionUrgencia = {
        idAtencion: urgencia.Id,
        idPaciente: urgencia.IdPaciente,
        motivoConsulta: urgencia.MotivoConsulta,
        categorizacion: categorizacionUrgencia,
        tipoAtencion: urgencia.TipoAtencion,
        tipoEstado: urgencia.TipoEstadoSistema
    }


    await linkEgresoIngresoUrgencia(objAtencionUrgencia, true);

}

// ASIGNAR BOX
function linkAsignarBox(e) {
    const PACIENTE_ADMITIDO = 100;
    const idEstado = $(e).data("id-estado");

    if (idEstado === PACIENTE_ADMITIDO) {
        Swal.fire({
            title: 'Paciente no ha sido categorizado.',
            text: '¿Está seguro que quiere continuar?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.value) {
                redireccionarAMapaDeCamas(e);
            }
        });
    } else {
        redireccionarAMapaDeCamas(e);
    }
}

function redireccionarAMapaDeCamas(e) {
    setSession('ID_DAU', $(e).data("id"));
    setSession('ID_PACIENTE', $(e).data("idpaciente"));
    setSession('ID_TIPO_ATENCION', $(e).data("idtipoatencion"));
    window.location.href = ObtenerHost() + "/Vista/ModuloUrgencia/VistaMapaCamasUrgencia.aspx";
}
function AsignarBox(e) {
    nombrePaciente = $(e).data('nombrepaciente');
    comboServicioBox(nombrePaciente);
    $("#modalAlerta").modal("hide");
    $("#aEnviarABox").data("id", $(e).data('id'));
    $("#sltTipoBox").attr("disabled", "disabled").val("0");
    $('#sltNombreBox').attr('disabled', 'disabled').val('0');
    CargarTablaTraslados($(e).data('id'));
    $('#mdlPasaraBox').modal('show');
    $("#aEnviarABox").unbind().click(function () {

        let host = ObtenerHost();
        host += "/Vista/ModuloUrgencia/BandejaUrgencia.aspx#/";
        if (validarCampos("#divBox", false)) {
            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${parseInt($("#aEnviarABox").data("id"))}/box/${parseInt($("#sltNombreBox").val())}`,
                contentType: 'application/json',
                data: JSON.stringify({}),
                dataType: 'json',
                success: function (data) {
                    //en urgencia carga la tabla urgencia
                    if (window.location.href == host) {
                        CargarTablaUrgencia();
                    } else {

                        let idUbicacion = $("#inpIdUbicacion").data("id-ubicacion");
                        // en mapa de camas urgencias carga el mapa
                        cargarCamasUrgencia(false, idUbicacion);
                    }
                    $('#mdlPasaraBox').modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'El paciente ha sido enviado al box.',
                        showConfirmButton: false,
                        timer: 3000
                    })


                },
                error: function (jqXHR, status) {
                    console.log(`Error al enviar paciente.`);
                }
            });

        } else {
            console.log("Error validando los campos, hay campos vacíos");
        }

    });

}

// TRASLADOS
function CargarTablaTraslados(id) {

    var adataset = [];
    //ID de la atención de urgencia, Tipo Box, Box y fecha y hora
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/traslados`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {

            $("#lblPasaraBox").text(data.AtencionUrgencia[0].GEN_nombrePaciente);
            $.each(data.Traslados, function (i, r) {
                adataset.push([
                    r.NombreTipoBox,
                    r.NombreBox,
                    moment(r.Fecha).format('DD-MM-YYYY HH:mm:ss')
                ]);
            });

        }
    });

    $('#tblTrasladosUrgencia').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "Tipo box" },
            { title: "Box" },
            { title: "Fecha traslado" }
        ],
        "columnDefs": [
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });

    $('#tblTrasladosUrgencia').css("width", "100%");

}
function GetJsonProfesional(id) {

    var json = null;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            json = {
                "GEN_rutProfesional": data.GEN_rutProfesional,
                "GEN_digitoProfesional": data.GEN_digitoProfesional,
                "GEN_nombreProfesional": data.GEN_nombreProfesional,
                "GEN_apellidoProfesional": data.GEN_apellidoProfesional,
                "GEN_sapellidoProfesional": data.GEN_sapellidoProfesional
            };
        }, error: function (e, r) {
            console.log(`Error al cargar Json Profesional: ${JSON.stringify(e)}`);
        }
    });

    return json;

}

// INDICACIONES
function CargarIndicacionesUrg(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}?extens=INDICACIONESCLINICAS`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            cargarIndicacionesUrg(data);
            $("#mdlIndicaciones").modal('show');
        }
    });
}
function liquidarCuenta(id) {
    let object = getDauRecaudacion(id);
    let dau = new Proxy(object, handler);
    cargarDatosModalRecaudacion(dau);
    $("#mdlRecaudacion").modal("show");
    $("#aRecaudacionProceder").click(function () {
        //Acá una confirmación con SweetAlert
        modalConfirmarLiquidacion(id);
    });
}
function getDauRecaudacion(id) {
    let response = false;
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}`,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            response = data;
        },
        error: function (jqXHR, status) {
            console.log("Error al guardar Egresar, derivar o cerrar: " + JSON.stringify(jqXHR));
        }
    });
    return response;
}
function cargarDatosModalRecaudacion(datos) {
    $("#lblAtencionLiquidar").html(datos.AtencionAdministrativa.IdAtencionUrgencia);
    $("#lblTipoAtencionLiq").html(datos.AtencionAdministrativa.NombreTipoAtencion);
    $("#lblMotivoConsultaLiq").html(datos.AtencionAdministrativa.MotivoConsulta);
    $("#lblPacienteLiq").html(datos.Paciente.Nombre + " " + datos.Paciente.ApellidoPaterno + " " + datos.Paciente.ApellidoMaterno);
    $("#lblPrevisionLiq").html(datos.AtencionAdministrativa.NombrePrevision + " " + datos.AtencionAdministrativa.NombrePrevisionTramo);
}
function modalConfirmarLiquidacion(id) {
    $("#mdlRecaudacion").modal("hide");
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: "Está a punto de Proceder con la Liquidación de la Atención Nro. #" + id,
        text: 'Se recomienda revisar la información antes de Proceder. Puede que la acción no sea reversible. Desea continuar con la acción?',
        showConfirmButton: true,
        confirmButtonText: "Si",
        showCancelButton: true,
        cancelButtonText: "No",
    }).then((result) => {
        if (result.value) {
            hacerLiquidacionAtencion(id);
        } else if (result.dismiss) {
            $("#mdlRecaudacion").modal("show");
        }
    });
}
function hacerLiquidacionAtencion(idAtencion) {
    let json = {
        IdAtencion: idAtencion,
        Observacion: $("#txtObservacionesLiq")
    };
    //Acá se podría agregar modal Cargando
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/Liquidar`,
        contentType: 'application/json',
        async: false,
        dataType: 'json',
        data: JSON.stringify(json),
        success: function (data) {
            CargarTablaUrgencia();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
    //Acá se puede quitar modal "Cargando"
    $("#mdlRecaudacion").modal("hide");
}

function setJsonExportarURG(json) {
    json.forEach((row) => {
        let Object = new Proxy(row, handler);
        datosExportarURG.push([
            Object.Id,
            Object.Paciente.NumeroDocumento + " " + Object.Paciente.Digito,
            Object.Paciente.Nombre + " " + Object.Paciente.ApellidoPaterno + " " + Object.Paciente.ApellidoMaterno,
            Object.Paciente.Edad,
            formatDate(Object.Paciente.FechaNacimiento),
            formatDate(Object.FechaPrimeraAtencion),
            Object.Prevision,
            Object.PrevisionTramo,
            Object.MotivoConsulta,
            Object.Categorizacion,
            Object.Localizacion
        ]);
        //Indices de 0 a 8 fijos...
    });
}


