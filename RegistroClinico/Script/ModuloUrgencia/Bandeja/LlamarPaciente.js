let idAtencionUrgencia = null;
let idTipoEstadoSistemaBrazalete = null;

$(document).ready(function () {

    sSession = getSession();

    $('#btnPrimerLlamado, #btnPrimerLlamadoCategorizado').on('click', function () {
        registrarLlamado('primer');
    });

    $('#btnSegundoLlamado, #btnSegundoLlamadoCategorizado').on('click', function () {
        registrarLlamado('segundo');
    });

    $('#btnTercerLlamado, #btnTercerLlamadoCategorizado').on('click', function () {
        registrarLlamado('tercero');
    });

    $('#btnEgresarPaciente').on('click', function () {
        $('#mdlLlamarPaciente').modal('hide');
    });

});

function abrirModalLlamarPaciente(Id, idTipoEstadoSistema, Nombre, NombreSocial) {

    if (idTipoEstadoSistema !== 100 && idTipoEstadoSistema !== 102)
        return;

    if (idAtencionUrgencia === Id) {
        obtenerLlamados(idTipoEstadoSistema);
        $("#mdlLlamarPaciente").modal("show");
        return;
    }

    limpiarCamposModal();

    idAtencionUrgencia = Id;
    idTipoEstadoSistemaBrazalete = idTipoEstadoSistema;
    nombrePaciente = Nombre;

    $("#tituloModalLlamarPaciente").text(`Llamar al paciente con id: ${Id}`);

    let nombreCompletoPaciente = Nombre;
    if (NombreSocial) {
        nombreCompletoPaciente = `(${NombreSocial}) ${Nombre}`;
    }

    $("#textoModalLlamarPaciente").text(`Paciente: ${nombreCompletoPaciente}`);

    // Cargar el estado del modal
    obtenerLlamados(idTipoEstadoSistema);
    $("#mdlLlamarPaciente").modal("show");

}

function limpiarCamposModal() {

    $("#tituloModalLlamarPaciente").empty();
    $("#textoModalLlamarPaciente").empty();

    $("#spanPrimerLlamado, #spanPrimerLlamadoCategorizado").hide().removeClass('card-footer mt-1 border rounded-lg d-flex flex-wrap flex-row justify-content-center').empty();
    $("#spanSegundoLlamado, #spanSegundoLlamadoCategorizado").hide().removeClass('card-footer mt-1 border rounded-lg d-flex flex-wrap flex-row justify-content-center').empty();
    $("#spanTercerLlamado, #spanTercerLlamadoCategorizado").hide().removeClass('card-footer mt-1 border rounded-lg d-flex flex-wrap flex-row justify-content-center').empty();

    $("#btnPrimerLlamado, #btnPrimerLlamadoCategorizado").prop('disabled', false);
    $("#btnSegundoLlamado, #btnSegundoLlamadoCategorizado").prop('disabled', true);
    $("#btnTercerLlamado, #btnTercerLlamadoCategorizado").prop('disabled', true);

    $("#divBotonEgresarPaciente").hide();

}

function registrarLlamado(tipo) {

    if (!idAtencionUrgencia) return;

    $.ajax({
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/LlamarPaciente`,
        type: 'PATCH',
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            obtenerLlamados(idTipoEstadoSistemaBrazalete);
        },
        error: function (xhr, status, error) {
            console.error('Error al registrar el llamado:', status, error);
        }
    });

}

function obtenerLlamados(idTipoEstadoSistema) {

    ShowModalCargando(true);
    $("#btnPrimerLlamado, #btnSegundoLlamado, #btnTercerLlamado, #btnPrimerLlamadoCategorizado, #btnSegundoLlamadoCategorizado, #btnTercerLlamadoCategorizado").prop('disabled', false);
    $("#divBotonEgresarPaciente").hide();

    if (!idAtencionUrgencia) {
        console.error('ID de atenci�n de urgencia no definido.');
        return;
    }

    $.ajax({
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/LlamarPaciente`,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            
            const btns = ["#btnPrimerLlamado", "#btnSegundoLlamado", "#btnTercerLlamado"]
            const spans = ["#spanPrimerLlamado", "#spanSegundoLlamado", "#spanTercerLlamado"];
            let index = 0, idMovTemp = 0;
            data.forEach(({ Fecha, Movimiento, Usuario }) => {

                if (idMovTemp !== Movimiento.Id) {
                    idMovTemp = Movimiento.Id;
                    index = 0;
                }

                if (index < 3) {
                    const span = (Movimiento.Id === 270) ? spans[index] : `${spans[index]}Categorizado`; 
                    const btn = (Movimiento.Id === 270) ? btns[index] : `${btns[index]}Categorizado`;
                    $(span).show().addClass('card-footer mt-1 border rounded-lg d-flex flex-wrap flex-row justify-content-center').html(
                        `<div class="d-flex flex-column align-items-center">
                            <div>Fecha y Hora: ${ moment(Fecha).format("DD-MM-YYYY HH:mm:ss")}</div>
                            <div>Usuario: ${ Usuario.Nombre} ${Usuario.PrimerApellido} ${ Usuario.SegundoApellido }</div>
                        </div>`
                    );
                    $(btn).prop('disabled', true);
                }

                index++;

            });

            if (idTipoEstadoSistema === 100 && data.filter(c => c.Movimiento.Id === 270).length >= 3) 
                $("#divBotonEgresarPaciente").show();
            else if (idTipoEstadoSistema === 102 && data.filter(c => c.Movimiento.Id === 419).length >= 3)
                $("#divBotonEgresarPaciente").show();
            ShowModalCargando(false);
        },
        error: function (xhr, status, error) {
            //console.error('Error al obtener llamados:', status, error);
            ShowModalCargando(false);
        }
    });

    if (idTipoEstadoSistema === 102)
        $("#divBotoneraLlamarPaciente button").attr("disabled", "disabled");
    else
        $("#divBotoneraLlamarPacienteCategorizado button").attr("disabled", "disabled");


}

