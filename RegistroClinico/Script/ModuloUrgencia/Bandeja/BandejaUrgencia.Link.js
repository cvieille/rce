﻿function irANuevaSolicitudST(boton) {
    deleteSession("ID_SOLICITUD_TRANSFUSION");
    setSession('SISTEMA', 'URGENCIA');
    setSession('ID_ATENCION_URGENCIA', $(boton).data("id"));
    setSession('ID_PACIENTE', $(boton).data("idpaciente"));
    window.location.href = ObtenerHost() + "/Vista/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.aspx";
}
function linkHistorialHosp(event) {
    let id = $(event).data("idpac");
    let nombre = $(event).data("nompac");
    ShowModalHospitalizaciones(id, nombre);
}