﻿$(document).ready(function () {

    const { CODIGO_PERFIL } = getSession();
    if (CODIGO_PERFIL !== perfilAccesoSistema.administradorDeUrgencia)
        $("#btnReversarEstadoDAU").hide();

    $('#btnReversarEstadoDAU').on('click', function () {
        limpiarAlertas();
        $("#txtNumeroDau").val("");
        $("#divAlertaReversarPorDefecto").show();
        $("#mdlReversarDAU").modal("show");
    });

    $('#btnBuscarReversarDAU').on('click', function () {
        buscarDAU($("#txtNumeroDau").val());
    });

    $("#txtNumeroDau").on('input', function () {
        limpiarAlertas();
        $("#divAlertaReversarPorDefecto").show();
    });

    $('#btnAlertaReversarExitoso').on('click', function (evt) {
        evt.preventDefault();
        Swal.fire({
            title: 'Confirmar acción',
            text: "¿Está seguro que quiere revertir el estado de la Atención?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            allowEscapeKey: false,
            allowOutsideClick: false,
        }).then((result) => {
            if (result.value) {
                const idAtencionUrgencia = $(this).data("idAtencion");
                CambiarEstadoADadoDeAlta(idAtencionUrgencia).then(() => {
                    // modal de reubicación después de que el estado cambia
                    Swal.fire({
                        title: 'Reubicar Paciente',
                        text: "¿Desea enviar al paciente a un box?",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Sí',
                        cancelButtonText: 'No',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }).then(async (reubicarResult) => {
                        if (reubicarResult.value) {
                            let idModal = $('#btnAlertaReversarExitoso').data("idAtencion");
                            let json = GetJsonIngresoUrgencia(idModal);

                            ShowModalCargando(true);

                            // Llama a la función para reubicar al paciente
                            await reubicarPacienteEnBox(json.Id);
                            await sleep(200);

                            // Verifica si el valor existe en el select
                            if ($("#sltTipoAtencionReubicarPaciente").find("option[value='" + json.TipoAtencion.Id + "']").length > 0) {
                                // Establecer el valor en el select
                                $("#sltTipoAtencionReubicarPaciente").val(json.TipoAtencion.Id).trigger("change");
                            } else {
                                console.error("El valor TipoAtencion no está en el select.");
                            }

                            ShowModalCargando(false);
                            $("#mdlReubicarSacarPaciente").modal("show");
                        }
                    });
                });
            }
        });
    });
});

async function buscarDAU(idAtencionUrgencia) {
    ShowModalCargando(true);
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}`,
        success: function ({ TipoEstadoSistema }) {

            if (TipoEstadoSistema.Id !== 106) {
                showMensajeError(`N° de DAU <strong>${idAtencionUrgencia}</strong> no está cerrado.`);
            }
            else {

                limpiarAlertas();
                $("#divAlertaReversarExitoso").show();
                $("#divAlertaReversarExitoso span").html(`N° de DAU <strong>${idAtencionUrgencia}</strong> - Click en "Reversar" para cambiar estado de Atención a "Dado de Alta".`);
                $('#btnAlertaReversarExitoso').data("idAtencion", idAtencionUrgencia);
            }
            ShowModalCargando(false);
        },
        error: function (jqXHR, status) {

            if (jqXHR.status === 404)
                showMensajeError(`N° de DAU <strong>${idAtencionUrgencia}</strong> no existe.`);

            console.log(`Error al buscar atencion urgencia: ${JSON.stringify(jqXHR)}`);
            ShowModalCargando(false);

        }
    });
}

async function CambiarEstadoADadoDeAlta(idAtencionUrgencia) {
    ShowModalCargando(true);
    return $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/ReversarEstado`,
        success: function (data) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: "Acción realizada",
                showConfirmButton: false,
                timer: 2000
            });
            limpiarAlertas();
            $("#divAlertaReversarPorDefecto").show();
            ShowModalCargando(false);
            $("#mdlReversarDAU").modal("hide");
        },
        error: function (jqXHR, status) {
            console.log(`Error en reservar estado: ${JSON.stringify(jqXHR)}`)
            ShowModalCargando(false);
        }
    });
}

function limpiarAlertas() {
    $("#divAlertaReversarPorDefecto, #divAlertaReversarExitoso, #divAlertaReversarError").hide();
    $('#btnBuscarReversarDAU').removeData("idAtencion");
}

function showMensajeError(error) {
    limpiarAlertas();
    $("#divAlertaReversarError span").html(error);
    $("#divAlertaReversarError").show();
}
