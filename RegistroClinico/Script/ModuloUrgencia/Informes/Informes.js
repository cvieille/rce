﻿$(document).ready(function () {

    // FECHA PARA INICIAR TRATAMIENTO
    llenarRangoFecha("#fechaInicioInforme", "#fechaFinInforme", 1);

    $("#sltAnio").parent().parent().parent().hide();
    ShowModalCargando(false);
    
    $("#btnBuscarReporte").on('click', () => {
        if ($("#sltListaInformes").val() === "GinecoObstetrico" ||
            validarDiferencia($("#fechaInicioInforme").val(), $("#fechaFinInforme").val()))
            descargarReporte();
    });

    $("#sltListaInformes").on('change', () => {

        if ($("#sltListaInformes").val() === "GinecoObstetrico") {
            $("#fechaInicioInforme").parent().hide();
            $("#fechaFinInforme").parent().hide();
            $("#sltAnio").parent().parent().parent().show();
        } else {
            $("#fechaInicioInforme").parent().show();
            $("#fechaFinInforme").parent().show();
            $("#sltAnio").parent().parent().parent().hide();
        }

    });
});

function validarDiferencia(fechaInicio, fechaFin) {

    const inicio = moment(fechaInicio);
    const fin = moment(fechaFin);

    const diferencia = fin.diff(inicio, 'days');

    if (diferencia >= 0 && diferencia <= 31) {
        return true;
    }

    Swal.fire({
        icon: 'warning',
        title: 'Rango fechas demasiado largo',
        text: 'El rango de las fechas no puede sobrepasar los 31 días.'
    });
    showMensajeAlert(false);
    return false;

}

const descargarReporte = async () => {

    ShowModalCargando(true);

    const nombreInforme = $("#sltListaInformes").val();
    const parametros = ($("#sltListaInformes").val() === "GinecoObstetrico") ? `?mes=${$("#sltMes").val() }&anio=${$("#sltAnio").val()}` : `?fechaInicial=${$("#fechaInicioInforme").val()}&fechaFinal=${$("#fechaFinInforme").val()}`;
    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Informe/${nombreInforme}${parametros}`;
    const nombreArchivo = ($("#sltListaInformes").val() === "GinecoObstetrico")
                            ? `Informe_${nombreInforme}_${$("#sltMes option:selected").text()}_${$("#sltAnio option:selected").text() }.xlsx`
                            : `Informe_${nombreInforme}_${ $("#fechaInicioInforme").val() }_${$("#fechaFinInforme").val()}.xlsx`
    const existeReporte = await descargarReporteEnExcel(url, nombreArchivo);

    showMensajeAlert(existeReporte);
    ShowModalCargando(false);

}

const showMensajeAlert = (existeReporte) => {
    if (existeReporte) {
        $("#divResultadoReporte").attr("class", "alert alert-success text-center m-4");
        $("#divResultadoReporte").html("<i class='fa fa-check'></i> El reporte se ha descargado exitosamente.");
    } else {
        $("#divResultadoReporte").attr("class", "alert alert-warning text-center m-4");
        $("#divResultadoReporte").html("<i class='fa fa-exclamation'></i> No existen datos a mostrar.");
    }
}