﻿
var ingresoUrgencia = {}, profesional = {}, niSession = null;
var medicamentosAtencion = [], tipoGuardado = null;
const TIPO_GUARDADO = { Medico: 1, Admision: 16, AtencionClinica: 23 };
var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
};

$(document).ready(function () {

    // Hay que revisar el acceso a este formulario
    //RevisarAcceso(false, niSession);

    const IniciarComponentes = async () => {

        ShowModalCargando(true);
        ValidarPerfiles();
        CargarCombos();
        CargarIngresoUrgencia();

        await CargarPacienteNN()
    }

    IniciarComponentes().then(() => ShowModalCargando(false));

});

// INICIAR COMPONENTES
function ValidarPerfiles() {

    niSession = getSession();
    InicializarComponentes();
    InicializarDatosPaciente();
    InicializarDatosAdministrativos();

}
function InicializarComponentes() {

    $("textarea").val("");
    ActivarAcompañante();
    $("#txtnumerotPac").on('input', function () {
        $("#sltTipoAtencion").val('0');
    });

    $("#aSiguienteIngresoUrgencia").on('click', () => $("#liDatosAtencionAdm a").trigger('click'));

    $("#aAtrasIngresoUrgencia").on('click', () => $("#liDatosPaciente a").trigger('click'));

}

function InicializarDatosPaciente() {

    var html = $("#divPrevision").html();
    $("#divPrevision").remove();
    $("#divDatosPaciente .card-body").append(`<div id='divPrevision'>${html}</div>`);
    $("#sltProvincia, #sltCiudad").attr("disabled", "disabled");
    $('#sltPrevision').change(function (e) {
        comboPrevisionTramo(parseInt($('#sltPrevision').val()));
        $('#sltPrevisionTramo').val('0');
    });
    DeshabilitarPaciente(true);

    $(".show-more-body button").on('click', function () {
        let parent = $(this).parent().parent();
        if (parent.hasClass("show-more")) {
            parent.removeClass("show-more").addClass("show-less");
            $(this).html("<i class='fas fa-arrow-up'></i> Ver menos");
        } else {
            parent.removeClass("show-less").addClass("show-more");
            $(this).html("<i class='fas fa-arrow-down'></i> Ver más");
        }
    });
}

function InicializarDatosAdministrativos() {

    $('#sltTipoAccidente').change(function (e) {
        changeTipoAccidente();
    });

    $('.aGuardarIngresoUrgencia').click(function (e) {

        $(this).prop("disabled", true);

        let rep = $("#sltTipoRepresentante").val()

        GuardarDAU(rep);
        e.preventDefault();
    });

    $('#sltTipoAtencion').change(function (e) {

        if ($(this).val() == '3') { // Pediatrico
            if (!$("#chkAcompañante").bootstrapSwitch("state")) {
                $("#chkAcompañante").bootstrapSwitch("disabled", false);
                $("#chkAcompañante").bootstrapSwitch("state", true);
            }
            $("#chkAcompañante").bootstrapSwitch("disabled", true);
        } else {
            //$("#chkAcompañante").bootstrapSwitch("disabled", false);
            //$("#chkAcompañante").bootstrapSwitch("state", false);
            // $("#chkAcompañante").bootstrapSwitch("disabled", ($.isEmptyObject(GetPaciente())));
        }

        ReiniciarRequired();

    });

    ReiniciarRequired();
    ShowModalCargando(false);

}

// Funcion valida la cantidad de palabras para no romper el pdf. Atencion urgencia... lugar del accidente( input), motivo consulta (input)
function changeTipoAccidente() {

    if ($("#sltTipoAccidente").val() == '0') {
        $("#sltLeyesPrevisionales").val("0");
        $('#lblErrorAccidente').hide();
        $('#txtLugarAccidente').attr('disabled', true).val('');
        $('#txtLugarAccidente').attr('data-required', false);
        $('#txtLugarAccidente').removeClass('invalid-input');
        const divPadre = $('#txtLugarAccidente').parent()
        //Remover label de campo requerido
        if ($(divPadre).find('span').length > 0)
            $($($(divPadre).find('span'))[0]).remove()
    } else {
        $('#lblErrorAccidente').show();
        $('#txtLugarAccidente').removeAttr('disabled');
        $('#txtLugarAccidente').attr('data-required', true);
        if ($("#sltTipoAccidente").val() == '1' || $("#sltTipoAccidente").val() == '3')
            $("#sltLeyesPrevisionales").val("2");
        else if ($("#sltTipoAccidente").val() == '2')
            $("#sltLeyesPrevisionales").val("3");
        else if ($("#sltTipoAccidente").val() == '6')
            $("#sltLeyesPrevisionales").val("1");
        else
            $("#sltLeyesPrevisionales").val("0");
    }

    ReiniciarRequired();
}

function checkWords(slt) {
    const minimoLetras = 4;
    // Reiniciar estado para hacer comprobaciones
    $(slt).removeClass("invalid-input");
    $(slt).next().remove();

    // Boton habilitado para guardar
    $("#aaGuardarIngresoUrgencia").attr('disabled', false);

    let textoLimpio = $(slt).val();

    while (textoLimpio.indexOf(' ') !== -1) {
        textoLimpio = textoLimpio.replace(' ', '');
    }

    if (textoLimpio.length < minimoLetras) {
        $(slt).addClass("invalid-input");
        if (!$(slt).next().is('span')) {
            $(slt).after(`<span class='invalid-input'>Se necesita más información; la cantidad mínima son 4 letras (no se cuentan los espacios).</span>`);
        }
        $("#aaGuardarIngresoUrgencia").attr('disabled', true);
    }
}

// COMBOS
function CargarCombos() {

    comboTipoAtencion();
    comboPrevision();
    comboPrevisionTramo(0);
    comboMediosTransporte();
    comboTiposAccidente();
    comboServicioSalud();
    comboEstablecimiento("1", "#sltEstablecimiento");
    CargarCombosUbicacionPaciente();
    comboLeyesPrevisionales();
    comboProcedencia();

}

function CargarJsonIngresoUrgencia() {

    ingresoUrgencia.MotivoConsulta = valCampo($('#txtMotivoConsulta').val());
    ingresoUrgencia.IdPrevision = valCampo(parseInt($("#sltPrevision").val()));
    ingresoUrgencia.IdPrevision_Tramo = valCampo(parseInt($("#sltPrevisionTramo").val()));
    ingresoUrgencia.IdTipoAtencion = valCampo(parseInt($('#sltTipoAtencion').val()));
    ingresoUrgencia.IdPaciente = valCampo(parseInt(paciente.GEN_idPaciente));
    ingresoUrgencia.IdMediosTransporte = valCampo(parseInt($('#sltMedioTransporte').val()));
    ingresoUrgencia.IdTiposAccidente = valCampo(parseInt($('#sltTipoAccidente').val()));
    ingresoUrgencia.LugarAccidente = valCampo($('#txtLugarAccidente').val());
    ingresoUrgencia.IdEstablecimiento = valCampo($('#sltEstablecimiento').val());
    ingresoUrgencia.IdLeyPrevisional = valCampo(parseInt($('#sltLeyesPrevisionales').val()));
    ingresoUrgencia.IdTipoProcedencia = valCampo(parseInt($('#sltProcedencia').val()));

}

async function CargarIngresoUrgencia() {

    if (niSession.ID_INGRESO_URGENCIA != undefined) {

        var json = GetJsonIngresoUrgencia(niSession.ID_INGRESO_URGENCIA);
        ingresoUrgencia.IdAtenciones = niSession.ID_INGRESO_URGENCIA;
        deleteSession('ID_INGRESO_URGENCIA');

        // ESTABLECIMIENTO
        if (json.Establecimiento !== null) {
            $("#sltServicioSalud").val(json.Establecimiento.IdServicioSalud);
            if (json.Establecimiento !== null) {
                $("#sltServicioSalud").val(json.Establecimiento.IdServicioSalud);
                $.ajax({
                    method: "GET",
                    url: `${GetWebApiUrl()}GEN_Establecimiento/${json.Establecimiento.Id}`,
                    async: false,
                    success: function (res) {
                        if (res.ServicioSalud !== undefined)
                            $("#sltServicioSalud").val(res.ServicioSalud.Id);
                    }, error: function (err) {
                        console.log("NO se pudo obtener la respuesta de GEN_Establecimiento")
                        console.log(err)
                    }
                })
            }
            $("#sltEstablecimiento").val(json.Establecimiento.Id);
        }

        cargarPacientePorId(json.IdPaciente);
        DeshabilitarPaciente(false);
        ValidarRequired(true);

        if (json !== null) {
            if (json.Prevision != null) {
                comboPrevisionTramo(json.Prevision.Id);
                ValTipoCampo($('#sltPrevision'), json.Prevision.Id);
            }
            if (json.PrevisionTramo !== null)
                ValTipoCampo($('#sltPrevisionTramo'), json.PrevisionTramo.Id);

            // DATOS DE ATENCIÓN
            $('#sltTipoAtencion').val(json.TipoAtencion.Id);
        }

        if (json.Acompañante !== null) {

            $("#chkAcompañante").bootstrapSwitch('state', true)
            ShowAcompañante(true)

            const personaAcompanianteAdmision = await buscarPersonaAcompaniante(json.Acompañante.IdPersona)
            $("#sltTipoRepresentante").val(json.Acompañante.TipoRepresentante.Id)

            await cargarPersonaAcompaniante(personaAcompanianteAdmision)
        }

        if (json.MedioTransporte != null)
            $('#sltMedioTransporte').val(json.MedioTransporte.Id).change()
        else
            $('#sltMedioTransporte').val("0")

        $('#sltTipoAccidente').val(json.TipoAccidente?.Id ?? "0");
        changeTipoAccidente();

        $('#txtLugarAccidente').val(json.LugarAccidente);
        $('#txtMotivoConsulta').val(json.MotivoConsulta);
        $('#divMotivoConsulta').html(`<p class='text-black'>${json.MotivoConsulta}</p>`);
        $('#txtLugarAccidente').val(json.LugarAccidente);
        $("#sltLeyesPrevisionales").val(json.LeyesPrevisionales?.Id ?? "0");
        $("#sltProcedencia").val(json.TipoProcedencia?.Id ?? "0");
        // Bloqueando RUT cuando sea edicion
        //$("#sltIdentificacion, #txtnumerotPac, #txtDigitoPac").attr("disabled", "disabled");
    }

}

async function GuardarDAU(rep) {

    const valido = await ValidarDAU();

    let validarFonasa = await validaAppSettingFonasa()

    if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4')
        validarFonasa ? $("#btnCargarPrevisionFonasaPaciente").show() : $("#btnCargarPrevisionFonasaPaciente").hide()

    if (valido) {
        await GuardarIngresoUrgencia(validarFonasa, rep);
    }
    else
        $(".aGuardarIngresoUrgencia").prop("disabled", false);
}

async function validaAppSettingFonasa() {

    const verificarFonasa = await getverificarFonasa()
    const validarFonasa = JSON.parse(verificarFonasa.toLowerCase())

    return validarFonasa
}

async function ValidarDAU() {

    $('a[href="#divAntecedentesClinicos"]').trigger("click");
    await sleep(250);

    // PACIENTE
    if (!validarCampos("#divDPaciente", true) || !validarCampos("#divDatosAdicionalesPaciente", true))
        return false;

    //Solo validara el acompnante cuando el checkbox este en true
    if ($(`#chkAcompañante`).bootstrapSwitch('state') && ($("#sltIdentificacion").val() == 1 || $("#sltIdentificacion").val() == 4)) {
        if (!validarCampos("#divAcompañante", true))
            return false
    }

    $('a[href="#divDatosAtencionAdm"]').trigger("click");
    await sleep(250);

    // DATOS ADMINISTRATIVOS
    if (!validarCampos("#divDatosAtencion", true))
        return false

    return EsValidaSesionToken();

}

async function alertaNoActualizaPrevision() {

    await Swal.fire({
        icon: 'warning',
        title: 'No ha actualizado la previsión del paciente.',
        confirmButtonText: "Aceptar",
        allowOutsideClick: false
    })
}

async function GuardarIngresoUrgencia(validarFonasa, rep) {

    let acompaniante = {}
    let personaAcompaniante = {}
    let nuevosDatosPersona = {}
    idPacienteNI = 0

    let method = (ingresoUrgencia.IdAtenciones !== undefined) ? "PUT" : "POST";
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia`;

    if (!$("#btnCargarPrevisionFonasaPaciente").hasClass("click-prevision") && $("#sltIdentificacion").val() !== '5') {

        personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

        if (validarFonasa && $("#sltIdentificacion").val() === '1' && method === "POST") {
            alertaNoActualizaPrevision();
            await sleep(500);
            $('a[href="#divAntecedentesClinicos"]').trigger("click");
            await sleep(500);
            $(".aGuardarIngresoUrgencia").prop("disabled", false);
            await cargarPersonaAcompaniante(personaAcompaniante);
            return
        }

        if (personaAcompaniante !== undefined && method === "PUT") {

            if ($("#chkAcompañante").bootstrapSwitch("state") == true) {
                $("#chkAcompañante").bootstrapSwitch("state", true)
                await cargarPersonaAcompaniante(personaAcompaniante);
                $("#sltTipoRepresentante").val(rep)
            }
            else {
                $("#chkAcompañante").bootstrapSwitch("state", false)
            }
        }

        if (personaAcompaniante !== undefined && method === "POST") {
            let ckAcompaniante = $("#chkAcompañante").bootstrapSwitch("state")
            if (ckAcompaniante) {
                $("#chkAcompañante").bootstrapSwitch("state", true)
                await cargarPersonaAcompaniante(personaAcompaniante);
                $("#sltTipoRepresentante").val(rep)
            }
            else {
                $("#chkAcompañante").bootstrapSwitch("state", false)
            }
        } 
    }

    if ($("#collapseUserControlAcompanante").hasClass("show") === true && $("#chkAcompañante").bootstrapSwitch('state') === true) {

        //tiene acompanante
        personaAcompaniante = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

        if (personaAcompaniante === undefined || personaAcompaniante === null) {

            //No existe la persona en bd
            const nuevaPersonaAcompaniante = await formatJsonPersonaAcompaniante()

            acompaniante = await guardarPersonaAcompaniante(nuevaPersonaAcompaniante)

            if (acompaniante !== null) {
                ingresoUrgencia.IdAcompañante = acompaniante.Id
                ingresoUrgencia.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
            } else {
                ingresoUrgencia.IdAcompañante = null
                ingresoUrgencia.IdTipoRepresentante = null
            }

        } else {
            //La encontro deberia editarlo
            nuevosDatosPersona = await formatJsonPersonaAcompaniante()
            nuevosDatosPersona.Id = personaAcompaniante.Id
            await guardarPersonaAcompaniante(nuevosDatosPersona)
            ingresoUrgencia.IdAcompañante = personaAcompaniante.Id
            ingresoUrgencia.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
        }
    }
    else if ($("#collapseUserControlAcompanante").hasClass("show") === false && $("#chkAcompañante").bootstrapSwitch('state') === true) {

        let pacc = await buscarPersonaAcompaniante(null, $("#sltIdentificacionAcompañante").val(), $("#txtnumeroDocAcompañante").val())

        if (pacc !== undefined) {
            ingresoUrgencia.IdAcompañante = pacc.Id
            ingresoUrgencia.IdTipoRepresentante = valCampo(parseInt($("#sltTipoRepresentante").val()))
        }
    }
    else {
        //Viene sin acompanante
        ingresoUrgencia.IdAcompañante = null
        ingresoUrgencia.IdTipoRepresentante = null
    }

    idPacienteNI = await GuardarPaciente();
    url += (method == "PUT") ? `/${ingresoUrgencia.IdAtenciones}/Admision` : "";

    PostDau(method, url);

}

function PostDau(method, url) {

    CargarJsonIngresoUrgencia();
    ingresoUrgencia.IdPaciente = idPacienteNI !== 0 ? idPacienteNI : GetPaciente().GEN_idPaciente
    $.ajax({
        type: method,
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(ingresoUrgencia),
        dataType: 'json',
        async: true,
        success: async function (data) {

            SalirIngresoUrgencia();

            //if (data != null && data.Id !== null && data.Id !== undefined) {
            //    const ventana = await ImprimirApiExterno(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${data.Id}/Imprimir`);

            //    if (ventana) {
            //        const checkWindowLoaded = setInterval(() => {
            //            if (ventana.document.readyState === 'complete') {
            //                clearInterval(checkWindowLoaded);
            //                showMensajeIngresoCorrecto();
            //                return;
            //            }
            //        }, 100);
            //    }
            //} else {
            //    SalirIngresoUrgencia();
            //}

        },
        error: function (jqXHR, status) {
            $(".aGuardarIngresoUrgencia").prop("disabled", false);
            console.log(`Error al guardar Atención Urgencia ${JSON.stringify(jqXHR)}`);
        }
    });

}

function showMensajeIngresoCorrecto() {
    Swal.fire({
        position: 'center', //
        icon: 'success', //
        title: 'Ingreso Correcto',
        showConfirmButton: false, //
        timer: 1500,
        allowOutsideClick: false
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer)
            SalirIngresoUrgencia();
    });
}

// OPCIONES DE SALIDA
function CancelarIngresoUrgencia() {

    ShowModalAlerta('CONFIRMACION',
        'Salir del Ingreso de urgencia',
        '¿Está seguro que desea cancelar el registro?',
        SalirIngresoUrgencia);

}

function SalirIngresoUrgencia() {
    window.removeEventListener('beforeunload', bunload, false);
    window.location.href = `${ObtenerHost()}/Vista/ModuloUrgencia/BandejaUrgencia.aspx`;
}