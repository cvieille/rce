﻿function mostrarUsuarioLogueadoEnModales(selectorModal) {
    // Acceder al nombre de usuario desde nSession
    const nombreUsuario = nSession.NOMBRE_USUARIO;

    // Añadir el nombre del usuario logueado a los modales
    $(selectorModal).find(".nombreUsuarioModal").text(nombreUsuario);
}

function IrASolicitudTransfusion(idPaciente) {
    deleteSession("ID_SOLICITUD_TRANSFUSION");
    setSession('SISTEMA', 'URGENCIA');
    setSession('ID_ATENCION_URGENCIA', IdDauAtencionClinica);
    setSession('ID_PACIENTE', idPaciente);

    const urlTransfusion = `${ObtenerHost()}/Vista/ModuloSolicitudTransfusion/NuevaSolicitudTransfusion.aspx`;

    // URL en una nueva pestaña
    window.open(urlTransfusion, '_blank');
}

// Funcion para obtener nombre usuario y utilizarlo en otros scripts COMO EN EDITAR ANAMNESIS
function obtenerNombreUsuario() {
    let nSession = getSession();
    return nSession.NOMBRE_USUARIO;
}

async function getPacientePorId(id) {

    try {
        const paciente = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return paciente

    } catch (error) {
        console.error("Error al cargar paciente")
        console.log(JSON.stringify(error))
    }
}

async function getProfesionalPorId(id) {

    try {
        const profesional = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return profesional

    } catch (error) {
        console.error("Error al cargar profesional  ")
        console.log(JSON.stringify(error))
    }
}

//Lista los procedimeintos pendientes de la atencion
async function getProcedimientosPendientes(idTipoAtencion) {

    try {
        const procedimientos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/ProcedimientosPendientes/Buscar?idTipoAtencion=${idTipoAtencion}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return procedimientos;
    } catch (error) {
        console.error("Error al cargar procedimientos pendientes");
        console.log(JSON.stringify(error));
    }
}

//Lista los medicamentos pendientes de la atencion
async function getMedicamentosPendientes(idTipoAtencion) {

    try {
        const medicamentos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/MedicamentosPendientes/Buscar?idTipoAtencion=${idTipoAtencion}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return medicamentos

    } catch (error) {
        console.error("Error al cargar medicamentos pendientes")
        console.log(JSON.stringify(error))
    }
}

async function getCategorizacion(id) {

    try {
        const categorizacion = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Categorizacion`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return categorizacion

    } catch (error) {
        console.error("Error al cargar categorizacion")
        console.log(JSON.stringify(error))
        return null;
    }
}

async function getCarteraFavoritos(idAtencion, idTipoArancel) {

    try {
        const favoritos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Cartera_Servicio/tipoAtencion/${idAtencion}/tipoArancel/${idTipoArancel}/Favoritos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return favoritos

    } catch (error) {
        console.error("Error al cargar favoritos urgencia")
        console.log(JSON.stringify(error))
    }
}

// Preparar procedimientos pendientes
async function prepararProcedimientosPendientes(procedimientos) {
    try {
        const procedimientosPendientes = procedimientos.map(item => {
            const { NumeroDocumento, Digito, Nombre, ApellidoPaterno, ApellidoMaterno, NombreSocial } = item.Paciente;
            let nombrePaciente = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`;
            let rutPaciente = `${NumeroDocumento}-${Digito}`;
            let lugar = `${item.Lugar.TipoBox.Valor === null && item.Lugar.Box.Valor === null ? `Sin información` : `${item.Lugar.TipoBox.Valor} ${item.Lugar.Box.Valor}`}`;

            let configCategorizacion = {
                heading: "h4",
                badge: ""
            };

            // Mapeando los procedimientos pendientes
            const procedimientosPendientes = item.ProcedimientosPendientes.map(proc => {
                return `<li class="list-group-item text-left"><i class="fa fa-circle mr-2" style="color:#ffc107;font-size:11px;" aria-hidden="true"></i>${proc.Procedimiento.Valor}</li>`;
            }).join(" ");

            return {
                Id: item.IdAtencion,
                Paciente: nombrePaciente,
                Rut: rutPaciente,
                Lugar: lugar,
                Categoria: dibujarIconoCategorizacion(item.Categorizacion, configCategorizacion),
                ProcedimientosPendientes: procedimientosPendientes,
                //Pendientes: item.Pendientes,
                Completadas: `${item.Completadas} de ${item.Total}`
            };
        });

        return procedimientosPendientes;
    } catch (error) {
        console.error("Error al preparar procedimientos pendientes:", error);
        throw error;
    }
}

// Preparar medicamentos pendientes
async function prepararMedicamentosPendientes(medicamentos) {
    try {
        const medicamentosPendientes = medicamentos.map(item => {
            const { NumeroDocumento, Digito, Nombre, ApellidoPaterno, ApellidoMaterno } = item.Paciente;
            const nombrePaciente = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno}`;
            const rutPaciente = `${NumeroDocumento}-${Digito}`;
            const lugar = `${item.Lugar.TipoBox.Valor === null && item.Lugar.Box.Valor === null ? `Sin información` : `${item.Lugar.TipoBox.Valor} ${item.Lugar.Box.Valor}`}`;

            let configCategorizacion = {
                heading: "h4",
                badge: ""
            }


            // Mapeando los medicamentos pendientes
            const medicamentosPendientes = item.MedicamentosPendientes.map(med => {
                return `<li class="list-group-item text-left"><i class="fa fa-circle mr-2" style="color:#ffc107;font-size:11px;" aria-hidden="true"></i>${med.Medicamento.Valor}</li>`;
                /*return `<li class="list-group-item text-left"><i class="fa fa-circle mr-2" style="color:#ffc107;font-size:11px;" aria-hidden="true"></i>${med.Medicamento.Valor} (${med.Dosis}, ${med.Frecuencia}, ${med.Via.Valor})</li>`;*/
            }).join(" ");

            return {
                Id: item.IdAtencion,
                Paciente: nombrePaciente,
                Rut: rutPaciente,
                Lugar: lugar,
                Categoria: dibujarIconoCategorizacion(item.Categorizacion, configCategorizacion),
                MedicamentosPendientes: medicamentosPendientes,
                Pendientes: item.Pendientes,
                Completadas: `${item.Completadas} de ${item.Total}`
            };
        });

        return medicamentosPendientes;
    } catch (error) {
        console.error("Error al preparar medicamentos pendientes:", error);
        throw error;
    }
}

function GetJsonIngresoUrgencia(URG_idAtenciones_Urgencia) {

    let json = null;
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}`

    //let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}?`
    //    + `extens=ATENCIONMEDICA&`
    //    + `extens=SIGNOSVITALES&`
    //    + `extens=CATEGORIZACION&`
    //    + `extens=MEDICO&`
    //    + `extens=GINECOOBSTETRICO&`
    //    + `extens=INDICACIONESCLINICAS&`
    //    + `extens=ANTECEDENTESCLINICOS`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            json = data;
        }
    });

    return json;
}

// tipo atencion
async function obtenerTipoAtencion(URG_idAtenciones_Urgencia) {
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${URG_idAtenciones_Urgencia}`;

    try {
        const data = await $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json'
        });

        return data; // Devuelve la respuesta JSON completa
    } catch (error) {
        console.error("Error al obtener los datos de ingreso de urgencia:", error);
        return null;
    }
}

function cargarAtencionClinicaObstetrica(atencionObstetrica, idTipoAtencion) {

    let fechaActual = null;

    if (idTipoAtencion == 2) { // Ginecologico

        $("#divDatosGinecoObstetrico").show();
        $(`#txtUltimaRegla`).attr("data-required", true);
        fechaActual = GetFechaActual();
        $(`#spnFechaHoraAtencionObstetricia`).text(moment(fechaActual).format('DD-MM-YYYY HH:mm:ss'));
    } else {
        $("#divDatosGinecoObstetrico").hide();
        $(`#txtUltimaRegla`).attr("data-required", false);
    }

    if (idTipoAtencion == 2 && atencionObstetrica.length > 0) {

        const jsonObs = atencionObstetrica[0];

        $(`#spnFechaHoraAtencionObstetricia`).text(moment(jsonObs.FechaHora).format('DD-MM-YYYY HH:mm:ss'));
        $("#txtUltimaRegla").val(jsonObs.UltimaRegla);
        $("#txtAlturaUterinaObstetrico").val(jsonObs.AlturaUterina);
        $("#txtLatidosCardioFetalesObstetrico").val(jsonObs.LatidosCardioFetales);

        if (jsonObs.FlujoGenital === "SI")
            $("#rdoFlujoGenitalObstetricoSi").bootstrapSwitch('state', true);
        else if (jsonObs.FlujoGenital === "NO")
            $("#rdoFlujoGenitalObstetricoNo").bootstrapSwitch('state', true);

        if (sSession.CODIGO_PERFIL != perfilAccesoSistema.medico && sSession.CODIGO_PERFIL != 25) {
            $("#rdoFlujoGenitalObstetricoSi").bootstrapSwitch('disabled', true);
            $("#rdoFlujoGenitalObstetricoNo").bootstrapSwitch('disabled', true);
        }

        $("#txtTactoVaginalObstetrico").val(jsonObs.TactoVaginal);
        $("#txtEspeculoscopiaObstetrico").val(jsonObs.Especuloscopia);
        $("#txtEcografiaObstetrico").val(jsonObs.Ecografia);
        $("#txtActividadUterinaObstetrico").val(jsonObs.ActividadUterina);
        $("#txtOtrosGinecoObstetrico").val(jsonObs.Otros);

    }
}

async function showDatosPacienteInicacionMedica(objAtencionUrgencia, elemento) {
    $(elemento).empty();

    let titulo = "";
    let categorizacion = "";
    let html = "";
    let ultimoPeso = null;

    let configCategorizacion = {
        heading: "h1",
        badge: ""
    };

    const paciente = await getPacientePorId(objAtencionUrgencia.idPaciente);
    const signosVitales = await getSignosVitalesPorId(objAtencionUrgencia.idAtencion);
    const alergia = await getAlergiaPaciente(objAtencionUrgencia.idAtencion); // Obtener alergia desde el nuevo endpoint

    if (paciente === undefined) return;

    const { NombreSocial, Nombre, ApellidoPaterno, ApellidoMaterno, Genero, Edad, Identificacion } = paciente;

    const signosOrdenados = signosVitales.sort((a, b) => new Date(b.FechaHora) - new Date(a.FechaHora));
    let nombrePersona = $.trim(`${(NombreSocial != null) ? `(${NombreSocial})` : ""} ${Nombre ?? ""} ${ApellidoPaterno ?? ""} ${ApellidoMaterno ?? ""}`);

    if (nombrePersona === "" && Identificacion.Id === 5)
        nombrePersona = "NN";

    titulo = `<h6><strong>${nombrePersona}</strong></h6>
              <div class="d-flex flex-row justify-content-start flex-wrap">
                <div><strong>Género:</strong> ${Genero.Valor} | <strong>Edad:</strong> ${(Edad != null) ? `${Edad.EdadCompleta}` : ""}</div>               
              </div>
              <hr>`;
    if (objAtencionUrgencia.categorizacion !== null && objAtencionUrgencia.categorizacion.length > 0)
        categorizacion = `${dibujarIconoCategorizacion(objAtencionUrgencia.categorizacion[objAtencionUrgencia.categorizacion.length - 1], configCategorizacion, "w-100")}`;

    html = `<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                        ${titulo}
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                        ${categorizacion}
                    </div>
                </div>`;


    if (signosOrdenados.length > 0) {
        let contienePA = false;
        let pamValor = null;

        // Buscar el peso más reciente
        for (const signo of signosOrdenados) {
            for (const s of signo.Signos) {
                if (s.TipoMedida.Valor === "Peso") {
                    ultimoPeso = s.Valor; // Actualizar el último peso registrado
                    break;
                }
            }
            if (ultimoPeso !== null)
                break; // Salir del bucle si se encuentra el peso
        }

        signosOrdenados[0].Signos.forEach((s, index) => {
            $(elemento).empty();

            if (index == 0) {
                html += `<div class="row" id="contenedor-signos">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 border text-center rounded p-1 d-flex flex-row justify-content-start flex-wrap">
                                <span><strong>Última toma de signos vitales</strong></span><span>${moment(signosOrdenados[0].FechaHora).format('DD/MM/YYYY HH:mm:ss')}</span>
                                ${signosOrdenados[0].Profesional?.Valor !== undefined ?
                        `<div>
                            <span><strong>Profesional:</strong></span>
                            <span>${signosOrdenados[0].Profesional?.Valor == undefined ? "" : signosOrdenados[0].Profesional?.Valor}</span >
                        </div>`: ""
                    }
                            </div>`;
            }

            html += `<div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 border text-center rounded d-flex flex-column justify-content-center">
                        <span>${s.TipoMedida.Valor}</span>
                        <i class="${s.Icono} salud-icon"></i>
                        <span>${s.Valor}</span>
                     </div>`;

            if (s.TipoMedida.Valor === "P. Arterial") {
                contienePA = true;

                let valorPA = s.Valor;

                if (Array.isArray(valorPA)) {
                    let valores = valorPA.split('/');
                    let ps = parseInt(valores[0].trim());
                    let pd = parseInt(valores[1].trim());
                    if (!isNaN(ps) && !isNaN(pd)) {
                        let pam = (ps + 2 * pd) / 3;
                        pamValor = pam.toFixed(1);
                    }
                }
            }
        });

        if (contienePA && pamValor !== null) {
            html += `<div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 border text-center rounded d-flex flex-column justify-content-center">
                                <span>PAM</span>
                                <i class="fa fa-heart salud-icon"></i> <!-- Icono de corazón -->
                                <span>${pamValor}</span>
                            </div>`;
        }

        html += `</div>`;
    }

    $(elemento).append(html);
    $(elemento).append('</div></div>');

    // Filtrar solo las alergias
    let complementoConsulta = ``;
    if (objAtencionUrgencia.categorizacion !== null && objAtencionUrgencia.categorizacion.length > 0) {
        complementoConsulta = objAtencionUrgencia.categorizacion[0].Observaciones || "";
    }

    // Aplicar color rojo solo si alergia es "SI"
    let alergiaColor = alergia.startsWith("SI") ? "color: red;" : "";

    const $observacionesDiv = $(elemento).find(".col-md-12:has(span:contains('Observaciones'))")
    $observacionesDiv.remove()


    $(elemento).append(`
    <div class="row mt-3">
        ${complementoConsulta !== "" ? `
        <div class="col-md-12">
            <span>Observaciones:</span>
            <p><strong>${complementoConsulta}</strong></p>
        </div>
        ` : ``}
        <div class="col-md-12">
            <span>Motivo:</span>
            <p><strong>${objAtencionUrgencia.motivoConsulta}</strong></p>
        </div>
        <div class="col-md-12">
            <span>Alergias:</span>
            <p style="${alergiaColor}"><strong>${alergia}</strong></p>
        </div>
        ${ultimoPeso !== null ? `
        <div class="col-md-12">
            <span>Peso:</span>
            <p><strong>${ultimoPeso}</strong></p>
        </div>
        ` : ``}
    </div>`);
}

// GET PARA SACAR NOMBRE DE ALERGIAS
async function getAlergiaPaciente(idAtencionUrgencia) {
    try {
        // Realiza solicitud para obtener los datos de la atención de urgencia
        const atencionData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}`,
            contentType: 'application/json',
            dataType: 'json',
        });

        // Extraer el campo Alergia general y reemplazar "Desconocido" por "No sabe"
        const alergiaGeneral = atencionData.Alergia ? atencionData.Alergia.Valor : "No sabe";
        const alergiaTexto = alergiaGeneral === "Desconocido" ? "No sabe" : alergiaGeneral;

        // Realiza solicitud para obtener antecedentes específicos de alergias
        const antecedentesData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Antecedentes`,
            contentType: 'application/json',
            dataType: 'json',
        });

        // Filtrar y formatear las alergias específicas
        const alergiasEspecificas = formatoAlergias(antecedentesData);

        // Construir el mensaje completo de alergia
        let alergiaTextoCompleto = alergiaTexto;
        if (alergiaTexto === "SI") {
            alergiaTextoCompleto = alergiasEspecificas ? `SI (${alergiasEspecificas})` : "SI (Sin detalles específicos)";
        }

        return alergiaTextoCompleto;

    } catch (error) {
        console.error('Error obteniendo datos de alergias:', error);
        return "Error obteniendo datos de alergias";
    }
}

function formatoAlergias(antecedentes) {
    // Mapear los valores de UrgenciaClasificacionAntecedente.Valor
    const alergias = antecedentes.filter(item => item.ClasificacionAntecedente.Valor === 'Alergias');
    return alergias.map(item => item.UrgenciaClasificacionAntecedente.Valor).join(', ');
}

async function getSignosVitalesPorId(idAtencion) {

    try {
        const signos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/Signos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return signos

    } catch (error) {
        console.error("Error al cargar signos vitales de la atención de urgencia")
        console.log(JSON.stringify(error))
    }
}

function dibujarIconoCategorizacion(json, configCategorizacion, claseExtra = "") {

    let categorizacion = ""

    const estiloHTML = {
        C1: `class="badge ${configCategorizacion.badge} btn-danger badge-count ${claseExtra}`,
        C2: `class="badge ${configCategorizacion.badge} badge-count ${claseExtra}" style="color: #ffffff; background-color: orange;`,
        C3: `class="badge ${configCategorizacion.badge} btn-warning badge-count ${claseExtra}`,
        C4: `class="badge ${configCategorizacion.badge} btn-success badge-count ${claseExtra}`,
        C5: `class="badge ${configCategorizacion.badge} btn-info badge-count ${claseExtra}`,
        SC: `class="badge ${configCategorizacion.badge} btn-default badge-count ${claseExtra}`
    };

    if (json !== undefined) {
        if (json.NombreCategorizacion !== undefined)
            categorizacion = json.NombreCategorizacion
        else if (json.Valor != null)
            categorizacion = json.Valor
        else if (json.Codigo != undefined)
            categorizacion = json.Codigo
        else
            categorizacion = "S/C"
    }
    else
        categorizacion = "S/C"

    let iconoCategorizacion;
    /*categorizacion = json === undefined ? "S/C" : json.Codigo*/

    if (categorizacion == "C1")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.C1, categorizacion, configCategorizacion.heading);

    else if (categorizacion == "C2")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.C2, categorizacion, configCategorizacion.heading);

    else if (categorizacion == "C3")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.C3, categorizacion, configCategorizacion.heading);

    else if (categorizacion == "C4")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.C4, categorizacion, configCategorizacion.heading);

    else if (categorizacion == "C5")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.C5, categorizacion, configCategorizacion.heading);

    else if (categorizacion == "S/C")
        iconoCategorizacion = pintaIconoCategorizacion(estiloHTML.SC, categorizacion, configCategorizacion.heading);

    return iconoCategorizacion;
}
function pintaIconoCategorizacion(estiloHTML, categorizacion, heading) {

    return `<${heading}>
                <span id="bgEstadoC" ${estiloHTML}">
                    ${categorizacion}
                </span>
            </${heading}>`;
}

//arreglo responsive
function checkResolusionUrg(width) {

    var screenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth

    if (screenWidth <= width) {
        $(".contenedorCategorizacion").removeClass("col-md-4")
        $(".contenedorCategorizacion").addClass("col-md-12")
        $("#contenedorCategorizacion").addClass("mr-2")
        $("#contenedor-signos .salud-box").removeClass("col-lg-4")
    } else {
        $(".contenedorCategorizacion").removeClass("col-md-12")
        $(".contenedorCategorizacion").addClass("col-md-4")
        $("#contenedorCategorizacion").removeClass("mr-2")
        $("#contenedor-signos .salud-box").addClass("col-lg-4")
    }
}

function mensajeTablaNoItemUrg({ numeroFilas, elemento, mensaje, colspan }) {

    if (numeroFilas === 0) {
        $(elemento).empty()
        $(elemento).addClass("text-center")
        $(elemento).append(`<tr id="no-item"><td colspan="${colspan}">${mensaje}</td></tr>`)
    }
}

async function getAntecendentesUrg(idAtencionUrgencia) {

    try {
        const antecedentes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Antecedentes`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return antecedentes

    } catch (error) {
        console.error("Error al cargar antecedentes urgencia")
        console.log(JSON.stringify(error))
    }
}

function mostrarOcultarIndicacionesUrg(data, selector) {

    if (Array.isArray(data) && data.length > 0)
        $(selector).show()
    else
        $(selector).hide()
}

function imprimirEvolucionUrg() {

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Evolucion/Imprimir`
    ImprimirApiExterno(url)
}

async function procedimientosPendientesUrg(idTipoAtencion) {

    const procedimientosPendientes = await getProcedimientosPendientes(idTipoAtencion)

    const pendientes = await prepararProcedimientosPendientes(procedimientosPendientes)

    $('#tblProcedimientosPendientes').DataTable({
        data: pendientes,
        ordering: true,
        columns: [
           /* 0 */{ data: 'Id', title: 'ID' },
           /* 1 */{ data: 'Rut', title: 'Nº Documento' },
           /* 2 */{ data: 'Paciente', title: 'Paciente' },
           /* 3 */{ data: 'Lugar', title: 'Lugar' },
           /* 4 */{ data: 'Categoria', title: 'Cat.' },
           /* 5 */{ data: 'ProcedimientosPendientes', title: 'Procedimientos Pendientes' },
           /* 6 */{ data: 'Completadas', title: 'Completadas', "className": "text-center" },
           /* 7 */{ data: 'Ver', title: 'Ver', "className": "text-center" },
        ],
        columnDefs: [
            { responsivePriority: 1, targets: -1 },
            { responsivePriority: 2, targets: 1 },
            { responsivePriority: 3, targets: 2 },
            { responsivePriority: 4, targets: 5 },
            { responsivePriority: 5, targets: 6 },
            { responsivePriority: 6, targets: 4 },
            { responsivePriority: 7, targets: 3 },
            { targets: 0, visible: false },
            {
                targets: 1,
                render: function (data, type, full, meta, row) {
                    return `<div>${data}</div>`;
                },
                targets: -1,
                render: function (data, type, full, meta, row) {
                    const id = full.Id
                    //divProcedimientos
                    return `<a onclick="irAtencionClinica(${id})" class="btn btn-info">
                                <i class="fas fa-eye fa-xs"></i>
                            </a>`
                }
            },
        ],
        "bDestroy": true
    });

    $('#tblProcedimientosPendientes thead th:eq(0)').attr("style", "width:15vh; white-space:normal;") //Rut
    $('#tblProcedimientosPendientes thead th:eq(1)').attr("style", "width:16vh; white-space:normal;")  //Paciente
    $('#tblProcedimientosPendientes thead th:eq(2)').attr("style", "width:11vh; white-space:normal;") //Lugar
    $('#tblProcedimientosPendientes thead th:eq(3)').attr("style", "width:1vh; white-space:normal;")  //Cat.
    $('#tblProcedimientosPendientes thead th:eq(4)').attr("style", "width:70vh; white-space:normal;") //Procedimientos
    $('#tblProcedimientosPendientes thead th:eq(5)').attr("style", "width:1vh; white-space:normal;") //Total
}

async function medicamentosPendientesUrg(idTipoAtencion) {

    const medicamentosPendientes = await getMedicamentosPendientes(idTipoAtencion)

    const pendientes = await prepararMedicamentosPendientes(medicamentosPendientes)

    $('#tblMedicamentosPendientes').DataTable({
        data: pendientes,
        ordering: true,
        columns: [
           /* 0 */{ data: 'Id', title: 'ID' },
           /* 1 */{ data: 'Rut', title: 'Nº Documento' },
           /* 2 */{ data: 'Paciente', title: 'Paciente' },
           /* 3 */{ data: 'Lugar', title: 'Lugar' },
           /* 4 */{ data: 'Categoria', title: 'Cat.' },
           /* 5 */{ data: 'MedicamentosPendientes', title: 'Medicamentos Pendientes' },
           /* 6 */{ data: 'Completadas', title: 'Completadas', "className": "text-center" },
           /* 7 */{ data: 'Ver', title: 'Ver', "className": "text-center" },
        ],
        columnDefs: [
            { responsivePriority: 1, targets: -1 },
            { responsivePriority: 2, targets: 1 },
            { responsivePriority: 3, targets: 2 },
            { responsivePriority: 4, targets: 5 },
            { responsivePriority: 5, targets: 6 },
            { responsivePriority: 6, targets: 4 },
            { responsivePriority: 7, targets: 3 },
            { targets: 0, visible: false },
            {
                targets: 1,
                render: function (data, type, full, meta, row) {
                    return `<div>${data}</div>`;
                },
                targets: -1,
                render: function (data, type, full, meta, row) {
                    const id = full.Id
                    //divProcedimientos
                    return `<a onclick="irAtencionClinica(${id})" class="btn btn-info">
                                <i class="fas fa-eye fa-xs"></i>
                            </a>`
                }
            },
        ],
        "bDestroy": true
    });

    $('#tblMedicamentosPendientes thead th:eq(0)').attr("style", "width:15vh; white-space:normal;") //Rut
    $('#tblMedicamentosPendientes thead th:eq(1)').attr("style", "width:16vh; white-space:normal;")  //Paciente
    $('#tblMedicamentosPendientes thead th:eq(2)').attr("style", "width:11vh; white-space:normal;") //Lugar
    $('#tblMedicamentosPendientes thead th:eq(3)').attr("style", "width:1vh; white-space:normal;")  //Cat.
    $('#tblMedicamentosPendientes thead th:eq(4)').attr("style", "width:70vh; white-space:normal;") //Medicamentos
    $('#tblMedicamentosPendientes thead th:eq(5)').attr("style", "width:1vh; white-space:normal;") //Total
}


// SIGNOS VITALES
function linkSignosVitales(sender) {

    const hasClass = $("#mdlSignosVitales").hasClass("zindexSignosVitales")
    if (hasClass)
        $("#mdlSignosVitales").removeClass("zindexSignosVitales")

    $("#aGuardarSignoVital").show()
    $("#aEditarSignoVital").hide()

    MostrarSignosVitales(sender);
}

function GuardarSignosVitales(id) {
    // Deshabilitar el botón que llama a esta función
    $("#aGuardarSignoVital").prop('disabled', true);

    if (validarCamposRegex("#divSignosVitales", false)) {
        toastr.error("Debe ingresar valores válidos");
        // Habilitar el botón nuevamente si hay un error en los campos
        $("#aGuardarSignoVital").prop('disabled', false);
        return;
    }

    const ATENCION_CLINICA = 7;
    let array = [];
    const arrayInputs = $("#divSignosVitales input").toArray();

    for (let i = 0; i < $(arrayInputs).length; i++) {
        if ($.trim($(arrayInputs[i]).val()) != "") {
            array.push({
                "Id": $(arrayInputs[i]).data("id"),
                "Valor": $(arrayInputs[i]).val(),
                "IdTipoModulo": ATENCION_CLINICA
            });
        }
    }

    if (array.length > 0) {
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Signos`,
            contentType: 'application/json',
            data: JSON.stringify(array),
            success: function (data) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Los signos vitales se han guardado correctamente.",
                    showConfirmButton: false,
                    timer: 3000
                });
                $("#mdlSignosVitales").modal("hide");
                cargarUltimosSignosVitalesUrg("#divDetalleSignosVitalesUrg", id); // Actualizar la vista
                // Habilitar el botón después de la solicitud exitosa
                $("#aGuardarSignoVital").prop('disabled', false);
            },
            error: function () {
                console.log("Error al guardar signos vitales.");
                // Habilitar el botón en caso de error
                $("#aGuardarSignoVital").prop('disabled', false);
            }
        });
    } else {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "Llene por lo menos un campo.",
            showConfirmButton: false,
            timer: 3000
        });
        // Habilitar el botón si no hay datos para guardar
        $("#aGuardarSignoVital").prop('disabled', false);
    }
}

async function cargarUltimosSignosVitalesUrg(elemento, idAtencionUrgencia) {

    try {
        const signosVitales = await getSignosVitalesPorId(idAtencionUrgencia);
        // Ordenar por fecha de forma descendente (el más reciente primero)
        const signosOrdenados = signosVitales.sort((a, b) => new Date(b.FechaHora) - new Date(a.FechaHora));
        let html = `<div class="row d-flex flex-row justify-content-center flex-wrap">`

        $(elemento).empty(html)

        if (signosOrdenados.length > 0) {
            let fechaActualParaSignos = moment(GetFechaActual())
            let fechaUltimaToma = moment(signosOrdenados[0].FechaHora)
            const diferenciaMinutos = fechaActualParaSignos.diff(fechaUltimaToma, 'minutes');

            let contienePA = false;
            let contienePAM = false;

            $("#ultimaTomaSignosUrg").empty()
            $("#ultimaTomaSignosUrg").append(`<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex flex-row justify-content-start flex-wrap">
                <div>
                    <span><strong>Última toma de signos vitales:</strong></span>
                    <span>${moment(signosOrdenados[0]?.FechaHora).format('DD/MM/YYYY HH:mm:ss')}</span>    
                </div>
                ${signosOrdenados[0].Profesional?.Valor !== undefined ?
                    `<div>
                        <span><strong>Profesional:</strong></span>
                        <span>${signosOrdenados[0].Profesional?.Valor == undefined ? "" : signosOrdenados[0].Profesional?.Valor}</span >
                    </div>`: ""
                }
                ${diferenciaMinutos > 120 ?
                    `<div>
                    <span class="text-red"><i class="fa fa-exclamation-triangle latidos"></i> Han pasado más de 2 horas desde la última toma de signos vitales.</span> 
                </div>`
                    : ``}
            </div>`);

            $("#ultimaTomaSignosUrg").show()


            // Iterar sobre los signos del primer registro (el más reciente)
            signosOrdenados[0].Signos.forEach((s, index) => {

                html += `<div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 border border-dark text-center rounded d-flex flex-column justify-content-center m-1" style="min-width:116px;">
                            <span>${s.TipoMedida.Valor}</span>
                            <i class="${s.Icono} salud-icon"></i>
                            <span>${s.Valor}</span>
                         </div>`

                // Verificar si el tipo de medida es "P. Arterial" o "PAM"
                if (s.TipoMedida.Valor === "P. Arterial") {
                    contienePA = true;
                } else if (s.TipoMedida.Valor === "PAM") {
                    contienePAM = true;
                }
            });

            // Si se encontró "P. Arterial" pero no "PAM", calcular y mostrar "PAM"
            if (contienePA && !contienePAM) {
                let pamValor = null;

                // Buscar el valor de la presión arterial en los signos ordenados
                let presionArterial = signosOrdenados[0].Signos.find(signo => signo.TipoMedida.Valor === "P. Arterial");
                if (presionArterial) {
                    // Obtener PS y PD desde el valor de la presión arterial
                    let valorPA = presionArterial.Valor;
                    let valores = valorPA.split('/');
                    let ps = parseInt(valores[0].trim());
                    let pd = parseInt(valores[1].trim());

                    // Calcular PAM si se obtuvieron valores válidos de PS y PD
                    if (!isNaN(ps) && !isNaN(pd)) {
                        let pam = (ps + 2 * pd) / 3;
                        pamValor = pam.toFixed(1);
                    }
                }

                if (pamValor !== null) {
                    html += `<div class="col-sm-12 col-md-12 col-lg-12 col-xl-3 border border-dark text-center rounded d-flex flex-column justify-content-center m-1" style="min-width:116px;">
                                <span>PAM</span>
                                <i class="fa fa-heart salud-icon"></i> <!-- Icono de corazón -->
                                <span>${pamValor}</span>
                            </div>`;
                }
            }

            html += `</div>`; // Cerrar el contenedor de signos
            $(elemento).empty().append(html);
            // Mostrar el historial y el botón de agregar
            $("#aSignosVitalesHistorial").show();
            $("#aSignosVitalesAgregar").show();
        } else {
            html = `<div class="row" id="contenedor-signos">
                        <div class="col-md-12 d-flex justify-content-center">No hay información para mostrar</div>
                    </div>`;
            $("#ultimaTomaSignosUrg").hide()

            $(elemento).empty().append(html);
            // Mostrar el botón de agregar y ocultar el botón de historial
            $("#aSignosVitalesHistorial").hide();
            $("#aSignosVitalesAgregar").show();
        }
    } catch (error) {
        console.error("Error al cargar los últimos signos vitales:", error);
    }
}


function cargarTiposDeAtencion() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Tipo_Atencion/Combo`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                const tipoAtencionSelect = $('#tipoAtencionSelect');
                tipoAtencionSelect.empty(); // Limpiar el select antes de agregar nuevos options
                //tipoAtencionSelect.append(new Option("Todos", ""));
                data.forEach(tipo => {
                    tipoAtencionSelect.append(new Option(tipo.Valor, tipo.Valor));
                });
                resolve();
            },
            error: function (error) {
                console.error("Ocurrió un error al cargar los tipos de atención");
                console.log(error);
                reject(error);
            }
        });
    });
}