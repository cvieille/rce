﻿
const { medico, matroneriaUrgencia, tensUrgencia, enfermeraUrgencia, kinesiologoUrgencia, apoyoClinico, administradorDeUrgencia } = perfilAccesoSistema


nSession = getSession();
$(document).ready(async function () {

    if (nSession.CODIGO_PERFIL == medico || nSession.CODIGO_PERFIL == matroneriaUrgencia || nSession.CODIGO_PERFIL == tensUrgencia
        || nSession.CODIGO_PERFIL == enfermeraUrgencia || nSession.CODIGO_PERFIL == kinesiologoUrgencia ||
        nSession.CODIGO_PERFIL == apoyoClinico || nSession.CODIGO_PERFIL == administradorDeUrgencia) {
        await actualizarHistorialAltas(nSession.ID_INGRESO_URGENCIA);
    }
});

async function actualizarHistorialAltas(idAtencionUrgencia) {
    const altas = await getAltasUrg(idAtencionUrgencia);

    if (!altas || altas.length === 0) {
        $("#divHistorialAltas").hide();
        return;
    }

    mostrarUsuarioLogueadoEnModales("#mdlEgresarAtencion")

    const historialAltas = await prepararDatosAltas(altas);
    dibujarTablaHistorialAltasUrg(historialAltas);

    $("#divHistorialAltas").show();
}

async function prepararDatosAltas(data) {

    if (!Array.isArray(data) || data === undefined)
        return

    return data?.map(e => {

        const { Id, Nombre, ApellidoPaterno, ApellidoMaterno } = e.Profesional

        NombreProfesional = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`

        return ({
            Id: e.Id,
            TipoDestinoUrgencia: e.TipoDestinoUrgencia.Valor ?? "Sin Información",
            PronosticoMedicoLegal: `${e.PronosticoMedicoLegal.Valor ?? "Sin Información"}`,
            Diagnosticos: e.Diagnosticos,
            IndicacionesMedicas: e.IndicacionesMedicas,
            MedicamentosAlta: e.MedicamentosAlta,
            Fecha: moment(e.Fecha).format('DD-MM-YYYY HH:mm:ss'),
            Profesional: `${NombreProfesional}`,
            IdProfesional: Id,
            IdAtencion: e.IdAtencion
        })
    });
}

function dibujarTablaHistorialAltasUrg(altas) {

    $('#tblAltasUrg').DataTable({
        destroy: true,
        data: altas,
        createdRow: function (row, data, dataIndex) {
            $(row).attr('id', `alta_${data.Id}`);
        },
        columns: [
            { data: "Id", title: "Id" },
            { data: "TipoDestinoUrgencia", title: "Destino" },
            { data: "PronosticoMedicoLegal", title: "Pronostico" },
            {
                data: "Diagnosticos", title: "Diagnostico", render: function (data, type, row) {

                    const { Diagnosticos } = row

                    if (Diagnosticos === null) {
                        return `<li class="list-group-item text-left">
                                <i class="fa fa-circle mr-2"
                                   style="color:#ffc107;font-size:11px;"
                                   aria-hidden="true"
                                >
                                </i>No hay informacion</li>`
                    }
                    else if (Array.isArray(row.Diagnosticos)) {
                        return row.Diagnosticos.map(d =>
                            `<li 
                                class="list-group-item text-left">
                                <i class="fa fa-circle mr-2"
                                   style="color:#ffc107;font-size:11px;"
                                   aria-hidden="true">
                                </i>${d.Codigo} - ${d.Alias.Valor}</li>`
                        ).join(" ");
                    }
                    else {
                        return "";
                    }
                }
            },
            { data: "IndicacionesMedicas", title: "Indicaciones" },
            {
                data: "MedicamentosAlta", title: "Medicamentos", render: function (data, type, row) {

                    const { MedicamentosAlta } = row;

                    if (MedicamentosAlta === null || MedicamentosAlta.trim() === "" || MedicamentosAlta === 0) {
                        return `<li class="list-group-item text-left">
                                <i class="fa fa-circle mr-2"
                                   style="color:#DC3545;font-size:11px;"
                                   aria-hidden="true">
                                </i>Sin medicamentos</li>`;
                    } else {
                        return MedicamentosAlta
                            .split('\n') // Saltos de linea
                            .filter(m => m.trim() !== "") // Filtrando lineas con espacio
                            .map(m =>
                                `<li class="list-group-item text-left">
                                <i class="fa fa-circle mr-2"
                                   style="color:#ffc107;font-size:11px;"
                                   aria-hidden="true">
                                </i>${m.trim()}</li>` // Agrega medicamento a la fiola
                            ).join(" ");
                    }
                }
            },
            { data: "Fecha", title: "Fecha" },
            { data: "Profesional", title: "Profesional" },
            { data: null, title: 'Acciones' }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data, type, row) {
                    return botonesAccionAlta(row.Id, row.IdProfesional, row.IdAtencion);
                }
            }
        ],
        order: [[0, 'desc']]
    });
}

function showModalAltaUrg(data, id) {
    const modal = $("#mdlAltaDescripcionUrg");
    const descripcion = $("#descripcionAltaUrg");
    modal.attr("data-id-alta", id);
    modal.data("id-alta", id);
    descripcion.text(data);
    modal.modal("show");
}

// GET
async function getAltasUrg(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        return;

    if (nSession.CODIGO_PERFIL !== medico && nSession.CODIGO_PERFIL !== tensUrgencia && nSession.CODIGO_PERFIL !== enfermeraUrgencia &&
        nSession.CODIGO_PERFIL !== matroneriaUrgencia && nSession.CODIGO_PERFIL !== kinesiologoUrgencia &&
        nSession.CODIGO_PERFIL !== apoyoClinico && nSession.CODIGO_PERFIL !== administradorDeUrgencia) {
        return;
    }

    try {
        const altas = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Alta/Medica`,
            contentType: 'application/json',
            dataType: 'json'
        });

        return altas;

    } catch (error) {
        if (error.status === 400) {
            return;
        }
    }
}

// DELETE
async function deleteAltaMedico(idAltaMedico) {
    if (idAltaMedico === null || idAltaMedico === undefined)
        return


    //console.log(nSession.ID_INGRESO_URGENCIA, idAltaMedico)

    const result = await Swal.fire({
        title: "Seguro quieres eliminar el alta médica?",
        text: "No se podrán revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Alta/Medico/${idAltaMedico}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            success: async function (data, status, jqXHR) {
                toastr.success(`Alta médica eliminada`);

                // Eliminar la fila correspondiente al alta medica eliminada
                const table = $('#tblAltasUrg').DataTable();
                const rowIndex = table.row(`#alta_${idAltaMedico}`).index();
                table.row(rowIndex).remove().draw(false);
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar alta médica: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// BOTONES 
function botonesAccionAlta(idAltaMedico, IdProfesional, IdAtencion) {
    const esCreadorAlta = IdProfesional == nSession.ID_PROFESIONAL;
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 30;
    const html = `
        <div class="d-flex flex-row justify-content-center">
            <div>
                <button
                    type="button"
                    class="btn btn-info m-1 ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                    onclick="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? `editarAltaUrgencia(${idAltaMedico})` : ''}"
                    data-toggle="tooltip" 
                    data-placement="bottom"
                    title="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? 'Editar' : 'No tienes permiso para editar'}"
                    style="display: inline-block;"
                    ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}
                >
                    <i class="fa fa-edit fa-xs"></i>
                </button>
            </div>
            <div>
                <button 
                    type="button" 
                    class="btn btn-danger botonAltaMedico m-1 ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                    id="botonAltaMedico_${idAltaMedico}"
                    onclick="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? `deleteAltaMedico(${idAltaMedico})` : ''}"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? 'Eliminar' : 'No tienes permiso para eliminar'}"
                    style="display: inline-block;"
                    ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}
                >
                    <i class="fa fa-trash fa-xs"></i>
                </button>
            </div>

             <div>
                <button
                    type="button"
                    class="btn btn-info botonAltaMedico m-1 ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                    id="botonImprimirRecetaAltaMedico_${idAltaMedico}"
                    onclick="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? `imprimirRecetaAltaMedica(${IdAtencion})` : ''}"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="${(esCreadorAlta || esPerfilAutorizadoEliminar) ? 'Imprimir receta' : 'No tienes permiso para imprimir'}"
                    style="display: inline-block;"
                    ${esCreadorAlta || esPerfilAutorizadoEliminar ? '' : 'disabled'}
                >
                    <i class="fa fa-print fa-xs"></i>
                </button>
            </div>

        </div>
    `;
    return html;
}

function cargarAltaMedicaEditar(Alta) {

    $("#sltDestinoAltaPaciente").val(Alta.TipoDestinoUrgencia.Id).change()
    if (Alta.PronosticoMedicoLegal !== null && Alta.PronosticoMedicoLegal.Id !== null)
        $("#sltPronosticoMedicoLegal").val(Alta.PronosticoMedicoLegal.Id)

    if (Alta.TipoDestinoDerivacion !== null && Alta.TipoDestinoDerivacion.Id !== null)
        $("#sltDestinoDerivacion").val(Alta.TipoDestinoDerivacion.Id)

    if (Alta.UbicacionDestino !== null)
        $("#sltUbicacionDerivado").val(Alta.UbicacionDestino.Id)

    $("#txtIdAltaUrgencia").val(Alta.Id)
    $("#txaIndicacionesAlta").val(Alta.IndicacionesMedicas)
    $("#txaMedicamentosAlta").val(Alta.MedicamentosAlta)


}
function editarAltaUrgencia(idAltaMedica) {
    promesaAjax("GET", `URG_Atenciones_Urgencia/${objetoAtencionUrgenciaGlobal.idAtencion}/Alta/Medica/${idAltaMedica}`).then(async res => {
        await linkEgresoIngresoUrgencia(objetoAtencionUrgenciaGlobal);
        $("#alertEditandoAlta").append(`<div class='alert alert-warning mt-4 mb-4 text-center'>
                                            <strong><i class="fa fa-info-circle fa-lg"></i> Usted está editando un alta médica.</strong>
                                        </div>`);
        cargarAltaMedicaEditar(res);
        if (res.Diagnosticos != null && res.Diagnosticos.length > 0) {
            cargarDiagnostico(res.Diagnosticos);
        }
    }).catch(error => {
        console.error("Ha ocurrido un error al intentar obtener el alta medica por su id")
        console.log(error)
    })
}
function imprimirRecetaAltaMedica(IdAtencionUrgencia) {
    if (IdAtencionUrgencia == null)
        throw new Error("No se paso id alta medica")
    ImprimirApiExternoAsync(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${IdAtencionUrgencia}/Imprimir/Receta`)
}