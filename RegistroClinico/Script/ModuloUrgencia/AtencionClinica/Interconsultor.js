﻿$(document).ready(async function () {

    nSession = getSession();

    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 22 || nSession.CODIGO_PERFIL == 21 ||
        nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {

        mostrarOcultarInputSolicitud(true)
        await actualizarSolicitudesInterconsultor(nSession.ID_INGRESO_URGENCIA)
        await inicializarAutoCompleteMedicosInterconsultor()
    }

    $("#btnGuardarInterconsultorUrg").unbind().on('click', async function (event) {
        event.preventDefault(); // Previene la recarga de la página
    });

    $('#mdlInterconsultorUrg').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divInterconsultorUrg");
    });

})

function setTypeAheadInterconsultorUrg(data, element) {

    let templ =
        '<span>' +
        '<span class="Id" value="{{Id}}">{{Valor}}</span>' +
        '</span>';

    let typeAhObject = {
        input: element,
        minLength: 3,
        maxItem: 12,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el profesional especificado ({{query}})",
        display: ["Valor"],
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                idMedicoSolicita = item.Id

                $(element).on("input", function () {
                    let textoIngresado = $(this).val();
                    let elementosLista = $(".typeahead__list .typeahead__item");

                    elementosLista.each(function (index, e) {

                        if (textoIngresado.toUpperCase() === $(e).text())
                            coincide = true
                        else
                            coincide = false
                    })

                });

                $(element).attr("data-idprofesional", idMedicoSolicita)
                $(element).data("idprofesional", idMedicoSolicita)
                $(element).attr("data-valor")
                $(element).data("valor", item.Valor)

            },
            onCancel: function (node, item, event) {
                $(element).val(""); // Vaciar el input de búsqueda
            }
        }
    };

    $(element).on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    })

    return typeAhObject;
}

async function inicializarAutoCompleteMedicosInterconsultor() {

    const profesionales = await getProfesionalesInterconsultorUrg()
    profesionalesIdValor = await getProfesionalesIdValorInterconsultorUrg(profesionales)

    $("#sltMedicoInterconsultor").typeahead(setTypeAheadInterconsultorUrg(profesionalesIdValor, '#sltMedicoInterconsultor'));

}

async function getProfesionalesInterconsultorUrg() {

    const parametrizacion = {
        url: `${GetWebApiUrl()}GEN_Profesional/Buscar?idProfesion=1&idEstablecimiento=1`,
        method: 'GET'
    }

    const profesionales = await $.ajax({
        type: parametrizacion.method,
        url: parametrizacion.url,
        async: false,
        success: function (data) {
        }
    });

    return profesionales
}

async function getProfesionalesIdValorInterconsultorUrg(profesionales) {

    const profesionalIdValor = profesionales.map(profesional => {
        return {
            Id: profesional.Id,
            Valor: `${profesional.Persona.Nombre} ${profesional.Persona.ApellidoPaterno} ${profesional.Persona.ApellidoMaterno ?? ""}`
        }
    })

    return profesionalIdValor
}

async function actualizarSolicitudesInterconsultor(idAtencionUrgencia) {

    const solicitudesInterconsultores = await getSolicitudesInterconsultorUrgencia(idAtencionUrgencia)
    mostrarOcultarIndicacionesUrg(solicitudesInterconsultores, "#divSolicitudInterconsultor")
    await dibujarSolicitudesInterconsultorUrg(solicitudesInterconsultores)
}

function mostrarOcultarInputSolicitud(oculto) {

    if (oculto) {

        $("#divAtencionInterconsultorUrg").hide()
        $("#divFechaSolicitanteIterconsultor").hide()
        $("#divProfesionalRealizaInterconsultor").hide()
        $("#divInterconsultorUrg select, #divInterconsultorUrg textarea").prop("disabled", false)
        $("#divInterconsultorUrg select, #divInterconsultorUrg textarea").prop("data-required", true)
    }
    else {
        $("#divAtencionInterconsultorUrg").show()
        $("#divFechaSolicitanteIterconsultor").show()
        $("#divProfesionalRealizaInterconsultor").show()
        $("#divInterconsultorUrg select, #divInterconsultorUrg input:not(#sltMedicoInterconsultor), #divInterconsultorUrg textarea:not(#txtAtencionInterconsultorUrg)").prop("disabled", true)
        $("#divInterconsultorUrg select, #divInterconsultorUrg textarea").prop("data-required", false)
    }
}
async function dibujarSolicitudesInterconsultorUrg(solicitudes) {

    if (nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 22 && nSession.CODIGO_PERFIL !== 21 &&
        nSession.CODIGO_PERFIL !== 25 && nSession.CODIGO_PERFIL !== 29 && nSession.CODIGO_PERFIL !== 3 && nSession.CODIGO_PERFIL !== 30)
        return

    let html = ""

    if (solicitudes === undefined) {
        $("#tblInterconsultores tbody").addClass("text-center")
        html = `<tr>
            <td colspan="5">No hay informacion<td> 
        </tr>`
        $("#tblInterconsultores tbody").append(html)
        return
    }
    else {
        solicitudes.map((i, index) => {

            $("#tblInterconsultores tbody").empty()

            html += `
        <tr class="${pintarFilasAccionRealizadaInterconsultor(i.AccionRealizada)}">
            <td>${index + 1}</td>
            <td>${i.TipoConsultor.Valor}</td>
            <td>${moment(i.Fecha).format('DD-MM-YYYY HH:mm:ss')}</td>
            <td>${i.ActividadSolicitada}</td>
            <td>${i.FechaCierre !== null ? moment(i.FechaCierre).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
            <td>${i.ActividadRealizada ?? ""}</td>
            <td>${i.ProfesiaonlSolicita.Valor}</td>
            <td>
               <div class="row d-flex flex-row justify-content-center flex-nowrap">
                    ${i.Acciones.Cerrar ? botonesAccionCerrarSolicitud(i) : ""}
                    ${i.Acciones.Quitar ? botonEliminarSolicitud(i) : ""}
                    ${i.Acciones.Ver ? botonesVerSolicitud(i) : ""}
               </div>
            </td>
        </tr>
       `
            $("#tblInterconsultores tbody").append(html)
        })

        const cantidadSolicitudes = solicitudes.map(i => i.AccionRealizada).length
        const cantidadRealizados = await cantidadProcedimientosRealizados(solicitudes)

        if (cantidadSolicitudes === 0) {
            $("#aInterconsultor").addClass("default")
            $("#aInterconsultor").removeClass("warning")
        }
        else if (cantidadSolicitudes === cantidadRealizados && cantidadSolicitudes > 0) {
            $("#aInterconsultor").addClass("success")
            $("#aInterconsultor").removeClass("warning")
        }
        else {
            $("#aInterconsultor").addClass("warning")
            $("#aInterconsultor").addClass("default")
            $("#aInterconsultor").removeClass("success")
        }

        $("#spInterconsultor").html(`${cantidadRealizados}/${cantidadSolicitudes}`)

        $(`[data-toggle="tooltip"]`).tooltip();
    }
}

function botonesAccionCerrarSolicitud(solicitud) {

    html = `<a 
                id="btnAccionRealizadaSolicitud" 
                data-id="${solicitud.IdAtencionUrgenciaInterconsultor}"
                class="btn btn-success m-1" 
                onclick="showCerrarSolicitudInterconsultorUrg(this)" 
                data-realizado="true"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Realizar" 
                data-original-title="Acción realizada"
                >
                    <i class="fas fa-check fa-xs"></i></a> 
            <a 
                id="btnAccionNoRealizadaSolicitud" 
                data-id="${solicitud.IdAtencionUrgenciaInterconsultor}"
                class="btn btn-warning m-1" 
                onclick="showCerrarSolicitudInterconsultorUrg(this)" 
                data-realizado="false"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="No realizar" 
                data-original-title="Acción No realizada"
                >
                    <i class="fas fa-times fa-xs"></i>
                </a>`

    return html
}
function botonEliminarSolicitud(solicitud) {

    html = `<a 
                id="btnEliminarSolicitud" 
                class="btn btn-danger m-1" 
                onclick="deleteSolicitudInterconsultorUrg(${solicitud.IdAtencionUrgenciaInterconsultor})"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Eliminar"
                data-original-title="Quitar"
                >
                    <i class="fa fa-trash fa-xs"></i>
                </a>`
    return html
}
function botonesVerSolicitud(solicitud) {

    html = `<a 
                id="btnVerSolicitud" 
                data-id="${solicitud.IdAtencionUrgenciaInterconsultor}"
                data-ver="${solicitud.Acciones.Ver}"
                data-actividad-realizada="${solicitud.ActividadRealizada}"
                class="btn btn-info m-1" 
                onclick="showCerrarSolicitudInterconsultorUrg(this)" 
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Ver" 
                data-original-title="Ver solicitud"
                >
                <i class="fas fa-eye fa-xs"></i>
           </a>`

    return html
}

async function showCerrarSolicitudInterconsultorUrg(e) {

    const ver = $(e).data("ver")
    const actividadRealizada = $(e).data("actividad-realizada")

    const realizado = $(e).data("realizado")
    const id = $(e).data("id")
    $("#txtAtencionInterconsultorUrg").val("")
    const atencion = $("#txtAtencionInterconsultorUrg").val()
    $("#sltMedicoInterconsultor").attr("disabled", false)

    $("#divMedicoInterconsultor").show()

    $("#btnGuardarInterconsultorUrg").attr("data-btn-accion", "true") // botones desde tabla
    $("#btnGuardarInterconsultorUrg").data("btn-accion", "true")

    $("#btnGuardarInterconsultorUrg").attr("data-id", id)
    $("#btnGuardarInterconsultorUrg").data("id", id)

    $("#btnGuardarInterconsultorUrg").attr("data-realizado", realizado)
    $("#btnGuardarInterconsultorUrg").data("realizado", realizado)

    $("#btnGuardarInterconsultorUrg").attr("data-atencion", atencion)
    $("#btnGuardarInterconsultorUrg").data("atencion", atencion)

    if (ver) {
        $("#sltMedicoInterconsultor").attr("disabled", true)
        $("#txtAtencionInterconsultorUrg").val(actividadRealizada)
        $("#txtAtencionInterconsultorUrg").prop("disabled", true)
        $("#tituloInterconsultorUrg").html(`<i class="fas fa-eye"></i> Ver solicitud`)
        $('#btnGuardarInterconsultorUrg').hide()
    }
    else if (realizado) {
        $("#tituloInterconsultorUrg").html(`<i class="fas fa-check"></i> Realizar Atención`)
        $("#txtAtencionInterconsultorUrg").prop("disabled", false)
        $('#btnGuardarInterconsultorUrg').show()
    }
    else {
        $("#tituloInterconsultorUrg").html(`<i class="fas fa-exclamation-triangle"></i> No realizar atención`)
        $("#txtAtencionInterconsultorUrg").prop("disabled", false)
        $('#btnGuardarInterconsultorUrg').show()
    }

    comboTipoInterconsultor()

    const solicitud = await getSolicitudInterconsultorUrgencia(id)

    const { Fecha, ActividadSolicitada, ProfesiaonlSolicita, ProfesionalRealiza, IdTipoConsultor, ProfesionalInterconsultor } = solicitud

    // Nombre de profesional logeado
    let nombreProfesionalCierra = nSession.NOMBRE_USUARIO;

    await sleep(100)
    $("#sltTipoConsultorUrg").val(IdTipoConsultor).trigger("change")
    $("#sltMedicoInterconsultor").val(ProfesionalInterconsultor?.Valor ?? "")

    mostrarOcultarInputSolicitud(false)

    //$("#sltTipoConsultorUrg").val(IdTipoConsultor)
    $("#txtFechaSolicitudInterconsultor").val(`${moment(Fecha).format('DD-MM-YYYY HH:mm:ss')}`)
    $("#txtSolicitanteInterconsultor").val(ProfesiaonlSolicita.Valor)
    $("#txtProfesionalRealizaInterconsultor").val(ProfesionalRealiza ? ProfesionalRealiza.Valor : nombreProfesionalCierra)
    $("#txtSolicitudInterconsultorUrg").val(ActividadSolicitada)

    //datos de atencion paciente
    let json = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);

    let objAtencionUrgencia = {
        idAtencion: json.Id,
        idPaciente: json.IdPaciente,
        motivoConsulta: json.MotivoConsulta,
        categorizacion: categorizacionUrgencia
    }

    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteInterconsultor");

    mostrarUsuarioLogueadoEnModales("#mdlInterconsultorUrg")

    $("#mdlInterconsultorUrg").modal("show")
}

function comboTipoInterconsultor() {

    let url = `${GetWebApiUrl()}URG_Tipo_Consultor/Combo`;
    setCargarDataEnCombo(url, true, $("#sltTipoConsultorUrg"));
}

async function modalInterConsultorUrg(showModal, objAtencionUrgencia) {

    // 1 = Medico
    // 25 Matrona urgencia
    if (nSession.CODIGO_PERFIL !== perfilAccesoSistema.medico
        && nSession.CODIGO_PERFIL !== perfilAccesoSistema.matroneriaUrgencia
        && nSession.CODIGO_PERFIL !== perfilAccesoSistema.internoMedicina
        && nSession.CODIGO_PERFIL !== perfilAccesoSistema.administradorDeUrgencia) {

        Swal.fire({
            position: 'center',
            icon: 'warning',
            title: "No puede agregar solicitudes de interconsultor",
            showConfirmButton: true,
        })

        return
    }

    ShowModalCargando(true)

    // Llama a la función para mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteInterconsultor");

    if (showModal) {

        comboTipoInterconsultor()

        $("#tituloInterconsultorUrg").text("Solicitud de interconsultor")
        $('#btnGuardarInterconsultorUrg').show()
        $("#btnGuardarInterconsultorUrg").attr("data-btn-accion", "false")
        $("#btnGuardarInterconsultorUrg").data("btn-accion", "false")
        $("#divMedicoInterconsultor").hide()

        mostrarOcultarInputSolicitud(true)
        $("#txtSolicitudInterconsultorUrg").val("")
        $('#mdlInterconsultorUrg').modal('show');
    }

    ShowModalCargando(false)
}

async function guardarInterconsultorUrg(e) {
    $(e).prop('disabled', true); // Desactiva el botón para evitar múltiples clics

    try {
        // Espera 1 segundo antes de proceder
        //await new Promise(resolve => setTimeout(resolve, 1000));

        const accion = JSON.parse($("#btnGuardarInterconsultorUrg").data("btn-accion"));

        if (!validarCampos("#divInterconsultorUrg", false) && accion !== false)
            return;
        if (!validarCampos("#divMedicoInterconsultor", false))
            return


        if (accion) {
            const idUrgenciaInterconsultor = $(e).data("id");
            const AccionRealizada = $(e).data("realizado");
            const ActividadRealizada = $("#txtAtencionInterconsultorUrg").val();
            const IdProfesionalInterconsultor = $("#sltMedicoInterconsultor").data("idprofesional")

            const json = {
                idUrgenciaInterconsultor,
                AccionRealizada,
                ActividadRealizada,
                IdProfesionalInterconsultor
            };

            await patchAccionIterconsultorRealizada(json);
        } else {
            const IdTipoConsultor = $("#sltTipoConsultorUrg").val();
            const ActividadSolicitada = $("#txtSolicitudInterconsultorUrg").val();

            const interconsultor = {
                IdTipoConsultor,
                ActividadSolicitada
            };

            await postInterconsultorUrg(interconsultor);
        }
    } catch (error) {
        console.error(error);
    } finally {
        $(e).prop('disabled', false); // Reactiva el btn
    }
}

async function getSolicitudesInterconsultorUrgencia(idAtencionUrgencia) {

    if (idAtencionUrgencia === null)
        return

    try {

        const solicitudes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Interconsultores`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return solicitudes

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar solicitudes de interconsultor urgencia")
        console.log(JSON.stringify(error))
    }
}

async function getSolicitudInterconsultorUrgencia(idAtencionUrgenciaInterConsultor) {

    if (idAtencionUrgenciaInterConsultor === null)
        return

    try {

        const solicitud = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgenciaInterConsultor}/Interconsultor`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return solicitud

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar la solicitud de interconsultor urgencia")
        console.log(JSON.stringify(error))
    }
}

async function postInterconsultorUrg(interconsultor) {

    if (interconsultor === undefined)
        return

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/interconsultor`
    }

    try {
        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(interconsultor),
            contentType: 'application/json'
        });

        await actualizarSolicitudesInterconsultor(nSession.ID_INGRESO_URGENCIA)

        toastr.success("Solicitud guardada")
        $("#mdlInterconsultorUrg").modal("hide")
        resetInputInteronsultorUrg()


    } catch (error) {
        console.error('Error en la guardando Interconsultor:', error);
        throw error;
    }
}

async function patchAccionIterconsultorRealizada(interconsultor) {

    if (!validarCampos("#divFechaSolicitanteIterconsultor", false))
        return
    if (!validarCampos("#divMedicoInterconsultor", false))
        return

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Interconsultor/Cerrar?idUrgenciaInterconsultor=${interconsultor.idUrgenciaInterconsultor}`

    let body = {
        AccionRealizada: interconsultor.AccionRealizada,
        ActividadRealizada: interconsultor.ActividadRealizada,
        IdProfesionalInterconsultor: interconsultor.IdProfesionalInterconsultor
    }

    $.ajax({
        type: "PATCH",
        url: url,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {

            await actualizarSolicitudesInterconsultor(nSession.ID_INGRESO_URGENCIA)

            $("#mdlInterconsultorUrg").modal("hide")

            if (interconsultor.AccionRealizada)
                toastr.success(`Solicitud realizada`);
            else
                toastr.warning(`Solicitud no realizada`);
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar interconsultor urgencia: " + JSON.stringify(jqXHR));
        }
    });
}
async function deleteSolicitudInterconsultorUrg(idSolicitudInterconsultor) {

    if (idSolicitudInterconsultor === null || idSolicitudInterconsultor === undefined)
        return

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar la solicitud?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idSolicitudInterconsultor}/Interconsultor`
        }

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`solicitud de interconsultor eliminada`)
                $("#tblInterconsultores tbody").empty()

                await actualizarSolicitudesInterconsultor(nSession.ID_INGRESO_URGENCIA)
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar la solicitud interconsultor de urgencia: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}

function pintarFilasAccionRealizadaInterconsultor(accion) {

    let clase = ""

    if (accion !== null)
        clase = accion === true ? "table-success" : "table-warning"

    return clase
}
function resetInputInteronsultorUrg() {

    $("#sltTipoConsultorUrg").val("0")
    $("#txtSolicitudInterconsultorUrg").val("")
}

function imprimirSolicitudInterconsultorUrg() {

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Interconsultor/Imprimir`
    ImprimirApiExterno(url)
}