﻿let tipoExamen = {
    Imageonologia: 4,
    Laboratorio: 5
}
let examenLaboratorioInicializado = false, examenImagenInicializado = false
let arrayExamenesSeleccionados = [], arrayExamenesImagenSeleccionados = [], tablaExamenes, tablaImagen
let emptytempl = "No se encontraron examenes con las palabras ingresadas"
let templ = `<span><span data-id="ServicioCartera.Id">{{ServicioCartera.Valor}}</span></span>`, idTablaExamen = `#tblExamLab`
$(document).ready(async function () {
    $('#mdlExamenLaboratorio').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divObservacionesCierreLaboratorio");
    });
});

function inicializarTablasAgregarMedicamentos() {
    //inicializar tabla examenes
    tablaExamenes = $('#tblExamLab').DataTable({
        dom: "rti",
        info: false,
        columnDefs: [
            {
                targets: -1, render: function (data) {
                    return `<div class="d-flex flex-row align-items-center justify-content-center flex-nowrap p-2">
                                <button class="btn btn-danger btn-circle" type="button" onclick='eliminarExamen(${data}, this, 5)'>
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>`
                }
            }
        ]
    });
    //inicializar tabla imagenes
    tablaImagen = $('#tblExamImg').DataTable({
        dom: "rti",
        info: false,
        columnDefs: [
            {
                targets: -1, render: function (data) {
                    return `<div class="d-flex flex-row align-items-center justify-content-center flex-nowrap p-2">
                                <button class="btn btn-danger btn-circle" type="button" onclick='eliminarExamen(${data}, this, 4)'>
                                    <i class="fa fa-trash"></i>
                                </button>
                           </div>`
                }
            }
        ]
    });
}

//Limpiar la tabla cuando salen del modal
function inicializarEventoModal(modales) {
    $(modales).on('hidden.bs.modal', function (event) {
        limpiarTablaExamenes($(this).data('idtipoarancel'))
        $("#thExamenesLaboratorio").val('')
        $("#txtObservacionImagen").val('')
        $("#txtObservacionLaboratorio").val('')
        $("#sltTipoAtencionImagen").val(0)

        //limpiar campos tomografia
        $("#divMedicamentosTomografia").hide();
        $("#divAlergiasTomografia").hide();
        $("#checkMetformina").prop('checked', false);
        $("#checkLosartan").prop('checked', false);
        $("#checkEnalapril").prop('checked', false);
        $("#txtAlergiasTomografia").val('')
    })
}

async function inicializarModalExamenLaboratorio() {
    //Typeahead para buscar los examenes de laboratorio por coincidencias
    if (!examenLaboratorioInicializado) {
        let listadoExamenes = await buscarListadoExamenes(tipoExamen.Laboratorio)
        InicializarTypeAhead("#thExamenesLaboratorio", listadoExamenes, emptytempl, templ, listadoExamenes)
        examenLaboratorioInicializado = true
    }

}


async function inicializarModalExamenImagen() {
    //carga de modal de examen de imagen
    if (!examenImagenInicializado) {
        let listadoExamenesImagen = await buscarListadoExamenes(tipoExamen.Imageonologia)
        InicializarTypeAhead("#thExamenesImagenologia", listadoExamenesImagen, emptytempl, templ, listadoExamenesImagen)
        examenImagenInicializado = true
    }
}

async function buscarExamenesImagenPorEvento(idEvento) {
    let data = await promesaAjax("GET", `RCE_Solicitud_Imagenologia/Buscar?idEvento=${idEvento}`)
    return data
}

async function buscarExamenesPorEvento(idEvento) {
    let data = await promesaAjax("GET", `RCE_Solicitud_Laboratorio/Buscar?idEvento=${idEvento}`)
    return data
}

function validarExamenIngresado(idTipoArancel, input) {
    /*Esta funcion recibe informacion desde el typeahead de examenes pero tambien (un objeto) desde la botonera de los examenes favoritos
    Permite ingresar examenes a la solicitud de examenes de laboratorio o de imagen*/
    let objeto;
    //Por aca pasa cuando viene desde los favoritos, no desde el typeahead
    if (typeof input === 'object' && input !== null && Object.keys(input).length > 0) {
        if (input.IdCarteraServicio && input.DescripcionCarteraServicio) {
            objeto = {
                IdCarteraServicioArancel: input.IdCarteraServicioArancel,
                NombreExamen: input.DescripcionCarteraServicio
            }
        } else {
            console.error('El objeto recibido no tiene las propiedades idCartera y nombreCartera necesarias.');
            return null;
        }
    } else {//Aca pasa cuando se agrega desde el tipeahead
        const inputValor = $(input).val();
        const inputId = $(input).data("id-cartera");

        if (inputValor.trim() !== "") {
            objeto = {
                IdCarteraServicioArancel: inputId,
                NombreExamen: inputValor
            };
        } else {
            console.error('El input está vacío o no se proporcionó un objeto válido.');
            toastr.error("Debe seleccionar un examen.")
            return null;

        }
    }
    agregarExamen(idTipoArancel, objeto)
}

function agregarExamen(idTipoArancel, object) {
    //IdCarteraServicioArancel, NombreExamen
    console.log(idTipoArancel)
    let element = idTipoArancel == 5 ? "#thExamenesLaboratorio" : "#thExamenesImagenologia"
    let coincidencia;
    coincidencia = idTipoArancel == 5 ? arrayExamenesSeleccionados.find(x => x.IdCarteraServicioArancel == object.IdCarteraServicioArancel) : arrayExamenesImagenSeleccionados.find(x => x.IdCarteraServicioArancel == object.IdCarteraServicioArancel)
    console.log(coincidencia)
    if (coincidencia == undefined) {

        if (idTipoArancel == 5) {
            arrayExamenesSeleccionados.push(object)
            tablaExamenes.row.add([
                object.NombreExamen,
                object.IdCarteraServicioArancel
            ]).draw(false)
        } else {
            arrayExamenesImagenSeleccionados.push(object)
            tablaImagen.row.add([
                object.NombreExamen,
                object.IdCarteraServicioArancel
            ]).draw(false)
        }

        toastr.success("Examen agregado a la solicitud.")

        $(element).removeData("id")
        $(element).removeData("id-cartera")
        $(element).val("")
    } else {
        toastr.error("No puede agregar el mismo examen en dos veces.")
    }
}

async function guardarSolicitud(idTipoArancel) {
    let ingresoExamenes = false, urlPeticion = `${GetWebApiUrl()}`, mensajeExito = `Se ha ingresado la solicitud de examenes de `
    let modalExamen = "#mdlExamenes", cargarImagen = false, cargarLaboratorio = false
    if (idTipoArancel == tipoExamen.Laboratorio) {
        ingresoExamenes = arrayExamenesSeleccionados.length > 0 ? true : false
        urlPeticion += `RCE_Solicitud_Laboratorio/${idEventoUrgencia}`
        mensajeExito += `laboratorio`
        cargarLaboratorio = true
    } else if (idTipoArancel == tipoExamen.Imageonologia) {
        modalExamen = "#mdlImagenologia"
        ingresoExamenes = arrayExamenesImagenSeleccionados.length > 0 ? true : false
        urlPeticion += `RCE_Solicitud_Imagenologia/${idEventoUrgencia}`
        mensajeExito += `imagenologia`
        cargarImagen = true
    }

    if (ingresoExamenes) {
        let esValido = true
        let jsonEnviar = {
            IdAmbito: 3,
            IdEvento: idEventoUrgencia
        }
        jsonEnviar.Examenes = idTipoArancel == tipoExamen.Laboratorio ? arrayExamenesSeleccionados : arrayExamenesImagenSeleccionados
        if (idTipoArancel == tipoExamen.Laboratorio && $("#txtObservacionLaboratorio").val() !== "") {
            jsonEnviar.Observacion = $("#txtObservacionLaboratorio").val()
        }
        if (idTipoArancel == tipoExamen.Imageonologia && $("#txtObservacionImagen").val() !== "") {
            jsonEnviar.Observacion = $("#txtObservacionImagen").val()
        }
        //Esto solo cuando es imagen, y la solicitud es de tomografia computada
        esValido = validarCampos("#infoAtencionImagen", false)
        if ($("#sltTipoAtencionImagen").val() == 2 && idTipoArancel == tipoExamen.Imageonologia && esValido) {
            jsonEnviar.Metformina = $('#checkMetformina').is(':checked')
            jsonEnviar.Losartan = $('#checkLosartan').is(':checked')
            jsonEnviar.Enalapril = $('#checkEnalapril').is(':checked')
            jsonEnviar.ReaccionAlergica = $("#txtAlergiasTomografia").val() !== "" ? $("#txtAlergiasTomografia").val() : null
            urlPeticion += "/Tomografia"
        }
        jsonEnviar.IdTipoSolicitudImagenologia = parseInt($("#sltTipoAtencionImagen").val())

        if (esValido) {
            // Deshabilitar el botón
            $("#btnSolicitudExamenesUrg").prop('disabled', true); // lab
            $("#btnGuardarSolicitudExamenImagenologiaUrg").prop('disabled', true); // img

            $.ajax({
                url: urlPeticion,
                data: jsonEnviar,
                method: "POST",
                success: function (data) {
                    Swal.fire({
                        icon: 'info',
                        title: 'Ingreso exitoso',
                        text: mensajeExito
                    }).then(confirm => {
                        $(modalExamen).modal("hide")

                        // hab btn nuevamente al cerrar modal
                        $("#btnSolicitudExamenesUrg").prop('disabled', false); // lab
                        $("#btnGuardarSolicitudExamenImagenologiaUrg").prop('disabled', false); // img

                        dibujarTablaExamenes(idEventoUrgencia, cargarLaboratorio, cargarImagen)
                    });
                }, error: function (error) {
                    console.error("Error al guardar una solicitud de laboratorio")
                    console.log(error)
                    // hab btn nuevamente si hay un error
                    $("#btnSolicitudExamenesUrg").prop('disabled', false); // lab
                    $("#btnGuardarSolicitudExamenImagenologiaUrg").prop('disabled', false); // img
                }
            })
        }
    } else {
        toastr.error("No ha seleccionado ningun examen.")
    }
}


function eliminarExamen(id, tr, idTipoArancel) {
    let trcerca = $(tr).closest('tr');
    if (idTipoArancel == 5) {
        tablaExamenes.row(trcerca).remove().draw(false);
        arrayExamenesSeleccionados = arrayExamenesSeleccionados.filter(x => x.IdCarteraServicioArancel !== id)
    } else {
        tablaImagen.row(trcerca).remove().draw(false);
        arrayExamenesImagenSeleccionados = arrayExamenesImagenSeleccionados.filter(x => x.IdCarteraServicioArancel !== id)
    }

}
async function buscarListadoExamenes(tipoCartera) {
    let listadoExamenes = await promesaAjax("GET", `GEN_Cartera_Servicio/Combo?idTipoArancel[0]=${tipoCartera}`)
    return listadoExamenes
}

function mostrarModalExamenes(idTipoArancel) {
    if (idTipoArancel == 5) {
        inicializarModalExamenLaboratorio()

        mostrarUsuarioLogueadoEnModales("#mdlExamenes")

        $("#mdlExamenes").modal("show")
    } else {
        inicializarModalExamenImagen()

        mostrarUsuarioLogueadoEnModales("#mdlImagenologia")

        $("#mdlImagenologia").modal("show")
    }

}
function limpiarTablaExamenes(idTipoArancel) {
    if (idTipoArancel == 5) {
        tablaExamenes.clear().draw()
        arrayExamenesSeleccionados = []
    } else {
        tablaImagen.clear().draw()
        arrayExamenesImagenSeleccionados = []
    }
}
function eliminarSolicitudExamenUrgencia(idSolicitud, idTipoArancel) {
    let cargarImagenTabla = true, cargarLaboratorioTabla = false
    Swal.fire({
        title: "Eliminando una solicitud de examen.",
        text: "Está a punto de eliminar una solicitud de examenes, por favor confirme la acción.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        confirmButtonText: "Eliminar"
    }).then((result) => {
        urlPeticion = `${GetWebApiUrl()}RCE_Solicitud_Imagenologia/${idSolicitud}/Eliminar`
        if (idTipoArancel == tipoExamen.Laboratorio) {
            urlPeticion = `${GetWebApiUrl()}RCE_Solicitud_Laboratorio/${idSolicitud}/Eliminar`
            cargarImagenTabla = false, cargarLaboratorioTabla = true
        }

        if (result.value == true) {
            $.ajax({
                method: `patch`,
                url: urlPeticion,
                success: function (data) {
                    Swal.fire("Eliminado!", "La solicitud de examen ha sido eliminada.", "success");
                    dibujarTablaExamenes(idEventoUrgencia, cargarLaboratorioTabla, cargarImagenTabla)
                }, error: function (error) {
                    console.error("Errro al eliminar la solicitud")
                    console.log(error)
                }
            })

        }
    });
}

// Función para pintar el encabezado del modal
function pintaHeaderModalLaboratorio(dataModal) {

    $("#mdlExamenLaboratorio .modal-header h4").text(`${dataModal.originalTitle}`);

    if (dataModal.originalTitle === "Acción realizada") {
        $("#mdlExamenLaboratorio .modal-header").addClass("modal-header-success").removeClass("modal-header-warning modal-header-info");
        $("#mdlExamenLaboratorio .modal-header h4").html(`<i class="fas fa-check"></i> ${dataModal.originalTitle}`);

    } else if (dataModal.originalTitle === "Acción No realizada") {
        $("#mdlExamenLaboratorio .modal-header").addClass("modal-header-warning").removeClass("modal-header-success modal-header-info");
        $("#mdlExamenLaboratorio .modal-header h4").html(`<i class="fas fa-exclamation-triangle"></i> ${dataModal.originalTitle}`);

    } else if (dataModal.originalTitle === "Ver examen") {
        $("#mdlExamenLaboratorio .modal-header").removeClass("modal-header-warning modal-header-success")
        $("#mdlExamenLaboratorio .modal-header").addClass("modal-header-info");
        $("#mdlExamenLaboratorio .modal-header h4").html(`<i class="fas fa-eye"></i> ${dataModal.originalTitle}`);
    }
}

async function showCerrarExamenUrg(idSolicitud, idTipoArancel, tipo) {
    const dataModal = {
        originalTitle: tipo
    };

    let laboratorio = null;
    let tituloModal = "";

    if (idTipoArancel === 5) {
        laboratorio = await getSolicitudesLaboratorioPorId(idSolicitud);
        tituloModal = 'Detalles de laboratorio';
    } else {
        laboratorio = await getSolicitudesImagenologiaPorId(idSolicitud);
        tituloModal = 'Detalles de imagenología';
    }

    if (!laboratorio) {
        console.error('No se encontraron datos para la solicitud.');
        return;
    }

    const { Nombre, ApellidoPaterno, ApellidoMaterno } = laboratorio.Profesional.Persona;
    const nombreProfesional = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`;

    let cierreSolicitud = null;
    if (idTipoArancel === 5) {
        cierreSolicitud = laboratorio.CierreSolicitudLaboratorio;
    } else {
        cierreSolicitud = laboratorio.CierreSolicitudImagenologia;
    }

    let nombreProfesionalCierra = nSession.NOMBRE_USUARIO; // Se pinta el usuario logeado si el caso sigue abierto
    if (cierreSolicitud && cierreSolicitud.ProfesionalRealiza) {
        const { Nombre, ApellidoPaterno, ApellidoMaterno } = cierreSolicitud.ProfesionalRealiza.Persona;
        nombreProfesionalCierra = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`;
    }

    const horaSolicitud = moment(laboratorio.Fecha).format('HH:mm:ss');
    const horaCierre = cierreSolicitud?.FechaRealizacion ? moment(cierreSolicitud.FechaRealizacion).format('HH:mm:ss') : "Automática al guardar";
    let badgeRealizado = "";

    $("#txtObservacionesCierreLaboratorio").val("");

    pintaHeaderModalLaboratorio(dataModal);

    const isAccionNoRealizada = dataModal.originalTitle === 'Acción No realizada';

    const accionRealizada = cierreSolicitud ? cierreSolicitud.AccionRealizada : null;
    const observacionCierre = cierreSolicitud ? cierreSolicitud.ObservacionCierre : "Sin información";

    if (accionRealizada === null) {
        if (isAccionNoRealizada) {
            $("#txtObservacionesCierreLaboratorio").attr("data-required", true);
        } else {
            $("#txtObservacionesCierreLaboratorio").attr("data-required", false);
        }
        $("#txtObservacionesCierreLaboratorio").attr("readonly", false);
        $("#btnCerrarExamen").show();
    } else {
        badgeRealizado = accionRealizada ? `
            <span id="bgTrue" class="badge badge-pill btn-success badge-count ml-2" readonly>
                ${accionRealizada ? 'Realizado' : ''}
            </span>`
            :
            `<span id="bgFalse" class="badge badge-pill badge-count ml-2" style="color: #ffffff; background-color: orange;" readonly>
                ${!accionRealizada ? 'No realizado' : ''}
            </span>`;

        $("#txtObservacionesCierreLaboratorio").attr("data-required", false);
        $("#txtObservacionesCierreLaboratorio").val(observacionCierre);
        $("#txtObservacionesCierreLaboratorio").attr("readonly", true);
        $("#btnCerrarExamen").hide();
    }

    let tomografiaDetalle = '';
    if (idTipoArancel !== 5 && laboratorio.Tomografia) {
        tomografiaDetalle = `
            <div class="row mt-2">
                <div class="col-sm-4">
                    <label>Metformina:</label> 
                    <span class="form-control form-control-no-height" readonly>${laboratorio.Tomografia.Metformina ? 'Sí' : 'No'}</span>
                </div>
                <div class="col-sm-4">
                    <label>Losartan:</label> 
                    <span class="form-control form-control-no-height" readonly>${laboratorio.Tomografia.Losartan ? 'Sí' : 'No'}</span>
                </div>
                <div class="col-sm-4">
                    <label>Enalapril:</label> 
                    <span class="form-control form-control-no-height" readonly>${laboratorio.Tomografia.Enalapril ? 'Sí' : 'No'}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12">
                    <label>Reacción Alérgica:</label>
                    <span class="form-control form-control-no-height" readonly>${laboratorio.Tomografia.ReaccionAlergica ? laboratorio.Tomografia.ReaccionAlergica : 'No'}</span>
                </div>
            </div>`;
    }

    let examenesListados = '';
    if (laboratorio.Solicitud && laboratorio.Solicitud.Examen && laboratorio.Solicitud.Examen.length > 0) {
        examenesListados = `<span class="form-control form-control-no-height" readonly><ul style="padding-left: 0; list-style-position: inside;">`;
        laboratorio.Solicitud.Examen.forEach(examen => {
            examenesListados += `<li style="text-align: left;">${examen.Valor}</li>`;
        });
        examenesListados += `</ul></span>`;
    } else {
        examenesListados = '<span class="form-control form-control-no-height" readonly>Sin información</span>';
    }

    $("#divDetalleLaboratorio").html(`
        <div class="d-flex align-items-center">
            <h5 class="mb-0"><strong>${tituloModal}</strong></h5>
            ${badgeRealizado}
        </div>

        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Solicitante:</label> <span class="form-control form-control-no-height" readonly>${nombreProfesional}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de solicitud:</label> <span class="form-control form-control-no-height" readonly>${horaSolicitud}</span>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Profesional que cierra:</label>
                <span class="form-control" readonly>${nombreProfesionalCierra ?? ""}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de cierre:</label> <span class="form-control form-control-no-height" readonly>${horaCierre}</span>
            </div>
        </div>

        <label>Observaciones al ingreso:</label>
        <textarea
            id="txtObservacionesAdicionalesLaboratorio"
            class="form-control" rows="3"
            placeholder="Escriba observaciones acerca del caso"
            maxlength="500"
            readonly>${laboratorio.Observacion ?? "Sin información"}
        </textarea>

        <label>Examenes:</label>
        ${examenesListados}
        
        ${tomografiaDetalle}
    `);

    $("#btnCerrarExamen").data("id", laboratorio.Id);
    $("#btnCerrarExamen").data("realizada", dataModal.originalTitle);
    $("#btnCerrarExamen").data("tipoArancel", idTipoArancel);

    $("#btnCerrarExamen").unbind().on('click', async function (event) {
        event.preventDefault();
        $(this).prop('disabled', true); // Deshabilita btn
        try {
            realizarAccionLaboratorioUrg(this);
        } catch (error) {
            console.error(error);
        } finally {
            $(this).prop('disabled', false); // Habilita el btn nuevamente después de la acción
        }
    });

    $("#btnCerrarExamen").html(`<i class="fas fa-save"></i> Guardar`);

    $("#mdlExamenLaboratorio").modal("show");
}


async function realizarAccionLaboratorioUrg(e) {
    if (!validarCampos("#divObservacionesCierreLaboratorio", false))
        return;

    const textoAccion = $(e).data("realizada");
    const Id = $(e).data("id");
    const idTipoArancel = $(e).data("tipoArancel");
    const IdAtencionUrgencia = nSession.ID_INGRESO_URGENCIA;
    const ObservacionCierre = $("#txtObservacionesCierreLaboratorio").val();
    let AccionRealizada = false;

    if (textoAccion === "Acción realizada")
        AccionRealizada = true;

    const json = {
        Id,
        IdAtencionUrgencia,
        ObservacionCierre,
        AccionRealizada,
        textoAccion
    };

    ShowModalCargando(true);

    if (idTipoArancel === 5) {
        await patchAccionLaboratorioRealizada(json);
    } else {
        await patchAccionImagenologiaRealizada(json);
    }

    ShowModalCargando(false);
}

async function patchAccionLaboratorioRealizada(json) {
    const url = `${GetWebApiUrl()}RCE_Solicitud_Laboratorio/${json.Id}/Cerrar`;

    let body = {
        AccionRealizada: json.AccionRealizada,
        ObservacionCierre: json.ObservacionCierre
    }

    $.ajax({
        type: "PATCH",
        url: url,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            $("#mdlExamenLaboratorio").modal("hide")

            if (json.textoAccion === "Acción realizada")
                toastr.success(`Laboratorio realizado`);
            else if (json.textoAccion === "Acción No realizada")
                toastr.warning(`Laboratorio no realizado`);

            await dibujarTablaExamenes(idEventoUrgencia, true, false)
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar el lab: " + JSON.stringify(jqXHR));
        }
    });
}

async function patchAccionImagenologiaRealizada(json) {
    const url = `${GetWebApiUrl()}RCE_Solicitud_Imagenologia/${json.Id}/Cerrar`;

    let body = {
        AccionRealizada: json.AccionRealizada,
        ObservacionCierre: json.ObservacionCierre
    }

    $.ajax({
        type: "PATCH",
        url: url,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            $("#mdlExamenLaboratorio").modal("hide")

            if (json.textoAccion === "Acción realizada")
                toastr.success(`Imagenología realizado`);
            else if (json.textoAccion === "Acción No realizada")
                toastr.warning(`Imagenología no realizado`);

            await dibujarTablaExamenes(idEventoUrgencia, false, true)
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar la img: " + JSON.stringify(jqXHR));
        }
    });
}

// ACCIONES REALIZAR Y NO REALIZAR laboratorio
async function getSolicitudesLaboratorioPorId(idSolicitud) {
    try {
        const laboratorio = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Solicitud_Laboratorio/${idSolicitud}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return laboratorio;
    } catch (error) {
        if (error.status === 400) {
            return;
        }
        console.error("Error al cargar laboratorios de urgencia");
        console.log(JSON.stringify(error));
    }
}

// ACCIONES REALIZAR Y NO REALIZAR iagenoloigia
async function getSolicitudesImagenologiaPorId(idSolicitudImagenologia) {
    try {
        const imagenologia = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Solicitud_Imagenologia/${idSolicitudImagenologia}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return imagenologia;
    } catch (error) {
        if (error.status === 400) {
            return;
        }
        console.error("Error al cargar imagenologias de urgencia");
        console.log(JSON.stringify(error));
    }
}

function dibujarTablaExamenesIngresados(data, tabla, columnas, columnasdefs) {
    $(tabla).DataTable({
        dom: "lrtip",
        "rowCallback": function (row, data, index) {
            $node = this.api().row(row).nodes().to$();

            let claseRealizado = "";

            if (data.CierreSolicitudLaboratorio != null) {
                claseRealizado = pintarFilasAccionRealizadaProcedimiento(data.CierreSolicitudLaboratorio.AccionRealizada);
            } else if (data.CierreSolicitudImagenologia != null) {
                claseRealizado = pintarFilasAccionRealizadaProcedimiento(data.CierreSolicitudImagenologia.AccionRealizada);
            }

            $node.addClass(claseRealizado);
        },
        data: data,
        destroy: true,
        columns: columnas,
        columnDefs: columnasdefs
    });
}

async function examenesLaboratorioPendientes(idTipoAtencion) {
    let examenesLaboratorioPendiente = await promesaAjax("GET", `RCE_Solicitud_Laboratorio/Pendientes/Buscar?idTipoAtencion=${idTipoAtencion}`)
    return examenesLaboratorioPendiente
}


async function examenesImagenologiaPendientes(idTipoAtencion) {
    let examenesImagenPendiente = await promesaAjax("GET", `RCE_Solicitud_Imagenologia/Pendientes/Buscar?idTipoAtencion=${idTipoAtencion}`)
    return examenesImagenPendiente
}

function prepararListadoExamenesTablaPendientes(listadoExamenes) {
    //en esta funcion se ordena y prepara la informacion para mostrar los examenes pendientes en la bandeja de urgencia y en el map ade camas
    if (listadoExamenes == null || listadoExamenes == undefined)
        throw new Error("El listado de examenes pasado a la funcion esta vacio o es nulo")

    let examenesPreparadosParaTabla = listadoExamenes.map(x => {
        return {
            Id: x.idAtencion,
            Documento: x.Paciente !== "" ? `${x.Paciente.NumeroDocumento}-${x.Paciente.Digito}` : `No registra`,
            Nombre: x.Paciente !== "" ? `${x.Paciente.Nombre} ${x.Paciente.ApellidoPaterno} ${x.Paciente.ApellidoMaterno}` : `No registra`,
            Lugar: x.Lugar !== "" ? `${x.Lugar.Box.Valor} ${x.Lugar.TipoBox.Valor}` : `No registra`,
            Categorizacion: x.Categorizacion !== null && x.Categorizacion.length > 0 ? x.Categorizacion[0].Codigo : ``,
            Examenes: x.ExamenesPendientes,
            Completadas: `${x.Completadas} de ${x.Pendientes}`
        }
    })
    return examenesPreparadosParaTabla
}

function obtenerColumnasExamenesPendientes() {
    let columnas = [
        { title: "N° Documento", data: "Documento" },
        { title: "Paciente", data: "Nombre" },
        { title: "Lugar", data: "Lugar" },
        { title: "Categorizacion ", data: "Categorizacion" },
        { title: "Examenes Pendientes", data: "Examenes" },
        { title: "Ver", data: "Id" }
    ]
    return columnas
}
function obtenerColumnasDefsExamenesPendientes() {
    let columnasDefs = [
        {
            targets: [4],
            render: function (examenes) {
                if (examenes.length == 0 || examenes == null)
                    throw new Error(`Hay un error en la solicitud, no contiene examenes`)

                let examenesAgrupados = ``
                examenesAgrupados += examenes.map(examen =>
                    examen.Examenes.map(x => `<li class="list-group-item text-left"><i class="fa fa-circle mr-2" style="color:#ffc107;font-size:11px;" aria-hidden="true"></i>${x.DescripcionCarteraServicio}</li>`).join(" ")
                ).join(" ")
                return examenesAgrupados
            }
        }, {
            targets: [3],
            render: function (codigo) {
                if (codigo == null)
                    return `El codigo de categorizacion fue nulo`
                return dibujarIconoCategorizacion({ Codigo: codigo }, {
                    heading: "h4",
                    badge: ""
                })
            }
        }, {
            targets: -1,
            render: function (id) {
                return `<a onclick="irAtencionClinica(${id})" class="btn btn-info">
                                <i class="fas fa-eye fa-xs"></i>
                            </a>`
            }

        }
    ]
    return columnasDefs
}

function pintarFilasAccionRealizadaProcedimiento(accion) {

    let clase = ""

    if (accion !== null)
        clase = accion === true ? "table-success" : "table-warning"

    return clase
}