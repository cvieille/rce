﻿
async function IrASolicitudPabellon(idPaciente, idEventoUrgencia) {
    const token = GetToken().replace("Bearer ", "");
    const a = GetTextoEncriptado(`${idPaciente}|${idEventoUrgencia}|${token}`);
    const urlPab = await getUrlPabellon();
    window.open(`${urlPab}MetodosGrales/IniciarSesion.ashx?a=${a}`, '_blank');
    $("#mdlFormulariosExternos").modal("hide");
    window.addEventListener("message", function (event) {
        if (event.data === "Forbidden") {
            //Actualziar tabla de formularios externos
            cargarTablaFormulariosExternos(true);
        }
    });
}

function cargarTablaFormulariosExternos(irAHistorial) {

    $("#divHistorialFormularios").hide();
    promesaAjax("GET", `PAB_Formulario_Ind_Qx/Buscar?idEvento=${idEventoUrgencia}`).then(res => {

        if (res.length > 0 && res !== null) {

            if ($("#divHistorialFormularios").hasClass("d-none"))
                $("#divHistorialFormularios").removeClass("d-none");

            let datosPreparadosFormularios = res.map(elemento => {
                return {
                    Id: elemento.Id,
                    Diagnostico: elemento.Diagnostico,
                    Prestacion: elemento.Prestacion,
                    Estado: elemento.TipoEstado.Valor,
                    Uca: elemento.Uca
                }
            });

            $("#tblHistorialFormulario").DataTable({
                data: datosPreparadosFormularios,
                destroy: true,
                columns: [
                    { title: "Id", data: "Id" },
                    { title: "Diagnostico", data: "Diagnostico" },
                    { title: "Prestacion", data: "Prestacion" },
                    { title: "Estado", data: "Estado" },
                    { title: "Uca", data: "Uca" },
                    { title: "Acciones", data: "Id" },
                ], columnDefs: [
                    {
                        targets: -1, render: function (id) {
                            return `<button type="button" class="btn btn-info" onclick="imprimeFormularioIQX(${id})"> <i class="fa fa-print"></i> </button>`
                        }
                    }
                ]
            });

            $("#divHistorialFormularios").show();
            if (irAHistorial) {
                $('html, body').animate({ scrollTop: $("#divHistorialFormularios").offset().top - 10 }, 500);
                Swal.fire({
                    title: 'Solicitud Pabellón',
                    html: `¿Desea imprimir la última Solicitud Pabellón creada?`,
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Imprimir',
                    cancelButtonText: 'Cancelar',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value)
                        imprimeFormularioIQX(res[res.length - 1].Id);
                });
            }
        }

    }).catch(error => {
        console.error("Error al consultar los formularios externos por id Evento")
        console.log(error)
    })
}