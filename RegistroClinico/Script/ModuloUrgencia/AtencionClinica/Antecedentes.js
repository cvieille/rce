﻿$(document).ready(async function () {

    nSession = getSession();

    if (validaPerfil()) {
        mostrarOcultarInputSolicitud(true)
        await actualizarSolicitudesInterconsultor(nSession.ID_INGRESO_URGENCIA)
        await InicializarDetallesAlergias()
    }
    
})

async function modalAntecendetes(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);

    $("#spnProfesionalEvolucionUrg").text($("#sltProfesionEvolucionAtencion :selected").text());
    $("#spnNombreProfesionalUrg").text($("#txtProfesionalEvolucionAtencion").val());

    // Llama a la función para mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteAntecedentesUrg");

    if (showModal) {
        await mostrarUrgenciaAntecedentes(objAtencionUrgencia);
        const json = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);

        const validarPerfil = validaPerfil();

        // Función para manejar el estado de los switches de alergias
        const setAlergiaSwitches = (alergiaType) => {
            $("#rdoAlergiaSi").bootstrapSwitch('state', alergiaType === 1);
            $("#rdoAlergiaNo").bootstrapSwitch('state', alergiaType === 2);
            $("#rdoAlergiaDesconocido").bootstrapSwitch('state', alergiaType === 3);

            if (!validarPerfil) {
                $("#rdoAlergiaSi").bootstrapSwitch('disabled', alergiaType !== 1);
                $("#rdoAlergiaNo").bootstrapSwitch('disabled', alergiaType !== 2);
                $("#rdoAlergiaDesconocido").bootstrapSwitch('disabled', alergiaType !== 3);
            }
        };

        if (json.TipoAlternativa !== null) {
            setAlergiaSwitches(json.TipoAlternativa.Id);
            $("#divAntecedenteModal_3").show();
        }

        $('#mdlAntecedentesUrg').modal('show');
    }

    ShowModalCargando(false);
}


async function mostrarUrgenciaAntecedentes({ tipoAtencion }) {

    $("#divModalUrgenciaAntecedentes").empty();
    $("#divModalUrgenciaCategorizacionAlergia").empty()

    const data = await getAntecedentesComboUrg(tipoAtencion.Id)
    const antecedentesCargados = await getAntecedentesUrg(nSession.ID_INGRESO_URGENCIA)

    data.forEach((antecedente, index) => {

        const antecedenteCargado = antecedentesCargados.find(a => a.ClasificacionAntecedente.Id === antecedente.Id);
        const valorTextarea = antecedenteCargado ? antecedenteCargado.UrgenciaClasificacionAntecedente.Valor : '';

        const valida = validaPerfil()
        const disabled = !valida ? `disabled="disabled"` : ""
        !valida ? $("#btnGuardarAntecedentesUrg").hide() : null
        //!valida ? $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch('disabled', true) : null

        if (antecedente.Id !== 1 && antecedente.Id !== 3) { // 1: Antecedente mórbido, 3: Alergias
            $("#divModalUrgenciaAntecedentes").append(
                `<div class="form-row mt-2">
                            <div id="divAntecedenteModal_${antecedente.Id}" class="col-md-12 mt-3">
                                <strong>${antecedente.Valor}</strong>
                                <textarea id="txtAntecedenteModal_${antecedente.Id}" 
                                          data-id-antecedente='${antecedente.Id}' 
                                          class="form-control" 
                                          placeholder="Escriba detalles" 
                                          rows="3" maxlength="1000"
                                          ${disabled}>${valorTextarea}</textarea>
                            </div>
                        </div>`);
        } else {
            $("#divModalUrgenciaCategorizacionAlergia").append(`<div class="row mt-1">
                        <div id="divAntecedenteModal_${antecedente.Id}" class="col-lg-12 mt-2">
                            <strong>${antecedente.Valor}</strong>
                            <textarea id="txtAntecedenteModal_${antecedente.Id}" 
                                      data-id-antecedente='${antecedente.Id}' 
                                      class="form-control" 
                                      placeholder="Escriba detalles" 
                                      rows="3" maxlength="1000"
                                      ${disabled}>${valorTextarea}</textarea>
                        </div>`);
        }
    });

    // txt de Alergias
    $("#divAntecedenteModal_3").hide();

}

function validaPerfil() {
    const perfilesPermitidosAntecedentes = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.tensUrgencia,
        perfilAccesoSistema.enfermeraUrgencia,
        perfilAccesoSistema.matroneriaUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ];
    return perfilesPermitidosAntecedentes.includes(nSession.CODIGO_PERFIL);
    /*
    MÉDICO: 1
    MATRONERIA URGENCIA: 25
    ENFERMERA(O) DE URGENCIA: 22
    TENS DE URGENCIA: 21
    ADMINISTRADOR URGENCIA: 30
    */
    //return nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 22 ||
    //    nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 30
}

async function InicializarDetallesAlergias() {

    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").bootstrapSwitch();

    $("#rdoAlergiaSi").attr("data-id", "1");
    $("#rdoAlergiaNo").attr("data-id", "2");
    $("#rdoAlergiaDesconocido").attr("data-id", "3");
    $("#rdoAlergiaSi, #rdoAlergiaNo, #rdoAlergiaDesconocido").on('switchChange.bootstrapSwitch', function (event, state) {

        const id = $(this).attr("id");

        if (id === "rdoAlergiaSi" && $(this).bootstrapSwitch('state')) {
            $("#divAntecedenteModal_3").show();
            $("#txtAntecedenteModal_3").attr("data-required", true);
        } else if ((id === "rdoAlergiaNo" && $(this).bootstrapSwitch('state'))
            || (id === "rdoAlergiaDesconocido" && $(this).bootstrapSwitch('state'))) {
            $("#divAntecedenteModal_3").hide();
            $("#txtAntecedenteModal_3").attr("data-required", false);
            //$("#txtAntecedenteModal_3").val("");
        }

        ReiniciarRequired();

    });

    //$("#divDetalleAlergias").hide();

}

async function guardarAntecedentesUrg() {
    const alergiaSi = $("#rdoAlergiaSi").bootstrapSwitch('state');
    const alergiaNo = $("#rdoAlergiaNo").bootstrapSwitch('state');
    const alergiaDesconocido = $("#rdoAlergiaDesconocido").bootstrapSwitch('state');
    let idTipoAlternativa = 0;

    // Deshabilitar el botón
    $("#btnGuardarAntecedentesUrg").prop('disabled', true);

    if (alergiaSi)
        idTipoAlternativa = $("#rdoAlergiaSi").data("id");
    else if (alergiaNo)
        idTipoAlternativa = $("#rdoAlergiaNo").data("id");
    else if (alergiaDesconocido)
        idTipoAlternativa = $("#rdoAlergiaDesconocido").data("id");

    const textareas = $("#divAntecedentesUrg textarea:visible").filter(function () {
        return $(this).val().trim() !== "";
    });
    const antecedentes = textareas.map(function () {
        return {
            IdAntecedente: $(this).data('id-antecedente'),
            DescripcionUrgenciaAntecedente: $(this).val()
        };
    }).get();

    const antecedentesFinales = {
        IdTipoAlternativa: idTipoAlternativa,
        Antecedentes: antecedentes
    };

    if (idTipoAlternativa === 0)
        delete antecedentesFinales.IdTipoAlternativa;

    try {
        await patchUrgenciaAntecedentes(nSession.ID_INGRESO_URGENCIA, antecedentesFinales);
    } catch (error) {
        $("#btnGuardarAntecedentesUrg").prop('disabled', false);
    } finally {
        // Habilitar el botón después de la solicitud, ya sea exitosa o fallida
        $("#btnGuardarAntecedentesUrg").prop('disabled', false);
    }
}

async function getAntecedentesComboUrg(idTipoAtencion) {

    try {
        const antecedentes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idTipoAtencion}/Antecedentes/Combo`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return antecedentes

    } catch (error) {
        console.error("Error al cargar antecedentes  ")
        console.log(JSON.stringify(error))
    }
}

//Para dibujar dinamicamente los input
async function getAntecedentesUrg(idAtencion) {

    try {
        const antecedentes = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/Antecedentes`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return antecedentes

    } catch (error) {
        console.error("Error al cargar antecedentes  ")
        console.log(JSON.stringify(error))
    }
}

async function patchUrgenciaAntecedentes(idAtencion, antecedentes) {

    let parametrizacion = {
        method: "PATCH",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/Antecedentes`,
    };

    await $.ajax({
        method: parametrizacion.method,
        url: parametrizacion.url,
        data: antecedentes,
        success: async function (data) {

            toastr.success("Antecedentes actualizados");
            $("#mdlAntecedentesUrg").modal("hide");
        },
        error: function (error) {
            console.log(error);
            console.error("Error al intentar actualizar antecedentes");
        }
    });
}

