﻿$(document).ready(async function () {
    nSession = getSession();

    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 21 || nSession.CODIGO_PERFIL == 22 ||
        nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {
        await actualizarAlcoholemias(nSession.ID_INGRESO_URGENCIA)
        await actualizarToxicologico(nSession.ID_INGRESO_URGENCIA)
    }

    $('#mdlAlcoholemiaToxicologico').on('hidden.bs.modal', function () {
        limpiarCamposAlcoholemia();
        limpiarCamposToxicologico();
    });

    // Controlando que no se puedan teclear letras, solo numeros
    $('#txtNumeroFrasco').on('input', function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });

    $('#txtNumeroFrascoToxicologico').on('input', function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
});

// ALCOHOLEMIA //
async function actualizarAlcoholemias(idAtencionUrgencia) {

    const listaAlcoholemias = await getAlcoholemia(idAtencionUrgencia);

    // Verificar si listaAlcoholemias está indefinido o si su longitud es 0
    if (!listaAlcoholemias || listaAlcoholemias.length === 0) {
        // Ocultar el div que contiene la tabla de alcoholemias
        $("#divListaAlcoholemias").hide();
        return; // Salir de la función ya que no hay datos para mostrar
    }

    dibujarTablaAlcoholemias(listaAlcoholemias);
    // Mostrar el div que contiene la tabla de alcoholemias
    $("#divListaAlcoholemias").show();
}


async function modalAlcoholemiaToxicologico(showModal, objAtencionUrgencia) {

    ShowModalCargando(true);

    // Mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteAlcoholemiaToxicologico");

    if (showModal) {
        await actualizarAlcoholemias(nSession.ID_INGRESO_URGENCIA)
        await actualizarToxicologico(nSession.ID_INGRESO_URGENCIA)

        $('#divCardAlcoholemia').show();
        $('#btnGuardarAlcoholemia').show();

        $('#divCardToxicologico').show();
        $('#btnGuardarToxicologico').show();

        limpiarCamposAlcoholemia();
        $('#mdlAlcoholemiaToxicologico').modal('show');
    }

    ShowModalCargando(false);
}

async function dibujarTablaAlcoholemias(alcoholemias) {

    $('#tblAlcoholemia').DataTable({
        destroy: true,
        data: alcoholemias,
        ordering: true,
        order: [[0, 'desc']],
        language: {
            "emptyTable": "No hay alcoholemias disponibles para mostrar",
        },
        columns: [
            { data: 'Id', title: 'Id' },
            { data: 'NumeroFrasco', title: 'Boleta' },
            { data: 'FechaHora', title: 'Fecha', render: (FechaHora) => moment(FechaHora).format("DD-MM-YYYY HH:mm:ss") },
            { data: 'ApreciacionClinica.Valor', title: 'Apreciacion' },
            { data: null, title: 'Acciones' }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data, type, row) {
                    return botonesAccionAlcoholemia(row);
                }
            }
        ]
    });
}

function prepararDatosHistorialAlcoholemias(data) {
    if (data === undefined || data === null) {
        return [];
    }

    const alcoholemias = [];

    // Añadir la alcoholemia solo si hay datos presentes
    if (data.NumeroFrasco !== undefined) {
        alcoholemias.push({
            Id: data.Id,
            NumeroFrasco: data.NumeroFrasco,
            FechaHora: moment(data.FechaHora).format('DD-MM-YYYY HH:mm:ss'),
            ApreciacionClinica: data.ApreciacionClinica.Valor
        });
    }

    return alcoholemias;
}


// FUNCION PARA GUARDAR OBJETO DE ALCOHOLEMIA
async function guardarAlcoholemia() {
    if (validarCampos("#divAlcolemia")) {
        const alcoholemiaData = {
            NumeroFrasco: $('#txtNumeroFrasco').val(),
            FechaHora: $('#txtFechaAlcoholemia').val() + ' ' + $('#txtHoraAlcoholemia').val(),
            IdApreciacionClinica: $('#sltApreciacionClinica').val()
        };

        // Desactivar el botón para evitar múltiples clics
        $("#btnGuardarAlcoholemia").prop('disabled', true);

        try {
            await postAlcoholemia(alcoholemiaData);
            toastr.success("Alcoholemia guardada");
            limpiarCamposAlcoholemia();
            $("#mdlAlcoholemiaToxicologico").modal("hide");

            // Habilitar el botón nuevamente después de completar la solicitud
            $("#btnGuardarAlcoholemia").prop('disabled', false);

        } catch (error) {
            console.error('Error guardando Alcoholemia:', error);
            // Habilitar el botón en caso de error
            $("#btnGuardarAlcoholemia").prop('disabled', false);
            throw error;
        }
    }
}

// FUNCION PARA LIMPIAR CAMPOS DE ALCOHOLEMIA Y ESTABLECER FECHA Y HORA ACTUAL
async function limpiarCamposAlcoholemia() {
    $('#txtNumeroFrasco').val('').prop('disabled', false);
    // Obtener la fecha y hora actuales del sistema
    const fechaActual = moment().format('YYYY-MM-DD');
    const horaActual = moment().format('HH:mm');
    // Establecer la fecha y hora actuales en los campos correspondientes
    $('#txtFechaAlcoholemia').val(fechaActual).prop('disabled', false);
    $('#txtHoraAlcoholemia').val(horaActual).prop('disabled', false);
    // Restablecer la selección del campo de apreciación clínica a su valor por defecto
    $('#sltApreciacionClinica').val('0').prop('disabled', false);
    validarCampos("#divAlcolemia")
}

// POST ALCOHOLEMIA
async function postAlcoholemia(alcoholemiaData) {
    if (alcoholemiaData === undefined)
        return

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Alcolemia`
    }

    try {
        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(alcoholemiaData),
            contentType: 'application/json'
        })

        await actualizarAlcoholemias(nSession.ID_INGRESO_URGENCIA)

        toastr.success("Alcoholemia guardada")
        $("#mdlAlcoholemiaToxicologico").modal("hide")

    } catch (error) {
        console.error('Error guardando Alcoholemia:', error);
        throw error;
    }
}

// GET ALCOHOLEMIA
async function getAlcoholemia(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        return;

    mostrarUsuarioLogueadoEnModales("#mdlAlcoholemiaToxicologico")

    try {
        // Realizar la solicitud AJAX para obtener los datos de alcoholemia
        const alcoholemiaData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Alcolemia`,
            contentType: 'application/json',
            dataType: 'json',
        });

        return alcoholemiaData;

    } catch (error) {
        console.error('Error obteniendo datos de alcoholemia:', error);
        throw error;
    }
}

// DELETE ALCOHOLEMIA
async function deleteAlcoholemia(idAlcoholemia) {
    if (idAlcoholemia === null || idAlcoholemia === undefined) {
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar la alcoholemia?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Alcoholemia/${idAlcoholemia}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Alcoholemia eliminada`);

                // Eliminar la fila correspondiente a la alcoholemia eliminada
                const table = $('#tblAlcoholemia').DataTable();
                const rowIndex = table.row(`#alcoholemia_${idAlcoholemia}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de alcoholemias después de eliminar
                await actualizarAlcoholemias(nSession.ID_INGRESO_URGENCIA);

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divListaAlcoholemias').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar la alcoholemia: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// ACCIONES
async function verAccionAlcoholemia(idAlcoholemia) {
    try {
        $('#btnGuardarAlcoholemia').hide();
        $('#divCardToxicologico').hide();
        $('#divCardAlcoholemia').show();

        // Obtener todas las alcoholemias para la atenci�n actual
        const alcoholemias = await getAlcoholemia(nSession.ID_INGRESO_URGENCIA);

        // encontrar la alcoholemia especifica por ID
        const alcoholemiaSeleccionada = alcoholemias.find(alcoholemia => alcoholemia.Id === idAlcoholemia);

        // se encontro la alcoholemia correspondiente al ID
        if (!alcoholemiaSeleccionada) {
            console.error('No se encontr� ninguna alcoholemia con el ID proporcionado:', idAlcoholemia);
            return;
        }

        // Mostrar los datos en los campos del modal
        $('#txtNumeroFrasco').val(alcoholemiaSeleccionada.NumeroFrasco).prop('disabled', true);
        $('#txtFechaAlcoholemia').val(moment(alcoholemiaSeleccionada.FechaHora).format('YYYY-MM-DD')).prop('disabled', true);
        $('#txtHoraAlcoholemia').val(moment(alcoholemiaSeleccionada.FechaHora).format('HH:mm')).prop('disabled', true);
        $('#sltApreciacionClinica').val(alcoholemiaSeleccionada.ApreciacionClinica.Id).prop('disabled', true);

        // Mostrar el modal
        $('#mdlAlcoholemiaToxicologico').modal('show');

    } catch (error) {
        console.error('Error al mostrar los datos de la alcoholemia en el modal', error);
    }
}

// BOTONES
function botonesAccionAlcoholemia(row) {

    const { Id } = row;
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    const html = `
        <div class="d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button" 
                class="btn btn-info" 
                onclick="verAccionAlcoholemia(${Id})"
                data-toggle="tooltip" 
                data-placement="bottom"
                title="Ver"
                style="display: inline-block; margin-right: 5px;">
                <i class="fas fa-eye fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteAlcoholemia ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteAlcoholemia(${Id})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                style="display: inline-block;">
                <i class="fa fa-trash fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}


// TOXICOLOGICO //
async function actualizarToxicologico(idAtencionUrgencia) {
    const listaToxicologico = await getToxicologico(idAtencionUrgencia)

    // Verificar si lisyaToxicologico es undefined o 0
    if (!listaToxicologico || listaToxicologico.length === 0) {
        // Ocultar el div que contiene la tabla de alcoholemias
        $("#divListaToxicologicos").hide();
        return; // Salir de la función ya que no hay datos para mostrar
    }
    dibujarTablaToxicologico(listaToxicologico)
    $("#divListaToxicologicos").show();
}

async function dibujarTablaToxicologico(toxicologico) {
    $('#tblToxicologico').DataTable({
        destroy: true,
        data: toxicologico,
        ordering: true,
        order: [[0, 'desc']],
        language: {
            "emptyTable": "No hay toxicologicos disponibles para mostrar",
        },
        columns: [
            { data: 'Id', title: 'Id' },
            { data: 'NumeroFrasco', title: 'Boleta' },
            { data: 'FechaHora', title: 'Fecha', render: (FechaHora) => moment(FechaHora).format("DD-MM-YYYY HH:mm:ss") },
            { data: null, title: 'Acciones' }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data, type, row) {
                    return botonesAccionToxicologico(data.Id);
                }
            }
        ]
    });
}

// FUNCION PARA GUARDAR OBJETO DE TOXICOLOGICO
async function guardarToxicologico() {

    if (validarCampos("#divToxicologico")) {
        const toxicologicoData = {
            NumeroFrasco: $('#txtNumeroFrascoToxicologico').val(),
            FechaHora: $('#txtFechaToxicologico').val() + ' ' + $('#txtHoraToxicologico').val()
        }

        // Desactivar el botón para evitar múltiples clics
        $("#btnGuardarToxicologico").prop('disabled', true);

        try {
            await postToxicologico(toxicologicoData);
            toastr.success("Toxicologico guardado");
            limpiarCamposToxicologico();
            $("#mdlAlcoholemiaToxicologico").modal("hide");

            // Haabilitar btn al completar
            $("#btnGuardarToxicologico").prop('disabled', false);

        } catch (error) {
            console.error('Error guardando Toxicologico:', error);
            $("#btnGuardarToxicologico").prop('disabled', false);
            throw error;
        }
    }
}

// FUNCION PARA LIMPIAR CAMPOS DE TOXICOLOGICO
function limpiarCamposToxicologico() {
    $('#txtNumeroFrascoToxicologico').val('').prop('disabled', false);
    // Obtener la fecha y hora actuales del sistema
    const fechaActual = moment().format('YYYY-MM-DD');
    const horaActual = moment().format('HH:mm');
    $('#txtFechaToxicologico').val(fechaActual).prop('disabled', false);
    $('#txtHoraToxicologico').val(horaActual).prop('disabled', false);
    validarCampos("#divToxicologico")
}

// POST TOXICOLOGICO
async function postToxicologico(toxicologicoData) {
    if (toxicologicoData === undefined)
        return

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Toxicologico`
    }

    try {
        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(toxicologicoData),
            contentType: 'application/json'
        })

        await actualizarToxicologico(nSession.ID_INGRESO_URGENCIA)

        toastr.success("Toxicologico guardado")
        $("#mdlAlcoholemiaToxicologico").modal("hide")

    } catch (error) {
        console.error('Error guardando Toxicologico:', error);
        throw error;
    }
}

// GET TOXICOLOGICO
async function getToxicologico(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        return;

    mostrarUsuarioLogueadoEnModales("#mdlAlcoholemiaToxicologico")

    try {
        const toxicologicoData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Toxicologico`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return toxicologicoData

    } catch (error) {
        if (error.status === 400) {
            return
        }
    }
}

// DELETE TOXICOLOGICO
async function deleteToxicologico(idToxicologico) {
    if (idToxicologico === null || idToxicologico === undefined) {
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar el toxicol&oacute;gico?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Toxicologico/${idToxicologico}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Toxicologico eliminado`);

                // Eliminar la fila correspondiente al toxicologico eliminado
                const table = $('#tblToxicologico').DataTable();
                const rowIndex = table.row(`#toxicologico_${idToxicologico}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de toxicologicos después de eliminar
                await actualizarToxicologico(nSession.ID_INGRESO_URGENCIA);

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divListaToxicologicos').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar el toxicologico: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// ACCIONES
async function verAccionToxicologico(idToxicologico) {
    try {
        // Ocultar el botón de guardar y la sección de alcoholemia, mostrar la sección de toxicológico
        $('#btnGuardarToxicologico').hide();
        $('#divCardAlcoholemia').hide();
        $('#divCardToxicologico').show();

        const toxicologicos = await getToxicologico(nSession.ID_INGRESO_URGENCIA);

        const toxicologicoSeleccionado = toxicologicos.find(toxicologico => toxicologico.Id === idToxicologico);

        if (!toxicologicoSeleccionado) {
            console.error('No se encontró ningún tóxicológico con el ID proporcionado:', idToxicologico);
            return;
        }

        // Mostrar los datos en los campos del modal
        $('#txtNumeroFrascoToxicologico').val(toxicologicoSeleccionado.NumeroFrasco).prop('disabled', true);
        $('#txtFechaToxicologico').val(moment(toxicologicoSeleccionado.FechaHora).format('YYYY-MM-DD')).prop('disabled', true);
        $('#txtHoraToxicologico').val(moment(toxicologicoSeleccionado.FechaHora).format('HH:mm')).prop('disabled', true);

        // Mostrar el modal
        $('#mdlAlcoholemiaToxicologico').modal('show');

    } catch (error) {
        console.error('Error al mostrar los datos de toxicologico en el modal', error);
    }
}


// BOTONES
function botonesAccionToxicologico(idtoxicologico) {
    const idToxicologico = idtoxicologico;
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    const html = `
        <div class="d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button" 
                class="btn btn-info" 
                onclick="verAccionToxicologico(${idToxicologico})"
                data-toggle="tooltip" 
                data-placement="bottom"
                title="Ver"
                style="display: inline-block; margin-right: 5px;"
            >
                <i class="fas fa-eye fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteToxicologico ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteToxicologico(${idToxicologico})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                style="display: inline-block;"
            >
                <i class="fa fa-trash fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}

async function cargarAlcoholemiaYtoxicologico() {
    $("#divAlcolemia, #divToxicologico").hide();

    // Mostrar Alcoholemia por defecto
    $("#divAlcolemia").show();
    $("#divAlcolemia input, #divAlcolemia select").attr("data-required", true);
    const fechaHoy = GetFechaActual();
    $("#txtFechaAlcoholemia").val(moment(fechaHoy).format("YYYY-MM-DD"));
    $("#txtHoraAlcoholemia").val(moment(fechaHoy).format("HH:mm"));
    $("#sltApreciacionClinica").val("0");

    // Mostrar Toxicológico por defecto
    $("#divToxicologico").show();
    $("#divToxicologico input, #divToxicologico select").attr("data-required", true);
    $("#txtFechaToxicologico").val(moment(fechaHoy).format("YYYY-MM-DD"));
    $("#txtHoraToxicologico").val(moment(fechaHoy).format("HH:mm"));

    ReiniciarRequired();
}