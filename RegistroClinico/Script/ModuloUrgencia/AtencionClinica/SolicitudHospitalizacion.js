﻿// CONTROL DE 1 SOLA SOLICITUD DE HOSPITALIZACION
function IrASolicitudHospitalizacion(idPaciente, idEvento, idDau) {
    const btnSolicitudDeHospitalizacion = $("#btnSolicitudDeHospitalizacion");

    // Deshabilitar btnSolicitudDeHospitalizacion al hacer clic
    btnSolicitudDeHospitalizacion.prop('disabled', true);

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&idEvento=${idEvento}`)
        .then(res => {
            if (res.length > 0) {
                // Si ya existe una solicitud
                Swal.fire({
                    title: 'Solicitud ya existente',
                    text: 'Este paciente ya tiene una solicitud de hospitalización activa.',
                    icon: 'warning',
                    confirmButtonText: 'Aceptar'
                }).then(() => {
                    btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion
                });
            } else {
                // Crear nueva solicitud
                setSession('ID_PACIENTE', idPaciente);
                setSession('ID_EVENTO', idEvento);
                setSession('ID_DAU', idDau);

                const nuevaVentana = window.open(
                    `${ObtenerHost()}/Vista/ModuloHOS/NuevaSolicitudHospitalizacion.aspx`,
                    '_blank'
                );

                // Monitorear si la ventana se cierra manualmente y reactiva el btnSolicitudDeHospitalizacion
                const verificarVentana = setInterval(() => {
                    if (nuevaVentana.closed) {
                        clearInterval(verificarVentana);
                        cargarTablaSolicitudHospitalizacion(idEvento, true);
                    }
                }, 1000);
            }
        })
        .catch(error => {
            console.error("Error al verificar solicitudes de hospitalización existentes:", error);
            btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion en caso de error
        });
}

function cargarTablaSolicitudHospitalizacion(idEventoUrgencia, irAHistorial) {
    const btnSolicitudDeHospitalizacion = $("#btnSolicitudDeHospitalizacion");

    $("#divHistorialSolicitudesHospitalizacion").hide();

    promesaAjax("GET", `HOS_Solicitud_Hospitalizacion/Buscar?idTipoEstadoSistemas=157&idTipoEstadoSistemas=158&idEvento=${idEventoUrgencia}`)
        .then(res => {
            if (res.length > 0) {
                const sSession = getSession();

                $("#divHistorialSolicitudesHospitalizacion").show();
                const datosPreparadosFormularios = res.map(({ Id, Fecha, Motivo, Ubicacion, PlanManejo }) => ({
                    Id,
                    Fecha,
                    Motivo,
                    Ubicacion: Ubicacion.Valor,
                    PlanManejo
                }));

                $("#tblHistorialSolicitudesHospitalizacion").DataTable({
                    data: datosPreparadosFormularios,
                    destroy: true,
                    columns: [
                        { title: "Id", data: "Id" },
                        {
                            title: "Fecha",
                            data: "Fecha",
                            render: fecha => moment(fecha).format("DD-MM-YYYY HH:mm:SS")
                        },
                        { title: "Motivo", data: "Motivo" },
                        { title: "Ubicación", data: "Ubicacion" },
                        { title: "Plan Manejo", data: "PlanManejo" },
                        { title: "Acciones", data: "Id" }
                    ],
                    columnDefs: [
                        {
                            targets: -1,
                            render: id => {
                                let botones = `<button type="button" class="btn btn-info mr-1" onclick="imprimirSolicitudHospitalizacion(${id})"> 
                                                    <i class="fa fa-print"></i> 
                                                </button>`;

                                if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico ||
                                    sSession.CODIGO_PERFIL === perfilAccesoSistema.matroneriaUrgencia) {
                                    botones += `<button type="button" class="btn btn-success" onclick="editarSolicitudHospitalizacion(${id})"> 
                                                    <i class="fa fa-edit"></i> 
                                                </button>
                                                <button type="button" class="btn btn-danger" onclick="eliminarSolicitudHospitalizacion(${id})"> 
                                                    <i class="fa fa-trash"></i> 
                                                </button>`;
                                }
                                return botones;
                            }
                        }
                    ]
                });

                if (irAHistorial) {
                    $('html, body').animate({ scrollTop: $("#divHistorialSolicitudesHospitalizacion").offset().top - 10 }, 500);
                    Swal.fire({
                        title: 'Solicitud de Hospitalización',
                        html: '¿Desea imprimir la última Solicitud de hospitalización creada?',
                        icon: 'info',
                        showCancelButton: true,
                        confirmButtonText: 'Imprimir',
                        cancelButtonText: 'Cancelar',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }).then(result => {
                        if (result.value) {
                            imprimirSolicitudHospitalizacion(res[res.length - 1].Id);
                        }
                        btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion
                    });
                } else {
                    btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion
                }
            } else {
                btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion si no hay datos
            }
        })
        .catch(error => {
            console.error("Error al consultar las Solicitudes de Hospitalización:", error);
            btnSolicitudDeHospitalizacion.prop('disabled', false); // Habilitar btnSolicitudDeHospitalizacion en caso de error
        });
}

function editarSolicitudHospitalizacion(idSolicitudHospitalizacion) {
    setSession('ID_SOLICITUD_HOSPITALIZACION', idSolicitudHospitalizacion);
    setSession('ID_EVENTO', idEventoUrgencia);
    window.open(`${ObtenerHost()}/Vista/ModuloHOS/NuevaSolicitudHospitalizacion.aspx`, '_blank');
    window.addEventListener("message", function (event) {
        if (event.data === "Forbidden") {
            const idEvento = idEventoUrgencia;
            cargarTablaSolicitudHospitalizacion(idEvento, true);
        }
    });
}

function eliminarSolicitudHospitalizacion(idSolicitudHospitalizacion) {

    Swal.fire({
        title: 'Solicitud de Hospitalización',
        html: `¿Desea eliminar la Solicitud de hospitalización?`,
        icon: 'info',
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
        allowEscapeKey: false,
        allowOutsideClick: false,
    }).then((result) => {
        if (result.value) {
            promesaAjax("DELETE", `HOS_Solicitud_Hospitalizacion/${idSolicitudHospitalizacion}`).then(res => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Solicitud Eliminada",
                    showConfirmButton: false,
                    timer: 2000
                }).then((result) => {
                    cargarTablaSolicitudHospitalizacion(res.IdEvento, false);
                });
            });
        }
    });

}

function imprimirSolicitudHospitalizacion(id) {
    const url = `${GetWebApiUrl()}HOS_Solicitud_Hospitalizacion/${id}/Imprimir`;
    ImprimirApiExterno(url);
}