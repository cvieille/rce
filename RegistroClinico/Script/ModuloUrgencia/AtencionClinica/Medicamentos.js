﻿$(document).ready(async function () {

    nSession = getSession();
    // Medico o Matrona
    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 22 || nSession.CODIGO_PERFIL == 21 ||
        nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {

        //nSession.ID_INGRESO_URGENCIA
        const medicamentosArsenal = await getListaMedicamentosArsenalUrgencia()

        await actualizarMedicamentosSeleccionadosUrg(nSession.ID_INGRESO_URGENCIA)

        $("#thMedicamentos").typeahead(setMedicamentosTypeAhead(medicamentosArsenal));
        $("#thMedicamentos").on('input', () => $("#thMedicamentos").removeData("id"));
        $("#thMedicamentos").blur(() => {
            if ($("#thMedicamentos").data("id") == undefined)
                $("#thMedicamentos").val("");
        });
    }

    $('#mdlMedicamentos').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divInsumosMedicamentos");
    });

    $('#mdlCerrarMedicamentoUrg').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divObservacionesCierreMedicamentoUrg");
    });

    modalListaMedicamentos();
    cargarComboViaAdministracion();

});

// Función para cargar las opciones del combo de vía de administración
async function cargarComboViaAdministracion() {
    try {
        const response = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Tipo_Via_Administracion/Combo`,
            contentType: 'application/json',
            dataType: 'json'
        });

        // Verificar si la respuesta contiene datos
        if (response && Array.isArray(response)) {
            // Obtener referencia al select
            const sltViaAdministracion = document.getElementById('sltViaAdministracion');

            // Limpiar cualquier opción previa en el combo
            sltViaAdministracion.innerHTML = '';

            // Agregar "Seleccione"
            const optionSeleccione = document.createElement('option');
            optionSeleccione.value = '0';
            optionSeleccione.textContent = 'Seleccione';
            sltViaAdministracion.appendChild(optionSeleccione);

            // Iterar sobre los datos de la respuesta y agregar opciones al combo
            response.forEach(option => {
                const optionElement = document.createElement('option');
                optionElement.value = option.Id;
                optionElement.textContent = option.Valor;
                sltViaAdministracion.appendChild(optionElement);
            });
        } else {
            console.error('La respuesta del servidor no contiene datos válidos.');
        }
    } catch (error) {
        console.error('Error al obtener las opciones de vía de administración:', error);
    }
}

async function actualizarMedicamentosSeleccionadosUrg(idAtencionUrgencia) {

    const medicamentos = await getMedicamentosUrgencia(idAtencionUrgencia)
    mostrarOcultarIndicacionesUrg(medicamentos, "#divMedicamentos")
    await dibujarMedicamentosSeleccionadosUrg(medicamentos)
}

function modalInsumosMedicamentos() {

    $("#thMedicamentos").data("id", null);
    $("#thMedicamentos, #txtDosisMedicamento, #txtFrecuenciaMedicamento").val("");
    $("#sltViaAdministracion").val("0");

    mostrarModal("mdlMedicamentos");
}

async function getListaMedicamentosArsenalUrgencia() {

    try {
        const medicamentos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Medicamento_Arsenal/Combo`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return medicamentos

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar medicamentos del arsenal")
        console.log(JSON.stringify(error))
    }
}

async function getMedicamentosUrgencia(idAtencionUrgencia) {

    if (idAtencionUrgencia === null)
        return

    try {
        const medicamentos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Medicamentos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return medicamentos

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar medicamentos de urgencia")
        console.log(JSON.stringify(error))
    }
}

async function dibujarMedicamentosSeleccionadosUrg(medicamentos) {

    let html = ""

    if (medicamentos === undefined) {
        $("#tblMedicamentosBox tbody").addClass("text-center")
        html = `<tr>
            <td colspan="5">No hay informacion<td> 
        </tr>`
        $("#tblMedicamentosBox tbody").append(html)
        return
    }
    else {
        medicamentos.map((m, index) => {

            const { Forma, Presentacion, Valor } = m.Medicamento
            const formatoMedicamento = `${Valor} ${Forma} ${Presentacion}`

            $("#tblMedicamentosBox tbody").empty()
            html += `
        <tr class="${pintarFilasAccionRealizada(m.AccionRealizada)}">
            <td>${index + 1}</td>
            <td>
                ${formatoMedicamento}                
            </td>
            <td class="p-2">
                <strong>Dosis:</strong> ${m.Dosis}
                <br>
                <strong>Frecuencia:</strong> ${m.Frecuencia}
                <br>
                <strong>Vía de administración:</strong> ${m.TipoViaAdministracion.Valor}
                ${
                m.Observaciones !== ""
                    ?
                    `<div class="rounded p-2 table-active border border-grey w-100 mt-2">
                    <div class="row d-flex flex-row justify-content-start">
                        <div class="col-12">
                            <strong>Observación:</strong>
                        </div>
                        <div class="col-12">
                            ${m.Observaciones ?? ""}
                        </div>
                    <div>
                </div>`
                    :
                    ``
                }
            </td>
            <td>${moment(m.FechaIngreso).format('DD-MM-YYYY HH:mm:ss')}</td>
            <td>${m.FechaCierre !== null ? moment(m.FechaCierre).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
            <td>
                <div class="d-flex flex-row justify-content-center flex-nowrap">
                    ${m.FechaCierre !== null ? botonesVerMedicamento(m) : ""}
                    ${m.AccionRealizada == null ? botonesAccionMedicamento(m) : ""}
                    ${m.Acciones.Quitar ? botonEliminarMedicamento(m) : ""}
                    ${nSession.CODIGO_PERFIL == perfilAccesoSistema.administradorDeUrgencia && m.AccionRealizada ?
                    `<button type="button" class="btn btn-warning"
                data-toggle="tooltip" data-placement="bottom" title="Reversar" onclick='reversarElemento("medicamento" ,${JSON.stringify(m)})' data-original-title="Reversar"><i class="fas fa-retweet"></i></button>`
                    : ''}
                </div>
            </td>
        </tr>
       `
            $("#tblMedicamentosBox tbody").append(html)
        })

        const cantidadMedicamentos = medicamentos.map(p => p.AccionRealizada).length
        const cantidadRealizados = await cantidadProcedimientosRealizados(medicamentos) // Se reutiliza funcion para los medicamentos

        if (cantidadMedicamentos === 0) {
            $("#aMedicamentoBox").addClass("default")
            $("#aMedicamentoBox").removeClass("warning")
        }
        else if (cantidadMedicamentos === cantidadRealizados && cantidadMedicamentos > 0) {
            $("#aMedicamentoBox").addClass("success")
            $("#aMedicamentoBox").removeClass("warning")
        }
        else {
            $("#aMedicamentoBox").addClass("warning")
            $("#aMedicamentoBox").removeClass("default")
            $("#aMedicamentoBox").removeClass("success")
        }

        $("#spInsumosMedicamentos").html(`${cantidadRealizados}/${cantidadMedicamentos}`)

        $(`[data-toggle="tooltip"]`).tooltip();
    }
}

function pintarFilasAccionRealizada(accion) {

    let clase = ""

    if (accion !== null)
        clase = accion === true ? "table-success" : "table-warning"

    return clase
}

function botonesAccionMedicamento(medicamento) {

    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 3 || nSession.CODIGO_PERFIL === 30;

    html = `<a 
                id="btnAccionRealizadaMedicamento"                 
                data-id="${medicamento.IdAtencionUrgenciaMedicamento}"
                class="btn btn-success m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                onclick="showCerrarMedicamentoUrg(this)"
                data-original-title="Acción realizada"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title=""                   
                >
                    <i class="fas fa-check fa-xs"></i></a> 
            <a 
                id="btnAccionNoRealizadaMedicamento"
                data-id="${medicamento.IdAtencionUrgenciaMedicamento}"
                class="btn btn-warning m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                onclick="showCerrarMedicamentoUrg(this)"
                data-original-title="Acción No realizada"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="" 
                
                >
                    <i class="fas fa-times fa-xs"></i>
                </a>`

    return html
}

function botonEditarMedicamento(medicamento) {

    html = `<a 
                id="btnEditarMedicamento" 
                class="btn btn-info m-1"
                data-id="${medicamento.IdAtencionUrgenciaMedicamento}"
                data-original-title="Acción realizada"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title=""
                onclick="showCerrarMedicamentoUrg(this)"
                >
                    <i class="fas fa-pencil-alt fa-xs"></i>
           </a>
           <a 
                id="btnImprimirMedicamento" 
                class="btn btn-info m-1"
                data-id="${medicamento.IdAtencionUrgenciaMedicamento}"
                data-original-title="Acción realizada"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="" 
                onclick="printMedicamentoUrg(this)" 
                >
                    <i class="fas fa-print fa-xs"></i>
           </a>`
    return html
}

function botonEliminarMedicamento(medicamento) {

    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    html = `<a 
                id="btnEliminarMedicamento" 
                class="btn btn-danger m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                data-toggle="tooltip"
                data-placement="bottom"
                data-original-title="Quitar"
                title="Eliminar"
                onclick="deleteMedicamento(${medicamento.IdAtencionUrgenciaMedicamento})"
                >
                    <i class="fa fa-trash fa-xs"></i>
                </a>`
    return html;

}

function botonesVerMedicamento(medicamento) {

    html = `<a 
                id="btnVerMedicamento" 
                data-id="${medicamento.IdAtencionUrgenciaMedicamento}"
                class="btn btn-info m-1" 
                onclick="showCerrarMedicamentoUrg(this)" 
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Ver" 
                data-original-title="Ver Medicamento"
                >
                <i class="fas fa-eye fa-xs"></i>
           </a>`

    return html
}

async function showCerrarMedicamentoUrg(e) {

    const dataModal = $(e).data()
    let nombreProfesionalRealiza = nSession.NOMBRE_USUARIO;

    $("#aCerrarMedicamento").attr("data-idmedicamento")
    $("#aCerrarMedicamento").data("idmedicamento", 1)

    const medicamento = await getMedicamentoUrgenciaPorId(nSession.ID_INGRESO_URGENCIA, dataModal.id)


    const { Nombre, ApellidoPaterno, ApellidoMaterno } = medicamento.ProfesionalSolicita
    const nombreProfesionalSolicitante = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`

    if (medicamento.ProfesionalRealiza !== null) {
        const { Nombre, ApellidoPaterno, ApellidoMaterno } = medicamento.ProfesionalRealiza
        nombreProfesionalRealiza = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`
    }

    const horaSolicitud = moment(medicamento.FechaIngreso).format('HH:mm:ss')
    const horaCierre = medicamento.FechaCierre !== null ? moment(medicamento.FechaCierre).format('HH:mm:ss') : "Automática al guardar"
    let badgeRealizado = ""

    $("#txtObservacionesCierreMedicamento").val("")

    pintaHeaderModalMedicamento(dataModal)

    // Determina si el modal se levantó mediante el botón de acción no realizada
    const isAccionNoRealizada = $(e).data('originalTitle') === 'Acción No realizada';

    if (medicamento.FechaCierre === null) {
        if (isAccionNoRealizada) {
            $("#txtObservacionesCierreMedicamento").attr("data-required", true)
        } else {
            $("#txtObservacionesCierreMedicamento").attr("data-required", false)
        }
        $("#txtObservacionesCierreMedicamento").attr("readonly", false)
        $("#aCerrarMedicamento").show()
    } else {
        badgeRealizado = `<span id="bgTrue" class="badge badge-pill btn-success badge-count ml-2" readonly>${medicamento.AccionRealizada ? 'Realizado' : ''}</span>
    <span id="bgFalse" class="badge badge-pill badge-count ml-2" style="color: #ffffff; background-color: orange;" readonly>${!medicamento.AccionRealizada ? 'No realizado' : ''}</span>`

        $("#txtObservacionesCierreMedicamento").attr("data-required", false)
        $("#txtObservacionesCierreMedicamento").val(medicamento.ObservacionesCierre ?? "Sin Información")
        $("#txtObservacionesCierreMedicamento").attr("readonly", true)
        $("#aCerrarMedicamento").hide()
    }

    $("#divDetalleMedicamento").html(
        `
        <div class="d-flex align-items-center">
        <h5 class="mb-0"><strong>Detalles de Medicamento</strong></h5>
        ${badgeRealizado}
        </div>
        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Solicitante:</label> <span class="form-control form-control-no-height" readonly>${nombreProfesionalSolicitante}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de solicitud:</label> <span class="form-control form-control-no-height" readonly>${horaSolicitud}</span>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Profesional que cierra:</label>
                <span class="form-control" readonly>${nombreProfesionalRealiza ?? ""}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de cierre:</label> <span class="form-control form-control-no-height" readonly>${horaCierre}</span>
            </div>
        </div>
        
        <label>Medicamento:</label>
        <span class="form-control form-control-no-height" readonly>${medicamento.Medicamento.Valor}</span>
        <label>Descripción:</label>
        <span class="form-control form-control-no-height" readonly>` +
        `   <strong>Dosis:</strong> ${medicamento.Dosis}
            <strong>Frecuencia:</strong> ${medicamento.Frecuencia}
            <strong>Vía de administración:</strong> ${medicamento.Via.Valor}</span>`)

    $("#txtObservacionesMedicamento").val(medicamento.Observaciones ?? "Sin Información")

    $("#aCerrarMedicamento").attr("data-id")
    $("#aCerrarMedicamento").data("id", medicamento.IdAtencionMedicamento)

    $("#aCerrarMedicamento").data("accion-realizada", medicamento.accionRealizada)
    $("#aCerrarMedicamento").attr("data-realizada")
    $("#aCerrarMedicamento").data("realizada", dataModal.originalTitle)

    $("#aCerrarMedicamento").unbind().on('click', async function (event) {
        event.preventDefault();

        $(this).prop('disabled', true); // Deshabilita btn

        try {
            realizarAccionMedicamentoUrg(this);
        } catch (error) {
            console.error(error);
        } finally {
            $(this).prop('disabled', false); // Habilita el btn nuevamente después de la acción
        }
    });

    $("#aCerrarMedicamento").html(`<i class="fas fa-save"></i> Guardar`)

    $("#mdlCerrarMedicamentoUrg").modal("show")
}

async function realizarAccionMedicamentoUrg(e) {

    if (!validarCampos("#divObservacionesCierreMedicamentoUrg", false))
        return

    const textoAccion = $(e).data("realizada")

    const IdUrgenciaMedicamento = $(e).data("id")
    const IdAtencionUrgencia = nSession.ID_INGRESO_URGENCIA
    const ObservacionesCierre = $("#txtObservacionesCierreMedicamento").val()
    let AccionRealizada = false

    if (textoAccion === "Acción realizada")
        AccionRealizada = true

    let json = {
        IdUrgenciaMedicamento,
        IdAtencionUrgencia,
        ObservacionesCierre,
        AccionRealizada,
        textoAccion
    }

    ShowModalCargando(true)
    await patchAccionMedicamentoRealizada(json)
    ShowModalCargando(false)
}
async function patchAccionMedicamentoRealizada(json) {

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.IdAtencionUrgencia}/medicamento/Cerrar`

    let body = {
        IdMedicamento: json.IdUrgenciaMedicamento,
        IdAtencionUrgencia: json.IdAtencionUrgencia,
        AccionRealizada: json.AccionRealizada,
        ObservacionesCierre: json.ObservacionesCierre
    }

    $.ajax({
        type: "PATCH",
        url: url,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            $("#mdlCerrarMedicamentoUrg").modal("hide")
            actualizarTablaMedicamentosUrgencia()

            if (json.textoAccion === "Acción realizada")
                toastr.success(`Medicamento realizado`);
            else if (json.textoAccion === "Acción No realizada")
                toastr.warning(`Medicamento no realizado`);
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar el medicamento: " + JSON.stringify(jqXHR));
        }
    });
}


function prepararMedicamentosUrg() {

    let medicamentos = []
    let medicamento = {}

    //let tblMedicamentos = $("#tbodyMedicamentos tr td:not(.align-middle)")

    $('#tblMedicamentos tbody tr').each(function () {
        const IdMedicamento = parseInt($(this).data("id-medicamento"))
        const Dosis = $(this).find('td:eq(1)').text()
        const Frecuencia = $(this).find('td:eq(2)').text()
        const IdTipoViaAdministracion = $(this).data("via")
        const Observacion = $(this).find('td:eq(4)').text()

        medicamento = {
            IdMedicamento: IdMedicamento,
            Dosis: Dosis,
            Frecuencia: Frecuencia,
            IdTipoViaAdministracion: IdTipoViaAdministracion,
            Observacion: Observacion
        }

        medicamentos.push(medicamento);
    });

    return medicamentos
}

async function guardarMedicamentosUrg() {

    let medicamentos = await prepararMedicamentosUrg()
    if (medicamentos.length === 0)
        return

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Medicamentos`
    }

    // Deshabilitar btn
    $("#aAceptarMedicamentosUrg").prop('disabled', true);

    try {
        const response = await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(medicamentos),
            contentType: 'application/json'
        });

        toastr.success("Medicamentos guardados")
        $("#mdlMedicamentos").modal("hide")

        // habilitar btn en caso de exito
        $("#aAceptarMedicamentosUrg").prop('disabled', false);

        $("#tbodyMedicamentos").empty()

        //const medicamentosSeleccionados = await getMedicamentosUrgencia(nSession.ID_INGRESO_URGENCIA)
        //await dibujarMedicamentosSeleccionadosUrg(medicamentosSeleccionados)
        await actualizarMedicamentosSeleccionadosUrg(nSession.ID_INGRESO_URGENCIA)

    } catch (error) {
        console.error('Error en la solicitud:', error);
        // habilitar btn en caso de exito
        $("#aAceptarMedicamentosUrg").prop('disabled', false);
        throw error;
        
    }
}

function setMedicamentosTypeAhead(data) {

    var typeAhObject = {
        input: "#thMedicamentos",
        minLength: 3,
        maxItem: 300,
        maxItemPerGroup: 10,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: "No existe el medicamento especificado ({{query}})",
        template:
            `<span>
                <span class="{{Valor}}">{{Valor}} {{Forma}} {{Presentacion}}</span>
            </span>`,
        correlativeTemplate: true,
        source: {
            GEN_Medicamento: { data }
        },
        callback: {
            onClick: function (node, a, item, event) {
                //$("#thMedicamentos").attr('data-id', item.Id);
                $("#thMedicamentos").data('id', item.Id)
                $("#aAgregarMedicamento").attr("data-id")
                $("#aAgregarMedicamento").data("id", item.Id)
                $("#aAgregarMedicamento").attr("data-descripcion")
                $("#aAgregarMedicamento").data("descripcion", item.compiled)
            }
        }
    };

    return typeAhObject;
}

function agregarMedicamentoModalUrg(e) {

    if (!validarCampos("#divInsumosMedicamentos", false))
        return

    const descripcionMedicamento = $(e).data("descripcion")
    const idMedicamento = $(e).data("id")

    const objMedicamento = {
        medicamento: { id: idMedicamento, valor: descripcionMedicamento },
        dosis: $("#txtDosisMedicamento").val(),
        frecuencia: $("#txtFrecuenciaMedicamento").val(),
        via: {
            id: $('#sltViaAdministracion option:selected').val(),
            valor: $('#sltViaAdministracion option:selected').text()
        },
        observacion: $("#txtObservaciones").val()
    }

    html = dibujarHtmlFilaMedicamentosUrg(objMedicamento)
    addFilaTablaMedicamentosUrg("#tblMedicamentos", html)

    let numeroFilas = $("#tbodyMedicamentos tr").length
    if (numeroFilas > 0) {
        $("#tbodyMedicamentos tr#no-item").remove()
    }
}

function addFilaTablaMedicamentosUrg(tabla, html) {
    $(`${tabla} tbody`).append(html)
    $(`${tabla}`).show()
    //toastr.success("Medicamento agregado")
}

function dibujarHtmlFilaMedicamentosUrg(data) {

    console.log(data.medicamento.valor)

    const html = `
            <tr id="tr-medicamentos-urg-${data.medicamento.id}" 
                data-id-medicamento="${data.medicamento.id}" 
                data-via="${data.via.id}" 
                class='text-center'>
                <td>${data.medicamento.valor}</td>
                <td>${data.dosis}</td>
                <td>${data.frecuencia}</td>
                <td>${data.via.valor}</td>
                <td>${data.observacion}</td>
                <td class="align-middle text-center">
                    <a class="aQuitarExamen btn btn-danger"
                        data-id="${data.medicamento.id}"
                        onclick="deleteFilaMedicamento(${data.medicamento.id})"
                        style="color:red; cursor:pointer;"> 
                        <i class="fa fa-trash"></i>
                    </a>
                    </td>
            </tr>`

    return html
}

async function deleteFilaMedicamento(id) {
    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el medicamento?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        $(`#tbodyMedicamentos tr[id="tr-medicamentos-urg-${id}"]`).remove()

        let numeroFilas = $("#tbodyMedicamentos tr").length

        let objTablaNoItem = {
            numeroFilas,
            elemento: "#tbodyMedicamentos",
            mensaje: "No hay medicamentos agregados",
            colspan: 6
        }

        mensajeTablaNoItemUrg(objTablaNoItem)
    }
}

function modalListaMedicamentos() {

    $("#chkDosisUnica").bootstrapSwitch('state', false);

    // Obtener referencias a los elementos HTML del modal Lista de Medicamentos
    let chkDosisUnica = $("#chkDosisUnica");
    let txtDosisMedicamento = $("#txtDosisMedicamento");
    let txtFrecuenciaMedicamento = $("#txtFrecuenciaMedicamento");
    let txtObservaciones = $("#txtObservaciones");
    let modal = $("#mdlMedicamentos");

    // Agregar un evento al cierre del modal
    modal.on('hidden.bs.modal', function () {
        // Limpiar y restablecer los campos y el modal
        chkDosisUnica.bootstrapSwitch('state', false);

        // Restablecer los valores predeterminados
        txtDosisMedicamento.val('');
        txtDosisMedicamento.prop('disabled', false);

        txtFrecuenciaMedicamento.val('');
        txtFrecuenciaMedicamento.prop('disabled', false);

        txtObservaciones.val('');
    });

    // Añadir un evento al cambio del checkbox
    chkDosisUnica.on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            // Si el checkbox está activado, configurar los campos y el select según lo requerido
            txtDosisMedicamento.val('1');
            txtDosisMedicamento.prop('disabled', true);

            txtFrecuenciaMedicamento.val('Única');
            txtFrecuenciaMedicamento.prop('disabled', true);

            //txtFrecuenciaMedicamento.attr('data-required', false)
        } else {
            // Si el checkbox está desactivado, habilitar los campos y el select
            txtDosisMedicamento.val('');
            txtDosisMedicamento.prop('disabled', false);

            txtFrecuenciaMedicamento.val('');
            txtFrecuenciaMedicamento.prop('disabled', false);
        }
    })
}

async function modalMedicamentos(showModal, objAtencionUrgencia) {

    ShowModalCargando(true)

    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteMedicamentos")

    if (showModal) {

        $("#tbodyMedicamentos tr").remove()

        let numeroFilas = $("#tbodyMedicamentos tr").length

        let objTablaNoItem = {
            numeroFilas,
            elemento: "#tbodyMedicamentos",
            mensaje: "No hay medicamentos agregados",
            colspan: 6
        }

        mensajeTablaNoItemUrg(objTablaNoItem)

        mostrarModal("mdlMedicamentos")
    }

    ShowModalCargando(false)
}

async function getMedicamentoUrgenciaPorId(idAtencionUrgencia, idUrgenciaMedicamento) {

    if (idAtencionUrgencia === null || idUrgenciaMedicamento === null)
        return

    try {
        const medicamento = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/medicamento/${idUrgenciaMedicamento}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return medicamento

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar medicamento de urgencia")
        console.log(JSON.stringify(error))
    }
}

async function deleteMedicamento(idMedicamento) {

    if (idMedicamento === null || idMedicamento === undefined)
        return

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el medicamento?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Medicamento/${idMedicamento}`
        }

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Medicamento eliminado`)
                $("#tblMedicamentosBox tbody").empty()
                actualizarTablaMedicamentosUrgencia()
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar el medicamento en urgencia: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}

function pintaHeaderModalMedicamento(dataModal) {

    $("#mdlCerrarMedicamentoUrg .modal-header h4").text(`${dataModal.originalTitle}`)

    if (dataModal.originalTitle === "Acción realizada") {
        $("#mdlCerrarMedicamentoUrg .modal-header").addClass("modal-header-success").removeClass("modal-header-warning modal-header-info")
        $("#mdlCerrarMedicamentoUrg .modal-header h4").html(`<i class="fas fa-check"></i> ${dataModal.originalTitle}`)
    }
    else if (dataModal.originalTitle === "Acción No realizada") {
        $("#mdlCerrarMedicamentoUrg .modal-header").addClass("modal-header-warning").removeClass("modal-header-success modal-header-info")
        $("#mdlCerrarMedicamentoUrg .modal-header h4").html(`<i class="fas fa-exclamation-triangle"></i> ${dataModal.originalTitle}`)
    }
    else if (dataModal.originalTitle === "Ver Medicamento") {
        $("#mdlCerrarMedicamentoUrg .modal-header").removeClass("modal-header-warning modal-header-success")
        $("#mdlCerrarMedicamentoUrg .modal-header").addClass("modal-header-info")
        $("#mdlCerrarMedicamentoUrg .modal-header h4").html(`<i class="fas fa-eye"></i> ${dataModal.originalTitle}`)
    }
}

function imprimirMedicamentosUrg() {

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Medicamentos/Imprimir`
    ImprimirApiExterno(url)
}
async function actualizarTablaMedicamentosUrgencia() {
    const medicamentos = await getMedicamentosUrgencia(nSession.ID_INGRESO_URGENCIA)
    await dibujarMedicamentosSeleccionadosUrg(medicamentos)
}

