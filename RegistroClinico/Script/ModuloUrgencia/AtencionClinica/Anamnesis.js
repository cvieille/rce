// Buscando el nombre del usuario logeado
let nombreUsrLogeado = obtenerNombreUsuario();

$(document).ready(async function () {

    $('#mdlAnamnesis').on('hidden.bs.modal', function () {
        validarCampos("#divInfoAnamnesisExamenFisico");
    });
});

async function modalAnamnesisUrgencia(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);

    // Mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteAnamnesis");

    if (showModal) {

        // Eliminar el ID almacenado en el bot�n de guardar
        $("#btnGuardarAnamnesis").removeData("id");

        $("#btnGuardarAnamnesis")
            .html('<i class="fa fa-save"></i> Guardar')
            .removeClass('btn-warning')
            .addClass('btn-primary');


        $("#txtAnamnesis").val("");
        $("#txtExamenFisico").val("");

        $('#mdlAnamnesis').modal('show');
    }

    ShowModalCargando(false);
}

async function cargarHistorialAnamnesisExamenFisico() {
    //const perfilesPermitidosAnamnesis = [1, 12, 22, 25, 30];
    const perfilesPermitidosAnamnesis = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.enfermeraUrgencia,
        perfilAccesoSistema.matroneriaUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ];


    if (!perfilesPermitidosAnamnesis.includes(nSession.CODIGO_PERFIL) && nSession.CODIGO_PERFIL !== 3 && nSession.CODIGO_PERFIL !== 29) {
        console.info(`El usuario con codigo perfil ${nSession.CODIGO_PERFIL} no tiene acceso a los anamnesis`)
        return;
    }
    
    $("#historialAnamnesisExamenFisico").empty();

    try {
        const res = await promesaAjax("GET", `URG_Atenciones_Urgencia/${IdDauAtencionClinica}/Medico`);
        if (res.length > 0) {
            anamnesisAnterior = res[res.length - 1].Anamnesis;
            examenAnterior = res[res.length - 1].ExamenFisico;
            $("#txtAnamnesis").val(anamnesisAnterior);
            $("#txtExamenFisico").val(examenAnterior);

            $("#ocultarSinAnamnesis").show();

            for (let i = res.length - 1; i >= 0; i--) {
                const item = res[i];
                const esUsrLogeado = item.Profesional && item.Profesional.Id === nSession.ID_PROFESIONAL;
                const esProfesional = perfilesPermitidosAnamnesis.includes(nSession.CODIGO_PERFIL);

                $("#historialAnamnesisExamenFisico").append(`
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 mb-2">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <div class="d-flex flex-row justify-content-between align-items-center flex-nowrap">
                                    <div class="mr-auto p-2 bd-highlight">
                                        <i class="fa fa-circle fa-2 text-success" aria-hidden="true"></i>
                                        Por: ${item.Profesional !== null ? item.Profesional.Valor : `Sin profesional registrado`}
                                    </div>
                                    <div class="p-2 bd-highlight">
                                        ${moment(item.Fecha).format("DD-MM-YYYY HH:mm")}                                            
                                    </div>
                                    <div class="p-2 bd-highlight">
                                        ${esProfesional && esUsrLogeado ? `
                                        <button class="btn btn-warning btn-sm" onclick="editarAnamnesisExamenFisico(event, ${item.IdAtencionUrgenciaMedico})">
                                            <i class="fas fa-edit"></i>
                                        </button>` : ''}
                                    </div>
                                </div>                                
                            </div>
                            <div class="card-body">
                                <div class="card w-100 p-2">
                                    <h5 class="card-title">Anamnesis</h5>
                                    <p>${item.Anamnesis}</p>
                                </div>
                                <div class="card w-100 p-2">
                                    <h5 class="card-title">Examen f&iacute;sico</h5>
                                    <p class="card-text">${item.ExamenFisico}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            }
        } else {
            throw new Error("No se encontraron registros");
        }
    } catch (error) {
        $("#ocultarSinAnamnesis").hide();
        $("#historialAnamnesisExamenFisico").append(`<h5>No se encontraron registros de anamnesis o examen f&iacute;sico</h5>`);
    }
}
async function guardarAnamnesis() {
    const idAtencionUrgenciaMedico = $("#btnGuardarAnamnesis").data("id"); // Obtener el ID del bot�n de guardar

    // Desactivar el bot�n para evitar m�ltiples clics
    $("#btnGuardarAnamnesis").prop('disabled', true);

    // Validar campos
    if (validarCampos("#divInfoAnamnesisExamenFisico")) {
        let objetoEnviar = {
            Anamnesis: $("#txtAnamnesis").val(),
            ExamenFisico: $("#txtExamenFisico").val()
        };

        try {
            if (idAtencionUrgenciaMedico) {
                // Edici�n de una anamnesis existente
                let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${IdDauAtencionClinica}/Medico/${idAtencionUrgenciaMedico}`;

                await $.ajax({
                    method: "PUT",
                    url: url,
                    data: objetoEnviar,
                    success: function (data) {
                        cargarHistorialAnamnesisExamenFisico();
                        $('#mdlAnamnesis').modal('hide');
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: "Anamnesis actualizado",
                            showConfirmButton: false,
                            timer: 3000
                        });
                    },
                    error: function (error) {
                        console.log(error);
                        console.error("Error al intentar actualizar el diagn�stico");
                    },
                    complete: function () {
                        // Habilitar el bot�n nuevamente despu�s de completar la solicitud
                        $("#btnGuardarAnamnesis").prop('disabled', false);
                    }
                });
            } else {
                // Creaci�n de una nueva anamnesis
                let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${IdDauAtencionClinica}/Medico`;

                await $.ajax({
                    method: "POST",
                    url: url,
                    data: objetoEnviar,
                    success: function (data) {
                        cargarHistorialAnamnesisExamenFisico();
                        $('#mdlAnamnesis').modal('hide');
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: "Anamnesis guardado",
                            showConfirmButton: false,
                            timer: 3000
                        });
                    },
                    error: function (error) {
                        console.log(error);
                        console.error("Error al intentar guardar el diagn�stico");
                    },
                    complete: function () {
                        // Habilitar el bot�n nuevamente despu�s de completar la solicitud
                        $("#btnGuardarAnamnesis").prop('disabled', false);
                    }
                });
            }
        } catch (error) {
            console.error("Error en la solicitud:", error);
            // Habilitar el bot�n en caso de error
            $("#btnGuardarAnamnesis").prop('disabled', false);
        }
    } else {
        // Habilitar el bot�n si la validaci�n falla
        $("#btnGuardarAnamnesis").prop('disabled', false);
    }
}

async function editarAnamnesisExamenFisico(event, idAtencionUrgenciaMedico) {
    event.preventDefault();

    try {
        const res = await promesaAjax("GET", `URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Medico`);

        if (res.length > 0) {
            const objetoEncontrado = res.find(objeto => objeto.IdAtencionUrgenciaMedico === idAtencionUrgenciaMedico);

            if (objetoEncontrado) {
                // Limpiar campos antes de asignar nuevos valores
                $("#txtAnamnesis").val("");
                $("#txtExamenFisico").val("");

                // Asignar nuevos valores
                $("#txtAnamnesis").val(objetoEncontrado.Anamnesis);
                $("#txtExamenFisico").val(objetoEncontrado.ExamenFisico);

                // Guardar el ID en el bot�n de guardar
                $("#btnGuardarAnamnesis").data("id", idAtencionUrgenciaMedico);

                // Cambiar el texto del bot�n a "Actualizar"
                $("#btnGuardarAnamnesis").text("Actualizar");

                mostrarUsuarioLogueadoEnModales("#mdlAnamnesis")

                // Mostrar el modal
                $('#mdlAnamnesis').modal('show');
            } else {
                console.log("No se encontr� el registro con el ID especificado.");
            }
        }
    } catch (error) {
        console.error("Error al intentar obtener los datos del registro:", error);
        $("#ocultarSinAnamnesis").hide();
        $("#historialAnamnesisExamenFisico").append(`<h5>No se encontraron registros de anamnesis o examen f&iacute;sico</h5>`);
    }
}
