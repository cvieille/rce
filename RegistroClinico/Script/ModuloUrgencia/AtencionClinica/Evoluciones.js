﻿$(document).ready(async function () {
    nSession = getSession();

    //const perfilesPermitidosEvoluciones = [1, 3, 12, 21, 22, 25, 29, 30];
    const perfilesPermitidosEvoluciones = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.apoyoClinico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.tensUrgencia,
        perfilAccesoSistema.enfermeraUrgencia,
        perfilAccesoSistema.matroneriaUrgencia,
        perfilAccesoSistema.kinesiologoUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ];


    if (!perfilesPermitidosEvoluciones.includes(nSession.CODIGO_PERFIL)) {
        console.info(`El usuario con codigo perfil ${nSession.CODIGO_PERFIL} no tiene acceso a las evoluciones`)
    } else {
        await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA);
    }

    //if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 21 || nSession.CODIGO_PERFIL == 22 ||
    //    nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 30) {
    //    await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA);
    //}

    $('#mdlEvolucionesUrg').on('hidden.bs.modal', function () {
        validarCampos("#descEvoluciones");
    });

    //const puedeEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25;
    //$("#deleteBotonEvolucion").attr("disabled", !puedeEliminar);
});

async function actualizarHistorialEvoluciones(idAtencionUrgencia) {
    const historialEvoluciones = await getEvolucionesUrg(idAtencionUrgencia);
    // Verificar si historialEvoluciones es undefined o 0
    if (!historialEvoluciones || historialEvoluciones.length === 0) {
        // Ocultar div con la tabla evoluciones
        $("#divHistorialEvoluciones").hide();
        return;
    }
    const evoluciones = await prepararDatosHistorialEvoluciones(historialEvoluciones);
    dibujarTablaHistorialEvolucionesUrg(evoluciones);
    mostrarUsuarioLogueadoEnModales("#mdlEvolucionesUrg")
    $("#divHistorialEvoluciones").show();
}

async function modalEvolucionesUrg(showModal, objAtencionUrgencia) {
    ShowModalCargando(true)

    $("#spnProfesionalEvolucionUrg").text($("#sltProfesionEvolucionAtencion :selected").text())
    $("#spnNombreProfesionalUrg").text($("#txtProfesionalEvolucionAtencion").val())

    // Llama a la función para mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteEvolucionUrg")

    if (showModal) {

        await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA)
        
        const $botonGuardarEvolucion = $("#btnguardarEvolucionUrg")

        $botonGuardarEvolucion
            .attr("data-accion", "post")
            .data("accion", "post")
            .removeAttr("data-id data-descripcion")
            .text("Agregar evolución")

        $("#tituloModalEvolucion").text("Ingresar evolución")
        $("#txtEvolucionUrg").val("")

        

        $('#mdlEvolucionesUrg').modal('show')
    }

    ShowModalCargando(false)
}

function dibujarTablaHistorialEvolucionesUrg(evoluciones) {
    $('#tblEvolucionesUrg').DataTable({
        data: evoluciones,
        ordering: false,
        language: {
            "emptyTable": "No hay evoluciones disponibles para mostrar",
        },
        columns: [
            { data: 'Id', title: 'Id' },
            { data: 'Fecha', title: 'Fecha' },
            { data: 'Descripcion', title: 'Descripción' },
            { data: 'Profesional', title: 'Profesional' },
            { data: 'IdProfesional', title: 'ID Profesional' },
            { data: null, title: 'Acciones' }
        ],
        columnDefs: [
            {
                targets: 1,
                render: function (data, type, full) {
                    return `<div>${capturarExtractoParrafo(showModalEvolucionUrg, data, 6, full.Id)}</div>`;
                }
            },
            {
                targets: -1,
                render: function (data) {
                    return botonesAccionEvolucion(data);
                }
            },
            {
                targets: 4, visible: false
            }
        ],
        "bDestroy": true
    });
}

function showModalEvolucionUrg(data, id) {
    const modal = $("#mdlEvolucionDescripcionUrg");
    const descripcion = $("#descripcionEvolucionUrg");
    modal.attr("data-id-evolucion", id);
    modal.data("id-evolucion", id);
    descripcion.text(data);
    modal.modal("show");
}

function prepararDatosHistorialEvoluciones(data) {
    if (data === undefined) return [];

    return data.map(e => ({
        Id: e.Id,
        Descripcion: e.Descripcion,
        Fecha: moment(e.Fecha).format('DD-MM-YYYY HH:mm:ss'),
        Profesional: `${e.Profesional.Valor}`,
        IdProfesional: `${e.Profesional.Id}`,
        Profesion: `${e.Profesion.Valor}`
    }));
}

async function guardarEvolucionUrg(e) {

    if (!validarCampos("#descEvoluciones"))
        return

    let descripcion = $("#txtEvolucionUrg").val()
    const id = $(e).attr("data-id")
    let { accion } = $(e).data()

    let evolucion = {
        Descripcion: descripcion
    };

    // Deshabilitar el botón
    $("#btnguardarEvolucionUrg").prop('disabled', true);

    // Simular una respuesta lenta para probar
    //await new Promise(resolve => setTimeout(resolve, 5000));

    if (id !== undefined && accion === 'update') {

        evolucion.Id = parseInt(id)
        await patchEvolucion(evolucion)
    }
    else if (accion === 'post')
        await postEvolucionUrg(evolucion)

    // Habilitar el botón después de la solicitud exitosa
    $("#btnguardarEvolucionUrg").prop('disabled', false);
}

// POST
async function postEvolucionUrg(evolucion) {
    if (evolucion === undefined) return;

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Evolucion`
    };

    try {
        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(evolucion),
            contentType: 'application/json'
        });

        await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA);

        toastr.success("Evolución guardada");
        $("#mdlEvolucionesUrg").modal("hide");
        $("#txtEvolucionUrg").val("");

    } catch (error) {
        console.error('Error en la guardando evolución:', error);
        throw error;
    }
}

// GET
async function getEvolucionesUrg(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        throw new Error("No se paso id de la atencion de urgencia")
    const perfilesPermitidosEvoluciones = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.apoyoClinico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.tensUrgencia,
        perfilAccesoSistema.enfermeraUrgencia,
        perfilAccesoSistema.matroneriaUrgencia,
        perfilAccesoSistema.kinesiologoUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ];

    if (!perfilesPermitidosEvoluciones.includes(nSession.CODIGO_PERFIL)) {
        console.info(`El usuario con codigo perfil ${nSession.CODIGO_PERFIL} no tiene acceso a los Evoluciones`)
        return;
    }

    try {
        const evoluciones = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Evoluciones`,
            contentType: 'application/json',
            dataType: 'json'
        });

        return evoluciones;

    } catch (error) {
        if (error.status === 400) {
            return;
        }
    }
}

async function patchEvolucion(evolucion) {

    let parametrizacion = {
        method: "PATCH",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Evolucion/${evolucion.Id}`,
    };

    delete evolucion.Id

    await $.ajax({
        method: parametrizacion.method,
        url: parametrizacion.url,
        data: evolucion,
        success: async function (data) {

            toastr.success("Evolución actualizada");
            $("#mdlEvolucionesUrg").modal("hide");
            await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA);
        },
        error: function (error) {
            console.log(error);
            console.error("Error al intentar actualizar la evolucion");
        }
    });
}
async function editEvolucion(e) {

    const { id, idProfesional, descripcion } = $(e).data()
    let esPropietario = (idProfesional === nSession.ID_PROFESIONAL)

    // no es propietario de la evolucion
    if (!esPropietario) {
        mensajeProfesionalNoEsPropietario()
        return
    }

    const objAtencionUrgencia = cargarDatosPacienteModalEvolucion(nSession.ID_INGRESO_URGENCIA, descripcion)
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteEvolucionUrg")

    const $btnGuardarEvolucion = $("#btnguardarEvolucionUrg")

    $btnGuardarEvolucion
        .attr({ "data-accion": "update", "data-id": id, "data-descripcion": descripcion })
        .data({ accion: "update", id: id, descripcion: descripcion })

    $("#tituloModalEvolucion").text("Actualizar evolución")
    $btnGuardarEvolucion.text("Actualizar evolución")

    $("#mdlEvolucionesUrg").modal("show")
}

function mensajeProfesionalNoEsPropietario() {

    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: "Usted no es el autor de la evolución",
        showConfirmButton: true,
    })
}

function cargarDatosPacienteModalEvolucion(id, descripcion) {

    let json = GetJsonIngresoUrgencia(id)

    let objAtencionUrgencia = {
        idAtencion: json.Id,
        idPaciente: json.IdPaciente,
        motivoConsulta: json.MotivoConsulta,
        categorizacion: categorizacionUrgencia
    }

    $("#spnProfesionalEvolucionUrg").text($("#sltProfesionEvolucionAtencion :selected").text())
    $("#spnNombreProfesionalUrg").text($("#txtProfesionalEvolucionAtencion").val())
    $("#txtEvolucionUrg").val(descripcion ?? "")

    return objAtencionUrgencia
}

// DELETE
async function deleteEvolucion(idEvolucion, idProfesional) {
    if (idEvolucion === null || idEvolucion === undefined) {
        return;
    }

    // Verificar si el usuario es el propietario de la evolución
    const esPropietario = idProfesional === nSession.ID_PROFESIONAL;
    if (!esPropietario) {
        mensajeProfesionalNoEsPropietario();
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar la evolución?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Evolucion/${idEvolucion}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            success: async function (data, status, jqXHR) {
                toastr.success(`Evolución eliminada`);

                // Eliminar la fila correspondiente a la evolucion eliminada
                const table = $('#tblEvolucionesUrg').DataTable();
                const rowIndex = table.row(`#evolucion_${idEvolucion}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de evolucion despues de eliminar
                await actualizarHistorialEvoluciones(nSession.ID_INGRESO_URGENCIA);

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divHistorialEvoluciones').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar la evolucion: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// BOTONES 
function botonesAccionEvolucion({ Id, IdProfesional, Descripcion }) {

    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    const html = `
        <div class="d-flex flex-row justify-content-center">
            <div>
                <button
                    type="button"
                    id="deleteBotonEvolucion_${Id}"
                    class="btn btn-danger deleteEvolucion m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}"
                    onclick="deleteEvolucion(${Id}, ${IdProfesional})"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Eliminar"
                    ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                >
                    <i class="fa fa-trash fa-xs"></i>
                </button>
            </div>
            <div>
                <button
                    type="button"
                    class="btn btn-warning m-1"
                    onclick="editEvolucion(this)"
                    data-id="${Id}"
                    data-id-profesional="${IdProfesional}"
                    data-descripcion="${Descripcion}"
                >
                    <i class="fa fa-edit fa-xs"></i>
                </button>
            </div>
        </div>
    `;

    return html;
}

