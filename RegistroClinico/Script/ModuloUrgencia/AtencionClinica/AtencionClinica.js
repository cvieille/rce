﻿let categorizacion = {}
let IdDauAtencionClinica, anamnesisAnterior, examenAnterior, idEventoUrgencia, objetoAtencionUrgenciaGlobal
$(document).ready(async function () {

    //agrega clase col-md-12 si el ancho es 991px para el responsive
    window.addEventListener('resize', function () {
        //checkResolusionUrg(991)
        checkResolusionUrg(1207)
    });

    $("#ultimaTomaSignosUrg").hide()

    //Se oculta div temporal de ginecoobstetrico
    $("#divDatosGinecoObstetrico").hide()
    ShowModalCargando(false)
    $("#divDPacienteDatosClinico").hide()
    nSession = getSession();
    let json = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);


    console.log(json.ValidadoPorMedico)
    //Cuando esta validad, o no es un medico se oculta boton de validar dau
    if (json.ValidadoPorMedico || nSession.CODIGO_PERFIL!==1) {
        $("#aValidarDau").addClass('d-none')
    }

    // Mostrar SweetAlert al cargar la página para validar usuario logeado
    Swal.fire({
        title: 'Usted ha iniciado sesión como',
        html: `${nSession.NOMBRE_USUARIO}<br><br>
        <p style="font-size: 14px;">Si este usuario no corresponde, por favor haga clic en el botón <strong>"Cambiar Usuario"</strong> para evitar que el formulario salga a nombre de otra persona.</p>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cambiar usuario',
        allowEscapeKey: false,
        allowOutsideClick: false,
    }).then((result) => {
        if (result.isConfirmed) {
            // Simplemente se cierra el sweet alart y nos deja trabajar en la atencion clinoca
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            // Si el usuario hace clic en "Cambiar usuario" se va al login
            window.location.replace(`${ObtenerHost()}/Vista/Default.aspx`);
        }
    });

    idEventoUrgencia = json?.IdEvento
    //Guardar el idDau
    IdDauAtencionClinica = nSession.ID_INGRESO_URGENCIA
    //CargarSignosVitalesPorId(nSession.ID_INGRESO_URGENCIA)

    //PACIENTE
    await cargarPaciente(json?.IdPaciente)

    //Aca consulto atenciones de urgencia previa del paciente
    cargarAtencionesUrgenciaPrevias(json?.IdPaciente, nSession.ID_INGRESO_URGENCIA)

    //CATEGORIZACION
    categorizacionUrgencia = await getCategorizacion(nSession.ID_INGRESO_URGENCIA)
    await cargarCategorizacion(categorizacionUrgencia)

    let objAtencionUrgencia = {
        idAtencion: json.Id,
        idPaciente: json.IdPaciente,
        motivoConsulta: json.MotivoConsulta,
        categorizacion: categorizacionUrgencia,
        tipoAtencion: json.TipoAtencion,
        tipoEstado: json.TipoEstadoSistema
    }
    objetoAtencionUrgenciaGlobal = objAtencionUrgencia
    //SIGNOS VITALES
    await cargarUltimosSignosVitalesUrg("#divDetalleSignosVitalesUrg", nSession.ID_INGRESO_URGENCIA)

    //PINTAR NOMBRE PACIENTE EN LABEL
    let nombrePaciente = await cargarPaciente(json.IdPaciente);

    // Actualiza el lblPacienteSignosVitales con el nombre del paciente
    $("#lblPacienteSignosVitales").text(nombrePaciente);

    $("#aGuardarSignoVital").click(function (event) {
        event.preventDefault();
        const valida = $(this).val() === "Editar"

        if (valida) {
            $(this).removeAttr("value")
        }
        else {
            const idIngreso = nSession.ID_INGRESO_URGENCIA;
            GuardarSignosVitales(idIngreso);
        }
    });

    //ANTECEDENTES CLINICOS PACIENTE
    await cargarInformacionAdicionalUrg(objAtencionUrgencia)

    //EVOLUCIONES
    await dibujarTablaHistorialEvolucionesUrg()

    //EXAMENES DE LABORATORIO E IMAGEN
    dibujarTablaExamenes(json.IdEvento, true, true)


    if (json.TipoAtencion !== null) {
        cargarFavoritosAranceles(json.TipoAtencion.Id)
    }

    //ANTECEDENTES OBSTETRICOS PENDIENTE....
    if (json.TipoAtencion === 2) {
        const obstetricos = await getOsbtetricos(nSession.ID_INGRESO_URGENCIA)
        cargarAtencionClinicaObstetrica(json.AtencionMedica.AtencionObstetrica, json.AtencionAdministrativa.TipoAtencion.Id);
    }

    //MOTIVO CONSULTA
    cargarMotivoConsulta(json)

    //ALCOHOLEMIA Y TOXICOLOGICO
    cargarAlcoholemiaYtoxicologico()

    //COMBO APRECIACION CLINICA
    comboApreciacionClínica()

    //PROFESIONAL
    await cargarProfesional()

    comboProfesionEvolucionAtencion(nSession.ID_PROFESIONAL)

    // COMBOS VIOLENCIA POR CONDICION
    combosViolencia();

    // COMBO MOTIVO EMERGENCIA OBSTETRICA
    comboMotivoEmergenciaObstetrica();

    // COMBO DE MORDEDURA
    combosMordedura();

    //Combo tipo atencion imagenologia
    cargarComboTipoAtencionImagenologia()

    //Cargar formularios externos
    cargarTablaFormulariosExternos(false);

    // Cargar Solicitudes de Hospitalización
    cargarTablaSolicitudHospitalizacion(json.IdEvento, false);

    //ANAMNESIS Y EXAMEN FISICO
    await cargarHistorialAnamnesisExamenFisico();

    $("#aSignosVitalesHistorial").on('click', async (e) => {
        await modalHistorialSignosVitales(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlHistorialSignosVitales")
    })

    $("#aAnamnesis").on('click', async (e) => {
        await modalAnamnesisUrgencia(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlAnamnesis")
    })

    buscarCrearTablaDiagnosticos();

    $("#aViolencia").on('click', async (e) => {
        await modalViolencia(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlViolencia")
    })

    const { tipoAtencion } = objAtencionUrgencia

    if (tipoAtencion?.Id === 2) {
        $("#aGinecoObstetrico").on('click', async (e) => {
            await modalGinecoObstetrico(true, objAtencionUrgencia);
            mostrarUsuarioLogueadoEnModales("#mdlObstetrico")
        })
    }
    else {
        $("#aGinecoObstetrico").attr("disabled", true)
        $("#aGinecoObstetrico").addClass("disabled")
    }

    // LPP
    if (nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 22) {
        $("#aLpp").on('click', async (e) => {
            await modalLpp(true, objAtencionUrgencia);
            mostrarUsuarioLogueadoEnModales("#mdlLppAdulto")
            mostrarUsuarioLogueadoEnModales("#mdlLppPediatrico")
        })
    }
    else {
        $("#aLpp").attr("disabled", true)
        $("#aLpp").addClass("disabled")
    }

    $("#aEvolucionesUrg").on('click', async (e) => {
        await modalEvolucionesUrg(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlEvolucionesUrg")
    })

    $("#aAntecedentes").on('click', async (e) => {
        const TipoAlternativa = json.TipoAlternativa
        const newObjAtencionUrgencia = { ...objAtencionUrgencia, TipoAlternativa }
        await modalAntecendetes(true, newObjAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlAntecedentesUrg")
    })

    $("#aAlcoholemiaToxicologico").on('click', async (e) => {
        await modalAlcoholemiaToxicologico(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlAlcoholemiaToxicologico")
    })

    $("#aAltaMedica").on('click', async (e) => {
        let { TipoEstadoSistema } = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);
        objAtencionUrgencia.tipoEstado = TipoEstadoSistema;
        $("#btnEgresarAltaDigitador, #btnEgresarAlta").data("idPaciente", objAtencionUrgencia.idPaciente);
        $("#btnEgresarAltaDigitador, #btnEgresarAlta").data("idEvento", idEventoUrgencia);
        
        await linkEgresoIngresoUrgencia(objAtencionUrgencia, true);
        mostrarUsuarioLogueadoEnModales("#mdlEgresarAtencion")
    })

    $("#aComentariosDau").data("id", nSession.ID_INGRESO_URGENCIA)

    $("#aMordedura").on('click', async (e) => {
        await modalMordedura(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlMordedura")
    })

    $("#aSolicitudesExternas").on('click', async (e) => {
        // Oculta botones para perfil matroneria urgencia
        if (nSession.CODIGO_PERFIL === 25) {
            $("#btnNuevoFQX").prop("disabled", true)
            $("#btnNuevaSolicitudTrasfusion").prop("disabled", true)
        }
        $("#btnNuevoFQX").data("idPaciente", objAtencionUrgencia.idPaciente);
        //prueba para solicitud externa de transfusion
        $("#btnNuevaSolicitudTrasfusion").data("idPaciente", objAtencionUrgencia.idPaciente);
        mostrarUsuarioLogueadoEnModales("#mdlFormulariosExternos")
        $("#mdlFormulariosExternos").modal("show")
    })

    $("#aProcedimientos").on('click', async (e) => {
        await modalProcedimientos(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlProcedimientos")
    })

    $("#aMedicamentoBox").on('click', async (e) => {
        await modalMedicamentos(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlMedicamentos")
    })

    $("#aInterconsultor").on('click', async (e) => {
        await modalInterConsultorUrg(true, objAtencionUrgencia);
        mostrarUsuarioLogueadoEnModales("#mdlInterconsultorUrg")
    })

    $("#btnVolverBandejaUrgencia").on('click', async (e) => {
        window.location.href = `${ObtenerHost()}/Vista/ModuloUrgencia/VistaMapaCamasUrgencia.aspx`;
    });

    $("#btnNuevoFQX").unbind().on('click', async (evt) => {
        evt.preventDefault();
        const idPaciente = $("#btnNuevoFQX").data("idPaciente");
        IrASolicitudPabellon(idPaciente, idEventoUrgencia);
    })

    $("#btnNuevaSolicitudTrasfusion").unbind().on('click', function (evt) {
        evt.preventDefault();
        const idPaciente = $(this).data("idPaciente");
        IrASolicitudTransfusion(idPaciente);
    });

    $("#btnSolicitudDeHospitalizacion").unbind().on('click', function (evt) {
        evt.preventDefault();
        IrASolicitudHospitalizacion(objAtencionUrgencia.idPaciente, idEventoUrgencia, IdDauAtencionClinica);
    });

    $("#sltTipoAtencionImagen").on('change', () => {
        if ($("#sltTipoAtencionImagen").val() == 2) {
            $("#divMedicamentosTomografia , #divAlergiasTomografia").show()
        } else {
            $("#divMedicamentosTomografia , #divAlergiasTomografia").hide()
        }
    })

    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divInfoPacienteDiagnosticos")
    //showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteAnamnesis")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteAlcoholemiaToxicologico")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divHistorialSignosVitales")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divPacienteViolencia")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteMordedura")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteAnamnesis")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteObstetrico")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteLppAdulto")
    showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteLppPediatrico")


    //perf medico, matro, admin si ve anamnesis

    // Controlando que perfil TENS URGENCIA SOLO VISUALICE

    // Mostrar/ocultar elementos y deshabilitar eventos según el perfil
    if (nSession.CODIGO_PERFIL == 21 || nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3) {
        $("#aAnamnesis").hide(); // Ocultar botón de agregar anamnesis
        $("#otrosDatos").show(); // Mostrar otros datos
        deshabilitarEventosOcultar(); // Deshabilitar eventos

        if (nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3) {
            $("#divAgregarDiagnostico").hide(); // Ocultar agregar diagnóstico para perfiles 29 y 3
        }
    }

    //Esta funcion esta en el archivo examenes.js, es un evento para limpiar las tablas cuando se sale del modal
    inicializarEventoModal('#mdlExamenes, #mdlImagenologia')
    //Esta funcion esta en el archivo examenes.js y sirve para inicializar las tablas de atencion clinica
    inicializarTablasAgregarMedicamentos()

    //Medicamentos habilitado solo perfil medico y matrona
    let perfilesPermitidosAccionesClinicas = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.matronaUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ]
    //$("#aMedicamentoBox").attr("disabled", !perfilesPermitidosAccionesClinicas.includes(nSession.CODIGO_PERFIL))
    $("#aProcedimientos").attr("disabled", !(perfilesPermitidosAccionesClinicas.includes(nSession.CODIGO_PERFIL) || nSession.CODIGO_PERFIL === 21 ||  nSession.CODIGO_PERFIL === 22 ))
    //$("#aLaboratorio").attr("disabled", !perfilesPermitidosAccionesClinicas.includes(nSession.CODIGO_PERFIL))
    //$("#aImagenologia").attr("disabled", !perfilesPermitidosAccionesClinicas.includes(nSession.CODIGO_PERFIL))
    $("#aInterconsultor, #aMedicamentoBox, #aLaboratorio, #aImagenologia").attr("disabled", !perfilesPermitidosAccionesClinicas.includes(nSession.CODIGO_PERFIL))

    //integracion infinity 

    let rutpaciente = $("#txtRutPacienteUrg").text(), fechaActual = moment(GetFechaActual()).format("YYYY-MM-DD"), fechaLlegada = moment(json.FechaLlegada).format("YYYY-MM-DD")
    let urlInifity = `RCE_Solicitud_Laboratorio/Infinity/Ordenes?rut=${rutpaciente}&fechaInicio=${fechaLlegada}&fechaFinal=${fechaActual}`
    promesaAjax(`GET`, urlInifity).then(res => {
        if (res !== null)
            cargarTablaExamenesInfinity(res)
    }).catch(error => {
        console.error("Ocurrio un error al intentar obtener los examenes de laboratorio (INFINITY)")
        console.log(error)
        console.log(error.xhr.responseText)
        ShowModalCargando(false)
    })
    //validaciones medicas 
    cargarValidacionesMedicas(nSession.ID_INGRESO_URGENCIA);

})

async function deshabilitarEventosOcultar() {
    // Ocultando otros datos
    $("#aViolencia").hide();
    $("#aAlcoholemiaToxicologico").hide();
    $("#aMordedura").hide();
    $("#aGinecoObstetrico").hide();
    $("#aSolicitudesExternas").hide();
    $("#aAltaMedica").hide();
}
async function cargarFavoritosAranceles(idTipoAtencion = 1) {
    let urlExamenesFavoritos = `RCE_solicitud_imagenologia/${idTipoAtencion}/Favoritos`
    let urlExamenesLabFavoritos = `RCE_solicitud_laboratorio/${idTipoAtencion}/Favoritos`
    let urlProcedimientos = `URg_Atenciones_Urgencia/${idTipoAtencion}/procedimiento/Favoritos`
    //Procedimientos favoritos
    let arsenalProcedimientosFavoritos = await promesaAjax("get", urlProcedimientos)
    dibujarFavoritosDiv("#divListaProcedimientosModal", arsenalProcedimientosFavoritos, 3)
    //Examenes laboratorio favoritos
    let arsenalExamenesLaboratorio = await promesaAjax("get", urlExamenesLabFavoritos)
    dibujarFavoritosDiv("#divModalExamenesLaboratorio", arsenalExamenesLaboratorio, 5)
    //Examenes imagen favoritos
    let arsenalExamenesImagen = await promesaAjax("get", urlExamenesFavoritos)
    dibujarFavoritosDiv("#divModalExamenesImagenologia", arsenalExamenesImagen, 4)
}

function dibujarFavoritosDiv(divFavoritos, arrayFavoritos, idTipoArancel) {
    let funcion = "validarExamenIngresado"
    if (idTipoArancel == 3)
        funcion = "obtenerHtmlProcedimientos"
    if (Array.isArray(arrayFavoritos)) {
        arrayFavoritos.forEach(item => {
            $(divFavoritos).append(`<div class="col-md-4 col-xs-12"> <button class="btn btn-outline-dark btn-sm m-1 w-100" onclick='${funcion}(${idTipoArancel},${JSON.stringify(item)})' type="button" style="overflow:hidden;">${item.DescripcionCarteraServicio}</button></div>`)
        })
    }
}
function imprimirSolicitudExamenUrgencia(idSolicitud, idTipoArancel) {
    if (idTipoArancel == null || idSolicitud == null)
        throw new Error("Error no paso los parametros requeridos idSolicitudExamen o idTipoArancel no puede ser null");

    let url = `${GetWebApiUrl()}`
    if (idTipoArancel == 5)
        url += `RCE_Solicitud_Laboratorio/${idSolicitud}/Imprimir`
    else
        url += `RCE_Solicitud_Imagenologia/${idSolicitud}/Imprimir`
    try {
        ImprimirApiExterno(url, null, "GET")
    } catch (error) {
        console.error("Ocurrio un errro al imprimir la solicitud de examen")
        console.log(error)
    }
}

function cargarComboTipoAtencionImagenologia() {
    setCargarDataEnCombo(`${GetWebApiUrl()}RCE_Tipo_Solicitud_Imagenologia/combo`, false, `#sltTipoAtencionImagen`)
}

async function dibujarTablaExamenes(idEvento, cargarExamenLaboratorio = false, cargaExamenImagenologia = false) {

    let columnas = [
        { title: "Id", data: "Id", className: "text-center" },
        { title: "Fecha", data: "Fecha", className: "text-center" },
        { title: "Solicitante", data: "Profesional", orderable: false, className: "text-center" },
        { title: "Examenes", data: "Solicitud", orderable: false, className: "text-center" },
        { title: "Observaciones", data: "Observacion", orderable: false, className: "text-center" },
        { title: "Acciones", data: "Id", orderable: false, className: "text-center" }
    ]

    let configColumn = [
        { targets: 1, render: function (fecha) { return moment(fecha).format("DD/MM/YYYY HH:mm") } }//fecha tiene que ser formateada
        , {
            targets: 2,//Profesional validando nulos y retornando el nombre
            render: function (profesional) {
                let nombreProfesional = `No ingresado`
                if (profesional !== null) {
                    if (profesional.Persona !== null) {
                        nombreProfesional = `${profesional.Persona.Nombre} ${profesional.Persona.ApellidoPaterno} ${profesional.Persona.ApellidoMaterno}`
                    }
                }
                return nombreProfesional
            }
        }, {
            targets: 3,//Recorriendo los examenes para devolver un listado 
            render: function (solicitud) {
                let stringExamenes = `Sin examenes, revise la solicitud`
                if (solicitud !== null) {
                    if (solicitud.Examen.length > 0) {
                        stringExamenes = `<ul>`
                        let htmlExamenes = ``
                        solicitud.Examen.map(examen => {
                            htmlExamenes += `<li>${examen.Valor}</li>`
                        })
                        stringExamenes += `${htmlExamenes} </ul>`
                    }
                }
                return stringExamenes
            }
        },
        {
            targets: -2,//Recorriendo los examenes para devolver un listado
            render: function (observacion) {
                if (observacion == null)
                    return `Sin observaciones`

                return observacion
            }
        },
        {
            targets: -1, // Recorriendo los exámenes para devolver un listado
            render: function (solicitud, columna, data) {
                // Verifica si los datos necesarios están presentes para laboratorio e imagenología
                let cierre = data.CierreSolicitudLaboratorio || data.CierreSolicitudImagenologia;
                const examenRealizado = cierre ? cierre.AccionRealizada : null;
                const esPerfilAutorizadoRealizar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 3 || nSession.CODIGO_PERFIL === 30;

                // 1 medico
                // 21 tens urgencia
                // 22 enfermero de urgencia
                // 25 matroneria urgencia
                // 30 administrador urgencia
                const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

                // Construye el HTML de los botones según el estado de AccionRealizada
                let botones = `
                <div class="d-flex flex-row justify-content-center flex-nowrap">
                    <a class="btn btn-info btn-sm m-1" onclick="imprimirSolicitudExamenUrgencia(${data.Id}, ${data.IdTipoArancel})" title="Imprimir">
                        <i class="fa fa-print"></i>
                    </a>
                `;

                if (examenRealizado === null) {
                    botones += `
                    <a class="btn btn-success btn-sm m-1 ${esPerfilAutorizadoRealizar ? '' : 'disabled'}" id="btnAccionRealizadaExamen" onclick="showCerrarExamenUrg(${data.Id}, ${data.IdTipoArancel}, 'Acción realizada')" title="Realizar">
                        <i class="fas fa-check fa-xs"></i>
                    </a>
                    <a class="btn btn-warning btn-sm m-1 ${esPerfilAutorizadoRealizar ? '' : 'disabled'}" id="btnAccionNoRealizadaExamen" onclick="showCerrarExamenUrg(${data.Id}, ${data.IdTipoArancel}, 'Acción No realizada')" title="No realizar">
                        <i class="fas fa-times fa-xs"></i>
                    </a>
                    <a class="btn btn-danger btn-sm m-1 eliminarSolExUrg ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" id="eliminarSolExUrg" onclick="eliminarSolicitudExamenUrgencia(${data.Id}, ${data.IdTipoArancel})" title="Eliminar">
                        <i class="fa fa-trash"></i>
                    </a>
                `;
                } else {
                    botones += `
                    <a class="btn btn-info btn-sm m-1" id="btnVerExamen" onclick="showCerrarExamenUrg(${data.Id}, ${data.IdTipoArancel}, 'Ver examen')" title="Ver Examen">
                        <i class="fas fa-eye fa-xs"></i>
                    </a>
                `;
                }

                return botones;
            }
        }
    ]

    if (cargarExamenLaboratorio) {

        let examenesLaboratorioPorEvento = await buscarExamenesPorEvento(idEvento);
        const realizadosExamanesLaboratorio = examenesLaboratorioPorEvento.filter(e => e.CierreSolicitudLaboratorio !== null).length;
        $("#spLaboratorio").html(`${realizadosExamanesLaboratorio}/${examenesLaboratorioPorEvento.length}`);

        if (examenesLaboratorioPorEvento.length === 0) {
            $("#aLaboratorio").addClass("default")
            $("#aLaboratorio").removeClass("warning")
        }
        else if (examenesLaboratorioPorEvento.length === realizadosExamanesLaboratorio && examenesLaboratorioPorEvento.length > 0) {
            $("#aLaboratorio").addClass("success")
            $("#aLaboratorio").removeClass("warning")
        }
        else {
            $("#aLaboratorio").addClass("warning")
            $("#aLaboratorio").removeClass("default")
            $("#aLaboratorio").removeClass("success")
        }

        if (examenesLaboratorioPorEvento.length > 0)
            $("#divExamenesLaboratorio").show();

        dibujarTablaExamenesIngresados(examenesLaboratorioPorEvento, "#tblExamenesLaboratorio", columnas, configColumn)

    }

    if (cargaExamenImagenologia) {

        let examenesImagenPorEvento = await buscarExamenesImagenPorEvento(idEvento);
        const realizadosExamanesImagenologia = examenesImagenPorEvento.filter(e => e.CierreSolicitudImagenologia !== null).length;
        $("#spExamenesImagenologia").html(`${realizadosExamanesImagenologia}/${examenesImagenPorEvento.length}`);

        if (examenesImagenPorEvento.length === 0) {
            $("#aImagenologia").addClass("default")
            $("#aImagenologia").removeClass("warning")
        }
        else if (examenesImagenPorEvento.length === realizadosExamanesImagenologia && examenesImagenPorEvento.length > 0) {
            $("#aImagenologia").addClass("success")
            $("#aImagenologia").removeClass("warning")
        }
        else {
            $("#aImagenologia").addClass("warning")
            $("#aImagenologia").removeClass("default")
            $("#aImagenologia").removeClass("success")
        }

        if (examenesImagenPorEvento.length > 0)
            $("#divExamenesImagenologia").show();

        dibujarTablaExamenesIngresados(examenesImagenPorEvento, "#tblExamenesImagenologia", columnas, configColumn);
    }

}

//// CARTERA ARANCEL
function GetJsonCarteraFavoritos(idTipoArancel) {

    let array = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/tipoAtencion/${$("#sltTipoAtencion").val()}/tipoArancel/${idTipoArancel}/Favoritos`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            array = data;
        }
    });

    return array;

}
function GetJsonCartera(idTipoArancel) {

    let array = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Cartera_Servicio/Combo?idTipoArancel=${idTipoArancel}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            array = data;
        }
    });

    return array;

}

async function cargarInformacionAdicionalUrg({ idAtencion, motivoConsulta, tipoAtencion }) {
    // MOTIVO
    $("#txtMotivoConsultaUrg").val(motivoConsulta ?? "")

    // ANTECEDENTES 
    const antecedentes = await getAntecendentesUrg(idAtencion)

    // Verificar si antecedentes es vacío, null o undefined
    if (!antecedentes || antecedentes.length === 0) {
        $("#otrosAntecedentes").hide();
        return;
    }

    const atencedentesOrdenados = antecedentesOrderByIdDescending(antecedentes)

    let html = ""
    const LIMITE_MATRONERIA = 5
    const LIMITE_MEDICO = 4

    atencedentesOrdenados.map((a, index) => {
        let count = index + 1

        let limiteAntecedentes = tipoAtencion.Id === 2 //Gineco-Obstetrica
            ? LIMITE_MATRONERIA
            : LIMITE_MEDICO

        // FORMA NUEVA
        if (count < limiteAntecedentes) {
            html += `                            
                <h6>${a.ClasificacionAntecedente.Valor}: <span>${a.UrgenciaClasificacionAntecedente.Valor}</span></h6>                        
            `;
        }
    })

    $("#divAntecedentesMorbidosUrg").html(html)
}



function antecedentesOrderByIdDescending(antecedentes) {

    if (antecedentes.length <= 0)
        return
    const antecedentesOrdenados = antecedentes.sort((a, b) => b.Id - a.Id);

    return antecedentesOrdenados
}

async function cargarCategorizacion(categorizacion) {

    if (categorizacion[0] === undefined)
        return

    const categorizacionCodigo = categorizacion[0].Codigo;

    let colorDivClass = '';
    let textColorClass = '';

    // Asignando clases de color según la categorización con un switch
    switch (categorizacionCodigo) {
        case 'C1':
            colorDivClass = 'bg-danger';
            textColorClass = 'text-white';
            break;
        case 'C2':
            colorDivClass = 'bg-orange';
            textColorClass = 'text-white';
            break;
        case 'C3':
            colorDivClass = 'bg-warning';
            textColorClass = 'text-white';
            break;
        case 'C4':
            colorDivClass = 'bg-success';
            textColorClass = 'text-white';
            break;
        case 'C5':
            colorDivClass = 'bg-info';
            textColorClass = 'text-white';
            break;
        default:
            colorDivClass = 'bg-secondary';
            break;
    }

    const divHTML = `<div class='p-1 ${colorDivClass} rounded'>
                        <h1 class='text-center m-0 ${textColorClass}'><strong>${categorizacionCodigo}</strong></h1>
                    </div>`;
    $("#divCategorizacion").append(divHTML);
    $("#divCategorizacionVacio").hide();
}

function cargarMotivoConsulta(json) {

    //$('#divMotivoConsulta').html(`<p class='text-black'>${json.MotivoConsulta}</p>`);
}


// ANTECEDENTE CLINICOS PACIENTE (CATEGORIZACIÓN)
function CargarAntecedentesClinicosPaciente(json) {

    $("#divAntecedentesPacienteVacio, #divAntecedentesPaciente").hide();

    if (json.AntecedentesClinicos !== null) {

        $("#spnAlergias").text(json.AntecedentesClinicos.TipoAlternativaAntecedentes.Valor);

        if (json.AntecedentesClinicos.TipoAlternativaAntecedentes.Valor === "SI") {
            $("#divDescripcionAlergias").show();
            $("#spnDetalleAlergias").text(json.AntecedentesClinicos.Alergias);
        }

        if (json.AntecedentesClinicos.AntecedentesMorbidos != null) {
            $("#divAntecedentesMorbidos").show();
            $("#spnAntecedentesMorbidos").text(json.AntecedentesClinicos.AntecedentesMorbidos)
        }

        if (json.AntecedentesClinicos.AntecedentesQx != null) {
            $("#divAntecedentesQx").show();
            $("#spnAntecedentesQx").text(json.AntecedentesClinicos.AntecedentesQx);
        }

        if (json.AntecedentesClinicos.AntecedentesGinecoObstetrico != null) {
            $("#divAntecedentesGinecoObstetrico").show();
            $("#spnAntecedentesGinecoObstetrico").text(json.AntecedentesClinicos.AntecedentesGinecoObstetrico);
        }
    }

    if (json.AtencionAdministrativa.TipoClasificacion !== null) {
        if (json.AtencionAdministrativa.TipoClasificacion.Id != null) {

            $("#divAntecedentesPaciente").show();
            $("#divDescripcionAlergias, #divAntecedentesMorbidos, #divAntecedentesQx, #divAntecedentesGinecoObstetrico").hide();
            $("#spnClasificacion").text(json.AtencionAdministrativa.TipoClasificacion.Valor);

        } else
            $("#divAntecedentesPacienteVacio").show();
    }
}

async function cargarProfesional() {

    const profesional = await getProfesionalPorId(nSession.ID_PROFESIONAL)
    await cargarDatosProfesionalMedico(profesional)
    await cargarProfesionalEvolucionAtencion(profesional)
}

async function cargarDatosProfesionalMedico(profesional) {
    // Asignar el número de documento al elemento span correspondiente
    let numeroDocumento = document.getElementById("txtNumeroDocumentoProfesional");
    numeroDocumento.textContent = `${profesional.NumeroDocumento}${profesional.Digito !== null ? `-${profesional.Digito}` : ""}`;

    // Asignar el nombre al elemento span correspondiente
    let nombre = document.getElementById("txtNombreProfesional");
    nombre.textContent = profesional.Nombre;

    // Asignar el primer apellido al elemento span correspondiente
    let apePat = document.getElementById("txtApePatProfesional");
    apePat.textContent = profesional.ApellidoPaterno;

    // Asignar el segundo apellido al elemento span correspondiente
    let apeMat = document.getElementById("txtApeMatProfesional");
    apeMat.textContent = profesional.ApellidoMaterno;
}

async function cargarProfesionalEvolucionAtencion(profesional) {
    $("#txtProfesionalEvolucionAtencion").val(`${profesional.Nombre} ${profesional.ApellidoPaterno} ${profesional.ApellidoMaterno ?? ""}`)
}

//EVOLUCIONES
// Función para obtener y mostrar el historial de evoluciones en la vista principal
async function mostrarHistorialEvoluciones(idAtencionUrgencia) {
    try {
        const historialEvoluciones = await getAltasUrg(idAtencionUrgencia);
        if (historialEvoluciones) {
            const evoluciones = prepararDatosHistorialEvoluciones(historialEvoluciones);
            dibujarTablaHistorialEvolucionesUrg(evoluciones);
        }
    } catch (error) {
        console.error('Error al obtener el historial de evoluciones:', error);
        throw error;
    }
}

async function cargarPaciente(id) {

    const paciente = await getPacientePorId(id);

    const admisor = await GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA)
    //const lugar = await prepararMedicamentosPendientes()

    let nombreCompletoPac = paciente?.NombreSocial !== null ?
        `(${paciente.NombreSocial}) ${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}` :
        `${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}`;

    // Cambiar el valor del Rut a un span
    let rut = document.getElementById("txtRutPacienteUrg");
    rut.textContent = `${paciente.NumeroDocumento}${paciente.Digito !== null ? `-${paciente.Digito}` : ""}`;

    // Asignar el nombre completo al elemento span correspondiente
    let nombre = document.getElementById("txtNombrePacienteUrg");
    nombre.textContent = nombreCompletoPac;

    // Asignar el sexo al elemento span correspondiente
    let sexo = document.getElementById("txtSexoUrg");
    sexo.textContent = paciente.Sexo.Valor;

    // Asignar el género al elemento span correspondiente
    let genero = document.getElementById("txtGeneroUrg");
    genero.textContent = paciente.Genero.Valor;

    // Asignar fecha de nacimiento
    let fechaNac = moment(paciente.FechaNacimiento).format("DD-MM-YYYY");
    document.getElementById("txtFechaNacUrg").textContent = fechaNac;

    // Asignar la edad al elemento span correspondiente
    let edad = document.getElementById("txtEdadUrg");
    edad.textContent = paciente.Edad?.EdadCompleta;

    // Obtener el elemento span correspondiente a la PREVISION
    let previsionCompletaElement = document.getElementById("txtPrevisionUrg");

    // Concatenar los valores de Prevision y PrevisionTramo
    let previsionCompleta = paciente.Prevision?.Valor + " " + paciente.PrevisionTramo?.Valor;

    // Asignar el valor concatenado al elemento span
    previsionCompletaElement.textContent = previsionCompleta;

    // Obtener el elemento span correspondiente a la FECHA
    let horaIngresoElement = document.getElementById("txtHoraIngreso");

    let horaIngreso = moment(paciente.FechaActualizacion).format("HH:mm");
    let fechaIngreso = moment(paciente.FechaActualizacion).format("DD-MM-YYYY");

    // Concatenar la hora y la fecha
    let ingresoCompleto = horaIngreso + " | " + fechaIngreso;

    // Asignar el valor concatenado al elemento span
    horaIngresoElement.textContent = ingresoCompleto;

    // Asignar Admisor
    let admisorUrgencia = document.getElementById("txtAdmisorUrg");
    admisorUrgencia.textContent = admisor.Admisor.Valor;

    // Asignar Acompañante
    let acompanante = document.getElementById("txtAcompanante");

    // Verificar si Acompañante es null
    if (admisor.Acompañante === null) {
        acompanante.textContent = "Sin información de acompañante";
    } else {
        // Obtener el valor del acompañante
        let idPersona = admisor.Acompañante.IdPersona;
        let tipoRepresentanteValor = admisor.Acompañante.TipoRepresentante.Valor;

        // Obtener el nombre del acompañante por ID
        obtenerNombrePorIdPersona(idPersona).then(nombre => {
            if (nombre) {
                // Concatenar el nombre con el tipo de representante
                let acompananteValor = `${nombre} (${tipoRepresentanteValor})`;
                // Asignar el valor del acompañante al elemento span
                acompanante.textContent = acompananteValor;
            } else {
                acompanante.textContent = "Error al obtener información del acompañante";
            }
        }).catch(error => {
            console.error('Error al obtener el nombre del acompañante:', error);
            acompanante.textContent = "Error al obtener información del acompañante";
        });
    }

    // Asignar box
    let lugar = document.getElementById("txtLugarPaciente");

    // Verificar si Ubicacion es null
    if (admisor.Ubicacion === null) {
        lugar.textContent = "No tiene box asignado";
    } else {
        // Obtener los valores de TipoBox y Box
        let tipoBox = admisor.Ubicacion.TipoBox.Valor;
        let box = admisor.Ubicacion.Box.Valor;
        // Concatenar los valores
        let lugarBox = box + " - " + tipoBox;
        // Asignar el valor concatenado al elemento span
        lugar.textContent = lugarBox;
    }

    // Devuelve el nombre completo del paciente
    return nombreCompletoPac;
}

async function obtenerNombrePorIdPersona(idPersona) {
    try {
        const usuario = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Personas/${idPersona}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        // Obtener el nombre completo del usuario
        const nombreCompleto = `${usuario.Nombre} ${usuario.ApellidoPaterno} ${usuario.ApellidoMaterno}`;
        return nombreCompleto;
    } catch (error) {
        throw error;
    }
}

//async function cargarInformacionAdicionalUrg({ idAtencion, motivoConsulta, tipoAtencion }) {

//    //MOTIVO
//    $("#txtMotivoConsultaUrg").val(motivoConsulta ?? "")

//    // ANTECEDENTES 
//    const antecedentes = await getAntecendentesUrg(idAtencion)

//    if (antecedentes.length === 0)
//        return

//    const atencedentesOrdenados = antecedentesOrderByIdDescending(antecedentes)

//    let html = ""
//    const LIMITE_MATRONERIA = 5
//    const LIMITE_MEDICO = 4

//    atencedentesOrdenados.map((a, index) => {

//        let count = index + 1

//        let limiteAntecedentes = tipoAtencion.Id === 2 //Gineco-Obstetrica
//            ? LIMITE_MATRONERIA
//            : LIMITE_MEDICO

//        if (count < limiteAntecedentes) {
//            $("#divAntecedentesMorbidosUrg").empty()

//            html += `
//        <div class="col-md-6">
//            <label>${a.ClasificacionAntecedente.Valor}</label>
//            <textarea class="form-control" rows="2" maxlength="100" disabled="disabled">${a.UrgenciaClasificacionAntecedente.Valor}</textarea>
//        </div>
//        `
//        }
//    })

//    $("#divAntecedentesMorbidosUrg").append(html)
//}

//function antecedentesOrderByIdDescending(antecedentes) {

//    if (antecedentes.length <= 0)
//        return
//    const antecedentesOrdenados = antecedentes.sort((a, b) => b.Id - a.Id);

//    return antecedentesOrdenados
//}

async function cargarCategorizacion(categorizacion) {
    if (!categorizacion || categorizacion.length === 0 || categorizacion[0] === undefined) {
        $("#divCategorizacionVacio").show();
        $("#divCategorizacion").hide();
        $("#divCategorizacionInfo").hide();
        return;
    }
    let ultimoElemento = categorizacion.length - 1
    const categorizacionCodigo = categorizacion[ultimoElemento].Codigo;
    const fechaHora = categorizacion[ultimoElemento].FechaHora ? moment(categorizacion[ultimoElemento].FechaHora).format("HH:mm | DD-MM-YYYY") : "Sin información";
    const usuarioCategorizador = categorizacion[ultimoElemento].Usuario && categorizacion[ultimoElemento].Usuario.Valor ? categorizacion[ultimoElemento].Usuario.Valor : "Sin información";
    const observaciones = categorizacion[ultimoElemento].Observaciones;

    let colorDivClass = '';
    let textColorClass = '';

    // Asignando clases de color según la categorización con un switch
    switch (categorizacionCodigo) {
        case 'C1':
            colorDivClass = 'bg-danger';
            textColorClass = 'text-white';
            break;
        case 'C2':
            colorDivClass = 'bg-orange';
            textColorClass = 'text-white';
            break;
        case 'C3':
            colorDivClass = 'bg-warning';
            textColorClass = 'text-white';
            break;
        case 'C4':
            colorDivClass = 'bg-success';
            textColorClass = 'text-white';
            break;
        case 'C5':
            colorDivClass = 'bg-info';
            textColorClass = 'text-white';
            break;
        default:
            colorDivClass = 'bg-secondary';
            break;
    }

    const divHTML = `<div class='p-1 ${colorDivClass} rounded'>
                        <h1 class='text-center m-0 ${textColorClass}'><strong>${categorizacionCodigo}</strong></h1>
                    </div>`;
    $("#divCategorizacion").append(divHTML);
    $("#divCategorizacionVacio").hide();

    // Asignar los valores a los elementos span
    $("#txtFechaHoraCategorizacion").text(fechaHora);
    $("#txtUsuarioCategorizador").text(usuarioCategorizador);

    if (observaciones) {
        $("#txtObsCategorizacion").text(observaciones);
        $("#divObsCategorizacion").show();
    } else {
        $("#divObsCategorizacion").hide();
    }

    $("#divCategorizacionInfo").show();
}

async function modalHistorialSignosVitales(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);

    try {
        // Mostrar los datos del paciente
        if (objAtencionUrgencia && objAtencionUrgencia.idPaciente) {
            await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divHistorialSignosVitales");
        }

        if (showModal) {
            buscarCrearTablaHistorialSignosVitales();
            $("#mdlHistorialSignosVitales").modal("show");
        }
        //} else {
        //    Swal.fire({
        //        icon: 'info',
        //        title: 'No hay signos vitales',
        //        text: 'No se encontraron signos vitales asociados al paciente.'
        //    });
        //}
    } catch (error) {
        console.error("Error en modalHistorialSignosVitales:", error);
    }

    ShowModalCargando(false);

}

async function buscarCrearTablaHistorialSignosVitales() {

    try {
        let historialSignosVitales = await getSignosVitalesAsync(IdDauAtencionClinica)

        // Ordenar los signos vitales por fecha de manera descendente
        historialSignosVitales.sort((a, b) => {

            let dateA = new Date(a.FechaHora);
            let dateB = new Date(b.FechaHora);
            return dateB - dateA;
        });

        // Extraer todos los posibles nombres de medidas de signos vitales y sus iconos
        let tiposMedida = [];
        let iconosMedida = {};
        historialSignosVitales.forEach(registro => {
            registro.Signos.forEach(signo => {
                let nombreMedida = signo.TipoMedida.Valor.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(".", "");
                if (!tiposMedida.includes(nombreMedida)) {
                    tiposMedida.push(nombreMedida);
                    iconosMedida[nombreMedida] = signo.Icono;
                }
            });
        });

        // Agregar PAM a tiposMedida y asignar icono correspondiente
        tiposMedida.push("PAM");
        iconosMedida["PAM"] = "fa fa-heart";

        let datosDataTable = historialSignosVitales.map((registro) => {

            let uuid = convertFechaAID(registro.FechaHora)
            let fila = {
                Id: uuid,
                Fecha: moment(registro.FechaHora).format("DD-MM-YYYY HH:mm:ss"),
                FechaISO: new Date(registro.FechaHora).toISOString(), // Añadiendo una columna ISO para ordenar
                "F Cardiaca": null,
                "P Arterial": null,
                "T° Axilar": null,
                "F respiratoria": null,
                Saturacion: null,
                EVA: null,
                "Escala de Glasgow": null,
                "T° rectal": null,
                "Hemoglucotest": null,
                Peso: null,
                LCF: null,
                O2: null,
                PAM: null
            };

            // Buscar el valor de la presian arterial en el registro
            let presionArterial = registro.Signos.find(signo => signo.TipoMedida.Valor === "P. Arterial");
            if (presionArterial) {
                // Obtener PS y PD desde el valor de la presión arterial
                let valorPA = presionArterial.Valor;
                let valores = valorPA.split('/');
                let ps = parseInt(valores[0].trim());
                let pd = parseInt(valores[1].trim());

                // Calcular PAM si se obtuvieron valores validos de PS y PD
                if (!isNaN(ps) && !isNaN(pd)) {
                    let pam = (ps + 2 * pd) / 3;
                    fila["PAM"] = pam.toFixed(1); // cantidad de decimales
                } else {
                    fila["PAM"] = null;
                }
            } else {
                fila["PAM"] = null;
            }

            // Asignar valores de signos vitales a la fila
            registro.Signos.forEach(signo => {
                let nombreMedida = signo.TipoMedida.Valor.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(".", "");
                fila[nombreMedida] = signo.Valor !== undefined ? signo.Valor : null;
            });

            // Rellenar con null si algún signo vital no está presente en esta fila
            tiposMedida.forEach(tipo => {
                if (!(tipo in fila)) {
                    fila[tipo] = null;
                }
            });

            return fila;
        });

        const columnasDataTable = [
            {
                "title": "ID",
                "data": "Id",
                "className": "fecha-columna",
                "width": "auto",
                "orderable": false,
                "bSortable": false,
                "sTitle": "ID",
                "mData": "Id",
                "sWidth": "auto",
                "sClass": "fecha-columna"
            },
            {
                "title": "Fecha",
                "data": "Fecha",
                "className": "fecha-columna",
                "width": "auto",
                "orderable": false,
                "bSortable": false,
                "sTitle": "Fecha",
                "mData": "Fecha",
                "sWidth": "auto",
                "sClass": "fecha-columna"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-file-medical-alt\"></i><span>F Cardiaca</span></div>",
                "data": "F Cardiaca",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-file-medical-alt\"></i><span>F Cardiaca</span></div>",
                "mData": "F Cardiaca"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-heartbeat\"></i><span>P Arterial</span></div>",
                "data": "P Arterial",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-heartbeat\"></i><span>P Arterial</span></div>",
                "mData": "P Arterial"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-chart-line\"></i><span>PAM</span></div>",
                "data": "PAM",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-chart-line\"></i><span>PAM</span></div>",
                "mData": "PAM"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-temperature-high\"></i><span>T° Axilar</span></div>",
                "data": "T° Axilar",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-temperature-high\"></i><span>T° Axilar</span></div>",
                "mData": "T° Axilar"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-stethoscope\"></i><span>F respiratoria</span></div>",
                "data": "F respiratoria",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-stethoscope\"></i><span>F respiratoria</span></div>",
                "mData": "F respiratoria"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-tint\"></i><span>Saturacion</span></div>",
                "data": "Saturacion",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-tint\"></i><span>Saturacion</span></div>",
                "mData": "Saturacion"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-smile\"></i><span>EVA</span></div>",
                "data": "EVA",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-smile\"></i><span>EVA</span></div>",
                "mData": "EVA"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-brain\"></i><span>Escala de Glasgow</span></div>",
                "data": "Escala de Glasgow",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-brain\"></i><span>Escala de Glasgow</span></div>",
                "mData": "Escala de Glasgow"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-thermometer\"></i><span>T° rectal</span></div>",
                "data": "T° rectal",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-thermometer\"></i><span>T° rectal</span></div>",
                "mData": "T° rectal"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-flask\"></i><span>Hemoglucotest</span></div>",
                "data": "Hemoglucotest",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-flask\"></i><span>Hemoglucotest</span></div>",
                "mData": "Hemoglucotest"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-weight\"></i><span>Peso</span></div>",
                "data": "Peso",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-weight\"></i><span>Peso</span></div>",
                "mData": "Peso"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-heartbeat\"></i><span>LCF</span></div>",
                "data": "LCF",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-heartbeat\"></i><span>LCF</span></div>",
                "mData": "LCF"
            },
            {
                "title": "<div class=\"icon-title\"><i class=\"fas fa-wind\"></i><span>O2</span></div>",
                "data": "O2",
                "sTitle": "<div class=\"icon-title\"><i class=\"fas fa-wind\"></i><span>O2</span></div>",
                "mData": "O2"
            },
        ]

        cargarColumnaEditarSignos(columnasDataTable)

        // CSS para ajustar el ancho de las columnas
        agregarEstilos()

        destruirTablaSiExiste('#tblHistorialSignosVitales')

        $("#tblHistorialSignosVitales").DataTable({
            data: datosDataTable,
            destroy: true,
            responsive: false,
            searching: false,
            order: [[1, 'desc']], // Ordenar por la columna Fecha
            columns: columnasDataTable,
            columnDefs: [{ targets: 0, visible: false }],
            createdRow: function (row, data, index) {
                $('td', row).css('text-align', 'center');
            },
            initComplete: function () {
                $('#tblHistorialSignosVitales thead th').css({ "text-align": "center", "padding": "8px" });
            }
        });

        // Ajustar columnas
        $('#tblHistorialSignosVitales').DataTable().columns.adjust().draw();

    } catch (error) {
        console.error("Error en buscarCrearTablaHistorialSignosVitales:", error);
    }
}

function cargarColumnaEditarSignos(columnas) {
    columnas.push({
        data: null,
        orderable: false,
        render: function (data, type, row) {

            return `<button 
                        type="button"
                        class="btn btn-warning btn-sm eliminar-btn"
                        data-id="${row.Id}" 
                        data-fecha="${row.Fecha}" 
                        onclick="CargarModalSignosVitales(this);">
                            <i class="fa fa-pencil-alt mr-1"></i>
                   </button>`;
        },
        className: 'text-center'
    });
}

function agregarEstilos() {
    let style = document.createElement('style');
    style.innerHTML = `
        #tblHistorialSignosVitales th, #tblHistorialSignosVitales td {
            vertical-align: middle;
        }
        .icon-title {
            display: flex;
            flex-direction: column;
            align-items: center;
            white-space: nowrap;
        }
        .icon-title i {
            margin-bottom: 5px;
        }
        .fecha-columna {
            text-align: center;
        }
    `;
    document.head.appendChild(style);
}

function destruirTablaSiExiste(selector) {
    if ($.fn.DataTable.isDataTable(selector)) {
        $(selector).DataTable().clear().destroy();
    }
}

async function CargarModalSignosVitales(e) {

    const uuid = $(e).data("id")
    const fechaHora = $(e).data("fecha")

    const historialSignosVitales = await getSignosVitalesAsync(nSession.ID_INGRESO_URGENCIA)

    const objeto = {
        historialSignosVitales,
        uuid
    }

    const signos = getSignosPorIdFila(objeto)

    $("#mdlSignosVitales").addClass("zindexSignosVitales")
    $("#aGuardarSignoVital").hide()
    $("#aEditarSignoVital").show()

    MostrarSignosVitales(e);

    let inputSignos = document.querySelectorAll("#divSignosVitales input")
    let inputArray = Array.from(inputSignos)

    inputArray.forEach(input => {
        let id = parseInt(input.dataset.id);
        let signosVitales = signos.find(s => s.TipoMedida.Id === id)
        if (signosVitales) {
            input.value = signosVitales.Valor
        }
    })

    let fecha = fechaHora.split(" ")

    $('#staticBackdropLabel12 span').remove()
    $('#staticBackdropLabel12').append(`<span> del ${fecha[0]} a las ${fecha[1]}</span>`)

    $("#mdlSignosVitales").attr("data-fecha", fecha)
}

function validaInputVacios() {

    let inputSignos = document.querySelectorAll("#divSignosVitales input")
    let inputArray = Array.from(inputSignos)

    return inputArray.some(input => input.value !== "")
}


async function editarSignosVitales() {

    let inputSignos = document.querySelectorAll("#divSignosVitales input")
    let inputArray = Array.from(inputSignos)
    let dataFecha = $("#mdlSignosVitales").attr("data-fecha")

    const fecha = moment(dataFecha, 'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
    const signosVitales = []

    inputArray.forEach(input => {
        if (input.value !== "")
            signosVitales.push(
                {
                    Fecha: fecha,
                    Id: input.dataset.id,
                    Valor: input.value,
                    IdTipoModulo: 7
                })
    })

    putSignosVitales(signosVitales, fecha)
}
function putSignosVitales(json, fecha) {

    if (!validaInputVacios()) {
        toastr.error("Llene por lo menos un campo.")
        return
    }
    else if (validarCamposRegex("#divSignosVitales", false)) {
        toastr.error("Debe ingresar valores válidos");
        return
    }

    $("#mdlSignosVitales").removeAttr("data-fecha")

    ShowModalCargando(true)

    const parametrizacion = {
        type: "PUT",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Signos`,
        mensaje: `Se actuliza signos vitales fecha: ${moment(fecha).format("DD-MM-YYYY HH:mm:ss")}`
    }

    $.ajax({
        type: parametrizacion.type,
        url: parametrizacion.url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            toastr.success(parametrizacion.mensaje)
            ShowModalCargando(false)
            buscarCrearTablaHistorialSignosVitales()
            $("#mdlSignosVitales").modal("hide")
        },
        error: function (err) {
            console.log(JSON.stringify(err))
        }
    })
}


function getSignosPorIdFila({ historialSignosVitales, uuid }) {

    if (!historialSignosVitales)
        return

    for (item of historialSignosVitales) {
        const { FechaHora, Signos } = item
        if (parseInt(convertFechaAID(FechaHora)) === uuid)
            return Signos
    }
}

async function getSignosVitalesAsync(idAtencion) {

    try {
        return await promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencion}/Signos`).catch(error => {
            console.error("Error al obtener los signos vitales:", error);
            return [];
        });
    }
    catch (error) {
        consol.log(error)
    }
}

function convertFechaAID(FechaHora) {

    const fechaHora = FechaHora.split('T')
    const fecha = fechaHora[0].split('-').join('')
    const hora = fechaHora[1].split(':').join('')
    const id = fecha.concat(hora)
    const uuid = id.split('.')
    return uuid[0]
}

function toogleTablaHistorialSignos() {

    const $divDatosPaciente = document.getElementById("datosPacienteHistorialSignos")
    const $divContainerHistorial = document.getElementById("containerHistorialSignos")
    const $divTableResponsive = document.getElementById("tableResponsiveHistorialSignos")

    $divDatosPaciente.classList.toggle("visible")

    if ($divContainerHistorial.classList.contains("col-xl-8")) {
        $divContainerHistorial.classList.remove("col-xl-8")
        $divContainerHistorial.classList.add("col-xl-12")
        $divTableResponsive.classList.add("d-flex", "flex-row", "justify-content-center")
    }
    else {
        $divContainerHistorial.classList.remove("col-xl-12")
        $divContainerHistorial.classList.add("col-xl-8")
        $divTableResponsive.classList.remove("d-flex", "flex-row", "justify-content-center")
    }

    if ($divDatosPaciente.classList.contains("visible")) {
        $divDatosPaciente.style.display = "none"
    }
    else {
        $divDatosPaciente.style.display = "block"
    }
}
function cargarAtencionesUrgenciaPrevias(idPaciente, idAtencionActual) {
    if (idPaciente == null || idPaciente == undefined)
        throw new Error("No se recibio id paciente")

    promesaAjax("GET", `URG_Atenciones_Urgencia/Buscar?idPaciente=${idPaciente}&filas=500`).then(res => {
        $("#spnCantidadAtencionesUrgencia").empty()

        atencionesPreviasPaciente = res.filter(x => x.Id !== idAtencionActual)
        $("#spnCantidadAtencionesUrgencia").append(atencionesPreviasPaciente.length)

        //tblHistorialHospitalizacionesPrevias
        $("#tblHistorialHospitalizacionesPrevias").DataTable({
            data: atencionesPreviasPaciente,
            order: [[0, "desc"]],
            responsive: true,
            columns: [
                { title: "Id", data: "Id" },
                { title: "Fecha", data: "FechaAdmision", render: function (fecha) { return moment(fecha).format("DD-MM-YYYY HH:mm") } },
                { title: "Tipo Atencion", data: "NombreTipoAtencion" },
                { title: "Motivo", data: null },  // null porque combina motivoConsulta con Categorizacion.Observaciones
                { title: "Clasificación", data: "Clasificacion" },
                { title: "Imprimir", data: "Id" },
            ],
            columnDefs: [
                {
                    targets: -3,
                    render: function (data, type, row) {
                        const motivoConsulta = row.MotivoConsulta ? row.MotivoConsulta : "Sin motivo";
                        const observaciones = row.Categorizacion && row.Categorizacion.Observaciones
                            ? `<div class="rounded p-2 table-active border border-grey w-100 mt-2"><small><em>${row.Categorizacion.Observaciones}</small></em></div>`
                            : `<div class="rounded p-2 table-active border border-grey w-100 mt-2"><small><em>Sin observaciones</small></em></div>`;

                        return `${motivoConsulta} ${observaciones}`;
                    }
                },
                {
                    targets: -2,
                    render: function (dato) {
                        return dato ? dato.Valor : 'No ingresado.';
                    }
                },
                {
                    targets: -1,
                    render: function (idDauImprimir) {
                        urlimprimirdaucompleto = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idDauImprimir}/Imprimir`
                        return `<button type="button" class="btn btn-info" onclick="ImprimirApiExterno('${urlimprimirdaucompleto}')"> <i class="fa fa-print"></i></button>`
                    }
                }
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData.Id == idAtencionActual)
                    $('td', nRow).css('background-color', '#dff0d8');//verde
            }
        })
    }).catch(error => {
        console.error("ocurrio un error al listar atenciones previas")
        console.log(error)
    })
}
function cargarTablaExamenesInfinity(data) {
    //El idPeticion, fechaOrden, procedencia, tipoAtencion y observaciones
    $("#divIntegracionInfinity").removeClass("d-none")
    $("#tblIntegracionInfinity").DataTable({
        data: data,
        destroy: true,
        columns: [
            { title: "Id peticion", data: "idPeticion" },
            { title: "Fecha", data: "fechaOrden", render: function (fecha) { return moment(fecha).format("DD-MM-YYYY HH:mm") } },
            { title: "Procedencia", data: "procedencia" },
            { title: "Tipo Atencion", data: "tipoAtencion" },
            { title: "Observaciones", data: "observaciones" },
            { title: "Acciones", data: "idPeticion" },
        ], columnDefs: [
            {
                targets: -1,
                render: function (id, meta, data) {
                    return `<button type="button" class="btn btn-info" onclick='imprimirModalExamenesIntegracion(${JSON.stringify(data)})'><i class="fa fa-print"></i></button>`
                }
            }
        ]
    })
}
function imprimirModalExamenesIntegracion(data) {
    ShowModalCargando(true)
    promesaAjax("GET", `RCE_Solicitud_Laboratorio/Infinity/Pdf?idPeticion=${data.idPeticion}&fecha=${moment(data.fechaOrden).format("YYYY-MM-DD")}`).then(res => {
        ShowModalCargando(false)
        if (res.length == 1) {
            window.open(res[0].url, 'blank');
        } else {
            Swal.fire({
                title: "<strong>Se encontraron dos tipos de examenes en esta petición</strong>",
                icon: "info",
                html: `Seleccione que tipo de examen quiere revisar`,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: `Microbiología`,
                cancelButtonText: `General`,
            }).then(evento => {
                if (evento.value) {
                    //microbiologia
                    examenMicrobiologia = res.find(x => x.tipoOrden == "Microbiología")
                    if (examenMicrobiologia !== null && examenMicrobiologia !== undefined)
                        window.open(examenMicrobiologia.url, 'blank');
                } else {
                    //general
                    examenGeneral = res.find(x => x.tipoOrden == "General")
                    if (examenGeneral !== null && examenGeneral !== undefined)
                        window.open(examenGeneral.url, 'blank');
                }
            })
        }
    }).catch(error => {
        ShowModalCargando(false)
        toastr.error("Ocurrio un error con la integración INFINITY")
        console.error("Ocurrio un error al obtener las peticiones infinity")
        console.log(error)
        console.log(error.xhr.responseText)
    })
}
//funcion para reversar un medicamento o procedimiento
function reversarElemento(tipoElemento, elemento) {
    let titulo = `Esta a punto de revertir el estado de un `
    let texto = `Por favor confirme la acción`, metodo = `PUT`, urlendpoint = ``, idelemento = 0
    if (elemento !== null && elemento !== undefined) {
        switch (tipoElemento) {
            case `procedimiento`:
                titulo += `procedimiento`
                urlendpoint = `URG_Atenciones_urgencia/${elemento.IdProcedimiento}/procedimiento/Reversar`
                break;
            case `medicamento`:
                titulo += `medicamento`
                urlendpoint = `URG_Atenciones_urgencia/${elemento.IdAtencionUrgenciaMedicamento}/medicamento/Reversar`
                break;
        }
        Swal.fire({
            title: titulo,
            icon: "warning",
            html: texto,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: `Reversar`,
            cancelButtonText: `Cancelar`,
        }).then(evento => {
            if (evento.value) {
                promesaAjax("Patch", urlendpoint).then(res => {
                    Swal.fire({
                        title: `El ${tipoElemento} ha sido revertido`,
                        icon: "info",
                    })
                    if (tipoElemento == `procedimiento`)
                        actualizarTablaProcedimientosAtencionClinica()
                    else
                        actualizarTablaMedicamentosUrgencia()

                }).catch(error => {
                    console.error("Error al reversar el procedimiento")
                    console.log(error)
                    console.log(error.xhr.responseText)
                })
            }
        })
    }

}

async function validarDauPorMedico() {
    //mostrar sweetalert de adverencia
    let DauById = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);
    if (DauById.ValidadoPorMedico) {
        Swal.fire({
            title: 'Atención de urgencia está validada',
            icon: "info",
            html: 'Esta atención de urgencia ya ha sido validada.',
        });
        return;
    }
    const { value: observacion, dismiss } = await Swal.fire({
        title: "Validando atención de urgencia",
        input: "text",
        icon: "warning",
        confirmButtonText: `
        <i class="fa fa-thumbs-up"></i> Validar`,
        cancelButtonText: `Cancelar`,
        inputLabel: "Ingrese su observación",
        inputPlaceholder: "Ingrese observaciones.",
        html: `Usted está validando la atención de urgencia y las <b>acciones de un interno de medicina.</b> </br>
        Revise con detalle las acciones del interno e ingrese observaciones si es necesario`,
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value) {
                return "Este campo es requerido.";
            }
        }
    })
    if (dismiss !== "cancel") {
        console.log("realiza acciones")
        $.ajax({
            method: 'POST',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Validacion`,
            data: { Observaciones: observacion },
            success: function (data) {
                $("#aValidarDau").addClass("d-none")
                Swal.fire({
                    icon: "info",
                    title: 'Validación exitosa',
                    html: 'La atención ha sido validada.',
                });
            }, error: function (error) {
                console.error("Error al validar la atencion de urgencia")
                console.log(error)
            }
        })
    }
   
}

function cargarValidacionesMedicas(idAtencionUrgencia) {
    promesaAjax("GET", `URG_Atenciones_Urgencia/${idAtencionUrgencia}/Validacion`).then(res => {
        console.log("Validaciones de urgencia", res)
        if (res.length > 0 && res !== undefined) {
            $("#divValidacionesMedicas").removeClass("d-none")
            $("#tblValidacionesMedicas").DataTable({
                columns: [
                    {title:"Observaciones", data:"Observaciones"},
                    { title: "Fecha", data: "Fecha", render: function (fecha) {return moment(fecha).format("DD-MM-YYYY HH:mm") } },
                    { title: "Profesional", data: "Profesional", render: function (prof) { return prof.Valor } }
                ],
                data: res,
                destroy:true
            })
        }
    }).catch(error => {
        console.error("Ha ocurrido un error al obtener la validaciones de urgencia")
        console.log(error)
    })
}