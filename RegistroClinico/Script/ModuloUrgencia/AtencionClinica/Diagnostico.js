﻿async function modalDiagnosticoUrgencia() {

    if (nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 25 && nSession.CODIGO_PERFIL !== 30 && nSession.CODIGO_PERFIL !== 12) {

        Swal.fire({
            position: 'center',
            icon: 'warning',
            title: "No tiene acceso a los diagnosticos",
            showConfirmButton: true,
        })

        return;
    }

    //Esta funcion se llama para limpiar cuando se realizan busquedas de diagnostico por codigos
    cancelarSeleccionDiagnosticoPorAlias()
    $("#btnGuardarDiagnostico").unbind().on('click', function () {
        guardarDiagnosticos();
    });

    InicializarDatosDiagnostico("Content_wuc_diagnosticoAtencionUrgencia")

    mostrarUsuarioLogueadoEnModales("#mdlDiagnosticoUrgencia"); // Llamar la función para añadir el usuario

    $("#mdlDiagnosticoUrgencia").modal("show")
    buscarCrearTablaDiagnosticos();

}

async function buscarCrearTablaDiagnosticos() {
    //const perfilesPermitidos = [1, 3, 12, 21, 22, 25, 30];
    const perfilesPermitidosDiagnostico = [
        perfilAccesoSistema.medico,
        perfilAccesoSistema.apoyoClinico,
        perfilAccesoSistema.internoMedicina,
        perfilAccesoSistema.tensUrgencia,
        perfilAccesoSistema.enfermeraUrgencia,
        perfilAccesoSistema.matroneriaUrgencia,
        perfilAccesoSistema.administradorDeUrgencia
    ];


    if (!perfilesPermitidosDiagnostico.includes(nSession.CODIGO_PERFIL)) {
        console.info(`El usuario con perfil ${nSession.CODIGO_PERFIL} no tiene acceso a los diagnosticos`)
        return;
    }

    let diagnosticosUrgencia;
    try {
        diagnosticosUrgencia = await promesaAjax("GET", `URG_Atenciones_Urgencia/${IdDauAtencionClinica}/Diagnosticos`);
        diagnosticosUrgencia.forEach((diagnostico) => {
            agregarFilaDiagnostico({...diagnostico},
                () => eliminarDiagnosticoAtencionUrgencia(diagnostico.Id));
        });
    } catch ({ xhr: error }) {

        if (error.status === 404) {
            diagnosticosUrgencia = [];
            console.log('No hay diagnósticos', error);
        } else {
            console.error('Error obteniendo datos de diagnósticos:', error);
        }


    }

    // Dibujar la tabla con los datos obtenidos
    $("#tblDiagnosticosUrgencia").DataTable({
        language: {
            "emptyTable": "No hay diagnósticos disponibles para mostrar",
        },
        data: diagnosticosUrgencia,
        destroy: true,
        columns: [
            { title: "Diagnóstico", data: "Diagnostico", orderable: false },
            { title: "Clasificación", data: "Clasificacion", orderable: false },
            { title: "Profesional", data: "Profesional", orderable: false },
            { title: "Detalle", data: "DetalleDiagnostico", orderable: false },
            { title: "Acciones", data: "Id", orderable: false },
        ],
        columnDefs: [
            {
                targets: [0],
                render: function (elemento, row, data) {
                    let stringretorno = `
                    ${data.Codigo !== null ? data.Codigo : ""} -  ${data.Alias !== null ? data.Alias.Valor : ""}
                    `
                    return stringretorno;
                }
            },
            {
                targets: [1, 2],
                render: function (elemento) {
                    if (elemento.Valor !== null) {
                        return elemento.Valor;
                    }
                }
            },
            {
                targets: [3],
                render: function (detalleDiagnostico) {
                    return detalleDiagnostico == null ? `Sin detalle` : detalleDiagnostico;
                }
            },
            {
                targets: [-1],
                render: function (idDetalleDiagnostico) {
                    return `<div class="d-flex flex-row justify-content-center flex-nowrap">
                                <button
                                    type="button"
                                    class="btn btn-danger"
                                    onclick="eliminarDiagnosticoAtencionUrgencia(${idDetalleDiagnostico})"
                                    ${nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 25 && nSession.CODIGO_PERFIL !== 30 ? "disabled" : ""}> 
                                        <i class="fa fa-trash fa-xs"></i> 
                                </button>
                            </div>`;
                }
            }
        ]
    });
}

async function guardarDiagnosticos() {
    const diagnosticosAGuardar = getDiagnosticosRealizados();
    if (existenDiagnosticosAgregados()) {

        if (diagnosticosAGuardar.length === 0) {
            $("#mdlDiagnosticoUrgencia").modal('hide');
            return;
        }

        let jsonDiagnostico = {
            IdAtencionUrgencia: IdDauAtencionClinica,
            Diagnosticos: diagnosticosAGuardar
        };

        // Deshabilitar el botón
        $("#btnGuardarDiagnostico").prop('disabled', true);

        $.ajax({
            method: "POST",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${IdDauAtencionClinica}/Diagnosticos`,
            data: jsonDiagnostico,
            success: function (data) {
                limpiarDiagnosticos();
                $("#txtDiagnosticoDetalleMedico").val("");
                $("#sltClafisifacionCie10").val(0);

                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Diagnóstico guardado",
                    showConfirmButton: false,
                    timer: 3000
                });

                buscarCrearTablaDiagnosticos();
                actualizarHistorialAltas(IdDauAtencionClinica);
                $("#mdlDiagnosticoUrgencia").modal("hide");

                // Habilitar el botón después de la solicitud exitosa
                $("#btnGuardarDiagnostico").prop('disabled', false);
            },
            error: function (error) {
                console.log(error);
                console.error("Error al intentar guardar el diagnóstico");

                // Habilitar el botón en caso de error
                $("#btnGuardarDiagnostico").prop('disabled', false);
            }
        });
    } else {
        throw new Error("No ha seleccionado exámenes");
    }
}

function eliminarDiagnosticoAtencionUrgencia(idDetalleDiagnostico) {

    promesaAjax("delete", `URG_Atenciones_Urgencia/${idDetalleDiagnostico}/Diagnosticos`).then(res => {
        Swal.fire({
            title: "Eliminado",
            text: "El diagnostico CIE10 ha sido eliminado.",
            icon: "success"
        });
        buscarCrearTablaDiagnosticos()
    }).catch(error => {
        console.error("ha ocurrido un erro al intentar ")
        console.log(error)
    })
}

