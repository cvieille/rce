﻿let ObstetricoUrgencia = [];
$(document).ready(async function () {
    nSession = getSession();

    $('#divHistorialGineco').hide(); // Ocultar al inicio
    ObstetricoUrgencia = await getObstetrico(nSession.ID_INGRESO_URGENCIA);
    dibujarTablaObstetrico(ObstetricoUrgencia);

    $('#mdlObstetrico').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divInfoObstetrico");
    });
});

async function modalGinecoObstetrico(showModal, btnText = false) {
    // 1 = Medico
    // 25 Matrona urgencia
    // 30 Administrador de urgencia
    if (nSession.CODIGO_PERFIL !== 25 && nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 30) {
        Swal.fire({
            position: 'center',
            icon: 'warning',
            title: `No tiene acceso a Gineco-Obst&eacute;trico`,
            showConfirmButton: true,
        });
        return;
    }
    ShowModalCargando(true);

    if (showModal) {
        $('#mdlObstetrico').modal('show');
    }

    // Cambiar texto del botón
    const $btnGuardarObstetrico = $('#btnGuardarObstetrico');
    if (btnText) {
        $btnGuardarObstetrico.text('Guardar');
    } else {
        $btnGuardarObstetrico.text('Actualizar');
    }

    // Obtener el último registro de obstetrico
    const idAtencionUrgencia = nSession.ID_INGRESO_URGENCIA;
    const ultimoRegistro = await getObstetrico(idAtencionUrgencia);
    // Si no hay un registro existente, dejar los campos vacíos
    if (!ultimoRegistro || ultimoRegistro.length === 0) {
        $('#txtUltimaRegla').val('');
        $('#txtLatidosCardioFetalesObstetrico').val('');
        $('#sltMotivoEmergenciaObstetrica').val('0');
    } else {
        const registro = ultimoRegistro[0];
        // Mostrar los datos del último registro en el formulario
        $('#txtUltimaRegla').val(registro.UltimaRegla);
        $('#txtLatidosCardioFetalesObstetrico').val(registro.LatidosCardioFetales);
        $('#txtAlturaUterina').val(registro.AlturaUterina);
        $('#sltMotivoEmergenciaObstetrica').val(registro.TipoMotivoEmergenciaObstetrica.Id);
        if (registro.FlujoGenital)
            document.getElementById('checkFlujoGenital').checked = true

        $('#txtActividadUterina').val(registro.ActividadUterina);
        $('#txtTactoVaginal').val(registro.TactoVaginal);
        $('#txtEspeculoscopia').val(registro.Especuloscopia);
        $('#txtMonitoreoBasal').val(registro.MonitoreoBasal);
        $('#txtEcografia').val(registro.Ecografia);

    }

    ShowModalCargando(false);
}


async function guardarObstetrico() {
    if (validarCampos("#divInfoObstetrico")) {
        // Preparando el objeto de obstetrica
        const obstetricoData = {
            UltimaRegla: $('#txtUltimaRegla').val(),
            LatidosCardioFetales: $('#txtLatidosCardioFetalesObstetrico').val(),
            IdTipoMotivoEmergenciaObstetrica: $('#sltMotivoEmergenciaObstetrica').val(),
            AlturaUterina: $('#txtAlturaUterina').val(),
            FlujoGenital: document.getElementById('checkFlujoGenital').checked,
            ActividadUterina: document.getElementById('txtActividadUterina').value,
            TactoVaginal: document.getElementById('txtTactoVaginal').value,
            Especuloscopia: document.getElementById('txtEspeculoscopia').value,
            MonitoreoBasal: document.getElementById('txtMonitoreoBasal').value,
            Ecografia: document.getElementById('txtEcografia').value,
        }
        // Deshabilitar el botón
        $("#btnGuardarObstetrico").prop('disabled', true);
        // Espera 1 segundo antes de proceder
        //await new Promise(resolve => setTimeout(resolve, 1000));

        const idAtencionUrgencia = nSession.ID_INGRESO_URGENCIA;
        const ultimoRegistro = await getObstetrico(idAtencionUrgencia);

        if (!ultimoRegistro || ultimoRegistro.length === 0) {
            // Si no hay un registro existente, realizar solicitud POST
            await postObstetrico(obstetricoData);
            // Habilitar el botón después de la solicitud exitosa
            $("#btnGuardarObstetrico").prop('disabled', false);
        } else {
            const registro = ultimoRegistro[0];
            await putObstetrico(registro.Id, obstetricoData);
            $("#btnGuardarObstetrico").prop('disabled', false);
        }

        // Actualizar la tabla de obstetrico
        await actualizarObstetricos(idAtencionUrgencia);
    }
}

// POST GINCEO-OBSTETRICO
async function postObstetrico(obstetricoData) {
    if (obstetricoData === undefined)
        return;
    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Obstetrica`
    }
    try {
        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(obstetricoData),
            contentType: 'application/json'
        });
        toastr.success("Gineco-Obstetrico guardada");
        $("#mdlObstetrico").modal("hide");
    } catch (error) {
        console.error('Error guardando Gineco Obstetrico:', error);
        throw error;
    }
}

// PUT
async function putObstetrico(idAtencionUrgencia, obstetricoData) {
    if (obstetricoData === undefined || idAtencionUrgencia === undefined)
        return;
    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Obstetrica`;
    try {
        await $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify(obstetricoData),
            contentType: 'application/json'
        });
        toastr.success("Gineco-Obstetrico actualizado");
        $("#mdlObstetrico").modal("hide");
    } catch (error) {
        console.error('Error actualizando Gineco Obstetrico:', error);
        throw error;
    }
}

// GET GINECO-OBSTETRICO
async function getObstetrico(idAtencionUrgencia) {
    if (idAtencionUrgencia === null)
        return [];
    try {
        const obstetricoData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Obstetrica`,
            contentType: 'application/json',
            dataType: 'json',
        });

        return Array.isArray(obstetricoData) ? obstetricoData : (obstetricoData ? [obstetricoData] : []);

    } catch (error) {
        if (error.status === 404) {
            return [];
        }
        console.error('Error obteniendo datos de Gineco Obstetrico:', error);
        throw error;
    }
}

async function dibujarTablaObstetrico(obstetricos) {

    const table = $('#tblGinecoObstetrico').DataTable({
        destroy: true,
        data: Array.isArray(obstetricos) ? obstetricos : (obstetricos ? [obstetricos] : []),
        columns: [
            { data: "Id" },
            { data: "FechaHora" },
            { data: "TipoMotivoEmergenciaObstetrica.Valor" },
            { data: "LatidosCardioFetales" },
            { data: "UltimaRegla" },
            { data: "Id" } // Columna para los botones de acción
        ],
        columnDefs: [
            {
                targets: 1,
                render: function (data, type, row) {
                    return moment(data).format('DD-MM-YYYY HH:mm:ss');
                }
            },
            {
                targets: -1,
                render: function (data) {
                    return botonesAccionGineco(data);
                }
            },
        ],
        order: [[0, 'desc']]
    });
    // Mostrar/ocultar el div basado en la presencia de datos
    if (obstetricos && obstetricos.length > 0) {
        $('#divHistorialGineco').show();
    } else {
        $('#divHistorialGineco').hide();
    }
}

async function actualizarObstetricos(idAtencionUrgencia) {

    mostrarUsuarioLogueadoEnModales("#mdlObstetrico")

    try {
        ObstetricoUrgencia = await getObstetrico(idAtencionUrgencia);
        dibujarTablaObstetrico(ObstetricoUrgencia);
    } catch (error) {
        console.error('Error actualizando tabla de gineco-obstetrico:', error);
    }
}

// DELETE GINECO OBSTETRICO
async function deleteGinecoObstetrico(idGineco) {
    if (idGineco === null || idGineco === undefined) {
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar gineco-obstétrico?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Obstetrica/${idGineco}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Gineco-obstétrico eliminado`);

                // Eliminar la fila correspondiente a gineco eliminada
                const table = $('#tblGinecoObstetrico').DataTable();
                const rowIndex = table.row(`#gineco_${idGineco}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de gineco despues de eliminar
                await actualizarObstetricos(idGineco);

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divHistorialGineco').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar gineco-obstetrico: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// BOTONES 
function botonesAccionGineco(idGineco) {
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    const html = `
        <div class="d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button" 
                class="btn btn-warning m-1"
                onclick="modalGinecoObstetrico(true, false)"
                data-toggle="tooltip" 
                data-placement="bottom"
                title="Ver"
                style="display: inline-block;"
            >
                <i class="fas fa-edit fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteGinecoObstetrico m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteGinecoObstetrico(${idGineco})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
            >
                <i class="fa fa-trash fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}