
let ViolenciasUrgencia = []

$(document).ready(async function () {
    nSession = getSession();

    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 22 || nSession.CODIGO_PERFIL == 21 ||
        nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {
        ViolenciasUrgencia = await getViolencia(nSession.ID_INGRESO_URGENCIA);
        dibujarTablaViolencias(ViolenciasUrgencia);
    }

    $('#mdlViolencia').on('hidden.bs.modal', function () {
        limpiarCamposViolencia();
        resetCombosViolencia();
        camposViolenciaSexual();
    });

    // Variable para almacenar el estado original de los switches
    let switchesEstadoOriginal = {
        chkGestante: false,
        chkAnticoncepcion: false,
        chkProfilaxisVIH: false,
        chkProfilaxisITS: false,
        chkProfilaxisHepatitis: false,
        chkTieneLesion: false
    };



    // Evento change del campo sltTipoViolencia
    $('#sltTipoViolencia').change(function () {
        let tipoViolenciaSeleccionado = $(this).val();
        // Si el tipo de violencia seleccionado es "Violencia sexual", mostramos los campos espec�ficos
        if (tipoViolenciaSeleccionado === "2") {
            camposViolenciaSexual(true);
            // Si es violencia sexual, deshabilitar el campo sltTipoLesion y establecer el valor en 5
            //$('#sltTipoLesion').val('5').prop('disabled', true);

            $("#chkTieneLesion").bootstrapSwitch('state', false)
            $("#chkTieneLesion").bootstrapSwitch('disabled', true)

            // Restaurar los switches al estado original
            restoreSwitchesToOriginalState();
        } else {
            // En caso contrario, ocultamos los campos espec�ficos y habilitamos el campo sltTipoLesion
            camposViolenciaSexual(false);
            $("#chkTieneLesion").bootstrapSwitch('disabled', false)
        }
    }).trigger('change');

    // Funci�n para restaurar los switches al estado original
    function restoreSwitchesToOriginalState() {
        $('#chkGestante').bootstrapSwitch('state', switchesEstadoOriginal.chkGestante);
        $('#chkAnticoncepcion').bootstrapSwitch('state', switchesEstadoOriginal.chkAnticoncepcion);
        $('#chkProfilaxisVIH').bootstrapSwitch('state', switchesEstadoOriginal.chkProfilaxisVIH);
        $('#chkProfilaxisITS').bootstrapSwitch('state', switchesEstadoOriginal.chkProfilaxisITS);
        $('#chkProfilaxisHepatitis').bootstrapSwitch('state', switchesEstadoOriginal.chkProfilaxisHepatitis);
        $('#chkTieneLesion').bootstrapSwitch('state', switchesEstadoOriginal.chkTieneLesion);
    }

    $('#chkTieneLesion').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state)
            $("#divLesiones").slideDown()
        else {
            $("#divLesiones").slideUp()
            $("#sltTipoLesion").selectpicker("val", [])
        }
    });

});

function camposViolenciaSexual(visible) {
    if (visible) {
        $('#camposViolenciaSexual').show();
    } else {
        $('#camposViolenciaSexual').hide();
        // Restablecer el valor del campo sltTipoLesion a 0
        $('#sltTipoLesion').val('0');
    }
}
async function actualizarViolencias(idAtencionUrgencia) {
    ViolenciasUrgencia = await getViolencia(idAtencionUrgencia)
    dibujarTablaViolencias(ViolenciasUrgencia);
    $("#divListaViolencias").show();
}

async function modalViolencia(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);

    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divPacienteViolencia");

    if (showModal) {
        $('#mdlViolencia').modal('show');
        $('#divtieneLesion').show();
        // Inicializar checkbox como switch de Bootstrap
        $('#chkGestante').bootstrapSwitch('disabled', false);
        $('#chkAnticoncepcion').bootstrapSwitch('disabled', false);
        $('#chkProfilaxisVIH').bootstrapSwitch('disabled', false);
        $('#chkProfilaxisITS').bootstrapSwitch('disabled', false);
        $('#chkProfilaxisHepatitis').bootstrapSwitch('disabled', false);
        $('#chkTieneLesion').bootstrapSwitch('disabled', false);
        $('#sltTipoLesion').prop('disabled', false).selectpicker('refresh');

        $('#btnGuardarViolencia').show();
    }

    ShowModalCargando(false);
}

async function dibujarTablaViolencias(violencias) {
    if (violencias.length === 0) {
        // Ocultar la tabla si no hay violencias
        $('#divListaViolencias').hide();
        return;
    } else {
        // Mostrar la tabla si hay violencias
        $('#divListaViolencias').show();
    }

    $('#tblViolencias').DataTable({
        destroy: true,
        data: violencias,
        columns: [
            { title: "Id", data: "Id" },
            { title: "Agresor", data: "TipoAgresor" },
            { title: "Lesiones", data: "Lesiones" },
            { title: "Condicion", data: "TipoCondicionPersona" },
            { title: "Observaciones", data: "Observaciones" },
            { title: "Acciones", data: "Id" }
        ],
        columnDefs: [
            {
                targets: -1,
                render: function (data, type, row) {
                    return botonesAccionViolencia(data);
                }
            },
            {
                targets: [1, 3],
                render: function (data, type, row) {
                    return `${data.Valor}`
                }
            },
            {
                targets: [2],
                render: function (data, type, row) {
                    if (data.length > 0) {
                        let lesiones = data.map(x => `${x.Valor} `)
                        return lesiones
                    } else {
                        return `Sin lesiones constatables`
                    }
                }
            }
        ]
    });
}

async function postViolencia(idAtencionUrgencia, datosViolencia) {
    try {
        const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Violencia`;

        const response = await $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(datosViolencia)
        });

        return response;

    } catch (error) {
        console.error('Error al enviar datos de violencia:', error);
        throw error;
    }
}

async function guardarViolencia() {

    const idTipoViolencia = parseInt($("#sltTipoViolencia").val())

    if (!validarCampos("#divAgresor", false))
        return

    // Deshabilitar el btn
    $("#btnGuardarViolencia").prop('disabled', true);

    //VIOLENCIA
    if (idTipoViolencia !== 2) {

        let lesiones = $("#sltTipoLesion").selectpicker("val")
        if (lesiones.length > 0)
            lesiones = lesiones.map(x => parseInt(x))
        else {
            if ($("#chckTieneLesiones").bootstrapSwitch('state')) {
                toastr.error("Debe ingresar las lesiones constatables")
                // habilitar boton si falta algo
                $("#btnGuardarViolencia").prop('disabled', false);
                return false
            }
        }

        const violenciaData = {
            IdTipoAgresor: $('#sltTipoAgresor').val(),
            Lesiones: lesiones,
            IdTipoCondicionPersona: $('#sltTipoCondicion').val(),
            Observaciones: $('#observaciones').val()
        };

        try {
            await postViolencia(nSession.ID_INGRESO_URGENCIA, violenciaData)
            toastr.success("Violencia guardada exitosamente");
        } catch (error) {
            console.error('Error guardando violencia:', error);
            // habilitar boton si falla
            $("#btnGuardarViolencia").prop('disabled', false);
            throw error;
        }
    }
    //VIOLENCIA SEXUAL
    else {
        const violenciaSexualData = {
            IdTipoTemporalidad: $('#sltTipoTemporalidad').val(),
            Gestante: $('#chkGestante').prop('checked'),
            Anticoncepcion: $('#chkAnticoncepcion').prop('checked'),
            ProfilaxisVIH: $('#chkProfilaxisVIH').prop('checked'),
            ProfilaxisITS: $('#chkProfilaxisITS').prop('checked'),
            ProfilaxisHepatitis: $('#chkProfilaxisHepatitis').prop('checked'),
            IdTipoAtencionMedica: $('#sltTipoAtencionMedica').val(),
            IdTipoAgresor: $('#sltTipoAgresor').val(),
            IdTipoCondicionPersona: $('#sltTipoCondicion').val(),
            Observaciones: $('#observaciones').val()
        };

        try {
            await postViolenciaSexual(nSession.ID_INGRESO_URGENCIA, violenciaSexualData);
            toastr.success("Violencia sexual guardada exitosamente");
        } catch (error) {
            console.error('Error guardando violencia sexual:', error);
            throw error;
        }
    }

    await actualizarViolencias(nSession.ID_INGRESO_URGENCIA); // Actualizar la DataTable
    limpiarCamposViolencia();
    $("#mdlViolencia").modal("hide");

    // Habilitar el bot�n despu�s de la solicitud exitosa
    $("#btnGuardarViolencia").prop('disabled', false);
}

async function getViolencia(idAtencionUrgencia) {
    if (!idAtencionUrgencia) {
        return;
    }

    if (nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 21 && nSession.CODIGO_PERFIL !== 22 && nSession.CODIGO_PERFIL !== 25 &&
        nSession.CODIGO_PERFIL !== 29 && nSession.CODIGO_PERFIL !== 3 && nSession.CODIGO_PERFIL !== 30) {
        return;
    }

    mostrarUsuarioLogueadoEnModales("#mdlViolencia")

    try {
        const violenciasData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Violencia`,
            contentType: 'application/json',
            dataType: 'json',
        });

        return violenciasData;

    } catch (error) {
        console.error('Error obteniendo datos de violencias:', error);
        throw error;
    }
}

function limpiarSwitches() {
    // Desactivar y establecer los switch de Bootstrap en false
    $('#chkGestante').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    $('#chkAnticoncepcion').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    $('#chkProfilaxisVIH').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    $('#chkProfilaxisITS').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    $('#chkProfilaxisHepatitis').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    $('#chkTieneLesion').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
}

function limpiarCamposViolencia() {
    $('#sltTipoViolencia').val('0').prop('disabled', false);
    $('#sltTipoAgresor').val('0').prop('disabled', false);
    $('#sltTipoLesion').val('0').prop('disabled', false);
    $('#sltTipoCondicion').val('0').prop('disabled', false);
    $('#observaciones').val('').prop('readonly', false);
    $('#sltTipoTemporalidad').val('0').prop('disabled', false);
    $('#sltTipoAtencionMedica').val('0').prop('disabled', false);
    validarCampos("#divAgresor");
    validarCampos("#divTipoAgresor");
    limpiarSwitches();
}


function resetCombosViolencia() {
    $('#divTipoAgresor').hide();
    $('#sltTipoViolencia').val('0');
    $('#sltTipoAgresor').val('0');
    limpiarSwitches();
}

async function combosViolencia() {
    const urlViolencia = `${GetWebApiUrl()}GEN_Tipo_Violencia/Combo`;
    const urlLesion = `${GetWebApiUrl()}GEN_Tipo_Lesion/Combo`;
    const urlCondicion = `${GetWebApiUrl()}GEN_Tipo_Condicion_Persona/Combo`;
    const urlTipoTemporalidad = `${GetWebApiUrl()}URG_Tipo_Temporalidad/Combo`;
    const urlTipoAtencionMedica = `${GetWebApiUrl()}URG_Tipo_Atencion_Medica_Violencia/Combo`;

    // Cargar los datos en los selects
    await setCargarDataEnCombo(urlViolencia, false, "#sltTipoViolencia");
    await setCargarDataEnCombo(urlLesion, false, "#sltTipoLesion")
    await setCargarDataEnCombo(urlCondicion, false, "#sltTipoCondicion");
    await setCargarDataEnCombo(urlTipoTemporalidad, false, "#sltTipoTemporalidad");
    await setCargarDataEnCombo(urlTipoAtencionMedica, false, "#sltTipoAtencionMedica");

    // Guardar el valor seleccionado en el select de tipo de agresor al inicio
    let tipoAgresorInicial = $('#sltTipoAgresor').val();

    $('#divTipoAgresor').hide();

    // Agregar un evento change (anidando los select) al select de tipo de violencia
    $('#sltTipoViolencia').change(async function () {
        const tipoViolenciaSeleccionado = $(this).val();

        if (tipoViolenciaSeleccionado === "0") {
            $('#sltTipoAgresor').val(tipoAgresorInicial); // Restaurar el valor inicial
            $('#divTipoAgresor').hide();
        } else {
            // Si el tipo de violencia seleccionado no es 0, mostrar el div que contiene el select tipo de agresor
            $('#divTipoAgresor').show();

            // URL con el idTipoViolencia seleccionado
            const urlAgresor = `${GetWebApiUrl()}URG_Tipo_Agresor/Combo?idTipoViolencia=${tipoViolenciaSeleccionado}`;
            await setCargarDataEnCombo(urlAgresor, false, "#sltTipoAgresor");
        }
    });
    //eliminar opcion seleccione
    $("#sltTipoLesion option[value='0']").remove();
    //eliminar sin lesiones
    $("#sltTipoLesion option[value='5']").remove();
    $("#sltTipoLesion").selectpicker();

}

function botonesAccionViolencia(idviolencia) {
    const idViolencia = idviolencia;
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    // FALTA METODO DELETE PARA DARLE VIDA A LA FUNCION eliminarViolencia
    const html = `
        <div class="row d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button" 
                class="btn btn-info m-1" 
                onclick="verAccionViolencia(${idViolencia})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Ver"
            >
                <i class="fas fa-eye fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteViolencia m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteViolencia(${idViolencia})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
            >
                <i class="fa fa-trash fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}


// VER ACCIONES
async function verAccionViolencia(idAtencionUrgenciaViolencia) {
    try {
        violenciaSeleccionada = await promesaAjax("GET", `URG_Atenciones_Urgencia/Violencia/${idAtencionUrgenciaViolencia}`);

        $('#mdlViolencia').modal('show');
        $('#btnGuardarViolencia').hide();

        // Verificar si se encontr� la violencia correspondiente al ID
        if (!violenciaSeleccionada) {
            console.error('No se encontr� ninguna violencia con el ID proporcionado:', idAtencionUrgenciaViolencia);
            return;
        }

        // Mostrar los datos generales en los selects del modal
        $('#sltTipoViolencia').val(violenciaSeleccionada.TipoViolencia.Id).prop('disabled', true);
        $('#sltTipoCondicion').val(violenciaSeleccionada.TipoCondicionPersona.Id).prop('disabled', true);
        $('#observaciones').val(violenciaSeleccionada.Observaciones).prop('readonly', true);

        // Mostrar/ocultar el campo sltTipoAgresor seg�n el valor de TipoViolencia
        if (violenciaSeleccionada.TipoViolencia.Id !== 0) {
            $('#divTipoAgresor').show();

            const urlAgresor = `${GetWebApiUrl()}URG_Tipo_Agresor/Combo?idTipoViolencia=${violenciaSeleccionada.TipoViolencia.Id}`;
            await setCargarDataEnCombo(urlAgresor, false, "#sltTipoAgresor");

            $('#sltTipoAgresor').val(violenciaSeleccionada.TipoAgresor.Id).prop('disabled', true);
        } else {
            $('#divTipoAgresor').hide();
        }

        // Mostrar/ocultar los campos espec�ficos para violencia sexual
        camposViolenciaSexual(violenciaSeleccionada.TipoViolencia.Id === 2);

        if (violenciaSeleccionada.TipoViolencia.Id === 2) {
            const violenciaSexual = violenciaSeleccionada.ViolenciaSexual;
            if (violenciaSexual != null) {
                $('#chkGestante').prop('checked', violenciaSexual.Gestante).bootstrapSwitch('state', violenciaSexual.Gestante);
                $('#chkAnticoncepcion').prop('checked', violenciaSexual.Anticoncepcion).bootstrapSwitch('state', violenciaSexual.Anticoncepcion);
                $('#chkProfilaxisVIH').prop('checked', violenciaSexual.ProfilaxisVIH).bootstrapSwitch('state', violenciaSexual.ProfilaxisVIH);
                $('#chkProfilaxisITS').prop('checked', violenciaSexual.ProfilaxisITS).bootstrapSwitch('state', violenciaSexual.ProfilaxisITS);
                $('#chkProfilaxisHepatitis').prop('checked', violenciaSexual.ProfilaxisHepatitis).bootstrapSwitch('state', violenciaSexual.ProfilaxisHepatitis);

                $('#chkGestante').bootstrapSwitch('disabled', true);
                $('#chkAnticoncepcion').bootstrapSwitch('disabled', true);
                $('#chkProfilaxisVIH').bootstrapSwitch('disabled', true);
                $('#chkProfilaxisITS').bootstrapSwitch('disabled', true);
                $('#chkProfilaxisHepatitis').bootstrapSwitch('disabled', true);

                $('#sltTipoTemporalidad').val(violenciaSexual.TipoTemporalidad.Id).prop('disabled', true);
                $('#sltTipoAtencionMedica').val(violenciaSexual.TipoAtencionMedica.Id).prop('disabled', true);

                $('#divtieneLesion').hide();
            }
        }

        if (violenciaSeleccionada.Lesiones.length > 0) {
            $('#divtieneLesion').show();
            $('#divLesiones').show();
            $('#chkTieneLesion').bootstrapSwitch('state', true);
            $('#chkTieneLesion').bootstrapSwitch('disabled', true);

            $('#sltTipoLesion').selectpicker("val", violenciaSeleccionada.Lesiones.map(x => x.Id));
            $('#sltTipoLesion').prop('disabled', true).selectpicker('refresh');
        } else {
            $('#chkTieneLesion').bootstrapSwitch('state', false);
        }

    } catch (error) {
        console.error('Error al mostrar los datos de violencia en el modal:', error);
    }
}


// POST VIOLENCIA SEXUAL
async function postViolenciaSexual(idAtencion, datosViolenciaSexual) {

    try {
        const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/ViolenciaSexual`;

        const response = await $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(datosViolenciaSexual)
        });

        return response;

    } catch (error) {
        console.error('Error al enviar datos de violencia sexual:', error);
        throw error;
    }
}

// ELIMINAR VIOLENCIA
async function deleteViolencia(idViolencia) {
    if (idViolencia === null || idViolencia === undefined)
        return;

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar la violencia?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Violencia/${idViolencia}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Violencia eliminada`);

                // Eliminar la fila correspondiente a la violencia eliminada
                const table = $('#tblViolencias').DataTable();
                const rowIndex = table.row(`#violencia_${idViolencia}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de violencias despu�s de eliminar
                await actualizarListaViolencias();

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divListaViolencias').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar la violencia: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

async function actualizarListaViolencias() {
    const idAtencionUrgencia = nSession.ID_INGRESO_URGENCIA;
    await actualizarViolencias(idAtencionUrgencia);
}
