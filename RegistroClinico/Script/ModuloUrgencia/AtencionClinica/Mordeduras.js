﻿let MordedurasUrgencia = [];

$(document).ready(async function () {
    $('#divListaMordeduras').hide();
    nSession = getSession();

    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 22 || nSession.CODIGO_PERFIL == 21 ||
        nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {
        MordedurasUrgencia = await getMordedura(nSession.ID_INGRESO_URGENCIA);
        dibujarTablaMordeduras(MordedurasUrgencia);
    }

    $('#mdlMordedura').on('hidden.bs.modal', function () {
        limpiarCamposMordedura();
    });
});

// LEVANTANDO MODAL
async function modalMordedura(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);

    // Mostrar los datos del paciente
    await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#divDatosPacienteMordedura");

    if (showModal) {
        $('#divCardMordedura').show();
        $('#btnGuardarMordedura').show();

        $('#chkVacunaMordedura')
            .bootstrapSwitch('state', false)
            .bootstrapSwitch('disabled', false);

        $('#codigoMidas').prop('disabled', false);
        $('#observacionesMordedura').prop('disabled', false);
        $('#mdlMordedura').modal('show');
    }

    ShowModalCargando(false);
}

// GET MORDEDURA
async function getMordedura(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        return [];

    if (nSession.CODIGO_PERFIL !== 1 && nSession.CODIGO_PERFIL !== 21 && nSession.CODIGO_PERFIL !== 29 &&
        nSession.CODIGO_PERFIL !== 3 && nSession.CODIGO_PERFIL !== 30) {
        return
    }

    mostrarUsuarioLogueadoEnModales("#mdlMordedura")

    try {
        const mordeduraData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Mordedura`,
            contentType: 'application/json',
            dataType: 'json',
        });

        return mordeduraData;

    } catch (error) {
        console.error('Error obteniendo datos de mordedura:', error);
        throw error;
    }
}

// POST MORDEDURA
async function postMordeduras(datosMordedura) {
    try {
        const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Mordedura`;

        const response = await $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(datosMordedura)
        });

        return response;

    } catch (error) {
        console.error('Error al enviar datos de mordedura:', error);
        throw error;
    }
}

// DELETE MORDEDURA
async function deleteMordedura(idMordedura) {
    if (idMordedura === null || idMordedura === undefined) {
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar la mordedura?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Mordedura/${idMordedura}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Mordedura eliminada`);

                // Eliminar la fila correspondiente a la mordedura eliminada
                const table = $('#tblMordeduras').DataTable();
                const rowIndex = table.row(`#mordedura_${idMordedura}`).index();
                table.row(rowIndex).remove().draw(false);

                // Actualizar la lista de mordeduras despu�s de eliminar
                await actualizarMordeduras(idMordedura);

                // Verificar si el DataTable tiene 0 filas y ocultar la tabla en ese caso
                if (table.rows().count() === 0) {
                    $('#divListaMordeduras').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar la mordedura: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// LIMPIEZA DE CAMPOS
function limpiarCamposMordedura() {
    $('#sltTipoAnimal').val('0').prop('disabled', false);
    $('#sltTipoMordedura').val('0').prop('disabled', false);
    $('#codigoMidas').val('').prop('readonly', false);
    $('#observacionesMordedura').val('').prop('readonly', false);
    $('#chkVacunaMordedura').bootstrapSwitch('state', false).bootstrapSwitch('disabled', false);
    validarCampos("#divMordedura");
}

// COMBOS
async function combosMordedura() {
    const urlAnimal = `${GetWebApiUrl()}GEN_Tipo_Animal/Combo`;
    const urlMordedura = `${GetWebApiUrl()}GEN_Tipo_Mordedura/Combo`;

    await setCargarDataEnCombo(urlAnimal, false, "#sltTipoAnimal");
    await setCargarDataEnCombo(urlMordedura, false, "#sltTipoMordedura");
}

// FUNCION PARA DIBUJAR LA TABLA EN LA VISTA PRINCIPAL DE ATENCION CLINICA
async function dibujarTablaMordeduras(mordeduras) {
    if (mordeduras === undefined || mordeduras.length === 0 ) {
        $('#divListaMordeduras').hide();
        return;
    } else {
        $('#divListaMordeduras').show();
    }

    // Inicializar DataTable
    const table = $('#tblMordeduras').DataTable({
        destroy: true,
        data: mordeduras,
        columns: [
            { data: "Id" },
            { data: "TipoAnimal.Valor" },
            { data: "TipoMordedura.Valor" },
            { data: "IndicacionVacuna" },
            { data: "CodigoMidas" },
            { data: "Observaciones" },
            { data: "Id" }, // Columna para los botones de accion
        ],
        columnDefs: [
            {
                targets: 3,
                render: function (data, type, row) {
                    return data ? 'Si' : 'No';
                }
            },
            {
                targets: -1,
                render: function (data, type, row) {
                    return botonesAccionMordedura(data);
                }
            }
        ],

        // Ordenar por la primera columna (ID) en orden descendente
        order: [[0, 'desc']]
    });
}

// FUNCION PARA GUARDAR MORDEDURA Y ACTUALIAZR TABLA
async function guardarMordedura() {
    if (validarCampos("#divMordedura")) {
        const mordeduraData = {
            IdTipoAnimal: parseInt($('#sltTipoAnimal').val()),
            IdTipoMordedura: parseInt($('#sltTipoMordedura').val()),
            IndicacionVacuna: $('#chkVacunaMordedura').prop('checked'),
            CodigoMidas: $('#codigoMidas').val() || null,
            Observaciones: $('#observacionesMordedura').val() || null
        };

        // Deshabilitar el botón
        $("#btnGuardarMordedura").prop('disabled', true);

        try {
            await postMordeduras(mordeduraData);
            toastr.success("Mordedura guardada");
            await actualizarMordeduras(nSession.ID_INGRESO_URGENCIA);
            limpiarCamposMordedura();
            $("#mdlMordedura").modal("hide");

            // Habilitar el botón después de la solicitud exitosa
            $("#btnGuardarMordedura").prop('disabled', false);

        } catch (error) {
            console.error('Error guardando Mordedura:', error);
            toastr.error("Error guardando mordedura. Verifica los datos e intenta nuevamente.");
            // Habilitar el botón después de algun error
            $("#btnGuardarMordedura").prop('disabled', false);
        }
    }
}

// ACTUALIZAR MORDEDURAS
async function actualizarMordeduras(idAtencionUrgencia) {
    try {
        MordedurasUrgencia = await getMordedura(idAtencionUrgencia);
        dibujarTablaMordeduras(MordedurasUrgencia);
        $("#divListaMordeduras").show();
    } catch (error) {
        console.error('Error actualizando tabla de mordeduras:', error);
    }
}

// LEVANTANDO EL MODAL PARA VER LA MORDEDURA ESPECIFICA
async function verAccionMordedura(idMordedura) {
    try {
        const mordeduras = await getMordedura(nSession.ID_INGRESO_URGENCIA);
        const mordeduraSeleccionada = mordeduras.find(mordedura => mordedura.Id === idMordedura);

        // Verificar si se encontr� la mordedura correspondiente al ID
        if (!mordeduraSeleccionada) {
            console.error('No se encontr� mordedura con el ID proporcionado:', idMordedura);
            return;
        }

        $('#btnGuardarMordedura').hide();

        // Mostrar los campos en el modal
        $('#sltTipoAnimal').val(mordeduraSeleccionada.TipoAnimal.Id).prop('disabled', true);
        $('#sltTipoMordedura').val(mordeduraSeleccionada.TipoMordedura.Id).prop('disabled', true);

        $('#chkVacunaMordedura')
            .prop('checked', mordeduraSeleccionada.IndicacionVacuna)
            .bootstrapSwitch('state', mordeduraSeleccionada.IndicacionVacuna)
            .bootstrapSwitch('disabled', true);

        $('#codigoMidas').val(mordeduraSeleccionada.CodigoMidas).prop('disabled', true);
        $('#observacionesMordedura').val(mordeduraSeleccionada.Observaciones).prop('disabled', true);

        // Mostrar el modal
        $('#mdlMordedura').modal('show');
    } catch (error) {
        console.error('Error al mostrar los datos de la mordedura en el modal:', error);
    }
}

// BOTONES 
function botonesAccionMordedura(idMordedura) {

    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30;

    const html = `
        <div class="d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button" 
                class="btn btn-info" 
                onclick="verAccionMordedura(${idMordedura})"
                data-toggle="tooltip" 
                data-placement="bottom"
                title="Ver"
                style="display: inline-block; margin-right: 5px;"
            >
                <i class="fas fa-eye fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteMordedura ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteMordedura(${idMordedura})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                style="display: inline-block;"
            >
                <i class="fa fa-trash fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}

// Funcion que manda llamar la impresion de la api impresiones
function imprimirMordeduraUrg() {
    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Mordedura/Imprimir`
    ImprimirApiExterno(url)
}