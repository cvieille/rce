﻿
let actividadesAgregadas = [];

$(document).ready(async function () {
    nSession = getSession();

    await actualizarLpp(nSession.ID_INGRESO_URGENCIA);

    const modales = {
        mdlLppAdulto: "adulto",
        mdlLppPediatrico: "pediatrico"
    };

    Object.entries(modales).forEach(([modalId, tipo]) => {
        $(`#${modalId}`).on("shown.bs.modal", function () {
            if (!window[`medidasCargadas${tipo.charAt(0).toUpperCase() + tipo.slice(1)}`]) {
                cargarDatosModal(tipo);
                window[`medidasCargadas${tipo.charAt(0).toUpperCase() + tipo.slice(1)}`] = true;
            }
        });

        $(`#${modalId}`).on("hidden.bs.modal", function () {
            resetearCamposModalLpp(`#${modalId}`);
        });
    });

    actividadesTipoAtencion();

});

function actividadesTipoAtencion() {
    $("#btnAgregarActividadAdulto").on('click', (evt) => {
        evt.preventDefault();
        if (validarCampos("#actividadesAdulto", false)) {
            const json = {
                Fecha: GetFechaActual(),
                CambioPosicion: $("#sltActividadAdulto").val() === "posicion",
                Observaciones: $("#txtObservacionesActividadesAdulto").val().trim(),
                Profesional: { Valor: getSession().NOMBRE_USUARIO },
                index: actividadesAgregadas.length
            }
            agregarActividadTr("Adulto", json);
            actividadesAgregadas.push({ Observaciones: json.Observaciones, CambioPosicion: json.CambioPosicion, index: json.index });
        }
    })

    $("#btnAgregarActividadPediatrico").on('click', (evt) => {
        evt.preventDefault();
        if (validarCampos("#actividadesPediatrico", false)) {
            const json = {
                Fecha: GetFechaActual(),
                CambioPosicion: $("#sltActividadPediatrico").val() === "posicion",
                Observaciones: $("#txtObservacionesActividadesPediatrico").val().trim(),
                Profesional: { Valor: getSession().NOMBRE_USUARIO },
                index: actividadesAgregadas.length
            };
            agregarActividadTr("Pediatrico", json);
            actividadesAgregadas.push({ Observaciones: json.Observaciones, CambioPosicion: json.CambioPosicion, index: json.index });
        }
    })

    $("#sltActividadAdulto, #sltActividadPediatrico").val("posicion");
}

function agregarActividadTr(tipo, json) {
    let cambioPosicionColumna = "";

    if (tipo !== "Gine") {
        // En Gine no se dibuja Cambio de Posición
        cambioPosicionColumna = `<td>${json.CambioPosicion ? "Posición" : "Pañales"}</td>`;
    }

    $(`#tblActividades${tipo} tbody`).append(`
        <tr class='text-center'>
            <td>${moment(json.Fecha).format("DD-MM-YYYY HH:mm:ss")}</td>
            ${cambioPosicionColumna} 
            <td>${json.Observaciones}</td>
            <td>${json.Profesional.Valor}</td>
            <td>${json.index === undefined ? "<br />" : `<button class='btn btn-danger' onClick='eliminarActividad(this, "#tblActividades${tipo}", ${json.index})'>
                                                            <i class='fa fa-trash'></i>
                                                        </button>` }
            </td>
        </tr>
    `);

    // Limpiar campos después de agregar la actividad
    $(`#txtObservacionesActividades${tipo}`).val("");

    // Mosrar la tabla del tipo correcto
    $(`#tblActividades${tipo}`).show();
}

function eliminarActividad(sender, tbl, index) {
    $(sender).parent().parent().remove();
    const indexSeleccionado = actividadesAgregadas.findIndex(actividad => actividad.index === index);
    actividadesAgregadas.splice(indexSeleccionado, 1);
    if ($(tbl + " tbody").html().trim() === "")
        $(tbl).hide();
}

// LEVANTA MODAL LPP
async function modalLpp(showModal, objAtencionUrgencia) {
    ShowModalCargando(true);
    // Determinar el tipo de atencion
    const tipoAtencion = objAtencionUrgencia.tipoAtencion?.Id;
    const esAdulto = tipoAtencion === 1 || tipoAtencion === 2;
    const esPediatrico = tipoAtencion === 3;
    // Determinar el modal y contenedores
    const pintarDatosPaciente = esAdulto ? "#divDatosPacienteLpp" : esPediatrico ? "#divDatosPacienteLppPediatrico" : null;
    const levantaModal = esAdulto ? "#mdlLppAdulto" : esPediatrico ? "#mdlLppPediatrico" : null;
    //Verificar categorizacion
    const categorizacion = objAtencionUrgencia.categorizacion[objAtencionUrgencia.categorizacion.length - 1];
    const tipoCategorizacion = categorizacion?.Codigo;

    // Actualizar requerimientos de escala Braden
    actualizarRequerimientosEscalaBraden(tipoCategorizacion);

    // Mostrar datos del paciente
    if (pintarDatosPaciente) {
        await showDatosPacienteInicacionMedica(objAtencionUrgencia, pintarDatosPaciente);
    }
    // Levantar modal
    if (showModal && levantaModal) {
        await actualizarLpp(nSession.ID_INGRESO_URGENCIA)

        resetearCamposModalLpp(levantaModal);

        $(".btnGuardarLpp").text("Guardar");
        $(".btnGuardarLpp").data("id", "");

        $(levantaModal).modal('show');

        $(".lblTotalPuntaje").text("0");
        $("#puntajeAdulto").hide();
        $("#puntajePediatrico").hide();

        controlarObservacionesGine(tipoAtencion)
    }
    ShowModalCargando(false);
}

// Controlar la visibilidad de las obs gine
function controlarObservacionesGine(tipoAtencion) {
    if (tipoAtencion === 2) {
        // Si el tipo de atención es Ginec mostrar el bloque de observaciones de gine
        $("#actividadesGine").show();
        $("#actividadesAdulto").hide();

        // Cambiar el atributo data-required de 'txtObservacionesActividadesAdulto' a 'false'
        $("#txtObservacionesActividadesAdulto").attr("data-required", "false");
    } else {
        // Si no es ginecología, ocultarlo y restaurar el atributo 'data-required' a 'true'
        $("#actividadesGine").hide();
        $("#actividadesAdulto").show();

        // Restaurar el atributo data-required a 'true'
        $("#txtObservacionesActividadesAdulto").attr("data-required", "true");
    }
}

function cargarDatosModal(tipo) {
    cargarEscalasBraden(tipo);
    cargarMedidasPrevencion(tipo);
    cargarValoracionPielMucosas(tipo);
}

function resetearCamposModalLpp(modalId) {

    const modal = $(modalId);
    // Limpiar campos de formulario
    modal.find("input[type='checkbox']").prop("checked", false);
    modal.find("select").val("0");
    modal.find("#sltActividadAdulto, #sltActividadPediatrico").val("posicion");
    modal.find("#tblActividadesAdulto").hide();
    modal.find("#tblActividadesGine").hide();
    modal.find("#tblActividadesPediatrico").hide();
    modal.find("#tblActividadesAdulto tbody").empty();
    modal.find("#tblActividadesPediatrico tbody").empty();
    modal.find("#tblActividadesGine tbody").empty();

    modal.find("textarea, input[type='text']").val("");
    actividadesAgregadas = [];
    // Resetear pestañas a la primera
    modal.find(".nav-tabs .nav-link:first").tab("show");
    // Resetear variables de control
    if (modalId === "#mdlLppAdulto") {
        medidasCargadasAdulto = false;
    } else if (modalId === "#mdlLppPediatrico") {
        medidasCargadasPediatrico = false;
    }
}

// Función que carga las escalas Braden de adultos y pediátricos
async function cargarEscalasBraden(tipo) {
    const id = tipo === "adulto" ? 10 : 11;
    const preguntas = tipo === "adulto" ? getAdultos() : getPediatricos();

    try {
        const data = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Tipo_Escala_Clinica/${id}`,
            dataType: 'json'
        });

        preguntas.forEach(pregunta => {
            const escalaPregunta = data.PreguntaEscalaClinica.find(
                e => e.IdPreguntaEscalaClinica === pregunta.id
            );

            if (escalaPregunta) {
                poblarRadioBraden(pregunta.selector, escalaPregunta.Alternativas);
            } else {
                console.error(`El Pregunta con Id ${pregunta.id} no se encuentra en ${tipo}`);
            }
        });
    } catch (error) {
        console.error(`Error al cargar escalas Braden para ${tipo}:`, error);
    }
}

// Función para poblar radio buttons
function poblarRadioBraden(selector, alternativas) {
    const container = $(selector);
    container.empty(); // Limpiar el contenedor
    alternativas.forEach(alternativa => {
        const radioId = `${selector.replace('#', '')}_${alternativa.IdAlternativaEscalaClinica}`;
        container.append(`
            <div class="form-check">
                <input class="form-check-input escala-braden" type="radio" 
                       name="${selector.replace('#grp', '')}" 
                       id="${radioId}" 
                       value="${alternativa.IdAlternativaEscalaClinica}"
                       data-puntaje="${alternativa.Puntaje}">
                <label class="form-check-label" for="${radioId}">
                    ${alternativa.DescripcionAlternativaEscalaClinica} (Puntaje: ${alternativa.Puntaje})
                </label>
            </div>
        `);
    });
}

// Obtener Preguntas de adultos
function getAdultos() {
    return [
        { id: 49, selector: "#grpPercepcionSensorialAdulto" },
        { id: 50, selector: "#grpHumedadAdulto" },
        { id: 14, selector: "#grpActividadAdulto" },
        { id: 15, selector: "#grpMovilidadAdulto" },
        { id: 17, selector: "#grpNutricionAdulto" },
        { id: 51, selector: "#grpFuerzaAdulto" }
    ];
}

// Obtener Preguntas de pediátricos
function getPediatricos() {
    return [
        // Intensidad y duracion de presion
        { id: 49, selector: "#grpPercepcionSensorialPediatrico" },
        { id: 14, selector: "#grpActividadPediatrico" },
        { id: 15, selector: "#grpMovilidadPediatrico" },
        // Tolerancia de la piel y estructura
        { id: 50, selector: "#grpHumedadPediatrico" },
        { id: 52, selector: "#grpFriccionCizallamientoPediatrico" },
        { id: 17, selector: "#grpNutricionPediatrico" },
        { id: 53, selector: "#grpPerfusionOxigenacionPediatrico" }
    ];
}

async function cargarValoracionPielMucosas(tipo) {

    try {
        // Obtener los datos de urgencia
        const ingresoUrgencia = await obtenerTipoAtencion(nSession.ID_INGRESO_URGENCIA);

        // Verificar que se obtuvo el tipo de atención
        if (!ingresoUrgencia || !ingresoUrgencia.TipoAtencion || !ingresoUrgencia.TipoAtencion.Id) {
            console.error("No se pudo obtener el ID del tipo de atención.");
            return;
        }

        const idTipoAtencion = ingresoUrgencia.TipoAtencion.Id;

        // URL con el ID de TipoAtencion
        const url = `${GetWebApiUrl()}GEN_Tipo_Valoracion/Combo?idTipoAtencionUrgencia=${idTipoAtencion}`;

        const valoraciones = await $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json'
        });

        // Seleccionar el contenedor (adulto o pediátrico)
        const contenedor = tipo === "adulto" ? $("#valoracionPielRadiosAdulto") : $("#valoracionPielRadiosPediatrico");
        contenedor.empty();

        valoraciones.forEach(valoracion => {
            // Crear radios utilizando la clase bootstrapSwitch para cada opción
            const radioGroup = `
                <div class="col-md-4">
                    <strong class="text-dark">${valoracion.Valor}</strong><br />
                    <input 
                        id="medidaValoracionSi-${valoracion.Id}" 
                        type="radio" 
                        name="medidaValoracion-${valoracion.Id}" 
                        class="bootstrapSwitch"
                        data-on-text="SI" 
                        data-off-text="SI" 
                        data-on-color="primary" 
                        data-size="small"
                        value="${valoracion.Id}" />
                    <input 
                        id="medidaValoracionNo-${valoracion.Id}" 
                        type="radio" 
                        name="medidaValoracion-${valoracion.Id}" 
                        class="bootstrapSwitch"
                        data-on-text="NO" 
                        data-off-text="NO" 
                        data-on-color="danger" 
                        data-size="small"
                        value="${valoracion.Id}" />
                </div>
            `;
            contenedor.append(radioGroup);
        });

        contenedor.find(".bootstrapSwitch").bootstrapSwitch({
            radioAllOff: true,
        });

        inicializarEventosValoracionPielMucosas(tipo);

    } catch (error) {
        console.error("Error al cargar las valoraciones de piel y mucosas:", error);
    }
}

function inicializarEventosValoracionPielMucosas(tipo) {
    const contenedor = tipo === "adulto" ? $("#valoracionPielRadiosAdulto") : $("#valoracionPielRadiosPediatrico");
    const idNoAplica = 12; // ID del "NO APLICA"

    contenedor.on("switchChange.bootstrapSwitch", "input[type='radio']", function (event, state) {
        const radioId = $(this).attr("id"); // ID del radio seleccionado
        const valoracionId = $(this).attr("name").split("-")[1]; // Extraer el ID de la valoración

        // Caso 1: Si cualquier valoración (excepto NO APLICA) se selecciona como "SI", marcar automáticamente "NO" en NO APLICA
        if (radioId.startsWith("medidaValoracionSi-") && valoracionId != idNoAplica && state) {
            $(`#medidaValoracionNo-${idNoAplica}`).bootstrapSwitch('state', true, true); // Marcar "NO" en NO APLICA
            $(`#medidaValoracionSi-${idNoAplica}`).bootstrapSwitch('state', false, true); // Desmarcar "SI" en NO APLICA
        }

        // Caso 2: Si "NO APLICA" se selecciona como "SI", desmarcar "SI" en todas las valoraciones y marcar "NO"
        if (radioId === `medidaValoracionSi-${idNoAplica}` && state) {
            contenedor.find(`[name^="medidaValoracion-"]:not([name="medidaValoracion-${idNoAplica}"])`).each(function () {
                const id = $(this).attr("id");
                if (id.startsWith("medidaValoracionSi-")) {
                    $(this).bootstrapSwitch('state', false, true); // Desmarcar "SI"
                }
                if (id.startsWith("medidaValoracionNo-")) {
                    $(this).bootstrapSwitch('state', true, true); // Marcar "NO"
                }
            });
        }
    });
}

// Obtener las valoraciones de piel y mucosas seleccionadas
function getValoracionesSeleccionadas(tipoAtencion) {
    const contenedor = tipoAtencion === "adulto" ? $("#valoracionPielRadiosAdulto") : $("#valoracionPielRadiosPediatrico");
    const valoraciones = [];

    // Iterar sobre cada radio SI
    contenedor.find("input[type='radio']:checked").each(function () {
        if ($(this).data("on-text") === "SI") {
            const valoracionId = parseInt($(this).val(), 10); // Obtener id del valro seleccionado
            valoraciones.push(valoracionId); // Agrega solo si es "SI"
        }
    });

    return valoraciones;
}

// Medidas prevencion
async function cargarMedidasPrevencion(tipo) {
    try {
        const url = `${GetWebApiUrl()}GEN_Tipo_Medidas_Prevencion/Combo`;
        const medidas = await $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json'
        });

        // Seleccionar adulto o pediatrico
        const contenedor = tipo === "adulto" ? $("#medidasPrevencionRadiosAdulto") : $("#medidasPrevencionRadiosPediatrico");
        contenedor.empty();

        medidas.forEach(medida => {
            // Crear radios para cada medida
            const radioGroup = `
                <div class="col-md-4">
                    <strong class="text-dark">${medida.Valor}</strong><br />
                    <label>
                        <input 
                            id="medidaPrevencionSi-${medida.Id}" 
                            type="radio" 
                            name="medidaPrevencion-${medida.Id}" 
                            class="bootstrapSwitch"
                            data-on-text="SI" 
                            data-off-text="SI" 
                            data-on-color="primary" 
                            data-size="small"
                            value="${medida.Id}" />
                    </label>
                    <label>
                        <input 
                            id="medidaPrevencionNo-${medida.Id}" 
                            type="radio" 
                            name="medidaPrevencion-${medida.Id}" 
                            class="bootstrapSwitch"
                            data-on-text="NO" 
                            data-off-text="NO" 
                            data-on-color="danger"
                            data-size="small"
                            value="${medida.Id}" />
                    </label>
                </div>
            `;

            contenedor.append(radioGroup);
        });

        // Inicializar Bootstrap Switch después de agregar los radios al DOM
        contenedor.find(".bootstrapSwitch").bootstrapSwitch({
            radioAllOff: true, // Permite desmarcar todos los radios si es necesario
        });

        inicializarEventosMedidasPrevencion(tipo);

    } catch (error) {
        console.error("Error al cargar las medidas de prevención:", error);
    }
}

function inicializarEventosMedidasPrevencion(tipo) {
    const contenedor = tipo === "adulto" ? $("#medidasPrevencionRadiosAdulto") : $("#medidasPrevencionRadiosPediatrico");
    const idNoAplica = 5; // ID del "NO APLICA"

    contenedor.on("switchChange.bootstrapSwitch", "input[type='radio']", function (event, state) {
        const radioId = $(this).attr("id"); // ID del radio seleccionado
        const medidaId = $(this).attr("name").split("-")[1]; // Extraer el ID de la medida

        // Caso 1: Si cualquier medida (1-4) se selecciona como "SI", marcar automáticamente "NO" en NO APLICA
        if (radioId.startsWith("medidaPrevencionSi-") && medidaId != idNoAplica && state) {
            $(`#medidaPrevencionNo-${idNoAplica}`).bootstrapSwitch('state', true, true); // Marcar "NO" en NO APLICA
            $(`#medidaPrevencionSi-${idNoAplica}`).bootstrapSwitch('state', false, true); // Descmarcar "SI" en NO APLICA
        }

        // Caso 2: Si "NO APLICA" (ID 5) se selecciona como "SI", desmarcar "SI" en todas las medidas (1-4) y marcar "NO"
        if (radioId === `medidaPrevencionSi-${idNoAplica}` && state) {
            contenedor.find(`[name^="medidaPrevencion-"]:not([name="medidaPrevencion-${idNoAplica}"])`).each(function () {
                const id = $(this).attr("id");
                if (id.startsWith("medidaPrevencionSi-")) {
                    $(this).bootstrapSwitch('state', false, true); // Desmarcar "SI" en otras medidas
                }
                if (id.startsWith("medidaPrevencionNo-")) {
                    $(this).bootstrapSwitch('state', true, true); // Marcar "NO" en otras medidas
                }
            });
        }
    });
}

// Obtener las medidas de prevencion
function getMedidasPrevencionSeleccionadas(tipoAtencion) {
    const contenedor = tipoAtencion === "adulto" ? $("#medidasPrevencionRadiosAdulto") : $("#medidasPrevencionRadiosPediatrico");
    const medidas = [];

    // Iterar sobre cada radio "SI"
    contenedor.find("input[type='radio']:checked").each(function () {
        if ($(this).data("on-text") === "SI") {
            const medidaId = parseInt($(this).val(), 10); // Obtener id de la medida seleccionada
            medidas.push(medidaId); // Agregamos solo si es SI
        }
    });

    return medidas;
}

// GET LPP
async function getLpp(idAtencionUrgencia) {
    if (!idAtencionUrgencia)
        return;
    try {
        // Realizar solicitud para obtener datos del LPP
        const lppData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/LPP`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return lppData;
    } catch (error) {
        console.error('Error obteniendo datos de LPP:', error);
        throw error;
    }
}

// GET específico para un registro LPP
async function getLppEspecifico(idAtencionUrgencia, idLpp) {
    if (!idAtencionUrgencia || !idLpp) {
        console.error("ID de atención de urgencia o ID de LPP no proporcionado");
        return;
    }
    try {
        // Realizar solicitud para obtener un registro específico del LPP
        const lppData = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/LPP/${idLpp}`,
            contentType: 'application/json',
            dataType: 'json',
        });
        return lppData;

    } catch (error) {
        console.error('Error obteniendo datos del registro LPP:', error);
        throw error;
    }
}

// ACTUALIZAR LPP
async function actualizarLpp(idAtencionUrgencia) {
    const listaLpp = await getLpp(idAtencionUrgencia);

    // Verificar si LPP es indefinido o si es 0
    if (!listaLpp || listaLpp.length === 0) {
        // Ocultar div LPP por estar vacia
        $("#divListaLpp").hide();
        return;
    }
    dibujarTablaLpp(listaLpp);
    $("#divListaLpp").show();
}

// DELETE LPP
async function deleteLpp(idLpp) {
    if (idLpp === null || idLpp === undefined) {
        return;
    }

    const result = await Swal.fire({
        title: "\u00BFSeguro quieres eliminar el LPP?",
        text: "No se podr\u00E1n revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    });

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/LPP/${idLpp}`
        };

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`LPP Eliminado`);
                // Elimnando la fila correspondiente al lpp eliminado
                const table = $('#tblLpp').DataTable();
                const rowIndex = table.row(`#lpp_${idLpp}`).index();
                table.row(rowIndex).remove().draw(false);
                // Actualizar el div de lpp despues de eliminar
                await actualizarLpp(nSession.ID_INGRESO_URGENCIA);
                // Verifica si wel datatable tiene 0 filas y asi ocultamos el div
                if (table.rows().count() === 0) {
                    $('#divListaLpp').hide();
                }
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar LPP: ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}

// POST PUT LPP
async function guardarDatosLpp() {

    // Validamos los campos requeridos antes de continuar
    const validacion = await validacionRequeridos();
    if (!validacion) {
        return; // No permite guardar si existen requeridos
    }

    const idLpp = $(".btnGuardarLpp").data("id");
    const tipoAtencion = $("#mdlLppAdulto").is(":visible") ? "adulto" : "pediatrico";

    const escalaClinicaPreguntas = tipoAtencion === "adulto" ? getAdultos() : getPediatricos();
    const medidasPrevencion = getMedidasPrevencionSeleccionadas(tipoAtencion);
    const valoraciones = getValoracionesSeleccionadas(tipoAtencion);

    let json = GetJsonIngresoUrgencia(nSession.ID_INGRESO_URGENCIA);

    // Determina el IdTipoEscalaClinica según el tipo atencion
    let idTipoEscala;
    if (tipoAtencion === "adulto") {
        idTipoEscala = 10; // Adulto
    } else {
        idTipoEscala = 11; // Pediátrico
    }

    // Capturar observaciones si es gine
    const observacionesGine = $("#txtObservacionesActividadesGine").val().trim();

    // Agregar respuestas para el objeto
    const respuestas = escalaClinicaPreguntas.map(pregunta => {
        const valorSeleccionado = $(`${pregunta.selector} input[type='radio']:checked`).val();
        return parseInt(valorSeleccionado, 10) || 0;
    }).filter(respuesta => respuesta !== 0);

    const postData = {
        EscalaClinica: {
            IdEvento: json.IdEvento,
            IdTipoEscala: idTipoEscala,
            Observacion: null,
            Respuestas: respuestas
        },
        MedidasPrevencion: medidasPrevencion,
        Valoraciones: valoraciones,
        Actividades: actividadesAgregadas,
        Observaciones: observacionesGine
    };

    const urlBase = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/LPP`;
    const method = idLpp ? "PUT" : "POST";
    const url = idLpp ? `${urlBase}/${idLpp}` : urlBase;

    // Deshabilitar botón para evitar múltiples clics en guardar
    $(".btnGuardarLpp").prop("disabled", true);

    try {
        await $.ajax({
            type: method,
            url: url,
            contentType: "application/json",
            data: JSON.stringify(postData),
        });

        await actualizarLpp(nSession.ID_INGRESO_URGENCIA);
        toastr.success(`Los datos LPP se han ${idLpp ? "actualizado" : "guardado"} correctamente.`);
        $("#mdlLppAdulto").modal("hide");
        $("#mdlLppPediatrico").modal("hide");

    } catch (error) {
        console.error("Error en la llamada AJAX:", error.responseText || error);
        toastr.error(`Error al guardar los datos: ${error.responseText || "Revisa la consola para más detalles."}`);
    } finally {
        $(".btnGuardarLpp").prop("disabled", false);
    }
}

async function editarLpp(idLpp) {

    try {

        // Obtener los datos de urgencia
        const ingresoUrgencia = await obtenerTipoAtencion(nSession.ID_INGRESO_URGENCIA);

        // Verificar que se obtuvo el tipo de atención
        if (!ingresoUrgencia || !ingresoUrgencia.TipoAtencion || !ingresoUrgencia.TipoAtencion.Id) {
            console.error("No se pudo obtener el ID del tipo de atención.");
            return;
        }

        const idTipoAtencion = ingresoUrgencia.TipoAtencion.Id;
        const res = await getLppEspecifico(nSession.ID_INGRESO_URGENCIA, idLpp);

        if (res && res.UrgenciaLPP) {

            const { LPPMedidasPrevencion, LPPValoracion, LPPObservaciones, UrgenciaLPP } = res;
            const tipoAtencion = nSession.ID_TIPO_ATENCION_URGENCIA === "3" ? "Pediatrico" : nSession.ID_TIPO_ATENCION_URGENCIA === "2" ? "Gine" : "Adulto";
            // Obteniendo categorización
            const categorizacion = await getCategorizacion(nSession.ID_INGRESO_URGENCIA);
            const tipoCategorizacion = categorizacion[categorizacion.length - 1].Codigo;

            // Usar la categorización para actualizar requerido de escala Braden
            actualizarRequerimientosEscalaBraden(tipoCategorizacion);

            resetearCamposModalLpp();

            setTimeout(function () {
                if (UrgenciaLPP && UrgenciaLPP.EscalaClinica && UrgenciaLPP.EscalaClinica.Respuestas) {
                    const mapeoSelectores = {
                        Adulto: {
                            49: "#grpPercepcionSensorialAdulto",
                            50: "#grpHumedadAdulto",
                            14: "#grpActividadAdulto",
                            15: "#grpMovilidadAdulto",
                            17: "#grpNutricionAdulto",
                            51: "#grpFuerzaAdulto"
                        },
                        Gine: null,
                        Pediatrico: {
                            49: "#grpPercepcionSensorialPediatrico",
                            14: "#grpActividadPediatrico",
                            15: "#grpMovilidadPediatrico",
                            50: "#grpHumedadPediatrico",
                            52: "#grpFriccionCizallamientoPediatrico",
                            17: "#grpNutricionPediatrico",
                            53: "#grpPerfusionOxigenacionPediatrico"
                        }
                    };

                    // Hacer que Ginecología use el mismo mapeo que Adulto
                    mapeoSelectores.Gine = mapeoSelectores.Adulto;

                    UrgenciaLPP.EscalaClinica.Respuestas.forEach(respuesta => {
                        const idPregunta = respuesta.Pregunta?.Id;
                        const idEscalaClinica = respuesta.Id;

                        if (mapeoSelectores[tipoAtencion][idPregunta]) {
                            const selectorGrupo = mapeoSelectores[tipoAtencion][idPregunta];
                            $(`${selectorGrupo} input[type="radio"][value="${idEscalaClinica}"]`).prop("checked", true);
                        }
                    });
                } else {
                    console.warn("UrgenciaLPP, EscalaClinica o Respuestas están vacíos o no existen.");
                }
            }, 1000);


            // Valoracion piel y mucosas
            setTimeout(function () {
                // Aseguramos que todos los radios "NO" estén marcados por defecto
                $('[id^="medidaValoracionSi-"]').prop('checked', false);  // Desmarcar todos los radios SI
                $('[id^="medidaValoracionNo-"]').prop('checked', true);  // Marcar todos los radios NO

                if (LPPValoracion && LPPValoracion.length > 0) {
                    LPPValoracion.forEach(function (valoracion) {
                        const tipoMedidaId = valoracion.TipoMedidaPrevencion?.Id;
                        const valor = valoracion.TipoMedidaPrevencion?.Valor;  // tomamos el valor

                        const radioSiId = `#medidaValoracionSi-${tipoMedidaId}`;
                        const radioNoId = `#medidaValoracionNo-${tipoMedidaId}`;

                        const radioSi = $(radioSiId);
                        const radioNo = $(radioNoId);

                        // Si el valor existe, marcamos "SI"
                        if (valor) {
                            radioSi.prop('checked', true);
                            radioNo.prop('checked', false);
                        }
                    });
                } else {
                    console.warn("LPPValoracion está vacío o no existe.");
                }

                // Inicializamos el switch para los radios
                $('[id^="medidaValoracionSi-"]').each(function () {
                    const radioSi = $(this);
                    radioSi.bootstrapSwitch('state', radioSi.is(":checked"), true);
                });

                $('[id^="medidaValoracionNo-"]').each(function () {
                    const radioNo = $(this);
                    radioNo.bootstrapSwitch('state', radioNo.is(":checked"), false);
                });
            }, 1000);

            // Medidas de prevencion
            setTimeout(function () {
                // Aseguramos que todos los radios "NO" estén marcados por defecto
                $('[id^="medidaPrevencionSi-"]').prop('checked', false);  // Desmarcar todos los radios SI
                $('[id^="medidaPrevencionNo-"]').prop('checked', true);  // Marcar todos los radios NO

                if (LPPMedidasPrevencion && LPPMedidasPrevencion.length > 0) {
                    LPPMedidasPrevencion.forEach(function (prevencion) {
                        const tipoMedidaId = prevencion.TipoMedidaPrevencion?.Id;
                        const valor = prevencion.TipoMedidaPrevencion?.Valor; // tomamos el valor

                        const radioSiId = `#medidaPrevencionSi-${tipoMedidaId}`;
                        const radioNoId = `#medidaPrevencionNo-${tipoMedidaId}`;

                        const radioSi = $(radioSiId);
                        const radioNo = $(radioNoId);

                        // Si el valor existe y es "SI", marcamos "si"
                        if (valor) {
                            radioSi.prop('checked', true);
                            radioNo.prop('checked', false);
                        }
                    });
                } else {
                    console.warn("LPPMedidasPrevencion está vacío o no existe.");
                }

                // Inicializar los switch radio buttons
                $('[id^="medidaPrevencionSi-"]').each(function () {
                    const radioSi = $(this);
                    radioSi.bootstrapSwitch('state', radioSi.is(":checked"), true);
                });

                $('[id^="medidaPrevencionNo-"]').each(function () {
                    const radioNo = $(this);
                    radioNo.bootstrapSwitch('state', radioNo.is(":checked"), false);
                });
            }, 1000);

            cargarObservaciones(tipoAtencion, LPPObservaciones);

            if (idTipoAtencion === 3) {
                $(`#mdlLppPediatrico`).modal("show");
                mostrarUsuarioLogueadoEnModales("#mdlLppPediatrico");
                await cargarEscalasBraden("pediatrico");
                await cargarValoracionPielMucosas("pediatrico");
                await cargarMedidasPrevencion("pediatrico");

                // Obtener el puntaje pediatrico
                const puntajePediatrico = await obtenerTotalPuntaje(UrgenciaLPP.EscalaClinica.Id);
                if (puntajePediatrico == null || puntajePediatrico === 0) {
                    $("#puntajePediatrico .lblTotalPuntaje").html(`0 <span class="badge badge-danger">Riesgo alto</span>`);
                } else {
                    const { nivelRiesgo, colorRiesgo } = calcularNivelRiesgo(puntajePediatrico, nSession.ID_TIPO_ATENCION_URGENCIA);
                    $("#puntajePediatrico .lblTotalPuntaje").html(`${puntajePediatrico} <span class="${colorRiesgo}">${nivelRiesgo}</span>`);
                }

                $("#puntajePediatrico").show();

            } else if (idTipoAtencion === 2) {
                $(`#mdlLppAdulto`).modal("show");
                mostrarUsuarioLogueadoEnModales("#mdlLppAdulto");
                await cargarEscalasBraden("adulto");
                await cargarValoracionPielMucosas("adulto");
                await cargarMedidasPrevencion("adulto");

                if (UrgenciaLPP && UrgenciaLPP.Observaciones) {
                    $("#txtObservacionesActividadesGine").val(UrgenciaLPP.Observaciones);
                } else {
                    $("#txtObservacionesActividadesGine").val("");
                }

                // Obtener el puntaje adulto
                const puntajeAdulto = await obtenerTotalPuntaje(UrgenciaLPP.EscalaClinica.Id);
                if (puntajeAdulto == null || puntajeAdulto === 0) {
                    $("#puntajeAdulto .lblTotalPuntaje").html(`0 <span class="badge badge-danger">Riesgo alto</span>`);
                } else {
                    // Establecer el puntaje con riesgo calculado
                    const { nivelRiesgo, colorRiesgo } = calcularNivelRiesgo(puntajeAdulto, nSession.ID_TIPO_ATENCION_URGENCIA);
                    $("#puntajeAdulto .lblTotalPuntaje").html(`${puntajeAdulto} <span class="${colorRiesgo}">${nivelRiesgo}</span>`);
                }

                $("#puntajeAdulto").show();
                $("#actividadesAdulto").hide();

            } else {
                $(`#mdlLppAdulto`).modal("show");
                mostrarUsuarioLogueadoEnModales("#mdlLppAdulto");
                await cargarEscalasBraden("adulto");
                await cargarValoracionPielMucosas("adulto");
                await cargarMedidasPrevencion("adulto");

                // Obtener el puntaje adulto
                const puntajeAdulto = await obtenerTotalPuntaje(UrgenciaLPP.EscalaClinica.Id);
                if (puntajeAdulto == null || puntajeAdulto === 0) {
                    $("#puntajeAdulto .lblTotalPuntaje").html(`0 <span class="badge badge-danger">Riesgo alto</span>`);
                } else {
                    // Establecer el puntaje con riesgo calculado
                    const { nivelRiesgo, colorRiesgo } = calcularNivelRiesgo(puntajeAdulto, nSession.ID_TIPO_ATENCION_URGENCIA);
                    $("#puntajeAdulto .lblTotalPuntaje").html(`${puntajeAdulto} <span class="${colorRiesgo}">${nivelRiesgo}</span>`);
                }

                $("#puntajeAdulto").show();
                $("#actividadesGine").hide();
            }

            $(".btnGuardarLpp").data("id", idLpp);
            $(".btnGuardarLpp").text("Actualizar");
        } else {
            console.log("No se encontró el registro LPP con el ID especificado.");
        }
    } catch (error) {
        console.error("Error al intentar obtener los datos del registro LPP:", error);
        toastr.error("Hubo un problema al cargar los datos de LPP.");
    }
}

function cargarObservaciones(tipoAtencion, observaciones) {
    observaciones.forEach((obs) => {
        agregarActividadTr(tipoAtencion, obs);
    });
}

// BOTONES
function botonesAccionLpp(row) {

    const { Id } = row;
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25;

    const html = `
        <div class="d-flex flex-row justify-content-center flex-nowrap">
            <button 
                type="button"
                class="btn btn-warning"
                onclick="editarLpp(${Id})"
                data-toggle="tooltip" 
                data-placement="bottom"
                title="Editar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                style="display: inline-block; margin-right: 5px;">
                <i class="fas fa-edit fa-xs"></i>
            </button>
            <button 
                type="button" 
                class="btn btn-danger deleteLpp ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteLpp(${Id})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Eliminar"
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                style="display: inline-block; margin-right: 5px;">
                <i class="fa fa-trash fa-xs"></i>
            </button>
            <button
                type="button"
                class="btn btn-info" 
                onclick="imprimirLpp(${Id})"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Imprimir"
                style="display: inline-block;">
                <i class="fa fa-print fa-xs"></i>
            </button>
        </div>
    `;

    return html;
}

async function obtenerTotalPuntaje(idEscalaClinica) {
    const url = `${GetWebApiUrl()}RCE_Escala_Clinica/${idEscalaClinica}`;

    try {
        const data = await $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
        });

        const totalPuntaje = data.TotalPuntaje; // Capturamos el total del puntaje
        const { nivelRiesgo, colorRiesgo } = calcularNivelRiesgo(totalPuntaje, nSession.ID_TIPO_ATENCION_URGENCIA);

        // Actualizando DOM con la clase lblTotalPuntaje
        $('.lblTotalPuntaje').html(`${totalPuntaje} <span class="${colorRiesgo}">${nivelRiesgo}</span>`);

        return totalPuntaje;
    } catch (error) {
        return 0;
    }
}

// DIBUJA TABLA EN ATENCION CLINICA
async function dibujarTablaLpp(lpp) {
    // Obtener los datos de urgencia
    const ingresoUrgencia = await obtenerTipoAtencion(nSession.ID_INGRESO_URGENCIA);

    // Verificar que se obtuvo el tipo de atención
    if (!ingresoUrgencia || !ingresoUrgencia.TipoAtencion || !ingresoUrgencia.TipoAtencion.Id) {
        console.error("No se pudo obtener el ID del tipo de atención.");
        return;
    }

    const idTipoAtencion = ingresoUrgencia.TipoAtencion.Id;

    const lpps = await Promise.all(lpp.map(async (lpp) => {
        const totalPuntaje = await obtenerTotalPuntaje(lpp.UrgenciaLPP.EscalaClinica.Id); // Llama a la función para obtener el puntaje
        const { nivelRiesgo, colorRiesgo } = calcularNivelRiesgo(totalPuntaje, nSession.ID_TIPO_ATENCION_URGENCIA);

        return {
            Id: lpp.UrgenciaLPP.Id,
            TotalPuntaje: totalPuntaje, // Asigna el puntaje obtenido
            NivelRiesgo: nivelRiesgo, // Asigna nivel de riesgo
            ColorRiesgo: colorRiesgo, // Asigna el color del badge según nivel de riesgo
            Valoracion: lpp.LPPValoracion.map(mv => mv.TipoMedidaPrevencion.Valor),
            MedidasPrevencion: lpp.LPPMedidasPrevencion.map(mp => mp.TipoMedidaPrevencion.Valor),
            Fecha: lpp.UrgenciaLPP.Fecha,
            Profesional: lpp.UrgenciaLPP.Profesional.Persona.Nombre,
            // Condición para mostrar las Observaciones o las LPPObservaciones
            Observaciones: idTipoAtencion === 2
                ? (lpp.UrgenciaLPP.Observaciones)
                : (lpp.LPPObservaciones && lpp.LPPObservaciones.length > 0
                    ? lpp.LPPObservaciones.map(o => o.Observaciones).join("; ")
                    : "Sin observaciones"),
        };
    }));

    // Obteniendo categorización
    const categorizacion = await getCategorizacion(nSession.ID_INGRESO_URGENCIA);
    const tipoCategorizacion = categorizacion[categorizacion.length - 1].Codigo;

    // Crear o actualizar DataTable
    $("#tblLpp").DataTable({
        destroy: true,
        data: lpps,
        ordering: true,
        order: [[0, 'desc']],
        language: {
            "emptyTable": "No hay LPP disponibles para mostrar",
        },
        columns: [
            { data: 'Id' },
            { data: 'TotalPuntaje' },
            { data: 'Valoracion' },
            { data: 'MedidasPrevencion' },
            { data: 'Observaciones' },
            { data: 'Fecha' },
            { data: 'Profesional' },
            { data: 'Id' },
        ],
        columnDefs: [
            {
                targets: 1, // TotalPuntaje
                render: function (data, type, row) {
                    const nivelRiesgo = row.NivelRiesgo || "Sin riesgo";
                    const colorRiesgo = row.ColorRiesgo || "badge badge-secondary";
                    if (!data) {
                        return `<div class="text-left">
                                    <i class="fa fa-circle mr-2 text-danger"
                                        style="font-size:11px"
                                        aria-hidden="true"></i>No aplica en <strong>${tipoCategorizacion}<strong><br>
                                        <span class="${colorRiesgo}">${nivelRiesgo}</span></div>`;
                    } else {
                        return `Puntaje: <strong>${data}</strong> <br>
                        <span class="${colorRiesgo}">${nivelRiesgo}</span>`;
                    }
                }
            },
            {
                targets: 2, // Valoración
                render: function (data, type, row) {
                    if (!data || data.length === 0) {
                        return `<div class="text-left">
                                    <i class="fa fa-circle mr-2 text-danger"
                                        style="font-size:11px"
                                        aria-hidden="true"></i>No hay información</div>`;
                    } else {
                        return data.map(valoracion =>
                            `<div class="text-left">
                                <i class="fa fa-circle mr-2 text-primary" 
                                    style="font-size:11px;" 
                                    aria-hidden="true"></i>${valoracion}
                            </div>`
                        ).join("");
                    }
                }
            },
            {
                targets: 3, // Medidas de Prevención
                render: function (data, type, row) {
                    if (!data || data.length === 0) {
                        return `<div class="text-left">
                                    <i class="fa fa-circle mr-2 text-danger"
                                        style="font-size:11px"
                                        aria-hidden="true"></i>No hay información</div>`;
                    } else {
                        return data.map(medidasPrevencion =>
                            `<div class="text-left">
                                <i class="fa fa-circle mr-2 text-primary"
                                    style="font-size:11px;"
                                    aria-hidden="true"></i>${medidasPrevencion}
                            </div>`
                        ).join("");
                    }
                }
            },
            {
                targets: 4, // Observaciones
                render: function (data) {
                    if (typeof data === "string" && data.trim().length > 0) {
                        if (data.includes("; ")) {
                            return data
                                .split("; ") // Separamos las observaciones
                                .map(
                                    (obs) =>
                                        `<div class="text-left">
                                <i class="fa fa-circle mr-2 text-primary"
                                    style="font-size:11px;"
                                    aria-hidden="true"></i>${obs}
                            </div>`
                                )
                                .join("");
                        } else {
                            // Si es un string simple, lo mostramos tal cual
                            return `<div class="text-left">
                            <i class="fa fa-circle mr-2 text-primary"
                                style="font-size:11px;"
                                aria-hidden="true"></i>${data}
                        </div>`;
                        }
                    }
                    // Caso sin observaciones
                    return `<div class="text-left">
                    <i class="fa fa-circle mr-2 text-danger"
                        style="font-size:11px"
                        aria-hidden="true"></i>No hay observaciones</div>`;
                }
            },
            {
                targets: 5, // Fecha
                render: function (data) {
                    return data ? moment(data).format('DD-MM-YYYY HH:mm') : "Sin fecha";
                }
            },
            {
                targets: -1, // Acciones
                render: function (data, type, row) {
                    return botonesAccionLpp(row);
                }
            }
        ]
    });
}

// Funcion especifica para calcular niveles de riesgo
function calcularNivelRiesgo(totalPuntaje, tipoAtencion) {
    let nivelRiesgo = '';
    let colorRiesgo = '';

    if (tipoAtencion === "3") { // Pediatrico  
        if (totalPuntaje <= 16) {
            nivelRiesgo = 'Riesgo alto';
            colorRiesgo = 'badge badge-danger';
        } else if (totalPuntaje >= 17 && totalPuntaje <= 21) {
            nivelRiesgo = 'Riesgo moderado';
            colorRiesgo = 'badge badge-warning';
        } else {
            nivelRiesgo = 'Riesgo bajo';
            colorRiesgo = 'badge badge-success';
        }
    } else { // Adulto   
        if (totalPuntaje <= 12) {
            nivelRiesgo = 'Riesgo alto';
            colorRiesgo = 'badge badge-danger';
        } else if (totalPuntaje >= 13 && totalPuntaje <= 15) {
            nivelRiesgo = 'Riesgo moderado';
            colorRiesgo = 'badge badge-warning';
        } else {
            nivelRiesgo = 'Riesgo bajo';
            colorRiesgo = 'badge badge-success';
        }
    }

    return { nivelRiesgo, colorRiesgo };
}

// VALIDAR RADIOS ESCALA BRADEN
async function validacionRequeridos() {

    // Obtener total puntaje dinamico
    const puntajeEscalaBraden = obtenerPuntajeTotalBraden();
    // Obtener los datos de urgencia
    const ingresoUrgencia = await obtenerTipoAtencion(nSession.ID_INGRESO_URGENCIA);

    // Verificar que se obtuvo el tipo de atención
    if (!ingresoUrgencia || !ingresoUrgencia.TipoAtencion || !ingresoUrgencia.TipoAtencion.Id) {
        console.error("No se pudo obtener el ID del tipo de atención.");
        return;
    }

    const tipoAtencion = ingresoUrgencia.TipoAtencion.Id;

    // Definir umbrales de riesgo segun tipo atencion
    //const tipoAtencion = nSession.ID_TIPO_ATENCION_URGENCIA;

    let riesgoBajo;
    if (tipoAtencion === 3) { // Pediátrico
        riesgoBajo = 22; // Riesgo bajo en pediátricos es 22 en adelante
    } else { // Adulto
        riesgoBajo = 16; // Riesgo bajo en adultos es 16 en adelante
    }

    // Validamos los campos requeridos en la sección de escala Braden
    if (!validarRadiosRequeridos("#divEscalaBradenAdulto") && !validarRadiosRequeridos("#divEscalaBradenPediatrico")) {
        //toastr.error("Por favor, selecciona una opción en todos los campos requeridos de la Escala Braden.");
        Swal.fire({ icon: 'error', title: 'Escala Braden', text: 'Por favor, selecciona una opción en todos los campos requeridos de la Escala Braden.', timer: 5000 });
        return false;
    }

    // Si puntaje es bajo, las valoraciones y las medidas no son requeridas
    if (puntajeEscalaBraden >= riesgoBajo) {
        // Requiriendo que haya al menos una actividad agregada, sin importar si riesgo es bajo
        const tbl = $(`#tblActividades${(tipoAtencion === 3) ? "Pediatrico" : tipoAtencion === 2 ? "Adulto" : "Adulto"} tbody`);

        // Si tipo de atención es 1 o 3 (adulto o pediátrico) y el perfil es 25, se requieren observaciones
        if ((tipoAtencion === 1 || tipoAtencion === 3) && $(tbl).html().trim() === "" && nSession.CODIGO_PERFIL === 25) {
            Swal.fire({ icon: 'error', title: 'Observaciones', text: 'Debe agregar por al menos una observación.', timer: 5000 });
            return false;
        }

        // Si tipo de atención es 2 (gine) y el perfil es diferente a 25, se requieren observaciones
        if (tipoAtencion !== 2 && $(tbl).html().trim() === "" && nSession.CODIGO_PERFIL !== 25) {
            Swal.fire({ icon: 'error', title: 'Observaciones', text: 'Debe agregar por al menos una observación.', timer: 5000 });
            return false;
        }

        return true;
    }

    // Validación de la valoración de piel y mucosas
    const valoracionRadios = $("input[name^='medidaValoracion']:radio");
    let todasSeleccionadas = true;

    // Crear un conjunto para almacenar los nombres únicos de los grupos de radios
    const gruposRadios = new Set();

    valoracionRadios.each(function () {
        gruposRadios.add($(this).attr("name"));
    });

    // Verificar que todos los grupos tengan al menos un radio seleccionado
    gruposRadios.forEach(grupo => {
        const radiosDelGrupo = $(`input[name='${grupo}']:radio`);
        if (radiosDelGrupo.filter(":checked").length === 0) {
            todasSeleccionadas = false;
        }
    });

    if (!todasSeleccionadas) {
        //toastr.error("Debe seleccionar una opción para cada valoración de piel y mucosas.");
        Swal.fire({ icon: 'error', title: 'Valoración Piel y Mucosas', text: 'Debe seleccionar una opción para cada valoración de piel y mucosas.', timer: 5000 });
        return false;
    }

    // Validacion de las medidas de prevencion
    const medidasRadios = $("input[name^='medidaPrevencion']:radio");
    let medidasSeleccionadas = true;

    // Crear un conjunto para almacenar los nombres únicos de los grupos de radios
    const gruposMedidasRadios = new Set();

    medidasRadios.each(function () {
        gruposMedidasRadios.add($(this).attr("name"));
    });

    // Verificar que todos los grupos de medidas tengan al menos un radio seleccionado
    gruposMedidasRadios.forEach(grupo => {
        const radiosDelGrupo = $(`input[name='${grupo}']:radio`);
        if (radiosDelGrupo.filter(":checked").length === 0) {
            medidasSeleccionadas = false;
        }
    });

    if (!medidasSeleccionadas) {
        //toastr.error("Debe seleccionar una opción para cada medida de prevención.");
        Swal.fire({ icon: 'error', title: 'Medidas de Prevención', text: 'Debe seleccionar una opción para cada medida de prevención.', timer: 5000 });
        return false;
    }

    // Validación de observaciones
    const tbl = $(`#tblActividades${(tipoAtencion === 3) ? "Pediatrico" : tipoAtencion === 2 ? "Adulto" : "Adulto"} tbody`);
    if (tipoAtencion !== 2 && $(tbl).html().trim() === "" && nSession.CODIGO_PERFIL !== 25) {
        Swal.fire({ icon: 'error', title: 'Observaciones', text: 'Debe agregar por al menos una observación.', timer: 5000 });
        return false;
    }

    return true;
}

function validarRadiosRequeridos(containerSelector) {
    let camposInvalidos = 0;

    // Recorremos todos los elementos con data-required="true" dentro del contenedor
    $(`${containerSelector} [data-required="true"]`).each(function () {
        // Verificamos si hay un radio seleccionado dentro de este grupo
        if ($(this).find("input[type='radio']:checked").length === 0) {
            camposInvalidos++;
        }
    });

    // Devolver true si no hay campos inválidos
    return camposInvalidos === 0;
}

// Añadir (*) si es requerido
function actualizarRequeridos(idGrupo, esRequerido) {
    const label = $(`#${idGrupo}`).prev("label");

    if (esRequerido) {
        if (!label.find(".color-error").length) {
            label.append(" <b class='color-error'>(*)</b>");
        }
    } else {
        label.find(".color-error").remove();
    }
}

// FUNCION PARA VALIDAR REQUERIDOS DINAMICAMENTE
function actualizarRequerimientosEscalaBraden(tipoCategorizacion) {
    const esObligatoria = tipoCategorizacion !== "C1" && tipoCategorizacion !== "C2";

    // Actualiza los atributos de los contenedores de radio en Adulto
    $("#divEscalaBradenAdulto .radio-container").each(function () {
        const idGrupo = $(this).attr("id");
        $(this).attr("data-required", esObligatoria);
        if (idGrupo) {
            actualizarRequeridos(idGrupo, esObligatoria);
        }
    });

    // Actualiza los atributos de los contenedores de radio en Pediátrico
    $("#divEscalaBradenPediatrico .radio-container").each(function () {
        const idGrupo = $(this).attr("id");
        $(this).attr("data-required", esObligatoria);
        if (idGrupo) {
            actualizarRequeridos(idGrupo, esObligatoria);
        }
    });
}

// Obtener puntaje total desde la función de calculo para saber si requerir valoraciones y medidas
function obtenerPuntajeTotalBraden() {
    let totalPuntaje = 0;
    $(".escala-braden:checked").each(function () {
        totalPuntaje += parseInt($(this).data("puntaje"));
    });

    // Si no se selecciona radio, el puntaje es 0 (riesgo alto)
    if (totalPuntaje === 0) {
        totalPuntaje = 0;
    }

    return totalPuntaje;
}

function imprimirLpp(idLpp) {
    if (idLpp === null || idLpp === undefined) {
        throw new Error("No se paso id lpp, no se puede realizar el formulario de impresion")
    }
    const url = `${GetWebApiUrl()}URG_Atenciones_urgencia/${idLpp}/LPP/Imprimir`;
    ImprimirApiExterno(url);
}