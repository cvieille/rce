
const TIPO_ARANCEL = { Procedimiento: 3, Imagenologia: 4, Laboratorio: 5 };

$(document).ready(async function () {

    nSession = getSession();

    // Medico o Matrona o enfermero urgencia
    if (nSession.CODIGO_PERFIL == 1 || nSession.CODIGO_PERFIL == 25 || nSession.CODIGO_PERFIL == 22 ||
        nSession.CODIGO_PERFIL == 21 || nSession.CODIGO_PERFIL == 29 || nSession.CODIGO_PERFIL == 3 || nSession.CODIGO_PERFIL == 30) {

        await actualizarProcedimientosSeleccionadosUrg(nSession.ID_INGRESO_URGENCIA)

    }

    $('#mdlCerrarCarteraServicio').on('hidden.bs.modal', function () {
        // Limpiar mensajes de error y estilos de campos requeridos
        validarCampos("#divObservacionesCierreProcedimientoUrg");
    });
});

async function actualizarProcedimientosSeleccionadosUrg(idAtencionUrgencia) {

    const procedimientos = await getProcedimientosUrgencia(idAtencionUrgencia)
    mostrarOcultarIndicacionesUrg(procedimientos, "#divProcedimientos")
    await dibujarProcedimientosSeleccionadosUrg(procedimientos)
}

async function modalProcedimientos(showModal, objAtencionUrgencia) {
    ShowModalCargando(true)

    //if ($("#divListaProcedimientosModal").html() == "") {

    arrayCarteraProcedimientos = GetJsonCartera(TIPO_ARANCEL.Procedimiento);
    arrayProcedimientosFavoritos = getCarteraFavoritos(nSession.ID_INGRESO_URGENCIA, TIPO_ARANCEL.Procedimiento);
    SetHtmlCarteraArancel(arrayCarteraProcedimientos, arrayProcedimientosFavoritos, "divListaProcedimientosModal", TIPO_ARANCEL.Procedimiento);
    // }

    //await showDatosPacienteInicacionMedica(objAtencionUrgencia, "#datosPacienteInicacionMedica")

    if (showModal) {

        $("#tbodyProd tr").remove()

        let numeroFilas = $("#tbodyProd tr").length

        let objTablaNoItem = {
            numeroFilas,
            elemento: "#tbodyProd",
            mensaje: "No hay procedimientos agregados",
            colspan: 4
        }

        mensajeTablaNoItemUrg(objTablaNoItem)

        //#accordionProcedimientoUrgencia
        colapsarElementoDOM("#accordionProcedimientoUrgencia", false)

        mostrarModal("mdlProcedimientos");
    }

    ShowModalCargando(false)
}

function SetHtmlCarteraArancel(arrayCarteraArancel, arrayFavoritos, divCarteraArancelModal, tipoCartera) {

    //Inicializar input filtro para los procedimientos.
    let emptytempl = "No existe el procedimiento especificado ({{query}})";
    let templ = `<span>
                    <span class="{{IdCarteraServicio}}">{{ServicioCartera.Valor}}</span>
                </span>`;
    let source = "GEN_cartera";
    InicializarTypeAhead2("#thProcedimientos", arrayCarteraArancel, emptytempl, templ, source, TIPO_ARANCEL.Procedimiento)
}

function InicializarTypeAhead2(element, arrayCarteraArancel, emptytempl, templ, source, idTipoArancel) {
    $(element).typeahead(setTypeAhead2(arrayCarteraArancel, element, emptytempl, templ, source, idTipoArancel));
    $(element).change(() => $(element).removeData("id"));
    $(element).blur(() => {
        if ($(element).data("id") == undefined)
            $(element).val("");
    });
}

function setTypeAhead2(data, input, emptytempl, templ, source, tipoCartera) {

    var typeAhObject = {
        input: input,
        minLength: 3,
        maxItem: 120,
        maxItemPerGroup: 120,
        hint: true,
        cache: false,
        accent: true,
        emptyTemplate: emptytempl,
        template: templ,
        correlativeTemplate: true,
        source: {
            source: { data }
        },
        callback: {
            onClick: async function (node, a, item, event) {

                let html = ""
                $(input).data('id', item.IdCarteraServicio);

                $(input).data("text-servicio", item.ServicioCartera.Valor)
                $(input).data("id-cartera", item.Aranceles[0].IdCarteraServicioArancel)
                $(input).data("id-tipo-cartera", tipoCartera)

                const objProcedimientos = {
                    IdCarteraServicio: item.Aranceles[0].IdCarteraServicioArancel,
                    DescripcionCarteraServicio: item.ServicioCartera.Valor
                }

                obtenerHtmlProcedimientos(null, objProcedimientos)

                var time = setTimeout(function () {
                    $(input).val("");
                    clearTimeout(time);
                }, 10);
            }
        }
    };

    $(input).unbind().on('input', function () {
        if ($('.typeahead__list.empty').length > 0)
            $(".typeahead__result .typeahead__list").attr("style", "display: none !important;");
        else
            $(".typeahead__result .typeahead__list").removeAttr("style");
    });

    return typeAhObject;
}

function obtenerHtmlProcedimientos(idTipoArancel = null, objeto) {
    let numeroFilas = $("#tbodyProd tr").length

    if (numeroFilas > 0) {
        $("#tbodyProd tr#no-item").remove()
    }

    html = dibujarHtmlFilaProcedimientosUrg(objeto)
    addFilaTablaProcedimientosUrg(tbodyProd, html)
}

function encuentraDuplicadosEnTabla(data) {

    let encontrado = false
    $('#tbodyProd tr').each(function () {
        if ($(this).attr('id') === `tr-procedimientos-urg-${data.IdCarteraServicio}`)
            encontrado = true
        return
    });

    return encontrado
}

function dibujarHtmlFilaProcedimientosUrg(data) {
    // busca procedimiento en la tabla duplicado
    if (encuentraDuplicadosEnTabla(data)) {

        Swal.fire({
            icon: "info",
            title: "Procedimiento duplicado",
            text: "El procedimiento ya se encuentra en la lista"
        });

        return
    }

    const html = `<tr id="tr-procedimientos-urg-${data.IdCarteraServicio}" class="number-procedimiento">
                    <td>
                        <div class="row">
                            <div class="col-md mb-1">
                                    ${data.DescripcionCarteraServicio}
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-md">
                                <textarea id="txtObsProcedimientoUrg-${data.IdCarteraServicio}" 
                                    class="form-control" 
                                    rows="2" 
                                    data-id="${data.IdCarteraServicio}"
                                    placeholder="Escriba observaciones adicionales para este procedimiento" maxlength="500"></textarea>
                            </div>
                        </div>
                    </td>
                    <td class="align-middle text-center">
                        <a class="aQuitarExamen btn btn-danger"
                            onclick="deleteFilaProcedimietosUrg(${data.IdCarteraServicio})"
                            style="color:red; cursor:pointer;"> 
                                <i class="fa fa-trash"></i>
                        </a>
                    </td>
                  </tr>`
    return html
}

function addFilaTablaProcedimientosUrg(tabla, html) {

    $(tabla).append(html)
    //toastr.success("Procedimiento agregado")
}

async function PrepararProcedimientosUrg() {

    let procedimientos = []
    let procedimiento = {}

    let tblProcedimientos = $("#tbodyProd tr td:not(.align-middle)")

    tblProcedimientos.each(function (index, element) {

        let IdCarteraArancel = parseInt($(element).find('textarea').data("id"))
        let ObservacionesSolicitud = $(element).find('textarea').val()

        procedimiento = {
            IdCarteraArancel,
            ObservacionesSolicitud
        }

        procedimientos.push(procedimiento)
    })

    return procedimientos
}

async function deleteFilaProcedimietosUrg(IdCarteraServicio) {

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el procedimiento?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value)
        $(`#tbodyProd tr[id=tr-procedimientos-urg-${IdCarteraServicio}]`).remove()

    let numeroFilas = $("#tbodyProd tr").length

    let objTablaNoItem = {
        numeroFilas,
        elemento: "#tbodyProd",
        mensaje: "No hay procedimientos agregados",
        colspan: 4
    }

    mensajeTablaNoItemUrg(objTablaNoItem)
}


async function guardarProcedimientosUrg() {

    let numeroFilas = 0
    numeroFilas = $("#tbodyProd tr.number-procedimiento").length
    if (numeroFilas === 0) {
        toastr.info("Debe agregar un procedimiento a la lista")
        return
    }

    let procedimientos = await PrepararProcedimientosUrg()
    if (procedimientos.length === 0)
        return

    let parametrizacion = {
        method: "POST",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/procedimientos`
    }

    // Deshabilitar btn
    $("#aAceptarProcedimientosUrg").prop('disabled', true);

    try {
        const response = await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            data: JSON.stringify(procedimientos),
            contentType: 'application/json'
        });

        toastr.success("Procedimientos guardados")
        $("#mdlProcedimientos").modal("hide")

        // habilitar btn en caso de exito
        $("#aAceptarProcedimientosUrg").prop('disabled', false);

        $("#tbodyProd").empty()

        await actualizarProcedimientosSeleccionadosUrg(nSession.ID_INGRESO_URGENCIA)

    } catch (error) {
        console.error('Error en la solicitud:', error);
        throw error;
        // habilitar btn en caso de fallo
        $("#aAceptarProcedimientosUrg").prop('disabled', false);
    }
}

async function getProcedimientosUrgencia(idAtencionUrgencia) {

    if (idAtencionUrgencia === null)
        return

    try {

        const procedimientos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Procedimientos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return procedimientos

    } catch (error) {
        if (error.status === 404 || error.status === 400) {
            return
        }
        console.error("Error al cargar procedimientos de urgencia")
        console.log(JSON.stringify(error))
    }
}

async function getProcedimientoUrgenciaPorId(idAtencionUrgencia, idUrgenciaProcedimiento) {

    if (idAtencionUrgencia === null || idUrgenciaProcedimiento === null)
        return

    try {
        const procedimiento = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/procedimiento/${idUrgenciaProcedimiento}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return procedimiento

    } catch (error) {
        if (error.status === 400) {
            return
        }
        console.error("Error al cargar procedimientos de urgencia")
        console.log(JSON.stringify(error))
    }
}

async function dibujarProcedimientosSeleccionadosUrg(procedimientos) {

    let html = ""

    if (procedimientos === undefined) {
        $("#tblProcedimientos tbody").addClass("text-center")
        html = `<tr>
            <td colspan="5">No hay informacion<td> 
        </tr>`
        $("#tblProcedimientos tbody").append(html)
        return
    }
    else {
        procedimientos.map((p, index) => {
            $("#tblProcedimientos tbody").empty()

            html += `
        <tr class="${pintarFilasAccionRealizadaProcedimiento(p.AccionRealizada)}">
            <td>${index + 1}</td>
            <td>${p.CarteraServicio.Valor}</td>
            <td>${p.ObservacionesSolicitud ?? "Sin Información"}</td>
            <td>${moment(p.FechaIngreso).format('DD-MM-YYYY HH:mm:ss')}</td>
            <td>${p.FechaRealizacion !== null ? moment(p.FechaRealizacion).format('DD-MM-YYYY HH:mm:ss') : ""}</td>
            <td>
                <div class="d-flex flex-row justify-content-center flex-nowrap">
                        ${p.AccionRealizada == null ? botonesAccionProcedimiento(p) : ""}
                        ${p.Acciones.Quitar ? botonEliminarProcedimiento(p) : ""}
                        ${p.FechaRealizacion != null ? botonesVerProcedimiento(p) : ""}
                        ${nSession.CODIGO_PERFIL == perfilAccesoSistema.administradorDeUrgencia && p.AccionRealizada ?
                `<button type="button" class="btn btn-warning"
                data-toggle="tooltip" data-placement="bottom" title="Reversar" onclick='reversarElemento("procedimiento" ,${JSON.stringify(p)})' data-original-title="Reversar"><i class="fas fa-retweet"></i></button>`
                            : ''}
                </div>
            </td>
        </tr>
       `
            $("#tblProcedimientos tbody").append(html)
        })

        const cantidadProcedimientos = procedimientos.map(p => p.AccionRealizada).length
        const cantidadRealizados = await cantidadProcedimientosRealizados(procedimientos)

        if (cantidadProcedimientos === 0) {
            $("#aProcedimientos").addClass("default")
            $("#aProcedimientos").removeClass("warning")
        }
        else if (cantidadProcedimientos === cantidadRealizados && cantidadProcedimientos > 0) {
            $("#aProcedimientos").addClass("success")
            $("#aProcedimientos").removeClass("warning")
        }
        else {
            $("#aProcedimientos").addClass("warning")
            $("#aProcedimientos").removeClass("default")
            $("#aProcedimientos").removeClass("success")
        }

        $("#spProcedimientos").html(`${cantidadRealizados}/${cantidadProcedimientos}`)

        $(`[data-toggle="tooltip"]`).tooltip();
    }
}

function pintarFilasAccionRealizadaProcedimiento(accion) {

    let clase = ""

    if (accion !== null)
        clase = accion === true ? "table-success" : "table-warning"

    return clase
}

async function deleteProcedimiento(IdProcedimiento) {

    if (IdProcedimiento === null || IdProcedimiento === undefined)
        return

    const result = await Swal.fire({
        title: `¿Seguro quieres eliminar el procedimiento?`,
        text: "No se podran revertir los cambios",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    })

    if (result.value) {
        let parametrizacion = {
            method: "DELETE",
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Procedimiento/${IdProcedimiento}`
        }

        await $.ajax({
            type: parametrizacion.method,
            url: parametrizacion.url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success(`Procedimiento eliminado`)
                $("#tblProcedimientos tbody").empty()
                actualizarTablaProcedimientosAtencionClinica()
            },
            error: function (jqXHR, status) {
                console.error(`Error al eliminar el procedimiento en urgencia: ${JSON.stringify(jqXHR)} `)
            }
        })
    }
}

function botonesAccionProcedimiento(procedimiento) {

    // 1 = medico
    // 3 = apoyo clinico
    // 21 = tens de urgencia
    // 22 = enfermeto de urgencia
    // 25 = matroneria urgencia
    // 30 = administrador de urgencia
    // 29 = kinesiologo urgencia
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 3 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22
        || nSession.CODIGO_PERFIL === 25 || nSession.CODIGO_PERFIL === 30 || nSession.CODIGO_PERFIL === 29;

    html = `<a 
                id="btnAccionRealizadaProcedimiento" 
                data-id="${procedimiento.IdProcedimiento}"
                class="btn btn-success m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="${esPerfilAutorizadoEliminar ? `showCerrarProcedimientoUrg(this)` : ''}"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Realizar" 
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                data-original-title="Acción realizada"
                >
                    <i class="fas fa-check fa-xs"></i></a> 
            <a 
                id="btnAccionNoRealizadaProcedimiento" 
                data-id="${procedimiento.IdProcedimiento}"
                class="btn btn-warning m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="${esPerfilAutorizadoEliminar ? `showCerrarProcedimientoUrg(this)` : ''}"
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="No realizar" 
                ${esPerfilAutorizadoEliminar ? '' : 'disabled'}
                data-original-title="Acción No realizada"
                >
                    <i class="fas fa-times fa-xs"></i>
                </a>`

    return html
}

function botonEliminarProcedimiento(procedimiento) {
    // 1 = medico
    // 21 = tens de urgencia
    // 22 = enfermeto de urgencia
    // 25 = matroneria urgencia
    // 30 = administrador de urgencia
    const esPerfilAutorizadoEliminar = nSession.CODIGO_PERFIL === 1 || nSession.CODIGO_PERFIL === 21 || nSession.CODIGO_PERFIL === 22 || nSession.CODIGO_PERFIL === 25 ||
        nSession.CODIGO_PERFIL === 30;

    html = `<a 
                id="btnEliminarProcedimiento" 
                data-id-cartera="" 
                class="btn btn-danger m-1 ${esPerfilAutorizadoEliminar ? '' : 'disabled'}" 
                onclick="deleteProcedimiento(${procedimiento.IdProcedimiento})"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Eliminar"
                
                data-original-title="Quitar"
                >
                    <i class="fa fa-trash fa-xs"></i>
                </a>`
    return html
}

function botonesVerProcedimiento(procedimiento) {

    html = `<a 
                id="btnVerProcedimiento" 
                data-id="${procedimiento.IdProcedimiento}"
                class="btn btn-info m-1" 
                onclick="showCerrarProcedimientoUrg(this)" 
                data-toggle="tooltip" 
                data-placement="bottom" 
                title="Ver" 
                data-original-title="Ver procedimiento"
                >
                <i class="fas fa-eye fa-xs"></i>
           </a>`

    return html
}

function pintaHeaderModalProcedimiento(dataModal) {

    $("#mdlCerrarCarteraServicio .modal-header h4").text(`${dataModal.originalTitle}`)

    if (dataModal.originalTitle === "Acción realizada") {
        $("#mdlCerrarCarteraServicio .modal-header").addClass("modal-header-success").removeClass("modal-header-warning modal-header-info")
        $("#mdlCerrarCarteraServicio .modal-header h4").html(`<i class="fas fa-check"></i> ${dataModal.originalTitle}`)
    }
    else if (dataModal.originalTitle === "Acción No realizada") {
        $("#mdlCerrarCarteraServicio .modal-header").addClass("modal-header-warning").removeClass("modal-header-success modal-header-info")
        $("#mdlCerrarCarteraServicio .modal-header h4").html(`<i class="fas fa-exclamation-triangle"></i> ${dataModal.originalTitle}`)
    }
    else if (dataModal.originalTitle === "Ver procedimiento") {
        $("#mdlCerrarCarteraServicio .modal-header").removeClass("modal-header-warning modal-header-success")
        $("#mdlCerrarCarteraServicio .modal-header").addClass("modal-header-info")
        $("#mdlCerrarCarteraServicio .modal-header h4").html(`<i class="fas fa-eye"></i> ${dataModal.originalTitle}`)
    }
}

async function showCerrarProcedimientoUrg(e) {
    const dataModal = $(e).data();
    let nombreProfesionalRealiza = nSession.NOMBRE_USUARIO;

    const procedimiento = await getProcedimientoUrgenciaPorId(nSession.ID_INGRESO_URGENCIA, dataModal.id);

    const { Nombre, ApellidoPaterno, ApellidoMaterno } = procedimiento.ProfesionalSolicitante;
    const nombreProfesionalSolicitante = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`;

    if (procedimiento.ProfesionalRealiza !== null) {
        const { Nombre, ApellidoPaterno, ApellidoMaterno } = procedimiento.ProfesionalRealiza;
        nombreProfesionalRealiza = `${Nombre} ${ApellidoPaterno} ${ApellidoMaterno ?? ""}`;
    }

    const horaSolicitud = moment(procedimiento.FechaIngreso).format('HH:mm:ss');
    const horaCierre = procedimiento.FechaRealizacion !== null ? moment(procedimiento.FechaRealizacion).format('HH:mm:ss') : "Automática al guardar";
    let badgeRealizado = "";

    $("#txtObservacionesCarteraArancel").val("");

    pintaHeaderModalProcedimiento(dataModal);

    // Determina si el modal se levantó mediante el botón de acción no realizada
    const isAccionNoRealizada = $(e).data('originalTitle') === 'Acción No realizada';

    if (procedimiento.FechaRealizacion === null) {
        if (isAccionNoRealizada) {
            $("#txtObservacionesCarteraArancel").attr("data-required", true);
        } else {
            $("#txtObservacionesCarteraArancel").attr("data-required", false);
        }
        $("#txtObservacionesCarteraArancel").attr("readonly", false);
        $("#aCerrarCarteraArancel").show();
    } else {
        badgeRealizado = procedimiento.AccionRealizada ? `
            <span id="bgTrue" class="badge badge-pill btn-success badge-count ml-2" readonly>
                ${procedimiento.AccionRealizada ? 'Realizado' : ''}
            </span>`
            :
            `<span id="bgFalse" class="badge badge-pill badge-count ml-2" style="color: #ffffff; background-color: orange;" readonly>
                ${!procedimiento.AccionRealizada ? 'No realizado' : ''}
            </span>`;

        $("#txtObservacionesCarteraArancel").attr("data-required", false);
        $("#txtObservacionesCarteraArancel").val(procedimiento.ObservacionesCierre);
        $("#txtObservacionesCarteraArancel").attr("readonly", true);
        $("#aCerrarCarteraArancel").hide();
    }

    $("#divDetalleCarteraArancel").html(`
        <div class="d-flex align-items-center">
            <h5 class="mb-0"><strong>Detalles de Indicación</strong></h5>
            ${badgeRealizado}
        </div>
        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Solicitante:</label> <span class="form-control form-control-no-height" readonly>${nombreProfesionalSolicitante}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de solicitud:</label> <span class="form-control form-control-no-height" readonly>${horaSolicitud}</span>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-8">
                <label>Profesional que cierra:</label>
                <span class="form-control" readonly>${nombreProfesionalRealiza ?? ""}</span>
            </div>
            <div class="col-sm-4">
                <label>Hora de cierre:</label> <span class="form-control form-control-no-height" readonly>${horaCierre}</span>
            </div>
        </div>
        <label>Observaciones al ingreso:</label>
        <textarea 
            id="txtObservacionesAdicionalesUrg" 
            class="form-control" rows="3" 
            placeholder="Escriba observaciones acerca del caso"
            maxlength="500"
            readonly>${procedimiento.ObservacionesSolicitud ?? "Sin información"}
        </textarea>
        <label>Procedimiento:</label>
        <span class="form-control form-control-no-height" readonly>${procedimiento.CarteraServicio.Valor}</span>
    `);

    $("#aCerrarCarteraArancel").attr("data-id");
    $("#aCerrarCarteraArancel").data("id", procedimiento.IdAtencionCarteraArancel);
    $("#aCerrarCarteraArancel").attr("data-realizada");
    $("#aCerrarCarteraArancel").data("realizada", dataModal.originalTitle);

    $("#aCerrarCarteraArancel").unbind().on('click', async function (event) {
        event.preventDefault();

        $(this).prop('disabled', true); // Deshabilita btn

        try {
            //await new Promise(resolve => setTimeout(resolve, 1000));
            realizarAccionProcedimientoUrg(this);
        } catch (error) {
            console.error(error);
        } finally {
            $(this).prop('disabled', false); // Habilita el btn nuevamente después de la acción
        }
    });

    $("#aCerrarCarteraArancel").html(`<i class="fas fa-save"></i> Guardar`);

    $("#mdlCerrarCarteraServicio").modal("show");
}

async function realizarAccionProcedimientoUrg(e) {

    if (!validarCampos("#divObservacionesCierreProcedimientoUrg", false))
        return

    const textoAccion = $(e).data("realizada")

    const IdProcedimiento = $(e).data("id")
    const IdAtencionUrgencia = nSession.ID_INGRESO_URGENCIA
    const ObservacionesCierre = $("#txtObservacionesCarteraArancel").val()
    let AccionRealizada = false

    if (textoAccion === "Acción realizada")
        AccionRealizada = true

    let json = {
        IdProcedimiento,
        IdAtencionUrgencia,
        ObservacionesCierre,
        AccionRealizada,
        textoAccion
    }

    ShowModalCargando(true)
    await patchAccionProcedimientoRealizada(json)
    ShowModalCargando(false)
}

async function patchAccionProcedimientoRealizada(json) {

    const url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${json.IdAtencionUrgencia}/procedimiento/Cerrar`

    let body = {
        IdProcedimiento: json.IdProcedimiento,
        IdAtencionUrgencia: json.IdAtencionUrgencia,
        AccionRealizada: json.AccionRealizada,
        ObservacionesCierre: json.ObservacionesCierre
    }

    $.ajax({
        type: "PATCH",
        url: url,
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: async function (data, status, jqXHR) {
            $("#mdlCerrarCarteraServicio").modal("hide")
            actualizarTablaProcedimientosAtencionClinica()

            if (json.textoAccion === "Acción realizada")
                toastr.success(`Procedimiento realizado`);
            else if (json.textoAccion === "Acción No realizada")
                toastr.warning(`Procedimiento no realizado`);
        },
        error: function (jqXHR, status) {
            console.log("Error al actualizar el procedimiento: " + JSON.stringify(jqXHR));
        }
    });
}

async function cantidadProcedimientosRealizados(procedimientos) {

    const cantidad = procedimientos.map(p => p.AccionRealizada)
        .filter(value => value === true || value === false)
        .length
    return cantidad
}

function imprimirProcedimientosUrg() {

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${nSession.ID_INGRESO_URGENCIA}/Procedimientos/Imprimir`
    ImprimirApiExterno(url)
}

async function actualizarTablaProcedimientosAtencionClinica() {
    const procedimientos = await getProcedimientosUrgencia(nSession.ID_INGRESO_URGENCIA)
    await dibujarProcedimientosSeleccionadosUrg(procedimientos)
}


