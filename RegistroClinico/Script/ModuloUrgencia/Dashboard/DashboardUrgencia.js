﻿let dataSorted = [], PacientesSortedByAge = []
$(document).ready(function () {
    getUrgenciasApi();
    getUrgenciasConPaciente()
});
//funcion que carga las tablas de informacion resumida (index, on ready)
function getUrgenciasApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Dashboard/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            console.log(data.length)
            getUrgenciasPorEstado(data);
            getUrgenciasPorTipoAtencion(data);
            getUrgenciasPorCategorizacion(data);
            getUrgenciasPorClasificacion(data)
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });
    ShowModalCargando(false);
}
function getUrgenciasConPaciente() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Buscar?`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getUrgenciasPorRangoEtario(data)
        }
    })
}


//Info tabla por estado
function getUrgenciasPorEstado(data) {

    let html;
    let cabecera = `<tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblEstadosUrgencia').append(cabecera);

    const atencionesAgrupadas = Object.groupBy(data, e => e.TipoEstado.Valor)
    const atenciones = Object.entries(atencionesAgrupadas)
    const atencionesPorEstado = atenciones.map(([estado, atenciones]) => ({ estado, atenciones }))

    atencionesPorEstado.map(({ estado, atenciones }, i) => {

        const { TipoEstado } = atenciones[0]
        const cantidadAtenciones = atenciones.length

        html = `<tr>
                        <td>${estado}</td>
                        <td>${cantidadAtenciones}</td>
                        <td>
                           <a id="aVerDetalleEstsado_${i}"
                              onclick="getDetalleEstado(${TipoEstado.Id})"
                              class="btn  btn-outline-info"
                           >Ver</a>
                       </td>
                    <tr>`;
        $('#tblEstadosUrgencia').append(html);

        $('#spTotalEstadosUrgencia').text(data.length);
    })
}

//Tabla por tipo de Atencion
function getUrgenciasPorTipoAtencion(data) {

    let html;
    let cabecera = `<tr>
                        <th>Tipo</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblTipoUrgencia').append(cabecera);
    let contador = 0;
    let filtro = [1, 2, 3];
    filtro.forEach(function (key, i) {
        let registros = data.filter(d => d.TipoAtencion.Id == key);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].TipoAtencion.Valor}</td>
                    <td>${registros.length}</td>
                    <td>
                       <a id="aVerDetalleEstado_${i}" onclick="getDetalleTipoAtencion(${key})" class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
            $('#tblTipoUrgencia').append(html);
            contador += registros.length;
        }
        $('#spTotalTipoUrgencia').text(contador);
    });
}
//Tabla por categorizacion
//function getUrgenciasPorCategorizacion(data) {
//    let html;
//    let cabecera = `<tr>
//                        <th>Categorizacion</th>
//                        <th>Cantidad</th>
//                        <th>Detalle</th>
//                    <tr>`;
//    $('#tblCategorizacionUrgencia').append(cabecera);
//    let contador = 0;
//    let filtro = ["C1", "C2", "C3", "C4", "C5"];
//    filtro.forEach(function (key, i) {

//        let registros = data.filter(d => (d.Categorizacion != null ? d.Categorizacion.Codigo : "") == key);


//        if (registros.length > 0) {
//            //getDetalleUrgenciaApi(${key})
//            html = `<tr>
//                    <td>${registros[0].Categorizacion.Codigo}</td>
//                    <td>${registros.length}</td>
//                    <td>
//                       <a id="aVerDetalleEstsado_${i}" onclick=getDetalleCategorizados("${key}")
//                            class="btn  btn-outline-info">Ver</a></td>
//                <tr>`;
//            $('#tblCategorizacionUrgencia').append(html);
//            contador += registros.length;
//        }

//        $('#spTotalCategorizados').text(contador);
//    });
//}


function getUrgenciasPorCategorizacion(data) {
    let html;
    let cabecera = `<tr>
                        <th>Categorizacion</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblCategorizacionUrgencia').append(cabecera);

    const agrupados = Object.groupBy(data, c => c.Categorizacion?.Codigo)
    const atenciones = Object.entries(agrupados)
    const filtrados = atenciones.filter(([categorizacion]) => categorizacion !== 'undefined')

    let total = 0

    filtrados.map(([categorizacion, atenciones], i) => {

        total += atenciones.length

        const { Categorizacion } = atenciones[0]

        const cantidadAtenciones = atenciones.length

        html = `<tr>
                    <td>${categorizacion}</td>
                    <td>${cantidadAtenciones}</td>
                    <td>
                       <a id="aVerDetalleEstsado_${i}" onclick=getDetalleCategorizados("${Categorizacion.Codigo}")
                            class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
        $('#tblCategorizacionUrgencia').append(html);

        $('#spTotalCategorizados').text(total);
    })
}

//Tabla por clasificacion
function getUrgenciasPorClasificacion(data) {

    let html;
    contador = 0
    let cabecera = `<tr>
                        <th>Clasificacion</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblClasificacionUrgencia').append(cabecera);

    //Agrupados por tipo de clasificacion
    const agrupados = Object.entries(Object.groupBy(data, a => a.TipoClasificacion?.Id ?? 0)).map(([key, value]) => ({
        Clasificacion: Number(key),
        Casos: value,
        Valor: value[0].TipoClasificacion?.Valor ?? null
    }));

    // Tipos de atencion
    const tiposAtencion = agrupados.map(({ Clasificacion, Valor }) => ({
        Id: Clasificacion,
        Valor: Valor
    }));

    if (agrupados.length > 0) {
        for (let item of agrupados) {

            let clasificacion = ""


            html = `<tr>
                    <td>${tiposAtencion.find(z => z.Id == item.Clasificacion).Valor ?? "Sin clasificación"}</td>
                    <td>${item.Casos.length}</td>
                    <td>
                       <a id="aClasificacion" onclick=getDetalleClasificacion(${item.Clasificacion})
                            class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
            $('#tblClasificacionUrgencia').append(html);
            contador += item.Casos.length
        }
    }
    $('#spTotalCClasificacion').text(contador);
}
function getUrgenciasPorRangoEtario(data) {
    let html, years, contador = 0, index = 0
    PacientesSortedByAge = [
        {
            RangoEtario: "Menores 1 año",
            Pacientes: []
        },
        {
            RangoEtario: "1 - 4 años",
            Pacientes: []
        },
        {
            RangoEtario: "5 - 9 años",
            Pacientes: []
        },
        {
            RangoEtario: "10 - 14 años",
            Pacientes: []
        },
        {
            RangoEtario: "15 - 19 años",
            Pacientes: []
        },
        {
            RangoEtario: "20 - 24 años",
            Pacientes: []
        },
        {
            RangoEtario: "25 - 64 años",
            Pacientes: []
        },
        {
            RangoEtario: "Mayores de 65 años",
            Pacientes: []
        },
        {
            RangoEtario: "Sin clasificar",
            Pacientes: []
        }
    ]

    data.map(e => {
        years = e.Paciente.Edad.años

        if (years < 1) {
            PacientesSortedByAge[0].Pacientes.push(e)
        } else if (years >= 1 && years < 4) {
            PacientesSortedByAge[1].Pacientes.push(e)
        } else if (years >= 4 && years < 9) {
            PacientesSortedByAge[2].Pacientes.push(e)
        } else if (years >= 9 && years < 14) {
            PacientesSortedByAge[3].Pacientes.push(e)
        } else if (years >= 14 && years < 19) {
            PacientesSortedByAge[4].Pacientes.push(e)
        } else if (years >= 19 && years < 24) {
            PacientesSortedByAge[5].Pacientes.push(e)
        } else if (years >= 24 && years < 64) {
            PacientesSortedByAge[6].Pacientes.push(e)
        } else if (years >= 64) {
            PacientesSortedByAge[7].Pacientes.push(e)
        } else {
            PacientesSortedByAge[8].Pacientes.push(e)
        }
    })
    let cabecera = `<tr>
                        <th>Rango etario</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblEdadUrgencia').append(cabecera);
    for (let item of PacientesSortedByAge) {
        html = `<tr>
                    <td>${item.RangoEtario}</td>
                    <td>${item.Pacientes.length}</td>
                    <td>
                       <a id="aClasificacion" onclick=getDetalleEdad(${index})
                            class="btn  btn-outline-info">Ver</a></td>
                <tr>`;
        $('#tblEdadUrgencia').append(html);
        contador += item.Pacientes.length
        index++
    }
    $('#spTotalEdad').text(contador);

}
function getDetalleEdad(indice) {
    let data = PacientesSortedByAge[indice].Pacientes, adataset = []
    $.each(data, function (key, val) {
        adataset.push([
            val.Id,
            //val.IdTipoEstadoSistema,
            val.Paciente.Identificacion.Valor,
            val.Paciente.NumeroDocumento,
            (val.Paciente.Nombre + " " ?? "") +
            (val.Paciente.ApellidoPaterno ?? "") + " " +
            (val.ApellidoMaterno ?? ""),
            val.NombreTipoAtencion,
            val.Categorizacion != null ? val.Categorizacion.Codigo : "S/C",
            val.TipoEstadoSistema.Valor,
            moment(val.FechaAdmision).format('DD-MM-YYYY HH:mm:ss')
        ]);
    });
    ObtenerTablaDetalles(adataset)
}

const filas = 200
function getDetalleEstado(id) {
    let parametro = `idTipoEstadoSistemas=${id}&filas=${filas}`;
    getDetalleAtencionUrgenciaApi(parametro)
}

function getDetalleTipoAtencion(id) {
    let parametro = `IdTipoAtencion=${id}&idTipoEstadoSistemas=100&idTipoEstadoSistemas=102&idTipoEstadoSistemas=103&idTipoEstadoSistemas=104&idTipoEstadoSistemas=105&filas=${filas}`;
    getDetalleAtencionUrgenciaApi(parametro)
}
function getDetalleCategorizados(codigo) {
    let parametro = `codigoCategorizacion=${codigo}&idTipoEstadoSistemas=102&idTipoEstadoSistemas=103&idTipoEstadoSistemas=104&filas=${filas}`;
    getDetalleAtencionUrgenciaApi(parametro)
}
function getDetalleClasificacion(idClasificacion) {

    if (idClasificacion === 0)
        idClasificacion = -1
    let parametro = `idTipoEstadoSistemas=100&idTipoEstadoSistemas=102&idTipoEstadoSistemas=103&idTipoEstadoSistemas=104&idTipoEstadoSistemas=105&idTipoClasificacion=${idClasificacion}&filas=${filas}`
    getDetalleAtencionUrgenciaApi(parametro)
}
function getDetalleAtencionUrgenciaApi(parametros) {

    ShowModalCargando(true)

    let url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Buscar?${parametros}`

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            var adataset = [];

            $.each(data, function (key, val) {

                let tipoPrioridad = val.TipoPrioridadPaciente && val.TipoPrioridadPaciente.TipoPrioridadPaciente
                    ? val.TipoPrioridadPaciente.TipoPrioridadPaciente.Valor
                    : ''; // Valor predeterminado en caso de null

                adataset.push([
                    val.Id,
                    val.Paciente.Identificacion.Valor,
                    val.Paciente.NumeroDocumento,
                    val.Paciente.Nombre === null ? "" : (val.Paciente.Nombre + " " ?? "") + (val.Paciente.ApellidoPaterno ?? "") + " " + (val.ApellidoMaterno ?? ""),
                    val.NombreTipoAtencion,
                    val.Categorizacion != null ? val.Categorizacion.Codigo : "S/C",
                    val.TipoEstadoSistema.Valor,
                    tipoPrioridad,
                    val.FechaAdmision,
                    moment(val.FechaAdmision).format('DD-MM-YYYY HH:mm:ss'),                 
                ]);

                ShowModalCargando(false)
            });
            ObtenerTablaDetalles(adataset)
        }
    });
}

function ObtenerTablaDetalles(adataset) {
    $('#tblDetalle').DataTable({
        data: adataset,
        order: [],
        columnDefs: [
            {
                targets: [8],
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    let dateOne = new Date(adataset[fila][8]);
                    let dateTwo = new Date();

                    let msdiferencia = dateTwo - dateOne;
                    let minutos = Math.floor(msdiferencia / 1000 / 60);
                    let horas = 0;
                    if (minutos > 60) {
                        horas = Math.floor(minutos / 60);
                        minutos = minutos - (horas * 60);
                    }

                    return (horas < 10 ? "0" + horas : horas) + ":" + (minutos < 10 ? "0" + minutos : minutos);
                }
            },
            {
                targets: -1,
                render: function (data, type, row, meta) {
                    let idAtencionUrgencia = adataset[meta.row][0];
                    let nombrePaciente = adataset[meta.row][3]; // Capturando el nombre del pacietne

                    // Solo pasa Nombre al botón
                    return `<button class="btn btn-info btn-sm print-btn" data-id="${idAtencionUrgencia}" data-name="${nombrePaciente}" type="button"> <i class="fa fa-print"></i> </button>`;
                }
            }
        ],
        columns: [
            { title: 'DAU' },
            //{ title: 'Fecha DAU' },
            //{ title: 'IdTipoEstadoSistema' },
            { title: 'Dcto' },
            { title: 'Número' },
            { title: 'Nombre' },
            { title: 'Tipo' },
            { title: 'Categorización' },
            { title: 'Estado' },
            { title: 'Prioridad' },
            { title: 'Tiempo HH:MM' },
            { title: '' }
        ],
        bDestroy: true
    });

    $("#mdlDetalle").modal("show");

    // Manejar el evento de clic en el botón de impresión
    $('#tblDetalle').on('click', '.print-btn', function () {
        let idAtencionUrgencia = $(this).data('id');
        let nombrePaciente = $(this).data('name');

        // Llamar a la función para abrir el modal de impresión
        abrirModalImprimirUc(idAtencionUrgencia, nombrePaciente);

        // Ocultar el modal de detalles
        $("#mdlDetalle").modal("hide");
    });
}


