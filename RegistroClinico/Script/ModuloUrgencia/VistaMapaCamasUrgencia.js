﻿let tableACargar;
$(document).ready(function () {
    ShowModalCargando(false);
    sSession = undefined
    sSession = getSession();

    cargarComboTipoAtencion();

    const selectTipoAtencion = document.querySelector('#selectTipoAtencion')
    const btnRefrescar = document.querySelector('#btnRefrescar')


    if (sSession.ID_PERFIL !== 149)
        $("#btnMisATencionesMedicas").addClass("d-none")

    // AQUI CONTROLAR SI ES DISTINTO DE NULL Y VER LO DEL TOASTR ERROR AL TRASLADAR PACIENTE Y TRAER EL TIPO DE ATENCION QUE YA TENIA EL PACIENTE
    if (selectTipoAtencion) { // Verifica si el elemento existe
        selectTipoAtencion.addEventListener('change', function () {
            const value = this.value;
            // Controlar si el valor es distinto de null
            if (value !== null && value !== undefined) {
                setSession('ID_TIPO_ATENCION_URGENCIA', value);
            }
        });

        if (sSession.ID_TIPO_ATENCION_URGENCIA !== undefined) {
            selectTipoAtencion.value = sSession.ID_TIPO_ATENCION_URGENCIA;
        } else {
            selectTipoAtencion.value = 1;
        }

        if (sSession.ID_PERFIL == 206) {
            selectTipoAtencion.value = 2;
        }

        setTimeout(() => {
            if (btnRefrescar) { // Verifica si el botón existe antes de hacer clic
                btnRefrescar.click();
            }
        }, 200);

        $("#selectTipoAtencion").on('change', function () {
            let idTipoBusqueda = $("#selectTipoAtencion").val();
            let esValido = validarCampos(("#DivSelectTipoAtencion"), $("#selectTipoAtencion"));
            if (esValido) {
                $("#infoInicial").hide();
            }
            cargarCamasUrgencia(idTipoBusqueda, true);
        });
    } else {
        console.warn("El elemento selectTipoAtencion no existe en el DOM.");
    }

    if (btnRefrescar) {
        $("#btnRefrescar").click(function () {
            let idTipoBusqueda = $("#selectTipoAtencion").val();
            if (idTipoBusqueda && idTipoBusqueda !== "0") {
                cargarCamasUrgencia(idTipoBusqueda, true);
            }
        });
    }

    $('#btnIndicacionesPendientes').click(function () {
        const idTipoAtencion = $("#selectTipoAtencion").val()

        procedimientosPendientesUrg(idTipoAtencion);
        medicamentosPendientesUrg(idTipoAtencion);
        cargarExamenesPendientesMapaCamas(idTipoAtencion)
        cargarSolicitudesInterconsultorPendientes(idTipoAtencion)
        cargarAtencionesConTomaSignosVitalesPendientes(idTipoAtencion)
        $("#mdlIndicacionesPendientes").modal("show")
    });

    $(`#btnMisATencionesMedicas`).click(function () {
        if (sSession.ID_PERFIL == 149 && sSession.ID_PROFESIONAL !== null) {

            
            promesaAjax("GET", `URG_Atenciones_Urgencia/Atenciones/${sSession.ID_PROFESIONAL}/Medico`).then(res => {
                $("#tblMisAtencionesMedicas").DataTable({
                    data: res,
                    destroy: true,
                    columns: [
                        { title: "Id DAU", data: "IdAtencionesUrgencia" },
                        { title: "N° documento", data: "Paciente" },
                        { title: "Paciente", data: "Paciente" },
                        { title: "Diagnosticos", data: "Diagnosticos" },
                        { title: "Anamnesis", data: "Anamnesis" },
                        { title: "Ubicacion", data: "Ubicacion" },
                        { title: "Box", data: "Box" },
                        { title: "Examen fisico", data: "ExamenFisico" },
                        { title: "", data: "IdAtencionesUrgencia" }
                    ], columnDefs: [
                        {
                            targets: 2,
                            render: function (Paciente) {
                                if (Paciente !== null)
                                    return `${Paciente.Nombre ?? ""} ${Paciente.ApellidoPaterno ?? ""} ${Paciente.ApellidoMaterno ?? ""}`
                            }
                        },{
                            targets: 1,
                            render: function (Paciente) {
                                if (Paciente !== null)
                                    return Paciente.NumeroDocumento
                            }
                        },{
                            targets: -1,
                            render: function (idDau) {
                                return `
                                    <div class="d-inline-flex">
                                        <button class="btn btn-outline-primary btn-sm" type="button"
                                            data-toggle="tooltip" data-placement="right" title="Ver más"
                                            onclick="botonVerMasDesdeMisAtenciones(${idDau})"
                                            style="margin-right: 5px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-outline-success btn-sm" onclick="irAtencionClinica(${idDau})" type="button"
                                            data-toggle="tooltip" data-placement="right" title="Ir atención clínica">
                                            <i class="fa fa-file"></i>
                                        </button>
                                    </div>
                                `;
                            }
                        },{
                            targets: 3,
                            render: function (Diagnosticos) {
                                if (Diagnosticos !== null && Diagnosticos.length > 0)
                                    return `${Diagnosticos[0].Valor}`
                                else
                                    return `No se han ingresado hipótesis diagnóstica.`

                            }
                        },
                        {
                            targets: [-3,-4],
                            render: function (item) {
                                return item!==null?item.Valor??`No asignado`:`No asignado`
                            }
                        }
                    ]
                })
                $(mdlMisAtencionesMedicas).find(".nombreUsuarioModal").text(sSession.NOMBRE_USUARIO);
                $("#mdlMisAtencionesMedicas").modal("show")
            }).catch(({ xhr }) => {
                if (xhr.status === 404) {
                    Swal.fire({
                        icon: 'info',
                        title: 'Sin atenciones pendientes',
                        text: 'No se encontraron atenciones clinicas pendientes'
                    });

                    return
                }

                console.error("Ocurrio un error al intentar obtener las atenciones del medico")

            })
        }
    });

    $(document).on('click', function (event) {
        if (!$(event.target).closest('.modal-content').length) {
            $('#mdlIndicacionesPendientes').modal('hide');
        }
    });

    // Función para recargar el mapa de camas
    function recargarMapaCamas() {
        let idTipoBusqueda = $("#selectTipoAtencion").val();
        if (idTipoBusqueda && idTipoBusqueda !== "0") {
            cargarCamasUrgencia(idTipoBusqueda, true);
        } else {
            toastr.error("Debe seleccionar un tipo de búsqueda válido.");
        }
    }

    // Variables para controlar la inactividad
    let temporizadorRecarga;
    let tiempoInactividad = 3 * 60 * 1000; // 3 minutos en milisegundos
    let ultimaActividad = Date.now();

    function verificarInactividad() {
        if (Date.now() - ultimaActividad >= tiempoInactividad) {
            recargarMapaCamas();
        }
    }

    // Función para reiniciar la verificación de inactividad
    function reiniciarTemporizadorInactividad() {
        ultimaActividad = Date.now();
        clearTimeout(temporizadorRecarga);
        temporizadorRecarga = setTimeout(verificarInactividad, tiempoInactividad);
    }

    // Reiniciar el temporizador en cada movimiento o clic del mouse
    $(document).on('mousemove click', function () {
        reiniciarTemporizadorInactividad();
    });

    // Inicializar el temporizador al cargar la página
    reiniciarTemporizadorInactividad();

    if (sSession.ID_ATENCION_URGENCIA != undefined) {
        abrirModalImprimir(sSession.ID_ATENCION_URGENCIA);
        deleteSession("ID_ATENCION_URGENCIA");
    }

    

});

function botonVerMasDesdeMisAtenciones(idDau) {
    $("#mdlMisAtencionesMedicas").modal("hide")
    setTimeout(function () {
        verInformacionAtencionUrgencia(idDau);
    }, 100);
}
function cargarAtencionesConTomaSignosVitalesPendientes(idTipoAtencion) {
    promesaAjax(`GET`, `URG_Atenciones_Urgencia/${idTipoAtencion}/signos/Pendientes`).then(res => {
        $("#tblSignosVitalesPendientes").DataTable({
            data: res,
            destroy:true,
            columns: [
                { title: "Id Dau", data: "IdDau" },
                { title: "Paciente", data: "Paciente" },
                { title: "Categorizacion", data:"Categorizacion"},
                { title: "Ubicacion", data:"Ubicacion"},
                { title: "Estado", data:"Estado"},
                { title: "Acciones", data: "IdDau" }
            ], columnDefs: [
                {
                    targets: 1,
                    render: function (paciente) {
                        return `${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}`
                    }
                },{
                    targets: 2,
                    render: function (categorizacion) {
                        if (categorizacion == null)
                            return `N/A`
                        return dibujarIconoCategorizacion(categorizacion, {
                            heading: "h4",
                            badge: ""
                        })
                    }
                 },{
                    targets: [3,4],
                    render: function (elemento) {
                        if (elemento == undefined || elemento == null)
                            return `No registra`

                        return elemento.Valor??"N/A"
                    }
                },{
                    targets: -1,
                    render: function (iddau) {
                        return `<button class="btn btn-outline-success btn-sm" type="button" onclick="irAtencionClinica(${iddau})">
                                <i class="fa fa-file"></i><br>Atención clínica
                            </button>`
                    }
                }

            ]

        })
    }).catch(error => {
        console.error("Error al cargar los signos vitales pendientes")
        console.log(error)
    })
}

function cargarSolicitudesInterconsultorPendientes(idTipoAtencion) {
    //si no se le pasa el id atencion a la funcion
    if (idTipoAtencion == null)
        idTipoAtencion = parseInt($("#selectTipoAtencion").val())

    promesaAjax(`GET`, `URG_Atenciones_Urgencia/${idTipoAtencion}/Interconsultor/Pendientes`).then(res => {
        $("#tblInterconsultorPendientes").DataTable({
            data: res,
            destroy: true,
            columns: [
                { title: "Id", data: "Id" },
                { title: "Numero documento", data: "Paciente" },
                { title: "Paciente", data: "Paciente" },
                { title: "Ubicacion", data: "CamaActual", render: function (ubicacion) { return ubicacion !== null ? ubicacion : `` } },
                { title: "Fecha solicitud", data: "Fecha", render: function (fecha) { return moment(fecha).format("DD-MM-YYYY HH:mm") } },
                { title: "Especialidad", data: "DescripcionTipoConsultor" },
                { title: "Actividad solicitada", data: "ActividadSolicitada" },
                { title: "Solicitante", data: "ProfesionalSolicita" },
                { title: "Ver", data: "IdAtencionUrgencia" },
            ], columnDefs: [
                {
                    targets: 1,
                    render: function (paciente) {
                        if (paciente !== null) {
                            return `${paciente.NumeroDocumento}-${paciente.Digito}`
                        }
                    }
                },
                {
                    targets: 2,
                    render: function (paciente) {
                        if (paciente != null) {
                            return `${paciente.Nombre ?? ""} ${paciente.ApellidoPaterno ?? ""} ${paciente.ApellidoMaterno ?? ""}`
                        }
                    }
                },
                {
                    targets: -2,
                    render: function (profesional) {
                        if (profesional == null || profesional == undefined) 
                            return `No registra profesional`

                        return `${profesional.Valor ?? '' }`
                        
                    }
                },
                {
                    targets: -1,
                    render: function (idAtencionUrgencia) {
                        return `<a onclick="irAtencionClinica(${idAtencionUrgencia})" class="btn btn-info">
                                <i class="fas fa-eye fa-xs"></i>
                            </a>`
                    }
                }
            ]
        })
    }).catch(error => {
        console.error("Error al listar solicituddes de interconsultor pendientes")
        console.log(error)
    })
}


async function cargarExamenesPendientesMapaCamas(idTipoAtencion) {
    let examenesLaboratorioOrdenadosParaTabla = [], examenesImagenOrdenadosParaTabla
    //Obtene examenes pendientes desde api
    examenesLaboratorioOrdenadosParaTabla = prepararListadoExamenesTablaPendientes(await examenesLaboratorioPendientes(idTipoAtencion))
    examenesImagenOrdenadosParaTabla = prepararListadoExamenesTablaPendientes(await examenesImagenologiaPendientes(idTipoAtencion))
    //columnas de la tabla
    let columnas = obtenerColumnasExamenesPendientes()
    //Render de columnas 
    let columnasDefs = obtenerColumnasDefsExamenesPendientes()
    //cargar tablas de examenes pendientes
    dibujarTablaExamenesIngresados(examenesLaboratorioOrdenadosParaTabla, "#tblExamenLaboratorioPendientes", columnas, columnasDefs)
    dibujarTablaExamenesIngresados(examenesImagenOrdenadosParaTabla, "#tblExamenImagenPendientes", columnas, columnasDefs)
}

function cancelarMovimientoPaciente() {
    deleteSession('ID_DAU')
    deleteSession('ID_PACIENTE')
    deleteSession('ID_TIPO_ATENCION')
    $("#divTrasladandoPaciente").slideUp()
    sSession = undefined
}

async function cargarCamasUrgencia(idBusqueda, collapse = false) {
    ShowModalCargando(true);
    if (idBusqueda) {
        $("#infoInicial").hide();
        var arrayBoxesPadre = [];
        var arrayBoxeshijo = [];
        var url = GetWebApiUrl() + "URG_Atenciones_Urgencia/Mapa/" + idBusqueda;
        await $.ajax({
            type: 'GET',
            url: url,
            success: function (data, status, jqXHR) {
                // Extrae las camas y crea un arreglo independiente.
                arrayBoxeshijo = data.map((e) => {
                    return e['Box'];
                });
                // Dibuja los DAU de atención
                printBoxes(data);
                // Dibuja las camas de cada DAU de atención
                if (collapse) $('[data-toggle="collapse"]').each(function (index, item) { $(this).trigger("click"); });
                ShowModalCargando(false);
            },
            error: function (jqXHR, status) {
                ShowModalCargando(false);
                console.log("Error al Cargar Mapa de Camas: " + JSON.stringify(jqXHR));
                $("#infoInicial").show();
                if (!evitarToastr) { // Solo muestra toastr si evitarToastr es false
                    toastr.error("Error al cargar camas.");
                }
            }
        });
    } else {
        if (!evitarToastr) {
            toastr.error("Debe seleccionar un tipo de búsqueda.");
        }
        ShowModalCargando(false);
    }
}
function cargarComboTipoAtencion() {
    setCargarDataEnCombo(`${GetWebApiUrl()}URG_Tipo_Atencion/Combo`, false, "#selectTipoAtencion")
    $('#selectTipoAtencion option[value="0"]').remove();

}

function printBoxes(data) {

    var boxesAtencion = "";
    var boxesHijos = "";
    data.map((d) => {

        let camasOcupadas = 0;
        let camasDisponibles = 0;
        let camasDeshabilitadas = 0;
        let camasBloqueadas = 0;
        d.Boxes.forEach(box => {
            switch (box.Estado.Id) {
                case 84:
                    camasDisponibles++;
                    break;
                case 85:
                    camasOcupadas++;
                    break;
            }
        })

        boxesAtencion += `
                        <div class="col-md-12" >
                            <div class="card card-primary " >
                                <div class='card-header ' style="cursor:pointer;">
                                <div class="row">
                                    <div class="col-md-10">
                                             <a
                                                 class="w-100"
                                                 id="boxPadre`+ d.Id + `"
                                                 data-toggle="collapse"
                                                 href="#BoxHijo`+ d.Id + `"
                                                 aria-expanded="false"
                                                 aria-controls="collapseOne"><h4>` + d.Valor + `</h4>
                                              </a>
                                    </div>
                                    <!--*****conteo de camas******-->

                                    <div class="col-md-2 ">
                                        <div class="d-inline">
                                            Disponibles: <span class="badge badge-success">${camasDisponibles}</span> |
                                            Ocupadas: <span class="badge badge-ocupada">${camasOcupadas}</span> 
                                        </div>
                                    </div>
                                </div>
                                <!-- ***Fin conteo de camas ***-->
                                </div>
                                <div class="card-body  collapse show" id="BoxHijo`+ d.Id + `" >
                                    <div class="row"> 
                                        ${printBoxCamillasAtencion(d.Boxes, d.Id)}
                                    </div>
                                </div>
                            </div>
                        </div>`;

        $("#ContCamasUrg").html(boxesAtencion);

    });

}

function printBoxCamillasAtencion(boxes, idTipoBox) {

    if (boxes.length == 0)
        return `<h4> <i class="fa fa-info"></i> No se encontraron box de atención</h4>`
    let contenido = ``
    boxes.forEach(box => {
        contenido += `<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 ${box.Estado.Id == 84 ? `boxdisponible` : `boxocupado`}">
        <div class="card ">
            <div class="card-header bg-dark">
            <div class="row">
                <div class="col-md-8">
                ${box.Valor}
                </div>
                <div class="col-md-4">
                 ${dibujarEstado(box.Estado.Id)}
                </div>
            </div>
            </div>
            <div class="card-body" id="divBoxPaciente-${box.Id}">
                ${dibujarInformacionPaciente(box, idTipoBox)}
            </div>
        </div>
        </div>`
    })
    return contenido
}

function ocultarCamas() {
    let boxesDisponibles = [...$(".boxdisponible")]
    boxesDisponibles.forEach((box, index) => {
        $(box).fadeOut()
    })
}

function dibujarEstado(estado) {
    let clase = `light`, estadoString = `Sin estado`, icon = ``
    switch (estado) {
        case 84:
            clase = `success`, estadoString = `Disponible`, icon = `fa fa-check`
            break
        case 85:
            clase = `warning`, estadoString = `Ocupado`, icon = `fa fa-user`
            break
    }
    return `<span class="badge badge-${clase}">${estadoString} <i class="${icon}"></i></span>`
}

function dibujarCategorizacion(codigoCategorizacion) {

    color = `dark`
    switch (codigoCategorizacion) {
        case "C1":
            color = `danger`
            break
        case "C2":
            color = `orange bg-orange text-white`
            break
        case "C3":
            color = `warning`
            break
        case "C4":
            color = `success`
            break
        case "C5":
            color = `primary`
            break
    }
    return `<span class="badge badge-${color}" style="color:white !important;">${codigoCategorizacion ?? `SC`}</span>`


}

// Función para obtener el nombre del médico tratante
function getMedicoTratante(idAtencionUrgencia) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Medico`,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                // Ordena los datos por fecha descendente para obtener el más reciente primero
                data.sort((a, b) => new Date(b.Fecha) - new Date(a.Fecha));

                // Devuelve solo el primer registro (el más reciente)
                resolve(data.length > 0 ? data[0] : null);
            },
            error: function (xhr, status, error) {
                reject(error);
            }
        });
    });
}

function getTipoPrioridad(idAtencionUrgencia) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}`,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                const tipoPrioridad = data.TipoPrioridadPaciente && data.TipoPrioridadPaciente.TipoPrioridadPaciente
                    ? data.TipoPrioridadPaciente.TipoPrioridadPaciente.Valor
                    : '';
                resolve(tipoPrioridad);
            },
            error: function (xhr, status, error) {
                reject(error);
            }
        });
    });
}

// Función para dibujar la información del paciente
function dibujarInformacionPaciente(data, idTipoBox) {
    if (data.Atencion == null) {
        return `
            <div class="row">
                <div class="col-12">
                    <h5 class="text-center"><i class="fa fa-info-circle"></i>  Box de atención sin paciente.</h5>
                </div>
                <div class="col-12">
                    <button class="btn btn-outline-primary w-100" type="button" onclick='buscarPacientesParaBox(${JSON.stringify(data)}, ${idTipoBox})'>
                        Asignar paciente a este box <i class="fa fa-search"></i> <i class="fa fa-user"></i>
                    </button>
                </div>
            </div>`;
    } else {
        let { Paciente, Atencion, Categorizacion, TipoAcompañante } = data.Atencion;
        const { IdCamaTipoBox } = data;
        const { IdTipoEstado, NombreTipoEstado, TiempoTranscurrido } = Atencion;
        const [horas, minutos, segundos] = TiempoTranscurrido.split(':');
        const { Nombre, NombreSocial, ApellidoPaterno, ApellidoMaterno, NumeroDocumento, Digito, FechaNacimiento, Sexo, Genero, Edad } = Paciente;
        const pintarNombreSocial = NombreSocial ? `<h5><span class="badge table-success ">Responde al nombre de: <span class="font-weight-normal">${NombreSocial}</span></span></h5>` : '';
        const pintarTipoPrioridad = `<h5><span id="tipoPrioridad-${Atencion.Id}" class="badge badge-info" style="white-space: normal;"></span></h5>`;
        const pintarMedicoTratante = `<b class="mr-1">Médico tratante:</b> <span id="medicoTratante-${Atencion.Id}"></span>`;

        // Llamar a getMedicoTratante para obtener el médico tratante más reciente
        getMedicoTratante(Atencion.Id)
            .then(ultimoProfesional => {
                const nombreUltimoProfesional = ultimoProfesional ? ultimoProfesional.Profesional.Valor : 'No disponible';
                document.getElementById(`medicoTratante-${Atencion.Id}`).innerText = nombreUltimoProfesional;
            })
            .catch(error => {
                document.getElementById(`medicoTratante-${Atencion.Id}`).innerText = 'No disponible';
            });

        // Llamar a getTipoPrioridad para obtener el tipo de prioridad
        getTipoPrioridad(Atencion.Id)
            .then(tipoPrioridad => {
                document.getElementById(`tipoPrioridad-${Atencion.Id}`).innerText = tipoPrioridad;
            })
            .catch(error => {
                document.getElementById(`tipoPrioridad-${Atencion.Id}`).innerText = 'Sin prioridad';
            });

        return `
        <div class="row">
            <div class="col-12">
                <div class="card border border-dark p-2">
                    <div class="row d-flex flex-row overflow-hidden">
                        <div class="col-12 col-4 d-flex justify-content-center justify-content-md-start">
                            ${Categorizacion.length > 0
                ? `<h1>${dibujarCategorizacion(Categorizacion[0].Codigo)}</h1>`
                : `<h1><span class="badge badge-dark">SC</span></h1>`
            }    
                        </div>
                        <div class="col-12 col-8 mt-1 d-flex flex-wrap justify-content-center justify-content-md-start">
                            <h5><span class="badge ${Categorizacion.length === 0 ? 'badge-warning' : 'badge-dark'}">
                                <i class="fa fa-address-card ${Categorizacion.length === 0 ? 'mr-1' : ''}"></i>
                                ${Categorizacion.length === 0 ? 'No categorizado' : 'Paciente'}
                            </span></h5>
                            <h5 class="ml-1"><span class="badge badge-secondary">${NombreTipoEstado}</span></h5>
                            <h5 class="ml-1">${pintarTipoPrioridad}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md">
                            <b class="mr-1">Nombre:</b>${Nombre ?? ''} ${ApellidoPaterno ?? ''} ${ApellidoMaterno ?? ''}
                            ${pintarNombreSocial}
                        </div>                     
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md">
                            <b class="mr-1">N° Documento:</b>${NumeroDocumento}${Digito ? `-${Digito}` : ''}
                        </div>                     
                    </div>                     
                    <div class="row">
                        <div class="col-xs-12 col-md">
                            <b class="mr-1">Fecha nacimiento:</b>${FechaNacimiento ? moment(FechaNacimiento).format("DD-MM-YYYY") : ''}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md">
                            <b class="mr-1">Edad:</b>${Edad ? Edad.EdadCompleta : 'No registra'}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md">
                            <b class="mr-1">Sexo:</b>${Sexo ? Sexo.Valor : 'No registra'}
                            <b class="mr-1">Género:</b>${Genero ? Genero.Valor : 'No registra'}
                        </div>
                    </div>
                    <div class="card-footer mt-3 border rounded-lg d-flex flex-wrap flex-row justify-content-center">
                        <i class="fa fa-user-clock mr-1 mt-2"></i>
                        <b class="mr-1 mt-2" style="font-size:13px;">Tiempo transcurrido:</b>
                        <span class="h6 text-muted mt-2" style="font-size:15px;">${horas} horas, ${minutos} minutos</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card border border-dark p-2">
                    <div class="row d-flex flex-row justify-content-between">
                        <div class="d-flex justify-content-md-start m-2">
                            <h5><span class="badge badge-dark"><i class="fa fa-ambulance"></i> Atención</span></h5>
                        </div>
                        <div class="d-flex justify-content-md-end m-2">
                            <h5><span class="badge badge-dark">ID: ${Atencion.Id}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-8">
                            <b class="mr-1">Fecha Llegada:</b> ${moment(Atencion.Fecha).format('DD-MM-YYYY HH:mm')}
                        </div>
                        <div class="col-12">
                            <b class="mr-1">Motivo atención:</b> <p>${Atencion.MotivoAtencion} ${Categorizacion.length > 0 && Categorizacion[0].Observaciones ? ` (${Categorizacion[0].Observaciones})` : ''}</p>
                        </div>
                        <div class="col-12">
                            ${pintarMedicoTratante}
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 p-1">
                <button class="btn btn-outline-success w-100" type="button" onclick='irAtencionClinica(${Atencion.Id})'>
                    <i class="fa fa-file"></i><br>Atención clínica
                </button>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 p-1">
                <button class="btn btn-outline-primary w-100" type="button" onclick='verInformacionAtencionUrgencia(${Atencion.Id})'>
                    <i class="fa fa-search"></i><br>Ver más
                </button>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 p-1">
                <button
                class="btn btn-outline-warning w-100"
                type="button"
                onclick="reubicarPacienteEnBox(${Atencion.Id}, ${IdCamaTipoBox})">
                    <i class="fa fa-bed"></i> </br>
                    Reubicar paciente
                </button>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 p-1">
                <button class="btn btn-outline-info w-100" type="button" onclick="abrirModalImprimirUc(${Atencion.Id}, '${Nombre} ${ApellidoPaterno}', '${NombreSocial ? NombreSocial : ''}', ${Atencion.IdEvento}, ${IdTipoEstado})">
                    <i class="fa fa-print"></i> </br>
                    Imprimir 
                </button>
            </div>
        </div>`;
    }
}


function printCamas(data, arrayBoxeshijos) {
    //Obtener sesión
    let SesionActual = getSession();
    let idPerfil = SesionActual.ID_PERFIL;
    let deshabilitarBoton = "";
    var codigo;
    var tipoPill;
    var color = "black";
    //Boolean si tiene categorización
    let categorizacion = true;
    //Verificar que es enfermero urgencia o tens de urgencia

    if (idPerfil != 190 && idPerfil != 191 && idPerfil != 149) {
        var disabled = "disabled";
        deshabilitarBoton = "d-none";
    }
    var fechaActual = GetFechaActual();
    var htmlSinInfo = "<h1 class='text-center w-100'><b><i class='fa fa-info-circle'></i> Sin Información</b></h1>";
    let icon = "";
    let clase = "";
    if (arrayBoxeshijos) {
        for (var i in arrayBoxeshijos) {
            let nombrePadre = "";
            let idPadre = "";
            nombrePadre = data[i].Valor;
            idPadre = data[i].Id;
            boxesHijos = "";

            for (var a in arrayBoxeshijos[i]) {
                //Gestionar el estado de las camas
                switch (arrayBoxeshijos[i][a].IdTipoEstado) {
                    // Cama Disponible
                    case 84:
                        icon = "fa fa-check-circle";
                        clase = "cama disponible";
                        break;
                    // Cama Ocupada
                    case 85:
                        icon = "fa fa-user-circle";
                        clase = "cama ocupada";
                        break;
                    // Cama Bloqueada
                    case 86:
                        icon = "fa fa-lock";
                        clase = "cama bloqueada";
                        break;
                    // Cama Deshabilitada
                    case 99:
                        icon = "fa fa-ban";
                        clase = "cama deshabilitada";
                        break;
                    // Sin Estado
                    default:
                        icon = "fa fa-question-circle";
                        card = "card-sin-estado";
                        break;

                }
                //Categorias
                arrayBoxeshijos[i][a].Paciente.map((e) => {
                    if (e.Categorizacion.length > 0) {
                        switch (e.Categorizacion[0].Codigo) {
                            case "C1":
                                color = "#dc3545";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "danger";
                                break
                            case "C2":
                                color = "#fd7e14";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "orange";
                                break
                            case "C3":
                                color = "#ffc107";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "warning";
                                break
                            case "C4":
                                color = "#28a745";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "success";
                                break
                            case "C5":
                                color = "#17a2b8";
                                codigo = e.Categorizacion[0].Codigo;
                                tipoPill = "info";
                                break
                            default:
                                color = "black";
                                codigo = "SC";
                                tipoPill = "default";
                                break
                        }
                    } else {
                        color = "black";
                        codigo = "SC";
                        tipoPill = "default";
                        categorizacion = false;
                    }
                })

                //Dibuja las camas
                boxesHijos += ` <div class="m-2" style="float:left;">
                                    <div class="">
                                        <strong> ${arrayBoxeshijos[i][a].Valor}</strong>
                                    </div>
                                    <button type="button" style="border:none;  background-color:white;" data-toggle="modal" data-target="#idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}">
                                    <div id='divCama_${arrayBoxeshijos[i][a].Id}'>
                                    <a id='aCama_${arrayBoxeshijos[i][a].Id}' class='cama-a'>
                                        <div class='${clase}' >
                                            <img src='../../Style/img/bed.PNG' width='40' draggable='false' />
                                            <i id='iCama_${arrayBoxeshijos[i][a].Id}' class='fa ${icon}' style='z-index:1;  ${arrayBoxeshijos[i][a].IdTipoEstado == 85 ? `color:${color}` : ""}'></i>
                                        </div>
                                     </a>
                                     </div>
                                    </button>
                                </div>
                    `;
                //Comprobar si la cama tiene paciente, true = tiene paciente.
                var comprobar = arrayBoxeshijos[i][a].IdTipoEstado == 84 ? false : true;
                //Cama disponible
                if (!comprobar) {
                    //Dibuja modales sin información (cuando la cama está disponible)
                    boxesHijos += ` <!--Modal -->
                    <div class="modal fade"  id="idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color:lightyellow;">
                                    <h5 class="modal-title" id="exampleModalLongTitle"><strong>${nombrePadre} | ${arrayBoxeshijos[i][a].Valor} </strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Cama disponible, no registra información de paciente</h5>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                </div>
                            </div>
                        </div>
                    </div>`;
                } else {


                    //Dibuja modal con la informaci+on del paciente
                    for (var s in arrayBoxeshijos[i][a]['Paciente']) {

                        let nombrePaciente = arrayBoxeshijos[i][a]['Paciente'][s].Paciente.Nombre + " " + arrayBoxeshijos[i][a]['Paciente'][s].Paciente.ApellidoPaterno + " " + arrayBoxeshijos[i][a]['Paciente'][s].Paciente.ApellidoMaterno;
                        var fechaIngreso = "";
                        //Manejo de fechas y tiempos transcurridos
                        if (arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Fecha != null) {
                            fechaIngreso = arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Fecha;
                            fechaIngreso = moment(fechaIngreso);
                            tiempoTranscurridoHoras = Math.abs(fechaIngreso.diff(fechaActual, 'hours'));
                            tiempoTranscurridoDias = Math.abs(fechaIngreso.diff(fechaActual, 'days'));
                            fechaIngreso = fechaIngreso.format("DD-MM-YY HH:mm");
                        } else {
                            tiempoTranscurridoDias = 0
                            tiempoTranscurridoHoras = 0
                            fechaIngreso = " No se registro fecha de ingreso";
                        }
                        //calcular edad
                        let fechaNac = moment(arrayBoxeshijos[i][a]['Paciente'][s].Paciente.FechaNacimiento);
                        let edadPaciente = CalcularEdad(moment(fechaNac).format("YYYY-MM-DD")).edad;


                        //Acceso a acompañante
                        let telefonoAcompañante;
                        let rutAcompañante;
                        let nombreAcompañante;
                        var htmlAcomp = "";
                        let acompañante = true;
                        //No registra acompañantes en la ultima atención
                        if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.IdPersona == null) {
                            htmlAcomp = "<p>No se ha registrado información de acompañante</p>";
                            acompañante = false;
                        } else {
                            //Si tiene acompañante
                            //Valida nombre
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Nombre) {
                                nombreAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Nombre + " " + arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.ApellidoPaterno + " " + arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.ApellidoMaterno;
                            } else {
                                nombreAcompañante = "No registro nombre el acompañante";
                            }
                            //rut invalido
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.NumeroDocumento == undefined || arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Digito == undefined) {
                                rutAcompañante = "No registro numero documento";
                            } else {
                                rutAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.NumeroDocumento + "-" + arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Digito;
                            }
                            //Valida telefono
                            if (arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Telefono) {
                                telefonoAcompañante = arrayBoxeshijos[i][a]['Paciente'][s].Acompañante.Telefono;

                            } else {
                                telefonoAcompañante = "No registro telefono";
                            }

                        }
                        boxesHijos += ` <!--Modal -->
                    <div class="modal fade"  id="idModalPaciente_${(i + 1) + "" + arrayBoxeshijos[i][a].Id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color:lightyellow;">
                                    <h5 class="modal-title col-md-9" id="exampleModalLongTitle">
                                    <strong> ${nombrePadre} | ${arrayBoxeshijos[i][a].Valor} </strong> 
                                    </h5>

                                    <span class="col-md-2 mt-1 badge badge-pill bg-${tipoPill} badge-count" style="font-size:1.1em; float:rigth;">${codigo}</span>
                                    <button type="button" class="close col-md-1 mr-1" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <strong><p>Nombre paciente: ${nombrePaciente} </p></strong>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreSocial ? `<p><b>Nombre Social:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreSocial}</p>` : ""}
                                    <p><b>Numero de documento:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NumeroDocumento}-${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.Digito}</p>
                                    <p><b>Edad: </b> ${edadPaciente} </p>
                                    <p><b>Genero :</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Paciente.NombreGenero}</p>
                                    <p><b>Estado del paciente:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.NombreTipoEstado} </p>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.MotivoAtencion ? ` <p><b>Estado del paciente:</b> ${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.MotivoAtencion} </p>` : ""}
                                    <p><b>Fecha de ingreso: </b>${fechaIngreso} </p>
                                    <p><b>Tiempo atención transcurrido:</b> En días: ${tiempoTranscurridoDias} días, En horas: ${tiempoTranscurridoHoras} horas</p>
                                    <h5><b>Información de acompañante</b></h5>
                                    ${acompañante == false ? htmlAcomp : ""}
                                    <div id='divInfoAcomp' class="${acompañante == false ? "d-none" : ""}">
                                    <p><b>Nombre acompañante:</b> ${nombreAcompañante}</p>
                                    <p><b>Rut acompañante:</b> ${rutAcompañante}</p>
                                    <p><b>Telefono acompañante: </b>${telefonoAcompañante}</p>
                                    ${arrayBoxeshijos[i][a]['Paciente'][s].TipoAcompañante.Valor ? `<p><b>Tipo acompañante: </b>${arrayBoxeshijos[i][a]['Paciente'][s].TipoAcompañante.Valor}</p>` : ""}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" ${disabled} id="botonMdlEnviaraBox"  class="btn btn-primary ${deshabilitarBoton}" data-id-categorizacion='${categorizacion ? arrayBoxeshijos[i][a]['Paciente'][s].Categorizacion.Codigo : ""}' data-nombrePaciente="${nombrePaciente}" data-dismiss="modal" onClick='llenarModalBox(this)'  data-id='${arrayBoxeshijos[i][a]['Paciente'][s].Atencion.Id}' data-toggle="modal" data-target="#mdlPasaraBox">Movimiento de paciente</button>
                                </div>
                            </div>
                        </div>
                    </div>`;

                        //Información del acompañante

                        //$("#divInfoAcomp").append(htmlAcomp);
                    }
                }
            }

            //comprobacion de que existan camas

            //si no hay camas, escribe mensaje "sin información"
            arrayBoxeshijos[i].length > 0 ? $("#BoxHijo" + idPadre).html(boxesHijos) : $("#BoxHijo" + idPadre).html(htmlSinInfo);
        }
    }
}

async function verInformacionAtencionUrgencia(id) {

    const data = GetJsonIngresoUrgencia(id);

    const { IdPaciente, MotivoConsulta, FechaLlegada, IdEvento } = data;
    const paciente = await getPacientePorId(IdPaciente)
    const { NombreSocial, Nombre, ApellidoPaterno, ApellidoMaterno } = paciente

    categorizacionUrgencia = await getCategorizacion(id)

    let AcompanantePaciente

    let AtencionCompleta = await promesaAjax("GET", `URG_Atenciones_Urgencia/${id}?extens=ATENCIONMEDICA&extens=SIGNOSVITALES`)
    if (AtencionCompleta.Acompañante !== null) {
        AcompanantePaciente = await promesaAjax("GET", `GEN_Personas/${AtencionCompleta.Acompañante.IdPersona}`)
    }

    $("#modalContenidoAtencionUrgencia").empty()
    $("#modalContenidoAtencionUrgencia").append(`
        <div class="col-6 col-xs-12">
            <div class="row">
                <div class="col-12">
                    <div class="card p-1"> 
                        <div class="row">
                            <div class="col-12"><h4><span class="badge badge-info"><i class="fa fa-ambulance"></i> Atención:</span></h4></div>
                            <div class="col-md-6 col-xs-12"><b>Id Atención:</b> ${id}</div>
                            <div class="col-md-6 col-xs-12"><b>Fecha llegada:</b>${moment(FechaLlegada).format(`DD-MM-YYYY HH:mm`)}</div>
                            <div class="col-12"><b>Motivo consulta:</b>${MotivoConsulta ?? ""}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-6 col-xs-12">
            <div class="row">
                <div class="col-12">
                    <div class="card p-1">
                        <div class="row"> 
                            <div class="col-12"><h4><span class="badge badge-info"><i class="fa fa-file"></i> Administrativo:</span></h4></div>
                            <div class="col-md-6 col-xs-12"><b>Lugar de accidente:</b> ${AtencionCompleta.LugarAccidente ?? `No informado.`}</div>
                            <div class="col-md-6 col-xs-12"><b>Medio transporte:</b> ${AtencionCompleta.MedioTransporte !== null ? AtencionCompleta.MedioTransporte.Valor : `No informado.`}</div>
                            <div class="col-md-6 col-xs-12"><b>Previsión:</b>
                            ${AtencionCompleta.Prevision !== null ? AtencionCompleta.Prevision.Valor : `No informado.`}
                            ${AtencionCompleta.PrevisionTramo !== null ? AtencionCompleta.PrevisionTramo.Valor : `No informado.`}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ${AtencionCompleta.Acompañante !== null && AcompanantePaciente !== undefined ? `
         <div class="col-12 col-xs-12">
            <div class="row">
                <div class="col-12">
                    <div class="card p-1">
                        <div class="row">
                            <div class="col-12"><h4><span class="badge badge-info"><i class="fa fa-users"></i> Acompañante:</span></h4></div>
                             <div class="col-md-5 col-xs-12"><b>Nombre acompañante:</b> ${AcompanantePaciente.Nombre} ${AcompanantePaciente.ApellidoPaterno} ${AcompanantePaciente.ApellidoMaterno}</div>
                             <div class="col-md-4 col-xs-12"><b>Genero:</b> ${AcompanantePaciente.Genero !== null ? `${AcompanantePaciente.Genero.Valor}` : `No informado`} </div>
                             <div class="col-md-3 col-xs-12"><b>Tipo acompañante:</b> ${AtencionCompleta.Acompañante.TipoRepresentante !== null ? `${AtencionCompleta.Acompañante.TipoRepresentante.Valor}` : `No informado`} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `: ``}
    `)
    cargarProcedimientosPendientes(id)
    dibujarSignosVitales(id)
    cargarMedicamentos(id)
    cargarExamenes(IdEvento)
    cargarInterconsultor(id)
    cargarComentarios(id)
    $("#modalTituloAtencionUrgencia").empty()
    $("#modalTituloAtencionUrgencia").append(`Paciente:${NombreSocial !== null ? `(${NombreSocial})` : ``}  ${Nombre ?? ``} ${ApellidoPaterno ?? ""} ${ApellidoMaterno ?? ``}`)
    $("#mdlInformacionAtencionUrgencia").modal("show")
}

async function dibujarSignosVitales(IdAtencion) {
    $("#spnSignosVitales").empty();
    $("#divSigosVitales").empty();
    let signosVitalesUrgencia = [];

    try {
        // Obtener los signos vitales
        signosVitalesUrgencia = await promesaAjax(`GET`, `/URG_Atenciones_Urgencia/${IdAtencion}/Signos`);

        if (signosVitalesUrgencia.length > 0) {
            // Ordenar los signos vitales por fecha de manera descendente
            signosVitalesUrgencia.sort((a, b) => new Date(b.FechaHora) - new Date(a.FechaHora));

            // Obtener la última toma de signos vitales
            let signosVitales = signosVitalesUrgencia[0];
            let contenido = ``;

            // Calcular PAM si existe presión arterial
            let pam = null;
            let presionArterial = signosVitales.Signos.find(signo => signo.TipoMedida.Valor === "P. Arterial");
            if (presionArterial) {
                let valorPA = presionArterial.Valor;
                let valores = valorPA.split('/');
                let ps = parseInt(valores[0].trim());
                let pd = parseInt(valores[1].trim());
                if (!isNaN(ps) && !isNaN(pd)) {
                    pam = ((ps + 2 * pd) / 3).toFixed(1);
                }
            }

            // Mostrar número de signos vitales
            $("#spnSignosVitales").append(signosVitales.Signos.length);

            // Generar contenido para los signos vitales
            signosVitales.Signos.forEach(item => {
                contenido += `
                    <div class="col-sm-4 col-md-3 col-lg-2 salud-box">
                        <div class="row">
                            <div class="col-12 card text-center">
                                <span>${item.TipoMedida.Valor}</span>
                                <br>
                                <h4><i class="salud-icon fas ${item.Icono} fa-lg"></i></h4>
                                <span>${item.Valor}</span>
                            </div>
                        </div>
                    </div>
                `;
            });

            // Añadir el parámetro PAM si está calculado
            if (pam !== null) {
                contenido += `
                    <div class="col-sm-4 col-md-3 col-lg-2 salud-box">
                        <div class="row">
                            <div class="col-12 card text-center">
                                <span>PAM</span>
                                <br>
                                <h4><i class="salud-icon fas fa-heart fa-lg"></i></h4>
                                <span>${pam}</span>
                            </div>
                        </div>
                    </div>
                `;
            }

            $("#divSigosVitales").append(contenido);

            // Pintar la última toma de signos vitales en el div correspondiente
            let fechaActualParaSignos = moment(GetFechaActual());
            let fechaUltimaToma = moment(signosVitales.FechaHora);
            const diferenciaMinutos = fechaActualParaSignos.diff(fechaUltimaToma, 'minutes');

            $("#ultimaTomaSignosUrg").empty();
            $("#ultimaTomaSignosUrg").append(`
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex flex-row justify-content-start flex-wrap">
                    <div>
                        <span><strong>Última toma de signos vitales:</strong></span>
                        <span>${moment(signosVitales.FechaHora).format('DD/MM/YYYY HH:mm:ss')}</span>    
                    </div>
                    ${signosVitales.Profesional?.Valor !== undefined ?
                    `<div>&nbsp;
                            <span><strong>Profesional:</strong></span>
                            <span>${signosVitales.Profesional?.Valor || ""}</span >
                        </div>` : ""
                }
                    ${diferenciaMinutos > 120 ?
                    `<div>
                            <span class="text-red"><i class="fa fa-exclamation-triangle latidos"></i> Han pasado más de 2 horas desde la última toma de signos vitales.</span> 
                        </div>` : ""
                }
                </div>
            `);
        } else {
            $("#spnSignosVitales").append(0);
            $("#ultimaTomaSignosUrg").html(`<div class="col-md-12 d-flex justify-content-center">No hay información para mostrar</div>`);
        }
    } catch (error) {
        console.error("Error al buscar signos vitales:", error);
        $("#ultimaTomaSignosUrg").html(`<div class="col-md-12 d-flex justify-content-center">Error al cargar los signos vitales.</div>`);
    }
}


async function cargarInterconsultor(IdAtencionUrgencia) {
    const solicitudes = await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${IdAtencionUrgencia}/Interconsultores`,
        contentType: 'application/json',
        dataType: 'json',
    })
    $("#spnInterconsultor").empty()
    $("#spnInterconsultor").append(solicitudes.length)
    //tblInterconsultor
    $("#tblInterconsultor").DataTable({
        destroy: true,
        data: solicitudes,
        columns: [
            { title: "Especialidad", data: "TipoConsultor" },
            { title: "Fecha solicitud", data: "Fecha" },
            { title: "Solicitud", data: "ActividadSolicitada" },
            { title: "Fecha cierre", data: "FechaCierre" },
            { title: "Actividad", data: "ActividadRealizada" },
            { title: "Profesional", data: "ProfesiaonlSolicita" }
        ], columnDefs: [
            {
                targets: [0, -1],
                render: function (data) {
                    if (data !== null)
                        return data.Valor
                }
            }, {
                targets: [1, 3],
                render: function (fecha) {
                    if (fecha !== null)
                        return moment(fecha).format("DD/MM/YYYY HH:mm")
                    else
                        return `No registra fecha`
                }
            }, {
                targets: [-2],
                render: function (actividad) {
                    if (actividad !== null)
                        return actividad
                    else
                        return `No se ha realizado actividad`
                }
            }
        ]
    })
}
async function cargarComentarios(idAtencionUrgencia) {
    $("#spnComentarios").empty()
    $.ajax({
        method: "GET",
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencionUrgencia}/Comentarios`,
        success: function (data) {
            $("#spnComentarios").append(data.length)
            llenarTablaComentarios(data)
        }, error: function (error) {
            $("#spnComentarios").append(0)
            llenarTablaComentarios([])
            console.error("Ocurrio un error al buscar comentarios de la atencion")
            console.log(error)
        }
    })
}
function llenarTablaComentarios(data) {
    $("#tblComentarios").DataTable({
        data: data,
        columns: [
            { title: "Comentario", data: "Comentario" },
            { title: "Fecha", data: "FechaHora", render: function (fechahora) { return moment(fechahora).format("DD-MM-YYYY HH:mm") } },
            { title: "Profesional", data: "Profesional", render: function (profesional) { return profesional !== null ? profesional.Valor : `No registra` } }
        ],
        destroy: true
    })
}
async function cargarExamenes(IdEvento) {

    let arrayExamenesLaboratorio = await buscarExamenesPorEvento(IdEvento)
    let arrayExamenesImagen = await buscarExamenesImagenPorEvento(IdEvento)

    $("#spnExamenesLab").html(`${arrayExamenesLaboratorio.length}`)
    $("#spnExamenesImagen").html(`${arrayExamenesImagen.length}`)

    let columnas = [
        { title: "Id", data: "Id" },
        { title: "Fecha", data: "Fecha" },
        { title: "Solicitante", data: "Profesional", orderable: false },
        { title: "Examenes", data: "Solicitud", orderable: false },
        { title: "Observaciones", data: "Observacion", orderable: false },
        { title: "Acciones", data: "Id", orderable: false }
    ]

    let configColumn = [
        { targets: 1, render: function (fecha) { return moment(fecha).format("DD/MM/YYYY HH:mm") } }//fecha tiene que ser formateada
        , {
            targets: 2,//Profesional validando nulos y retornando el nombre
            render: function (profesional) {
                let nombreProfesional = `No ingresado`
                if (profesional !== null) {
                    if (profesional.Persona !== null) {
                        nombreProfesional = `${profesional.Persona.Nombre} ${profesional.Persona.ApellidoPaterno} ${profesional.Persona.ApellidoMaterno}`
                    }
                }
                return nombreProfesional
            }
        }, {
            targets: 3,//Recorriendo los examenes para devolver un listado 
            render: function (solicitud) {
                let stringExamenes = `Sin examenes, revise la solicitud`
                if (solicitud !== null) {
                    if (solicitud.Examen.length > 0) {
                        stringExamenes = `<ul>`
                        let htmlExamenes = ``
                        solicitud.Examen.map(examen => {
                            htmlExamenes += `<li>${examen.Valor}</li>`
                        })
                        stringExamenes += `${htmlExamenes} </ul>`
                    }
                }
                return stringExamenes
            }
        },
        {
            targets: -2,//Recorriendo los examenes para devolver un listado
            render: function (observacion) {
                if (observacion == null)
                    return `Sin observaciones`
                return observacion
            }
        },
        {
            targets: -1,//Recorriendo los examenes para devolver un listado
            render: function (solicitud, columna, data) {
                return `<button class="btn btn-info btn-sm" onclick="imprimirSolicitudExamenUrgencia(${data.Id}, ${data.IdTipoArancel})" type="button"> <i class="fa fa-print"></i> </button>`
            }
        }
    ]
    //tblExamenLaboratorio
    dibujarTablaExamenesIngresados(arrayExamenesLaboratorio, "#tblExamenLaboratorio", columnas, configColumn)
    dibujarTablaExamenesIngresados(arrayExamenesImagen, "#tblExamenImagen", columnas, configColumn)

}

async function cargarProcedimientosPendientes(IdAtencion) {
    $("#spnProcedimientos").empty()
    $("#tblProcedimientos").empty()
    let procedimientosUrgencia = []
    await promesaAjax("GET", `URG_Atenciones_Urgencia/${IdAtencion}/Procedimientos`).then(res => {
        procedimientosUrgencia = res
    }).catch(error => {
        console.error("Error al buscar procedimientos")
        console.log(error)
    })
    $("#spnProcedimientos").append(procedimientosUrgencia.length)

    $("#tblProcedimientos").DataTable({
        data: procedimientosUrgencia,
        columns: [
            { title: "Procedimiento", data: "Arancel", orderable: false },
            { title: "Fecha solicitud", data: "FechaIngreso", orderable: false },
            { title: "Solicitante", data: "ProfesionalSolicitante", orderable: false },
            { title: "Estado", data: "TipoEstado", orderable: false },
        ],
        columnDefs: [
            {
                targets: [0, 3],
                render: function (elemento) {
                    if (elemento !== null)
                        return elemento.Valor
                },
            }, {
                targets: 1,
                render: function (fecha) {
                    return moment(fecha).format("DD-MM-YYYY HH:mm")
                },
            }, {
                targets: 2,
                render: function (profesional) {
                    if (profesional)
                        return `${profesional.Nombre} ${profesional.ApellidoPaterno} ${profesional.ApellidoMaterno}`
                },
            }
        ],
        destroy: true
    })
}

function buscarPacientesParaBox(Box, idTipoBox) {

    const pacienteDeBandeja = sSession && sSession.ID_PACIENTE !== undefined;

    //Mostrar modal
    $("#modalPacienteUrgencia").modal("show");
    //Se debe mostrar el box que esa gestionado para el usuario
    $("#TituloModalPacienteUrgencia").empty();
    $("#TituloModalPacienteUrgencia").append(`Buscando pacientes ingresados para asignar a box: ${Box.Valor}`);
    //Rutas para consultar
    let urlAsignados = `URG_Atenciones_Urgencia/Buscar?idTipoEstadoSistemas=103&filas=300`;
    let urlSinAsignar = `URG_Atenciones_Urgencia/Buscar?idTipoEstadoSistemas=100&idTipoEstadoSistemas=102&filas=300`;
    let urlTodos = `URG_Atenciones_Urgencia/Buscar?idTipoEstadoSistemas=100&idTipoEstadoSistemas=102&idTipoEstadoSistemas=103&filas=500`;

    // Mostrar u ocultar tabs dependiendo si el paciente viene (enla sesion) desde bandeja
    if (pacienteDeBandeja) {
        $("#tabSinAsignar").show();
        $("#tabAsignados, #tabTodos").hide();
        $("#tipoAtencionSelect").prop('disabled', true);
    } else {
        $("#tabSinAsignar, #tabAsignados, #tabTodos").show();
        $("#tipoAtencionSelect").prop('disabled', false);
    }

    if (pacienteDeBandeja) {
        //si viene paciente desde sesion, se modifica en el endpoint para solo cargar ese paciente
        urlSinAsignar += `&idPaciente=${sSession.ID_PACIENTE}`;
    }

    cargarTiposDeAtencion().then(() => {
        //cargar pacientes sin asignar
        fetchData(urlSinAsignar, "#tblSinAsignar", Box);
        if (!pacienteDeBandeja) {
            //Cuando no viene paciente en la sesion se cargan todas las demas tablas
            fetchData(urlAsignados, "#tblAsignados", Box);
            fetchData(urlTodos, "#tblTodos", Box);
        }
    });

    // cuando se cierra el modal:impiar los datos del paciente que viene desde la bandeja para volver a mostrar todos
    $("#modalPacienteUrgencia").on('hidden.bs.modal', function () {
        // Resetear la variable de sesión
        sSession = undefined;
    });
}

function fetchData(url, selector, Box) {
    promesaAjax("GET", url).then(res => {
        renderDataTable(selector, res, Box);
        if (res[0].NombreTipoAtencion != null && res[0].NombreTipoAtencion !== undefined) {
            $("#tipoAtencionSelect").val(res[0].NombreTipoAtencion).trigger("change");
        }
        $('#tipoAtencionSelect').unbind().on('change', function () {
            const tipoAtencion = this.value;
            $(selector).DataTable().column(5).search(tipoAtencion || '').draw();
            //if (tipoAtencion) {
            //    tableACargar.column(5).search(tipoAtencion).draw();
            //} else {
            //    tableACargar.column(5).search('').draw();
            //}
        });

        //let tipoAtencion = $("#selectTipoAtencion option:selected").text();
        //$("#tipoAtencionSelect").val(tipoAtencion).trigger("change");
    }).catch(error => {
        console.error(`Ocurrió un error al buscar pacientes para ${selector}`);
        console.log(error);
    });
}

function renderDataTable(selector, data, Box) {
    tableACargar = $(selector).DataTable({
        data: data,
        ordering: false,
        columns: [
            { title: "Id", data: "Id" },
            { title: "N° doc", data: "Paciente" },
            { title: "Nombre", data: "Paciente" },
            { title: "Fecha admisión", data: "FechaAdmision" },
            { title: "Cat", data: "Categorizacion", orderable: false },
            { title: "Tipo", data: "NombreTipoAtencion", orderable: false },
            { title: "Estado", data: "TipoEstadoSistema", orderable: false },
            { title: "Motivo consulta", data: "MotivoConsulta", orderable: false },
            { title: "", data: "Id" }
        ],
        columnDefs: [
            {
                targets: 2,
                render: function (Paciente) {
                    if (Paciente !== null) {
                        let nombre = `${Paciente.Nombre ?? ''} ${Paciente.ApellidoPaterno ?? ''} ${Paciente.ApellidoMaterno ?? ''}`;
                        if (Paciente.NombreSocial)
                            nombre = `(${Paciente.NombreSocial}) ${nombre}`;
                        return nombre;
                    } else {
                        return 'NN';
                    }
                }
            },
            {
                targets: 1,
                render: function (Paciente) {
                    if (Paciente !== null)
                        return `${Paciente.NumeroDocumento ?? ``}`;
                }
            },
            {
                targets: 3,
                render: function (Fecha) {
                    if (Fecha !== null)
                        return moment(Fecha).format("DD-MM-YYYY HH:mm")
                }
            },
            {
                targets: 4,
                render: function (Categorizacion) {
                    if (Categorizacion !== null)
                        return `<h5>${dibujarCategorizacion(Categorizacion.Codigo)}</h5>`;
                    else
                        return `<h5><span class="badge badge-dark">SC</span></h5>`;
                }
            },
            {
                targets: -3,
                render: function (Estado) {
                    if (Estado !== null)
                        return Estado.Valor;
                    else
                        return `Sin estado`;
                }
            },
            {
                targets: -1,
                render: function (idAtencion, row, data) {

                    const { TipoEstadoSistema, Paciente } = data;
                    const nombrePaciente = $.trim(`${Paciente.Nombre ?? ""} ${Paciente.ApellidoPaterno ?? ""} ${Paciente.ApellidoMaterno ?? ""}`);

                    return `<button 
                            class="btn btn-primary btn-sm"
                            type="button"
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Asignar box"
                            onclick='asignarPacienteABox(${idAtencion}, ${JSON.stringify(Box)},${TipoEstadoSistema.Id})'>
                                <i class="fas fa-procedures"></i>
                            </button>`;
                }
            }
        ],
        destroy: true,
    });

}


function asignarPacienteABox(idAtencion, Box, idEstado) {

    const PACIENTE_ADMITIDO = 100
    const esPacienteAdmitido = PACIENTE_ADMITIDO === idEstado

    const mensaje = {
        title: esPacienteAdmitido
            ? 'Paciente no ha sido categorizado.'
            : 'Confirme la acción',
        text: esPacienteAdmitido
            ? '¿Está seguro que quiere continuar?'
            : `Usted está a punto de asignar este box a la atención con DAU N° ${idAtencion} a ${Box.Valor}`,
        icon: esPacienteAdmitido
            ? 'question'
            : `warning`
    }

    Swal.fire({
        title: mensaje.title,
        text: mensaje.text,
        icon: mensaje.icon,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Asignar box",
        cancelButtonText: "Cancelar"
    }).then((result) => {

        if (result.value) {

            $.ajax({
                method: 'PATCH',
                url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idAtencion}/box/${Box.IdCamaTipoBox}`,
                success: function (data) {
                    //en urgencia carga la tabla urgencia
                    cancelarMovimientoPaciente();
                    cargarCamasUrgencia(parseInt($("#selectTipoAtencion").val()), false);
                    $('#modalPacienteUrgencia').modal('hide');
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'El paciente ha sido enviado al box.',
                        showConfirmButton: false,
                        timer: 3000
                    })
                },
                error: function (jqXHR, status) {
                    ShowModalCargando(false);
                    if (jqXHR.status === 404) {
                        Swal.fire({
                            title: "Box Ocupado",
                            text: jqXHR.statusText,
                            icon: "warning"
                        });
                        cargarCamasUrgencia(parseInt($("#selectTipoAtencion").val()), false);
                        return;
                    }
                }
            });
        }
    });
}

async function cargarMedicamentos(idDAU) {
    let medicamentosUrgencia = []
    await promesaAjax(`GET`, `URG_Atenciones_Urgencia/${idDAU}/Medicamentos`).then(res => {
        medicamentosUrgencia = res
    }).catch(error => {
        console.log(error)
        console.error("Error al buscar medicamentos")
    })
    $("#spnMedicamentos").empty()
    $("#spnMedicamentos").append(medicamentosUrgencia.length)

    $("#tblMedicamentos").DataTable({
        data: medicamentosUrgencia,
        columns: [
            { title: "Medicamento", data: "Medicamento", orderable: false },
            { title: "Dosis", data: "Dosis", orderable: false },
            { title: "Frecuencia", data: "Frecuencia", orderable: false },
            { title: "Via administracion", data: "TipoViaAdministracion", orderable: false },
            { title: "Fecha solicitud", data: "FechaIngreso", orderable: false },
            { title: "Observaciones", data: "Observaciones", orderable: false },
            { title: "Solicita", data: "ProfesionalSolicita", orderable: false },
            { title: "Estado", data: "TipoEstado", orderable: false },
        ],
        columnDefs: [
            {
                targets: [0, 3, 6, 7],
                render: function (elemento) {
                    if (elemento !== null)
                        return elemento.Valor
                }
            },
            {
                targets: 4,
                render: function (fecha) {
                    return moment(fecha).format("DD-MM-YYYY HH:mm")
                }
            }
        ],
        destroy: true
    })

}

function irAtencionClinica(id) {

    localStorage.setItem("ES_ATENCION_CLINICA", true);
    setSession('ID_INGRESO_URGENCIA', id);
    window.location.replace(`${ObtenerHost()}/Vista/ModuloUrgencia/AtencionClinica.aspx`);
}

function llenarModalBox(e) {
    linkAsignarBox(e);
    //Cargar combo tipo atencion
}
