﻿function comboServicioSalud() {
    const url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, "#sltServicioSalud");
    $("#sltServicioSalud").val('1').attr("disabled", "disabled");
}
function comboEstablecimiento(idServicioSalud, slt, idEstablecimientoPorDefecto = '1', idTipoEstablecimiento = 2) {
    const url = `${GetWebApiUrl()}GEN_Establecimiento/Combo?idServicioSalud=${idServicioSalud}&idTipoEstablecimiento=${idTipoEstablecimiento}`;
    setCargarDataEnCombo(url, false, slt);
    if (idEstablecimientoPorDefecto != null)
        $(slt).val(idEstablecimientoPorDefecto).attr("disabled", "disabled");
}
function comboMotivoEmergenciaObstetrica() {
    const url = `${GetWebApiUrl()}URG_Tipo_Motivo_Emergencia_Obstetrica/Combo`;
    setCargarDataEnCombo(url, false, "#sltMotivoEmergenciaObstetrica");
}

function comboApreciacionClínica() {
    const url = `${GetWebApiUrl()}GEN_Apreciacion_Clinica_Alcolemia/combo`;
    setCargarDataEnCombo(url, false, "#sltApreciacionClinica");
}
function comboMediosTransporte() {
    let url = `${GetWebApiUrl()}/URG_Medios_Transporte/combo`;
    setCargarDataEnCombo(url, false, $('#sltMedioTransporte'));
}
function comboTipoSamu() {
    let url = `${GetWebApiUrl()}URG_Tipo_SAMU/Combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoSamu'));
}
function comboTiposAccidente() {
    let url = `${GetWebApiUrl()}/URG_Tipos_Accidente/combo`;
    setCargarDataEnCombo(url, false, $('#sltTipoAccidente'));
}
function comboLeyesPrevisionales() {
    let url = `${GetWebApiUrl()}/GEN_Ley_Previsional/combo`;
    setCargarDataEnCombo(url, false, $('#sltLeyesPrevisionales'));
}
function comboProcedencia() {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Procedencia/combo`;
    setCargarDataEnCombo(url, false, $('#sltProcedencia'));
}
function comboProfesionEvolucionAtencion(idProfesional) {

    $("#sltProfesionEvolucionAtencion").empty();
    $("#sltProfesionEvolucionAtencion").append("<option value='0'>Seleccione</option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${idProfesional}/Profesion`,
        success: function (data, status, jqXHR) {

            data.forEach((item, index) => {
                $("#sltProfesionEvolucionAtencion").append(`<option value='${item.GEN_idProfesion}'>${item.GEN_nombreProfesion}</option>`);
            });

            if ($("#sltProfesionEvolucionAtencion option").length == 2) {
                $("#sltProfesionEvolucionAtencion option:eq(1)").attr('selected', 'selected');
                $("#sltProfesionEvolucionAtencion").attr("disabled", "disabled");
                $("#spnProfesionEvolucionAtencion").text($("#sltProfesionEvolucionAtencion :selected").text());
            }
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Profesion de Profesional Atención clínica ${JSON.stringify(jqXHR)}`);
        }
    });
}
function comboTipoEstado() {
    $("#sltEstado, #sltEstadoAlta").empty();
    $("#sltEstado").append(`<option value="0">Seleccione</option>`);
    $("#sltEstado, #sltEstadoAlta").append(`<option value="100">Paciente Admitido</option>`);
    $("#sltEstado, #sltEstadoAlta").append(`<option value="102">Paciente Categorizado</option>`);
    $("#sltEstado, #sltEstadoAlta").append(`<option value="103">Paciente en Box</option>`);
    $("#sltEstado, #sltEstadoAlta").append(`<option value="104">Paciente en Atencion</option>`);
    $("#sltEstado, #sltEstadoAlta").append(`<option value="105">Paciente dado de alta</option>`);
}
function comboCategorizacionObstetrico() {

    let url = `${GetWebApiUrl()}URG_Categorizacion_Obstetrica/Combo`;
    $("#sltCategorizacionObstetrico").empty();

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $("#sltCategorizacionObstetrico").append("<option value='0'>Seleccione</option>");
            let optGroup = null, htmlOptions = "";

            data.forEach(function (val, index) {

                let tiempoEstimado = "";
                switch (val.IdCategorizacion) {
                    case 1:
                        tiempoEstimado = `Es evaluada de inmediato por profesional matrona y derivada a medico según la urgencia.`;
                        break;
                    case 2:
                        tiempoEstimado = `Categorización dentro de los 10 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 30 minutos desde la hora de categorización.`;
                        break;
                    case 3:
                        tiempoEstimado = `Categorización dentro de los 30 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 90 minutos desde la hora de categorización o en su defecto reevaluar. `;
                        break;
                    case 4:
                        tiempoEstimado = `Categorización dentro de los 60 minutos desde la llegada de la paciente y atención por profesional ` +
                            `matrona y/o médico dentro de los 180 minutos desde la hora de categorización o en su defecto reevaluar.`;
                        break;
                    case 5:
                        tiempoEstimado = `Paciente espera hasta que se resuelva la atención de las usuarias categorizadas como C1, C2, C3 y C4. ` +
                            `Es una consulta que no debió haber llegado a este nivel de complejidad de la atención en red. Se debe educar para ` +
                            `consulta en Atención Primaria de Salud.`;
                        break;
                }

                if (optGroup != val.DescripcionCategorizacion)
                    $("#sltCategorizacionObstetrico").append(`<optgroup label="${val.DescripcionCategorizacion}"></optgroup>`);

                $(`#sltCategorizacionObstetrico optgroup[label="${val.DescripcionCategorizacion}"]`).append(
                    `<option value='${val.Id}'>${val.Valor}</option>`);

                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("id-Categorizacion", val.IdCategorizacion);
                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("tiempo-estimado", tiempoEstimado);
                $(`#sltCategorizacionObstetrico option[value='${val.Id}']`).data("descripcion-categorizacion", val.DescripcionCategorizacion);

                optGroup = val.DescripcionCategorizacion;

            });


        },
        error: function (jqXHR, status) {
            console.log(`Error URL: ${url} ${JSON.stringify(jqXHR)}`);
        }
    });
}
function comboTipoClasificacion(IdTipoAtencion, sltClasificacion) {
    let url = `${GetWebApiUrl()}URG_Tipo_Clasificacion/Combo${(IdTipoAtencion != null) ? `?idTipoAtencion=${IdTipoAtencion}` : ""}`;
    setCargarDataEnCombo(url, false, $(`#${sltClasificacion}`));
}
function comboTipoPrioridadPaciente() {
    if ($(`#sltTipoPrioridadPaciente option`).length === 0) {
        let url = `${GetWebApiUrl()}URG_Tipo_Prioridad_Paciente/Combo`;
        setCargarDataEnCombo(url, false, $(`#sltTipoPrioridadPaciente`));
    }
}
function comboMotivoCierre() {
    if ($('#sltMotivoCierre option').length === 0) {
        let url = `${GetWebApiUrl()}URG_Tipo_Motivo_Cierre/Combo`;
        setCargarDataEnCombo(url, true, $("#sltMotivoCierre"));
    }
}
function ComboPronosticoMedicoLegal() {
    let url = `${GetWebApiUrl()}URG_Pronostico_Medico_Legal/Combo`;
    setCargarDataEnCombo(url, true, $("#sltPronosticoMedicoLegal"));
}
function comboTipoAtencion() {
    if ($("#sltTipoAtencion option").length === 0) {
        let url = `${GetWebApiUrl()}/URG_Tipo_Atencion/Combo`;
        setCargarDataEnCombo(url, false, $("#sltTipoAtencion"));
    }
}
function CargarComboUbicacion() {
    if ($('#sltUbicacionDerivado option').length === 0) {
        //let url = GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1";
        let url = GetWebApiUrl() + "GEN_Ubicacion/Hospitalizacion/1";
        setCargarDataEnCombo(url, true, $("#sltUbicacionDerivado"));
    }
}

function comboProcesoFallecimiento() {
    let url = `${GetWebApiUrl()}URG_Tipo_Proceso_Fallecimiento/Combo`;
    setCargarDataEnCombo(url, true, $("#sltProcesoFallecimiento"));
}

function ComboDestinoAltaPaciente() {

    // Limpiar las opciones actuales excepto el primer option vacío
    //$select.find('option:not(:first)').remove();

    let url = `${GetWebApiUrl()}URG_Destino_Alta_Paciente/Combo`;
    setCargarDataEnCombo(url, true, $("#sltDestinoAltaPaciente"));
}
function ComboDestinoDerivacion() {
    let url = `${GetWebApiUrl()}URG_Tipo_Destino_Derivacion/Combo`;
    setCargarDataEnCombo(url, true, $("#sltDestinoDerivacion"));
}

function comboPrioridad() {
    let url = `${GetWebApiUrl()}URG_Tipo_Prioridad_Paciente/Combo`;
    setCargarDataEnCombo(url, false, $("#sltPrioridad"));
    $('#sltPrioridad option[value="0"]').remove();
    $('#sltPrioridad').selectpicker('refresh');
}

async function comboTipoUrgencia(array) {

    if ($('#sltTipoUrg').has('option')) {
        $('#sltTipoUrg').empty();
        $.each(array, function (i, r) {
            $('#sltTipoUrg').append(`<option value='${r.Id}'>${r.Valor}</option>`);
        });

        $('#sltTipoUrg').selectpicker('refresh');
    }
}

function comboTipoBox(idUbicacion) {
    $("#inpIdUbicacion").data("id-ubicacion", idUbicacion);
    //Escribe el nombre en el modal.
    //if (vCodigoPerfil === perfilAccesoSistema.matroneriaUrgencia)
    let url = `${GetWebApiUrl()}URG_Tipo_Box/Combo/TipoAtencion/${idUbicacion}`;
    setCargarDataEnCombo(url, false, $("#sltTipoBox"));
}
function comboServicioBox(nombrePaciente) {
    $("#lblPasaraBox").empty();
    $("#lblPasaraBox").append(nombrePaciente);
    $("#sltServicioBox").empty();
    //Cargar combo tipo atencion
    if ($("#sltServicioBox").has('option').length === 0) {
        let url = `${GetWebApiUrl()}/URG_Tipo_Atencion/Combo`;
        setCargarDataEnCombo(url, true, $("#sltServicioBox"));
    }
    $("#sltServicioBox").unbind().change(function () {

        if ($("#sltServicioBox").val() != '0' && $("#sltServicioBox").val() != null) {
            comboTipoBox($("#sltServicioBox").val());
            $('#sltTipoBox').removeAttr('disabled');
            $('#sltNombreBox').attr('disabled', 'disabled').val('0');
        } else {
            $('#sltTipoBox').attr('disabled', 'disabled').val('0');
            $('#sltNombreBox').attr('disabled', 'disabled').val('0');
        }

    });

}
function comboNombreBox() {

    if ($("#sltTipoBox").val() != '0' && $("#sltTipoBox").val() != null) {
        let url = `${GetWebApiUrl()}URG_Box/Combo/Tipo/${$("#sltTipoBox").val()}/TipoAtencion/${$("#sltServicioBox").val()}`;
        setCargarDataEnCombo(url, true, $("#sltNombreBox"));
        $('#sltNombreBox').removeAttr('disabled');
    } else {
        $('#sltNombreBox').val('0').attr('disabled', 'disabled');
    }

}
function comboPrevision() {
    const url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, "#sltPrevision");
}
function comboPrevisionTramo(idPrevision) {
    if (idPrevision !== 0) {
        let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${idPrevision}`;
        setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
    }
}
function comboMedicoRemitente() {

    $('#selNombreMedicoRemitente').empty().append(`<option value='0'>Seleccione</option>`);

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/Medico`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (i, r) {
                $('#selNombreMedicoRemitente').append(`<option value='${r.GEN_idProfesional}'>${r.GEN_nombrePersonas}</option>`);
            });
        }
    });

}