﻿$(document).ready(function () {
    InicializarComponentesRut('#selTipoIdentificacion', '#txtHosFiltroRut', '#txtHosFiltroDV', "#lblTipoIdentificacion")
    InicializarComponentesRut('#selTipoIdentificacionAcompanante', '#txtnumeroDocAcompañante', '#txtDigAcompañante', "#lblTipoIdentificacionAcompanante")
});

function InicializarComponentesRut(selectTipoIdentificacion, inputRut, inputDv, labelTipoIdentificacion) {
    $(selectTipoIdentificacion).change(function (e) {
        $(inputRut).val('');
        $(inputDv).val('');
        if ($(this).val() == 1 || $(this).val() == 4) {
            $('.digito').show();
            $(inputRut).attr('maxlength', 8);
        } else {
            $('.digito').hide();
            $(inputRut).attr('maxlength', 10);
        }
        $(labelTipoIdentificacion).text($(selectTipoIdentificacion).children("option:selected").text());
    });
}

function InicializarInputRut(sltTipoIdentificacion, inputRut, inputDv) {
    
    $(inputRut).on('input', function () {
        if ($(sltTipoIdentificacion).val() === '1' || $(sltTipoIdentificacion).val() === '4') {
            $(inputDv).val('');
            $("#txtnombreacompanante , #txtapellidopcompanante, #txtapellidomacompanante").val("")
            $("#txtnombreacompanante , #txtapellidopcompanante, #txtapellidomacompanante").attr("disabled",true)
        }
    });
    $(inputRut).on('blur', function () {

        if ($(sltTipoIdentificacion).val() === '2' || $(sltTipoIdentificacion).val() === '3') {
            if ($.trim($(inputRut).val()) !== '') {
                
            } else {
                limpiarPersona()
                deshabilitarPersona()
            }
        }
    });
    $(inputDv).on('input', async function () {

        if ($.trim($(inputDv).val()) != "") {

            // 1 = RUT
            // 4 = RUT MATERNO
            if ($(sltTipoIdentificacion).val() === '1' || $(sltTipoIdentificacion).val() === '4') {

                // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
                let esRutCorrecto =
                    await EsValidoDigitoVerificador($(inputRut).val(), $(inputDv).val());

                // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA 
                //TRAER LA INFO DEL PACIENTE
                if (esRutCorrecto) {
                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Personas/Buscar?idIdentificacion=${$(sltTipoIdentificacion).val()}&numeroDocumento=${$(inputRut).val()}`,
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            if (data.length > 0) {
                                $("#txtnombreacompanante").val(data[0].Nombre)
                                $("#txtapellidopcompanante").val(data[0].ApellidoPaterno)
                                $("#txtapellidomacompanante").val(data[0].ApellidoMaterno)
                                $("#txtidacompanante").val(data[0].Id)
                            } else {
                                habilitarPersona()
                            }
                        }, error: function (err) {
                            console.error("Ha ocurrido un erro al buscar la persona")
                            habilitarPersona()
                            console.log(err)
                        }
                    })
                } else {
                    //$(inputRut).val("")
                    //$(inputDv).val("")
                    //Swal.fire({
                    //    position: 'center',
                    //    icon: 'error',
                    //    title: 'RUT Incorrecto',
                    //    showConfirmButton: false,
                    //    timer: 1500,
                    //    allowOutsideClick: false
                    //});
                }
            }

        }
    });
}

async function setDigitoVerificadorEnInput(input) {
    //console.log(input)
    //if (tipoIdentificacion.val() == 0) {
    //    tipoIdentificacion.val("1");
    //}
    //if (tipoIdentificacion.val() == 1 || tipoIdentificacion.val() == 4) {
    //        dv.val(await ObtenerVerificador(rut.val()));
    //} else {
    //        dv.val("");
    //}
}

// Función para manejar evento blur de identificacion
async function manejarBlurRut(rut, tipoIdentificacion, dv) {
    if (tipoIdentificacion.val() == 0) {
        tipoIdentificacion.val("1");
    }
    if (tipoIdentificacion.val() == 1 || tipoIdentificacion.val() == 4) {
        dv.val(await ObtenerVerificador(rut.val()));
    } else {
        dv.val("");
    }
}

function ocultarMostrarAcompanante(divAcompanante, mostrar) {
    if (mostrar) {
        $(divAcompanante).show()
    } else {
        $(divAcompanante).hide()
        limpiarPersona()
    }
}
function limpiarPersona() {
    $("#txtnombreacompanante , #txtapellidopcompanante, #txtapellidomacompanante").val("")
}
function deshabilitarPersona() {
    $("#txtnombreacompanante , #txtapellidopcompanante, #txtapellidomacompanante").attr("disabled", true)
}
function habilitarPersona() {
    $("#txtnombreacompanante , #txtapellidopcompanante, #txtapellidomacompanante").attr("disabled", false)
}
function ComboTipoEscolaridad() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`;
    setCargarDataEnCombo(url, false, "#sltEscolaridad");
}
function ComboOcupacion() {
    let url = `${GetWebApiUrl()}GEN_Ocupaciones/Combo`;
    setCargarDataEnCombo(url, false, "#sltOcupacion");
}
function ComboCategoriaOcupacion() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Categoria_Ocupacion/Combo`;
    setCargarDataEnCombo(url, false, "#sltCategoriaOcupacional");
}

function comboIdentificacion(selector, idIdentificacion = "1") {
    let url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    setCargarDataEnCombo(url, false, selector);
    $(selector).val(idIdentificacion);
}

function CargarComboSexosPacientes() {

    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, false, "#sltsexoPac");
}
function CargarComboGenerosPacientes() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, false, "#sltgeneroPac");
}
function ComboPais() {
    let url = `${GetWebApiUrl()}GEN_Pais/Combo`;
    setCargarDataEnCombo(url, false, "#sltPais");
}

function ComboRegion(slt, idPais) {   
    
    if (EsValidaSesionToken(slt)) {
        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Pais/${idPais}/Region`,
            async: false,
            success: function (data, status, jqXHR) {
                //Pais extranjero sin regiones debe ocultar la region,provincia y la ciudad
                if (data.length < 1) {
                    $(slt).parent().addClass("d-none");
                    $("#sltProvincia").parent().addClass("d-none");
                    $("#sltCiudad").parent().addClass("d-none");
                } else {
                    $(slt).parent().removeClass("d-none");
                    $("#sltProvincia").parent().removeClass("d-none");
                    $("#sltCiudad").parent().removeClass("d-none");
                }
                $.each(data, function () {
                    $(slt).append(`<option value='${this.Id}'>${this.Valor}</option>`);
                });
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar region: ${JSON.stringify(jqXHR)}`);
            }
        });
    }

}
function ComboProvincia(slt, idRegion) {
    if (idRegion != '' && idRegion != null && idRegion != undefined && EsValidaSesionToken(slt)) {

        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");

        let url = `${GetWebApiUrl()}GEN_Region/${idRegion}/Provincias`;
        setCargarDataEnCombo(url, false, slt);        
    }
}
function ComboComuna(slt, idProvincia) {

    if (idProvincia != null && idProvincia != undefined && idProvincia != '' && EsValidaSesionToken(slt)) {

        $(slt).removeAttr("disabled").empty().append("<option value='0'>Seleccione</option>");
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Comuna/Provincia/${idProvincia}`,
            async: false,
            success: function (data, status, jqXHR) {
                $.each(data, function () {
                    $(slt).append(`<option value='${this.Id}'>${this.Valor}</option>`);
                });
            },
            error: function (jqXHR, status) {
                console.log(`Error al llenar Comuna ${JSON.stringify(jqXHR)}`);
            }
        });
    }
}
//para llenar el combo de tipo representante, tutor acompanante... etc..
function ComboTipoRepresentante(select) {
    let url = `${GetWebApiUrl()}GEN_Tipo_Representante/Combo`;
    setCargarDataEnCombo(url, false, $(select));
}