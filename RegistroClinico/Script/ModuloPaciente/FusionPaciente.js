﻿
$(document).ready(function () {
    ShowModalCargando(false);
    var sSession = getSession();
    RevisarAcceso(false, sSession);

    cargarCombos();
    bloquearDatos();

    //ObtenerVerificador

    //Origen
    $("#sltIdentificacion").on('change', function () {
        if ($("#sltIdentificacion").val() != 1 && $("#sltIdentificacion").val() != 4) {
            $("#txtDigitoPac").hide();
            $("#digitoSeparacion").hide();
        } else {
            $("#txtDigitoPac").show();
            $("#digitoSeparacion").show();
        }
    });

    $('#txtnumerotPac').on('blur', async function () {
        $('#txtDigitoPac').val(await ObtenerVerificador($(this).val()));
    });

    $('#btnBuscarOrigen').click(function (e) {
        getTablaPaciente(sSession,"origen")
    });
    

    //Destino
    $("#sltIdentificacionD").on('change', function () {
        if ($("#sltIdentificacionD").val() != 1 && $("#sltIdentificacionD").val() != 4) {
            $("#txtDigitoPacD").hide();
            $("#digitoSeparacionD").hide();
        } else {
            $("#txtDigitoPacD").show();
            $("#digitoSeparacionD").show();
        }
    });

    $('#txtnumerotPacD').on('blur', async function () {
        $('#txtDigitoPacD').val(await ObtenerVerificador($(this).val()));
    });

    $('#btnBuscarOrigenD').click(function (e) {
        getTablaPaciente(sSession, "destino")
    });

})

function bloquearDatos() {
    $("#txtNombrePaciente").attr("disabled", "disabled");
    $("#txtNombrePacienteD").attr("disabled", "disabled");
    $("#txtFechaNacimiento").attr("disabled", "disabled");
    $("#txtFechaNacimientoD").attr("disabled", "disabled");
    $("#txtEdad").attr("disabled", "disabled");
    $("#txtEdadD").attr("disabled", "disabled");
    $("#txtDigitoPac").attr("disabled", "disabled");
    $("#txtDigitoPacD").attr("disabled", "disabled");

}
//Cargar en Combo
function cargarCombos(){
    CargarComboIdentificadores();
}

function CargarComboIdentificadores() {

    //let url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    //setCargarDataEnCombo(url, false, $('#sltIdentificacion'))

    $("#sltIdentificacion").empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $("#sltIdentificacion").append(`<option value='${val.GEN_idIdentificacion}'>${val.GEN_nombreIdentificacion}</option>`);
                $("#sltIdentificacionD").append(`<option value='${val.GEN_idIdentificacion}'>${val.GEN_nombreIdentificacion}</option>`);
            });
            $("#lblIdentificacion").text($("#sltIdentificacion").children("option:selected").text());
            $("#lblIdentificacionD").text($("#sltIdentificacion").children("option:selected").text());

        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Seleccion de Identificación: ${JSON.stringify(jqXHR)}`);
        }
    });

}


function getTablaPaciente(sSession, opcion) {
    if(opcion == "origen") {
        idIdentificacion = $("#sltIdentificacion").val();
        idRut = $("#txtnumerotPac").val();
    }else{
        idIdentificacion = $("#sltIdentificacionD").val();
        idRut = $("#txtnumerotPacD").val();
    }

    var adataset = [];

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${idIdentificacion}/${idRut}`,
        contentType: "application/json",
        dataType: 'json',
        async: false,
        headers: { 'Authorization': GetToken() },
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    val.GEN_idPaciente,
                    val.GEN_nombrePaciente + " " + val.GEN_ape_paternoPaciente + " " + val.GEN_ape_maternoPaciente,
                    moment(new Date(val.GEN_fec_nacimientoPaciente)).format('DD-MM-YYYY'),
                    val.Edad.edad
                ]);
            });

            if (opcion == "origen") {
                LlenaGrillaTabla(adataset, '#tblOrigen', sSession,"origen");
            } else {
                LlenaGrillaTabla(adataset, '#tblDestino', sSession, "destino");
            }
        }
    });
}


function LlenaGrillaTabla(datos, grilla, sSession, opcion) {

    $(grilla).addClass("nowrap").addClass("dataTable").DataTable({
        data: datos,
        columns: [
            { title: "ID Paciente", className: "text-center" },
            { title: "Nombre Paciente", className: "text-center" },
            { title: "Fecha Nacimiento", className: "text-center" },
            { title: "Edad", className: "text-center" },
            { title: "", className: "text-center" },
        ],

        "columnDefs": [
            { "targets": 1, "sType": "date-ukShort" },
            {
                "targets": 4,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;

                    if (opcion == "origen") {
                        var botones = `
                        <input id="ckbx${datos[fila][0]}" type="checkbox" onchange="checkboxOrigen(${datos[fila][0]}, 'origen', this)" />
                    `;
                    } else {
                        var botones = `
                        <input id="ckbx${datos[fila][0]}" type="checkbox" onchange="checkboxOrigen(${datos[fila][0]}, 'destino' , this )" />
                    `;
                    }
                   
                    return botones
                }
            }
        ],
        "bDestroy": true
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function checkboxOrigen(element, opcion, input) {
    if ($(input).is(':checked')) {
        if (opcion == "origen") {
            $("#idPacienteOrigenSel").val(element);
        } else {
            $("#idPacienteDestinoSel").val(element);
        }
    } else {
        if (opcion == "origen") {
            $("#idPacienteOrigenSel").val("");
        } else {
            $("#idPacienteDestinoSel").val("");
        }
    }
}


function FusionarPacienteConfirmacion() {
    if ($("#idPacienteDestinoSel").val() == "" || $("#idPacienteOrigenSel").val() == "") {
        ShowModalAlerta("ADVERTENCIA", "Falta seleccionar datos", "Se debe seleccionar un paciente de origen y uno de destino", null, null, '', 'Cerrar');
    } else {
        ShowModalAlerta('CONFIRMACION', 'Fusionar Pacientes', '¿Está seguro que desea fusionar los pacientes?', FusionarPaciente);
    }
    
}

function FusionarPaciente() {
    var datos = {};
    datos.IdPacienteOrigen = $("#idPacienteOrigenSel").val();
    datos.IdPacienteDestino = $("#idPacienteDestinoSel").val();
    console.log("FUSIONANDO")
    $.ajax({
        type: 'PATCH',
        data: JSON.stringify(datos),
        url: `${GetWebApiUrl()}GEN_Paciente/FusionarPaciente`,
        contentType: "application/json",
        dataType: 'json',
        async: false,
        headers: { 'Authorization': GetToken() },
        success: function (data) {
            
            toastr.success('Datos fusionados correctamente', '', {
                onHidden: function () {
                    window.location.replace(ObtenerHost() + "/Vista/ModuloPaciente/FusionPaciente.aspx");
                }
            });
            
        }
    });
}


            