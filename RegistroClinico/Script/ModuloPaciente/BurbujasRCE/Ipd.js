
const columnasIpd = [{ title: 'ID' }, { title: '�MBITO' }, { title: 'PATOLOG�A GES' }, { title: 'DIAGN�STICO' }, { title: 'FECHA' }, { title: '' }];

async function getDatosIpd(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const ipds = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_IPD/Paciente/${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return ipds

    } catch (error) {
        console.error("Error IPD del paciente")
        console.log(JSON.stringify(error))
    }
}
async function preparaDatosTablaIpd(ipds) {

    if (ipds === undefined || ipds === null)
        return

    let adataset = []

    $.each(ipds, function () {
        adataset.push([
            this.RCE_idIPD,
            this.GEN_nombreAmbito,
            (this.RCE_descripcion_Patologia_GES != null) ? this.RCE_descripcion_Patologia_GES : '<span style="color:gray">No registra descripci�n de Patolog�a</span>',
            this.RCE_diagnosticoIPD,
            (this.RCE_fechaIPD != null) ? moment(new Date(this.RCE_fechaIPD)).format('DD-MM-YYYY') : '',
        ]);
    });

    return adataset
}

async function llenarGrillaIpd(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasIpd,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/'
                                        onclick="ImprimirDocumento(${adataset[fila][0]}, 'Epicrisis');" data-print='true'
                                        data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}
async function cargarIpd(idPaciente) {
    const ipds = await getDatosIpd(idPaciente)
    const adataset = await preparaDatosTablaIpd(ipds)
    await llenarGrillaIpd(adataset, "#tblIPD")
}