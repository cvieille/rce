﻿
const columnasBiopsias = [{ title: 'ID' }, { title: 'N° REGISTRO' }, { title: 'FECHA RECEPCION' }, { title: 'ORGANO' }, { title: 'ESTADO' }, { title: '' }];

async function getDatosBiopsias(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const biopsias = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?idPaciente=${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return biopsias || [];

    } catch (error) {
        console.error("Error biopsias del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaBiopsias(biopsias) {

    if (biopsias === undefined || biopsias === null || biopsias === 0)
        return

    let adataset = []
    let stringBiopsia = ""

    $.each(biopsias, function () {

        if (this.OrganoBiopsia.length <= 0)
            stringBiopsia = "No registra órganos"
        else
            this.OrganoBiopsia.map(e => {
                stringBiopsia += e.Valor + " "
            })

        adataset.push(
            [
                this.Id,
                this.Numero,
                (this.FechaRecepcion != null) ? moment(new Date(this.FechaRecepcion)).format('DD-MM-YYYY') : '',
                stringBiopsia,
                this.Estado.Valor
            ]
        );
    });

    return adataset
}

async function llenarGrillaBiopsias(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasBiopsias,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones =
                        `<a id='imprimirBIO_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/' onclick='imprimirBIO(this);'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],

        "bDestroy": true
    });
}
async function cargarGrillaBiopsia(idPaciente) {
    const biopsias = await getDatosBiopsias(idPaciente)
    const adataset = await preparaDatosTablaBiopsias(biopsias)
    await llenarGrillaBiopsias(adataset, "#tblBiopsia")
}