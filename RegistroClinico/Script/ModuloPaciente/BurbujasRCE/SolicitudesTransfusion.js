const columnasTransfusiones = [{ title: 'ID' }, { title: 'FECHA' }, { title: '�MBITO' }, { title: 'TIPO' }, { title: 'MOTIVO' }, { title: 'PRIORIDAD' }, { title: 'ESTADO' }, { title: '' }];

async function getDatosTransfusiones(idPaciente) {
    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const transfusiones = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}RCE_Solicitud_Transfusion/Buscar?idPaciente=${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return transfusiones

    } catch (error) {
        console.error("Error en Solicitud de Transfusi�n del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTransfusiones(transfusiones) {
    if (transfusiones === undefined || transfusiones === null)
        return

    let adataset = []

    $.each(transfusiones, function () {
        adataset.push([
            this.Id,
            moment(this.FechaSolicitud).format("DD-MM-YYYY HH:mm"),
            this.TipoAmbito.Valor,
            this.TipoSolicitud.Valor,
            this.TipoMotivoTransfusion.Valor,
            this.TipoPrioridad.Valor,
            this.TipoEstadoSistema.Valor,
        ]);
    });

    return adataset
}

async function llenarGrillaTransfusiones(adataset, tabla) {
    $(tabla).addClass("nowrap").DataTable({
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasTransfusiones,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/'
                                        onclick="ImprimirDocumento(${adataset[fila][0]}, 'Epicrisis');" data-print='true'
                                        data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

async function cargarTransfusiones(idPaciente) {
    const transfusiones = await getDatosTransfusiones(idPaciente)
    const adataset = await preparaDatosTransfusiones(transfusiones)
    await llenarGrillaTransfusiones(adataset, "#tblSolicitudTransfusion")
    //console.log(columnasTransfusiones)
}

//EJEMPLO ANTIGUO
//function getSolicitudTransfusion() {

//    var idPaciente = $('#idPaciente').val();
//    var adataset = [];
//    var columnas = [{ title: 'ID' }, { title: 'FECHA' }, { title: '�MBITO' }, { title: 'TIPO' }, { title: 'MOTIVO' }, { title: 'PRIORIDAD' }, { title: 'ESTADO' }, { title: '' }];
//    var url = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/Buscar?idPaciente=${idPaciente}`;

//    LlenaGrilla(adataset, '#tblSolicitudTransfusion', columnas, 'SolicitudTransfusion', null);

//    $.ajax({
//        type: 'GET',
//        url: url,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (data) {
//            try {
//                if ($.isEmptyObject(data)) {
//                    mostrarPillSinDatosRegistroClinico("Solicitud Transfusion");
//                } else {
//                    $('#alertRegistroClinico').hide();
//                    $.each(data, function () {
//                        adataset.push(
//                            [
//                                this.Id,
//                                moment(this.FechaSolicitud).format("DD-MM-YYYY HH:mm"),
//                                this.TipoAmbito.Valor,
//                                this.TipoSolicitud.Valor,
//                                this.TipoMotivoTransfusion.Valor,
//                                this.TipoPrioridad.Valor,
//                                this.TipoEstadoSistema.Valor,
//                            ]
//                        );
//                    });
//                }
//            } catch (err) {
//                console.log("error: " + err.message);
//            }
//        }
//    });
//}
