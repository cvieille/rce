﻿
const columnasHospitalizaciones = [{ title: 'ID' }, { title: 'FECHA INGRESO' }, { title: 'FECHA EGRESO' }, { title: 'ESTADO' }, { title: 'DIAGNÓSTICO' }, { title: 'PROCEDENCIA' }, { title: 'ÁMBITO' }, { title: '' }, { title: '' }];
//const idPaciente = $('#idPaciente').val();

async function getDatosHospitalizacion(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const hospitalizacion = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return hospitalizacion

    } catch (error) {
        console.error("Error hospitalizacion del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaHospitalizacion(hospitalizaciones) {

    if (hospitalizaciones === undefined || hospitalizaciones === null)
        return

    let adataset = []

    $.each(hospitalizaciones, function () {
        adataset.push([
            this.Id,
            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY') : '',
            (this.FechaEgreso != null) ? moment(new Date(this.FechaEgreso)).format('DD-MM-YYYY') : '<span style="color:gray">No se ha egresado</span>',
            this.Estado.Valor,
            (this.Epicrisis != null) ? this.Epicrisis.DiagnosticoPrincipal.split('\n').join('<br>') : '<span style="color:gray">No registra epicrisis</span>',
            this.ProcedenciaIngreso,
            this.Ambito.Valor]);
    });

    return adataset
}

async function llenarGrillaHospitalizacion(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasHospitalizaciones,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: [3],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: [4],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: 7,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones =
                        `<a id='verMovimientosPacienteHos_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-block load-click' href='#/' onclick='verMovimientosPacienteHos(this);'>
                                        <i class='fa fa-list'></i>
                                    </a>`;
                    return botones;
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let id = adataset[fila][0];
                    let botones = `
                        <a id='abrirModalImprimirHosp_${fila}' data-id='${id}' 
                            class='btn btn-info btn-block load-click' href='#/' 
                            onclick='abrirModalImprimir(${id});'>
                            <i class='fa fa-print'></i>
                        </a>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

function abrirModalImprimir(id) {
    promesaAjax("GET", `HOS_Hospitalizacion/${id}`).then(async res => {
        let idEvento = res.IdEvento;
        let pacienteId = res.Paciente.Id;

        await abrirModalImprimirHosp(id, idEvento, pacienteId);
        ShowModalCargando(false);
    }).catch(error => {
        console.error("Error al realizar GET en impresión de brazaletes.");
        ShowModalCargando(false);
    });
}

async function abrirModalImprimirHosp(id, idEvento, pacienteId) {
    // Asignar datos a los botones
    $(`#btnIngresoEgreso`).data("id", id);
    $(`#btnImprimirSolicitudHos`).data("idevento", idEvento);
    $(`#btnImprimirSolicitudHos`).data("pacienteid", pacienteId);

    // Mostrar el nombre del paciente si es necesario
    const nombrePacienteHos = getNombrePacienteMovHos();
    $("#nombrePacienteHosImp").html(`Paciente: <strong>${nombrePacienteHos}</strong> | ID Hospitalización: #${id}`);

    // Mostrar el modal
    $("#mdlImprimirHospi").modal("show");
}

async function imprimirFormularioHosp(button) {
    let id = $(button).data("id")
    let tipoFormulario = $(button).data("tipo")
    let idEvento = $(button).data("idevento")

    let url = GetWebApiUrl()
    switch (tipoFormulario) {
        case "ingresoegreso":
            url += `HOS_Hospitalizacion/${id}/Imprimir`;
            break;
        case "solicitudhospi":
            if (idEvento === null || idEvento === undefined) {
                // Mostrar alerta si el idEvento es null o undefined
                Swal.fire({
                    position: 'center',
                    icon: 'info',
                    title: 'El paciente no tiene solicitudes de Hospitalización.',
                    showConfirmButton: false,
                    timer: 2000
                });
                return; // Detener la ejecución si el idEvento es null
            }
            let solicitud = await getLastSolicitudHospitalizacion(idEvento)
            if (!solicitud)
                return
            url += `HOS_Solicitud_Hospitalizacion/${solicitud.Id}/Imprimir`
    }
    ImprimirApiExterno(url);
}

async function cargarGrillaHospitalizacion(idPaciente) {
    const hospitalizaciones = await getDatosHospitalizacion(idPaciente)
    const adataset = await preparaDatosTablaHospitalizacion(hospitalizaciones)
    await llenarGrillaHospitalizacion(adataset, "#tblHospitalizacion")
}

// GRILLAS DE LOS MOVIMIENTOS DE HOSPITALIZACION
async function getMovimientosHospitalizacion(idHospitalizacion) {
    try {
        const movimientos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHospitalizacion}/Movimientos`,
            contentType: 'application/json',
            dataType: 'json',
        });

        return movimientos;
    } catch (error) {
        console.error("Error al cargar los movimientos de hospitalización");
        console.log(JSON.stringify(error));
    }
}

async function llenarGrillaMovimientosHos(movimientos, tabla) {
    $(tabla).addClass("nowrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                title: `Movimiento hospitalizacion del paciente: ${getNombrePacienteMovHos()}`,
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                title: `Movimiento hospitalizacion  del paciente: ${getNombrePacienteMovHos()}`,
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        "order": [[0, "desc"]],
        data: movimientos.Movimiento,
        columns: [
            { data: "Id", title: "ID" },
            {
                data: "Fecha",
                title: "Fecha Movimiento",
                render: function (data, type, row) {
                    // Aplicar el formato deseado utilizando moment.js
                    return moment(new Date(data)).format('DD-MM-YYYY HH:mm:ss');
                }
            },
            { data: "TipoMovimiento", title: "Tipo Movimiento" },
            { data: "Usuario", title: "Usuario" }
        ],
        "bDestroy": true
    });
}

async function verMovimientosPacienteHos(element) {
    const idHospitalizacion = $(element).data('id');
    const movimientos = await getMovimientosHospitalizacion(idHospitalizacion);

    // Obtenerel nombre del paciente utilizando la funcion
    const nombrePacienteHos = getNombrePacienteMovHos();

    // Llenar la tabla de movimientos y mostrar el modal
    await llenarGrillaMovimientosHos(movimientos, "#tblMovimientosPacienteHos");

    // Mostrar nombre del paciente en el modal
    $("#nombrePacienteHos").html(`Paciente: <strong>${nombrePacienteHos}</strong> | ID Hospitalización: #${idHospitalizacion}`);

    // Mostrar el modal después de llenar la tabla
    $("#mdlVerMovimientosPacienteHos").modal("show");
}

function getNombrePacienteMovHos() {

    const nombre = $("#txtNombrePaciente").val()
    const primerApellido = $("#txtApellidoPaciente").val()
    const segundoApellido = $("#txtApellidoMPaciente").val()

    const nombrePacienteHos = `${nombre} ${primerApellido} ${segundoApellido}`

    return nombrePacienteHos
}
