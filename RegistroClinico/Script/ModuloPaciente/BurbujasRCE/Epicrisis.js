﻿
const columnasEpicrisis = [{ title: 'ID' }, { title: 'FECHA INGRESO' }, { title: 'FECHA EGRESO' }, { title: 'DIAGNÓSTICO' }, { title: '' }];

async function getDatosEpicrisis(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const epicrisis = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Epicrisis/Paciente/${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return epicrisis

    } catch (error) {
        console.error("Error epicrisis del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaEpicrisis(epicrisis) {

    if (epicrisis === undefined || epicrisis === null)
        return

    let adataset = []

    $.each(epicrisis, function () {
        adataset.push([
            this.HOS_idEpicrisis,
            (this.HOS_fecha_ingresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_ingresoEpicrisis)).format('DD-MM-YYYY') : '',
            (this.HOS_fecha_egresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_egresoEpicrisis)).format('DD-MM-YYYY') : '',
            this.HOS_diagnostico_principalEpicrisis.split('\n').join('<br>')
        ]);
    });

    return adataset
}

async function llenarGrillaEpicrisis(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasEpicrisis,
        columnDefs: [
            {
                targets: [3],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/'
                                        onclick="ImprimirDocumento(${adataset[fila][0]}, 'Epicrisis');" data-print='true'
                                        data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],

        "bDestroy": true
    });
}

async function cargarEpicrisis(idPaciente) {
    const epicrisis = await getDatosEpicrisis(idPaciente)
    const adataset = await preparaDatosTablaEpicrisis(epicrisis)
    await llenarGrillaEpicrisis(adataset, "#tblEpicrisis")
}

// EJEMPLO ANTIGUO DE COMO CAPTURAR LA DATA DE LA EPICRISIS
//function getEpicrisis() {
//    var idPaciente = $('#idPaciente').val();
//    var adataset = [];
//    var columnas = [{ title: 'ID' }, { title: 'FECHA INGRESO' }, { title: 'FECHA EGRESO' }, { title: 'DIAGNOSTICO' }, { title: '' }];
//    var url = `${GetWebApiUrl()}HOS_Epicrisis/Paciente/${idPaciente}`;
//    $.ajax({
//        type: 'GET',
//        url: url,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (data) {
//            try {
//                if ($.isEmptyObject(data)) {
//                    mostrarPillSinDatosRegistroClinico("Epicrisis");
//                } else {
//                    $('#alertRegistroClinico').hide();
//                    $.each(data, function () {
//                        adataset.push([this.HOS_idEpicrisis, (this.HOS_fecha_ingresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_ingresoEpicrisis)).format('DD-MM-YYYY') : '', (this.HOS_fecha_egresoEpicrisis != null) ? moment(new Date(this.HOS_fecha_egresoEpicrisis)).format('DD-MM-YYYY') : '', this.HOS_diagnostico_principalEpicrisis.split('\n').join('<br>')]);
//                    });
//                    LlenaGrilla(adataset, '#tblEpicrisis', columnas, 'EPICRISIS', null);
//                }
//            } catch (err) {
//                console.log("error: " + err.message);
//            }
//        }
//    });
//}


