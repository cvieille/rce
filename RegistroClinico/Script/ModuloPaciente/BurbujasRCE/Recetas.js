﻿
const columnasRecetas = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'DIAGNOSTICO' }, { title: '' }];

async function getDatosRecetas(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const recetas = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Receta_Indicaciones/Paciente/${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return recetas

    } catch (error) {
        console.error("Error recetas del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaRecetas(recetas) {

    if (recetas === undefined || recetas === null)
        return

    let adataset = []

    $.each(recetas, function () {
        adataset.push([
            this.HOS_idReceta_Indicaciones,
            (this.HOS_fecha_recetaReceta_Indicaciones != null) ? moment(new Date(this.HOS_fecha_recetaReceta_Indicaciones)).format('DD-MM-YYYY') : '',
            SaltoDeLinea(this.HOS_diagnostico_principalReceta_Indicaciones, 6)
        ]);
    });

    return adataset
}

async function llenarGrillaRecetas(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasRecetas,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/'
                                        onclick="ImprimirDocumento(${adataset[fila][0]}, 'Receta');">
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],

        "bDestroy": true
    });
}

async function cargarRecetas(idPaciente) {
    const recetas = await getDatosRecetas(idPaciente)
    const adataset = await preparaDatosTablaRecetas(recetas)
    await llenarGrillaRecetas(adataset, "#tblReceta")
}

