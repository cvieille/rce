﻿
const columnasAtencionesUrgencia = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'HORA' }, { title: 'MOTIVO' }, { title: '' }];

async function getDatosAtencionesUrgencia(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {

        const atencionesUrgencia = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/Buscar?idPaciente=${idPaciente}&filas=500`,
            contentType: 'application/json',
            dataType: 'json',
        });        
        return atencionesUrgencia

    } catch (error) {
        console.error("Error atenciones de urgencia del paciente")
        console.log(JSON.stringify(error))
    }

}

async function preparaDatosTablaAtencionesUrgencia(atnecionesUrgencia) {

    if (atnecionesUrgencia === undefined || atnecionesUrgencia === null)
        return

    let adataset = []

    $.each(atnecionesUrgencia, function () {
        adataset.push([
            this.Id,
            (this.FechaAdmision != null) ? moment(new Date(this.FechaAdmision)).format('DD-MM-YYYY') : '<span style="color:gray">No se registra fecha</span>',
            (this.FechaAdmision != null) ? moment(new Date(this.FechaAdmision)).format('HH:mm') : '<span style="color:gray">No se registra hora</span>',
            this.MotivoConsulta,
            `${this.Paciente.Nombre} ${this.Paciente.ApellidoPaterno}`,
            this.Paciente.NombreSocial,
            this.IdEvento,
            this.TipoEstadoSistema.Id
        ]);
    });

    return adataset
}

function linkMovimientosIngresoUrgencia(e) {
    let id = $(e).data("id")
    let titulo = `Urgencia #${id}`
    let _url = `${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Movimientos`

    getMovimientosSistemaGenerico(_url, titulo)
}

async function llenarGrillaAtencionesUrgencia(adataset, tabla) {
    $(tabla).DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasAtencionesUrgencia,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<div class='d-flex justify-content-center flex-nowrap'>
                            <a id='urgencia_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-md me-2' href='#/' onclick='abrirModalImprimirUc(${adataset[fila][0]}, "${adataset[fila][4]}", ${adataset[fila][5] !== null ? `"${adataset[fila][5]}"` : null}, ${adataset[fila][6]} , ${adataset[fila][7]});' style='margin-right: 5px;'>
                                <i class='fa fa-print'></i>
                            </a>
                            <a id='movimientos_${fila}' data-id='${adataset[fila][0]}' class='btn btn-info btn-md' href='#/' onclick='linkMovimientosIngresoUrgencia(this);'>
                                <i class='fa fa-list'></i>
                            </a>
                        </div>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

async function cargarGrillaAtencionesUrgencias(idPaciente) {
    const atencionesUrgencia = await getDatosAtencionesUrgencia(idPaciente)
    const adataset = await preparaDatosTablaAtencionesUrgencia(atencionesUrgencia)
    await llenarGrillaAtencionesUrgencia(adataset, "#tblUrgencia")
}

// BUSCAR REGISTROS ANTERIORES DESDE FLORENCE
function buscarInformacionFlorence(tipoBusqueda) {
    let tabla = ``, url = ``, columnas = [], columnasRender = []
    const idPaciente = parseInt($('#idPaciente').val())
    switch (tipoBusqueda) {
        case 'urgencia':
            tabla = "#tblHistorialOtrasUrgencias"
            url = `URG_Atenciones_Urgencia/HistorialFlorence/Buscar?idPaciente=${idPaciente}`
            columnas = [
                { title: "Id DAU", data: "id" },
                { title: "Nombre paciente", data: "nombre_paciente" },
                { title: "Rut", data: "rut" },
                { title: "Fecha categorizacion", data: "f_categorizacion" },
                { title: "Categorizacion", data: "triage" },
                { title: "Causa", data: "causaurgencias" },
                { title: "Diagnostico", data: "descdiagnostico" },
                { title: "Profesional que atendio", data: "nomatencion" },
                { title: "GES", data: "ges" },
                { title: "Numero DAU", data: "numero_dau" },
                { title: "Prevision", data: "prevision" },
                { title: "Tipo", data: "area" },
                { title: "Ingreso", data: "f_atencion" },
                { title: "Motivo", data: "motivo_consulta" },
                { title: "Destino alta", data: "destinoaltaurgencias" }
            ]
            columnasRender = [
                {
                    targets: [1, 2, 3, 4, 5, 6, 7], visible: false
                }
            ]
            break;
        case 'hospitalizacion':
            tabla = "#tblHistorialOtrasHospitalizacionesModal"
            url = `HOS_Hospitalizacion/HistorialFlorence/Buscar?idPaciente=${idPaciente}`
            columnas = [
                { title: "Id atencion", data: "id" },
                { title: "Nombres", data: "nombre" },
                { title: "Apellidos", data: "apellidos" },
                { title: "Rut", data: "rut" },
                { title: "Diagnostico confirmado", data: "diagnostico_confirmado" },
                { title: "Profesional alta", data: "nombre_funcionario_alta" },
                { title: "Ingreso", data: "fecha_ingreso" },
                { title: "Egreso", data: "fecha_egreso" },
                { title: "Duración", data: "dias_hospitalizado" },
                { title: "Diagnóstico", data: "diagnostico_ingreso" },
                { title: "Previsión", data: "prevision" }
            ]
            columnasRender = [
                { targets: [1, 2, 3, 4, 5, 6], visible: false },
                {
                    targets: 7,//controlar fecha  egreso
                    render: function (fecha_egreso) {
                        if (fecha_egreso == "" && fecha_egreso == null)
                            return `Sin informacion`

                        return fecha_egreso
                    }
                },
                {
                    targets: 8,//controlar dias hospitalizado
                    render: function (dias) {
                        if (dias == "" && dias == null)
                            return `Sin informacion`
                        else
                            return `${dias} días`
                    }
                }
            ]
            break;
    }
    showModalCargandoComponente(tabla)
    promesaAjax(`get`, url).then(res => {
        if ($.fn.DataTable.isDataTable(tabla))
            $(tabla).DataTable().destroy();

        $(tabla).DataTable({
            dom: 'Bfrtipl',
            buttons: [
                {
                    extend: 'excel',// Usa la extensión de Excel
                    title: 'Listado registros anteriores',
                    text: 'Exportar a excel',// mostrar texto
                    className: 'btn btn-info'
                }
            ],
            language: {
                "processing": `<i class="fa fa-cog fa-spin fa-3x"></i>`
            },
            filter: false,
            data: res,
            columns: columnas,
            columnDefs: columnasRender,
            destroy: true
        })
        hideModalCargandoComponente($(tabla))
    }).catch(error => {
        console.error("Error al buscar las hospitalizaciones de florence.")
        console.log(error)
        toastr.error("Ocurrio un error al buscar en la integración, la información no esta disponible en este momento")
    })
}