﻿
const columnasEventosAdversos = [{ title: 'ID' }, { title: 'TIPO DE FORMULARIO' }, { title: 'TIPO EVENTO' }, { title: 'FECHA DE OCURRENCIA' }, { title: 'FECHA DE NOTIFICACION' }, { title: '' }];

async function getDatosEventosAdversos(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const eventosAdversos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}NEA_Evento_Adverso/Paciente/${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return eventosAdversos

    } catch (error) {
        console.error("Error eventos adversos del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaEventosAdversos(eventosAdversos) {

    if (eventosAdversos === undefined || eventosAdversos === null)
        return

    let adataset = []

    $.each(eventosAdversos, function () {
        adataset.push(
            [
                this.NEA_idEvento_Adverso,
                this.NEA_nombreTipo_Formulario,
                this.NEA_nombreTipo_Evento,
                moment(this.NEA_fecha_hora_ocurrenciaEvento_Adverso).format("DD-MM-YYYY HH:mm"),
                moment(this.NEA_fecha_notificacionEvento_Adverso).format("DD-MM-YYYY HH:mm")
            ]
        );
    });

    return adataset
}

async function llenarGrillaEventosAdversos(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasEventosAdversos,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/' onclick='imprimirEventoAdverso(${adataset[fila][0]});'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],

        "bDestroy": true
    });
}

async function cargarEventosAdversos(idPaciente) {
    const eventosAdversos = await getDatosEventosAdversos(idPaciente)
    const adataset = await preparaDatosTablaEventosAdversos(eventosAdversos)
    await llenarGrillaEventosAdversos(adataset, "#tblEventoAdverso")
}

//Ejemplo...

//function getEventosAdversos() {

//    var idPaciente = $('#idPaciente').val();
//    var adataset = [];
//    var columnas = [{ title: 'ID' }, { title: 'TIPO DE FORMULARIO' }, { title: 'TIPO EVENTO' }, { title: 'FECHA DE OCURRENCIA' }, { title: 'FECHA DE NOTIFICACION' }, { title: '' }];
//    var url = `${GetWebApiUrl()}NEA_Evento_Adverso/Paciente/${idPaciente}`;

//    $.ajax({
//        type: 'GET',
//        url: url,
//        contentType: "application/json",
//        dataType: "json",
//        success: function (data) {
//            try {
//                if ($.isEmptyObject(data)) {
//                    mostrarPillSinDatosRegistroClinico("Evento Adverso");
//                } else {
//                    $('#alertRegistroClinico').hide();
//                    $.each(data, function () {
//                        adataset.push(
//                            [
//                                this.NEA_idEvento_Adverso,
//                                this.NEA_nombreTipo_Formulario,
//                                this.NEA_nombreTipo_Evento,
//                                moment(this.NEA_fecha_hora_ocurrenciaEvento_Adverso).format("DD-MM-YYYY HH:mm"),
//                                moment(this.NEA_fecha_notificacionEvento_Adverso).format("DD-MM-YYYY HH:mm")
//                            ]
//                        );
//                    });
//                    LlenaGrilla(adataset, '#tblEventoAdverso', columnas, 'EventoAdverso', null);
//                }
//            } catch (err) {
//                console.log("error: " + err.message);
//            }
//        }
//    });

//}


