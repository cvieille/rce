﻿
const columnasProtocoloOperatorio = [{ title: 'ID' }, { title: 'ÁMBITO' }, { title: 'DIAGNÓSTICO' }, { title: 'CIRUGÍA' }, { title: 'FECHA' }, { title: 'EVENTO' }, { title: '' }];

async function getDatosProtocoloOperatorio(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const protocolosOperatorios = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}PAB_Protocolo_Operatorio/buscar?idPaciente=${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return protocolosOperatorios

    } catch (error) {
        console.error("Error protocolos operatorios del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaProtocoloOperatorio(protocolosOperatorios) {

    if (protocolosOperatorios === undefined || protocolosOperatorios === null)
        return

    let adataset = []

    $.each(protocolosOperatorios, function () {
        adataset.push([
            this.Id,
            this.Ambito.Valor,
            this.Diagnostico,
            this.OperacionRealizada,
            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY') : '',
            this.IdEvento ?? "N/A"]);
    });

    return adataset
}

async function llenarGrillaProtocoloOperatorio(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasProtocoloOperatorio,
        columnDefs: [
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: [3],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones =
                        `<button class='btn btn-info btn-block muestraEvento' data-target='#panelBoton'
                                        data-id='${adataset[fila][0]}' onclick="ImprimirDocumento(${adataset[fila][0]}, 'ProtocoloOperatorio');"
                                        data-print='true' data-frame='#frameImprimir'>
                                        <i class='fa fa-print'></i>
                                    </button>`;
                    return botones;
                }
            }
        ],

        "bDestroy": true
    });
}

async function cargarProtocolosOperatorios(idPaciente) {
    const protocolosOperatorios = await getDatosProtocoloOperatorio(idPaciente)
    const adataset = await preparaDatosTablaProtocoloOperatorio(protocolosOperatorios)
    await llenarGrillaProtocoloOperatorio(adataset, "#tblProtocoloOperatorio")
}

