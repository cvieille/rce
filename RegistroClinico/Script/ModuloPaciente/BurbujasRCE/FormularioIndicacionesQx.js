﻿
const columnasFormularioIndQx = [{ title: 'FOLIO' }, { title: 'DIAGNOSTICO' }, { title: 'PRESTACIÓN' }, { title: 'GES' }, { title: 'UCA' }, { title: 'APTO' }, { title: 'ESTADO' }, { title: 'EVENTO' }, { title: '' }];

async function getDatosFormularioIndQx(idPaciente) {

    if (idPaciente === null || idPaciente === undefined || idPaciente <= 0)
        return

    try {
        const formulariosIndQx = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}PAB_Formulario_Ind_Qx/buscar?idPaciente=${idPaciente}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return formulariosIndQx

    } catch (error) {
        console.error("Error formularios indicacion Qx del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaFormularioIndicacionQx(formulariosIndQx) {

    if (formulariosIndQx === undefined || formulariosIndQx === null)
        return

    let adataset = []

    $.each(formulariosIndQx, function () {
        adataset.push([this.Id,
        this.Diagnostico,
        this.Prestacion,
        this.Ges,
        this.Uca,
        this.UcaApto != null ? this.UcaApto : ' - ',
        this.TipoEstado.Valor,
        this.IdEvento]);
    });

    return adataset
}

async function llenarGrillaFormularioIndQx(adataset, tabla) {

    $(tabla).addClass("nowrap").DataTable({
        "responsive": true,
        "order": [[0, "desc"]],
        data: adataset,
        columns: columnasFormularioIndQx,
        columnDefs: [
            {
                targets: [1],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: [2],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: [6],
                render: function (data, type, full, meta) {
                    return "<div style='white-space:normal;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                data: null,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    botones =
                        `<a data-id='${adataset[fila][0]}' class='btn btn-info btn-block' href='#/'
                                        onclick='imprimeFormularioIQX(${adataset[fila][0]});'>
                                        <i class='fa fa-print'></i>
                                    </a>`;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    });
}

async function cargarFormularioIndicacionesQx(idPaciente) {
    const formulariosIndQx = await getDatosFormularioIndQx(idPaciente)
    const adataset = await preparaDatosTablaFormularioIndicacionQx(formulariosIndQx)
    await llenarGrillaFormularioIndQx(adataset, "#tblFormularioIndQx")
}

