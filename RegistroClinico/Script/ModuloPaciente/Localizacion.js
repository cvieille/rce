﻿$(document).ready(function () {
    ShowModalCargando(false)
    InicializarComponentesRut("#sltTipoIdentificacion", "#txtFiltroRut", "#txtFiltroDV", "#lblTipoIdentificacion")
    setCargarDataEnCombo(`${GetWebApiUrl()}GEN_Ambito/Combo`, true, "#sltAmbito")
    comboIdentificacion("#sltTipoIdentificacion")

    $("#txtFiltroRut").on("blur", async function () {
        manejarBlurRut($("#txtFiltroRut"), $("#sltTipoIdentificacion"), $("#txtFiltroDV"));
    });
})
function buscarPaciente(filas = 1) {
    if (validarCampos("#divFiltrosBusqueda", true)) {
        let urlConsulta = ``

        $.ajax({
            method: "GET",
            url: `${GetWebApiUrl()}GEN_Paciente/buscar?numeroDocumento=${$("#txtFiltroRut").val()}&idIdentificacion=${$("#sltTipoIdentificacion").val()}`,
            success: function (data) {

                let idPaciente = data[0].IdPaciente
                let columnastabla = [
                    { title: "Id", data: "Id" },
                    { title: "Nombre", data: "Paciente" },
                    { title: "Edad", data: "Paciente.Edad.edad" }
                ]

                let columnasDef = [
                    {
                        targets: 1,
                        render: function (paciente) {
                            return `${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}`
                        }
                    },
                    {
                        targets: 3,
                        render: function (fecha) {
                            return moment(fecha).format("DD-MM-YYYY")
                        }
                    }
                ]
                if (parseInt($("#sltAmbito").val()) == 1) {
                    //Hospitalziacion
                    divtablamostrar = "#divTblLocalizacionHospitalizacion", divtablaocultar = "#divTblLocalizacionUrgencia"
                    table = "#tblLocalizacionHospitalizacion"
                    columnastabla.push(
                        { title: "Fecha admisión", data: "Fecha" },
                        { title: "Ambito", data: "Ambito.Valor" },
                        { title: "Cama", data: "Cama.Actual.Valor" },
                        { title: "Ubicacion", data: "Cama" },
                        { title: "Estado", data: "Estado.Valor" },
                    )
                    columnasDef.push(
                        {
                            targets: 5,
                            render: function (cama) {

                                if (cama.Actual !== null) {
                                    return `${cama.Actual.Valor}`
                                }
                            },
                            targets: 6,
                            render: function (cama) {
                                if (cama.Actual !== null) {
                                    return `${cama.Actual.Ubicacion.Valor}`
                                }
                            }
                        }
                    )

                    urlConsulta = `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idPaciente=${idPaciente}&filas=${filas}`

                } else if (parseInt($("#sltAmbito").val()) == 3) {
                    //Urgencia
                    divtablaocultar = "#divTblLocalizacionHospitalizacion", divtablamostrar = "#divTblLocalizacionUrgencia"
                    table = "#tblLocalizacionUrgencia"
                    columnastabla.push(
                        { title: "Fecha admision", data: "FechaAdmision" },
                        { title: "Motivo consulta", data: "MotivoConsulta" },
                        { title: "Tipo atencion", data: "NombreTipoAtencion" },
                        { title: "Estado", data: "TipoEstadoSistema.Valor" }
                    )

                    urlConsulta = `${GetWebApiUrl()}URG_Atenciones_Urgencia/Buscar?idPaciente=${idPaciente}&filas=${filas}`
                }
                $(divtablaocultar).hide()
                $(divtablamostrar).show()
                if ($.fn.DataTable.isDataTable(table))
                    $(table).DataTable().destroy();

                $(table).DataTable({
                    ajax: {
                        url: urlConsulta,
                        dataSrc: ''
                    },
                    columns: columnastabla,
                    columnDefs: columnasDef,
                    destroy: true
                });

                $.ajax({
                    method: "GET",
                    url: urlConsulta,
                    success: function (dataLocalizacion) {
                        console.log(dataLocalizacion)
                    }, error(error) {
                        console.error("Ha ocurrido un error al intentar consultar la informacion de la bandeja")
                        console.log(error)
                    }
                })
            }, error(error) {
                console.error("Ha ocurrido un error al intentar consultar la informacion del paciente")
                console.log(error)
            }
        })

    }
}