﻿
var sSession;
var idPac;
var dataTable = null;


//READY
$(document).ready(async function () {

    let IniciarComponentes = async function () {

        ShowModalCargando(true);
        await sleep(0);

        sSession = getSession();
        RevisarAcceso(true, sSession);

        if (session.ID_PERFIL == 155) { //Administrador de Sistema
            document.getElementById('btnTraerDatosDesdeFonasa').style.display = 'block';
        }

        cargaCombos();
        $.fn.bootstrapSwitch.defaults.onColor = 'info';
        $.fn.bootstrapSwitch.defaults.offColor = 'danger';
        $.fn.bootstrapSwitch.defaults.onText = 'SI';
        $.fn.bootstrapSwitch.defaults.offText = 'NO';
        $.fn.bootstrapSwitch.defaults.labelWidth = '1';
        DarFuncionRadios();

        let html = $(".divDatosAdicionalesPaciente").html();
        $(".divDatosAdicionalesPaciente").remove();
        $("#divDatosPaciente .card-body").append(`<div class='divDatosAdicionalesPaciente'>${html}</div>`);

        $("#ddlTramo").append("<option value='0'> - Seleccione - </option>");
        $(".material-icons").tooltip();
        $(".material-icons").click(function () {
            $(".material-icons").tooltip("hide");
        }); btnNuevoPaciente

        //Cuando cambia algun valor de algun input o combo asigna valor a hdf
        $(".editable").change(function () {
            if ($("#hdfEditable").val() != "SI") {
                $("#hdfEditable").val("SI");
            }
        });

        //CUando cambia algun valor de algun checkbox asigna valor a hdf
        $('.editable-switch').on('switchChange.bootstrapSwitch', function () {
            if ($("#hdfEditable").val() != "SI") {
                $("#hdfEditable").val("SI");
            }
        });

        //Cuando se hace click sobre algun item de la lista del sidebar paciente pregunta si se edito algun campo
        //si es así levanta modal de guardado
        $(".nav-link.panel-paciente").click(function () {

            if ($("#hdfEditable").val() == "SI") {

                $("#btnGuardarCambios").data("pacienteDclinico", ($("#hdfTransicion").val() === "btnDatosClinicos"));
                //Asigna a hdf la pestaña a la cual iba antes de lanzar el modal y terminar el proceso
                $("#hdfTransicion").val($(this).attr("id"));
                $("#mdlGuardado").modal("toggle");
                return false;
            }
        });

        $("#ddlTipoIdentificacionPaciente").change(async function () {
            comboTipoIdentificacion($(this).val());
        });

        //propiedades toastr
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "300",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        //Prueba del boton de evento
        $('body').on('click', '.muestraEvento', function (e) {
            var id = $(this).data("id");
            e.preventDefault();
        });

        $('body').on('change', '#ddlAseguradora', function (e) {
            if ($(this).val() != '0') {
                if ($('#ddlTramo').hasClass('disabled'))
                    $('#ddlTramo').removeClass('disabled');
                APITramo($(this).val(), null);
            }
        });
        // oculta o muestra ficha paciente
        // #sltTipoIdentificacionBusqueda #txtFiltroRut
        ocultarMostrarFichaPac($("#sltTipoIdentificacionBusqueda"), $("#txtFiltroRut"))

        $("#showControlUsuarioAlergias").hide()


        //CUando cambia algun valor de algun checkbox asigna valor a hdf
        $('#switchOtrasAlergias').on('switchChange.bootstrapSwitch', function () {
            if ($("#switchOtrasAlergias").attr("checked")) {
                $("#divTxtOtrasAlergias").removeClass("hide")
            } else {
                $("#divTxtOtrasAlergias").addClass("hide");
            }
        });

        if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico)
            $("#showControlUsuarioAlergias").show()

        //calcula edad segun fecha de nacimiento
        $('#txtFechaNacPaciente').blur(function () {
            $('#txtEdadPaciente').val(CalcularEdad($(this).val()).edad);
        });

        $('#btnNuevoPacienteModal').click(function () {
            LimpiarPaciente();
            DeshabilitarPaciente(true);
            comboTipoIdentificacionModal("#sltIdentificacion")
            $("#sltIdentificacion").val('1').change();
            $("#sltIdentificacion option[value='5']").remove();
            $("#divExistente, #btnNuevoPaciente").hide();
            CargarCombosUbicacionPaciente();
            $('#mdlNuevoPaciente').modal('show');
        });

        //CLICK GRILLA FORMULARIO IND QX
        $('#btnSolPabellon').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarFormularioIndicacionesQx(idPaciente)
        });

        //CLICK GRILLA PROTOCOLO OPERATORIO
        $('#btnProtocolo').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarProtocolosOperatorios(idPaciente)
        });

        //CLICK GRILLA RECETA
        $('#btnReceta').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarRecetas(idPaciente)
        });

        //CLICK GRILLA BIOPSIA
        $('#btnBiopsia').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarGrillaBiopsia(idPaciente)
        });

        //CLICK GRILLA HOSPITALIZACION
        $('#btnHospitalizacion').click(async function () {

            if ($.fn.DataTable.isDataTable("#tblHistorialOtrasHospitalizacionesModal"))
                $("#tblHistorialOtrasHospitalizacionesModal").DataTable().destroy();

            const idPaciente = parseInt($('#idPaciente').val())
            await cargarGrillaHospitalizacion(idPaciente)
        });

        //CLICK GRILLA URGENCIA
        $('#btnUrgencia').click(async function () {

            if ($.fn.DataTable.isDataTable("#tblHistorialOtrasUrgencias")) {
                $("#tblHistorialOtrasUrgencias").DataTable().destroy();
                $("#tblHistorialOtrasUrgencias").empty();
            }

            const idPaciente = parseInt($('#idPaciente').val())
            await cargarGrillaAtencionesUrgencias(idPaciente)
        });

        //CLICK GRILLA EPICRISIS
        $('#btnEpicrisis').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarEpicrisis(idPaciente)
        });

        //CLICK GRILLA EVENTOS ADVERSOS
        $('#btnEventoAdverso').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarEventosAdversos(idPaciente)
        });

        //CLICK GRILLA IPD
        $('#btnIPD').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarIpd(idPaciente)
        });

        //CLICK GRILLA SOLICITUDES TRANSFUSIONES
        $('#btnSolicitudTransfusion').click(async function () {
            const idPaciente = parseInt($('#idPaciente').val())
            await cargarTransfusiones(idPaciente)
        });

        $('#btnHojaEvolucion').click(function () {
            getHojasEvolucion();
        });
        $('#btnInterconsulta').click(function () {
            getInterconsultas();
        });
        $('#btnLaboratorio').click(function () {
            getLaboratorio();
        });



        //Boton de Registro Clínico
        $('#btnRegistroClinico').click(function () {

            if ($.fn.DataTable.isDataTable("#tblHistorialOtrasHospitalizacionesModal"))
                $(tabla).DataTable().destroy();
            if ($.fn.DataTable.isDataTable("#tblHistorialOtrasUrgencias"))
                $(tabla).DataTable().destroy();

            let verRCE = async function () {

                $("#modalCargando").show();
                await sleep(0);

                //Obtiene una lista de los sistemas a los cuales tiene permiso
                getPermisosSistemas();

                $('.material-icons.nav-link.active.show').removeClass('active show');
                $('.tab-pane.fade.panel-rc.active.show').removeClass('active show in');
                $('#btnSolPabellon').addClass('active show');
                $('#panel10').addClass('active show');

                //Primera vista la de FIQ
                getCountRegistroClinico();
                //getHospitalizaciones();
                getLaboratorio();
                inactivarBotonesRegistroClinico();
                $('#alertRegistroClinico').hide();
            }

            verRCE().then(() => ShowModalCargando(false));

        });

        $("#btnLocalizacion").click(function () {
            localizarPaciente()
        })

        $('#btnAtencionesPaciente').click(function () {
            getAtencionesPaciente();
        });

        //Boton Refresh de registro clinico
        $('#btnRefresh').click(function (e) {

            e.preventDefault();
            getCountRegistroClinico();
            //getFormulariosIndQx();
            //getProtocolosOperatorios();
            //getRecetas();
            //getRegistrosBiopsias();
            //getHospitalizaciones();
            //getAtencionesUrgencia();
            //getEpicrisis();
            getLaboratorio();
            //getEventosAdversos();
            getSolicitudTransfusion()
            inactivarBotonesRegistroClinico();

        });

        $("#ddlPaisPaciente").change(function () {
            if ($("#ddlPaisPaciente").val() != '0') {
                APIRegion($("#ddlPaisPaciente").val());
                $("#ddlProvinciaPaciente").val('0');
                $("#ddlCiudadPaciente").val('0');
            }
        });

        $("#ddlRegionPaciente").change(function () {
            if ($("#ddlRegionPaciente").val() != '0') {
                APIProvincia($("#ddlRegionPaciente").val());
                $("#ddlCiudadPaciente").val('0');
            }
        });

        $("#ddlProvinciaPaciente").change(function () {
            if ($("#ddlProvinciaPaciente").val() != '0') {
                APIComuna($("#ddlProvinciaPaciente").val());
            }
        });

        //Select tipo de identificación
        $('#sltTipoIdentificacion').change(function () {
            $('#txtFiltroRut').val("");
            $('#txtDv').val("");
            $('#txtFiltroRut').attr("placeholder", $(this).children("option:selected").html());
            if ($(this).val() == 1 || $(this).val() == 4) {
                $('.digito').show();
                $('#txtFiltroRut').attr("maxlength", 8);
            } else {
                $('.digito').hide();
                $('#txtFiltroRut').attr("maxlength", 10);
            }
        });

        //Select RUT
        $('#txtFiltroRut').on('blur', function () {
            ValidarRut();
        });

        $('#txtFiltroRut').on('input', function () {

            if ($.trim($(this).val()) !== "")
                $("#txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").val("").attr("disabled", "disabled");
            else
                $("#txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").removeAttr("disabled");

        });

        // Select RUT en edicion de paciente
        $('#txtRutPaciente').on('blur', function () {
            ValidarRutPacienteEdicion();
        });

        $('#txtFiltroNUI').on('input', async function () {

            if ($.trim($(this).val()) !== "")
                $("#txtFiltroRut, #txtDv, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe").val("").attr("disabled", "disabled");
            else
                $("#txtFiltroRut, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe").removeAttr("disabled");

        });

        $('#txtRutPac').on('blur', async function () {
            ('#txtRutDigitoPaciente').val(await ObtenerVerificador($(this).val()));
        });

        $('#txtEmailPaciente').on('blur', function () {
            var valido = caracteresCorreoValido($(this));
        });

        $('.buscar').on('keypress', async function (e) {
            if (e.keyCode == 13) {
                ValidarRut();
                await seleccionaPaciente();
            }
        });

        $('#btnFiltroBandeja').click(async function () {

            ShowModalCargando(true);

            // Si no es NN, realizar la búsqueda normal

            await seleccionaPaciente();

            if ($("#txtFiltroRut").val() !== "") {

                let pacienteAlergia = await buscarPacientePorIdentificacionPac($("#sltTipoIdentificacionBusqueda").val(), $("#txtFiltroRut").val());
                if (pacienteAlergia === undefined)
                    return;

                linkMostrarModalAlergias(null, 0, pacienteAlergia.IdPaciente);
            }

            ShowModalCargando(false);

        });

        $('#ddlAseguradora').change(function () {
            if ($(this).val() != '1' && $(this).val() != '2') {
                $('#divPrais').hide();
            } else {
                $('#divPrais').show();
            }
        });

        cargaPerfiles();
        CargarComboTipoRiesgo();
        ComboNacionalidad($("#sltNacionalidadPaciente"));
        deleteSession('ID_PACIENTE');

        $('#btnNuevoPaciente').click(async function (e) {

            if (!validarCampos(".divNuevoModalBody", true)) {
                return false
            }
            await GuardarPaciente();
            $("#sltTipoIdentificacion").val($("#sltIdentificacion").val());
            $("#txtFiltroRut").val($("#txtnumerotPac").val());
            ValidarRut();
            $('#mdlNuevoPaciente').modal('hide');
            //seleccionaPaciente();
        });

        $("#btnLimpiarFiltros").click(function () {
            $("#sltTipoIdentificacion").val(1);
            $("#txtFiltroRut, #txtDv, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").val("");
            $("#txtFiltroRut, #txtFiltroNombre, #txtFiltroApe, #txtFiltroSApe, #txtFiltroNUI").removeAttr("disabled");
            limpiaCampos();
            $('#divPacientes').hide();
        });

        OyentesNuevoPaciente();
    }

    // CONTROLANDO txtNuiPaciente PARA PERMITIR SOLO NUMEROS
    $("#txtNuiPaciente").on("input", function (event) {
        let inputValue = $(this).val();
        // Reemplazar cualquier caracter que no sea un número con una cadena vacía
        $(this).val(inputValue.replace(/\D/g, ''));
    });

    $("#divRegionPaciente, #divProvincia, #divCiudad").show()

    IniciarComponentes().then(() => ShowModalCargando(false));
});

function comboTipoIdentificacionModal(element) {

    //let url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    //setCargarDataEnCombo(url, false, $('#sltIdentificacion'))

    $(element).empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $(element).append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });
            //$("#lblIdentificacion").text($(element).children("option:selected").text());
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Seleccion de Identificación: ${JSON.stringify(jqXHR)}`);
        }
    });
}
function CargarComboIdentificadores(element) {

    //let url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    //setCargarDataEnCombo(url, false, $('#sltIdentificacion'))

    $(element).empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function (key, val) {
                $("#sltTipoIdentificacionBusqueda").append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });
            $("#lblIdentificacion").text($(element).children("option:selected").text());
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar Seleccion de Identificación: ${JSON.stringify(jqXHR)}`);
        }
    });
}
function removeDataRequiredRegionProvinciaCiudad(isRemove) {

    if (isRemove) {
        $("#ddlRegionPaciente").removeAttr("data-required")
        $("#ddlProvinciaPaciente").removeAttr("data-required")
        $("#ddlCiudadPaciente").removeAttr("data-required")
    }
    else {
        $("#ddlRegionPaciente").attr("data-required", true)
        $("#ddlProvinciaPaciente").attr("data-required", true)
        $("#ddlCiudadPaciente").attr("data-required", true)
    }
}
function ocultaRegionSiEsDistintadeChile(paisorigen, region, provincia, ciudad) {

    if ($(`${paisorigen}`).val() !== "1") {
        $(`${region},${provincia},${ciudad}`).hide()
        removeDataRequiredRegionProvinciaCiudad(true)
    }
    else {
        $(`${region},${provincia},${ciudad}`).show()
        removeDataRequiredRegionProvinciaCiudad(false)
    }

    $(paisorigen).change(function () {
        let value = $(this).val()
        if (value === "1") {
            $(`${region},${provincia},${ciudad}`).show()
            $(`${provincia},${ciudad}`).removeClass("editable")
        }
        else {
            $(`${region},${provincia},${ciudad}`).hide()
        }
    })
}

function OyentesNuevoPaciente() {

    $("#sltIdentificacion").change(function () {
        $("#divInfoPaciente, #divUbicacionPaciente").show();
        $("#btnNuevoPaciente, #divExistente").hide();
    });

    $("#txtnumerotPac").on('input', function () {
        // 1 = RUT / 4 = RUT MATERNO
        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {
            $("#divInfoPaciente, #divUbicacionPaciente").show();
            $("#divExistente").hide();
        }
    });

    $("#txtnumerotPac").on("blur", async function () {
        if ($("#sltIdentificacion").val() === '2' || $("#sltIdentificacion").val() === '3') {
            if ($.trim($("#txtnumerotPac").val()) != "") {
                const paciente = await buscarPacientePorIdentificacionPac($("#sltIdentificacion").val(), $("#txtnumerotPac").val())
                if (paciente != undefined) {
                    $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").hide();
                    $("#divExistente").show();
                } else {
                    LimpiarPaciente();
                    $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").show();
                    $("#divExistente").hide();
                }
            } else {
                $("#divInfoPaciente, #divUbicacionPaciente").show();
                $("#btnNuevoPaciente, #divExistente").hide();
            }
        }
    });

    $("#txtDigitoPac").on("input", async function () {

        if ($("#sltIdentificacion").val() === '1' || $("#sltIdentificacion").val() === '4') {

            const paciente = await buscarPacientePorIdentificacionPac($("#sltIdentificacion").val(), $("#txtnumerotPac").val())

            if (paciente !== undefined) {

                $("#divInfoPaciente, #divUbicacionPaciente, #btnNuevoPaciente").hide();
                $("#divExistente").show();
            } else {
                $("#divInfoPaciente, #divUbicacionPaciente").show();
                $("#divExistente").hide();
                if ($.trim($("#txtDigitoPac").val()) != "")
                    $("#btnNuevoPaciente").show();
            }
        }
    });
}

const buscarPacientePorIdentificacionPac = async (idIdentificacion, numeroDocumento) => {

    try {
        const paciente = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/Buscar?numeroDocumento=${numeroDocumento}&idIdentificacion=${idIdentificacion}`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return paciente[0]

    } catch (error) {
        console.error("Error al cargar paciente")
        console.log(JSON.stringify(error))
    }
}

async function ValidarRut() {
    if ($('#txtFiltroRut').val() != "" && ($('#sltTipoIdentificacionBusqueda').val() == '1' || $('#sltTipoIdentificacionBusqueda').val() == '4')) {
        let dig = await ObtenerVerificador($('#txtFiltroRut').val());
        if (dig !== null)
            $('#txtDv').val(dig);
        else
            $('#txtDv, #txtFiltroRut').val("");
    } else
        $('#txtDv').val("");
}

// Valida y calcula el digito verificador en la edicion de un paciente
async function ValidarRutPacienteEdicion() {
    if ($('#txtRutPaciente').val() != "" && ($('#sltTipoIdentificacion').val() == '1' || $('#sltTipoIdentificacion').val() == '4')) {
        let dig = await ObtenerVerificador($('#txtRutPaciente').val());
        if (dig !== null)
            $('#txtRutDigitoPaciente').val(dig);
        else
            $('#txtDv, #txtRutPaciente').val("");
    } else
        $('#txtRutDigitoPaciente').val("");
}

//FUNCIONES
function inactivarBotonesRegistroClinico() {
    $('.material-icons.nav-link').removeClass('active').removeClass('show');
    $('.panel-rc').removeClass('in').removeClass('active').removeClass('show');
}

function getAtencionesPaciente() {

    let adataset = [];

    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Vista/ModuloPaciente/BandejaPAC.aspx/bandeja",
        contentType: "application/json",
        data: JSON.stringify({ s: $('#idPaciente').val() }),
        dataType: "json",
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (i, r) {
                adataset.push([
                    r.GEN_idAtencion_Paciente_Establecimiento,
                    r.GEN_nombreEstablecimiento,
                    r.GEN_fechaAtencion_Paciente_Establecimiento
                ]);
            });

        }
    });

    $('#tblAtenciones').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "ID Atención" },
            { title: "Establecimiento" },
            { title: "Fecha atención" }
        ],
        columnDefs: [
            {
                targets: [0],
                searchable: false
            }
        ],
        bDestroy: true
    });
}

function getPermisosSistemas() {

    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Usuarios/Sistemas`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            $.each(data, function () {
                $("[data-id-sistema='" + this.GEN_idSistemas + "']").removeClass("disabled");
                $("[data-id-sistema='" + this.GEN_idSistemas + "'] > span").removeClass("hide");
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error:' + xhr.responseText);
        }
    });
}


function CargarComboTipoRiesgo() {
    let url = `${GetWebApiUrl()}/PAB_Tipo_Riesgo/Combo`;
    setCargarDataEnCombo(url, false, $("#sltTabaco, #sltOh"));
}

function guardarPaciente_click(e) {

    if (!validarCampos("#divDPaciente", true)) {
        return
    }

    var valid = validaCampos();
    if (!valid) {
        toastr.error("Falta completar algunos campos obligatorios");
        return;
    }

    const esMedico = sSession.CODIGO_PERFIL === perfilAccesoSistema.medico;
    guardaPaciente(esMedico);
}

function guardarDatosClinicos_click(e) {
    //var valid = validaCampos();
    if (/*!valid | */!validarCampos("#divHabitos", true)) {
        toastr.error("Falta completar algunos campos obligatorios");
        //e.preventDefault();
    } else
        guardaPacienteDClinico()
    //guardaPaciente(true);
}

function guardarDatosPrevisionales_click(e) {

    var valid = validaCampos();

    if (!valid) {
        toastr.error("Falta completar algunos campos obligatorios");
    }
    else
        guardaPaciente(false);
}

async function cargarPrevisionApiFonasa() {
    ShowModalCargando(true);

    let numeroDocumento = $("#txtFiltroRut").val();
    let mensaje = "";
    let verificarFonasa = true;

    try {
        if (numeroDocumento === "") {
            let numeroDocumentoBandeja = $("#lblRutPacientePill").html();
            let rut = numeroDocumentoBandeja.split("-");
            numeroDocumento = rut[0];
        }

        const personaFonasa = await GetApiFonasa(numeroDocumento);

        if (personaFonasa === undefined) {
            mensaje = "PREVISIÓN NO DISPONIBLE";
        } else if (personaFonasa.coddesc === 'BLOQUEADO SIN INFORMACION') {
            mensaje = "BLOQUEADO SIN INFORMACION";
        }

        if (mensaje !== "") {
            toastr.error(mensaje);
            ShowModalCargando(false);
            return;
        }

        if (personaFonasa.replyTO.estado === 0) {
            if (personaFonasa.beneficiarioTO.rutbenef === null) {
                ShowModalCargando(false);
                return;
            }

            // ISAPRE Y TRAMO
            await cargarPrevisionIsapre(personaFonasa, numeroDocumento, "#ddlAseguradora", "#ddlTramo");

            // FONASA Y TRAMO
            await cargarPrevisionFonasa(personaFonasa, "#ddlAseguradora", "#ddlTramo");

            // PRAIS
            if (personaFonasa.codigoprais !== "") {
                let codPrais = (personaFonasa.codigoprais === "111") ? true : false;
                $('#switchPrais').bootstrapSwitch('state', codPrais);
            }
        }
    } catch (error) {
        if (verificarFonasa) {
            $("#divInfoFonasa").hide();
        }
        console.error("Error en cargarPrevisionApiFonasa bandeja: ", error);
        ShowModalCargando(false);
        return;
    }

    ShowModalCargando(false);
}

async function verMovimientosPaciente() {

    const idPaciente = parseInt($('#idPaciente').val())
    const nombrePaciente = `<span> Paciente: </span><strong> ${getNombrePacienteMov()}</strong>`

    $("#nombrePaciente").html(nombrePaciente)

    const datos = await getMovimientosPaciente(idPaciente)
    const movimientos = await preparaDatosTablaMovimientos(datos)
    await dibujarTablaMovimientosPaciente("#tblMovimientosPaciente", movimientos)

    $("#mdlVerMovimientosPaciente").modal("show")
}

// VER LOS MOVIMIENTOS DE HOSPITALIZACIONES
//async function verMovimientosPacienteHos() {

//    const idHospitalizacion = $(element).data('id');
//    const nombrePacienteHos = `<span> Paciente: </span><strong> ${getNombrePacienteMovHos()}</strong>`

//    $("#nombrePacienteHos").html(nombrePacienteHos)

//    const datos = await getMovimientosHospitalizacion(idHospitalizacion)
//    console.log(datos)
//    const movimientos = await preparaDatosTablaMovimientosHos(datos)
//    await llenarGrillaMovimientosHos("#tblMovimientosPacienteHos", movimientos)

//    $("#mdlVerMovimientosPacienteHos").modal("show")
//}


//VALIDA (Hans)
function validaCampos() {
    let valido = true;
    //Datos paciente
    valido = ValidarTxt($("#txtNombrePaciente"), valido);
    valido = ValidarTxt($("#txtApellidoPaciente"), valido);
    valido = ValidarTxt($("#txtFechaNacPaciente"), valido);
    valido = ValidarTxt($("#ddlSexoPaciente"), valido);
    valido = ValidarTxt($("#ddlGeneroPaciente"), valido);
    valido = ValidarTxt($("#sltNacionalidadPaciente"), valido);
    if ($('#ddlTipoIdentificacionPaciente').val() == 1 || $('#ddlTipoIdentificacionPaciente').val() == 4) {
        //valido = ValidarTxt($("#txtTelefonoPaciente"), valido);
    }
    valido = caracteresCorreoValido($('#txtEmailPaciente'), valido);
    return valido;
}

//Cambia de tipo de identificación
async function comboTipoIdentificacion(value) {
    if (value == 1 || value == 4) {
        $("#txtRutPaciente").show();
        $("#RutPaciente").show();
        $("#txtRutDigitoPaciente").show();
        $("#lblRutPaciente").show();
        //calcula rut
        if ($("#txtRutPaciente").val() != null) {
            $("#txtRutDigitoPaciente").val(await ObtenerVerificador($("#txtRutPaciente").val()));
        }
    } else {
        $("#txtRutDigitoPaciente").val(null);
        if (value == 5) {
            $("#lblRutPaciente").hide();
            $("#txtRutPaciente").hide();
            $("#RutPaciente").hide();
            $("#txtRutDigitoPaciente").hide();
        } else {
            $("#lblRutPaciente").show();
            $("#txtRutPaciente").show();
            $("#RutPaciente").show();
            $("#txtRutDigitoPaciente").hide();
        }
    }
}

//GET API REST Para Traer objeto Paciente completo
async function seleccionaPaciente() {

    $("#divPacientes").hide();

    if ($('#txtFiltroRut').val() != "") {

        let valido = await validaFiltroRut("#sltTipoIdentificacionBusqueda")
        let iden = '';
        let rut = '';

        if (!valido) {
            toastr.error("Debe seleccionar un tipo de identificación para buscar al paciente por ficha clínica.");
            //event.preventDefault();
        }
        //ddlFiltroIdentificacion-

        iden = $('#sltTipoIdentificacionBusqueda').val();
        rut = $('#txtFiltroRut').val();

        //Obtener el Paciente por RUT
        getPacienteRut(iden, rut);
    } else {

        let nombre = '', apep = '', apem = '', nui = '', idIdentificacion = '';

        if ($.trim($('#txtFiltroNombre').val()) != "")
            nombre = $.trim($('#txtFiltroNombre').val());
        if ($.trim($('#txtFiltroApe').val()) != "")
            apep = $.trim($('#txtFiltroApe').val());
        if ($.trim($('#txtFiltroSApe').val()) != "")
            apem = $.trim($('#txtFiltroSApe').val());
        if ($.trim($("#txtFiltroNUI").val()) != "")
            nui = $.trim($("#txtFiltroNUI").val());
        if ($('#sltTipoIdentificacionBusqueda').val() == 5)
            idIdentificacion = 5;

        if (nombre != "" || apep != "" || apem != "" || nui != "" || idIdentificacion != '') {
            $("#divPacientes").show();
            getPacienteFiltro(nombre, apep, apem, nui, idIdentificacion);
        } else {
            toastr.error("Debe ingresar algún dato de búsqueda (Documento o nombres del paciente).");
            limpiaCampos();
        }
    }
}

//Valida que al buscar por ficha clínica haya seleccionado algun identificador
async function validaFiltroRut(element) {

    let idenvalido;

    if ($(element).val() == 0) {
        if (!$(element).hasClass('is-invalid')) {
            $(element).addClass('is-invalid')
        }
        idenvalido = false;
    } else {
        if ($(element).hasClass('is-invalid')) {
            $(element).removeClass('is-invalid');
        }
        idenvalido = true;
    }
    return idenvalido;
}

//Descarta el guardado de datos
async function descartaGuardar() {
    $("#hdfEditable").val("NO");
    $("#mdlGuardado").modal("toggle");
    descartarGuardar();
    await sleep(200)
    $("#" + ($("#hdfTransicion").val())).trigger("click");
}

//Modal guardar
function btnGuardaDatosModal() {
    if (!validarCampos("#divformPaciente", true)) {
        toastr.error("Falta completar algunos campos obligatorios");
        $("#mdlGuardado").modal("toggle");
        //event.preventDefault();
        return
    }
    else {
        $("#mdlGuardado").modal("toggle");
        guardaPaciente(($("#btnGuardarCambios").data("pacienteDclinico") == true));
        $("#hdfEditable").val("NO");
        toastr.success('DATOS DE PACIENTE GUARDADOS CORRECTAMENTE');
        $("#" + ($("#hdfTransicion").val())).trigger("click");
    }
}

//funciones de guardado por sección
function guardaPaciente(esGuardadoDClinico = false) {

    let url = `${GetWebApiUrl()}GEN_Paciente/${$('#idPaciente').val()}`

    if ($("#hdfEditable").val() == "SI") {
        $.ajax({
            type: 'PUT',
            url: url,
            data: JSON.stringify(JSONPaciente()),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {

                if (status === "nocontent") {
                    if (esGuardadoDClinico)
                        guardaPacienteDClinico();
                    toastr.success('SE HAN ACTUALIZADO LOS DATOS DEL PACIENTE');

                    // Actualizar el valor de #lblNUIPacientePill con el valor de #txtNuiPaciente al pasarlo de forma manual
                    const nuevoNUI = $("#txtNuiPaciente").val();
                    $('#lblNUIPacientePill').empty().append(`${nuevoNUI ? `Número ubicación interna: ${nuevoNUI}` : "No posee NUI"}`);
                }
            },
            error: function (jqXHR, status) {

                toastr.error('ERROR AL GUARDAR DATOS DE PACIENTE');
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#hdfEditable").val("NO");
}

function guardaPacienteDClinico() {
    let json = JSONPacienteDClinico();
    const idPaciente = parseInt($('#idPaciente').val());
    const idPacienteDclinico = parseInt($('#idPacienteDClinico').val());

    if (idPacienteDclinico && idPacienteDclinico !== '0') {
        json.IdPacienteDclinico = idPacienteDclinico;
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/DClinico`,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data, status, jqXHR) {
                toastr.success('SE HAN ACTUALIZADO LOS DATOS DEL PACIENTE DE CLÍNICO');
            },
            error: function (jqXHR, status) {
                console.log("Error en PUT:", jqXHR.responseText);
                toastr.error('ERROR AL ACTUALIZAR DATOS DE PACIENTE DE CLÍNICO');
            }
        });

    } else {
        json.IdPaciente = idPaciente;
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}/DClinico`,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data, status, jqXHR) {
                toastr.success('SE HAN GUARDADO LOS DATOS DEL PACIENTE DE CLÍNICO');
            },
            error: function (jqXHR, status) {
                console.log("Error en POST:", jqXHR.responseText);
                toastr.error('ERROR AL GUARDAR DATOS DE PACIENTE DE CLÍNICO');
            }
        });
    }
}

function JSONPacienteDClinico() {
    const pacienteDClinico = {
        IdGrupoSanguineo: valCampo(parseInt($('#ddlGrupoSanguineoPaciente').val())),
        AlergiasDetalle: valCampo($('#txtOtrasAlergias').val()),
        Cardiopatia: ($('#switchCardiopatia').bootstrapSwitch('state')) ? 'SI' : 'NO',
        CardiopatiaDetalle: valCampo($('#txtCardiopatiaDetalle').val()),
        Hta: ($('#switchHta').bootstrapSwitch('state')) ? 'SI' : 'NO',
        HtaDetalle: valCampo($('#txtHtaDetalle').val()),
        Diabetes: ($('#switchDiabetes').bootstrapSwitch('state')) ? 'SI' : 'NO',
        DiabetesDetalle: valCampo($('#txtDiabetes').val()),
        AntecedentesQuirurgicos: ($('#switchAntecedentesQuirurgicos').val() != '') ? 'SI' : 'NO',
        AntecedentesQuirurgicosDetalle: valCampo($('#txtAntecedentesQuirurgicos').val()),
        HospitalizacionesAnteriores: ($('#txtHospitalizacionesAnteriores').val() != '') ? 'SI' : 'NO',
        HospitalizacionesAnterioresDetalle: valCampo($('#txtHospitalizacionesAnteriores').val()),
        Latex: ($('#rdoLatexSi').bootstrapSwitch('state')) ? 'SI' : 'NO',
        TratamientoFarmacologico: valCampo($('#txtTratFarmacologico').val()),
        AntecedentesFamiliares: valCampo($('#txtAntecedentesFamiliares').val()),
        EnfermedadesHereditarias: valCampo($('#txtEnfermedadesHereditarias').val()),
        EnfermedadesCronicas: valCampo($('#txtEnfermedadesCronicas').val()),
        MedicacionPrevia: valCampo($('#txtMedicacionPrevia').val()),
        AntecedentesGinecobstetricos: valCampo($('#txtAntecedentesObstetricos').val()),
        AntecedentesMorbidos: valCampo($('#txtAntecedentesMorbidos').val()),
        AntecedentesNeonatales: valCampo($('#txtAntecedentesNeonatales').val()),
        EnfermedadesPrevias: valCampo($('#txtEnfermedadesPrevias').val()),
        Drogas: valCampo($("#txtDrogas").val()),
        Hipertension: valCampo($("#txtHipertension").val()),
        Asma: valCampo($("#txtAsma").val()),
        Tabaco: valCampo($("#txtDetalleTabaco").val()),
        OHPaciente: valCampo($("#txtDetalleOH").val())
    };

    return pacienteDClinico;
}


//descartar guaradado (carga nuevamente al paciente)
function descartarGuardar() {
    let identificacion = $("#ddlTipoIdentificacionPaciente").val()
    let documento = $("#txtRutPaciente").val()
    getPacienteRut(identificacion, documento);
}

//Funciones validar(Hans)
function ValidarTxt(text, b) {
    let bValido = true;
    if (text.val().trim() == '') {
        bValido = false;
        if (!text.hasClass('is-invalid'))
            text.addClass('is-invalid');
    } else {
        if (text.hasClass('is-invalid'))
            text.removeClass('is-invalid');
    }
    if (!b) {
        return false;
    }
    return bValido;
}
function ValidarDropDown(drop, b) {
    let bValido = true;
    if (drop[0].selectedIndex == 0) {
        bValido = false;
        if (!drop.hasClass('is-invalid'))
            drop.addClass('is-invalid');
    } else {
        if (drop.hasClass('is-invalid'))
            drop.removeClass('is-invalid');
    }

    if (drop[0].length == 1) {
        if (drop.hasClass('is-invalid'))
            drop.removeClass('is-invalid');
        bValido = true;

    }

    if (!b)
        return false;

    return bValido;
}
//Carga combos por API
function cargaCombos() {

    comboIdentificacionBandejaPac()

    ComboTipoEscolaridad()
    //comboIdentificacion(selectorIdentificacion)
    CombosTiposDomicilios()

    APISexo();
    APIGenero();
    //ssacionalidad();
    APIEstadoConyugal();
    APIPuebloOriginario();
    APIGrupoSanguineo();
    APITipoEscolaridad();
    APIPais();
    APIPrevision();
    $("#ddlRegionPaciente, #ddlProvinciaPaciente, #ddlCiudadPaciente").addClass('disabled');
    //$("#ddlRegionPaciente, #ddlProvinciaPaciente, #ddlCiudadPaciente").append("<option value='0'> - Seleccione - </option>");
    //$("#sltTipoIdentificacionBusqueda").unbind().change(function () {
    //    ($(this).val() != '1' && $(this).val() != '4') ? $("#divFichaClinicaBuscador .digito").hide() : $("#divFichaClinicaBuscador .digito").show();
    //});

}

function comboIdentificacionBandejaPac() {
    const url = `${GetWebApiUrl()}GEN_Identificacion/Combo`;
    setCargarDataEnCombo(url, false, "#sltTipoIdentificacion");
}

function APISexo() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnCombo(url, true, "#ddlSexoPaciente");
}

function APIGenero() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Genero/Combo`;
    setCargarDataEnCombo(url, true, "#ddlGeneroPaciente");
}

function APIEstadoConyugal() {
    let url = `${GetWebApiUrl()}GEN_Estado_Conyugal/Combo`
    setCargarDataEnCombo(url, false, "#ddlEstadoConyuPaciente");
}
function APIPuebloOriginario() {
    let url = `${GetWebApiUrl()}GEN_Pueblo_Originario/Combo`
    setCargarDataEnCombo(url, false, "#ddlPuebloOrigPaciente");
}
function APITipoEscolaridad() {
    const url = `${GetWebApiUrl()}GEN_Tipo_Escolaridad/Combo`
    setCargarDataEnCombo(url, false, "#sltEscolaridad");
}
function APIGrupoSanguineo() {
    let url = `${GetWebApiUrl()}GEN_Grupo_Sanguineo/Combo`;
    setCargarDataEnCombo(url, false, "#ddlGrupoSanguineoPaciente");
}

function APIPais() {
    let url = `${GetWebApiUrl()}GEN_Pais/Combo`;
    setCargarDataEnCombo(url, false, "#ddlPaisPaciente");
}

//Region aun no tiene controlador
function APIRegion(idPais) {
    $("#ddlRegionPaciente").removeClass('disabled');
    $("#ddlRegionPaciente").empty();
    $("#ddlRegionPaciente").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/GEN_Region/GEN_idPais/${idPais}`,
        async: false,
        success: function (data, status, jqXHR) {

            $.each(data, function () {
                $("#ddlRegionPaciente").append("<option value='" + this.Id + "'>" + this.Valor + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar region ${JSON.stringify(jqXHR)}`);
        }
    });

    cargaPerfiles();

}
//Provincia aun no tiene controlador
function APIProvincia(idRegion) {

    $("#ddlProvinciaPaciente").removeClass('disabled');
    $("#ddlProvinciaPaciente").empty();
    $("#ddlProvinciaPaciente").append("<option value='0'> - Seleccione - </option>");

    let url = `${GetWebApiUrl()}/GEN_Region/${idRegion}/Provincias`;
    setCargarDataEnCombo(url, false, "#ddlProvinciaPaciente");

    cargaPerfiles();

}
function APIComuna(idProvincia) {

    $("#ddlCiudadPaciente").removeClass('disabled');
    $("#ddlCiudadPaciente").empty();
    $("#ddlCiudadPaciente").append("<option value='0'> - Seleccione - </option>");

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Comuna/Provincia/${idProvincia}`,
        async: false,
        success: function (data, status, jqXHR) {
            $.each(data, function () {
                $("#ddlCiudadPaciente").append(`<option value='${this.Id}'>${this.Valor}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(`Error al llenar ciudad ${JSON.stringify(jqXHR)}`);
        }
    });

    cargaPerfiles();

}

function APIPrevision() {
    const url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, $("#ddlAseguradora"));
}
function APITramo(idPrevision, select) {
    let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${idPrevision}`;
    setCargarDataEnCombo(url, false, $("#ddlTramo"));

    if (select != null)
        $("#ddlTramo").val(select);
    cargaPerfiles();

}

function imprimirIPD(e) {
    visibleDesdeOyente($(e));
    let id = $(e).data("id");
    setSession('ID_IPD', id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloIPD/ImprimirIPD.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirIC(e) {
    let id = $(e).data("id");
    const url = `${GetWebApiUrl()}RCE_Interconsulta/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

function imprimirHE(e) {
    visibleDesdeOyente($(e));
    let id = $(e).data("id");
    setSession('ID_HOJAEVOLUCION', id);
    $('#frameImprimir').attr('src', ObtenerHost() + '/Vista/ModuloHOS/ImpresionHojaEvolucion.aspx?id=' + id);
    $('#mdlImprimir').modal('show');
}

function imprimirURG(e) {
    let id = $(e).data("id");
    ImprimirApiExterno(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Imprimir`);
    console.log(`${GetWebApiUrl()}URG_Atenciones_Urgencia/${id}/Imprimir`)
}

function imprimirEventoAdverso(idEventoAdverso) {
    const urlImprimirST = `${GetWebApiUrl()}NEA_Evento_Adverso/${idEventoAdverso}/Imprimir`
    ImprimirApiExterno(urlImprimirST)
}
function imprimirST(idTransfusion) {
    const urlImprimirST = `${GetWebApiUrl()}RCE_Solicitud_Transfusion/${parseInt(idTransfusion)}/Imprimir`
    ImprimirApiExterno(urlImprimirST)
}
function imprimirBIO(sender) {
    const urlImprimirBio = `${GetWebApiUrl()}ANA_Registro_Biopsias/${parseInt($(sender).data("id"))}/Imprimir`
    ImprimirApiExterno(urlImprimirBio)
}

function grillaEventosVacia() {
    if (!$("#btnExportarEvento").hasClass('hide')) {
        $("#btnExportarEvento").addClass('hide');
    }
    $('#errGrillaEventos').empty();
    $('#errGrillaEventos').append("No existen eventos para este paciente.");
    if ($('#errGrillaEventos').hasClass('hide')) {
        $('#errGrillaEventos').removeClass('hide');
    }
    $('#tblEventos').DataTable().clear().draw();
    $('#tblEventos_wrapper').addClass('hide');
    $('#tblEventos').addClass('hide');
}

//ABRE MODAL NUEVO EVENTO
function showNuevoEvento() {
    $('#txtEventoPacNom').val($('#txtNombrePaciente').val() + ' ' + $('#txtApellidoPaciente').val() + ' ' + $('#txtApellidoMPaciente').val());
    $('#txtEventoPacRut').val($('#txtRutPaciente').val() + '-' + $('#txtRutDigitoPaciente').val());
    $('#mdlEventoNuevo').modal('show');
}

function linkPacienteRut(tipo, rut, id) {
    getPacienteRut(tipo, rut, id);
}

//API GET PACIENTE POR RUT
function getPacienteRut(tipo, rut, id) {

    if (id == 0 || tipo !== 5) {

        limpiaCampos();
        $('#divPacientes').hide();
        rut = (rut.indexOf("-") == -1) ? rut : rut.split("-")[0];

        let url = `${GetWebApiUrl()}GEN_Paciente/Buscar?numeroDocumento=${rut}&idIdentificacion=${tipo}`;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                if (data.length <= 0) {
                    toastr.error("No se encontró el paciente");
                    ShowModalCargando(false);
                    return
                }

                try {
                    let json = data[0];
                    //errorPaciente(); VALIDAR CON ERROR 404

                    $("#infoResponseFonasa").empty()
                    $("#infoResponseFonasa").append(`
                        <div class="alert alert-info w-100" role="alert">
                             <i class="fa fa-cog fa-spin fa-3x"></i><h3> Cargando esto puede tomar algunos segundos..</h3>
                        </div>`)

                    $.ajax({
                        method: "GET",
                        url: `${GetWebApiUrl()}GEN_Paciente/${json.NumeroDocumento}/Fonasa`,
                        success: function (dataFonasa) {
                            let contenidoFonasa = ``;
                            if (Object.keys(dataFonasa).length > 0) {
                                contenidoFonasa = `
                                <div class="col-md-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-header bg-primary">
                                            <h6> CERTIFICADO: ${dataFonasa.coddesc} </h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="card">
                                                <div class="card-header bg-info">
                                                    <h6> Datos beneficiario </h6>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nombre: ${dataFonasa.beneficiarioTO.nombres} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Paterno: ${dataFonasa.beneficiarioTO.apell1}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Materno: ${dataFonasa.beneficiarioTO.apell2}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Rut: ${dataFonasa.beneficiarioTO.rutbenef}-${dataFonasa.beneficiarioTO.dgvbenef} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Fecha Nac: ${dataFonasa.beneficiarioTO.fechaNacimiento}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Sexo: ${dataFonasa.beneficiarioTO.generoDes}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nacionalidad: ${dataFonasa.beneficiarioTO.desNacionalidad} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Región: ${dataFonasa.beneficiarioTO.desRegion}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Comuna: ${dataFonasa.beneficiarioTO.desComuna}</div>
                                                        <div class="col-md-8 col-sm-6 col-xs-12 border-bottom">Dirección: ${dataFonasa.beneficiarioTO.direccion} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Teléfono: ${dataFonasa.beneficiarioTO.telefono}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header bg-info">
                                                    <h6> Datos afiliado </h6>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nombre: ${dataFonasa.afiliadoTO.nombres} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Paterno: ${dataFonasa.afiliadoTO.apell1}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Materno: ${dataFonasa.afiliadoTO.apell2}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Rut: ${dataFonasa.afiliadoTO.rutafili}-${dataFonasa.afiliadoTO.dgvafili} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Fecha Nac: ${dataFonasa.afiliadoTO.fecnac}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Tramo: ${dataFonasa.afiliadoTO.tramo}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Sexo: ${dataFonasa.afiliadoTO.generoDes} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Estado: ${dataFonasa.afiliadoTO.desEstado}</div>
                                                    </div>
                                                </div>
                                            </div>`;

                                // Verificar si listCargas existe y si tiene elementos
                                if (dataFonasa.listCargas && dataFonasa.listCargas.length > 0) {
                                    contenidoFonasa += `
                                    <div class="card">
                                        <div class="card-header bg-info">
                                            <h6> Listado de cargas </h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <table class="table table-bordered table-striped table-hover w-100">
                                                        <thead>
                                                            <tr>
                                                                <th>RUT</th>
                                                                <th>Apellido Paterno</th>
                                                                <th>Apellido Materno</th>
                                                                <th>Nombres</th>
                                                                <th>Parentesco</th>
                                                                <th>Fecha Nacimiento</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>`;

                                    // Iterar sobre cada carga
                                    dataFonasa.listCargas.forEach(carga => {
                                        contenidoFonasa += `
                                        <tr>
                                            <td>${carga.rutcarga}-${carga.dgvcarga}</td>
                                            <td>${carga.apell1}</td>
                                            <td>${carga.apell2}</td>
                                            <td>${carga.nombres}</td>
                                            <td>${carga.parentesco}</td>
                                            <td>${moment(carga.fecnac).format('DD-MM-YYYY')}</td>
                                        </tr>`;
                                    });

                                    contenidoFonasa += `
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                                } else {
                                    contenidoFonasa += `
                                    <div class="alert alert-warning" role="alert">
                                        No se encontraron cargas asociadas
                                    </div>`;
                                }

                                contenidoFonasa += `
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary" onclick='imprimirFonasa(${JSON.stringify(dataFonasa)})'>Generar formulario fonasa</button>
                                </div>
                            `;
                                $("#infoResponseFonasa").empty();
                                $("#infoResponseFonasa").append(contenidoFonasa);
                            }
                        }, error: function (error) {
                            console.error("Ha ocurrido un error al intentar obtener la información de fonasa");
                            console.log(error);
                            let contenidoFonasa = `<div class="alert alert-danger w-100" role="alert">
                                  La información de Fonasa no está disponible en este momento.
                              </div>`;
                            $("#infoResponseFonasa").empty();
                            $("#infoResponseFonasa").append(contenidoFonasa);
                        }
                    });

                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Paciente/${json.IdPaciente}/DClinico`,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {

                            $("#idPacienteDClinico").val(data.Id);
                            $("#ddlGrupoSanguineoPaciente").val(data.GrupoSanguineo !== null ? data.GrupoSanguineo.Id : "0");

                            if (data.Latex == "SI")
                                $("#rdoLatexSi").bootstrapSwitch('state', true, true);
                            else if (data.Latex == "NO")
                                $("#rdoLatexNo").bootstrapSwitch('state', true, true);
                            else
                                $("#rdoLatexSi, #rdoLatexNo").bootstrapSwitch('state', false, true);

                            $("#switchOtrasAlergias").bootstrapSwitch('state', (data.Alergias != null), true);
                            $("#txtOtrasAlergias").val(data.Alergias);
                            $("#txtAntecedentesFamiliares").val(data.AntecedentesFamiliares);
                            $("#txtEnfermedadesHereditarias").val(data.EnfermedadesHereditarias);
                            $("#txtEnfermedadesPrevias").val(data.EnfermedadesPrevias);
                            $("#txtAntecedentesNeonatales").val(data.AntecedentesNeonatales);
                            $("#txtMedicacionPrevia").val(data.MedicacionPrevia);
                            $("#txtTratFarmacologico").val(data.TratamientoFarmatologico);
                            $("#txtAntecedentesObstetricos").val(data.AntecedentesGinecobstetricos);
                            $("#txtAntecedentesMorbidos").val(data.AntecedentesMorbidos);
                            $("#txtAntecedentesQuirurgicos").val(data.AntecendetesQxDetalle);
                            $("#txtDetalleTabaco").val(data.Tabaco);
                            $("#txtDetalleOH").val(data.OH);
                            $("#txtDrogas").val(data.Drogas);
                            $("#txtHipertension").val(data.Hipertension);
                            $("#txtDiabetes").val(data.DiabetesDetalle);
                            $("#txtAsma").val(data.Asma);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            $('#idPacienteDClinico').val('0');
                            if (xhr.status == 404) {//no encontró paciente
                                //no se encontraron datos dclinico
                            }
                        }
                    });

                    $('#idPaciente').val(json.IdPaciente);
                    idPac = json.IdPaciente;
                    if (json.NumeroDocumento != null) {
                        $('#txtRutPaciente').val(json.NumeroDocumento);
                    }

                    if (json.Digito != null) {
                        $('#txtRutDigitoPaciente').val(json.Digito);
                    }

                    $('#sltTipoIdentificacion').val(json.Identificacion.Id);
                    ($('#ddlTipoIdentificacionPaciente').val() != '5') ? $("#ddlTipoIdentificacionPaciente option[value='5']").hide() :
                        $("#ddlTipoIdentificacionPaciente option[value='5']").show();

                    if (json.Nui != null && json.Nui != 0)
                        $('#txtNuiPaciente').val(json.Nui);

                    if (json.Nombre != null) {
                        $('#txtNombrePaciente').val(json.Nombre);
                    }

                    if (json.ApellidoPaterno != null) {
                        $('#txtApellidoPaciente').val(json.ApellidoPaterno);
                    }

                    if (json.ApellidoMaterno != null) {
                        $('#txtApellidoMPaciente').val(json.ApellidoMaterno);
                    }

                    if (json.FechaNacimiento != null) {
                        $('#txtFechaNacPaciente').val(moment(new Date(json.FechaNacimiento)).format('YYYY-MM-DD'));
                    }

                    if (json.Nacionalidad != null) {
                        $('#sltNacionalidadPaciente').val(json.Nacionalidad.Id);
                    }

                    if (json.Sexo != null) {
                        $('#ddlSexoPaciente').val(json.Sexo.Id);
                    }

                    if (json.Genero != null) {
                        $('#ddlGeneroPaciente').val(json.Genero.Id);
                    }

                    if (json.Pais != null) {
                        $('#ddlPaisPaciente').val(json.Pais.Id);
                        APIRegion(json.Pais.Id);
                    }

                    if (json.Region != null) {
                        $('#ddlRegionPaciente').val(json.Region.Id);
                        APIProvincia(json.Region.Id);
                    }

                    if (json.Provincia != null) {
                        $('#ddlProvinciaPaciente').val(json.Provincia.Id);
                        APIComuna(json.Provincia.Id);
                    }

                    if (json.Comuna != null) {
                        $('#ddlCiudadPaciente').val(json.Comuna.Id);
                    }

                    //              $('#ddlProvinciaPaciente').val(val.GEN_idProvincia);
                    if (json.DireccionCalle != null) {
                        $('#txtDireccionPaciente').val(json.DireccionCalle);
                    }

                    if (json.DireccionNumero != null) {
                        $('#txtNumeroDireccionPaciente').val(json.DireccionNumero);
                    }

                    if (json.Telefono != null) {
                        $('#txtTelefonoPaciente').val(json.Telefono);
                    }

                    if (json.OtrosFonos != null) {
                        $('#txtOtroTelefonoPaciente').val(json.OtrosFonos);
                    }

                    if (json.Email != null) {
                        $('#txtEmailPaciente').val(json.Email);
                    }

                    if (json.TipoDomicilio !== null) {
                        $('#sltRural').val(json.TipoDomicilio.Id);
                    }


                    if (json.PuebloOriginario != null) {
                        $('#ddlPuebloOrigPaciente').val(json.PuebloOriginario.Id);
                    }

                    if (json.NivelEducacional != null) {
                        $('#sltEscolaridad').val(json.NivelEducacional.Id);
                    }

                    if (json.EstadoConyugal != null) {
                        $('#ddlEstadoConyuPaciente').val(json.EstadoConyugal.Id);
                    }

                    if (json.FechaActualizacion != null) {
                        $('#txtFechaActualizacionPaciente').val(moment(new Date(json.FechaActualizacion)).format('DD-MM-YYYY'));
                    }

                    if (json.FechaFallecimiento != null) {
                        $('#txtFechaFallecimiento').val(moment(new Date(json.FechaFallecimiento)).format('YYYY-MM-DD'));
                        $('#txtFechaFallecimiento').removeClass("hide");
                        $('#lblFechaFallecimientoPacientePill').text(moment(new Date(json.FechaFallecimiento)).format('DD-MM-YYYY'));
                    } else {
                        $('#txtFechaFallecimiento').addClass("hide");
                    }

                    if (json.NombreSocial != null) {
                        $('#txtRespondePaciente').val(json.NombreSocial);
                    }

                    //DATOS PREVISIONALES
                    if (json.Prevision != null) {
                        $('#ddlAseguradora').val(json.Prevision.Id);
                        if (json.PrevisionTramo != null) {
                            APITramo(json.Prevision.Id, json.PrevisionTramo.Id);
                        }
                    }

                    if (json.Prais != null) {
                        if (json.Prais === "SI") {
                            $('#switchPrais').bootstrapSwitch('state', true, true);
                        } else {
                            $('#switchPrais').bootstrapSwitch('state', false, true);
                        }
                    }
                    if ($('#txtFechaNacPaciente').val() != "")
                        $('#txtEdadPaciente').val(CalcularEdad($('#txtFechaNacPaciente').val()).edad);

                    $(".nav-paciente .nav-link").removeClass("disabled");

                    var NombreCompleto = $('#txtNombrePaciente').val() + ' ' + $('#txtApellidoPaciente').val() + ' ' + $('#txtApellidoMPaciente').val();
                    var documento = $('#txtRutPaciente').val() + (($('#txtRutDigitoPaciente').val() != '') ? '-' + $('#txtRutDigitoPaciente').val() : '');

                    $('#lblNombrePacientePill').empty();
                    $('#lblRutPacientePill').empty();
                    $('#lblNUIPacientePill').empty();
                    $('#lblNombrePacientePill').append(firstUpper(NombreCompleto));
                    $('#lblRutPacientePill').append(documento);
                    $('#lblNUIPacientePill').append(`${(json.Nui !== undefined && json.Nui !== null) ? `Número ubicación interna: ${json.Nui}` : "No posee NUI"}`);
                    $('#lblPaciente').show();

                    if ($('#txtFechaFallecimiento').val() != "") { //fecha fallecimiento
                        fadeIn($('#lblPacienteFallecido'));
                    }

                    $('#hdfEditable').val('NO');

                    //'habilitar' botones
                    if (sSession.CODIGO_PERFIL != 28) {
                        $('#btnDatosClinicos').removeClass('bg-secondary disabled').addClass('bg-info');
                        $('#btnDatosPrev').removeClass('bg-secondary disabled').addClass('bg-info');
                        $('#btnRegistroClinico').removeClass('bg-secondary disabled').addClass('bg-info');
                    }
                    $('#btnPaciente').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnLocalizacion').removeClass('bg-secondary disabled').addClass('bg-info');

                    $('html, body').stop().animate({ scrollTop: 0 }, 500);

                    ocultaRegionSiEsDistintadeChile("#ddlPaisPaciente", "#divRegionPaciente", "#divProvincia", "#divCiudad")


                } catch (err) {
                    console.log("Error: " + err.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.status == 404) {//no encontró paciente
                    toastr.error("No se han encontrado pacientes con los campos seleccionados");
                }
            }
        });
    } else {

        limpiaCampos();
        $('#divPacientes').hide();

        //let url = `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=${idIdentificacion}&idPaciente=${idPaciente}`;
        let url = `${GetWebApiUrl()}GEN_Paciente/Buscar?idPaciente=${id}`;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                if (data.length <= 0) {
                    toastr.error("No se encontró el paciente")
                    return
                }

                try {
                    let json = data[0];
                    //errorPaciente(); VALIDAR CON ERROR 404

                    $("#infoResponseFonasa").empty()
                    $("#infoResponseFonasa").append(`
                <div class="alert alert-info w-100" role="alert">
                     <i class="fa fa-cog fa-spin fa-3x"></i><h3> Cargando esto puede tomar algunos segundos..</h3>
                </div>
                `)

                    $.ajax({
                        method: "GET",
                        url: `${GetWebApiUrl()}GEN_Paciente/${json.NumeroDocumento}/Fonasa`,
                        success: function (dataFonasa) {
                            let contenidoFonasa = ``
                            if (Object.keys(dataFonasa).length > 0) {
                                contenidoFonasa = `
                            <div class="col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header bg-primary">
                                        <h6> CERTIFICADO: ${dataFonasa.coddesc} </h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="card">
                                            <div class="card-header bg-info">
                                                <h6> Datos beneficiario </h6>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nombre: ${dataFonasa.beneficiarioTO.nombres} </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Paterno: ${dataFonasa.beneficiarioTO.apell1}</div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Materno: ${dataFonasa.beneficiarioTO.apell2}</div>

                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Rut: ${dataFonasa.beneficiarioTO.rutbenef}-${dataFonasa.beneficiarioTO.dgvbenef} </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Fecha Nac: ${dataFonasa.beneficiarioTO.fechaNacimiento}</div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Sexo: ${dataFonasa.beneficiarioTO.generoDes}</div>

                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nacionalidad: ${dataFonasa.beneficiarioTO.desNacionalidad} </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Región: ${dataFonasa.beneficiarioTO.desRegion}</div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Comuna: ${dataFonasa.beneficiarioTO.desComuna}</div>

                                                    <div class="col-md-8 col-sm-6 col-xs-12 border-bottom">Dirección: ${dataFonasa.beneficiarioTO.direccion} </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Teléfono: ${dataFonasa.beneficiarioTO.telefono}</div>
                                            
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header bg-info">
                                                    <h6> Datos afiliado </h6>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Nombre: ${dataFonasa.afiliadoTO.nombres} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Paterno: ${dataFonasa.afiliadoTO.apell1}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">A. Materno: ${dataFonasa.afiliadoTO.apell2}</div>

                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Rut: ${dataFonasa.afiliadoTO.rutafili}-${dataFonasa.afiliadoTO.dgvafili} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Fecha Nac: ${dataFonasa.afiliadoTO.fecnac}</div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Tramo: ${dataFonasa.afiliadoTO.tramo}</div>

                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Sexo: ${dataFonasa.afiliadoTO.generoDes} </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 border-bottom">Estado: ${dataFonasa.afiliadoTO.desEstado}</div>
                                                    </div>
                                                </div>
                                            </div>
                                         
                                         ${dataFonasa.listCargas !== null ?
                                        `

                                    <div class="card">
                                        <div class="card-header bg-info">
                                                <h6> Listado de cargas </h6s>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                <table class="table table-bordered table-striped table-hover w-100">
                                                <thead>
                                                    <tr>
                                                        <th>RUT</th>
                                                        <th>Apellido Paterno</th>
                                                        <th>Apellido Materno</th>
                                                        <th>Nombres</th>
                                                        <th>Parentesco</th>
                                                        <th>Fecha Nacimiento</th>
                                                    <tr/>
                                                </thead>
                                                <tbody>
                                                    ${Array.isArray(dataFonasa.listCargas.CargasTO) ?
                                            dataFonasa.listCargas.CargasTO.map(carga => {
                                                return `<tr>
                                                                                   <td>${carga.rutcarga}-${carga.dgvcarga}</td>
                                                                                   <td>${carga.apell1}</td>
                                                                                   <td>${carga.apell2}</td>
                                                                                   <td>${carga.nombres}</td>
                                                                                   <td>${carga.parentesco}</td>
                                                                                   <td>${carga.fecnac}</td>
                                                                                </tr>`
                                            }) :
                                            `<tr>
                                                        <td>${dataFonasa.listCargas.CargasTO.rutcarga}-${dataFonasa.listCargas.CargasTO.dgvcarga}</td>
                                                        <td>${dataFonasa.listCargas.CargasTO.apell1}</td>
                                                        <td>${dataFonasa.listCargas.CargasTO.apell2}</td>
                                                        <td>${dataFonasa.listCargas.CargasTO.nombres}</td>
                                                        <td>${dataFonasa.listCargas.CargasTO.parentesco}</td>
                                                        <td>${dataFonasa.listCargas.CargasTO.fecnac}</td>
                                                     </tr >
                                                    
                                                    `}
                                                </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                `
                                        :
                                        `
                                    <div class="alert alert-warning" role="alert">
                                        No se encontraron cargas asociadas
                                    </div>
                                ` }

                                         
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" onclick='imprimirFonasa(${JSON.stringify(dataFonasa)})'>Generar formulario fonasa</button>
                            </div>
                            `
                                $("#infoResponseFonasa").empty()
                                $("#infoResponseFonasa").append(contenidoFonasa)
                            }
                        }, error: function (error) {
                            console.error("Ha ocurrido un error al intentar obtener la informacion de fonasa")
                            console.log(error)
                            let contenidoFonasa = `<div class="alert alert-danger w-100" role="alert">
                                                  La información de Fonasa no está disponible en este momento.
                                              </div>`
                            $("#infoResponseFonasa").empty()
                            $("#infoResponseFonasa").append(contenidoFonasa)
                        }

                    })

                    $.ajax({
                        type: 'GET',
                        url: `${GetWebApiUrl()}GEN_Paciente/${json.IdPaciente}/DClinico`,
                        contentType: "application/json",
                        dataType: "json",
                        success: function (data) {

                            $("#idPacienteDClinico").val(data.Id);
                            $("#ddlGrupoSanguineoPaciente").val(data.GrupoSanguineo !== null ? data.GrupoSanguineo.Id : "0");

                            if (data.Latex == "SI")
                                $("#rdoLatexSi").bootstrapSwitch('state', true, true);
                            else if (data.Latex == "NO")
                                $("#rdoLatexNo").bootstrapSwitch('state', true, true);
                            else
                                $("#rdoLatexSi, #rdoLatexNo").bootstrapSwitch('state', false, true);

                            $("#switchOtrasAlergias").bootstrapSwitch('state', (data.Alergias != null), true);
                            $("#txtOtrasAlergias").val(data.Alergias);
                            $("#txtAntecedentesFamiliares").val(data.AntecedentesFamiliares);
                            $("#txtEnfermedadesHereditarias").val(data.EnfermedadesHereditarias);
                            $("#txtEnfermedadesPrevias").val(data.EnfermedadesPrevias);
                            $("#txtAntecedentesNeonatales").val(data.AntecedentesNeonatales);
                            $("#txtMedicacionPrevia").val(data.MedicacionPrevia);
                            $("#txtTratFarmacologico").val(data.TratamientoFarmatologico);
                            $("#txtAntecedentesObstetricos").val(data.AntecedentesGinecobstetricos);
                            $("#txtAntecedentesMorbidos").val(data.AntecedentesMorbidos);
                            $("#txtAntecedentesQuirurgicos").val(data.AntecendetesQxDetalle);
                            $("#txtDetalleTabaco").val(data.Tabaco);
                            $("#txtDetalleOH").val(data.OH);
                            $("#txtDrogas").val(data.Drogas);
                            $("#txtHipertension").val(data.Hipertension);
                            $("#txtDiabetes").val(data.DiabetesDetalle);
                            $("#txtAsma").val(data.Asma);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            $('#idPacienteDClinico').val('0');
                            if (xhr.status == 404) {//no encontró paciente
                                //no se encontraron datos dclinico
                            }
                        }
                    });

                    $('#idPaciente').val(json.IdPaciente);
                    idPac = json.IdPaciente;
                    if (json.NumeroDocumento != null) {
                        $('#txtRutPaciente').val(json.NumeroDocumento);
                    }

                    if (json.Digito != null) {
                        $('#txtRutDigitoPaciente').val(json.Digito);
                    }

                    $('#sltTipoIdentificacion').val(json.Identificacion.Id);
                    ($('#ddlTipoIdentificacionPaciente').val() != '5') ? $("#ddlTipoIdentificacionPaciente option[value='5']").hide() :
                        $("#ddlTipoIdentificacionPaciente option[value='5']").show();

                    if (json.Nui != null && json.Nui != 0)
                        $('#txtNuiPaciente').val(json.Nui);

                    if (json.Nombre != null) {
                        $('#txtNombrePaciente').val(json.Nombre);
                    }

                    if (json.ApellidoPaterno != null) {
                        $('#txtApellidoPaciente').val(json.ApellidoPaterno);
                    }

                    if (json.ApellidoMaterno != null) {
                        $('#txtApellidoMPaciente').val(json.ApellidoMaterno);
                    }

                    if (json.FechaNacimiento != null) {
                        $('#txtFechaNacPaciente').val(moment(new Date(json.FechaNacimiento)).format('YYYY-MM-DD'));
                    }

                    if (json.Nacionalidad != null) {
                        $('#sltNacionalidadPaciente').val(json.Nacionalidad.Id);
                    }

                    if (json.Sexo != null) {
                        $('#ddlSexoPaciente').val(json.Sexo.Id);
                    }

                    if (json.Genero != null) {
                        $('#ddlGeneroPaciente').val(json.Genero.Id);
                    }

                    if (json.Pais != null) {
                        $('#ddlPaisPaciente').val(json.Pais.Id);
                        APIRegion(json.Pais.Id);
                    }

                    if (json.Region != null) {
                        $('#ddlRegionPaciente').val(json.Region.Id);
                        APIProvincia(json.Region.Id);
                    }

                    if (json.Provincia != null) {
                        $('#ddlProvinciaPaciente').val(json.Provincia.Id);
                        APIComuna(json.Provincia.Id);
                    }

                    if (json.Comuna != null) {
                        $('#ddlCiudadPaciente').val(json.Comuna.Id);
                    }

                    //              $('#ddlProvinciaPaciente').val(val.GEN_idProvincia);
                    if (json.DireccionCalle != null) {
                        $('#txtDireccionPaciente').val(json.DireccionCalle);
                    }

                    if (json.DireccionNumero != null) {
                        $('#txtNumeroDireccionPaciente').val(json.DireccionNumero);
                    }

                    if (json.Telefono != null) {
                        $('#txtTelefonoPaciente').val(json.Telefono);
                    }

                    if (json.OtrosFonos != null) {
                        $('#txtOtroTelefonoPaciente').val(json.OtrosFonos);
                    }

                    if (json.Email != null) {
                        $('#txtEmailPaciente').val(json.Email);
                    }

                    if (json.TipoDomicilio !== null) {
                        $('#sltRural').val(json.TipoDomicilio.Id);
                    }


                    if (json.PuebloOriginario != null) {
                        $('#ddlPuebloOrigPaciente').val(json.PuebloOriginario.Id);
                    }

                    if (json.NivelEducacional != null) {
                        $('#sltEscolaridad').val(json.NivelEducacional.Id);
                    }

                    if (json.EstadoConyugal != null) {
                        $('#ddlEstadoConyuPaciente').val(json.EstadoConyugal.Id);
                    }

                    if (json.FechaActualizacion != null) {
                        $('#txtFechaActualizacionPaciente').val(moment(new Date(json.FechaActualizacion)).format('DD-MM-YYYY'));
                    }

                    if (json.FechaFallecimiento != null) {
                        $('#txtFechaFallecimiento').val(moment(new Date(json.FechaFallecimiento)).format('YYYY-MM-DD'));
                        $('#txtFechaFallecimiento').removeClass("hide");
                        $('#lblFechaFallecimientoPacientePill').text(moment(new Date(json.FechaFallecimiento)).format('DD-MM-YYYY'));
                    } else {
                        $('#txtFechaFallecimiento').addClass("hide");
                    }

                    if (json.NombreSocial != null) {
                        $('#txtRespondePaciente').val(json.NombreSocial);
                    }

                    //DATOS PREVISIONALES
                    if (json.Prevision != null) {
                        $('#ddlAseguradora').val(json.Prevision.Id);
                        if (json.PrevisionTramo != null) {
                            APITramo(json.Prevision.Id, json.PrevisionTramo.Id);
                        }
                    }

                    if (json.Prais != null) {
                        if (json.Prais === "SI") {
                            $('#switchPrais').bootstrapSwitch('state', true, true);
                        } else {
                            $('#switchPrais').bootstrapSwitch('state', false, true);
                        }
                    }
                    if ($('#txtFechaNacPaciente').val() != "")
                        $('#txtEdadPaciente').val(CalcularEdad($('#txtFechaNacPaciente').val()).edad);

                    $(".nav-paciente .nav-link").removeClass("disabled");

                    var NombreCompleto = $('#txtNombrePaciente').val() + ' ' + $('#txtApellidoPaciente').val() + ' ' + $('#txtApellidoMPaciente').val();
                    var documento = $('#txtRutPaciente').val() + (($('#txtRutDigitoPaciente').val() != '') ? '-' + $('#txtRutDigitoPaciente').val() : '');

                    $('#lblNombrePacientePill').empty();
                    $('#lblRutPacientePill').empty();
                    $('#lblNUIPacientePill').empty();
                    $('#lblNombrePacientePill').append(firstUpper(NombreCompleto));
                    $('#lblRutPacientePill').append(documento);
                    $('#lblNUIPacientePill').append(`${(json.Nui !== undefined && json.Nui !== null) ? `Número ubicación interna: ${json.Nui}` : "No posee NUI"}`);
                    $('#lblPaciente').show();

                    if ($('#txtFechaFallecimiento').val() != "") { //fecha fallecimiento
                        fadeIn($('#lblPacienteFallecido'));
                    }

                    $('#hdfEditable').val('NO');

                    //'habilitar' botones
                    $('#btnPaciente').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnDatosClinicos').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnDatosPrev').removeClass('bg-secondary disabled').addClass('bg-info');
                    $('#btnRegistroClinico').removeClass('bg-secondary disabled').addClass('bg-info');

                    $('html, body').stop().animate({ scrollTop: 0 }, 500);

                    ocultaRegionSiEsDistintadeChile("#ddlPaisPaciente", "#divRegionPaciente", "#divProvincia", "#divCiudad")

                } catch (err) {
                    console.log("Error: " + err.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.status == 404) {//no encontró paciente
                    toastr.error("No se han encontrado pacientes con los campos seleccionados");
                }
            }
        });
    }
}

//LIMPIA CAMPOS
function limpiaCampos() {

    $('#idPaciente').val('');
    $('#txtRutPaciente').val('');
    $('#txtRutDigitoPaciente').val('');
    $('#ddlTipoIdentificacionPaciente').val('');
    $('#txtNuiPaciente').val('');
    $('#txtNombrePaciente').val('');
    $('#txtApellidoPaciente').val('');
    $('#txtApellidoMPaciente').val('');
    $('#txtFechaNacPaciente').val('');
    $('#ddlSexoPaciente').val('0');
    $('#ddlGeneroPaciente').val('0');
    $('#sltNacionalidadPaciente').val('0');
    $('#ddlRegionPaciente').val('0');
    $('#ddlProvinciaPaciente').val('0');
    $('#txtDireccionPaciente').val('');
    $('#txtNumeroDireccionPaciente').val('');
    $('#txtTelefonoPaciente').val('');
    $('#txtOtroTelefonoPaciente').val('');
    $('#txtEmailPaciente').val('');
    $('#txtRespondePaciente').val('');
    $('#ddlPuebloOrigPaciente').val('0');
    $('#sltEscolaridad').val('0');
    $('#ddlEstadoConyuPaciente').val('0');
    $('#txtFechaActualizacionPaciente').val('');
    $('#txtFechaFallecimiento').val('');
    $('#ddlGrupoSanguineoPaciente').val('0');
    $('#txtOtrasAlergias').val('');
    $('#txtAntecedentesFamiliares').val('');
    $('#txtEnfermedadesHereditarias').val('');
    $('#txtEnfermedadesPrevias').val('');
    $('#txtAntecedentesNeonatales').val('');
    $('#txtMedicacionPrevia').val('');
    $('#txtTratFarmacologico').val('');
    $('#txtAntecedentesObstetricos').val('');
    $('#txtAntecedentesQuirurgicos').val('');
    $('#txtAntecedentesMorbidos').val('');
    $('#ddlAseguradora').val('0');
    $('#ddlTramo').val('0');
    $('#switchPrais').bootstrapSwitch('state', false);
    $('#sltRural').val('0');
    $('#switchOtrasAlergias').bootstrapSwitch('state', false);
    $('#rdoLatexSi').bootstrapSwitch('state', false);
    $('#rdoLatexNo').bootstrapSwitch('state', false);
    $('#hdfEditable').val("NO");
    $('#lblPacienteFallecido').hide();
    $('#lblFechaFallecimientoPacientePill').text('');
    $('#txtFechaNacPaciente').val('');
    $('#txtEdadPaciente').val('');

    // HÁBITOS
    $("#txtDetalleTabaco").val("");
    $("#txtDetalleOH").val("");
    $("#txtDrogas").val('');
    $("#txtHipertension").val('');
    $("#txtDiabetes").val('');
    $("#txtAsma").val('');

    $('#lblPaciente').hide();
    BloquearMenu(true);
    limpiarGrillas();
}

//JSON PACIENTE
function JSONPaciente() {

    const paciente = {
        Id: $('#idPaciente').val(),
        NumeroDocumento: valCampo($('#txtRutPaciente').val()),
        Digito: valCampo($('#txtRutDigitoPaciente').val()),
        Nombre: valCampo($('#txtNombrePaciente').val()),
        ApellidoPaterno: valCampo($('#txtApellidoPaciente').val()),
        ApellidoMaterno: $('#txtApellidoMPaciente').val(),
        DireccionCalle: $('#txtDireccionPaciente').val(),
        DireccionNumero: $('#txtNumeroDireccionPaciente').val(),
        IdTiposDomicilio: ($('#sltRural').val() !== "0") ? $('#sltRural').val() : null,
        IdPais: ($('#ddlPaisPaciente').val() !== "0") ? $('#ddlPaisPaciente').val() : null,
        IdRegion: ($('#ddlRegionPaciente').val() !== "0") ? $('#ddlRegionPaciente').val() : null,
        IdComuna: ($('#ddlCiudadPaciente').val() !== "0") ? $('#ddlCiudadPaciente').val() : null,
        IdProvincia: ($('#ddlProvinciaPaciente').val() !== "0") ? $('#ddlProvinciaPaciente').val() : null,
        Telefono: $('#txtTelefonoPaciente').val(),
        IdTipoGenero: valCampo($('#ddlGeneroPaciente').val()),
        IdSexo: valCampo($('#ddlSexoPaciente').val()),
        FechaNacimiento: valCampo($('#txtFechaNacPaciente').val()),
        OtrosTelefonos: $('#txtOtroTelefonoPaciente').val(),
        Email: $('#txtEmailPaciente').val(),
        IdPrevision: ($('#ddlAseguradora').val() !== "0") ? $('#ddlAseguradora').val() : null,
        IdPrevisionTramo: ($('#ddlTramo').val() !== "0") ? $('#ddlTramo').val() : null,
        Nui: $('#txtNuiPaciente').val(),
        IdIdentificacion: valCampo($('#sltTipoIdentificacion').val()),
        Prais: $('#switchPrais').bootstrapSwitch('state') ? 'SI' : 'NO',
        IdEstadoConyugal: ($('#ddlEstadoConyuPaciente').val() !== "0") ? $('#ddlEstadoConyuPaciente').val() : null,
        IdPuebloOriginario: ($('#ddlPuebloOrigPaciente').val() !== "0") ? $('#ddlPuebloOrigPaciente').val() : null,
        IdNacionalidad: ($('#sltNacionalidadPaciente').val() !== "0") ? $('#sltNacionalidadPaciente').val() : null,
        IdTipoEscolaridad: ($('#sltEscolaridad').val() !== "0") ? $('#sltEscolaridad').val() : null,
        FechaFallecimiento: $('#txtFechaFallecimiento').val(),
        NombreSocial: $('#txtRespondePaciente').val(),
    }

    return paciente;
}

// HASTA AQUIIIII
var dataTablePacientes;

// Llenar grilla
function LlenaGrilla(datos, grilla, columnas, tabla, hide) {
    if ($(grilla).hasClass('hide')) {
        $(grilla).removeClass('hide');
    }

    // Si dataTablePacientes está inicializado, destruirlo antes de recrearlo
    if (dataTablePacientes) {
        dataTablePacientes.clear().destroy();
    }

    console.log(datos);

    dataTablePacientes = $(grilla).addClass("text-wrap").DataTable({
        order: [],
        data: datos,
        columns: columnas,
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all"
            },
            {
                targets: 1,
                render: function (data, type, full, meta) {
                    return "<div style='white-space: nowrap;'>" + data + "</div>";
                }
            },
            {
                targets: -1,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    var botones = "";
                    switch (tabla) {
                        case 'Pacientes':
                            let rut = datos[fila][1] ?? "";
                            if (rut.indexOf("-") > -1) {
                                rut = datos[fila][1].split("-")[0];
                                botones = `<a class='btn btn-info btnSeleccionPaciente' data-target='#panelBoton' 
                                        onclick='linkPacienteRut(${datos[fila][0]}, "${rut}", ${datos[fila][6]})'>
                                        <i class="fas fa-eye"></i> Ver
                                    </a>`;
                            } else {
                                rut = datos[fila][1];
                                botones = `<a class='btn btn-info btnSeleccionPaciente' data-target='#panelBoton' 
                                        onclick='linkPacienteRut(${datos[fila][0]}, "${rut}", ${datos[fila][6]})'>
                                        <i class="fas fa-eye"></i> Ver
                                    </a>`;
                            }
                            break;
                    }
                    return botones;
                }
            }
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (grilla == "#tblInterconsulta") {
                if (aData[3] == "Interconsulta con respuesta") {
                    $('td', nRow).css('background-color', '#dff0d8');
                } else if (aData[3] == "Interconsulta Cerrada") {
                    $('td', nRow).css('background-color', '#f8d7da');
                }
            }
        },
        responsive: {
            details: false,
            type: 'column'
        },
        buttons: ['copy', 'excel', 'pdf'],
        "paging": true,
        "pageLength": 10
    });

    if (hide != null) {
        dataTablePacientes.column(hide).visible(false);
    }
}

// Función para buscar pacientes NN
function buscarPacientesNN() {
    var idIdentificacion = 5;
    debugger;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=${idIdentificacion}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $('#divPacientes').show();

            // Llamar a la función para llenar la tabla con los resultados de pacientes NN
            llenarTablaPacientesNN(data);
        },
        error: function () {
            console.error('Error al obtener pacientes NN.');
        }
    });
}
// Función para llenar la tabla con los resultados de pacientes NN
function llenarTablaPacientesNN(data) {
    var adataset = [];
    var columnas = [
        { title: 'ID' },
        { title: '#' },
        { title: 'Nombre' },
        { title: 'Primer Apellido' },
        { title: 'Segundo Apellido' },
        { title: 'NUI' },
        { title: '' }];

    // Llenar el conjunto de datos con la información de los pacientes NN
    $.each(data, function () {
        adataset.push([
            this.Identificacion.Id,
            `${this.IdPaciente}` ?? "",
            this.Nombre,
            this.ApellidoPaterno,
            this.ApellidoMaterno,
            this.Nui
        ]);
    });

    // Llamar a la función para dibujar o actualizar la grilla
    LlenaGrilla(adataset, "#tblPacientes", columnas, 'Pacientes', 0);
}

//API GET PACIENTE POR FILTRO
function getPacienteFiltro(nombre, apep, apem, nui, idIdentificacion) {

    var adataset = [];
    var columnas = [

        { title: 'IDENTIFICACIÓN' },
        { title: 'RUT', className: "font-weight-bold" },
        { title: 'NOMBRE' },
        { title: 'APELLIDO' },
        { title: 'SEGUNDO APELLIDO' },
        { title: 'NUI' },
        { title: 'ID' },
        { title: '' }];

    let filtros = "";
    if (nombre != "")
        filtros = `nombrePaciente=${nombre}`;

    if (apep != "")
        filtros += (filtros == "") ? `ape_paternoPaciente=${apep}` : `&ape_paternoPaciente=${apep}`;

    if (apem != "")
        filtros += (filtros == "") ? `ape_maternoPaciente=${apem}` : `&ape_maternoPaciente=${apem}`;

    if (nui != "")
        filtros += (filtros == "") ? `nuiPaciente=${nui}` : `&nuiPaciente=${nui}`;

    if (idIdentificacion != "")
        filtros += (filtros == "") ? `idIdentificacion=${idIdentificacion}` : `&idIdentificacion=${idIdentificacion}`;

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?${filtros}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            if ($.isEmptyObject(data)) {
                errorPaciente();
            } else {

                $('#divPacientes').show();
                $.each(data, function () {
                    adataset.push([
                        this.Identificacion.Id,
                        this.Identificacion.Id != 5 ? (`${this.NumeroDocumento}-${this.Digito}` ?? "") : "NN",
                        this.Nombre,
                        this.ApellidoPaterno,
                        this.ApellidoMaterno,
                        this.Nui,
                        this.IdPaciente,
                    ]);
                });
                LlenaGrilla(adataset, "#tblPacientes", columnas, 'Pacientes', 0);
            }
        }
    });
}

function BloquearMenu(esBloqueo) {

    $("#btnPaciente, #btnDatosClinicos, #btnDatosPrev, #btnRegistroClinico, #btnLocalizacion")
        .removeAttr("disabled")
        .removeClass("bg-secondary bg-info disabled");
    $("#btnPaciente, #btnDatosClinicos, #btnDatosPrev, #btnRegistroClinico, #btnLocalizacion")
        .addClass((esBloqueo) ? "bg-secondary disabled" : "bg-info");

}

//NO SE ENCUENTRA PACIENTE
function errorPaciente() {
    //$(".nav-link").addClass("disabled");
    limpiaCampos();
    toastr.error('No existen pacientes con los datos ingresados');
    $('#divPacientes').hide();
}

function mostrarPillSinDatosRegistroClinico(modulo) {
    $('#alertRegistroClinico').empty();
    $('#alertRegistroClinico').append("No existe Información de " + modulo);
    $('#alertRegistroClinico').show();
}

function getHojasEvolucion() {
    const idPaciente = $('#idPaciente').val();
    let adataset = [];
    let columnas = [{ title: 'ID' }, { title: 'FECHA' }, { title: 'DIAGNÓSTICO' }, { title: 'ESTADO' }, { title: '' }];
    //let url = `${GetWebApiUrl()}RCE_Hoja_Evolucion/Paciente/${idPaciente}`;
    let url = `${GetWebApiUrl()}HOS_Hoja_Evolucion/buscar?idPaciente=${idPaciente}`;
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {

            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Hoja de Evolución");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push(
                            [
                                this.Id,
                                moment(this.Fecha).format("DD-MM-YYYY HH:mm"),
                                this.DiagnosticoPrincipal,
                                this.Estado.Valor
                            ]
                        );
                    });
                    LlenaGrilla(adataset, '#tblHojaEvolucion', columnas, 'HojaEvolucion', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });
}

function getInterconsultas() {

    var idPaciente = $('#idPaciente').val();

    var adataset = [];
    var columnas = [
        { title: 'ID' },
        { title: 'DIAGNOSTICO' },
        { title: 'FECHA' },
        { title: 'ESTADO' },
        { title: 'ESPECIALIDAD ORIGEN' },
        { title: 'ESPECIALIDAD DESTINO' },
        { title: '' }];
    var url = `${GetWebApiUrl()}RCE_Interconsulta/Buscar?idPaciente=${idPaciente}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            try {
                if ($.isEmptyObject(data)) {
                    mostrarPillSinDatosRegistroClinico("Interconsulta");
                } else {
                    $('#alertRegistroClinico').hide();
                    $.each(data, function () {
                        adataset.push([
                            this.Id,
                            this.Diagnostico,
                            moment(this.Fecha).format("DD-MM-YYYY"),
                            this.Estado.Valor,
                            this.Especialidad.Origen.Valor,
                            this.Especialidad.Destino.Valor
                        ]);
                    });
                    LlenaGrilla(adataset, '#tblInterconsulta', columnas, 'Inteconsulta', null);
                }
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

}

function abrirDoc(e) {

    var ruta = $(e).data('doc');
    var url = `http://10.6.180.186/laboratorio/infinity/${ruta}`;
    window.open(url, '_blank');

}

function verDocumentosLab(e) {
    var id = $(e).data('id');
    var rut = $('#lblRutPacientePill').text();

    var adataset = [];
    $.ajax({
        type: "GET",
        url: `http://10.6.180.186/laboratorio/getCasos.php?sRut=${rut}&sSampleID={id}`,
        contentType: "application/json",
        async: true,
        dataType: "json",
        success: function (data) {
            var json = JSON.parse(data.d);
            $.each(json, function (i, r) {
                adataset.push([
                    r.id,
                    r.nombre,
                    r.fecha,
                    r.sistema
                ]);
            });
        }
    });

    $('#tblDocumentosLab').addClass("nowrap").DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: "ID" },
            { title: "Nombre archivo" },
            { title: "Fecha archivo" },
            { title: "Sistema" },
            { title: "estado" }
        ],
        columnDefs: [
            { targets: [0, 4], searchable: false, visible: false },
            {
                targets: 1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    var fila = meta.row;
                    return '<a href="#/" data-doc="' + adataset[fila][1] + '" onclick="abrirDoc(this)" style="color:#0000ff;">' + adataset[fila][1] + '</a>';
                }
            },
        ],
        bDestroy: true
    });
    $('#mdlDocumentosLab').modal('show');
}

async function getLaboratorio() {

    var idPaciente = $('#idPaciente').val();
    var adataset = [];
    var sUsuario;
    var sPass;
    var sFecha;

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Usuarios/Login`,
        async: false,
        success: function (data) {
            sUsuario = data.GEN_loginUsuarios;
            sPass = data.GEN_claveUsuarios;
        }
    });
    sFecha = GetFechaActual();
    var s = `${sUsuario}|${sPass}|${moment(sFecha).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss')}|14`
    var sUrl = `http://10.6.180.236/LABWS/Lab.asmx/Orden?sRut=${$('#txtRutPaciente').val()}-${await ObtenerVerificador($("#txtRutPaciente").val())}&sFecha1=2000-01-01&sFecha2=2030-01-01&sToken=${GetTextoEncriptado(s)}`

    $.ajax({
        type: 'GET',
        url: sUrl,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'xml',
        async: false,
        success: function (response) {
            var $xml = $(response);
            var $orderData = $xml.find('OrderData');
            $orderData.each(function () {
                var g = $(this).find('GroupList');
                if (g.length > 0) {
                    var sSampleID = $(this).find('SampleID').text();
                    var sOrderID = $(this).find('InternalOrderID').text();
                    var sSampleFecha = $(this).find('RegisterDate').text();
                    var dFecha = $(this).find('RegisterDate').text();
                    var dHora = $(this).find('RegisterHour').text();
                    var dFechaOrden = moment(dFecha + " " + dHora);
                    var sSolicitado = '';
                    var sProcedencia = '';
                    var sHorario = '';
                    var iGroupID = $(g).find('GroupID').text();

                    var $demographic = $(this).find('Demographics');
                    if ($demographic.length > 0) {
                        var $demographics = $demographic.find('Demographic');

                        $demographics.each(function () {
                            var internalID = parseInt($(this).find('InternalDemographicID').text());
                            var dCurrentValue = $(this).find('DemographicCurrentValue').text();
                            switch (internalID) {
                                case 26: sProcedencia = dCurrentValue; break;
                                case 48: sHorario = dCurrentValue; break;
                                case 8: sSolicitado = dCurrentValue; break;
                                default: break;
                            }
                        });
                    }
                    adataset.push([sSampleID, sOrderID, sSampleFecha, moment(dFechaOrden).format('DD-MM-YYYY HH:mm:ss'), sSolicitado, sProcedencia, sHorario, iGroupID, '']);
                }
            });

            if (adataset.length > 0)
                $('#divLeyendaMicro').show();
            else
                $('#divLeyendaMicro').hide();

            $('#alertRegistroClinico').hide();
            $('#tblLaboratorio').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [2, 7], visible: false, searchable: false },
                        {
                            targets: 8,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;

                                return '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="verDocumentosLab(this)" >Ver documentos</a>';
                            }
                        },
                    ],
                columns: [
                    { title: 'Id petición' },
                    { title: 'Id orden' },
                    { title: 'sSampleFecha' },
                    { title: 'Fecha orden' },
                    { title: 'Solicitado por' },
                    { title: 'Procedencia' },
                    { title: 'Rutina/Urgencia' },
                    { title: 'iGroupID' },
                    { title: '' }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData[7].trim() == '-1')
                        $('td', nRow).css('background-color', '#eeffdd');
                },
                bDestroy: true
            });
        },
        error: function (error) {
            console.log(error);
        }
    });

}

function limpiarGrillaPacientes() {
    $('#tblPacientes').DataTable().clear().draw();
    $('#divPacientes').hide();
}

function limpiarGrillas() {
    if (!($.trim($("#tblEventos").html()) == '')) {
        $('#tblEventos').DataTable().clear().draw();
        $('#tblEventos_wrapper').addClass('hide');
        $('#tblEventos').addClass('hide');
    }
    if (!($.trim($("#tblFormularioIndQx").html()) == '')) {
        $('#tblFormularioIndQx').DataTable().clear().draw();
        $('#tblFormularioIndQx_wrapper').addClass('hide');
        $('#tblFormularioIndQx').addClass('hide');
    }
    if (!($.trim($("#tblProtocoloOperatorio").html()) == '')) {
        $('#tblProtocoloOperatorio').DataTable().clear().draw();
        $('#tblProtocoloOperatorio_wrapper').addClass('hide');
        $('#tblProtocoloOperatorio').addClass('hide');
    }
    if (!($.trim($("#tblHospitalizacion").html()) == '')) {
        $('#tblHospitalizacion').DataTable().clear().draw();
        $('#tblHospitalizacion_wrapper').addClass('hide');
        $('#tblHospitalizacion').addClass('hide');
    }
    if (!($.trim($("#tblReceta").html()) == '')) {
        $('#tblReceta').DataTable().clear().draw();
        $('#tblReceta_wrapper').addClass('hide');
        $('#tblReceta').addClass('hide');
    }
    if (!($.trim($("#tblBiopsia").html()) == '')) {
        $('#tblBiopsia').DataTable().clear().draw();
        $('#tblBiopsia_wrapper').addClass('hide');
        $('#tblBiopsia').addClass('hide');
    }
    if (!($.trim($("#tblUrgencia").html()) == '')) {
        $('#tblUrgencia').DataTable().clear().draw();
        $('#tblUrgencia_wrapper').addClass('hide');
        $('#tblUrgencia').addClass('hide');
    }
    if (!($.trim($("#tblEpicrisis").html()) == '')) {
        $('#tblEpicrisis').DataTable().clear().draw();
        $('#tblEpicrisis_wrapper').addClass('hide');
        $('#tblEpicrisis').addClass('hide');
    }
    if (!($.trim($("#tblIPD").html()) == '')) {
        $('#tblIPD').DataTable().clear().draw();
        $('#tblIPD_wrapper').addClass('hide');
        $('#tblIPD').addClass('hide');
    }
}

async function getCountRegistroClinico() {

    var idPaciente = $('#idPaciente').val();
    var url = GetWebApiUrl() + "/GEN_Paciente/CantidadHistoriaClinica/" + idPaciente;

    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            //console.info(data)
            try {
                $('.badge-count').show();
                $('.badge-count').empty();
                $('#bdFormularioIndQx').append(data.FORMULARIO_IND_QX.Cantidad);
                $('#bdProtocoloOperatorio').append(data.PROTOCOLO.Cantidad);
                //$('#bdExamen').val(this.EXAMEN);
                $('#bdReceta').append(data.RECETA);
                $('#bdBiopsia').append(data.BIOPSIA.Cantidad);
                $('#bdHospitalizacion').append(data.HOSPITALIZACION);
                $('#bdUrgencia').append(data.URGENCIA);
                $('#bdEpicrisis').append(data.EPICRISIS);
                $('#bdIPD').append(data.IPD);
                $('#bdHE').append(data.HOJAEVOLUCION);
                $('#bdIC').append(data.INTERCONSULTA);
                $('#bdNEA').append(data.EVENTOADVERSO);
                $('#bdST').append(data.TRANSFUSION);
                //$('.badge-count').each(function () {
                //    if ($(this).text() == '0') { $(this).hide(); }
                //});
            } catch (err) {
                console.log("error: " + err.message);
            }
        }
    });

    //no creo que LAB se pueda contar con la api, el count:
    var sUsuario;
    var sPass;
    var sFecha;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Usuarios/Login`,
        async: false,
        success: function (data) {
            sUsuario = data.GEN_loginUsuarios;
            sPass = data.GEN_claveUsuarios;
        }
    });
    sFecha = GetFechaActual();
    var s = `${sUsuario}|${sPass}|${moment(sFecha).add(1, 'hours').format('YYYY-MM-DD HH:mm:ss')}|14`
    var sUrl = `http://10.6.180.236/LABWS/Lab.asmx/Orden?sRut=${$('#txtRutPaciente').val()}-${await ObtenerVerificador($("#txtRutPaciente").val())}&sFecha1=2000-01-01&sFecha2=2030-01-01&sToken=${GetTextoEncriptado(s)}`

    var iCount = 0;
    $.ajax({
        type: 'GET',
        url: sUrl,
        async: false,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'xml',
        success: function (response) {
            var $xml = $(response);
            var $orderData = $xml.find('OrderData');
            $orderData.each(function () {
                var g = $(this).find('GroupList');
                if (g.length > 0)
                    iCount++;
            });
        }
    });

    if (iCount > 0) {
        $('#bdLAB').append(iCount);
        $('#bdLAB').show();
    }
    else
        $('#bdLAB').hide();
}

//FUNCIONES PARA PERFILES Y PERMISOS
function datosPaciente(permiso) {

    (!permiso) ? $('#txtRutPaciente').addClass('disabled').attr('disabled', true) : $('#txtRutPaciente').removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#lblRutPaciente").addClass('disabled') : $("#lblRutPaciente").removeClass('disabled');
    (!permiso) ? $("#ddlTipoIdentificacionPaciente").addClass('disabled').attr('disabled', true) : $("#ddlTipoIdentificacionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#sltTipoIdentificacion").addClass('disabled').attr('disabled', true) : $("#ddlTipoIdentificacionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtNuiPaciente").addClass('disabled').attr('disabled', true) : $("#txtNuiPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? '' : $("#lblNuiPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtNombrePaciente").addClass('disabled').attr('disabled', true) : $("#txtNombrePaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? '' : $("#lblNombrePaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtApellidoPaciente").addClass('disabled').attr('disabled', true) : $("#txtApellidoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblApellidoPaciente").addClass('disabled') : $("#lblApellidoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtApellidoMPaciente").addClass('disabled').attr('disabled', true) : $("#txtApellidoMPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblApellidoMPaciente").addClass('disabled') : $("#lblApellidoMPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtFechaNacPaciente").addClass('disabled').attr('disabled', true) : $("#txtFechaNacPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblFechaNacPaciente").addClass('disabled') : $("#lblFechaNacPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#ddlSexoPaciente").addClass('disabled').attr('disabled', true) : $("#ddlSexoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlGeneroPaciente").addClass('disabled').attr('disabled', true) : $("#ddlGeneroPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#sltNacionalidadPaciente").addClass('disabled').attr('disabled', true) : $("#sltNacionalidadPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlPaisPaciente").addClass('disabled').attr('disabled', true) : $("#ddlPaisPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlRegionPaciente").addClass('disabled').attr('disabled', true) : $("#ddlRegionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlProvinciaPaciente").addClass('disabled').attr('disabled', true) : $("#ddlProvinciaPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlCiudadPaciente").addClass('disabled').attr('disabled', true) : $("#ddlCiudadPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDireccionPaciente").addClass('disabled').attr('disabled', true) : $("#txtDireccionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblDireccionPaciente").addClass('disabled') : $("#lblDireccionPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtNumeroDireccionPaciente").addClass('disabled').attr('disabled', true) : $("#txtNumeroDireccionPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblNumeroDireccionPaciente").addClass('disabled') : $("#lblNumeroDireccionPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtTelefonoPaciente").addClass('disabled').attr('disabled', true) : $("#txtTelefonoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblTelefonoPaciente").addClass('disabled') : $("#lblTelefonoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtOtroTelefonoPaciente").addClass('disabled').attr('disabled', true) : $("#txtOtroTelefonoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblOtroTelefonoPaciente").addClass('disabled') : $("#lblOtroTelefonoPaciente").removeClass('disabled');
    (!permiso) ? $("#sltRural").addClass('disabled').attr('disabled', true) : $("#sltRural").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtEmailPaciente").addClass('disabled').attr('disabled', true) : $("#txtEmailPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblEmailPaciente").addClass('disabled') : $("#lblEmailPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtRespondePaciente").addClass('disabled').attr('disabled', true) : $("#txtRespondePaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblRespondePaciente").addClass('disabled') : $("#lblRespondePaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#ddlPuebloOrigPaciente").addClass('disabled').attr('disabled', true) : $("#ddlPuebloOrigPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#sltEscolaridad").addClass('disabled').attr('disabled', true) : $("#sltEscolaridad").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlEstadoConyuPaciente").addClass('disabled').attr('disabled', true) : $("#ddlEstadoConyuPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtFechaFallecimiento").addClass('disabled').attr('disabled', true) : $("#txtFechaFallecimiento").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblFechaFallecimientoPaciente").addClass('disabled') : $("#lblFechaFallecimientoPaciente").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#btnGuardaDatos").hide() : $("#btnGuardaDatos").show();

}

function datosClinicos(permiso) {
    (!permiso) ? $("#ddlGrupoSanguineoPaciente").addClass('disabled').attr('disabled', true) : $("#ddlGrupoSanguineoPaciente").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#rdoLatexSi").bootstrapSwitch('disabled', true) : $("#rdoLatexSi").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#rdoLatexNo").bootstrapSwitch('disabled', true) : $("#rdoLatexNo").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#switchOtrasAlergias").bootstrapSwitch('disabled', true) : $("#switchOtrasAlergias").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#txtOtrasAlergias").addClass('disabled').attr('disabled', true) : $("#txtOtrasAlergias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lblOtrasAlergias").addClass('disabled') : $("#lblOtrasAlergias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesFamiliares").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesFamiliares").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesFamiliares").addClass('disabled') : $("#lbltxtAntecedentesFamiliares").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtEnfermedadesHereditarias").addClass('disabled').attr('disabled', true) : $("#txtEnfermedadesHereditarias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtEnfermedadesHereditarias").addClass('disabled') : $("#lbltxtEnfermedadesHereditarias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtEnfermedadesPrevias").addClass('disabled').attr('disabled', true) : $("#txtEnfermedadesPrevias").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtEnfermedadesPrevias").addClass('disabled') : $("#Content_txtEnfermedadesPrevias").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesNeonatales").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesNeonatales").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesNeonatales").addClass('disabled') : $("#lbltxtAntecedentesNeonatales").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtMedicacionPrevia").addClass('disabled').attr('disabled', true) : $("#txtMedicacionPrevia").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtMedicacionPrevia").addClass('disabled') : $("#lbltxtMedicacionPrevia").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtTratFarmacologico").addClass('disabled').attr('disabled', true) : $("#txtTratFarmacologico").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtTratFarmacologico").addClass('disabled') : $("#lbltxtTratFarmacologico").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesObstetricos").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesObstetricos").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesObstetricos").addClass('disabled') : $("#lbltxtAntecedentesObstetricos").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesQuirurgicos").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesQuirurgicos").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    //(!permiso) ? $("#lbltxtAntecedentesQuirurgicos").addClass('disabled') : $("#lbltxtAntecedentesQuirurgicos").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAntecedentesMorbidos").addClass('disabled').attr('disabled', true) : $("#txtAntecedentesMorbidos").removeClass('disabled').removeClass('editable').removeAttr('disabled');

    // HÁBITOS
    (!permiso) ? $("#txtDetalleTabaco").addClass('disabled').attr('disabled', true) : $("#txtDetalleTabaco").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDetalleOH").addClass('disabled').attr('disabled', true) : $("#txtDetalleOH").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#txtDrogas").addClass('disabled') : $("#txtDrogas").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtHipertension").addClass('disabled') : $("#txtHipertension").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtDiabetes").addClass('disabled') : $("#txtDiabetes").removeClass('disabled').removeClass('editable');
    (!permiso) ? $("#txtAsma").addClass('disabled') : $("#txtAsma").removeClass('disabled').removeClass('editable');

    (!permiso) ? $("#btnGuardaClinicos").hide() : $("#btnGuardaClinicos").show();

}

function datosPrevisionales(permiso) {
    (!permiso) ? $("#ddlAseguradora").addClass('disabled').attr('disabled', true) : $("#ddlAseguradora").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#ddlTramo").addClass('disabled').attr('disabled', true) : $("#ddlTramo").removeClass('disabled').removeClass('editable').removeAttr('disabled');
    (!permiso) ? $("#switchPrais").bootstrapSwitch('disabled', true) : $("#switchPrais").bootstrapSwitch('disabled', false).removeClass('editable');
    (!permiso) ? $("#btnGuardaPrevision").hide() : $("#btnGuardaPrevision").show();
}


function cargaPerfiles() {
    // 6 = Admisión, 5 = Enfermera de Servicio, 15 = Gestión Cama y 7 = Admin. de Sistema.
    $("#btnNuevoPacienteModal").removeClass("bg-info").addClass('bg-secondary disabled').attr('disabled', true);

    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.admision || sSession.CODIGO_PERFIL === perfilAccesoSistema.recaudacion || sSession.CODIGO_PERFIL === perfilAccesoSistema.admisionUrgencia || sSession.CODIGO_PERFIL === perfilAccesoSistema.grd || sSession.CODIGO_PERFIL === perfilAccesoSistema.enfermeriaMatroneria)
        $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);
    switch (sSession.CODIGO_PERFIL) {
        case perfilAccesoSistema.medico:
            //Recaudación
            datosPaciente(false);
            datosClinicos(true);
            datosPrevisionales(true);
            break;
        case perfilAccesoSistema.recaudacion:
            //Recaudación
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break;
        case perfilAccesoSistema.usuariodeConsulta:
            //Usuario de Consulta
            datosPaciente(false);
            datosClinicos(false);
            datosPrevisionales(false);
            break;
        case perfilAccesoSistema.enfermeraUrgencia:
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break;
        case "1": // Médico
        case "2": // PROFESIONAL GES
        case "3": // APOYO CLINICO
        case "8": // MONITOR(A) GES
        case "9": // DIGITADOR(A) GES
        case "10":// INTERCONSULTOR
        case "12":// INTERNO DE MEDICINA
        case perfilAccesoSistema.admision:// ADMISIÓN
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
            break;
        case perfilAccesoSistema.enfermeriaMatroneria:// ENFERMERIA/MATRONERIA
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
            break;
        case perfilAccesoSistema.admisionUrgencia:// ADMISIÓN URGENCIA
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
            break;
        case perfilAccesoSistema.gestionPacientes:// GESTIÓN DE PACIENTES
            datosPaciente(true);
            //$('#txtRutPaciente').addClass('disabled').attr('disabled', true);
            $("#ddlTipoIdentificacionPaciente").addClass('disabled').attr('disabled', true);
            datosClinicos(false);
            datosPrevisionales(true);
            //Debe poder modificar el NUI
            $("#txtNuiPaciente").removeClass('disabled').attr('disabled', false);
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
            break;
        //GRD
        case perfilAccesoSistema.grd:
            datosPaciente(true);
            datosClinicos(false);
            datosPrevisionales(true);
            break
        case "5":
        case "7":
        case perfilAccesoSistema.gestionCama:
            $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
            break;
        case perfilAccesoSistema.estudianteMedicina: // Estudiante de medicina
            // Mostrar el botón "Actualizar NUI"
            $("#btnActualizarNUI").show();
        case perfilAccesoSistema.fichero: //FICHERO
            $("#dvCaratula").prop("style", "display: flex;");
            break;
        default:
            datosPaciente(false);
            datosClinicos(true);
            datosPrevisionales(false);
            break;
    }

    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.gestionPacientes)
        $("#btnNuevoPacienteModal").removeClass('bg-secondary disabled').addClass("bg-info").attr('disabled', false);

}

function linkPacienteCaratula() {
    if (idPac != null) {
        var url = ObtenerHost() + "/Vista/ModuloPaciente/ImprimirCaratulaFicha.aspx?id=" + idPac;
        window.open(url);
    }
}

function imprimirFonasa(dataFonasa) {
    console.log(dataFonasa)
    ImprimirApiExterno(`${GetWebApiUrl()}GEN_paciente/Fonasa/Imprimir`, null, "POST", JSON.stringify(dataFonasa))
}

async function confirmacionTraerDatosFonasa() {
    ShowModalAlerta(
        'CONFIRMACION',
        'Traer datos desde Fonasa',
        'Los datos del paciente serán reemplazados por los que vienen de fonasa, ¿Esta seguro que desea realizar esta modificación?',
        remplazarDatosPaciente,
        CancelarAlertaModal,
        'ACEPTAR',
        'CERRAR'
    )
}

async function remplazarDatosPaciente() {

    const numeroDocumento = $("#txtRutPaciente").val()

    if (numeroDocumento === "" || numeroDocumento === undefined)
        return

    ShowModalCargando(true)
    await ObtenerPacietneDesdeFonasa(numeroDocumento)
    ShowModalCargando(false)

    await VolverAlBuscador()
    await sleep(200)
    $("#btnFiltroBandeja").trigger("click")
    await CancelarAlertaModal()

}

async function CancelarAlertaModal() {
    $('#modalAlerta').modal('hide');
}

async function ObtenerPacietneDesdeFonasa(numeroDocumento) {

    const url = `${GetWebApiUrl()}GEN_Paciente/${numeroDocumento}/Fonasa`

    try {
        await $.ajax({
            type: 'PATCH',
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: async function (data, status, jqXHR) {
                toastr.success("Paciente Actualizado desde FONASA");
            },
            error: async function (jqXHR, status) {
                throw new Error("Error obteniendo datos desde API FONASA.")
            }
        })
    }
    catch (error) {
        console.error(`Error: ${error.message}`)
    }
}

// FUNCION PARA OBTENER NUI DESDE TELEMEDICAL
async function actualizarNuiPaciente() {

    ShowModalCargando(true);

    let numeroDocumento = $("#txtRutPaciente").val();
    const url = `${GetWebApiUrl()}GEN_Paciente/${numeroDocumento}/Telemedical`;

    try {
        const response = await $.ajax({
            type: 'GET',
            url: url,
            dataType: "json"
        });

        // Verificar si se obtuvo una respuesta válida y si existe el campo file_number
        if (response && response.patient && response.patient.length > 0 && response.patient[0].file_number) {
            // Actualizar el valor del campo txtNuiPaciente con el nuevo file_number obtenido
            const nuevoNUI = response.patient[0].file_number;
            $("#txtNuiPaciente").val(nuevoNUI);

            $("#txtNuiPaciente").trigger('change');

            // Actualizar el valor del NUI en #lblNUIPacientePill
            $('#lblNUIPacientePill').empty().append(`${nuevoNUI ? `Número ubicación interna: ${nuevoNUI}` : "No posee NUI"}`);

            toastr.success("NUI Actualizado desde TELEMEDICAL");

            ShowModalCargando(false);
        } else {
            toastr.error("Error al obtener el NUI desde TELEMEDICAL.");
            ShowModalCargando(false);
        }

    } catch (error) {
        console.error(`Error: ${error.message}`);
        toastr.error("Error al obtener el NUI desde TELEMEDICAL.");
    }
}

async function VolverAlBuscador() {

    await sleep(500)
    $("#btnBusqueda").trigger("click")
}
async function localizarPaciente() {
    var idPaciente = $('#idPaciente').val();

    let atencionesUrgencia = await promesaAjax("GET", `URG_Atenciones_Urgencia/Buscar?idPaciente=${idPaciente}&idTipoEstadoSistemas[0]=100&idTipoEstadoSistemas[1]=102&idTipoEstadoSistemas[2]=103&idTipoEstadoSistemas[3]=104`)

    let atencionesHospitalizacion = await promesaAjax("GET", `HOS_Hospitalizacion/Buscar?idPaciente=${idPaciente}&idTipoEstado=156&idTipoEstado=87&idTipoEstado=88`)

    $("#tblLocalizacionHospitalizacion").empty()
    $("#tblLocalizacionUrgencia").empty()

    if (atencionesUrgencia.length == 0 && atencionesHospitalizacion.length == 0) {//No se encontraron atenciones activas
        $("#divInfoUrgenciaHospitalizacion").slideDown()
        return false
    }
    let columnastabla = [
        { title: "Id", data: "Id" },
        { title: "Nombre", data: "Paciente" },
        { title: "Edad", data: "Paciente.Edad.edad" }
    ]

    let columnasDef = [
        {
            targets: 1,
            render: function (paciente) {
                return `${paciente.Nombre} ${paciente.ApellidoPaterno} ${paciente.ApellidoMaterno}`
            }
        },
        {
            targets: 3,
            render: function (fecha) {
                return moment(fecha).format("DD-MM-YYYY")
            }
        }
    ]


    if (atencionesUrgencia.length > 0) {
        //Carga Atenciones de urgencia
        $("#divTblLocalizacionUrgencia").slideDown()
        let columnasTablaUrgencia = [
            ...columnastabla,
            { title: "Fecha admision", data: "Fecha Admisión" },
            { title: "Motivo consulta", data: "MotivoConsulta", visible: false },
            { title: "Tipo atencion", data: "NombreTipoAtencion" },
            { title: "Estado", data: "TipoEstadoSistema.Valor" },
            { title: "Categorización", data: "Categorizacion", render: (categorizacion) => categorizacion != null ? categorizacion.Codigo : "" }
        ]
        $("#tblLocalizacionUrgencia").DataTable({
            data: atencionesUrgencia,
            columns: columnasTablaUrgencia,
            columnDefs: columnasDef,
            destroy: true
        });
    } else {
        $("#divInfoUrgenciaHospitalizacion").slideUp()
        if ($.fn.DataTable.isDataTable("#tblLocalizacionUrgencia"))
            $("#tblLocalizacionUrgencia").DataTable().destroy();
    }

    if (atencionesHospitalizacion.length > 0) {
        //Carga Atenciones de hospitalizacion
        $("#divTblLocalizacionHospitalizacion").slideDown()
        let columnasTablaHospitalizacion = [
            ...columnastabla,
            { title: "Fecha admisión", data: "Fecha" },
            { title: "Ambito", data: "Ambito.Valor" },
            { title: "Cama", data: "Cama.Actual.Valor" },
            { title: "Ubicacion", data: "Cama" },
            { title: "Estado", data: "Estado.Valor" }
        ]
        columnasDef.push(
            {
                targets: 5,
                render: function (cama) {
                    console.log("cama", cama)
                    if (cama.Actual !== null) {
                        return `${cama.Actual.Valor}`
                    }
                },
                targets: 6,
                render: function (cama) {
                    if (cama.Actual !== null) {
                        return `${cama.Actual.Ubicacion.Valor}`
                    }
                }
            }
        )
        $("#tblLocalizacionHospitalizacion").DataTable({
            data: atencionesHospitalizacion,
            columns: columnasTablaHospitalizacion,
            columnDefs: columnasDef,
            destroy: true
        });
    } else {
        $("#divTblLocalizacionHospitalizacion").slideUp()
        if ($.fn.DataTable.isDataTable("#tblLocalizacionHospitalizacion"))
            $("#tblLocalizacionHospitalizacion").DataTable().destroy();
    }
}
function buscarOtrasUrgencias() {

}