﻿function ComboPrevision() {
    let url = `${GetWebApiUrl()}GEN_Prevision/Combo`;
    setCargarDataEnCombo(url, false, "#sltPrevision");
}

function ComboTramo(slt, idPrevision) {
    if (idPrevision != '' && idPrevision != undefined && idPrevision != null) {
        try {
            $("#sltPrevisionTramo").empty();
            $("#sltPrevisionTramo").prop("disabled", false);
            $("#sltPrevisionTramo").append("<option value='0'>Seleccione</option>");


            let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${idPrevision}`;
            setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
        } catch (err) {
            alert(err.message);
        }
    }
}

function comboPrevisionTramo(idPrevision) {
    if (idPrevision === null)
        return

    if (idPrevision !== 0) {
        let url = `${GetWebApiUrl()}GEN_Prevision_Tramo/${idPrevision}`;
        setCargarDataEnCombo(url, false, $("#sltPrevisionTramo"));
    }
}

function CombosTiposDomicilios() {
    let url = `${GetWebApiUrl()}GEN_Tipos_Domicilio/Combo`;
    setCargarDataEnCombo(url, false, "#sltRural");
    $("#sltServicioSalud").val('1').attr("disabled", "disabled");
}