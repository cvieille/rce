const selectBusquedaTipo = document.getElementById("selectBusquedaTipo");
const labelBusqueda = document.getElementById("labelBusqueda");
const txtBusquedaAvanzada = document.getElementById("txtBusquedaAvanzada");
const btnBuscarAvanzado = document.getElementById("btnBuscarAvanzado");
const btnLimpiarFiltroAvanzado = document.getElementById("btnLimpiarFiltroAvanzado");

// Cambio de tipo de b�squeda (ID DAU o ID Hospitalizaci�n)
selectBusquedaTipo.addEventListener("change", function () {
    if (selectBusquedaTipo.value === "dau") {
        //labelBusqueda.innerText = "ID DAU";
        txtBusquedaAvanzada.placeholder = "Ingrese ID DAU";
    } else {
        //labelBusqueda.innerText = "ID HOS";
        txtBusquedaAvanzada.placeholder = "Ingrese ID HOS";
    }
});

// Acci�n al hacer clic en el bot�n de buscar
btnBuscarAvanzado.addEventListener("click", function () {
    const idBusqueda = txtBusquedaAvanzada.value.trim();
    const tipoBusqueda = selectBusquedaTipo.value;

    if (idBusqueda === "") {
        toastr.error("Debe ingresar un ID para buscar");
        return;
    }

    if (tipoBusqueda === "dau") {
        buscarPacienteUrgencia(idBusqueda);
    } else if (tipoBusqueda === "hos") {
        buscarPacienteHospitalizacion(idBusqueda);
    }
});

// Fx para realizar la b�squeda por urgencias (ID DAU)
function buscarPacienteUrgencia(idDAU) {
    $.ajax({
        url: `${GetWebApiUrl()}URG_Atenciones_Urgencia/${idDAU}`,
        type: 'GET',
        success: function (data) {
            if (data) {
                console.log("OBTENGO DATOS DESDE ID DAU:", data);
                let idPaciente = data.IdPaciente; // Capturar idPaciente
                buscarDetallesPaciente(idPaciente);
            } else {
                console.log("No se encontr� ning�n paciente con el ID DAU ingresado.");
            }
        },
        error: function (error) {
            console.error("Error en la b�squeda del paciente por DAU", error);
        }
    });
}

// Fx para realizar la b�squeda por hos
function buscarPacienteHospitalizacion(idHos) {
    $.ajax({
        url: `${GetWebApiUrl()}HOS_Hospitalizacion/${idHos}`,
        type: 'GET',
        success: function (data) {
            if (data && data.Paciente) {
                console.log("OBTENGO DATOS DESDE ID HOSPITALIZACION:", data);
                let idPaciente = data.Paciente.Id; // Capturar idPaciente desde la hos
                buscarDetallesPaciente(idPaciente);
            } else {
                console.log("No se encontr� ning�n paciente con el ID de hospitalizaci�n ingresado.");
            }
        },
        error: function (error) {
            console.error("Error en la b�squeda del paciente por hospitalizaci�n", error);
        }
    });
}

// Segunda llamada para obtener detalles del paciente por idPaciente
function buscarDetallesPaciente(idPaciente) {
    $.ajax({
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idPaciente=${idPaciente}`,
        type: 'GET',
        success: function (paciente) {
            console.log("DATOS ESPECIFICOS MEDIANTE ID PACIENTE:", paciente);
            mostrarPacienteEnTabla(paciente);  // Mostrar los datos del paciente en la tabla
        },
        error: function (error) {
            console.error("Error al buscar detalles del paciente", error);
        }
    });
}

// Fx para mostrar los datos del paciente en una tabla
function mostrarPacienteEnTabla(pacientes) {
    let tableBody = $('#tablaPacientes tbody');
    tableBody.empty(); // Limpiar la tabla antes de llenarla

    // Iterar sobre los pacientes y llenar la tabla
    pacientes.forEach(function (paciente) {
        let nombreCompleto = `${paciente.Nombre || ''} ${paciente.ApellidoPaterno || ''} ${paciente.ApellidoMaterno || ''}`;
        let row = `
                <tr>
                    <td>${paciente.IdPaciente || 'No tiene'}</td>
                    <td>${nombreCompleto.trim() || 'NN'}</td>
                    <td>
                        <a class="btn btn-info btnSeleccionPaciente" data-target="#panelBoton" onclick="linkPacienteRut(5, '${nombreCompleto.trim()}', ${paciente.IdPaciente})">
                            <i class="fas fa-eye"></i> Ver
                        </a>
                    </td>
                </tr>`;
        tableBody.append(row);
    });

    // Mostrar la tabla
    $('#tablaPacientes').removeClass('d-none');

    // Inicializar DataTable si no est� inicializada
    if (!dataTable) {
        dataTable = $('#tablaPacientes').DataTable({
            responsive: true,
            columnDefs: [
                { width: "60px", targets: -1 }
            ],
            autoWidth: false
        });
    } else {
        dataTable.clear().rows.add(tableBody.find('tr')).draw();
    }
}

// Fx para limpiar la tabla y ocultarla
function limpiarTabla() {
    $('#tablaPacientes tbody').empty();
    $('#tablaPacientes').addClass('d-none');

    if (dataTable) {
        dataTable.destroy();
        dataTable = null;
    }
}

// Acci�n al hacer clic en el bot�n de limpiar filtros
btnLimpiarFiltroAvanzado.addEventListener("click", function () {
    txtBusquedaAvanzada.value = "";
    limpiarTabla();
    limpiaCampos();
});