﻿
async function getMovimientosPaciente(id) {

    if (id === undefined || id === null)
        return

    try {
        const movimientos = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/${id}/Movimientos`,
            contentType: 'application/json',
            dataType: 'json',
        })

        return movimientos

    } catch (error) {
        console.error("Error al cargar los movimientos del paciente")
        console.log(JSON.stringify(error))
    }
}

async function preparaDatosTablaMovimientos({ Movimiento }) {

    if (Movimiento === undefined || Movimiento === null)
        return

    let adataset = []

    $.each(Movimiento, function () {
        adataset.push([
            this.Id,
            (this.Fecha != null) ? moment(new Date(this.Fecha)).format('DD-MM-YYYY HH:mm:ss') : '',
            this.TipoMovimiento,
            this.Usuario
        ]);
    });

    return adataset
}

async function dibujarTablaMovimientosPaciente(tabla, movimientos) {

    $(tabla).addClass("nowrap").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                title: `Movimiento del paciente: ${getNombrePacienteMov()}`,
                className: 'btn btn-info',
                text: 'Exportar a Excel'
            },
            {
                extend: 'pdf',
                title: `Movimiento del paciente: ${getNombrePacienteMov()}`,
                className: 'btn btn-info',
                text: 'Exportar en PDF'
            }
        ],
        data: movimientos,
        order: [],
        columns: [
            { title: "ID" },
            { title: "Fecha Movimiento" },
            { title: "Movimiento" },
            { title: "Usuario" }
        ],
        "columnDefs": [
            {
                "targets": 1,
                "sType": "date-ukLong"
            }
        ],
        "bDestroy": true
    });
}

function getNombrePacienteMov() {

    const nombre = $("#txtNombrePaciente").val()
    const primerApellido = $("#txtApellidoPaciente").val()
    const segundoApellido = $("#txtApellidoMPaciente").val()

    const nombrePaciente = `${nombre} ${primerApellido} ${segundoApellido}`

    return nombrePaciente
}