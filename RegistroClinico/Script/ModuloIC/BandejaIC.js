﻿
var sSession;
let idPro, isBoss = false, hasMoreThanOne = false;
let idDiv;
let especialidad, idEspecialidad, fechaActual, fechaMinima

$(document).ready(function () {

    defineRangosdeFecha();
    accionesJefeEspecialidad();

    $("#txtFechaCierre").on("change", function () {
        if ($("#txtFechaIngreso").val() !== "") {
            let diferencia = moment($(this).val()).diff(moment($("#txtFechaIngreso").val()), 'days')
            if (diferencia < 0) {
                toastr.warning("La fecha de cierre no puede ser menor a la fecha de ingreso")
                $("#txtFechaCierre").val("")
            }
        }
    })

    $("#txtFechaIngreso").on("change", function () {
        if ($("#txtFechaCierre").val() !== "") {
            let diferencia = moment($("#txtFechaCierre").val()).diff(moment($(this).val()), 'days')

            if (diferencia < 0) {

                toastr.warning("La fecha de ingreso no puede ser mayor a la fecha de cierre")
                $("#txtFechaIngreso").val("")
            }
        }
    })

    $("#sltFiltroEstadoIc").on("change", function () {
        if ($(this).val() == 76 || $(this).val() == "76") {
            $("#txtFechaIngreso").data("required", true)
            $("#txtFechaCierre").data("required", true)
        } else {
            $("#txtFechaIngreso").removeAttr("data-required")
            $("#txtFechaCierre").removeAttr("data-required")
        }
        ReiniciarRequired()
    })

    sSession = getSession();

    RevisarAcceso(false, sSession);

    idDiv = "";

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {
            idPro = data[0].Profesional.Id;
            //Es jefe de especialidad
            if (data[0].Profesional.JefeEspecialidad.length > 0)
                isBoss = true
            //Tiene mas de una especialidad
            if (data[0].Profesional.JefeEspecialidad.length > 1)
                hasMoreThanOne = true
            else {
                if (data[0].Profesional.Especialidad !== null) {
                    especialidad = data[0].Profesional.Especialidad.Valor
                    idEspecialidad = data[0].Profesional.Especialidad.Id
                }
            }

        }
    });
    cargarCombosBandeja();
    //Solo tiene una especialidad
    if (!hasMoreThanOne) {
        //Este filtro permitia controla que un medico no busque interconsultas fuera de su especialidad

        //$("#ddlEspecialidadDestino").on('select2:select', function (e) {
        //    let seleccionado = e.params.data.text
        //    if (seleccionado != especialidad) {
        //        if ($('#ddlEspecialidadOrigen').find("option[value='" + idEspecialidad + "']").length) {
        //            $('#ddlEspecialidadOrigen').val(idEspecialidad).trigger('change');
        //        }
        //    }
        //})
        //$("#ddlEspecialidadOrigen").on('select2:select', function (e) {
        //    let seleccionado = e.params.data.text
        //    if (seleccionado != especialidad) {
        //        if ($('#ddlEspecialidadDestino').find("option[value='" + idEspecialidad + "']").length) {
        //            $('#ddlEspecialidadDestino').val(idEspecialidad).trigger('change');
        //        }
        //    }
        //})
        //Setear especialidad origen como filtro por defecto al cargar la pagina
        //$('#ddlEspecialidadOrigen').val(idEspecialidad).trigger('change');
        //$('.select2').select2({ theme: 'bootstrap4' });

    }
    
   

    $('#txtFiltroRut').on('keypress', function (e) {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4) {
            var charCode = (e.which) ? e.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    });

    $('#txtFiltroRut').on('blur', async function () {
        if ($('#sltTipoIdentificacion').val() == 1 || $('#sltTipoIdentificacion').val() == 4)
            $('#txtHosFiltroDV').val(await ObtenerVerificador($(this).val()));
    });

    ocultarMostrarFichaPac($("#sltTipoIdentificacion"), $("#txtFiltroRut"))

    DarFuncionRadios();
    getTablaIC(sSession);

    if (sSession.CODIGO_PERFIL !== perfilAccesoSistema.medico) {
        $('#lnbNuevoIC').hide();
    }

    $('#btnFiltro').on('click', function (e) {
        getTablaIC(sSession)
        e.preventDefault();
    });
    $('#btnLimpiarFiltro').on('click', function (e) {
        limpiarFiltros();
        e.preventDefault();
    });

    // MÉTODOS DROPDOWN//////////////////////////////////

    $('body').on('click', '.dropdown-menu a', function (e) {
        $(this).parents('.dropdown').find('.btn').text($(this).text());
        $(this).parents('.dropdown').find('.btn').attr('val', $(this).attr('val'));
        e.preventDefault();
    });

    $('body').on('click', '.dropdown', function (e) {
        if ($('.dropdown-menu', this).children().length == 0)
            e.stopPropagation();
    });

    //$('#switchGES').on('switchChange.bootstrapSwitch', function (event, state) {
    //    if (state) {
    //        $('#divAuge').fadeIn(100);
    //        $('#divSubAuge').fadeIn(100);
    //    }
    //    else {
    //        $('#divAuge').fadeOut(100);
    //        $('#divSubAuge').fadeOut(100);
    //        $("#sltGrupoGes").val(0)
    //    }
    //});

    $('#btnAnular').click(function () {

        if (validarCampos("#divCierreIc", true)) {
            ShowModalAlerta('CONFIRMACION',
                'Cerrar IC',
                'Una vez cerrada esta interconsulta será quitada de la lista.',
                CerrarIC, null, 'CERRAR IC', 'CANCELAR');
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 2000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    //Boton para enviar una interconsulta desde una espcialidad (actual) a otra (nueva especialidad)
    //desde un modal en la bandeja IC
    $('#btnAceptarEsp').click(function () {
        var bValidar = validarCampos('#divEspecialidades', false);
        let nombre = $("#spanNombrePaciente").text()
        let rut = $("#spanRutPaciente").text()
        //Nombre de la especialidad destino
        let destino = $("#ddlEspecialidad2>option:selected").text()

        if (bValidar) {
            let idIC = parseInt($('#spanIdIc').text());
            $.ajax({
                type: 'PUT',
                url: `${GetWebApiUrl()}RCE_Interconsulta/CambiarEspecialidad/${idIC}/${ $('#ddlEspecialidad2').val()}`,                
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, jqXHR) {
                    $('#iID').val('0');
                    $('#mdlEspecialidad').modal('hide');
                    getTablaIC(sSession);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Interconsulta enviada",
                        text: `Interconsulta con id  ${idIC} del paciente: ${nombre} fue enviada a: ${destino}`,
                        showConfirmButton: false,
                        timer: 4000,
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            $("#mdlEspecialidad").modal('hide')
                        }
                    });

                },
                error: function (jqXHR, status) {
                    console.log(jqXHR);
                    alert('fail' + status.code);
                }
            });
        }
    });
        
    if (sSession.CODIGO_PERFIL == perfilAccesoSistema.medico) {
        $('#btnNuevaIC').show();
    }

    deleteSession('ID_INTERCONSULTA');

});

//$('#sltGrupoGes').change(async function () {
//    idPadre = $('#sltGrupoGes').val()
//    await CargarComboAugeHijo(idPadre)
//    await sleep(40)
//    console.log($("#sltSubGrupoGes > option").length)
//    if ($("#sltSubGrupoGes > option").length <= 1) { //No tiene valores
//        $('#divSubAuge').hide()
//    } else {
//        $('#divSubAuge').show()
//    }
//});

function accionesJefeEspecialidad() {
    if (isBoss) {
        $("#chkMisInterconsultas").bootstrapSwitch('state', false);
        $("#divSltFiltroEspecialista").removeClass("d-none");
    }

    else
        $("#chkMisInterconsultas").bootstrapSwitch('state', true);
}

function defineRangosdeFecha() {
    fechaActual = GetFechaActual();
    fechaActual = moment(fechaActual).format("YYYY-MM-DD");

    let fechaSinFormato = moment().subtract(10, 'year');
    fechaMinima = fechaSinFormato.format("YYYY-MM-DD");

    $("#txtFechaIngreso").prop("min", fechaMinima);
    $("#txtFechaIngreso").prop("max", fechaActual);

    $("#txtFechaCierre").prop("min", fechaMinima);
    $("#txtFechaCierre").prop("max", fechaActual);
}

function refrescarTabla() {
    getTablaIC(sSession)
}
function toggle(id) {
    if (idDiv == "") {

        $(id).fadeIn();
        idDiv = id;

    } else if (idDiv == id) {

        $(id).fadeOut();
        idDiv = "";

    } else {

        $(idDiv).fadeOut();
        $(id).fadeIn();
        idDiv = id;
    }

}


function limpiarFiltros() {

    $('#txtFiltroRut').val('');
    $('#txtFiltroNombre').val('');
    $('#txtFiltroApe').val('');
    $('#txtFiltroSApe').val('');
    /*$('#sltGrupoGes').val(0);*/
    $('#sltSubGrupoGes').empty().append("<option value='0'>-Seleccione-</option>");
    $('#sltSubGrupoGes').val(0);
    $('#ddlOrigen').val(0);
    $('#ddlEspecialidadOrigen').val(0).trigger('change');
    $('#ddlDestino').val(0);
    $('#ddlEspecialidadDestino').val(0).trigger('change');

    $('#sltFiltroEstadoIc').val(0)
    $('#sltEspecialistaFiltro').val(0)
    $('#txtFechaIngreso').val("")
    $('#txtFechaCierre').val("")
    $('#chkMisInterconsultas').bootstrapSwitch('state', false);
    $('#switchGES').bootstrapSwitch('state', false);

    getTablaIC(sSession);

}

function getTablaIC(sSession) {

    var adataset = [];

    var filtros = {};

    //if ($('#sltGrupoGes').val() != null && $('#sltGrupoGes').val() != '') {
    //    if ($("#sltSubGrupoGes").val() != null && $('#sltSubGrupoGes').val() != '')
    //        filtros["RCE_idPatologia_GES"] = $('#sltSubGrupoGes').val();
    //    else
    //        filtros["RCE_idPatologia_GES"] = $('#sltGrupoGes').val();
    //}

    if ($('#ddlEspecialidadOrigen').val() != '0')
        filtros["GEN_idEspecialidad_origenInterconsulta"] = $('#ddlEspecialidadOrigen').val();

    if (sSession.CODIGO_PERFIL == "10") {
        filtros["GEN_idEspecialidad_destinoInterconsulta"] = sSession.ID_ESPECIALIDAD;
    }
    else {
        if ($('#ddlEspecialidadDestino').val() != '0')
            filtros["GEN_idEspecialidad_destinoInterconsulta"] = $('#ddlEspecialidadDestino').val();
    }

    if ($('#switchGES').bootstrapSwitch('state'))
        filtros["Interconsulta_GES"] = 1;
    else
        filtros["Interconsulta_GES"] = 0;

    if ($('#txtFiltroRut').val() != '0')
        filtros["GEN_numero_documentoPaciente"] = $('#txtFiltroRut').val();
    if ($('#txtFiltroNombre').val() != '0')
        filtros["GEN_nombrePaciente"] = $('#txtFiltroNombre').val();
    if ($('#txtFiltroApe').val() != '0')
        filtros["GEN_ape_paternoPaciente"] = $('#txtFiltroApe').val();
    if ($('#txtFiltroSApe').val() != '0')
        filtros["GEN_ape_maternoPaciente"] = $('#txtFiltroSApe').val();

    if ($('#sltEspecialistaFiltro').val() != '0' && $('#txtFilsltEspecialistaFiltrotroSApe').val() != 0)
        filtros["IdProfesionalResponsable"] = $('#sltEspecialistaFiltro').val();

    if ($('#sltFiltroEstadoIc').val() != '0' && $('#sltFiltroEstadoIc').val() != 0 && $('#sltFiltroEstadoIc').val() != null) {
        filtros["IdEstado"] = $('#sltFiltroEstadoIc').val()

        if (parseInt($('#sltFiltroEstadoIc').val() == 76)) {
            if ($("#txtFechaIngreso").val() == "" || $("#txtFechaCierre").val() == "") {
                Swal.fire(
                    'Atención',
                    'Para busqueda de interconsultas cerradas es necesario introducir fecha de ingreso y cierre de la interconsulta',
                    'info'
                )
                return false
            }
        }
    }


    if ($("#chkMisInterconsultas").bootstrapSwitch('state')) {
        filtros["IdProfesional"] = idPro
    }

    if ($("#txtFechaIngreso").val() !== "") {
        filtros["FechaIngreso"] = moment($("#txtFechaIngreso").val()).format("DD-MM-YYYY")
    }

    if ($("#txtFechaCierre").val() !== "") {
        filtros["FechaCierre"] = moment($("#txtFechaCierre").val()).format("DD-MM-YYYY")
    }

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Interconsulta/Bandeja?` +
            `idPatologiaGES=${filtros["RCE_idPatologia_GES"]}` +
            `&idEspecialidadDestino=${filtros["GEN_idEspecialidad_destinoInterconsulta"]}` +
            `&idEspecialidadOrigen=${filtros["GEN_idEspecialidad_origenInterconsulta"]}` +
            `&interconsultaGES=${filtros["Interconsulta_GES"]}` +
            `&numeroDocumento=${filtros["GEN_numero_documentoPaciente"]}` +
            `&nombre=${filtros["GEN_nombrePaciente"]}` +
            `&apellidoPaterno=${filtros["GEN_ape_paternoPaciente"]}` +
            `&apellidoMaterno=${filtros["GEN_ape_maternoPaciente"]}` +
            `&idProfesional=${filtros["IdProfesional"]}` +
            `&idEstado=${filtros["IdEstado"]}` +
            `&fechaInicio=${filtros["FechaIngreso"] != undefined ? filtros["FechaIngreso"] : ""}` +
            `&fechaTermino=${filtros["FechaCierre"] != undefined ? filtros["FechaCierre"] : ""}` +
            `&idProfesionalResponsable=${filtros["IdProfesionalResponsable"]}`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            let nombreProfesionalDestino, idProfesionalResponsable
            $.each(data, function (key, val) {

                let nombrePaciente = val.Paciente.Nombre + " "
                val.Paciente.ApellidoPaterno != null ? nombrePaciente += val.Paciente.ApellidoPaterno + " " : ""
                val.Paciente.ApellidoMaterno != null ? nombrePaciente += val.Paciente.ApellidoMaterno : ""

                if (val.Destino.Profesional == null) {
                    nombreProfesionalDestino = 'Sin asignar'
                    idProfesionalResponsable = null
                } else {
                    nombreProfesionalDestino = val.Destino.Profesional.Nombre + ' ' + val.Destino.Profesional.ApellidoPaterno
                    idProfesionalResponsable = val.Destino.Profesional.Id
                }
                adataset.push([
                    /*0*/val.Id,
                    /*1*/moment(moment(val.Fecha).toDate()).format('DD-MM-YYYY'),
                    /*2*/nombrePaciente,
                    /*3*/val.Estado.Valor,
                    /*4*/val.Origen.Especialidad.Valor,
                    /*5*/`${val.Origen.Profesional.Nombre} ${val.Origen.Profesional.ApellidoPaterno} ${val.Origen.Profesional.ApellidoMaterno}`,
                    /*6*/val.Destino.Especialidad.Valor,
                    /*7*/nombreProfesionalDestino,
                    /*8*/val.Diagnostico,
                    /*9*/val.Sintomas,
                    /*10*/val.Solicitud,
                    /*11*/val.Dias + ' Días',
                    /*12*/val.Ambito.Valor,
                    /*13*/false,
                    /*14*/val.Estado.Id,
                    /*15*/val.Paciente.Id,
                    /*16*/val.Respuestas != null ? val.Respuestas.Respuesta : "Sin respuesta",
                    /*17*/val.Acciones.Cerrar,
                    /*18*/val.Acciones.Editar,
                    /*19*/val.Acciones.CambiarEspecialidad,
                    /*20*/val.Acciones.Imprimir,
                    /*21*/false,
                    /*22*/val.Acciones.Responder,
                    //23 val.Acciones.VerArchivosAdjuntos,
                    /*23*/val.Acciones.VerMovimientos,
                    /*24*/idProfesionalResponsable,
                    /*25*/val.Acciones.AsignarProfesional,
                    /*26*/val.Destino.Especialidad.Id,
                    /*27*/val.Cierre != null ? val.Cierre.MotivoCierre.Valor : "N/A",
                    /*28*/val.Cierre != null ? val.Cierre.Observacion : "N/A",
                    /*29*/val.Paciente.NumeroDocumento != null ? val.Paciente.NumeroDocumento : "N/A",
                    /*30*/val.Ges == true ? true : false,
                    /*31*/val.Origen.Especialidad.Id
                ]);
            });
            //Aca deberia ir el id de la hospitalizacion
            LlenaGrillaIC(adataset, '#tblIC', sSession);
        }
    });
}
function LlenaGrillaIC(datos, grilla, sSession) {
    if (datos.length == 0)
        $('#lnbExportar').addClass('disabled');
    else
        $('#lnbExportar').removeClass('disabled');
    var tableIc = $(grilla).addClass("text-wrap").addClass("dataTable").DataTable({
        data: datos,
        order: [],
        columns: [
            /*0*/{ title: "IDIC", className: "dt-control rce-tray-id" },
            /*1*/{ title: "Fecha", className: "dt-control" },
            /*2*/{ title: "Paciente", className: "dt-control text-wrap" },
            /*3*/{ title: "Estado", className: "dt-control" },
            /*4*/{ title: "Solicitud origen", className: "overflow-auto dt-control" },
            /*5*/{ title: "Prof Solicita", className: " dt-control" },
            /*6*/{ title: "Solicitud destino", className: "overflow-auto dt-control" },
            /*7*/{ title: "Prof. Responsable", className: "" },
            /*8*/{ title: "Diagnóstico", className: " overflow-auto" },
            /*9*/{ title: "Síntomas", className: " overflow-auto" },
            /*10*/{ title: "Solicitud", className: "" },
            /*11*/{ title: "Días", className: "t dt-control" },
            /*12*/{ title: "Ambito", className: "" },
            /*13*/{ title: "Patologia GES", className: "" },
            /*14*/{ title: "GEN_idTipo_Estados_Sistemas", className: "" },
            /*15*/{ title: "GEN_idPaciente", className: "" },
            ///**/{ title: "idEspecialidad", className: "text-center" },
            /*16*/{ title: "Respuesta", className: "" },
            //{ title: "Tiempo transcurrido", className: "text-center" },
            /*17*/{ title: "Ges", className: "" },
            /*18*/{ title: "Estado", className: "dt-control text-center" },
            /*19*/{ title: "", className: "" }
        ], "columnDefs": [
            { "targets": 5, "sType": "date-ukLong" },
            {
                "targets": 17,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {
                    var fila = meta.row;
                    let button
                    //es ges = true

                    if (datos[fila][30])
                        button = `<i class="fas fa-check text-center text-success"></i>`
                    else
                        button = `<i class="fas fa-times text-danger"></i>`


                    return button
                }
            },

            {
                "targets": -1,
                "data": null,
                orderable: false,
                "render": function (data, type, row, meta) {

                    var fila = meta.row;
                    var botones;
                    var botones =
                        `
                        <button class='btn btn-primary btn-circle' 
                                onclick='toggle("#div_accionesIC${fila}"); return false;'>
                                <i class="fa fa-list" style="font-size:13px;"></i>
                        </button>
                        <div id='div_accionesIC${fila}' class='btn-container' style="display: none;">
                              <center>
                              <i class="fas fa-caret-down" style="color: #007bff; font-size: 16px;"></i>
                              <div class="rounded-actions">
                                <center>
                                `;
                    //Editar
                    if (datos[fila][18]) {
                        botones += `<a id='linkVerEditar' data-id=${datos[fila][0]} class='btn btn-default btn-circle btn-lg' href='#/' onclick='linkVerEditar(this)' data-toggle="tooltip" data-placement="left" title="Ver/Editar IC"><i class='fa fa-edit'></i></a><br>`;
                    }
                    //Imprimir
                    if (datos[fila][20]) {
                        botones += `<a id='linkImprimir' data-id=${datos[fila][0]} class='btn btn-default btn-circle btn-lg' href='#/' onclick='linkImprimir(this);' data-print='true' data-frame='#frameIC' data-toggle="tooltip" data-placement="left" title="Imprimir"><i class='fa fa-print'></i></a><br>`;
                    }
                    //ver movimientos
                    if (datos[fila][23]) {
                        botones += `<a id='linkMovimientos' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg ' href='#/' onclick='linkMovimientos(this)' data-toggle="tooltip" data-placement="left" title="Movimientos"><i class='fa fa-list'></i></a><br>`;
                    }
                    //Archivos adjuntos
                    //if (datos[fila][23]) {
                    //    botones += `<a id='linkArchivosAdjuntos' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkVerArchivosAdjuntos(this);'><i class='fa fa-clipboard' data-toggle="tooltip" data-placement="left" title="Adjuntos"></i></a><br>`;
                    //}
                    //Dar Respuesta
                    if (datos[fila][22]) {
                        //75 ingresado y que el profesional logeado sea el destinatario de la IC
                        var sRes = 'Responder';
                        //Si ya tiene respuesta
                        if (datos[fila][16] != "N/A")
                            sRes = 'Editar Respuesta';
                        botones += `<a id='linkResponder' data-id=${datos[fila][0]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkResponder(this)'><i class='fa fa-reply' data-toggle="tooltip" data-placement="left" title="${sRes}"></i></a><br>`;

                    }
                    //Establecer especialidad
                    if (datos[fila][19] && datos[fila][14] == 75) {
                        botones += `<a id='linkEspecialidad' data-id=${datos[fila][0]} data-nombrePaciente="${datos[fila][2]}" data-rut="${datos[fila][29]}"  data-origen=${datos[fila][31]} data-destino=${datos[fila][26]} class='btn btn-info btn-circle btn-lg' href='#/' onclick='linkEspecialidad(this)' data-toggle="tooltip" data-placement="left" title="Cambiar Especialidad"><i class='fa fa-paper-plane' ></i></a><br>`;
                    }
                    //asignar especialista
                    if (datos[fila][25] && datos[fila][14] == 75) {
                        botones += `<a id='linkAsignarEspecialidad' 
                        data-id=${datos[fila][0]} 
                        data-nombrePaciente="${datos[fila][2]}" 
                        data-esp=${datos[fila][26]} 
                        data-diagnostico="${datos[fila][8]}"
                        data-profsolicita="${datos[fila][5]}"
                        data-espdestino="${datos[fila][6]}"
                        class='btn btn-info btn-circle btn-lg'
                        href='#/' onclick='AsignarEspecialista(this)'
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Asignar especialista">
                        <i class="fas fa-exchange-alt"></i></a><br>`;
                    }

                    //Cerrar y rechazar consulta.
                    if (sSession.CODIGO_PERFIL === perfilAccesoSistema.medico) {
                        //75 = Ic ingresada, IC rechazada por destino
                        if (datos[fila][14] !== 76 && datos[fila][17] == true)
                            //data-respuesta="${datos[fila][16]}"
                            botones += `<a id='linkCerrar' 
                                data-id=${datos[fila][0]}
                                data-nombrePaciente="${datos[fila][2]}"
                                data-estado="${datos[fila][3]}"
                                data-especialidadOrigen="${datos[fila][4]}"
                                data-especialidadDestino="${datos[fila][6]}"
                                class='btn btn-warning btn-circle btn-lg'
                                href='#/'
                                onclick='linkCerrar(this)'>
                                    <i class='fa fa-times-circle'
                                        data-toggle="tooltip"
                                        data-placement="left"
                                        title="Cerrar IC">
                                    </i>
                                </a>
                                <br>`;
                        //if (datos[fila][14] == 75 || datos[fila][4] == 78)
                        //    botones += `<a id='linkRechazar' data-id=${datos[fila][0]} data-nombre="${datos[fila][2]}" data-rut="${datos[fila][29]}" class='btn btn-danger btn-circle btn-lg' href='#/' onclick='linkRechazar(this)'><i class='fa fa-times' data-toggle="tooltip" data-placement="left" title="Rechazar IC"></i></a><br>`;
                    }

                    botones += `
                                    </center >
                                    </div >
                                </center >
                            </div >`
                        ;
                    return botones;
                }
            },{
                "targets": -2,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    let botones = ``, color = "", titulo = "", letra = ""
                    switch (datos[fila][14]) {
                        case 75:
                            color = `primary`
                            titulo = `ingresada`
                            letra = "I"
                            break
                        case 76:
                            color = `danger`
                            titulo = `cerrada`
                            letra = "C"
                            break
                        case 77:
                            color = `secondary`
                            titulo = `aceptada`
                            letra = "A"
                            break
                        case 83:
                            color = `success`
                            titulo = `respondida`
                            letra = "R"
                            break
                        default:
                            color = `danger`
                            titulo = `Rechazada`
                            letra = "R"
                            break
                    }
                    botones = `<button class="btn 
                                        btn-outline-${color} btn-circle"
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        data-original-title="Interconsulta ${titulo}">
                                        <b>${letra}</b>
                                        </button>`
                    return botones;
                }
            },{
                "targets": [3, 7, 8, 9, 10, 12, 13, 14, 15, 16],
                "visible": false
            },
            //ocultar columnas en responsivo.
            { responsivePriority: 3, targets: 0 }, // Id
            { responsivePriority: 1, targets: 2 }, // nombre paciente
            { responsivePriority: 2, targets: 19 }, // botonera
            { responsivePriority: 4, targets: 1 }, // fecha
            { responsivePriority: 5, targets: 6 }, // solicitud. destino
            { responsivePriority: 6, targets: 4 }, //solicitud origen
            { responsivePriority: 7, targets: 5 }, //profesional solicita
            { responsivePriority: 8, targets: 17 }, //icono info
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData[7] == "Sin asignar") {
                $('td', nRow).css('background-color', '#FFF8D9');//FFF8D9- amarillo
            }
        },

        //Crea la nueva tabla al redimensionar
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        var toReturn = '';
                        if (col.hidden) {
                            toReturn = toReturn.concat('<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">');

                            if (col.title == '') {

                                //Obtener objeto JQuery desde una cadena HTML
                                var button = $(col.data, "button").get();

                                var ID;
                                var newID;

                                if (button) {
                                    ID = $(button).attr("id");
                                    var r = ID.replace('#', '');
                                    newID = r + "-resp";
                                }

                                var first = col.data.replace(new RegExp(r, 'g'), newID);
                                var second = first.replace(new RegExp('<br>', 'g'), '');
                                var result = second.replace(new RegExp('rounded-actions', 'g'), 'rounded-actions-resp');

                                toReturn = toReturn.concat('<td colspan=2 align=center>' + result + '</td>');
                            }
                            else {
                                toReturn = toReturn.concat('<td align=right style="width: 50%;">' + col.title + ':' + '</td> ');
                                toReturn = toReturn.concat('<td style="width: 50%;">' + col.data + '</td>');
                            }

                            toReturn = toReturn.concat('</tr>');
                        }
                        return toReturn;

                    }).join('');

                    return data ?
                        $('<table style = "width: 100%"/>').append(data) :
                        false;
                }
            }
        },
        "bDestroy": true
    });
    function format(celda) {
        // celda, es la celda de la tabla que contiene toda la informacion de la celda seleccionada
        if (celda !== undefined) {
            return `<table id='table-${celda[0]}' cellpadding="10" cellspacing="0" border="0" class="table table-striped w-100" >
                <tr>
                <td><b>Nombre paciente:</b></td>
                <td class="overflow-auto"> ${celda[2]} </td>
                <td><b>Rut paciente:</b></td>
                <td> ${celda[29]} </td>
                <td><b>Prof. destino:</b></td>
                <td>  ${celda[7]} </td>
                <td><b>Estado:</b></td>
                <td> ${celda[3]} </td>
                </tr>
                <tr>
                <td><b>Diagnóstico:</b></td>
                <td  class="overflow-auto" colspan="5"> ${celda[8]} </td>
                <td><b>Ámbito:</b></td>
                <td> ${celda[12]} </td>
                </tr>
                <tr>
                <td><b>Síntomas:</b></td>
                <td colspan="2" class="overflow-auto">  ${celda[9]} </td>
                <td ><b>Solicitud:</b></td>
                <td colspan="4" class="overflow-auto"> ${celda[10]} </td>
                </tr>

                <tr>
                <td><b>Respuesta:</b></td>
                <td colspan="7" class="overflow-auto">  ${celda[16]} </td>
                </tr>
                
                ${celda[14] == 76 ? `
                <tr>
                    <td ><b>Motivo cierre:</b></td>
                    <td class="overflow-auto"> ${celda[27]} </td>
                    <td><b>Observacion cierre:</b></td>
                    <td colspan="5" class="overflow-auto">  ${celda[28]} </td>
                </tr>
                `: ``}
                </table>`;
        }
    }

    $('#tblIC tbody').unbind().on('click', 'td.dt-control', function () {
        var table = $(this).parent().parent().parent().DataTable();
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            //la fila ya esta abierta.. click para cerrar
            row.child.hide();
            tr.removeClass('shown');
            //Id de la tabla par destruirla y crearla nuevamente
            let idTable = row.data()[0]
            $('#table-' + idTable).DataTable().destroy();
            $('#table' + idTable + ' tbody').empty();
        }
        else {
            //Abre esta fila
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

}
//funcion para aasignar especialista a la IC
function asignarEspecialistaSeleccionado() {
    if (validarCampos("#divSeleccionEspecialista", true)) {
        let idEspecialista = parseInt($("#sltEspecialista").val())
        let nombreEspecialista = $('select[name="sltNombreEspecialista"] option:selected').text()
        let idIC = parseInt($("#btnAsignarEspecialista").data("idic"))

        let body = {
            Id: idIC,
            IdProfesional: idEspecialista
        }
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}RCE_Interconsulta/${idIC}/Asignar`,
            async: true,
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data, status, jqXHR) {
                getTablaIC(sSession)
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Interconsulta asignada",
                    text: `Interconsulta con id  ${idIC} fue asignada a: ${nombreEspecialista}`,
                    showConfirmButton: false,
                    timer: 3000,
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        $("#mdlAsignarEspecialista").modal('hide')
                    }
                });
            },
            error: function (jqXHR, status) {
                console.log(jqXHR);
            }
        });

    } else {
        toastr.error("Debe seleccionar un especialista.")
    }
}
