﻿//Cargar combos bandeja
function cargarCombosBandeja() {
    CargarComboEspecialidad('#ddlEspecialidad1, #ddlEspecialidad2, #ddlEspecialidadDestino, #ddlEspecialidadOrigen');
    CargarComboGrupoPatologiaGES()
    CargarMotivosCierreIC()
    cargarSelectEstadosIc()
    cargarEspecialistas(idEspecialidad, "#sltEspecialistaFiltro")
    comboIdentificacion("#sltTipoIdentificacion")
    cargarComboProfesional();
}
//Cargar combos nuevo ic
function cargarCombosNuevoIc() {
    CargarComboEspecialidad('#sltEspecialidadOrigen, #sltEspecialidadDestino');
    CargarComboGrupoPatologiaGES();
    CargarComboEstablecimiento();
    CargarComboTipointerconsulta();
    CargarComboServicioSalud();
    CargarComboAmbito()
}
function cargarSelectEstadosIc() {
    let url = `${GetWebApiUrl()}GEN_Tipo_Estados_Sistemas/INTERCONSULTA`
    setCargarDataEnCombo(url, true, "#sltFiltroEstadoIc")
}
function cargarEspecialistas(id, elemento) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/buscar?idProfesion=1&idEspecialidad=${id}&idEstablecimiento=1`,
        //data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data, status, jqXHR) {
            console.log("medicos por especialidad",data)
            let datosSorted
            let datos = data.map(medico => {
                return {
                    Id: medico.Id,
                    Valor: `${medico.Persona.ApellidoPaterno} ${medico.Persona.ApellidoMaterno ?? ""} ${medico.Persona.Nombre}`
                }
            })
            datosSorted = datos.sort(function (a, b) {
                if (a.Valor > b.Valor) { //ordenar alfabeticamente ascendente
                    return 1;
                } else if (a.Valor < b.Valor) {
                    return -1;
                }
                return 0;
            });
            $(elemento).empty()
            $(elemento).append("<option value='0'>Seleccione</option>");
            $.each(datosSorted, function (i, medico) {
                $(elemento).append(`<option value='${medico.Id}'>${medico.Valor}</option>`);
            });
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
            alert('fail' + status.code);
        }
    });
}
function CargarMotivosCierreIC() {
    let url = `${GetWebApiUrl()}RCE_Motivo_Cierre_Interconsulta/Combo`;
    setCargarDataEnCombo(url, true, $("#motivoCierre"));
}
function CargarComboEspecialidad(sender) {
    let url = `${GetWebApiUrl()}GEN_Especialidad/Combo`;    
    setCargarDataEnCombo(url, true, sender)
}
function CargarComboGrupoPatologiaGES() {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/Combo`;
    setCargarDataEnCombo(url, true, $("#sltGrupoGes"));
}
function CargarComboAugeHijo(idPadre) {
    let url = `${GetWebApiUrl()}RCE_Patologia_GES/${idPadre}/Combo`;
    setCargarDataEnCombo(url, true, $("#sltSubGrupoGes"));
}
function cargarComboProfesional() {
    $('#ddlMedicoIC').empty().append("<option value='0'>-Seleccione-</option>");
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Interconsulta/Medicos`,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, r) {
                $('#ddlMedicoIC').append("<option value='" + r.GEN_idProfesional + "'>" + r.GEN_nombrePersonas + "</option>");
            });

            if (idPro != null && idPro != undefined) {
                $('#ddlMedicoIC').prop('value', idPro);
            }
        }
    });
}
//Aca van las funciones usadas en nuevo IC
function CargarComboEstablecimiento() {
    let url = `${GetWebApiUrl()}GEN_Establecimiento/Combo`;
    setCargarDataEnCombo(url, false, $("#sltEstablecimientoOrigen"));
}
function CargarComboTipointerconsulta() { 
    let url = `${GetWebApiUrl()}RCE_Tipo_Interconsulta/Combo`;
    setCargarDataEnCombo(url, false, $("#sltTipoInterconsulta"));
}
function CargarComboServicioSalud() {
    let url = `${GetWebApiUrl()}GEN_Servicio_Salud/Combo`;
    setCargarDataEnCombo(url, false, $("#sltServicioSaludOrigen"));
}
function CargarComboAmbito() {
    let url = `${GetWebApiUrl()}GEN_Ambito/Combo`;
    setCargarDataEnCombo(url, false, $("#sltAmbito"));
}