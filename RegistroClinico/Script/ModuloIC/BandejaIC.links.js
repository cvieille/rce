﻿const linkVerEditar = (sender) => {
    let id = $(sender).data("id");
    setSession('ID_INTERCONSULTA', id);
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/NuevoIC.aspx");
}
const linkMovimientos = (sender) => {
    let id = $(sender).data("id")    
    let _url = `${GetWebApiUrl()}RCE_Interconsulta/${id}/Movimientos`
    getMovimientosSistemaGenerico(_url, id)
}
const linkCerrar = (sender) => {
    let id = $(sender).data('id');
    $('#idIC').val(id);

    $("#txtNombrePaciente").text($(sender).data('nombrepaciente'))
    $("#txtOrigenSolicitud").text($(sender).data('especialidadorigen'))
    $("#txtDestinoSolicitud").text($(sender).data('especialidaddestino'))
    $("#txtEstado").text($(sender).data('estado'))
    mostrarModal("mdlCerrar");    
}
const linkRechazar = (sender)=> {
    let id = $(sender).data('id');
    let nombre = $(sender).data('nombre');
    let rut = $(sender).data('rut');
    $('#idIC').val(id);
    mostrarMensajeConfirmacionAnularInterconsulta(id, nombre, rut);
    
}
const CerrarIC = () => {
    let id = $('#idIC').val()
    let body = {
        Id: parseInt(id),
        IdMotivo: parseInt($("#motivoCierre").val()),
        Observacion: $("#txtMotivoCierre").val(),
    }

    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}RCE_Interconsulta/${id}/Cerrar`,
        contentType: "application/json",
        data: JSON.stringify(body),
        success: function (data, status, jqXHR) {
            $('#idIC').val('0');
            getTablaIC(sSession);
            toastr.success('InterConsulta CERRADA');
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });
    $("#motivoCierre").val(0)
    $("#txtMotivoCierre").val("")
    $('#modalAlerta').modal('hide');
    $('#mdlCerrar').modal('hide');
}
const AnularIC= () => {
    let idIC = $('#idIC').val()
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}RCE_Interconsulta/${idIC}/Anular`,
        async: true,
        success: function (data, status, jqXHR) {
            $('#idIC').val('0');
            getTablaIC(sSession);
            $('#modalAlerta').modal('hide');
            Swal.fire({
                position: 'center',
                icon: 'info',
                title: "Interconsulta Anulada",
                text: `Interconsulta con ID  ${idIC} ha sido anulada`,
                showConfirmButton: false,
                timer: 3000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {

                }
            });
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });

}
const linkEspecialidad = (sender) => {
    var iID = $(sender).data('id');
    $('#iID').val(iID);
    let nombrePaciente = $(sender).data('nombrepaciente');
    let idOrigen = $(sender).data('origen');
    let idDestino = $(sender).data('destino');
    let rut = $(sender).data('rut');
    //Debe mostrar la informacion de la IC en el modal para
    //Que el usuario sepa que interconsulta esta enviando
    $("#spanIdIc").text(iID)
    $("#spanNombrePaciente").text(nombrePaciente)
    $("#spanRutPaciente").text(rut)
    $('#ddlEspecialidad1').val(idOrigen);
    $('#ddlEspecialidad2').val(idDestino);
    mostrarModal("mdlEspecialidad");
}
const AsignarEspecialista = (sender) => {
    let id = $(sender).data("esp")
    let idIC = $(sender).data("id")
    //espDestinoAE
    $("#idICAE").text(idIC)
    $("#nombrePacienteAE").text($(sender).data("nombrepaciente"))
    $("#profSolicitaAE").text($(sender).data("profsolicita"))
    $("#diagnosticoAE").text($(sender).data("diagnostico"))
    $("#espDestinoAE").text($(sender).data("espdestino"))
    cargarEspecialistas(id, "#sltEspecialista")
    mostrarModal("mdlAsignarEspecialista");    
    $("#btnAsignarEspecialista").data("idic", idIC)
}
const linkResponder = (sender) => {
    let id = $(sender).data("id");
    setSession('ID_INTERCONSULTA', id);
    setSession('RESPUESTA_INTERCONSULTA', true);
    //quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/NuevoIC.aspx");
}
const linkImprimir = (sender) => {
    const id = $(sender).data("id");
    const url = `${GetWebApiUrl()}RCE_Interconsulta/${id}/Imprimir`;
    ImprimirApiExterno(url);
}

function mostrarMensajeConfirmacionAnularInterconsulta(id, nombre, rut) {
    ShowModalAlerta('CONFIRMACION',
        'Anular IC',
        'Una vez anulada esta interconsulta con el ID <b> ' + id + '</b> del paciente:<br><b> ' + nombre + ',</b> <br> rut:<b> ' + rut + ' </b><br> será quitada de la lista.',
        AnularIC, null, 'ANULAR IC', 'CANCELAR');
}
