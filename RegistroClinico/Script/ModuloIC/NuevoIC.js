﻿
var sSession = null;
var ic = {}, icGes = {};
let EspecialidadProf, idPro
var alertFlag = 0;
let idCama, idPaciente, idHospitalizacion, IdInterconsultaGes, respondiendoInterconsulta = false;
var bunload = function beforeunload(e) {
    e.preventDefault();
    e.returnValue = '';
}
const tipoInterconsulta ={

};
$(document).ready(function () {
    cargarCombosNuevoIc();

    sSession = getSession();

    idCama = sSession.ID_CAMA
    deleteSession('ID_CAMA');   
    RevisarAcceso(true, sSession);
    window.addEventListener('beforeunload', bunload, false);

    $("#sltServicioSaludOrigen").val(1).change()
    $("#sltEstablecimientoOrigen").val(1).change()
    //Al scrollear hacia abajo se muestra el alert de Campos Obligatorios
    $(window).scroll(function () {
        if (alertFlag) {
            ($($(this)).scrollTop() > 150) ? $('#alertObligatorios').stop().show('fast') : $('#alertObligatorios').stop().hide('fast');
        }
    });

    $("#divOtrosGes, #divSubGruposGes").hide(); //Exc Tipo interconsulta == "Otro"
    $.fn.bootstrapSwitch.defaults.onColor = 'info';
    $.fn.bootstrapSwitch.defaults.offColor = 'danger';
    $.fn.bootstrapSwitch.defaults.onText = 'SI';
    $.fn.bootstrapSwitch.defaults.offText = 'NO';
    $("#sltAuge").bootstrapSwitch('state', false);

    $("#divAuge").hide();

    $("#sltAuge").on('switchChange.bootstrapSwitch', function () {
        
        ($(this).bootstrapSwitch('state')) ? $('#divAuge').stop().fadeIn(500) : $('#divAuge').stop().fadeOut(500);
        if ($("#sltAuge").bootstrapSwitch('state'))
            DarRequiredAuge();
    })
    $('#chbAuge').on('switchChange.bootstrapSwitch', function (event, state) {
        (state) ? $('#divAuge').stop().fadeIn(500) : $('#divAuge').stop().fadeOut(500);
        DarRequiredAuge();
    });

    $("#sltTipoInterconsulta").on('change', function () {
        ($(this).val() == 4) ? $("#divOtrosGes").fadeIn(500) : $("#divOtrosGes").fadeOut(500);
        $("#txtOtrosGes").attr("data-required", ($(this).val() == 4).toString());
        $("#txtOtrosGes").val('');
    });

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'GEN_Usuarios/LOGEADO',
        async: false,
        success: function (data) {

            let idEsp
            idPro = data[0].Profesional.Id;

            // Especialidad no es null
            if (data[0].Profesional.Especialidad !== null) {
                idEsp = data[0].Profesional.Especialidad.Id
            }
            // JefeEspecialidad no es null y tampoco un arreglo vacio
            else if (data[0].Profesional.JefeEspecialidad !== null && data[0].Profesional.JefeEspecialidad.length > 0) {
                idEsp = data[0].Profesional.JefeEspecialidad[0].IdEspecialidad
            }

            // Si hay valor valido
            if (idEsp !== undefined) {
                //Esperar un intervalo de tiempo mientras se carga el combo
                setTimeout(function () {
                    $("#sltEspecialidadOrigen").val(idEsp)
                    $("#sltEspecialidadOrigen").attr("disabled", true)
                }, 150);
            }
                
        }
    });

    if (sSession.RESPUESTA_INTERCONSULTA == true) {
        respondiendoInterconsulta = true

        $('#divDatosPaciente input').attr('disabled', true);
        $('#divDatosClinicos textarea').attr('disabled', true);

        $('#divDerivado select').attr('disabled', true);
        $('#IcGes select').attr('disabled', true);

        /*$('#divDatosClinicos input').bootstrapSwitch('disabled', true);*/
        $('#divCargarArchivos').hide();
        $('#aGuardarIC').html('<i class="fa fa-reply"></i> Responder')
        $('#aGuardarIC').data('save', true)
        $("#sltAuge").bootstrapSwitch('disabled', true);
        //Descomentar para dar respuesta a la IC
        $('#divRespuesta').show();
        //$("html, body").animate({ scrollTop: document.body.scrollHeight }, 'slow');
        deleteSession('RESPUESTA_INTERCONSULTA')
    }


    CargarUsuario(idPro);

    

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": function () {
            SalirIC();
        },
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3500,
        "extendedTimeOut": 2500,
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    }

    deleteSession('ID_INTERCONSULTA');
    ShowModalCargando(false);
    //Cuando se agrega el rut de un paciente y existe el paciente... procede a buscar la hospitalizacion
    $("#txtDigitoPac").on('input', function (e) {

        if ($("#txtDigitoPac").val() == "") {
            $("#infoHosp").addClass("d-none")
        } else {
            buscarPacientePorDocumento($("#sltIdentificacion").val(), $("#txtnumerotPac").val())
        }
        $("#infoHosp").addClass("d-none")
        setTimeout(function () {
            idPaciente = GetPaciente().GEN_idPaciente
            buscarPacienteHospitalizado(idPaciente)
        }, 500);
    });

    $("#txtnumerotPac").on('keydown', function (e) {
        if (!$("#infoHosp").hasClass("d-none")) {
            $("#infoHosp").addClass("d-none")
        }
        limpiarInputsHospitalizacion()
    })

});

$('#sltGrupoGes').change(async function () {
    idPadre = $('#sltGrupoGes').val()
    CargarComboAugeHijo(idPadre)
    await sleep(30)
    if ($("#sltSubGrupoGes > option").length <= 1) { //No tiene valores
        $('#divSubGruposGes').hide()
        $("#sltSubGrupoGes").attr("data-required", "false");
    } else {
        $('#divSubGruposGes').show()
        $("#sltSubGrupoGes").attr("data-required", "true");
    }
    ReiniciarRequired()
});

async function CargarUsuario(idPro) {
    if (sSession.CODIGO_PERFIL == '10' && sSession.ID_INTERCONSULTA == undefined) {
        quitarListener();
        window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
    }

    if (sSession.ID_EVENTO != undefined) {
        ic.RCE_idEventos = parseInt(sSession.ID_EVENTO);
        deleteSession("ID_EVENTO");
    }

    //Este es el flujo cuando viene desde la bandeja de hospitalizacion...
    if (sSession.ID_PACIENTE != undefined) {
        let idPacienteBusqueda = sSession.ID_PACIENTE
        await CargarPacienteActualizado(idPacienteBusqueda)
        buscarPacienteHospitalizado(idPacienteBusqueda)
        BloquearPaciente();
        CargarProfesional(idPro);
        deleteSession("ID_PACIENTE");
        if ($("#sltPrevision").val() == 0) {
            $("#sltPrevision").removeAttr("disabled")
        }

    } else if (sSession.ID_INTERCONSULTA != undefined) {
        //Este es el flujo cuando se esta editando o dando respuesta ala IC
        CargarIC();
    } else {
        CargarProfesional(idPro);

    }
}
function limpiarInputsHospitalizacion() {
    $("#txtIdHosp").val("")
    $("#txtFechaHosp").val("")
    $("#txtCamaHosp").val("")
    $("#txtUbicacionHosp").val("")
    $("#txtEstadoHosp").val("")
    //$("#sltServicioSaludOrigen").val(0)
    //$("#sltEstablecimientoOrigen").val(0)
    //$("#sltServicioSaludOrigen").attr('disabled', false)
    //$("#sltEstablecimientoOrigen").attr('disabled', false)
}
function buscarPacienteHospitalizado(idPaciente) {
    limpiarInputsHospitalizacion()

    if (idPaciente !== undefined) {
        let Hospitalizacion = buscarHospitalizacion(idPaciente)
        //esta hospitalizado actualmente.
        if (Hospitalizacion !== undefined) {
            if (Hospitalizacion.length > 0) {
                idHospitalizacion = Hospitalizacion[0].Id
                idCama = Hospitalizacion[0].Cama.Actual.Id
                //Buscar ubicacion actual.
                let ObjectHospitalizacion = {
                    IdHosp: Hospitalizacion[0].Id,
                    Fecha: moment(Hospitalizacion[0].Fecha).format("DD-MM-YYYY"),
                    Cama: Hospitalizacion[0].Cama.Actual.Valor,
                    Ubicacion: Hospitalizacion[0].Cama.Actual.Ubicacion.Valor,
                    Estado: Hospitalizacion[0].Estado.Valor
                }
                llenarInputHospitalizacion(ObjectHospitalizacion)
                if (idCama) {
                    dibujarUbicacionEstablecimiento(idCama)
                }
                toastr.info("El paciente buscado se encuentra hospitalizado")
                $("#infoHosp").removeClass("d-none")
            }
        }
    }
}
function dibujarUbicacionEstablecimiento(idCama) {
    const Ubicacion = buscarUbicacion(idCama)
    const idEstablecimiento = Ubicacion.Establecimiento.Id
    const Establecimiento = buscarEstablecimiento(idEstablecimiento)
    const idEstablecimientoPadre = Establecimiento.Id

    if (idEstablecimientoPadre)
        $("#sltServicioSaludOrigen").val(idEstablecimientoPadre)
    $("#sltServicioSaludOrigen").attr('disabled', true)
    if (idEstablecimiento)
        $("#sltEstablecimientoOrigen").attr('disabled', true)
    $("#sltEstablecimientoOrigen").val(idEstablecimiento)
}
function llenarInputHospitalizacion(ObjectHospitalizacion) {
    $("#txtIdHosp").val(ObjectHospitalizacion.IdHosp)
    $("#txtFechaHosp").val(ObjectHospitalizacion.Fecha)
    $("#txtCamaHosp").val(ObjectHospitalizacion.Cama)
    $("#txtUbicacionHosp").val(ObjectHospitalizacion.Ubicacion)
    $("#txtEstadoHosp").val(ObjectHospitalizacion.Estado)
}
function buscarHospitalizacion(idPaciente) {
    if (idPaciente != undefined) {
        let HospitalizacionPaciente
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}HOS_Hospitalizacion/Buscar?idTipoEstado[0]=87&idTipoEstado[1]=88&idPaciente=${idPaciente}`,
            async: false,
            success: function (data) {
                HospitalizacionPaciente = data
            }, error: function (err) {
                toastr.error("Error al buscar la hospitalizacion")
                console.log("Error al buscar la hospitalizacion Error:",err)
            }
        });
        return HospitalizacionPaciente
    }
}
function buscarUbicacion(id) {
    let Ubicacion
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/HOS_Cama/${id}`,
        async: false,
        success: function (data) {
            Ubicacion = data
        }
    });
    return Ubicacion
}
function buscarEstablecimiento(id) {
    let Establecimiento
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/GEn_Establecimiento/${id}`,
        async: false,
        success: function (data) {
            Establecimiento = data
        }
    });
    return Establecimiento
}

function DarRequiredAuge() {
    //sltAuge
    if ($("#sltAuge").bootstrapSwitch('state')) {
        $("#sltSubGrupoGes").attr("data-required", ($("#sltSubGrupoGes").children("option").length < 0).toString())
        $("#txtOtrosGes").attr("data-required", ($("#txtOtrosGes").val() == 4).toString());
        $("#txtFundamentosDiagnosticoGes, #txtTratamientosGes, #sltTipoInterconsulta, " +
            "#sltGrupoGes").attr("data-required", "true");
        $("#txtSintomalogia, #txtSolicitante").attr("data-required", "false");
    } else {
        $("#txtFundamentosDiagnosticoGes, #txtTratamientosGes, #sltTipoInterconsulta, " +
            "#sltGrupoGes, #txtOtrosGes, #sltSubGrupoGes").attr("data-required", "false");
        $("#txtSintomalogia, #txtSolicitante").attr("data-required", "true");
    }

    ReiniciarRequired();

}

function CargarProfesional(idProfesional) {
   
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + "GEN_Profesional/" + idProfesional,
        async: false,
        success: function (data, status, jqXHR) {
            $("#txtNumeroDocumentoProfesional").val(data.GEN_rutProfesional +
                (data.GEN_digitoProfesional !== null ? '-' + data.GEN_digitoProfesional : ''));
            $("#txtNombreProfesional").val(data.GEN_nombreProfesional);
            $("#txtApePatProfesional").val(data.GEN_apellidoProfesional);
            $("#txtApeMatProfesional").val(data.GEN_sapellidoProfesional);

        },
        error: function (jqXHR, status) {
            console.log("Error al cargar Profesional: " + jqXHR.responseText);
        }
    });
}

function CargarIC() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}RCE_Interconsulta/${sSession.ID_INTERCONSULTA}`,
        async: false,
        success: function (data, status, jqXHR) {
            //Si es ges consulta
            console.log(data)
            if (data.Ges) {
                $("#sltAuge").bootstrapSwitch('state', true)

                $.ajax({
                    method: "get",
                    url: `${GetWebApiUrl()}RCE_interconsulta/${data.Id}/ges`,
                    success: function (data) {
                        if (data.length > 0) {


                            ic.InterconsultaGES = data[0]
                            IdInterconsultaGes = data[0].IdInterconsultaGES;
                            $("#divAuge").show();
                            
                            CargarICGes(data[0]);

                        }
                    }, error: function (err) {
                        console.log(`ha ocurrido un error al intentar obtener el ges de la interconsulta con id: ${data.Id}`)
                        console.log(err)
                    }
                })
            }


            CargarJsonICNuevo(data);
            
            $("#sltEspecialidadDestino").val(ic.IdEspecialidadDestino);
            $("#sltAmbito").val(ic.IdAmbito);

            // DATOS CLINICOS
            $("#txtDiagnostico").val($.trim(ic.Diagnostico));
            $("#txtSintomalogia").val($.trim(ic.Sintomatologia));
            $("#txtSolicitante").val($.trim(ic.Solicitud));
            $("#strFechaCreacion").text(moment(ic.Fecha).format("DD-MM-YYYY"));
            CargarPacienteActualizado(data.Paciente.Id);
           
            buscarPacienteHospitalizado(data.Paciente.GEN_idPaciente)
            //cargarArchivosExistentes(ic.Id, 'IC');

            


            $("#sltServicioSaludOrigen").val(data.Origen.ServicioSalud.Id);
            $("#sltEstablecimientoOrigen").val(data.Origen.Establecimiento.Id);
            $("#sltEspecialidadOrigen").val(data.Origen.Especialidad.Id);
            $('#sltServicioSaludOrigen').prop('disabled', 'disabled');
            $('#sltEstablecimientoOrigen').prop('disabled', 'disabled');

            if (data.Origen.Profesional !== null) {
                CargarProfesional(data.Origen.Profesional.Id)
            }
            console.log(respondiendoInterconsulta)
            if (respondiendoInterconsulta) {//interconsultor
                promesaAjax("GET", `Rce_Interconsulta/${sSession.ID_INTERCONSULTA}/Respuestas`).then(res => {
                    console.log(res)
                    if (res.length > 0) {
                        //Dibujar historial de respuestas
                        let htmlRespuestas = ""
                        res.map((a, i) => {
                            htmlRespuestas += `
                                <div class"col-md-12 m-2">
                                    <div class="card">
                                        <div class="card-body row">
                                            
                                            <div class="col-md-8">
                                               <h5>Respuesta:</h5>
                                                ${a.Respuesta}
                                            </div>
                                            <div class="col-md-4">
                                                <p>
                                                <b>Respondida por:</b>
                                                ${a.Profesional.Nombre} 
                                                ${a.Profesional.ApellidoPaterno} 
                                                ${a.Profesional.ApellidoMaterno}
                                                </p>
                                                <p>
                                                <b>Fecha de respuesta:</b>
                                                ${moment(a.Fecha).format("DD-MM-YYYY HH:mm")}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
                        })
                        $("#divRespuestasAnteriores").append(htmlRespuestas)

                        let respuestasLength = res.length - 1
                        //para escribir la ultima respuesta
                        //textboxio.replace("#txtRespuesta").content.set(res[respuestasLength].Respuesta);
                    }
                }).catch(error => {
                    console.error("Ha ocurrido un error al cargas las respuesteas de  la interconsulta")
                    console.log(error)
                })
            }
        },
        error: function (jqXHR, status) {
            console.log("Error al cargar IC: " + jqXHR.responseText);
        }
    });

    //Desactivar campos no editables
    $('#sltIdentificacion').prop('disabled', 'disabled');
    $('#txtnumerotPac').prop('disabled', 'disabled');
    $('#txtDigitoPac').prop('disabled', 'disabled');
}

async function CargarICGes(IcGES) {
    //ESTE JSON SE UTILIZA EN CASO DE TENER QUE INACTIVAR LA INTERCONSULTA GES
    CargarJsonICGes(IcGES);

    let idPatologiaHijo = IcGES.Patologia!==null?IcGES.Patologia.Id:undefined;
    let idPatologiaPadre = IcGES.PatologiaPadre !== null ? IcGES.PatologiaPadre.Id : undefined;
    if (IcGES.PatologiaPadre == null) {
        idPatologiaPadre = idPatologiaHijo;
        idPatologiaHijo = undefined
    }

    $("#sltTipoInterconsulta").val(IcGES.Tipo.Id);


    $("#sltGrupoGes").val(idPatologiaPadre);
    if (idPatologiaHijo !== undefined) {
        $("#divSubGruposGes").show()
        CargarComboAugeHijo(idPatologiaPadre)
        await sleep(300)
        console.log(idPatologiaHijo)
        $("#sltSubGrupoGes").val(idPatologiaHijo)
    }
    $("#txtFundamentosDiagnosticoGes").val($.trim(IcGES.FundamentosDiagnostico));
    $("#txtTratamientosGes").val($.trim(IcGES.Tratamiento));

    if (IcGES.Otros != null) {
        $("#txtOtrosGes").val($.trim(IcGES.Otros));
        $("#divOtrosGes").show()
    }

    DarRequiredAuge();

}

function GuardarIC() {
    CargarJsonIC();
    let url = GetWebApiUrl()
    
    let method = (ic.Id != undefined) ? 'PUT' : 'POST';
    (ic.Id != undefined) ? url += `RCE_Interconsulta/${ic.Id}` : url += "RCE_Interconsulta/"

    $.ajax({
        type: method,
        url: url,
        data: JSON.stringify(ic),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, status, jqXHR) {
            //ic.Id = (ic.Id == undefined) ?
            //    data.RCE_idInterconsulta : ic.Id;
            //if ($("#chbAuge").bootstrapSwitch('state'))
            //    GuardarICGes(); 
            //else if (!$("#chbAuge").bootstrapSwitch('state') && icGes.RCE_idInterconsulta_GES != undefined) {
            //    icGes.RCE_estadoInterconsulta_GES = "Inactivo"
            //    GuardarICGes();
            //    //revisar
            //}else 
            //    SubirTodosArchivos(ic.RCE_idInterconsulta, 'IC', null);

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Datos Actualizados correctamente',
                showConfirmButton: false,
                timer: 2000,
                allowOutsideClick: false
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    SalirIC();
                }
            });
            
        },
        error: function (jqXHR, status) {
            alert("Error al guardar Interconsulta: " + jqXHR.status);
            console.log(jqXHR)
            console.log(status)
        }
    });
}
function CerrarIC() {
    CargarJsonIC();
    let url = `${GetWebApiUrl()}RCE_Interconsulta/${ic.RCE_idInterconsulta}`;
    ic.GEN_idTipo_Estados_Sistemas = 76;
    $.ajax({
        type: 'PUT',
        url: url,
        data: JSON.stringify(ic),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status, jqXHR) {
            quitarListener();
            window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
        },
        error: function (jqXHR, status) {
            console.log("Error al cerrar IC: " + jqXHR.message);
            $('#modalAlerta').modal('hide');
        }
    });
}
function BorradorRespuesta() {
    quitarListener();
    window.location.replace(ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx");
}
async function CrearInterconsulta(element) {
    let saving = false
    saving = $(element).data('save')

    //Guardando respuesta a la IC
    if (saving == true) //interconsultor
    {
        var json = {
            Id: sSession.ID_INTERCONSULTA,
            Respuesta: ('#txtRespuesta').val()
        };
        var sMetodo = 'PATCH'

        let url = `${GetWebApiUrl()}RCE_interconsulta/${sSession.ID_INTERCONSULTA}/respuesta`;


        $.ajax({
            type: sMetodo,
            url: url,
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, status, jqXHR) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Respuesta guardada',
                    showConfirmButton: false,
                    timer: 1000,
                    allowOutsideClick: false
                }).then((result) => {
                    BorradorRespuesta()
                });
            },
            error: function (jqXHR, status) {
                console.log("Error al guardar Respuesta: " + jqXHR.message);
            }
        });
    } else {
        if ((validarCampos("#divDatosClinicos", true)
            & validarCampos("#divDerivado", true)
            & validarCampos("#divDatosPaciente", true)
            & validarCampos("#divEnviado", true)
            & validarCampos("#IcGes", true)
        )) {
            $('#aGuardarIC').addClass('disabled');
            $('#aGuardarIC').attr('disabled', true);
            $('#aCancelarInterconsulta').addClass('disabled');
            $('#aCancelarInterconsulta').attr('disabled', true);
            await GuardarPaciente();
            GuardarIC();
        } else {
            console.log("Error al validar los campos")
            console.log("auge", validarCampos("#divAuge", true))
            console.log("datos clinicos", validarCampos("#divDatosClinicos", true))
            console.log("derivado", validarCampos("#divDerivado", true))
            console.log("datos paciente", validarCampos("#divDatosPaciente", true))
            console.log("enviado", validarCampos("#divEnviado", true))
            toastr.error("Error, faltan completar campos obligatorios")
            alertFlag = 1;
            return;

        }
    }
}

function CancelarIC() {
    ShowModalAlerta('CONFIRMACION',
        'Salir de la Interconsulta',
        '¿Está seguro que desea cancelar el registro?',
        SalirIC);
}

function SalirIC() {
    quitarListener();
    location.href = ObtenerHost() + "/Vista/ModuloIC/BandejaIC.aspx";
}


//Formatear Json ---Aca se esta editando
function CargarJsonICNuevo(json) { //Pasar a Variables JSON

    ic.Id = json.Id;
    ic.Diagnostico = json.Diagnostico
    ic.Sintomatologia = json.Sintomatologia
    ic.Solicitud = json.Solicitud
    ic.FechaIngreso = moment(json.Fecha).format("DD-MM-YYYY");
    ic.IdAmbito = json.Ambito.Id ?? null
    ic.IdEspecialidadOrigen = json.Origen.Especialidad.Id
    ic.IdEspecialidadDestino = json.Destino.Especialidad.Id
    //ic.IdEvento = json.IdEvento
    //ic.Estado = json.Estado.Valor
    ic.IdEstado = json.Estado.Id
    //if (json.Ges)
    //    ic.IdInterconsultaGES = json.InterconsultaGES.IdInterconsultaGES;
    ic.Respuestas = json.Destino.Respuestas
}

function CargarJsonIC() {
    ic.IdEspecialidadOrigen = parseInt(valCampo($("#sltEspecialidadOrigen").val()));
    ic.IdEspecialidadDestino = parseInt(valCampo($("#sltEspecialidadDestino").val()));
    ic.idEstablecimiento = parseInt($("#sltEstablecimientoOrigen").val())
    ic.IdAmbito = parseInt(valCampo($("#sltAmbito").val()));
    ic.IdHospitalizacion = parseInt(valCampo($("#txtIdHosp").val()) ?? null);
    ic.Diagnostico = valCampo($("#txtDiagnostico").val());
    ic.Sintomatologia = valCampo($("#txtSintomalogia").val());
    ic.Solicitud = valCampo($("#txtSolicitante").val());
    ic.IdPaciente = GetPaciente().Id ?? null
    ic.IdHospitalizacion = idHospitalizacion ?? null
    ic.ges = $("#sltAuge").bootstrapSwitch('state')
    if (sSession.ID_INTERCONSULTA == undefined) {
        ic.FechaIngreso = moment(GetFechaActual()).format("YYYY-MM-DD HH:mm:ss");
    }
    if ($("#sltAuge").bootstrapSwitch('state')) {
        CargarJsonICGes()
    }

}

//function CargarJsonICGesNuevo(IcGES) {
//    icGes.RCE_idTipo_Interconsulta = IcGES.IdTipoInterconsulta
//    icGes.RCE_fundamentos_diagnosticoInterconsulta_GES = IcGES.Diagnostico;
//    icGes.RCE_tratamientoInterconsulta_GES = IcGES.Tratamiento;
//    //icGes.RCE_estadoInterconsulta_GES = json.RCE_estadoInterconsulta_GES;
//    icGes.RCE_idInterconsulta = IcGES.IdInterconsultaGES;
//    icGes.RCE_idPatologia_GES = IcGES.IdPatologia.IdPatologiaPadre ?? 0
//    icGes.RCE_otros_tipoInterconsulta_GES = IcGES.Otros;
//    icGes.RCE_idInterconsulta_GES = IcGES.IdInterconsultaGES;

//}

function CargarJsonICGes() {
    
    const InterconsultaGES = {
        IdTipoInterconsulta: parseInt(valCampo($("#sltTipoInterconsulta").val())),
        FundamentosDiagnostico: valCampo($.trim($("#txtFundamentosDiagnosticoGes").val())),
        Tratamiento: valCampo($.trim($("#txtTratamientosGes").val())),
        IdPatologiaGES: parseInt(($("#sltSubGrupoGes").children("option").length == 1) ? $("#sltGrupoGes").val() : $("#sltSubGrupoGes").val()),//revisar
        Otros: valCampo($.trim($("#txtOtrosGes").val()))
    }


    //Editando una ges
    if (IdInterconsultaGes !== undefined && IdInterconsultaGes !== null)
        InterconsultaGES.IdInterconsultaGes = IdInterconsultaGes;
    ic = { ...ic, InterconsultaGES }
}
