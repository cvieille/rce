﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Dtos
{
    public class ImpresorasZebraDto
    {
        public List<ImpresoraZebraDto> Impresoras { get; set; }
    }
    public class ImpresoraZebraDto
    {
        public string Tipo { get; set; }
        public string NombreImpresora { get; set; }
        public string Nombre { get; set; }
        public string IP { get; set; }
    }
}