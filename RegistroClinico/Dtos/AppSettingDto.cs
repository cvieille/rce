﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistroClinico.Dtos
{

    public class AppSettingDto
    {
        public string Ambiente { get; set; }
        public List<IpServer> IpServers { get; set; }
        public bool VerificarFonasa { get; set; }
        public List<AccesoDto> WebApi { get; set; }
        public List<AccesoDto> Pabellon { get; set; }
        public List<AccesoDto> ApiFunciones { get; set; }
        public string Version { get; set; }
    }

    public class IpServer
    {
        public string Ip { get; set; }
        public string Nombre { get; set; }
    }

    public class AccesoDto
    {
        public string Url { get; set; }
        public string Ambiente { get; set; }
    }
}