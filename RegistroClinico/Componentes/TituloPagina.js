﻿
class tituloPagina extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        ////////////////////////////////////////
        ///////Dibuja titulo en pagina//////////
        ////////////////////////////////////////
        this.innerHTML = `
            <section class="content-header">
                 <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-12 text-center">
                            <h3 id="hTitle-page" id="title" > ${this.getAttribute("data-title")}</h3>
                        </div>
                    </div>
                </div>
            </section>`;
    }
}
window.customElements.define("titulo-pagina", tituloPagina)